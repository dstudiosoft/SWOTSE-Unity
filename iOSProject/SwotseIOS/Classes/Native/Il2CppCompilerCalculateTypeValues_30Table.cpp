﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// WebSocketSharp.WsFrame/<dump>c__AnonStorey1C
struct U3CdumpU3Ec__AnonStorey1C_t1594701605;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// WebSocketSharp.PayloadData
struct PayloadData_t688932160;
// WebSocketSharp.WsFrame
struct WsFrame_t4110797288;
// System.IO.Stream
struct Stream_t1273022909;
// System.Action`1<WebSocketSharp.WsFrame>
struct Action_1_t4283264883;
// System.Collections.Generic.List`1<WebSocketSharp.Net.Cookie>
struct List_1_t1380209731;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Version
struct Version_t3456873960;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// WebSocketSharp.Net.WebHeaderCollection
struct WebHeaderCollection_t1205255311;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t2033454307;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t1399197998;
// System.Collections.Generic.List`1<ArabicMapping>
struct List_1_t796524086;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t407452768;
// System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>
struct EventHandler_1_t3466559488;
// System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>
struct EventHandler_1_t2074869383;
// Firebase.Messaging.FirebaseMessaging/Listener
struct Listener_t3000907612;
// Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate
struct MessageReceivedDelegate_t2474724020;
// Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate
struct TokenReceivedDelegate_t1016457320;
// Firebase.FirebaseApp
struct FirebaseApp_t2526288410;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate
struct ExceptionDelegate_t1699519678;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate
struct ExceptionArgumentDelegate_t1079801895;
// System.Exception
struct Exception_t;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct SWIGStringDelegate_t3107596264;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.Specialized.NameObjectCollectionBase/_Item
struct _Item_t2272350267;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.IHashCodeProvider
struct IHashCodeProvider_t267601189;
// System.Collections.IComparer
struct IComparer_t1540313114;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t1318642398;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1493878338;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t701940803;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t1884415901;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_t768590915;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t3913627115;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_t2311174851;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_t3841783507;
// System.Action`1<System.Byte[]>
struct Action_1_t4289115252;
// System.Action`1<System.Exception>
struct Action_1_t1609204844;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Func`2<System.Int32,System.Boolean>
struct Func_2_t1300586518;
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct Func_2_t235587086;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t2197129486;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t2606371118;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t3014364904;
// System.Func`1<System.Boolean>
struct Func_1_t3822001908;
// WebSocketSharp.WebSocket
struct WebSocket_t62038747;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>
struct ObjectPool_1_t240936516;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t2748928575;
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct UnityAction_1_t2508470592;
// System.Action
struct Action_t1264377477;
// System.Void
struct Void_t1185182177;
// System.Uri
struct Uri_t100236324;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Collider
struct Collider_t1773347010;
// Mono.Security.Protocol.Tls.SslStreamBase
struct SslStreamBase_t1667413407;
// System.Net.Security.LocalCertificateSelectionCallback
struct LocalCertificateSelectionCallback_t2354453884;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t4072576034;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.Dictionary`2<System.String,WebSocketSharp.Net.HttpHeaderInfo>
struct Dictionary_2_t1847514588;
// System.Action`2<WebSocketSharp.LogData,System.String>
struct Action_2_t488357320;
// WebSocketSharp.AuthenticationChallenge
struct AuthenticationChallenge_t3974775170;
// WebSocketSharp.Net.WebSockets.WebSocketContext
struct WebSocketContext_t619421455;
// WebSocketSharp.Net.CookieCollection
struct CookieCollection_t962330244;
// WebSocketSharp.Net.NetworkCredential
struct NetworkCredential_t1094796801;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;
// System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String>
struct Func_2_t1203693676;
// WebSocketSharp.Logger
struct Logger_t4025333586;
// WebSocketSharp.WsStream
struct WsStream_t2500460924;
// System.Net.Sockets.TcpClient
struct TcpClient_t822906377;
// System.EventHandler`1<WebSocketSharp.CloseEventArgs>
struct EventHandler_1_t4095840750;
// System.EventHandler`1<WebSocketSharp.ErrorEventArgs>
struct EventHandler_1_t1344838444;
// System.EventHandler`1<WebSocketSharp.MessageEventArgs>
struct EventHandler_1_t149217156;
// System.EventHandler
struct EventHandler_t1348719766;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560;
// System.Diagnostics.StackFrame
struct StackFrame_t3217253059;
// UnityEngine.RectOffset
struct RectOffset_t1369453676;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t881764471;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;




#ifndef U3CMODULEU3E_T692745553_H
#define U3CMODULEU3E_T692745553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745553 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745553_H
#ifndef U3CMODULEU3E_T692745554_H
#define U3CMODULEU3E_T692745554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745554 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745554_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745552_H
#define U3CMODULEU3E_T692745552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745552 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745552_H
#ifndef U3CDUMPU3EC__ANONSTOREY1D_T905098148_H
#define U3CDUMPU3EC__ANONSTOREY1D_T905098148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WsFrame/<dump>c__AnonStorey1C/<dump>c__AnonStorey1D
struct  U3CdumpU3Ec__AnonStorey1D_t905098148  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.WsFrame/<dump>c__AnonStorey1C/<dump>c__AnonStorey1D::lineFmt
	String_t* ___lineFmt_0;
	// System.Int64 WebSocketSharp.WsFrame/<dump>c__AnonStorey1C/<dump>c__AnonStorey1D::lineCount
	int64_t ___lineCount_1;
	// WebSocketSharp.WsFrame/<dump>c__AnonStorey1C WebSocketSharp.WsFrame/<dump>c__AnonStorey1C/<dump>c__AnonStorey1D::<>f__ref$28
	U3CdumpU3Ec__AnonStorey1C_t1594701605 * ___U3CU3Ef__refU2428_2;

public:
	inline static int32_t get_offset_of_lineFmt_0() { return static_cast<int32_t>(offsetof(U3CdumpU3Ec__AnonStorey1D_t905098148, ___lineFmt_0)); }
	inline String_t* get_lineFmt_0() const { return ___lineFmt_0; }
	inline String_t** get_address_of_lineFmt_0() { return &___lineFmt_0; }
	inline void set_lineFmt_0(String_t* value)
	{
		___lineFmt_0 = value;
		Il2CppCodeGenWriteBarrier((&___lineFmt_0), value);
	}

	inline static int32_t get_offset_of_lineCount_1() { return static_cast<int32_t>(offsetof(U3CdumpU3Ec__AnonStorey1D_t905098148, ___lineCount_1)); }
	inline int64_t get_lineCount_1() const { return ___lineCount_1; }
	inline int64_t* get_address_of_lineCount_1() { return &___lineCount_1; }
	inline void set_lineCount_1(int64_t value)
	{
		___lineCount_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2428_2() { return static_cast<int32_t>(offsetof(U3CdumpU3Ec__AnonStorey1D_t905098148, ___U3CU3Ef__refU2428_2)); }
	inline U3CdumpU3Ec__AnonStorey1C_t1594701605 * get_U3CU3Ef__refU2428_2() const { return ___U3CU3Ef__refU2428_2; }
	inline U3CdumpU3Ec__AnonStorey1C_t1594701605 ** get_address_of_U3CU3Ef__refU2428_2() { return &___U3CU3Ef__refU2428_2; }
	inline void set_U3CU3Ef__refU2428_2(U3CdumpU3Ec__AnonStorey1C_t1594701605 * value)
	{
		___U3CU3Ef__refU2428_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU2428_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDUMPU3EC__ANONSTOREY1D_T905098148_H
#ifndef U3CDUMPU3EC__ANONSTOREY1C_T1594701605_H
#define U3CDUMPU3EC__ANONSTOREY1C_T1594701605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WsFrame/<dump>c__AnonStorey1C
struct  U3CdumpU3Ec__AnonStorey1C_t1594701605  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.WsFrame/<dump>c__AnonStorey1C::countFmt
	String_t* ___countFmt_0;
	// System.Text.StringBuilder WebSocketSharp.WsFrame/<dump>c__AnonStorey1C::buffer
	StringBuilder_t * ___buffer_1;

public:
	inline static int32_t get_offset_of_countFmt_0() { return static_cast<int32_t>(offsetof(U3CdumpU3Ec__AnonStorey1C_t1594701605, ___countFmt_0)); }
	inline String_t* get_countFmt_0() const { return ___countFmt_0; }
	inline String_t** get_address_of_countFmt_0() { return &___countFmt_0; }
	inline void set_countFmt_0(String_t* value)
	{
		___countFmt_0 = value;
		Il2CppCodeGenWriteBarrier((&___countFmt_0), value);
	}

	inline static int32_t get_offset_of_buffer_1() { return static_cast<int32_t>(offsetof(U3CdumpU3Ec__AnonStorey1C_t1594701605, ___buffer_1)); }
	inline StringBuilder_t * get_buffer_1() const { return ___buffer_1; }
	inline StringBuilder_t ** get_address_of_buffer_1() { return &___buffer_1; }
	inline void set_buffer_1(StringBuilder_t * value)
	{
		___buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDUMPU3EC__ANONSTOREY1C_T1594701605_H
#ifndef U3CGETENUMERATORU3EC__ITERATOR2_T269000295_H
#define U3CGETENUMERATORU3EC__ITERATOR2_T269000295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator2
struct  U3CGetEnumeratorU3Ec__Iterator2_t269000295  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator2::<<$$>>__0
	ByteU5BU5D_t4116647657* ___U3CU3CU24U24U3EU3E__0_0;
	// System.Int32 WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator2::<<$$>>__1
	int32_t ___U3CU3CU24U24U3EU3E__1_1;
	// System.Byte WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator2::<b>__2
	uint8_t ___U3CbU3E__2_2;
	// System.Byte[] WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator2::<<$$>>__3
	ByteU5BU5D_t4116647657* ___U3CU3CU24U24U3EU3E__3_3;
	// System.Int32 WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator2::<<$$>>__4
	int32_t ___U3CU3CU24U24U3EU3E__4_4;
	// System.Byte WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator2::<b>__5
	uint8_t ___U3CbU3E__5_5;
	// System.Int32 WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator2::$PC
	int32_t ___U24PC_6;
	// System.Byte WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator2::$current
	uint8_t ___U24current_7;
	// WebSocketSharp.PayloadData WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator2::<>f__this
	PayloadData_t688932160 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CU3CU24U24U3EU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator2_t269000295, ___U3CU3CU24U24U3EU3E__0_0)); }
	inline ByteU5BU5D_t4116647657* get_U3CU3CU24U24U3EU3E__0_0() const { return ___U3CU3CU24U24U3EU3E__0_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CU3CU24U24U3EU3E__0_0() { return &___U3CU3CU24U24U3EU3E__0_0; }
	inline void set_U3CU3CU24U24U3EU3E__0_0(ByteU5BU5D_t4116647657* value)
	{
		___U3CU3CU24U24U3EU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3CU24U24U3EU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3CU24U24U3EU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator2_t269000295, ___U3CU3CU24U24U3EU3E__1_1)); }
	inline int32_t get_U3CU3CU24U24U3EU3E__1_1() const { return ___U3CU3CU24U24U3EU3E__1_1; }
	inline int32_t* get_address_of_U3CU3CU24U24U3EU3E__1_1() { return &___U3CU3CU24U24U3EU3E__1_1; }
	inline void set_U3CU3CU24U24U3EU3E__1_1(int32_t value)
	{
		___U3CU3CU24U24U3EU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CbU3E__2_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator2_t269000295, ___U3CbU3E__2_2)); }
	inline uint8_t get_U3CbU3E__2_2() const { return ___U3CbU3E__2_2; }
	inline uint8_t* get_address_of_U3CbU3E__2_2() { return &___U3CbU3E__2_2; }
	inline void set_U3CbU3E__2_2(uint8_t value)
	{
		___U3CbU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3CU24U24U3EU3E__3_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator2_t269000295, ___U3CU3CU24U24U3EU3E__3_3)); }
	inline ByteU5BU5D_t4116647657* get_U3CU3CU24U24U3EU3E__3_3() const { return ___U3CU3CU24U24U3EU3E__3_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CU3CU24U24U3EU3E__3_3() { return &___U3CU3CU24U24U3EU3E__3_3; }
	inline void set_U3CU3CU24U24U3EU3E__3_3(ByteU5BU5D_t4116647657* value)
	{
		___U3CU3CU24U24U3EU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3CU24U24U3EU3E__3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3CU24U24U3EU3E__4_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator2_t269000295, ___U3CU3CU24U24U3EU3E__4_4)); }
	inline int32_t get_U3CU3CU24U24U3EU3E__4_4() const { return ___U3CU3CU24U24U3EU3E__4_4; }
	inline int32_t* get_address_of_U3CU3CU24U24U3EU3E__4_4() { return &___U3CU3CU24U24U3EU3E__4_4; }
	inline void set_U3CU3CU24U24U3EU3E__4_4(int32_t value)
	{
		___U3CU3CU24U24U3EU3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U3CbU3E__5_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator2_t269000295, ___U3CbU3E__5_5)); }
	inline uint8_t get_U3CbU3E__5_5() const { return ___U3CbU3E__5_5; }
	inline uint8_t* get_address_of_U3CbU3E__5_5() { return &___U3CbU3E__5_5; }
	inline void set_U3CbU3E__5_5(uint8_t value)
	{
		___U3CbU3E__5_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator2_t269000295, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator2_t269000295, ___U24current_7)); }
	inline uint8_t get_U24current_7() const { return ___U24current_7; }
	inline uint8_t* get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(uint8_t value)
	{
		___U24current_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator2_t269000295, ___U3CU3Ef__this_8)); }
	inline PayloadData_t688932160 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline PayloadData_t688932160 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(PayloadData_t688932160 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3EC__ITERATOR2_T269000295_H
#ifndef U3CGETENUMERATORU3EC__ITERATOR3_T1159708565_H
#define U3CGETENUMERATORU3EC__ITERATOR3_T1159708565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WsFrame/<GetEnumerator>c__Iterator3
struct  U3CGetEnumeratorU3Ec__Iterator3_t1159708565  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.WsFrame/<GetEnumerator>c__Iterator3::<<$$>>__0
	ByteU5BU5D_t4116647657* ___U3CU3CU24U24U3EU3E__0_0;
	// System.Int32 WebSocketSharp.WsFrame/<GetEnumerator>c__Iterator3::<<$$>>__1
	int32_t ___U3CU3CU24U24U3EU3E__1_1;
	// System.Byte WebSocketSharp.WsFrame/<GetEnumerator>c__Iterator3::<b>__2
	uint8_t ___U3CbU3E__2_2;
	// System.Int32 WebSocketSharp.WsFrame/<GetEnumerator>c__Iterator3::$PC
	int32_t ___U24PC_3;
	// System.Byte WebSocketSharp.WsFrame/<GetEnumerator>c__Iterator3::$current
	uint8_t ___U24current_4;
	// WebSocketSharp.WsFrame WebSocketSharp.WsFrame/<GetEnumerator>c__Iterator3::<>f__this
	WsFrame_t4110797288 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CU3CU24U24U3EU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator3_t1159708565, ___U3CU3CU24U24U3EU3E__0_0)); }
	inline ByteU5BU5D_t4116647657* get_U3CU3CU24U24U3EU3E__0_0() const { return ___U3CU3CU24U24U3EU3E__0_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CU3CU24U24U3EU3E__0_0() { return &___U3CU3CU24U24U3EU3E__0_0; }
	inline void set_U3CU3CU24U24U3EU3E__0_0(ByteU5BU5D_t4116647657* value)
	{
		___U3CU3CU24U24U3EU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3CU24U24U3EU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3CU24U24U3EU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator3_t1159708565, ___U3CU3CU24U24U3EU3E__1_1)); }
	inline int32_t get_U3CU3CU24U24U3EU3E__1_1() const { return ___U3CU3CU24U24U3EU3E__1_1; }
	inline int32_t* get_address_of_U3CU3CU24U24U3EU3E__1_1() { return &___U3CU3CU24U24U3EU3E__1_1; }
	inline void set_U3CU3CU24U24U3EU3E__1_1(int32_t value)
	{
		___U3CU3CU24U24U3EU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CbU3E__2_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator3_t1159708565, ___U3CbU3E__2_2)); }
	inline uint8_t get_U3CbU3E__2_2() const { return ___U3CbU3E__2_2; }
	inline uint8_t* get_address_of_U3CbU3E__2_2() { return &___U3CbU3E__2_2; }
	inline void set_U3CbU3E__2_2(uint8_t value)
	{
		___U3CbU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator3_t1159708565, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator3_t1159708565, ___U24current_4)); }
	inline uint8_t get_U24current_4() const { return ___U24current_4; }
	inline uint8_t* get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(uint8_t value)
	{
		___U24current_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator3_t1159708565, ___U3CU3Ef__this_5)); }
	inline WsFrame_t4110797288 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline WsFrame_t4110797288 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(WsFrame_t4110797288 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3EC__ITERATOR3_T1159708565_H
#ifndef U3CPARSEASYNCU3EC__ANONSTOREY1E_T3168773810_H
#define U3CPARSEASYNCU3EC__ANONSTOREY1E_T3168773810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WsFrame/<ParseAsync>c__AnonStorey1E
struct  U3CParseAsyncU3Ec__AnonStorey1E_t3168773810  : public RuntimeObject
{
public:
	// System.IO.Stream WebSocketSharp.WsFrame/<ParseAsync>c__AnonStorey1E::stream
	Stream_t1273022909 * ___stream_0;
	// System.Boolean WebSocketSharp.WsFrame/<ParseAsync>c__AnonStorey1E::unmask
	bool ___unmask_1;
	// System.Action`1<WebSocketSharp.WsFrame> WebSocketSharp.WsFrame/<ParseAsync>c__AnonStorey1E::completed
	Action_1_t4283264883 * ___completed_2;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CParseAsyncU3Ec__AnonStorey1E_t3168773810, ___stream_0)); }
	inline Stream_t1273022909 * get_stream_0() const { return ___stream_0; }
	inline Stream_t1273022909 ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Stream_t1273022909 * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier((&___stream_0), value);
	}

	inline static int32_t get_offset_of_unmask_1() { return static_cast<int32_t>(offsetof(U3CParseAsyncU3Ec__AnonStorey1E_t3168773810, ___unmask_1)); }
	inline bool get_unmask_1() const { return ___unmask_1; }
	inline bool* get_address_of_unmask_1() { return &___unmask_1; }
	inline void set_unmask_1(bool value)
	{
		___unmask_1 = value;
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CParseAsyncU3Ec__AnonStorey1E_t3168773810, ___completed_2)); }
	inline Action_1_t4283264883 * get_completed_2() const { return ___completed_2; }
	inline Action_1_t4283264883 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_1_t4283264883 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___completed_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPARSEASYNCU3EC__ANONSTOREY1E_T3168773810_H
#ifndef COOKIECOLLECTION_T962330244_H
#define COOKIECOLLECTION_T962330244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.CookieCollection
struct  CookieCollection_t962330244  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<WebSocketSharp.Net.Cookie> WebSocketSharp.Net.CookieCollection::_list
	List_1_t1380209731 * ____list_0;
	// System.Object WebSocketSharp.Net.CookieCollection::_sync
	RuntimeObject * ____sync_1;

public:
	inline static int32_t get_offset_of__list_0() { return static_cast<int32_t>(offsetof(CookieCollection_t962330244, ____list_0)); }
	inline List_1_t1380209731 * get__list_0() const { return ____list_0; }
	inline List_1_t1380209731 ** get_address_of__list_0() { return &____list_0; }
	inline void set__list_0(List_1_t1380209731 * value)
	{
		____list_0 = value;
		Il2CppCodeGenWriteBarrier((&____list_0), value);
	}

	inline static int32_t get_offset_of__sync_1() { return static_cast<int32_t>(offsetof(CookieCollection_t962330244, ____sync_1)); }
	inline RuntimeObject * get__sync_1() const { return ____sync_1; }
	inline RuntimeObject ** get_address_of__sync_1() { return &____sync_1; }
	inline void set__sync_1(RuntimeObject * value)
	{
		____sync_1 = value;
		Il2CppCodeGenWriteBarrier((&____sync_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIECOLLECTION_T962330244_H
#ifndef WEBSOCKETCONTEXT_T619421455_H
#define WEBSOCKETCONTEXT_T619421455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebSockets.WebSocketContext
struct  WebSocketContext_t619421455  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETCONTEXT_T619421455_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CGETHEADERINFOU3EC__ANONSTOREY17_T3185675556_H
#define U3CGETHEADERINFOU3EC__ANONSTOREY17_T3185675556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebHeaderCollection/<GetHeaderInfo>c__AnonStorey17
struct  U3CGetHeaderInfoU3Ec__AnonStorey17_t3185675556  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Net.WebHeaderCollection/<GetHeaderInfo>c__AnonStorey17::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CGetHeaderInfoU3Ec__AnonStorey17_t3185675556, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETHEADERINFOU3EC__ANONSTOREY17_T3185675556_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef HTTPUTILITY_T3452211165_H
#define HTTPUTILITY_T3452211165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpUtility
struct  HttpUtility_t3452211165  : public RuntimeObject
{
public:

public:
};

struct HttpUtility_t3452211165_StaticFields
{
public:
	// System.Char[] WebSocketSharp.Net.HttpUtility::_hexChars
	CharU5BU5D_t3528271667* ____hexChars_0;
	// System.Object WebSocketSharp.Net.HttpUtility::_sync
	RuntimeObject * ____sync_1;

public:
	inline static int32_t get_offset_of__hexChars_0() { return static_cast<int32_t>(offsetof(HttpUtility_t3452211165_StaticFields, ____hexChars_0)); }
	inline CharU5BU5D_t3528271667* get__hexChars_0() const { return ____hexChars_0; }
	inline CharU5BU5D_t3528271667** get_address_of__hexChars_0() { return &____hexChars_0; }
	inline void set__hexChars_0(CharU5BU5D_t3528271667* value)
	{
		____hexChars_0 = value;
		Il2CppCodeGenWriteBarrier((&____hexChars_0), value);
	}

	inline static int32_t get_offset_of__sync_1() { return static_cast<int32_t>(offsetof(HttpUtility_t3452211165_StaticFields, ____sync_1)); }
	inline RuntimeObject * get__sync_1() const { return ____sync_1; }
	inline RuntimeObject ** get_address_of__sync_1() { return &____sync_1; }
	inline void set__sync_1(RuntimeObject * value)
	{
		____sync_1 = value;
		Il2CppCodeGenWriteBarrier((&____sync_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPUTILITY_T3452211165_H
#ifndef HTTPVERSION_T1629733732_H
#define HTTPVERSION_T1629733732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpVersion
struct  HttpVersion_t1629733732  : public RuntimeObject
{
public:

public:
};

struct HttpVersion_t1629733732_StaticFields
{
public:
	// System.Version WebSocketSharp.Net.HttpVersion::Version10
	Version_t3456873960 * ___Version10_0;
	// System.Version WebSocketSharp.Net.HttpVersion::Version11
	Version_t3456873960 * ___Version11_1;

public:
	inline static int32_t get_offset_of_Version10_0() { return static_cast<int32_t>(offsetof(HttpVersion_t1629733732_StaticFields, ___Version10_0)); }
	inline Version_t3456873960 * get_Version10_0() const { return ___Version10_0; }
	inline Version_t3456873960 ** get_address_of_Version10_0() { return &___Version10_0; }
	inline void set_Version10_0(Version_t3456873960 * value)
	{
		___Version10_0 = value;
		Il2CppCodeGenWriteBarrier((&___Version10_0), value);
	}

	inline static int32_t get_offset_of_Version11_1() { return static_cast<int32_t>(offsetof(HttpVersion_t1629733732_StaticFields, ___Version11_1)); }
	inline Version_t3456873960 * get_Version11_1() const { return ___Version11_1; }
	inline Version_t3456873960 ** get_address_of_Version11_1() { return &___Version11_1; }
	inline void set_Version11_1(Version_t3456873960 * value)
	{
		___Version11_1 = value;
		Il2CppCodeGenWriteBarrier((&___Version11_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPVERSION_T1629733732_H
#ifndef PAYLOADDATA_T688932160_H
#define PAYLOADDATA_T688932160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.PayloadData
struct  PayloadData_t688932160  : public RuntimeObject
{
public:
	// System.Boolean WebSocketSharp.PayloadData::<IsMasked>k__BackingField
	bool ___U3CIsMaskedU3Ek__BackingField_0;
	// System.Byte[] WebSocketSharp.PayloadData::<ExtensionData>k__BackingField
	ByteU5BU5D_t4116647657* ___U3CExtensionDataU3Ek__BackingField_1;
	// System.Byte[] WebSocketSharp.PayloadData::<ApplicationData>k__BackingField
	ByteU5BU5D_t4116647657* ___U3CApplicationDataU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CIsMaskedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PayloadData_t688932160, ___U3CIsMaskedU3Ek__BackingField_0)); }
	inline bool get_U3CIsMaskedU3Ek__BackingField_0() const { return ___U3CIsMaskedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsMaskedU3Ek__BackingField_0() { return &___U3CIsMaskedU3Ek__BackingField_0; }
	inline void set_U3CIsMaskedU3Ek__BackingField_0(bool value)
	{
		___U3CIsMaskedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CExtensionDataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PayloadData_t688932160, ___U3CExtensionDataU3Ek__BackingField_1)); }
	inline ByteU5BU5D_t4116647657* get_U3CExtensionDataU3Ek__BackingField_1() const { return ___U3CExtensionDataU3Ek__BackingField_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CExtensionDataU3Ek__BackingField_1() { return &___U3CExtensionDataU3Ek__BackingField_1; }
	inline void set_U3CExtensionDataU3Ek__BackingField_1(ByteU5BU5D_t4116647657* value)
	{
		___U3CExtensionDataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtensionDataU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CApplicationDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PayloadData_t688932160, ___U3CApplicationDataU3Ek__BackingField_2)); }
	inline ByteU5BU5D_t4116647657* get_U3CApplicationDataU3Ek__BackingField_2() const { return ___U3CApplicationDataU3Ek__BackingField_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CApplicationDataU3Ek__BackingField_2() { return &___U3CApplicationDataU3Ek__BackingField_2; }
	inline void set_U3CApplicationDataU3Ek__BackingField_2(ByteU5BU5D_t4116647657* value)
	{
		___U3CApplicationDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CApplicationDataU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYLOADDATA_T688932160_H
#ifndef U3CGETOBJECTDATAU3EC__ANONSTOREY19_T3093865635_H
#define U3CGETOBJECTDATAU3EC__ANONSTOREY19_T3093865635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey19
struct  U3CGetObjectDataU3Ec__AnonStorey19_t3093865635  : public RuntimeObject
{
public:
	// System.Runtime.Serialization.SerializationInfo WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey19::serializationInfo
	SerializationInfo_t950877179 * ___serializationInfo_0;
	// System.Int32 WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey19::count
	int32_t ___count_1;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey19::<>f__this
	WebHeaderCollection_t1205255311 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_serializationInfo_0() { return static_cast<int32_t>(offsetof(U3CGetObjectDataU3Ec__AnonStorey19_t3093865635, ___serializationInfo_0)); }
	inline SerializationInfo_t950877179 * get_serializationInfo_0() const { return ___serializationInfo_0; }
	inline SerializationInfo_t950877179 ** get_address_of_serializationInfo_0() { return &___serializationInfo_0; }
	inline void set_serializationInfo_0(SerializationInfo_t950877179 * value)
	{
		___serializationInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___serializationInfo_0), value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(U3CGetObjectDataU3Ec__AnonStorey19_t3093865635, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CGetObjectDataU3Ec__AnonStorey19_t3093865635, ___U3CU3Ef__this_2)); }
	inline WebHeaderCollection_t1205255311 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline WebHeaderCollection_t1205255311 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(WebHeaderCollection_t1205255311 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETOBJECTDATAU3EC__ANONSTOREY19_T3093865635_H
#ifndef U3CTOSTRINGU3EC__ANONSTOREY1A_T546952984_H
#define U3CTOSTRINGU3EC__ANONSTOREY1A_T546952984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey1A
struct  U3CToStringU3Ec__AnonStorey1A_t546952984  : public RuntimeObject
{
public:
	// System.Text.StringBuilder WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey1A::sb
	StringBuilder_t * ___sb_0;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey1A::<>f__this
	WebHeaderCollection_t1205255311 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_sb_0() { return static_cast<int32_t>(offsetof(U3CToStringU3Ec__AnonStorey1A_t546952984, ___sb_0)); }
	inline StringBuilder_t * get_sb_0() const { return ___sb_0; }
	inline StringBuilder_t ** get_address_of_sb_0() { return &___sb_0; }
	inline void set_sb_0(StringBuilder_t * value)
	{
		___sb_0 = value;
		Il2CppCodeGenWriteBarrier((&___sb_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CToStringU3Ec__AnonStorey1A_t546952984, ___U3CU3Ef__this_1)); }
	inline WebHeaderCollection_t1205255311 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline WebHeaderCollection_t1205255311 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(WebHeaderCollection_t1205255311 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTOSTRINGU3EC__ANONSTOREY1A_T546952984_H
#ifndef ARABICFIXER_T565187177_H
#define ARABICFIXER_T565187177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArabicSupport.ArabicFixer
struct  ArabicFixer_t565187177  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARABICFIXER_T565187177_H
#ifndef NETWORKCREDENTIAL_T1094796801_H
#define NETWORKCREDENTIAL_T1094796801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.NetworkCredential
struct  NetworkCredential_t1094796801  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Net.NetworkCredential::_domain
	String_t* ____domain_0;
	// System.String WebSocketSharp.Net.NetworkCredential::_password
	String_t* ____password_1;
	// System.String WebSocketSharp.Net.NetworkCredential::_username
	String_t* ____username_2;

public:
	inline static int32_t get_offset_of__domain_0() { return static_cast<int32_t>(offsetof(NetworkCredential_t1094796801, ____domain_0)); }
	inline String_t* get__domain_0() const { return ____domain_0; }
	inline String_t** get_address_of__domain_0() { return &____domain_0; }
	inline void set__domain_0(String_t* value)
	{
		____domain_0 = value;
		Il2CppCodeGenWriteBarrier((&____domain_0), value);
	}

	inline static int32_t get_offset_of__password_1() { return static_cast<int32_t>(offsetof(NetworkCredential_t1094796801, ____password_1)); }
	inline String_t* get__password_1() const { return ____password_1; }
	inline String_t** get_address_of__password_1() { return &____password_1; }
	inline void set__password_1(String_t* value)
	{
		____password_1 = value;
		Il2CppCodeGenWriteBarrier((&____password_1), value);
	}

	inline static int32_t get_offset_of__username_2() { return static_cast<int32_t>(offsetof(NetworkCredential_t1094796801, ____username_2)); }
	inline String_t* get__username_2() const { return ____username_2; }
	inline String_t** get_address_of__username_2() { return &____username_2; }
	inline void set__username_2(String_t* value)
	{
		____username_2 = value;
		Il2CppCodeGenWriteBarrier((&____username_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCREDENTIAL_T1094796801_H
#ifndef FIREBASEMESSAGINGPINVOKE_T151993683_H
#define FIREBASEMESSAGINGPINVOKE_T151993683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessagingPINVOKE
struct  FirebaseMessagingPINVOKE_t151993683  : public RuntimeObject
{
public:

public:
};

struct FirebaseMessagingPINVOKE_t151993683_StaticFields
{
public:
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper Firebase.Messaging.FirebaseMessagingPINVOKE::swigExceptionHelper
	SWIGExceptionHelper_t2033454307 * ___swigExceptionHelper_0;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper Firebase.Messaging.FirebaseMessagingPINVOKE::swigStringHelper
	SWIGStringHelper_t1399197998 * ___swigStringHelper_1;

public:
	inline static int32_t get_offset_of_swigExceptionHelper_0() { return static_cast<int32_t>(offsetof(FirebaseMessagingPINVOKE_t151993683_StaticFields, ___swigExceptionHelper_0)); }
	inline SWIGExceptionHelper_t2033454307 * get_swigExceptionHelper_0() const { return ___swigExceptionHelper_0; }
	inline SWIGExceptionHelper_t2033454307 ** get_address_of_swigExceptionHelper_0() { return &___swigExceptionHelper_0; }
	inline void set_swigExceptionHelper_0(SWIGExceptionHelper_t2033454307 * value)
	{
		___swigExceptionHelper_0 = value;
		Il2CppCodeGenWriteBarrier((&___swigExceptionHelper_0), value);
	}

	inline static int32_t get_offset_of_swigStringHelper_1() { return static_cast<int32_t>(offsetof(FirebaseMessagingPINVOKE_t151993683_StaticFields, ___swigStringHelper_1)); }
	inline SWIGStringHelper_t1399197998 * get_swigStringHelper_1() const { return ___swigStringHelper_1; }
	inline SWIGStringHelper_t1399197998 ** get_address_of_swigStringHelper_1() { return &___swigStringHelper_1; }
	inline void set_swigStringHelper_1(SWIGStringHelper_t1399197998 * value)
	{
		___swigStringHelper_1 = value;
		Il2CppCodeGenWriteBarrier((&___swigStringHelper_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREBASEMESSAGINGPINVOKE_T151993683_H
#ifndef ARABICMAPPING_T3619416640_H
#define ARABICMAPPING_T3619416640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArabicMapping
struct  ArabicMapping_t3619416640  : public RuntimeObject
{
public:
	// System.Int32 ArabicMapping::from
	int32_t ___from_0;
	// System.Int32 ArabicMapping::to
	int32_t ___to_1;

public:
	inline static int32_t get_offset_of_from_0() { return static_cast<int32_t>(offsetof(ArabicMapping_t3619416640, ___from_0)); }
	inline int32_t get_from_0() const { return ___from_0; }
	inline int32_t* get_address_of_from_0() { return &___from_0; }
	inline void set_from_0(int32_t value)
	{
		___from_0 = value;
	}

	inline static int32_t get_offset_of_to_1() { return static_cast<int32_t>(offsetof(ArabicMapping_t3619416640, ___to_1)); }
	inline int32_t get_to_1() const { return ___to_1; }
	inline int32_t* get_address_of_to_1() { return &___to_1; }
	inline void set_to_1(int32_t value)
	{
		___to_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARABICMAPPING_T3619416640_H
#ifndef ARABICFIXERTOOL_T2520528640_H
#define ARABICFIXERTOOL_T2520528640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArabicFixerTool
struct  ArabicFixerTool_t2520528640  : public RuntimeObject
{
public:

public:
};

struct ArabicFixerTool_t2520528640_StaticFields
{
public:
	// System.Boolean ArabicFixerTool::showTashkeel
	bool ___showTashkeel_0;
	// System.Boolean ArabicFixerTool::useHinduNumbers
	bool ___useHinduNumbers_1;

public:
	inline static int32_t get_offset_of_showTashkeel_0() { return static_cast<int32_t>(offsetof(ArabicFixerTool_t2520528640_StaticFields, ___showTashkeel_0)); }
	inline bool get_showTashkeel_0() const { return ___showTashkeel_0; }
	inline bool* get_address_of_showTashkeel_0() { return &___showTashkeel_0; }
	inline void set_showTashkeel_0(bool value)
	{
		___showTashkeel_0 = value;
	}

	inline static int32_t get_offset_of_useHinduNumbers_1() { return static_cast<int32_t>(offsetof(ArabicFixerTool_t2520528640_StaticFields, ___useHinduNumbers_1)); }
	inline bool get_useHinduNumbers_1() const { return ___useHinduNumbers_1; }
	inline bool* get_address_of_useHinduNumbers_1() { return &___useHinduNumbers_1; }
	inline void set_useHinduNumbers_1(bool value)
	{
		___useHinduNumbers_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARABICFIXERTOOL_T2520528640_H
#ifndef TASHKEELLOCATION_T3651416022_H
#define TASHKEELLOCATION_T3651416022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TashkeelLocation
struct  TashkeelLocation_t3651416022  : public RuntimeObject
{
public:
	// System.Char TashkeelLocation::tashkeel
	Il2CppChar ___tashkeel_0;
	// System.Int32 TashkeelLocation::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_tashkeel_0() { return static_cast<int32_t>(offsetof(TashkeelLocation_t3651416022, ___tashkeel_0)); }
	inline Il2CppChar get_tashkeel_0() const { return ___tashkeel_0; }
	inline Il2CppChar* get_address_of_tashkeel_0() { return &___tashkeel_0; }
	inline void set_tashkeel_0(Il2CppChar value)
	{
		___tashkeel_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(TashkeelLocation_t3651416022, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASHKEELLOCATION_T3651416022_H
#ifndef ARABICTABLE_T2015225755_H
#define ARABICTABLE_T2015225755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArabicTable
struct  ArabicTable_t2015225755  : public RuntimeObject
{
public:

public:
};

struct ArabicTable_t2015225755_StaticFields
{
public:
	// System.Collections.Generic.List`1<ArabicMapping> ArabicTable::mapList
	List_1_t796524086 * ___mapList_0;
	// ArabicTable ArabicTable::arabicMapper
	ArabicTable_t2015225755 * ___arabicMapper_1;

public:
	inline static int32_t get_offset_of_mapList_0() { return static_cast<int32_t>(offsetof(ArabicTable_t2015225755_StaticFields, ___mapList_0)); }
	inline List_1_t796524086 * get_mapList_0() const { return ___mapList_0; }
	inline List_1_t796524086 ** get_address_of_mapList_0() { return &___mapList_0; }
	inline void set_mapList_0(List_1_t796524086 * value)
	{
		___mapList_0 = value;
		Il2CppCodeGenWriteBarrier((&___mapList_0), value);
	}

	inline static int32_t get_offset_of_arabicMapper_1() { return static_cast<int32_t>(offsetof(ArabicTable_t2015225755_StaticFields, ___arabicMapper_1)); }
	inline ArabicTable_t2015225755 * get_arabicMapper_1() const { return ___arabicMapper_1; }
	inline ArabicTable_t2015225755 ** get_address_of_arabicMapper_1() { return &___arabicMapper_1; }
	inline void set_arabicMapper_1(ArabicTable_t2015225755 * value)
	{
		___arabicMapper_1 = value;
		Il2CppCodeGenWriteBarrier((&___arabicMapper_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARABICTABLE_T2015225755_H
#ifndef HANDSHAKEBASE_T1142904083_H
#define HANDSHAKEBASE_T1142904083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.HandshakeBase
struct  HandshakeBase_t1142904083  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.HandshakeBase::_entity
	ByteU5BU5D_t4116647657* ____entity_0;
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.HandshakeBase::_headers
	NameValueCollection_t407452768 * ____headers_1;
	// System.Version WebSocketSharp.HandshakeBase::_version
	Version_t3456873960 * ____version_2;

public:
	inline static int32_t get_offset_of__entity_0() { return static_cast<int32_t>(offsetof(HandshakeBase_t1142904083, ____entity_0)); }
	inline ByteU5BU5D_t4116647657* get__entity_0() const { return ____entity_0; }
	inline ByteU5BU5D_t4116647657** get_address_of__entity_0() { return &____entity_0; }
	inline void set__entity_0(ByteU5BU5D_t4116647657* value)
	{
		____entity_0 = value;
		Il2CppCodeGenWriteBarrier((&____entity_0), value);
	}

	inline static int32_t get_offset_of__headers_1() { return static_cast<int32_t>(offsetof(HandshakeBase_t1142904083, ____headers_1)); }
	inline NameValueCollection_t407452768 * get__headers_1() const { return ____headers_1; }
	inline NameValueCollection_t407452768 ** get_address_of__headers_1() { return &____headers_1; }
	inline void set__headers_1(NameValueCollection_t407452768 * value)
	{
		____headers_1 = value;
		Il2CppCodeGenWriteBarrier((&____headers_1), value);
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(HandshakeBase_t1142904083, ____version_2)); }
	inline Version_t3456873960 * get__version_2() const { return ____version_2; }
	inline Version_t3456873960 ** get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(Version_t3456873960 * value)
	{
		____version_2 = value;
		Il2CppCodeGenWriteBarrier((&____version_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKEBASE_T1142904083_H
#ifndef AUTHENTICATIONCHALLENGE_T3974775170_H
#define AUTHENTICATIONCHALLENGE_T3974775170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.AuthenticationChallenge
struct  AuthenticationChallenge_t3974775170  : public RuntimeObject
{
public:
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.AuthenticationChallenge::_params
	NameValueCollection_t407452768 * ____params_0;
	// System.String WebSocketSharp.AuthenticationChallenge::_scheme
	String_t* ____scheme_1;

public:
	inline static int32_t get_offset_of__params_0() { return static_cast<int32_t>(offsetof(AuthenticationChallenge_t3974775170, ____params_0)); }
	inline NameValueCollection_t407452768 * get__params_0() const { return ____params_0; }
	inline NameValueCollection_t407452768 ** get_address_of__params_0() { return &____params_0; }
	inline void set__params_0(NameValueCollection_t407452768 * value)
	{
		____params_0 = value;
		Il2CppCodeGenWriteBarrier((&____params_0), value);
	}

	inline static int32_t get_offset_of__scheme_1() { return static_cast<int32_t>(offsetof(AuthenticationChallenge_t3974775170, ____scheme_1)); }
	inline String_t* get__scheme_1() const { return ____scheme_1; }
	inline String_t** get_address_of__scheme_1() { return &____scheme_1; }
	inline void set__scheme_1(String_t* value)
	{
		____scheme_1 = value;
		Il2CppCodeGenWriteBarrier((&____scheme_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONCHALLENGE_T3974775170_H
#ifndef FIREBASEMESSAGING_T2538764775_H
#define FIREBASEMESSAGING_T2538764775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessaging
struct  FirebaseMessaging_t2538764775  : public RuntimeObject
{
public:

public:
};

struct FirebaseMessaging_t2538764775_StaticFields
{
public:
	// System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs> Firebase.Messaging.FirebaseMessaging::MessageReceivedInternal
	EventHandler_1_t3466559488 * ___MessageReceivedInternal_0;
	// System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs> Firebase.Messaging.FirebaseMessaging::TokenReceivedInternal
	EventHandler_1_t2074869383 * ___TokenReceivedInternal_1;
	// Firebase.Messaging.FirebaseMessaging/Listener Firebase.Messaging.FirebaseMessaging::listener
	Listener_t3000907612 * ___listener_2;

public:
	inline static int32_t get_offset_of_MessageReceivedInternal_0() { return static_cast<int32_t>(offsetof(FirebaseMessaging_t2538764775_StaticFields, ___MessageReceivedInternal_0)); }
	inline EventHandler_1_t3466559488 * get_MessageReceivedInternal_0() const { return ___MessageReceivedInternal_0; }
	inline EventHandler_1_t3466559488 ** get_address_of_MessageReceivedInternal_0() { return &___MessageReceivedInternal_0; }
	inline void set_MessageReceivedInternal_0(EventHandler_1_t3466559488 * value)
	{
		___MessageReceivedInternal_0 = value;
		Il2CppCodeGenWriteBarrier((&___MessageReceivedInternal_0), value);
	}

	inline static int32_t get_offset_of_TokenReceivedInternal_1() { return static_cast<int32_t>(offsetof(FirebaseMessaging_t2538764775_StaticFields, ___TokenReceivedInternal_1)); }
	inline EventHandler_1_t2074869383 * get_TokenReceivedInternal_1() const { return ___TokenReceivedInternal_1; }
	inline EventHandler_1_t2074869383 ** get_address_of_TokenReceivedInternal_1() { return &___TokenReceivedInternal_1; }
	inline void set_TokenReceivedInternal_1(EventHandler_1_t2074869383 * value)
	{
		___TokenReceivedInternal_1 = value;
		Il2CppCodeGenWriteBarrier((&___TokenReceivedInternal_1), value);
	}

	inline static int32_t get_offset_of_listener_2() { return static_cast<int32_t>(offsetof(FirebaseMessaging_t2538764775_StaticFields, ___listener_2)); }
	inline Listener_t3000907612 * get_listener_2() const { return ___listener_2; }
	inline Listener_t3000907612 ** get_address_of_listener_2() { return &___listener_2; }
	inline void set_listener_2(Listener_t3000907612 * value)
	{
		___listener_2 = value;
		Il2CppCodeGenWriteBarrier((&___listener_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREBASEMESSAGING_T2538764775_H
#ifndef LISTENER_T3000907612_H
#define LISTENER_T3000907612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessaging/Listener
struct  Listener_t3000907612  : public RuntimeObject
{
public:
	// Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate Firebase.Messaging.FirebaseMessaging/Listener::messageReceivedDelegate
	MessageReceivedDelegate_t2474724020 * ___messageReceivedDelegate_0;
	// Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate Firebase.Messaging.FirebaseMessaging/Listener::tokenReceivedDelegate
	TokenReceivedDelegate_t1016457320 * ___tokenReceivedDelegate_1;
	// Firebase.FirebaseApp Firebase.Messaging.FirebaseMessaging/Listener::app
	FirebaseApp_t2526288410 * ___app_2;

public:
	inline static int32_t get_offset_of_messageReceivedDelegate_0() { return static_cast<int32_t>(offsetof(Listener_t3000907612, ___messageReceivedDelegate_0)); }
	inline MessageReceivedDelegate_t2474724020 * get_messageReceivedDelegate_0() const { return ___messageReceivedDelegate_0; }
	inline MessageReceivedDelegate_t2474724020 ** get_address_of_messageReceivedDelegate_0() { return &___messageReceivedDelegate_0; }
	inline void set_messageReceivedDelegate_0(MessageReceivedDelegate_t2474724020 * value)
	{
		___messageReceivedDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___messageReceivedDelegate_0), value);
	}

	inline static int32_t get_offset_of_tokenReceivedDelegate_1() { return static_cast<int32_t>(offsetof(Listener_t3000907612, ___tokenReceivedDelegate_1)); }
	inline TokenReceivedDelegate_t1016457320 * get_tokenReceivedDelegate_1() const { return ___tokenReceivedDelegate_1; }
	inline TokenReceivedDelegate_t1016457320 ** get_address_of_tokenReceivedDelegate_1() { return &___tokenReceivedDelegate_1; }
	inline void set_tokenReceivedDelegate_1(TokenReceivedDelegate_t1016457320 * value)
	{
		___tokenReceivedDelegate_1 = value;
		Il2CppCodeGenWriteBarrier((&___tokenReceivedDelegate_1), value);
	}

	inline static int32_t get_offset_of_app_2() { return static_cast<int32_t>(offsetof(Listener_t3000907612, ___app_2)); }
	inline FirebaseApp_t2526288410 * get_app_2() const { return ___app_2; }
	inline FirebaseApp_t2526288410 ** get_address_of_app_2() { return &___app_2; }
	inline void set_app_2(FirebaseApp_t2526288410 * value)
	{
		___app_2 = value;
		Il2CppCodeGenWriteBarrier((&___app_2), value);
	}
};

struct Listener_t3000907612_StaticFields
{
public:
	// Firebase.Messaging.FirebaseMessaging/Listener Firebase.Messaging.FirebaseMessaging/Listener::listener
	Listener_t3000907612 * ___listener_3;

public:
	inline static int32_t get_offset_of_listener_3() { return static_cast<int32_t>(offsetof(Listener_t3000907612_StaticFields, ___listener_3)); }
	inline Listener_t3000907612 * get_listener_3() const { return ___listener_3; }
	inline Listener_t3000907612 ** get_address_of_listener_3() { return &___listener_3; }
	inline void set_listener_3(Listener_t3000907612 * value)
	{
		___listener_3 = value;
		Il2CppCodeGenWriteBarrier((&___listener_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTENER_T3000907612_H
#ifndef AUTHENTICATIONRESPONSE_T4290196037_H
#define AUTHENTICATIONRESPONSE_T4290196037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.AuthenticationResponse
struct  AuthenticationResponse_t4290196037  : public RuntimeObject
{
public:
	// System.UInt32 WebSocketSharp.AuthenticationResponse::_nonceCount
	uint32_t ____nonceCount_0;
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.AuthenticationResponse::_params
	NameValueCollection_t407452768 * ____params_1;
	// System.String WebSocketSharp.AuthenticationResponse::_scheme
	String_t* ____scheme_2;

public:
	inline static int32_t get_offset_of__nonceCount_0() { return static_cast<int32_t>(offsetof(AuthenticationResponse_t4290196037, ____nonceCount_0)); }
	inline uint32_t get__nonceCount_0() const { return ____nonceCount_0; }
	inline uint32_t* get_address_of__nonceCount_0() { return &____nonceCount_0; }
	inline void set__nonceCount_0(uint32_t value)
	{
		____nonceCount_0 = value;
	}

	inline static int32_t get_offset_of__params_1() { return static_cast<int32_t>(offsetof(AuthenticationResponse_t4290196037, ____params_1)); }
	inline NameValueCollection_t407452768 * get__params_1() const { return ____params_1; }
	inline NameValueCollection_t407452768 ** get_address_of__params_1() { return &____params_1; }
	inline void set__params_1(NameValueCollection_t407452768 * value)
	{
		____params_1 = value;
		Il2CppCodeGenWriteBarrier((&____params_1), value);
	}

	inline static int32_t get_offset_of__scheme_2() { return static_cast<int32_t>(offsetof(AuthenticationResponse_t4290196037, ____scheme_2)); }
	inline String_t* get__scheme_2() const { return ____scheme_2; }
	inline String_t** get_address_of__scheme_2() { return &____scheme_2; }
	inline void set__scheme_2(String_t* value)
	{
		____scheme_2 = value;
		Il2CppCodeGenWriteBarrier((&____scheme_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONRESPONSE_T4290196037_H
#ifndef SWIGEXCEPTIONHELPER_T2033454307_H
#define SWIGEXCEPTIONHELPER_T2033454307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper
struct  SWIGExceptionHelper_t2033454307  : public RuntimeObject
{
public:

public:
};

struct SWIGExceptionHelper_t2033454307_StaticFields
{
public:
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::applicationDelegate
	ExceptionDelegate_t1699519678 * ___applicationDelegate_0;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::arithmeticDelegate
	ExceptionDelegate_t1699519678 * ___arithmeticDelegate_1;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::divideByZeroDelegate
	ExceptionDelegate_t1699519678 * ___divideByZeroDelegate_2;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::indexOutOfRangeDelegate
	ExceptionDelegate_t1699519678 * ___indexOutOfRangeDelegate_3;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::invalidCastDelegate
	ExceptionDelegate_t1699519678 * ___invalidCastDelegate_4;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::invalidOperationDelegate
	ExceptionDelegate_t1699519678 * ___invalidOperationDelegate_5;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::ioDelegate
	ExceptionDelegate_t1699519678 * ___ioDelegate_6;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::nullReferenceDelegate
	ExceptionDelegate_t1699519678 * ___nullReferenceDelegate_7;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::outOfMemoryDelegate
	ExceptionDelegate_t1699519678 * ___outOfMemoryDelegate_8;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::overflowDelegate
	ExceptionDelegate_t1699519678 * ___overflowDelegate_9;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::systemDelegate
	ExceptionDelegate_t1699519678 * ___systemDelegate_10;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::argumentDelegate
	ExceptionArgumentDelegate_t1079801895 * ___argumentDelegate_11;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::argumentNullDelegate
	ExceptionArgumentDelegate_t1079801895 * ___argumentNullDelegate_12;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::argumentOutOfRangeDelegate
	ExceptionArgumentDelegate_t1079801895 * ___argumentOutOfRangeDelegate_13;

public:
	inline static int32_t get_offset_of_applicationDelegate_0() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___applicationDelegate_0)); }
	inline ExceptionDelegate_t1699519678 * get_applicationDelegate_0() const { return ___applicationDelegate_0; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_applicationDelegate_0() { return &___applicationDelegate_0; }
	inline void set_applicationDelegate_0(ExceptionDelegate_t1699519678 * value)
	{
		___applicationDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___applicationDelegate_0), value);
	}

	inline static int32_t get_offset_of_arithmeticDelegate_1() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___arithmeticDelegate_1)); }
	inline ExceptionDelegate_t1699519678 * get_arithmeticDelegate_1() const { return ___arithmeticDelegate_1; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_arithmeticDelegate_1() { return &___arithmeticDelegate_1; }
	inline void set_arithmeticDelegate_1(ExceptionDelegate_t1699519678 * value)
	{
		___arithmeticDelegate_1 = value;
		Il2CppCodeGenWriteBarrier((&___arithmeticDelegate_1), value);
	}

	inline static int32_t get_offset_of_divideByZeroDelegate_2() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___divideByZeroDelegate_2)); }
	inline ExceptionDelegate_t1699519678 * get_divideByZeroDelegate_2() const { return ___divideByZeroDelegate_2; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_divideByZeroDelegate_2() { return &___divideByZeroDelegate_2; }
	inline void set_divideByZeroDelegate_2(ExceptionDelegate_t1699519678 * value)
	{
		___divideByZeroDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((&___divideByZeroDelegate_2), value);
	}

	inline static int32_t get_offset_of_indexOutOfRangeDelegate_3() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___indexOutOfRangeDelegate_3)); }
	inline ExceptionDelegate_t1699519678 * get_indexOutOfRangeDelegate_3() const { return ___indexOutOfRangeDelegate_3; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_indexOutOfRangeDelegate_3() { return &___indexOutOfRangeDelegate_3; }
	inline void set_indexOutOfRangeDelegate_3(ExceptionDelegate_t1699519678 * value)
	{
		___indexOutOfRangeDelegate_3 = value;
		Il2CppCodeGenWriteBarrier((&___indexOutOfRangeDelegate_3), value);
	}

	inline static int32_t get_offset_of_invalidCastDelegate_4() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___invalidCastDelegate_4)); }
	inline ExceptionDelegate_t1699519678 * get_invalidCastDelegate_4() const { return ___invalidCastDelegate_4; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_invalidCastDelegate_4() { return &___invalidCastDelegate_4; }
	inline void set_invalidCastDelegate_4(ExceptionDelegate_t1699519678 * value)
	{
		___invalidCastDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((&___invalidCastDelegate_4), value);
	}

	inline static int32_t get_offset_of_invalidOperationDelegate_5() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___invalidOperationDelegate_5)); }
	inline ExceptionDelegate_t1699519678 * get_invalidOperationDelegate_5() const { return ___invalidOperationDelegate_5; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_invalidOperationDelegate_5() { return &___invalidOperationDelegate_5; }
	inline void set_invalidOperationDelegate_5(ExceptionDelegate_t1699519678 * value)
	{
		___invalidOperationDelegate_5 = value;
		Il2CppCodeGenWriteBarrier((&___invalidOperationDelegate_5), value);
	}

	inline static int32_t get_offset_of_ioDelegate_6() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___ioDelegate_6)); }
	inline ExceptionDelegate_t1699519678 * get_ioDelegate_6() const { return ___ioDelegate_6; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_ioDelegate_6() { return &___ioDelegate_6; }
	inline void set_ioDelegate_6(ExceptionDelegate_t1699519678 * value)
	{
		___ioDelegate_6 = value;
		Il2CppCodeGenWriteBarrier((&___ioDelegate_6), value);
	}

	inline static int32_t get_offset_of_nullReferenceDelegate_7() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___nullReferenceDelegate_7)); }
	inline ExceptionDelegate_t1699519678 * get_nullReferenceDelegate_7() const { return ___nullReferenceDelegate_7; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_nullReferenceDelegate_7() { return &___nullReferenceDelegate_7; }
	inline void set_nullReferenceDelegate_7(ExceptionDelegate_t1699519678 * value)
	{
		___nullReferenceDelegate_7 = value;
		Il2CppCodeGenWriteBarrier((&___nullReferenceDelegate_7), value);
	}

	inline static int32_t get_offset_of_outOfMemoryDelegate_8() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___outOfMemoryDelegate_8)); }
	inline ExceptionDelegate_t1699519678 * get_outOfMemoryDelegate_8() const { return ___outOfMemoryDelegate_8; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_outOfMemoryDelegate_8() { return &___outOfMemoryDelegate_8; }
	inline void set_outOfMemoryDelegate_8(ExceptionDelegate_t1699519678 * value)
	{
		___outOfMemoryDelegate_8 = value;
		Il2CppCodeGenWriteBarrier((&___outOfMemoryDelegate_8), value);
	}

	inline static int32_t get_offset_of_overflowDelegate_9() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___overflowDelegate_9)); }
	inline ExceptionDelegate_t1699519678 * get_overflowDelegate_9() const { return ___overflowDelegate_9; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_overflowDelegate_9() { return &___overflowDelegate_9; }
	inline void set_overflowDelegate_9(ExceptionDelegate_t1699519678 * value)
	{
		___overflowDelegate_9 = value;
		Il2CppCodeGenWriteBarrier((&___overflowDelegate_9), value);
	}

	inline static int32_t get_offset_of_systemDelegate_10() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___systemDelegate_10)); }
	inline ExceptionDelegate_t1699519678 * get_systemDelegate_10() const { return ___systemDelegate_10; }
	inline ExceptionDelegate_t1699519678 ** get_address_of_systemDelegate_10() { return &___systemDelegate_10; }
	inline void set_systemDelegate_10(ExceptionDelegate_t1699519678 * value)
	{
		___systemDelegate_10 = value;
		Il2CppCodeGenWriteBarrier((&___systemDelegate_10), value);
	}

	inline static int32_t get_offset_of_argumentDelegate_11() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___argumentDelegate_11)); }
	inline ExceptionArgumentDelegate_t1079801895 * get_argumentDelegate_11() const { return ___argumentDelegate_11; }
	inline ExceptionArgumentDelegate_t1079801895 ** get_address_of_argumentDelegate_11() { return &___argumentDelegate_11; }
	inline void set_argumentDelegate_11(ExceptionArgumentDelegate_t1079801895 * value)
	{
		___argumentDelegate_11 = value;
		Il2CppCodeGenWriteBarrier((&___argumentDelegate_11), value);
	}

	inline static int32_t get_offset_of_argumentNullDelegate_12() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___argumentNullDelegate_12)); }
	inline ExceptionArgumentDelegate_t1079801895 * get_argumentNullDelegate_12() const { return ___argumentNullDelegate_12; }
	inline ExceptionArgumentDelegate_t1079801895 ** get_address_of_argumentNullDelegate_12() { return &___argumentNullDelegate_12; }
	inline void set_argumentNullDelegate_12(ExceptionArgumentDelegate_t1079801895 * value)
	{
		___argumentNullDelegate_12 = value;
		Il2CppCodeGenWriteBarrier((&___argumentNullDelegate_12), value);
	}

	inline static int32_t get_offset_of_argumentOutOfRangeDelegate_13() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t2033454307_StaticFields, ___argumentOutOfRangeDelegate_13)); }
	inline ExceptionArgumentDelegate_t1079801895 * get_argumentOutOfRangeDelegate_13() const { return ___argumentOutOfRangeDelegate_13; }
	inline ExceptionArgumentDelegate_t1079801895 ** get_address_of_argumentOutOfRangeDelegate_13() { return &___argumentOutOfRangeDelegate_13; }
	inline void set_argumentOutOfRangeDelegate_13(ExceptionArgumentDelegate_t1079801895 * value)
	{
		___argumentOutOfRangeDelegate_13 = value;
		Il2CppCodeGenWriteBarrier((&___argumentOutOfRangeDelegate_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGEXCEPTIONHELPER_T2033454307_H
#ifndef SWIGPENDINGEXCEPTION_T717714779_H
#define SWIGPENDINGEXCEPTION_T717714779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGPendingException
struct  SWIGPendingException_t717714779  : public RuntimeObject
{
public:

public:
};

struct SWIGPendingException_t717714779_StaticFields
{
public:
	// System.Int32 Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGPendingException::numExceptionsPending
	int32_t ___numExceptionsPending_1;

public:
	inline static int32_t get_offset_of_numExceptionsPending_1() { return static_cast<int32_t>(offsetof(SWIGPendingException_t717714779_StaticFields, ___numExceptionsPending_1)); }
	inline int32_t get_numExceptionsPending_1() const { return ___numExceptionsPending_1; }
	inline int32_t* get_address_of_numExceptionsPending_1() { return &___numExceptionsPending_1; }
	inline void set_numExceptionsPending_1(int32_t value)
	{
		___numExceptionsPending_1 = value;
	}
};

struct SWIGPendingException_t717714779_ThreadStaticFields
{
public:
	// System.Exception Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGPendingException::pendingException
	Exception_t * ___pendingException_0;

public:
	inline static int32_t get_offset_of_pendingException_0() { return static_cast<int32_t>(offsetof(SWIGPendingException_t717714779_ThreadStaticFields, ___pendingException_0)); }
	inline Exception_t * get_pendingException_0() const { return ___pendingException_0; }
	inline Exception_t ** get_address_of_pendingException_0() { return &___pendingException_0; }
	inline void set_pendingException_0(Exception_t * value)
	{
		___pendingException_0 = value;
		Il2CppCodeGenWriteBarrier((&___pendingException_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGPENDINGEXCEPTION_T717714779_H
#ifndef SWIGSTRINGHELPER_T1399197998_H
#define SWIGSTRINGHELPER_T1399197998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper
struct  SWIGStringHelper_t1399197998  : public RuntimeObject
{
public:

public:
};

struct SWIGStringHelper_t1399197998_StaticFields
{
public:
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper::stringDelegate
	SWIGStringDelegate_t3107596264 * ___stringDelegate_0;

public:
	inline static int32_t get_offset_of_stringDelegate_0() { return static_cast<int32_t>(offsetof(SWIGStringHelper_t1399197998_StaticFields, ___stringDelegate_0)); }
	inline SWIGStringDelegate_t3107596264 * get_stringDelegate_0() const { return ___stringDelegate_0; }
	inline SWIGStringDelegate_t3107596264 ** get_address_of_stringDelegate_0() { return &___stringDelegate_0; }
	inline void set_stringDelegate_0(SWIGStringDelegate_t3107596264 * value)
	{
		___stringDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___stringDelegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGSTRINGHELPER_T1399197998_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef NAMEOBJECTCOLLECTIONBASE_T2091847364_H
#define NAMEOBJECTCOLLECTIONBASE_T2091847364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase
struct  NameObjectCollectionBase_t2091847364  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Collections.Specialized.NameObjectCollectionBase::m_ItemsContainer
	Hashtable_t1853889766 * ___m_ItemsContainer_0;
	// System.Collections.Specialized.NameObjectCollectionBase/_Item System.Collections.Specialized.NameObjectCollectionBase::m_NullKeyItem
	_Item_t2272350267 * ___m_NullKeyItem_1;
	// System.Collections.ArrayList System.Collections.Specialized.NameObjectCollectionBase::m_ItemsArray
	ArrayList_t2718874744 * ___m_ItemsArray_2;
	// System.Collections.IHashCodeProvider System.Collections.Specialized.NameObjectCollectionBase::m_hashprovider
	RuntimeObject* ___m_hashprovider_3;
	// System.Collections.IComparer System.Collections.Specialized.NameObjectCollectionBase::m_comparer
	RuntimeObject* ___m_comparer_4;
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase::m_defCapacity
	int32_t ___m_defCapacity_5;
	// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::m_readonly
	bool ___m_readonly_6;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.NameObjectCollectionBase::infoCopy
	SerializationInfo_t950877179 * ___infoCopy_7;
	// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection System.Collections.Specialized.NameObjectCollectionBase::keyscoll
	KeysCollection_t1318642398 * ___keyscoll_8;
	// System.Collections.IEqualityComparer System.Collections.Specialized.NameObjectCollectionBase::equality_comparer
	RuntimeObject* ___equality_comparer_9;

public:
	inline static int32_t get_offset_of_m_ItemsContainer_0() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_ItemsContainer_0)); }
	inline Hashtable_t1853889766 * get_m_ItemsContainer_0() const { return ___m_ItemsContainer_0; }
	inline Hashtable_t1853889766 ** get_address_of_m_ItemsContainer_0() { return &___m_ItemsContainer_0; }
	inline void set_m_ItemsContainer_0(Hashtable_t1853889766 * value)
	{
		___m_ItemsContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsContainer_0), value);
	}

	inline static int32_t get_offset_of_m_NullKeyItem_1() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_NullKeyItem_1)); }
	inline _Item_t2272350267 * get_m_NullKeyItem_1() const { return ___m_NullKeyItem_1; }
	inline _Item_t2272350267 ** get_address_of_m_NullKeyItem_1() { return &___m_NullKeyItem_1; }
	inline void set_m_NullKeyItem_1(_Item_t2272350267 * value)
	{
		___m_NullKeyItem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_NullKeyItem_1), value);
	}

	inline static int32_t get_offset_of_m_ItemsArray_2() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_ItemsArray_2)); }
	inline ArrayList_t2718874744 * get_m_ItemsArray_2() const { return ___m_ItemsArray_2; }
	inline ArrayList_t2718874744 ** get_address_of_m_ItemsArray_2() { return &___m_ItemsArray_2; }
	inline void set_m_ItemsArray_2(ArrayList_t2718874744 * value)
	{
		___m_ItemsArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsArray_2), value);
	}

	inline static int32_t get_offset_of_m_hashprovider_3() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_hashprovider_3)); }
	inline RuntimeObject* get_m_hashprovider_3() const { return ___m_hashprovider_3; }
	inline RuntimeObject** get_address_of_m_hashprovider_3() { return &___m_hashprovider_3; }
	inline void set_m_hashprovider_3(RuntimeObject* value)
	{
		___m_hashprovider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_hashprovider_3), value);
	}

	inline static int32_t get_offset_of_m_comparer_4() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_comparer_4)); }
	inline RuntimeObject* get_m_comparer_4() const { return ___m_comparer_4; }
	inline RuntimeObject** get_address_of_m_comparer_4() { return &___m_comparer_4; }
	inline void set_m_comparer_4(RuntimeObject* value)
	{
		___m_comparer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_comparer_4), value);
	}

	inline static int32_t get_offset_of_m_defCapacity_5() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_defCapacity_5)); }
	inline int32_t get_m_defCapacity_5() const { return ___m_defCapacity_5; }
	inline int32_t* get_address_of_m_defCapacity_5() { return &___m_defCapacity_5; }
	inline void set_m_defCapacity_5(int32_t value)
	{
		___m_defCapacity_5 = value;
	}

	inline static int32_t get_offset_of_m_readonly_6() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_readonly_6)); }
	inline bool get_m_readonly_6() const { return ___m_readonly_6; }
	inline bool* get_address_of_m_readonly_6() { return &___m_readonly_6; }
	inline void set_m_readonly_6(bool value)
	{
		___m_readonly_6 = value;
	}

	inline static int32_t get_offset_of_infoCopy_7() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___infoCopy_7)); }
	inline SerializationInfo_t950877179 * get_infoCopy_7() const { return ___infoCopy_7; }
	inline SerializationInfo_t950877179 ** get_address_of_infoCopy_7() { return &___infoCopy_7; }
	inline void set_infoCopy_7(SerializationInfo_t950877179 * value)
	{
		___infoCopy_7 = value;
		Il2CppCodeGenWriteBarrier((&___infoCopy_7), value);
	}

	inline static int32_t get_offset_of_keyscoll_8() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___keyscoll_8)); }
	inline KeysCollection_t1318642398 * get_keyscoll_8() const { return ___keyscoll_8; }
	inline KeysCollection_t1318642398 ** get_address_of_keyscoll_8() { return &___keyscoll_8; }
	inline void set_keyscoll_8(KeysCollection_t1318642398 * value)
	{
		___keyscoll_8 = value;
		Il2CppCodeGenWriteBarrier((&___keyscoll_8), value);
	}

	inline static int32_t get_offset_of_equality_comparer_9() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___equality_comparer_9)); }
	inline RuntimeObject* get_equality_comparer_9() const { return ___equality_comparer_9; }
	inline RuntimeObject** get_address_of_equality_comparer_9() { return &___equality_comparer_9; }
	inline void set_equality_comparer_9(RuntimeObject* value)
	{
		___equality_comparer_9 = value;
		Il2CppCodeGenWriteBarrier((&___equality_comparer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEOBJECTCOLLECTIONBASE_T2091847364_H
#ifndef REFLECTIONMETHODSCACHE_T2103211062_H
#define REFLECTIONMETHODSCACHE_T2103211062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_t2103211062  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t701940803 * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t1884415901 * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_t768590915 * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t3913627115 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_t2311174851 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_t3841783507 * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast3D_0)); }
	inline Raycast3DCallback_t701940803 * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t701940803 ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t701940803 * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3D_0), value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t1884415901 * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t1884415901 ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t1884415901 * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3DAll_1), value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast2D_2)); }
	inline Raycast2DCallback_t768590915 * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_t768590915 ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_t768590915 * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycast2D_2), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t3913627115 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t3913627115 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t3913627115 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAll_3), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_t2311174851 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_t2311174851 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_t2311174851 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAllNonAlloc_4), value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_t3841783507 * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_t3841783507 ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_t3841783507 * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((&___getRaycastNonAlloc_5), value);
	}
};

struct ReflectionMethodsCache_t2103211062_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_t2103211062 * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_t2103211062 * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_t2103211062 ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_t2103211062 * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReflectionMethodsCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMETHODSCACHE_T2103211062_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public RuntimeObject
{
public:

public:
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_0)); }
	inline Stream_t1273022909 * get_Null_0() const { return ___Null_0; }
	inline Stream_t1273022909 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t1273022909 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef U3CREADBYTESASYNCU3EC__ANONSTOREY9_T1780370735_H
#define U3CREADBYTESASYNCU3EC__ANONSTOREY9_T1780370735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey9
struct  U3CReadBytesAsyncU3Ec__AnonStorey9_t1780370735  : public RuntimeObject
{
public:
	// System.IO.Stream WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey9::stream
	Stream_t1273022909 * ___stream_0;
	// System.Int32 WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey9::length
	int32_t ___length_1;
	// System.Byte[] WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey9::buffer
	ByteU5BU5D_t4116647657* ___buffer_2;
	// System.Action`1<System.Byte[]> WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey9::completed
	Action_1_t4289115252 * ___completed_3;
	// System.Action`1<System.Exception> WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey9::error
	Action_1_t1609204844 * ___error_4;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey9_t1780370735, ___stream_0)); }
	inline Stream_t1273022909 * get_stream_0() const { return ___stream_0; }
	inline Stream_t1273022909 ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Stream_t1273022909 * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier((&___stream_0), value);
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey9_t1780370735, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_buffer_2() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey9_t1780370735, ___buffer_2)); }
	inline ByteU5BU5D_t4116647657* get_buffer_2() const { return ___buffer_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_2() { return &___buffer_2; }
	inline void set_buffer_2(ByteU5BU5D_t4116647657* value)
	{
		___buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_2), value);
	}

	inline static int32_t get_offset_of_completed_3() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey9_t1780370735, ___completed_3)); }
	inline Action_1_t4289115252 * get_completed_3() const { return ___completed_3; }
	inline Action_1_t4289115252 ** get_address_of_completed_3() { return &___completed_3; }
	inline void set_completed_3(Action_1_t4289115252 * value)
	{
		___completed_3 = value;
		Il2CppCodeGenWriteBarrier((&___completed_3), value);
	}

	inline static int32_t get_offset_of_error_4() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey9_t1780370735, ___error_4)); }
	inline Action_1_t1609204844 * get_error_4() const { return ___error_4; }
	inline Action_1_t1609204844 ** get_address_of_error_4() { return &___error_4; }
	inline void set_error_4(Action_1_t1609204844 * value)
	{
		___error_4 = value;
		Il2CppCodeGenWriteBarrier((&___error_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADBYTESASYNCU3EC__ANONSTOREY9_T1780370735_H
#ifndef U3CCONTAINSTWICEU3EC__ANONSTOREY8_T2970119324_H
#define U3CCONTAINSTWICEU3EC__ANONSTOREY8_T2970119324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey8
struct  U3CContainsTwiceU3Ec__AnonStorey8_t2970119324  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey8::len
	int32_t ___len_0;
	// System.String[] WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey8::values
	StringU5BU5D_t1281789340* ___values_1;
	// System.Func`2<System.Int32,System.Boolean> WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey8::contains
	Func_2_t1300586518 * ___contains_2;

public:
	inline static int32_t get_offset_of_len_0() { return static_cast<int32_t>(offsetof(U3CContainsTwiceU3Ec__AnonStorey8_t2970119324, ___len_0)); }
	inline int32_t get_len_0() const { return ___len_0; }
	inline int32_t* get_address_of_len_0() { return &___len_0; }
	inline void set_len_0(int32_t value)
	{
		___len_0 = value;
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(U3CContainsTwiceU3Ec__AnonStorey8_t2970119324, ___values_1)); }
	inline StringU5BU5D_t1281789340* get_values_1() const { return ___values_1; }
	inline StringU5BU5D_t1281789340** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(StringU5BU5D_t1281789340* value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier((&___values_1), value);
	}

	inline static int32_t get_offset_of_contains_2() { return static_cast<int32_t>(offsetof(U3CContainsTwiceU3Ec__AnonStorey8_t2970119324, ___contains_2)); }
	inline Func_2_t1300586518 * get_contains_2() const { return ___contains_2; }
	inline Func_2_t1300586518 ** get_address_of_contains_2() { return &___contains_2; }
	inline void set_contains_2(Func_2_t1300586518 * value)
	{
		___contains_2 = value;
		Il2CppCodeGenWriteBarrier((&___contains_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONTAINSTWICEU3EC__ANONSTOREY8_T2970119324_H
#ifndef BASEVERTEXEFFECT_T2675891272_H
#define BASEVERTEXEFFECT_T2675891272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t2675891272  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T2675891272_H
#ifndef U3CSPLITHEADERVALUEU3EC__ITERATOR0_T2991988224_H
#define U3CSPLITHEADERVALUEU3EC__ITERATOR0_T2991988224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0
struct  U3CSplitHeaderValueU3Ec__Iterator0_t2991988224  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::value
	String_t* ___value_0;
	// System.Int32 WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<len>__0
	int32_t ___U3ClenU3E__0_1;
	// System.Char[] WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::separator
	CharU5BU5D_t3528271667* ___separator_2;
	// System.String WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<separators>__1
	String_t* ___U3CseparatorsU3E__1_3;
	// System.Text.StringBuilder WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<buffer>__2
	StringBuilder_t * ___U3CbufferU3E__2_4;
	// System.Boolean WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<quoted>__3
	bool ___U3CquotedU3E__3_5;
	// System.Boolean WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<escaped>__4
	bool ___U3CescapedU3E__4_6;
	// System.Int32 WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<i>__5
	int32_t ___U3CiU3E__5_7;
	// System.Char WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<c>__6
	Il2CppChar ___U3CcU3E__6_8;
	// System.Int32 WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::$PC
	int32_t ___U24PC_9;
	// System.String WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::$current
	String_t* ___U24current_10;
	// System.String WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<$>value
	String_t* ___U3CU24U3Evalue_11;
	// System.Char[] WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<$>separator
	CharU5BU5D_t3528271667* ___U3CU24U3Eseparator_12;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_U3ClenU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U3ClenU3E__0_1)); }
	inline int32_t get_U3ClenU3E__0_1() const { return ___U3ClenU3E__0_1; }
	inline int32_t* get_address_of_U3ClenU3E__0_1() { return &___U3ClenU3E__0_1; }
	inline void set_U3ClenU3E__0_1(int32_t value)
	{
		___U3ClenU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_separator_2() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___separator_2)); }
	inline CharU5BU5D_t3528271667* get_separator_2() const { return ___separator_2; }
	inline CharU5BU5D_t3528271667** get_address_of_separator_2() { return &___separator_2; }
	inline void set_separator_2(CharU5BU5D_t3528271667* value)
	{
		___separator_2 = value;
		Il2CppCodeGenWriteBarrier((&___separator_2), value);
	}

	inline static int32_t get_offset_of_U3CseparatorsU3E__1_3() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U3CseparatorsU3E__1_3)); }
	inline String_t* get_U3CseparatorsU3E__1_3() const { return ___U3CseparatorsU3E__1_3; }
	inline String_t** get_address_of_U3CseparatorsU3E__1_3() { return &___U3CseparatorsU3E__1_3; }
	inline void set_U3CseparatorsU3E__1_3(String_t* value)
	{
		___U3CseparatorsU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CseparatorsU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U3CbufferU3E__2_4() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U3CbufferU3E__2_4)); }
	inline StringBuilder_t * get_U3CbufferU3E__2_4() const { return ___U3CbufferU3E__2_4; }
	inline StringBuilder_t ** get_address_of_U3CbufferU3E__2_4() { return &___U3CbufferU3E__2_4; }
	inline void set_U3CbufferU3E__2_4(StringBuilder_t * value)
	{
		___U3CbufferU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbufferU3E__2_4), value);
	}

	inline static int32_t get_offset_of_U3CquotedU3E__3_5() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U3CquotedU3E__3_5)); }
	inline bool get_U3CquotedU3E__3_5() const { return ___U3CquotedU3E__3_5; }
	inline bool* get_address_of_U3CquotedU3E__3_5() { return &___U3CquotedU3E__3_5; }
	inline void set_U3CquotedU3E__3_5(bool value)
	{
		___U3CquotedU3E__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CescapedU3E__4_6() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U3CescapedU3E__4_6)); }
	inline bool get_U3CescapedU3E__4_6() const { return ___U3CescapedU3E__4_6; }
	inline bool* get_address_of_U3CescapedU3E__4_6() { return &___U3CescapedU3E__4_6; }
	inline void set_U3CescapedU3E__4_6(bool value)
	{
		___U3CescapedU3E__4_6 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__5_7() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U3CiU3E__5_7)); }
	inline int32_t get_U3CiU3E__5_7() const { return ___U3CiU3E__5_7; }
	inline int32_t* get_address_of_U3CiU3E__5_7() { return &___U3CiU3E__5_7; }
	inline void set_U3CiU3E__5_7(int32_t value)
	{
		___U3CiU3E__5_7 = value;
	}

	inline static int32_t get_offset_of_U3CcU3E__6_8() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U3CcU3E__6_8)); }
	inline Il2CppChar get_U3CcU3E__6_8() const { return ___U3CcU3E__6_8; }
	inline Il2CppChar* get_address_of_U3CcU3E__6_8() { return &___U3CcU3E__6_8; }
	inline void set_U3CcU3E__6_8(Il2CppChar value)
	{
		___U3CcU3E__6_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U24current_10)); }
	inline String_t* get_U24current_10() const { return ___U24current_10; }
	inline String_t** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(String_t* value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U3CU24U3Evalue_11() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U3CU24U3Evalue_11)); }
	inline String_t* get_U3CU24U3Evalue_11() const { return ___U3CU24U3Evalue_11; }
	inline String_t** get_address_of_U3CU24U3Evalue_11() { return &___U3CU24U3Evalue_11; }
	inline void set_U3CU24U3Evalue_11(String_t* value)
	{
		___U3CU24U3Evalue_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24U3Evalue_11), value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eseparator_12() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U3CU24U3Eseparator_12)); }
	inline CharU5BU5D_t3528271667* get_U3CU24U3Eseparator_12() const { return ___U3CU24U3Eseparator_12; }
	inline CharU5BU5D_t3528271667** get_address_of_U3CU24U3Eseparator_12() { return &___U3CU24U3Eseparator_12; }
	inline void set_U3CU24U3Eseparator_12(CharU5BU5D_t3528271667* value)
	{
		___U3CU24U3Eseparator_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24U3Eseparator_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPLITHEADERVALUEU3EC__ITERATOR0_T2991988224_H
#ifndef LAYOUTUTILITY_T2745813735_H
#define LAYOUTUTILITY_T2745813735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutUtility
struct  LayoutUtility_t2745813735  : public RuntimeObject
{
public:

public:
};

struct LayoutUtility_t2745813735_StaticFields
{
public:
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache0
	Func_2_t235587086 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache1
	Func_2_t235587086 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache2
	Func_2_t235587086 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache3
	Func_2_t235587086 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache4
	Func_2_t235587086 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache5
	Func_2_t235587086 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache6
	Func_2_t235587086 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache7
	Func_2_t235587086 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTUTILITY_T2745813735_H
#ifndef EXT_T2749166018_H
#define EXT_T2749166018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext
struct  Ext_t2749166018  : public RuntimeObject
{
public:

public:
};

struct Ext_t2749166018_StaticFields
{
public:
	// System.Func`2<System.String,System.Boolean> WebSocketSharp.Ext::<>f__am$cache0
	Func_2_t2197129486 * ___U3CU3Ef__amU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(Ext_t2749166018_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t2197129486 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t2197129486 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t2197129486 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXT_T2749166018_H
#ifndef U3CVALIDATESECWEBSOCKETPROTOCOLHEADERU3EC__ANONSTOREYF_T3346908998_H
#define U3CVALIDATESECWEBSOCKETPROTOCOLHEADERU3EC__ANONSTOREYF_T3346908998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<validateSecWebSocketProtocolHeader>c__AnonStoreyF
struct  U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStoreyF_t3346908998  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.WebSocket/<validateSecWebSocketProtocolHeader>c__AnonStoreyF::value
	String_t* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStoreyF_t3346908998, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVALIDATESECWEBSOCKETPROTOCOLHEADERU3EC__ANONSTOREYF_T3346908998_H
#ifndef U3CREADHANDSHAKEHEADERSU3EC__ANONSTOREY14_T4227143414_H
#define U3CREADHANDSHAKEHEADERSU3EC__ANONSTOREY14_T4227143414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WsStream/<readHandshakeHeaders>c__AnonStorey14
struct  U3CreadHandshakeHeadersU3Ec__AnonStorey14_t4227143414  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Byte> WebSocketSharp.WsStream/<readHandshakeHeaders>c__AnonStorey14::buffer
	List_1_t2606371118 * ___buffer_0;
	// System.Int32 WebSocketSharp.WsStream/<readHandshakeHeaders>c__AnonStorey14::count
	int32_t ___count_1;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(U3CreadHandshakeHeadersU3Ec__AnonStorey14_t4227143414, ___buffer_0)); }
	inline List_1_t2606371118 * get_buffer_0() const { return ___buffer_0; }
	inline List_1_t2606371118 ** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(List_1_t2606371118 * value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(U3CreadHandshakeHeadersU3Ec__AnonStorey14_t4227143414, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADHANDSHAKEHEADERSU3EC__ANONSTOREY14_T4227143414_H
#ifndef WSSTREAM_T2500460924_H
#define WSSTREAM_T2500460924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WsStream
struct  WsStream_t2500460924  : public RuntimeObject
{
public:
	// System.Object WebSocketSharp.WsStream::_forWrite
	RuntimeObject * ____forWrite_0;
	// System.IO.Stream WebSocketSharp.WsStream::_innerStream
	Stream_t1273022909 * ____innerStream_1;
	// System.Boolean WebSocketSharp.WsStream::_secure
	bool ____secure_2;

public:
	inline static int32_t get_offset_of__forWrite_0() { return static_cast<int32_t>(offsetof(WsStream_t2500460924, ____forWrite_0)); }
	inline RuntimeObject * get__forWrite_0() const { return ____forWrite_0; }
	inline RuntimeObject ** get_address_of__forWrite_0() { return &____forWrite_0; }
	inline void set__forWrite_0(RuntimeObject * value)
	{
		____forWrite_0 = value;
		Il2CppCodeGenWriteBarrier((&____forWrite_0), value);
	}

	inline static int32_t get_offset_of__innerStream_1() { return static_cast<int32_t>(offsetof(WsStream_t2500460924, ____innerStream_1)); }
	inline Stream_t1273022909 * get__innerStream_1() const { return ____innerStream_1; }
	inline Stream_t1273022909 ** get_address_of__innerStream_1() { return &____innerStream_1; }
	inline void set__innerStream_1(Stream_t1273022909 * value)
	{
		____innerStream_1 = value;
		Il2CppCodeGenWriteBarrier((&____innerStream_1), value);
	}

	inline static int32_t get_offset_of__secure_2() { return static_cast<int32_t>(offsetof(WsStream_t2500460924, ____secure_2)); }
	inline bool get__secure_2() const { return ____secure_2; }
	inline bool* get_address_of__secure_2() { return &____secure_2; }
	inline void set__secure_2(bool value)
	{
		____secure_2 = value;
	}
};

struct WsStream_t2500460924_StaticFields
{
public:
	// System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.WsStream::<>f__am$cache3
	RemoteCertificateValidationCallback_t3014364904 * ___U3CU3Ef__amU24cache3_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(WsStream_t2500460924_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline RemoteCertificateValidationCallback_t3014364904 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline RemoteCertificateValidationCallback_t3014364904 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(RemoteCertificateValidationCallback_t3014364904 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WSSTREAM_T2500460924_H
#ifndef U3CCONNECTASYNCU3EC__ANONSTOREY10_T2977358739_H
#define U3CCONNECTASYNCU3EC__ANONSTOREY10_T2977358739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<ConnectAsync>c__AnonStorey10
struct  U3CConnectAsyncU3Ec__AnonStorey10_t2977358739  : public RuntimeObject
{
public:
	// System.Func`1<System.Boolean> WebSocketSharp.WebSocket/<ConnectAsync>c__AnonStorey10::connector
	Func_1_t3822001908 * ___connector_0;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<ConnectAsync>c__AnonStorey10::<>f__this
	WebSocket_t62038747 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_connector_0() { return static_cast<int32_t>(offsetof(U3CConnectAsyncU3Ec__AnonStorey10_t2977358739, ___connector_0)); }
	inline Func_1_t3822001908 * get_connector_0() const { return ___connector_0; }
	inline Func_1_t3822001908 ** get_address_of_connector_0() { return &___connector_0; }
	inline void set_connector_0(Func_1_t3822001908 * value)
	{
		___connector_0 = value;
		Il2CppCodeGenWriteBarrier((&___connector_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CConnectAsyncU3Ec__AnonStorey10_t2977358739, ___U3CU3Ef__this_1)); }
	inline WebSocket_t62038747 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline WebSocket_t62038747 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(WebSocket_t62038747 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONNECTASYNCU3EC__ANONSTOREY10_T2977358739_H
#ifndef LAYOUTREBUILDER_T541313304_H
#define LAYOUTREBUILDER_T541313304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutRebuilder
struct  LayoutRebuilder_t541313304  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutRebuilder::m_ToRebuild
	RectTransform_t3704657025 * ___m_ToRebuild_0;
	// System.Int32 UnityEngine.UI.LayoutRebuilder::m_CachedHashFromTransform
	int32_t ___m_CachedHashFromTransform_1;

public:
	inline static int32_t get_offset_of_m_ToRebuild_0() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304, ___m_ToRebuild_0)); }
	inline RectTransform_t3704657025 * get_m_ToRebuild_0() const { return ___m_ToRebuild_0; }
	inline RectTransform_t3704657025 ** get_address_of_m_ToRebuild_0() { return &___m_ToRebuild_0; }
	inline void set_m_ToRebuild_0(RectTransform_t3704657025 * value)
	{
		___m_ToRebuild_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ToRebuild_0), value);
	}

	inline static int32_t get_offset_of_m_CachedHashFromTransform_1() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304, ___m_CachedHashFromTransform_1)); }
	inline int32_t get_m_CachedHashFromTransform_1() const { return ___m_CachedHashFromTransform_1; }
	inline int32_t* get_address_of_m_CachedHashFromTransform_1() { return &___m_CachedHashFromTransform_1; }
	inline void set_m_CachedHashFromTransform_1(int32_t value)
	{
		___m_CachedHashFromTransform_1 = value;
	}
};

struct LayoutRebuilder_t541313304_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder> UnityEngine.UI.LayoutRebuilder::s_Rebuilders
	ObjectPool_1_t240936516 * ___s_Rebuilders_2;
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.UI.LayoutRebuilder::<>f__mg$cache0
	ReapplyDrivenProperties_t1258266594 * ___U3CU3Ef__mgU24cache0_3;
	// System.Predicate`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache0
	Predicate_1_t2748928575 * ___U3CU3Ef__amU24cache0_4;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache1
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache1_5;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache2
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache2_6;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache3
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache3_7;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache4
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache4_8;

public:
	inline static int32_t get_offset_of_s_Rebuilders_2() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___s_Rebuilders_2)); }
	inline ObjectPool_1_t240936516 * get_s_Rebuilders_2() const { return ___s_Rebuilders_2; }
	inline ObjectPool_1_t240936516 ** get_address_of_s_Rebuilders_2() { return &___s_Rebuilders_2; }
	inline void set_s_Rebuilders_2(ObjectPool_1_t240936516 * value)
	{
		___s_Rebuilders_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Rebuilders_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline ReapplyDrivenProperties_t1258266594 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(ReapplyDrivenProperties_t1258266594 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t2748928575 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t2748928575 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t2748928575 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTREBUILDER_T541313304_H
#ifndef U3CSTARTRECEIVINGU3EC__ANONSTOREYE_T1317827957_H
#define U3CSTARTRECEIVINGU3EC__ANONSTOREYE_T1317827957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<startReceiving>c__AnonStoreyE
struct  U3CstartReceivingU3Ec__AnonStoreyE_t1317827957  : public RuntimeObject
{
public:
	// System.Action WebSocketSharp.WebSocket/<startReceiving>c__AnonStoreyE::receive
	Action_t1264377477 * ___receive_0;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<startReceiving>c__AnonStoreyE::<>f__this
	WebSocket_t62038747 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_receive_0() { return static_cast<int32_t>(offsetof(U3CstartReceivingU3Ec__AnonStoreyE_t1317827957, ___receive_0)); }
	inline Action_t1264377477 * get_receive_0() const { return ___receive_0; }
	inline Action_t1264377477 ** get_address_of_receive_0() { return &___receive_0; }
	inline void set_receive_0(Action_t1264377477 * value)
	{
		___receive_0 = value;
		Il2CppCodeGenWriteBarrier((&___receive_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CstartReceivingU3Ec__AnonStoreyE_t1317827957, ___U3CU3Ef__this_1)); }
	inline WebSocket_t62038747 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline WebSocket_t62038747 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(WebSocket_t62038747 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTRECEIVINGU3EC__ANONSTOREYE_T1317827957_H
#ifndef U24ARRAYTYPEU3D16_T901447706_H
#define U24ARRAYTYPEU3D16_T901447706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{be551f8d-aed3-403c-a1e7-93fc616e7213}/$ArrayType=16
struct  U24ArrayTypeU3D16_t901447706 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU3D16_t901447706__padding[16];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D16_T901447706_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef AUTHENTICATEDSTREAM_T3415418016_H
#define AUTHENTICATEDSTREAM_T3415418016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.AuthenticatedStream
struct  AuthenticatedStream_t3415418016  : public Stream_t1273022909
{
public:
	// System.IO.Stream System.Net.Security.AuthenticatedStream::innerStream
	Stream_t1273022909 * ___innerStream_1;
	// System.Boolean System.Net.Security.AuthenticatedStream::leaveStreamOpen
	bool ___leaveStreamOpen_2;

public:
	inline static int32_t get_offset_of_innerStream_1() { return static_cast<int32_t>(offsetof(AuthenticatedStream_t3415418016, ___innerStream_1)); }
	inline Stream_t1273022909 * get_innerStream_1() const { return ___innerStream_1; }
	inline Stream_t1273022909 ** get_address_of_innerStream_1() { return &___innerStream_1; }
	inline void set_innerStream_1(Stream_t1273022909 * value)
	{
		___innerStream_1 = value;
		Il2CppCodeGenWriteBarrier((&___innerStream_1), value);
	}

	inline static int32_t get_offset_of_leaveStreamOpen_2() { return static_cast<int32_t>(offsetof(AuthenticatedStream_t3415418016, ___leaveStreamOpen_2)); }
	inline bool get_leaveStreamOpen_2() const { return ___leaveStreamOpen_2; }
	inline bool* get_address_of_leaveStreamOpen_2() { return &___leaveStreamOpen_2; }
	inline void set_leaveStreamOpen_2(bool value)
	{
		___leaveStreamOpen_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATEDSTREAM_T3415418016_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef __STATICARRAYINITTYPESIZEU3D18_T270274279_H
#define __STATICARRAYINITTYPESIZEU3D18_T270274279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{CC42C19A-51E0-4351-874B-4EFF206CE97F}/__StaticArrayInitTypeSize=18
struct  __StaticArrayInitTypeSizeU3D18_t270274279 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D18_t270274279__padding[18];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D18_T270274279_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef HANDSHAKEREQUEST_T3322584187_H
#define HANDSHAKEREQUEST_T3322584187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.HandshakeRequest
struct  HandshakeRequest_t3322584187  : public HandshakeBase_t1142904083
{
public:
	// System.String WebSocketSharp.HandshakeRequest::_method
	String_t* ____method_3;
	// System.String WebSocketSharp.HandshakeRequest::_rawUrl
	String_t* ____rawUrl_4;
	// System.Uri WebSocketSharp.HandshakeRequest::_uri
	Uri_t100236324 * ____uri_5;

public:
	inline static int32_t get_offset_of__method_3() { return static_cast<int32_t>(offsetof(HandshakeRequest_t3322584187, ____method_3)); }
	inline String_t* get__method_3() const { return ____method_3; }
	inline String_t** get_address_of__method_3() { return &____method_3; }
	inline void set__method_3(String_t* value)
	{
		____method_3 = value;
		Il2CppCodeGenWriteBarrier((&____method_3), value);
	}

	inline static int32_t get_offset_of__rawUrl_4() { return static_cast<int32_t>(offsetof(HandshakeRequest_t3322584187, ____rawUrl_4)); }
	inline String_t* get__rawUrl_4() const { return ____rawUrl_4; }
	inline String_t** get_address_of__rawUrl_4() { return &____rawUrl_4; }
	inline void set__rawUrl_4(String_t* value)
	{
		____rawUrl_4 = value;
		Il2CppCodeGenWriteBarrier((&____rawUrl_4), value);
	}

	inline static int32_t get_offset_of__uri_5() { return static_cast<int32_t>(offsetof(HandshakeRequest_t3322584187, ____uri_5)); }
	inline Uri_t100236324 * get__uri_5() const { return ____uri_5; }
	inline Uri_t100236324 ** get_address_of__uri_5() { return &____uri_5; }
	inline void set__uri_5(Uri_t100236324 * value)
	{
		____uri_5 = value;
		Il2CppCodeGenWriteBarrier((&____uri_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKEREQUEST_T3322584187_H
#ifndef HANDSHAKERESPONSE_T1669986296_H
#define HANDSHAKERESPONSE_T1669986296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.HandshakeResponse
struct  HandshakeResponse_t1669986296  : public HandshakeBase_t1142904083
{
public:
	// System.String WebSocketSharp.HandshakeResponse::_code
	String_t* ____code_3;
	// System.String WebSocketSharp.HandshakeResponse::_reason
	String_t* ____reason_4;

public:
	inline static int32_t get_offset_of__code_3() { return static_cast<int32_t>(offsetof(HandshakeResponse_t1669986296, ____code_3)); }
	inline String_t* get__code_3() const { return ____code_3; }
	inline String_t** get_address_of__code_3() { return &____code_3; }
	inline void set__code_3(String_t* value)
	{
		____code_3 = value;
		Il2CppCodeGenWriteBarrier((&____code_3), value);
	}

	inline static int32_t get_offset_of__reason_4() { return static_cast<int32_t>(offsetof(HandshakeResponse_t1669986296, ____reason_4)); }
	inline String_t* get__reason_4() const { return ____reason_4; }
	inline String_t** get_address_of__reason_4() { return &____reason_4; }
	inline void set__reason_4(String_t* value)
	{
		____reason_4 = value;
		Il2CppCodeGenWriteBarrier((&____reason_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKERESPONSE_T1669986296_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef CLOSEEVENTARGS_T1876714021_H
#define CLOSEEVENTARGS_T1876714021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.CloseEventArgs
struct  CloseEventArgs_t1876714021  : public EventArgs_t3591816995
{
public:
	// System.Boolean WebSocketSharp.CloseEventArgs::_clean
	bool ____clean_1;
	// System.UInt16 WebSocketSharp.CloseEventArgs::_code
	uint16_t ____code_2;
	// System.String WebSocketSharp.CloseEventArgs::_reason
	String_t* ____reason_3;

public:
	inline static int32_t get_offset_of__clean_1() { return static_cast<int32_t>(offsetof(CloseEventArgs_t1876714021, ____clean_1)); }
	inline bool get__clean_1() const { return ____clean_1; }
	inline bool* get_address_of__clean_1() { return &____clean_1; }
	inline void set__clean_1(bool value)
	{
		____clean_1 = value;
	}

	inline static int32_t get_offset_of__code_2() { return static_cast<int32_t>(offsetof(CloseEventArgs_t1876714021, ____code_2)); }
	inline uint16_t get__code_2() const { return ____code_2; }
	inline uint16_t* get_address_of__code_2() { return &____code_2; }
	inline void set__code_2(uint16_t value)
	{
		____code_2 = value;
	}

	inline static int32_t get_offset_of__reason_3() { return static_cast<int32_t>(offsetof(CloseEventArgs_t1876714021, ____reason_3)); }
	inline String_t* get__reason_3() const { return ____reason_3; }
	inline String_t** get_address_of__reason_3() { return &____reason_3; }
	inline void set__reason_3(String_t* value)
	{
		____reason_3 = value;
		Il2CppCodeGenWriteBarrier((&____reason_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSEEVENTARGS_T1876714021_H
#ifndef NAMEVALUECOLLECTION_T407452768_H
#define NAMEVALUECOLLECTION_T407452768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameValueCollection
struct  NameValueCollection_t407452768  : public NameObjectCollectionBase_t2091847364
{
public:
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAllKeys
	StringU5BU5D_t1281789340* ___cachedAllKeys_10;
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAll
	StringU5BU5D_t1281789340* ___cachedAll_11;

public:
	inline static int32_t get_offset_of_cachedAllKeys_10() { return static_cast<int32_t>(offsetof(NameValueCollection_t407452768, ___cachedAllKeys_10)); }
	inline StringU5BU5D_t1281789340* get_cachedAllKeys_10() const { return ___cachedAllKeys_10; }
	inline StringU5BU5D_t1281789340** get_address_of_cachedAllKeys_10() { return &___cachedAllKeys_10; }
	inline void set_cachedAllKeys_10(StringU5BU5D_t1281789340* value)
	{
		___cachedAllKeys_10 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAllKeys_10), value);
	}

	inline static int32_t get_offset_of_cachedAll_11() { return static_cast<int32_t>(offsetof(NameValueCollection_t407452768, ___cachedAll_11)); }
	inline StringU5BU5D_t1281789340* get_cachedAll_11() const { return ___cachedAll_11; }
	inline StringU5BU5D_t1281789340** get_address_of_cachedAll_11() { return &___cachedAll_11; }
	inline void set_cachedAll_11(StringU5BU5D_t1281789340* value)
	{
		___cachedAll_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAll_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUECOLLECTION_T407452768_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef ERROREVENTARGS_T3420679011_H
#define ERROREVENTARGS_T3420679011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.ErrorEventArgs
struct  ErrorEventArgs_t3420679011  : public EventArgs_t3591816995
{
public:
	// System.String WebSocketSharp.ErrorEventArgs::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorEventArgs_t3420679011, ___U3CMessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTARGS_T3420679011_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef U24ARRAYTYPEU3D12_T2488454196_H
#define U24ARRAYTYPEU3D12_T2488454196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454196 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454196__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454196_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef FORMATEXCEPTION_T154580423_H
#define FORMATEXCEPTION_T154580423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.FormatException
struct  FormatException_t154580423  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATEXCEPTION_T154580423_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef TEXTANCHOR_T2035777396_H
#define TEXTANCHOR_T2035777396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t2035777396 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_t2035777396, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T2035777396_H
#ifndef SSLSTREAM_T2700741536_H
#define SSLSTREAM_T2700741536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.SslStream
struct  SslStream_t2700741536  : public AuthenticatedStream_t3415418016
{
public:
	// Mono.Security.Protocol.Tls.SslStreamBase System.Net.Security.SslStream::ssl_stream
	SslStreamBase_t1667413407 * ___ssl_stream_3;
	// System.Net.Security.RemoteCertificateValidationCallback System.Net.Security.SslStream::validation_callback
	RemoteCertificateValidationCallback_t3014364904 * ___validation_callback_4;
	// System.Net.Security.LocalCertificateSelectionCallback System.Net.Security.SslStream::selection_callback
	LocalCertificateSelectionCallback_t2354453884 * ___selection_callback_5;

public:
	inline static int32_t get_offset_of_ssl_stream_3() { return static_cast<int32_t>(offsetof(SslStream_t2700741536, ___ssl_stream_3)); }
	inline SslStreamBase_t1667413407 * get_ssl_stream_3() const { return ___ssl_stream_3; }
	inline SslStreamBase_t1667413407 ** get_address_of_ssl_stream_3() { return &___ssl_stream_3; }
	inline void set_ssl_stream_3(SslStreamBase_t1667413407 * value)
	{
		___ssl_stream_3 = value;
		Il2CppCodeGenWriteBarrier((&___ssl_stream_3), value);
	}

	inline static int32_t get_offset_of_validation_callback_4() { return static_cast<int32_t>(offsetof(SslStream_t2700741536, ___validation_callback_4)); }
	inline RemoteCertificateValidationCallback_t3014364904 * get_validation_callback_4() const { return ___validation_callback_4; }
	inline RemoteCertificateValidationCallback_t3014364904 ** get_address_of_validation_callback_4() { return &___validation_callback_4; }
	inline void set_validation_callback_4(RemoteCertificateValidationCallback_t3014364904 * value)
	{
		___validation_callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___validation_callback_4), value);
	}

	inline static int32_t get_offset_of_selection_callback_5() { return static_cast<int32_t>(offsetof(SslStream_t2700741536, ___selection_callback_5)); }
	inline LocalCertificateSelectionCallback_t2354453884 * get_selection_callback_5() const { return ___selection_callback_5; }
	inline LocalCertificateSelectionCallback_t2354453884 ** get_address_of_selection_callback_5() { return &___selection_callback_5; }
	inline void set_selection_callback_5(LocalCertificateSelectionCallback_t2354453884 * value)
	{
		___selection_callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___selection_callback_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLSTREAM_T2700741536_H
#ifndef RAYCASTHIT2D_T2279581989_H
#define RAYCASTHIT2D_T2279581989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t2279581989 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2156229523  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2156229523  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2156229523  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t2806799626 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Centroid_0)); }
	inline Vector2_t2156229523  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2156229523 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2156229523  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Point_1)); }
	inline Vector2_t2156229523  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2156229523 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2156229523  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Normal_2)); }
	inline Vector2_t2156229523  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2156229523 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2156229523  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Collider_5)); }
	inline Collider2D_t2806799626 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t2806799626 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t2806799626 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_pinvoke
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_com
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T2279581989_H
#ifndef MASK_T3471462035_H
#define MASK_T3471462035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Mask
struct  Mask_t3471462035 
{
public:
	// System.Byte WebSocketSharp.Mask::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mask_t3471462035, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASK_T3471462035_H
#ifndef FIN_T411169233_H
#define FIN_T411169233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Fin
struct  Fin_t411169233 
{
public:
	// System.Byte WebSocketSharp.Fin::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Fin_t411169233, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIN_T411169233_H
#ifndef OPCODE_T2755924248_H
#define OPCODE_T2755924248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Opcode
struct  Opcode_t2755924248 
{
public:
	// System.Byte WebSocketSharp.Opcode::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Opcode_t2755924248, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPCODE_T2755924248_H
#ifndef HTTPHEADERTYPE_T2889339440_H
#define HTTPHEADERTYPE_T2889339440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpHeaderType
struct  HttpHeaderType_t2889339440 
{
public:
	// System.Int32 WebSocketSharp.Net.HttpHeaderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpHeaderType_t2889339440, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPHEADERTYPE_T2889339440_H
#ifndef RSV_T2704667083_H
#define RSV_T2704667083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Rsv
struct  Rsv_t2704667083 
{
public:
	// System.Byte WebSocketSharp.Rsv::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Rsv_t2704667083, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSV_T2704667083_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255366  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t2488454196  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t2488454196  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t2488454196 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t2488454196  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifndef VERTEXHELPER_T2453304189_H
#define VERTEXHELPER_T2453304189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t2453304189  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t899420910 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t4072576034 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t3628304265 * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t3628304265 * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t3628304265 * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t3628304265 * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t899420910 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t496136383 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t128053199 * ___m_Indices_8;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Positions_0)); }
	inline List_1_t899420910 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t899420910 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t899420910 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Colors_1)); }
	inline List_1_t4072576034 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t4072576034 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t4072576034 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv0S_2)); }
	inline List_1_t3628304265 * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t3628304265 ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t3628304265 * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv1S_3)); }
	inline List_1_t3628304265 * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t3628304265 ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t3628304265 * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv2S_4)); }
	inline List_1_t3628304265 * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t3628304265 ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t3628304265 * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv3S_5)); }
	inline List_1_t3628304265 * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t3628304265 ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t3628304265 * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Normals_6)); }
	inline List_1_t899420910 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t899420910 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t899420910 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Tangents_7)); }
	inline List_1_t496136383 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t496136383 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t496136383 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Indices_8)); }
	inline List_1_t128053199 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t128053199 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t128053199 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}
};

struct VertexHelper_t2453304189_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_t3319028937  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t3722313464  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t3319028937  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t3319028937 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t3319028937  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t3722313464  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t3722313464 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t3722313464  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T2453304189_H
#ifndef BYTEORDER_T2496067899_H
#define BYTEORDER_T2496067899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.ByteOrder
struct  ByteOrder_t2496067899 
{
public:
	// System.Byte WebSocketSharp.ByteOrder::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ByteOrder_t2496067899, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEORDER_T2496067899_H
#ifndef CLOSESTATUSCODE_T3786097442_H
#define CLOSESTATUSCODE_T3786097442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.CloseStatusCode
struct  CloseStatusCode_t3786097442 
{
public:
	// System.UInt16 WebSocketSharp.CloseStatusCode::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CloseStatusCode_t3786097442, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSESTATUSCODE_T3786097442_H
#ifndef HTTPSTATUSCODE_T4029129360_H
#define HTTPSTATUSCODE_T4029129360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpStatusCode
struct  HttpStatusCode_t4029129360 
{
public:
	// System.Int32 WebSocketSharp.Net.HttpStatusCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpStatusCode_t4029129360, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTATUSCODE_T4029129360_H
#ifndef COMPRESSIONMETHOD_T1062973517_H
#define COMPRESSIONMETHOD_T1062973517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.CompressionMethod
struct  CompressionMethod_t1062973517 
{
public:
	// System.Byte WebSocketSharp.CompressionMethod::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompressionMethod_t1062973517, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONMETHOD_T1062973517_H
#ifndef LOGLEVEL_T2581836550_H
#define LOGLEVEL_T2581836550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.LogLevel
struct  LogLevel_t2581836550 
{
public:
	// System.Int32 WebSocketSharp.LogLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogLevel_t2581836550, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_T2581836550_H
#ifndef WEBSOCKETSTATE_T45461673_H
#define WEBSOCKETSTATE_T45461673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketState
struct  WebSocketState_t45461673 
{
public:
	// System.UInt16 WebSocketSharp.WebSocketState::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WebSocketState_t45461673, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETSTATE_T45461673_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7BBE551F8DU2DAED3U2D403CU2DA1E7U2D93FC616E7213U7D_T3120359854_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7BBE551F8DU2DAED3U2D403CU2DA1E7U2D93FC616E7213U7D_T3120359854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{be551f8d-aed3-403c-a1e7-93fc616e7213}
struct  U3CPrivateImplementationDetailsU3EU7Bbe551f8dU2Daed3U2D403cU2Da1e7U2D93fc616e7213U7D_t3120359854  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7Bbe551f8dU2Daed3U2D403cU2Da1e7U2D93fc616e7213U7D_t3120359854_StaticFields
{
public:
	// <PrivateImplementationDetails>{be551f8d-aed3-403c-a1e7-93fc616e7213}/$ArrayType=16 <PrivateImplementationDetails>{be551f8d-aed3-403c-a1e7-93fc616e7213}::$field-0
	U24ArrayTypeU3D16_t901447706  ___U24fieldU2D0_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7Bbe551f8dU2Daed3U2D403cU2Da1e7U2D93fc616e7213U7D_t3120359854_StaticFields, ___U24fieldU2D0_0)); }
	inline U24ArrayTypeU3D16_t901447706  get_U24fieldU2D0_0() const { return ___U24fieldU2D0_0; }
	inline U24ArrayTypeU3D16_t901447706 * get_address_of_U24fieldU2D0_0() { return &___U24fieldU2D0_0; }
	inline void set_U24fieldU2D0_0(U24ArrayTypeU3D16_t901447706  value)
	{
		___U24fieldU2D0_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7BBE551F8DU2DAED3U2D403CU2DA1E7U2D93FC616E7213U7D_T3120359854_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_T293356573_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_T293356573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{CC42C19A-51E0-4351-874B-4EFF206CE97F}
struct  U3CPrivateImplementationDetailsU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_t293356573  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_t293356573_StaticFields
{
public:
	// <PrivateImplementationDetails>{CC42C19A-51E0-4351-874B-4EFF206CE97F}/__StaticArrayInitTypeSize=18 <PrivateImplementationDetails>{CC42C19A-51E0-4351-874B-4EFF206CE97F}::$$method0x6000009-1
	__StaticArrayInitTypeSizeU3D18_t270274279  ___U24U24method0x6000009U2D1_0;

public:
	inline static int32_t get_offset_of_U24U24method0x6000009U2D1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_t293356573_StaticFields, ___U24U24method0x6000009U2D1_0)); }
	inline __StaticArrayInitTypeSizeU3D18_t270274279  get_U24U24method0x6000009U2D1_0() const { return ___U24U24method0x6000009U2D1_0; }
	inline __StaticArrayInitTypeSizeU3D18_t270274279 * get_address_of_U24U24method0x6000009U2D1_0() { return &___U24U24method0x6000009U2D1_0; }
	inline void set_U24U24method0x6000009U2D1_0(__StaticArrayInitTypeSizeU3D18_t270274279  value)
	{
		___U24U24method0x6000009U2D1_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_T293356573_H
#ifndef WEBHEADERCOLLECTION_T1205255311_H
#define WEBHEADERCOLLECTION_T1205255311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebHeaderCollection
struct  WebHeaderCollection_t1205255311  : public NameValueCollection_t407452768
{
public:
	// System.Boolean WebSocketSharp.Net.WebHeaderCollection::internallyCreated
	bool ___internallyCreated_13;
	// WebSocketSharp.Net.HttpHeaderType WebSocketSharp.Net.WebHeaderCollection::state
	int32_t ___state_14;

public:
	inline static int32_t get_offset_of_internallyCreated_13() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t1205255311, ___internallyCreated_13)); }
	inline bool get_internallyCreated_13() const { return ___internallyCreated_13; }
	inline bool* get_address_of_internallyCreated_13() { return &___internallyCreated_13; }
	inline void set_internallyCreated_13(bool value)
	{
		___internallyCreated_13 = value;
	}

	inline static int32_t get_offset_of_state_14() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t1205255311, ___state_14)); }
	inline int32_t get_state_14() const { return ___state_14; }
	inline int32_t* get_address_of_state_14() { return &___state_14; }
	inline void set_state_14(int32_t value)
	{
		___state_14 = value;
	}
};

struct WebHeaderCollection_t1205255311_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,WebSocketSharp.Net.HttpHeaderInfo> WebSocketSharp.Net.WebHeaderCollection::headers
	Dictionary_2_t1847514588 * ___headers_12;

public:
	inline static int32_t get_offset_of_headers_12() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t1205255311_StaticFields, ___headers_12)); }
	inline Dictionary_2_t1847514588 * get_headers_12() const { return ___headers_12; }
	inline Dictionary_2_t1847514588 ** get_address_of_headers_12() { return &___headers_12; }
	inline void set_headers_12(Dictionary_2_t1847514588 * value)
	{
		___headers_12 = value;
		Il2CppCodeGenWriteBarrier((&___headers_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBHEADERCOLLECTION_T1205255311_H
#ifndef SSLSTREAM_T3289593333_H
#define SSLSTREAM_T3289593333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.Security.SslStream
struct  SslStream_t3289593333  : public SslStream_t2700741536
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLSTREAM_T3289593333_H
#ifndef LOGGER_T4025333586_H
#define LOGGER_T4025333586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Logger
struct  Logger_t4025333586  : public RuntimeObject
{
public:
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Logger::_file
	String_t* ____file_0;
	// WebSocketSharp.LogLevel modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Logger::_level
	int32_t ____level_1;
	// System.Action`2<WebSocketSharp.LogData,System.String> WebSocketSharp.Logger::_output
	Action_2_t488357320 * ____output_2;
	// System.Object WebSocketSharp.Logger::_sync
	RuntimeObject * ____sync_3;

public:
	inline static int32_t get_offset_of__file_0() { return static_cast<int32_t>(offsetof(Logger_t4025333586, ____file_0)); }
	inline String_t* get__file_0() const { return ____file_0; }
	inline String_t** get_address_of__file_0() { return &____file_0; }
	inline void set__file_0(String_t* value)
	{
		____file_0 = value;
		Il2CppCodeGenWriteBarrier((&____file_0), value);
	}

	inline static int32_t get_offset_of__level_1() { return static_cast<int32_t>(offsetof(Logger_t4025333586, ____level_1)); }
	inline int32_t get__level_1() const { return ____level_1; }
	inline int32_t* get_address_of__level_1() { return &____level_1; }
	inline void set__level_1(int32_t value)
	{
		____level_1 = value;
	}

	inline static int32_t get_offset_of__output_2() { return static_cast<int32_t>(offsetof(Logger_t4025333586, ____output_2)); }
	inline Action_2_t488357320 * get__output_2() const { return ____output_2; }
	inline Action_2_t488357320 ** get_address_of__output_2() { return &____output_2; }
	inline void set__output_2(Action_2_t488357320 * value)
	{
		____output_2 = value;
		Il2CppCodeGenWriteBarrier((&____output_2), value);
	}

	inline static int32_t get_offset_of__sync_3() { return static_cast<int32_t>(offsetof(Logger_t4025333586, ____sync_3)); }
	inline RuntimeObject * get__sync_3() const { return ____sync_3; }
	inline RuntimeObject ** get_address_of__sync_3() { return &____sync_3; }
	inline void set__sync_3(RuntimeObject * value)
	{
		____sync_3 = value;
		Il2CppCodeGenWriteBarrier((&____sync_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGER_T4025333586_H
#ifndef MESSAGEEVENTARGS_T2225057723_H
#define MESSAGEEVENTARGS_T2225057723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.MessageEventArgs
struct  MessageEventArgs_t2225057723  : public EventArgs_t3591816995
{
public:
	// System.String WebSocketSharp.MessageEventArgs::_data
	String_t* ____data_1;
	// WebSocketSharp.Opcode WebSocketSharp.MessageEventArgs::_opcode
	uint8_t ____opcode_2;
	// System.Byte[] WebSocketSharp.MessageEventArgs::_rawData
	ByteU5BU5D_t4116647657* ____rawData_3;

public:
	inline static int32_t get_offset_of__data_1() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2225057723, ____data_1)); }
	inline String_t* get__data_1() const { return ____data_1; }
	inline String_t** get_address_of__data_1() { return &____data_1; }
	inline void set__data_1(String_t* value)
	{
		____data_1 = value;
		Il2CppCodeGenWriteBarrier((&____data_1), value);
	}

	inline static int32_t get_offset_of__opcode_2() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2225057723, ____opcode_2)); }
	inline uint8_t get__opcode_2() const { return ____opcode_2; }
	inline uint8_t* get_address_of__opcode_2() { return &____opcode_2; }
	inline void set__opcode_2(uint8_t value)
	{
		____opcode_2 = value;
	}

	inline static int32_t get_offset_of__rawData_3() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2225057723, ____rawData_3)); }
	inline ByteU5BU5D_t4116647657* get__rawData_3() const { return ____rawData_3; }
	inline ByteU5BU5D_t4116647657** get_address_of__rawData_3() { return &____rawData_3; }
	inline void set__rawData_3(ByteU5BU5D_t4116647657* value)
	{
		____rawData_3 = value;
		Il2CppCodeGenWriteBarrier((&____rawData_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEEVENTARGS_T2225057723_H
#ifndef WEBSOCKET_T62038747_H
#define WEBSOCKET_T62038747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket
struct  WebSocket_t62038747  : public RuntimeObject
{
public:
	// WebSocketSharp.AuthenticationChallenge WebSocketSharp.WebSocket::_authChallenge
	AuthenticationChallenge_t3974775170 * ____authChallenge_0;
	// System.String WebSocketSharp.WebSocket::_base64Key
	String_t* ____base64Key_1;
	// System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.WebSocket::_certValidationCallback
	RemoteCertificateValidationCallback_t3014364904 * ____certValidationCallback_2;
	// System.Boolean WebSocketSharp.WebSocket::_client
	bool ____client_3;
	// System.Action WebSocketSharp.WebSocket::_closeContext
	Action_t1264377477 * ____closeContext_4;
	// WebSocketSharp.CompressionMethod WebSocketSharp.WebSocket::_compression
	uint8_t ____compression_5;
	// WebSocketSharp.Net.WebSockets.WebSocketContext WebSocketSharp.WebSocket::_context
	WebSocketContext_t619421455 * ____context_6;
	// WebSocketSharp.Net.CookieCollection WebSocketSharp.WebSocket::_cookies
	CookieCollection_t962330244 * ____cookies_7;
	// WebSocketSharp.Net.NetworkCredential WebSocketSharp.WebSocket::_credentials
	NetworkCredential_t1094796801 * ____credentials_8;
	// System.String WebSocketSharp.WebSocket::_extensions
	String_t* ____extensions_9;
	// System.Threading.AutoResetEvent WebSocketSharp.WebSocket::_exitReceiving
	AutoResetEvent_t1333520283 * ____exitReceiving_10;
	// System.Object WebSocketSharp.WebSocket::_forConn
	RuntimeObject * ____forConn_11;
	// System.Object WebSocketSharp.WebSocket::_forSend
	RuntimeObject * ____forSend_12;
	// System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String> WebSocketSharp.WebSocket::_handshakeRequestChecker
	Func_2_t1203693676 * ____handshakeRequestChecker_13;
	// WebSocketSharp.Logger modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.WebSocket::_logger
	Logger_t4025333586 * ____logger_14;
	// System.UInt32 WebSocketSharp.WebSocket::_nonceCount
	uint32_t ____nonceCount_15;
	// System.String WebSocketSharp.WebSocket::_origin
	String_t* ____origin_16;
	// System.Boolean WebSocketSharp.WebSocket::_preAuth
	bool ____preAuth_17;
	// System.String WebSocketSharp.WebSocket::_protocol
	String_t* ____protocol_18;
	// System.String[] WebSocketSharp.WebSocket::_protocols
	StringU5BU5D_t1281789340* ____protocols_19;
	// WebSocketSharp.WebSocketState modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.WebSocket::_readyState
	uint16_t ____readyState_20;
	// System.Threading.AutoResetEvent WebSocketSharp.WebSocket::_receivePong
	AutoResetEvent_t1333520283 * ____receivePong_21;
	// System.Boolean WebSocketSharp.WebSocket::_secure
	bool ____secure_22;
	// WebSocketSharp.WsStream WebSocketSharp.WebSocket::_stream
	WsStream_t2500460924 * ____stream_23;
	// System.Net.Sockets.TcpClient WebSocketSharp.WebSocket::_tcpClient
	TcpClient_t822906377 * ____tcpClient_24;
	// System.Uri WebSocketSharp.WebSocket::_uri
	Uri_t100236324 * ____uri_25;
	// System.EventHandler`1<WebSocketSharp.CloseEventArgs> WebSocketSharp.WebSocket::OnClose
	EventHandler_1_t4095840750 * ___OnClose_26;
	// System.EventHandler`1<WebSocketSharp.ErrorEventArgs> WebSocketSharp.WebSocket::OnError
	EventHandler_1_t1344838444 * ___OnError_27;
	// System.EventHandler`1<WebSocketSharp.MessageEventArgs> WebSocketSharp.WebSocket::OnMessage
	EventHandler_1_t149217156 * ___OnMessage_28;
	// System.EventHandler WebSocketSharp.WebSocket::OnOpen
	EventHandler_t1348719766 * ___OnOpen_29;

public:
	inline static int32_t get_offset_of__authChallenge_0() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____authChallenge_0)); }
	inline AuthenticationChallenge_t3974775170 * get__authChallenge_0() const { return ____authChallenge_0; }
	inline AuthenticationChallenge_t3974775170 ** get_address_of__authChallenge_0() { return &____authChallenge_0; }
	inline void set__authChallenge_0(AuthenticationChallenge_t3974775170 * value)
	{
		____authChallenge_0 = value;
		Il2CppCodeGenWriteBarrier((&____authChallenge_0), value);
	}

	inline static int32_t get_offset_of__base64Key_1() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____base64Key_1)); }
	inline String_t* get__base64Key_1() const { return ____base64Key_1; }
	inline String_t** get_address_of__base64Key_1() { return &____base64Key_1; }
	inline void set__base64Key_1(String_t* value)
	{
		____base64Key_1 = value;
		Il2CppCodeGenWriteBarrier((&____base64Key_1), value);
	}

	inline static int32_t get_offset_of__certValidationCallback_2() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____certValidationCallback_2)); }
	inline RemoteCertificateValidationCallback_t3014364904 * get__certValidationCallback_2() const { return ____certValidationCallback_2; }
	inline RemoteCertificateValidationCallback_t3014364904 ** get_address_of__certValidationCallback_2() { return &____certValidationCallback_2; }
	inline void set__certValidationCallback_2(RemoteCertificateValidationCallback_t3014364904 * value)
	{
		____certValidationCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&____certValidationCallback_2), value);
	}

	inline static int32_t get_offset_of__client_3() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____client_3)); }
	inline bool get__client_3() const { return ____client_3; }
	inline bool* get_address_of__client_3() { return &____client_3; }
	inline void set__client_3(bool value)
	{
		____client_3 = value;
	}

	inline static int32_t get_offset_of__closeContext_4() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____closeContext_4)); }
	inline Action_t1264377477 * get__closeContext_4() const { return ____closeContext_4; }
	inline Action_t1264377477 ** get_address_of__closeContext_4() { return &____closeContext_4; }
	inline void set__closeContext_4(Action_t1264377477 * value)
	{
		____closeContext_4 = value;
		Il2CppCodeGenWriteBarrier((&____closeContext_4), value);
	}

	inline static int32_t get_offset_of__compression_5() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____compression_5)); }
	inline uint8_t get__compression_5() const { return ____compression_5; }
	inline uint8_t* get_address_of__compression_5() { return &____compression_5; }
	inline void set__compression_5(uint8_t value)
	{
		____compression_5 = value;
	}

	inline static int32_t get_offset_of__context_6() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____context_6)); }
	inline WebSocketContext_t619421455 * get__context_6() const { return ____context_6; }
	inline WebSocketContext_t619421455 ** get_address_of__context_6() { return &____context_6; }
	inline void set__context_6(WebSocketContext_t619421455 * value)
	{
		____context_6 = value;
		Il2CppCodeGenWriteBarrier((&____context_6), value);
	}

	inline static int32_t get_offset_of__cookies_7() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____cookies_7)); }
	inline CookieCollection_t962330244 * get__cookies_7() const { return ____cookies_7; }
	inline CookieCollection_t962330244 ** get_address_of__cookies_7() { return &____cookies_7; }
	inline void set__cookies_7(CookieCollection_t962330244 * value)
	{
		____cookies_7 = value;
		Il2CppCodeGenWriteBarrier((&____cookies_7), value);
	}

	inline static int32_t get_offset_of__credentials_8() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____credentials_8)); }
	inline NetworkCredential_t1094796801 * get__credentials_8() const { return ____credentials_8; }
	inline NetworkCredential_t1094796801 ** get_address_of__credentials_8() { return &____credentials_8; }
	inline void set__credentials_8(NetworkCredential_t1094796801 * value)
	{
		____credentials_8 = value;
		Il2CppCodeGenWriteBarrier((&____credentials_8), value);
	}

	inline static int32_t get_offset_of__extensions_9() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____extensions_9)); }
	inline String_t* get__extensions_9() const { return ____extensions_9; }
	inline String_t** get_address_of__extensions_9() { return &____extensions_9; }
	inline void set__extensions_9(String_t* value)
	{
		____extensions_9 = value;
		Il2CppCodeGenWriteBarrier((&____extensions_9), value);
	}

	inline static int32_t get_offset_of__exitReceiving_10() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____exitReceiving_10)); }
	inline AutoResetEvent_t1333520283 * get__exitReceiving_10() const { return ____exitReceiving_10; }
	inline AutoResetEvent_t1333520283 ** get_address_of__exitReceiving_10() { return &____exitReceiving_10; }
	inline void set__exitReceiving_10(AutoResetEvent_t1333520283 * value)
	{
		____exitReceiving_10 = value;
		Il2CppCodeGenWriteBarrier((&____exitReceiving_10), value);
	}

	inline static int32_t get_offset_of__forConn_11() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____forConn_11)); }
	inline RuntimeObject * get__forConn_11() const { return ____forConn_11; }
	inline RuntimeObject ** get_address_of__forConn_11() { return &____forConn_11; }
	inline void set__forConn_11(RuntimeObject * value)
	{
		____forConn_11 = value;
		Il2CppCodeGenWriteBarrier((&____forConn_11), value);
	}

	inline static int32_t get_offset_of__forSend_12() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____forSend_12)); }
	inline RuntimeObject * get__forSend_12() const { return ____forSend_12; }
	inline RuntimeObject ** get_address_of__forSend_12() { return &____forSend_12; }
	inline void set__forSend_12(RuntimeObject * value)
	{
		____forSend_12 = value;
		Il2CppCodeGenWriteBarrier((&____forSend_12), value);
	}

	inline static int32_t get_offset_of__handshakeRequestChecker_13() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____handshakeRequestChecker_13)); }
	inline Func_2_t1203693676 * get__handshakeRequestChecker_13() const { return ____handshakeRequestChecker_13; }
	inline Func_2_t1203693676 ** get_address_of__handshakeRequestChecker_13() { return &____handshakeRequestChecker_13; }
	inline void set__handshakeRequestChecker_13(Func_2_t1203693676 * value)
	{
		____handshakeRequestChecker_13 = value;
		Il2CppCodeGenWriteBarrier((&____handshakeRequestChecker_13), value);
	}

	inline static int32_t get_offset_of__logger_14() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____logger_14)); }
	inline Logger_t4025333586 * get__logger_14() const { return ____logger_14; }
	inline Logger_t4025333586 ** get_address_of__logger_14() { return &____logger_14; }
	inline void set__logger_14(Logger_t4025333586 * value)
	{
		____logger_14 = value;
		Il2CppCodeGenWriteBarrier((&____logger_14), value);
	}

	inline static int32_t get_offset_of__nonceCount_15() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____nonceCount_15)); }
	inline uint32_t get__nonceCount_15() const { return ____nonceCount_15; }
	inline uint32_t* get_address_of__nonceCount_15() { return &____nonceCount_15; }
	inline void set__nonceCount_15(uint32_t value)
	{
		____nonceCount_15 = value;
	}

	inline static int32_t get_offset_of__origin_16() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____origin_16)); }
	inline String_t* get__origin_16() const { return ____origin_16; }
	inline String_t** get_address_of__origin_16() { return &____origin_16; }
	inline void set__origin_16(String_t* value)
	{
		____origin_16 = value;
		Il2CppCodeGenWriteBarrier((&____origin_16), value);
	}

	inline static int32_t get_offset_of__preAuth_17() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____preAuth_17)); }
	inline bool get__preAuth_17() const { return ____preAuth_17; }
	inline bool* get_address_of__preAuth_17() { return &____preAuth_17; }
	inline void set__preAuth_17(bool value)
	{
		____preAuth_17 = value;
	}

	inline static int32_t get_offset_of__protocol_18() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____protocol_18)); }
	inline String_t* get__protocol_18() const { return ____protocol_18; }
	inline String_t** get_address_of__protocol_18() { return &____protocol_18; }
	inline void set__protocol_18(String_t* value)
	{
		____protocol_18 = value;
		Il2CppCodeGenWriteBarrier((&____protocol_18), value);
	}

	inline static int32_t get_offset_of__protocols_19() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____protocols_19)); }
	inline StringU5BU5D_t1281789340* get__protocols_19() const { return ____protocols_19; }
	inline StringU5BU5D_t1281789340** get_address_of__protocols_19() { return &____protocols_19; }
	inline void set__protocols_19(StringU5BU5D_t1281789340* value)
	{
		____protocols_19 = value;
		Il2CppCodeGenWriteBarrier((&____protocols_19), value);
	}

	inline static int32_t get_offset_of__readyState_20() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____readyState_20)); }
	inline uint16_t get__readyState_20() const { return ____readyState_20; }
	inline uint16_t* get_address_of__readyState_20() { return &____readyState_20; }
	inline void set__readyState_20(uint16_t value)
	{
		____readyState_20 = value;
	}

	inline static int32_t get_offset_of__receivePong_21() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____receivePong_21)); }
	inline AutoResetEvent_t1333520283 * get__receivePong_21() const { return ____receivePong_21; }
	inline AutoResetEvent_t1333520283 ** get_address_of__receivePong_21() { return &____receivePong_21; }
	inline void set__receivePong_21(AutoResetEvent_t1333520283 * value)
	{
		____receivePong_21 = value;
		Il2CppCodeGenWriteBarrier((&____receivePong_21), value);
	}

	inline static int32_t get_offset_of__secure_22() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____secure_22)); }
	inline bool get__secure_22() const { return ____secure_22; }
	inline bool* get_address_of__secure_22() { return &____secure_22; }
	inline void set__secure_22(bool value)
	{
		____secure_22 = value;
	}

	inline static int32_t get_offset_of__stream_23() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____stream_23)); }
	inline WsStream_t2500460924 * get__stream_23() const { return ____stream_23; }
	inline WsStream_t2500460924 ** get_address_of__stream_23() { return &____stream_23; }
	inline void set__stream_23(WsStream_t2500460924 * value)
	{
		____stream_23 = value;
		Il2CppCodeGenWriteBarrier((&____stream_23), value);
	}

	inline static int32_t get_offset_of__tcpClient_24() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____tcpClient_24)); }
	inline TcpClient_t822906377 * get__tcpClient_24() const { return ____tcpClient_24; }
	inline TcpClient_t822906377 ** get_address_of__tcpClient_24() { return &____tcpClient_24; }
	inline void set__tcpClient_24(TcpClient_t822906377 * value)
	{
		____tcpClient_24 = value;
		Il2CppCodeGenWriteBarrier((&____tcpClient_24), value);
	}

	inline static int32_t get_offset_of__uri_25() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____uri_25)); }
	inline Uri_t100236324 * get__uri_25() const { return ____uri_25; }
	inline Uri_t100236324 ** get_address_of__uri_25() { return &____uri_25; }
	inline void set__uri_25(Uri_t100236324 * value)
	{
		____uri_25 = value;
		Il2CppCodeGenWriteBarrier((&____uri_25), value);
	}

	inline static int32_t get_offset_of_OnClose_26() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ___OnClose_26)); }
	inline EventHandler_1_t4095840750 * get_OnClose_26() const { return ___OnClose_26; }
	inline EventHandler_1_t4095840750 ** get_address_of_OnClose_26() { return &___OnClose_26; }
	inline void set_OnClose_26(EventHandler_1_t4095840750 * value)
	{
		___OnClose_26 = value;
		Il2CppCodeGenWriteBarrier((&___OnClose_26), value);
	}

	inline static int32_t get_offset_of_OnError_27() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ___OnError_27)); }
	inline EventHandler_1_t1344838444 * get_OnError_27() const { return ___OnError_27; }
	inline EventHandler_1_t1344838444 ** get_address_of_OnError_27() { return &___OnError_27; }
	inline void set_OnError_27(EventHandler_1_t1344838444 * value)
	{
		___OnError_27 = value;
		Il2CppCodeGenWriteBarrier((&___OnError_27), value);
	}

	inline static int32_t get_offset_of_OnMessage_28() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ___OnMessage_28)); }
	inline EventHandler_1_t149217156 * get_OnMessage_28() const { return ___OnMessage_28; }
	inline EventHandler_1_t149217156 ** get_address_of_OnMessage_28() { return &___OnMessage_28; }
	inline void set_OnMessage_28(EventHandler_1_t149217156 * value)
	{
		___OnMessage_28 = value;
		Il2CppCodeGenWriteBarrier((&___OnMessage_28), value);
	}

	inline static int32_t get_offset_of_OnOpen_29() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ___OnOpen_29)); }
	inline EventHandler_t1348719766 * get_OnOpen_29() const { return ___OnOpen_29; }
	inline EventHandler_t1348719766 ** get_address_of_OnOpen_29() { return &___OnOpen_29; }
	inline void set_OnOpen_29(EventHandler_t1348719766 * value)
	{
		___OnOpen_29 = value;
		Il2CppCodeGenWriteBarrier((&___OnOpen_29), value);
	}
};

struct WebSocket_t62038747_StaticFields
{
public:
	// System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String> WebSocketSharp.WebSocket::<>f__am$cache1E
	Func_2_t1203693676 * ___U3CU3Ef__amU24cache1E_30;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1E_30() { return static_cast<int32_t>(offsetof(WebSocket_t62038747_StaticFields, ___U3CU3Ef__amU24cache1E_30)); }
	inline Func_2_t1203693676 * get_U3CU3Ef__amU24cache1E_30() const { return ___U3CU3Ef__amU24cache1E_30; }
	inline Func_2_t1203693676 ** get_address_of_U3CU3Ef__amU24cache1E_30() { return &___U3CU3Ef__amU24cache1E_30; }
	inline void set_U3CU3Ef__amU24cache1E_30(Func_2_t1203693676 * value)
	{
		___U3CU3Ef__amU24cache1E_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1E_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKET_T62038747_H
#ifndef COOKIEEXCEPTION_T1899604716_H
#define COOKIEEXCEPTION_T1899604716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.CookieException
struct  CookieException_t1899604716  : public FormatException_t154580423
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIEEXCEPTION_T1899604716_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef WEBSOCKETEXCEPTION_T618477455_H
#define WEBSOCKETEXCEPTION_T618477455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketException
struct  WebSocketException_t618477455  : public Exception_t
{
public:
	// WebSocketSharp.CloseStatusCode WebSocketSharp.WebSocketException::<Code>k__BackingField
	uint16_t ___U3CCodeU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCodeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(WebSocketException_t618477455, ___U3CCodeU3Ek__BackingField_11)); }
	inline uint16_t get_U3CCodeU3Ek__BackingField_11() const { return ___U3CCodeU3Ek__BackingField_11; }
	inline uint16_t* get_address_of_U3CCodeU3Ek__BackingField_11() { return &___U3CCodeU3Ek__BackingField_11; }
	inline void set_U3CCodeU3Ek__BackingField_11(uint16_t value)
	{
		___U3CCodeU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETEXCEPTION_T618477455_H
#ifndef WSFRAME_T4110797288_H
#define WSFRAME_T4110797288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WsFrame
struct  WsFrame_t4110797288  : public RuntimeObject
{
public:
	// WebSocketSharp.Fin WebSocketSharp.WsFrame::<Fin>k__BackingField
	uint8_t ___U3CFinU3Ek__BackingField_1;
	// WebSocketSharp.Rsv WebSocketSharp.WsFrame::<Rsv1>k__BackingField
	uint8_t ___U3CRsv1U3Ek__BackingField_2;
	// WebSocketSharp.Rsv WebSocketSharp.WsFrame::<Rsv2>k__BackingField
	uint8_t ___U3CRsv2U3Ek__BackingField_3;
	// WebSocketSharp.Rsv WebSocketSharp.WsFrame::<Rsv3>k__BackingField
	uint8_t ___U3CRsv3U3Ek__BackingField_4;
	// WebSocketSharp.Opcode WebSocketSharp.WsFrame::<Opcode>k__BackingField
	uint8_t ___U3COpcodeU3Ek__BackingField_5;
	// WebSocketSharp.Mask WebSocketSharp.WsFrame::<Mask>k__BackingField
	uint8_t ___U3CMaskU3Ek__BackingField_6;
	// System.Byte WebSocketSharp.WsFrame::<PayloadLen>k__BackingField
	uint8_t ___U3CPayloadLenU3Ek__BackingField_7;
	// System.Byte[] WebSocketSharp.WsFrame::<ExtPayloadLen>k__BackingField
	ByteU5BU5D_t4116647657* ___U3CExtPayloadLenU3Ek__BackingField_8;
	// System.Byte[] WebSocketSharp.WsFrame::<MaskingKey>k__BackingField
	ByteU5BU5D_t4116647657* ___U3CMaskingKeyU3Ek__BackingField_9;
	// WebSocketSharp.PayloadData WebSocketSharp.WsFrame::<PayloadData>k__BackingField
	PayloadData_t688932160 * ___U3CPayloadDataU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CFinU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WsFrame_t4110797288, ___U3CFinU3Ek__BackingField_1)); }
	inline uint8_t get_U3CFinU3Ek__BackingField_1() const { return ___U3CFinU3Ek__BackingField_1; }
	inline uint8_t* get_address_of_U3CFinU3Ek__BackingField_1() { return &___U3CFinU3Ek__BackingField_1; }
	inline void set_U3CFinU3Ek__BackingField_1(uint8_t value)
	{
		___U3CFinU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CRsv1U3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WsFrame_t4110797288, ___U3CRsv1U3Ek__BackingField_2)); }
	inline uint8_t get_U3CRsv1U3Ek__BackingField_2() const { return ___U3CRsv1U3Ek__BackingField_2; }
	inline uint8_t* get_address_of_U3CRsv1U3Ek__BackingField_2() { return &___U3CRsv1U3Ek__BackingField_2; }
	inline void set_U3CRsv1U3Ek__BackingField_2(uint8_t value)
	{
		___U3CRsv1U3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CRsv2U3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WsFrame_t4110797288, ___U3CRsv2U3Ek__BackingField_3)); }
	inline uint8_t get_U3CRsv2U3Ek__BackingField_3() const { return ___U3CRsv2U3Ek__BackingField_3; }
	inline uint8_t* get_address_of_U3CRsv2U3Ek__BackingField_3() { return &___U3CRsv2U3Ek__BackingField_3; }
	inline void set_U3CRsv2U3Ek__BackingField_3(uint8_t value)
	{
		___U3CRsv2U3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CRsv3U3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WsFrame_t4110797288, ___U3CRsv3U3Ek__BackingField_4)); }
	inline uint8_t get_U3CRsv3U3Ek__BackingField_4() const { return ___U3CRsv3U3Ek__BackingField_4; }
	inline uint8_t* get_address_of_U3CRsv3U3Ek__BackingField_4() { return &___U3CRsv3U3Ek__BackingField_4; }
	inline void set_U3CRsv3U3Ek__BackingField_4(uint8_t value)
	{
		___U3CRsv3U3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3COpcodeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(WsFrame_t4110797288, ___U3COpcodeU3Ek__BackingField_5)); }
	inline uint8_t get_U3COpcodeU3Ek__BackingField_5() const { return ___U3COpcodeU3Ek__BackingField_5; }
	inline uint8_t* get_address_of_U3COpcodeU3Ek__BackingField_5() { return &___U3COpcodeU3Ek__BackingField_5; }
	inline void set_U3COpcodeU3Ek__BackingField_5(uint8_t value)
	{
		___U3COpcodeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CMaskU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(WsFrame_t4110797288, ___U3CMaskU3Ek__BackingField_6)); }
	inline uint8_t get_U3CMaskU3Ek__BackingField_6() const { return ___U3CMaskU3Ek__BackingField_6; }
	inline uint8_t* get_address_of_U3CMaskU3Ek__BackingField_6() { return &___U3CMaskU3Ek__BackingField_6; }
	inline void set_U3CMaskU3Ek__BackingField_6(uint8_t value)
	{
		___U3CMaskU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CPayloadLenU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(WsFrame_t4110797288, ___U3CPayloadLenU3Ek__BackingField_7)); }
	inline uint8_t get_U3CPayloadLenU3Ek__BackingField_7() const { return ___U3CPayloadLenU3Ek__BackingField_7; }
	inline uint8_t* get_address_of_U3CPayloadLenU3Ek__BackingField_7() { return &___U3CPayloadLenU3Ek__BackingField_7; }
	inline void set_U3CPayloadLenU3Ek__BackingField_7(uint8_t value)
	{
		___U3CPayloadLenU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CExtPayloadLenU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(WsFrame_t4110797288, ___U3CExtPayloadLenU3Ek__BackingField_8)); }
	inline ByteU5BU5D_t4116647657* get_U3CExtPayloadLenU3Ek__BackingField_8() const { return ___U3CExtPayloadLenU3Ek__BackingField_8; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CExtPayloadLenU3Ek__BackingField_8() { return &___U3CExtPayloadLenU3Ek__BackingField_8; }
	inline void set_U3CExtPayloadLenU3Ek__BackingField_8(ByteU5BU5D_t4116647657* value)
	{
		___U3CExtPayloadLenU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtPayloadLenU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CMaskingKeyU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(WsFrame_t4110797288, ___U3CMaskingKeyU3Ek__BackingField_9)); }
	inline ByteU5BU5D_t4116647657* get_U3CMaskingKeyU3Ek__BackingField_9() const { return ___U3CMaskingKeyU3Ek__BackingField_9; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CMaskingKeyU3Ek__BackingField_9() { return &___U3CMaskingKeyU3Ek__BackingField_9; }
	inline void set_U3CMaskingKeyU3Ek__BackingField_9(ByteU5BU5D_t4116647657* value)
	{
		___U3CMaskingKeyU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaskingKeyU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CPayloadDataU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(WsFrame_t4110797288, ___U3CPayloadDataU3Ek__BackingField_10)); }
	inline PayloadData_t688932160 * get_U3CPayloadDataU3Ek__BackingField_10() const { return ___U3CPayloadDataU3Ek__BackingField_10; }
	inline PayloadData_t688932160 ** get_address_of_U3CPayloadDataU3Ek__BackingField_10() { return &___U3CPayloadDataU3Ek__BackingField_10; }
	inline void set_U3CPayloadDataU3Ek__BackingField_10(PayloadData_t688932160 * value)
	{
		___U3CPayloadDataU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPayloadDataU3Ek__BackingField_10), value);
	}
};

struct WsFrame_t4110797288_StaticFields
{
public:
	// System.Byte[] WebSocketSharp.WsFrame::EmptyUnmaskPingData
	ByteU5BU5D_t4116647657* ___EmptyUnmaskPingData_0;

public:
	inline static int32_t get_offset_of_EmptyUnmaskPingData_0() { return static_cast<int32_t>(offsetof(WsFrame_t4110797288_StaticFields, ___EmptyUnmaskPingData_0)); }
	inline ByteU5BU5D_t4116647657* get_EmptyUnmaskPingData_0() const { return ___EmptyUnmaskPingData_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_EmptyUnmaskPingData_0() { return &___EmptyUnmaskPingData_0; }
	inline void set_EmptyUnmaskPingData_0(ByteU5BU5D_t4116647657* value)
	{
		___EmptyUnmaskPingData_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyUnmaskPingData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WSFRAME_T4110797288_H
#ifndef HTTPHEADERINFO_T2062258289_H
#define HTTPHEADERINFO_T2062258289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpHeaderInfo
struct  HttpHeaderInfo_t2062258289  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Net.HttpHeaderInfo::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// WebSocketSharp.Net.HttpHeaderType WebSocketSharp.Net.HttpHeaderInfo::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HttpHeaderInfo_t2062258289, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HttpHeaderInfo_t2062258289, ___U3CTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_1() const { return ___U3CTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_1() { return &___U3CTypeU3Ek__BackingField_1; }
	inline void set_U3CTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPHEADERINFO_T2062258289_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef RAYCASTALLCALLBACK_T1884415901_H
#define RAYCASTALLCALLBACK_T1884415901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct  RaycastAllCallback_t1884415901  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T1884415901_H
#ifndef GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#define GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_t3841783507  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifndef RAYCAST2DCALLBACK_T768590915_H
#define RAYCAST2DCALLBACK_T768590915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct  Raycast2DCallback_t768590915  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_T768590915_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#define GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t3913627115  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_t2311174851  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifndef RAYCAST3DCALLBACK_T701940803_H
#define RAYCAST3DCALLBACK_T701940803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct  Raycast3DCallback_t701940803  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T701940803_H
#ifndef MESSAGERECEIVEDDELEGATE_T2474724020_H
#define MESSAGERECEIVEDDELEGATE_T2474724020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate
struct  MessageReceivedDelegate_t2474724020  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGERECEIVEDDELEGATE_T2474724020_H
#ifndef SWIGSTRINGDELEGATE_T3107596264_H
#define SWIGSTRINGDELEGATE_T3107596264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct  SWIGStringDelegate_t3107596264  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIGSTRINGDELEGATE_T3107596264_H
#ifndef LOGDATA_T2329603299_H
#define LOGDATA_T2329603299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.LogData
struct  LogData_t2329603299  : public RuntimeObject
{
public:
	// System.Diagnostics.StackFrame WebSocketSharp.LogData::_caller
	StackFrame_t3217253059 * ____caller_0;
	// System.DateTime WebSocketSharp.LogData::_date
	DateTime_t3738529785  ____date_1;
	// WebSocketSharp.LogLevel WebSocketSharp.LogData::_level
	int32_t ____level_2;
	// System.String WebSocketSharp.LogData::_message
	String_t* ____message_3;

public:
	inline static int32_t get_offset_of__caller_0() { return static_cast<int32_t>(offsetof(LogData_t2329603299, ____caller_0)); }
	inline StackFrame_t3217253059 * get__caller_0() const { return ____caller_0; }
	inline StackFrame_t3217253059 ** get_address_of__caller_0() { return &____caller_0; }
	inline void set__caller_0(StackFrame_t3217253059 * value)
	{
		____caller_0 = value;
		Il2CppCodeGenWriteBarrier((&____caller_0), value);
	}

	inline static int32_t get_offset_of__date_1() { return static_cast<int32_t>(offsetof(LogData_t2329603299, ____date_1)); }
	inline DateTime_t3738529785  get__date_1() const { return ____date_1; }
	inline DateTime_t3738529785 * get_address_of__date_1() { return &____date_1; }
	inline void set__date_1(DateTime_t3738529785  value)
	{
		____date_1 = value;
	}

	inline static int32_t get_offset_of__level_2() { return static_cast<int32_t>(offsetof(LogData_t2329603299, ____level_2)); }
	inline int32_t get__level_2() const { return ____level_2; }
	inline int32_t* get_address_of__level_2() { return &____level_2; }
	inline void set__level_2(int32_t value)
	{
		____level_2 = value;
	}

	inline static int32_t get_offset_of__message_3() { return static_cast<int32_t>(offsetof(LogData_t2329603299, ____message_3)); }
	inline String_t* get__message_3() const { return ____message_3; }
	inline String_t** get_address_of__message_3() { return &____message_3; }
	inline void set__message_3(String_t* value)
	{
		____message_3 = value;
		Il2CppCodeGenWriteBarrier((&____message_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGDATA_T2329603299_H
#ifndef EXCEPTIONARGUMENTDELEGATE_T1079801895_H
#define EXCEPTIONARGUMENTDELEGATE_T1079801895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate
struct  ExceptionArgumentDelegate_t1079801895  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONARGUMENTDELEGATE_T1079801895_H
#ifndef EXCEPTIONDELEGATE_T1699519678_H
#define EXCEPTIONDELEGATE_T1699519678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate
struct  ExceptionDelegate_t1699519678  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONDELEGATE_T1699519678_H
#ifndef COOKIE_T4203102285_H
#define COOKIE_T4203102285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.Cookie
struct  Cookie_t4203102285  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Net.Cookie::_comment
	String_t* ____comment_2;
	// System.Uri WebSocketSharp.Net.Cookie::_commentUri
	Uri_t100236324 * ____commentUri_3;
	// System.Boolean WebSocketSharp.Net.Cookie::_discard
	bool ____discard_4;
	// System.String WebSocketSharp.Net.Cookie::_domain
	String_t* ____domain_5;
	// System.DateTime WebSocketSharp.Net.Cookie::_expires
	DateTime_t3738529785  ____expires_6;
	// System.Boolean WebSocketSharp.Net.Cookie::_httpOnly
	bool ____httpOnly_7;
	// System.String WebSocketSharp.Net.Cookie::_name
	String_t* ____name_8;
	// System.String WebSocketSharp.Net.Cookie::_path
	String_t* ____path_9;
	// System.String WebSocketSharp.Net.Cookie::_port
	String_t* ____port_10;
	// System.Int32[] WebSocketSharp.Net.Cookie::_ports
	Int32U5BU5D_t385246372* ____ports_11;
	// System.Boolean WebSocketSharp.Net.Cookie::_secure
	bool ____secure_12;
	// System.DateTime WebSocketSharp.Net.Cookie::_timestamp
	DateTime_t3738529785  ____timestamp_13;
	// System.String WebSocketSharp.Net.Cookie::_value
	String_t* ____value_14;
	// System.Int32 WebSocketSharp.Net.Cookie::_version
	int32_t ____version_15;
	// System.Boolean WebSocketSharp.Net.Cookie::<ExactDomain>k__BackingField
	bool ___U3CExactDomainU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of__comment_2() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____comment_2)); }
	inline String_t* get__comment_2() const { return ____comment_2; }
	inline String_t** get_address_of__comment_2() { return &____comment_2; }
	inline void set__comment_2(String_t* value)
	{
		____comment_2 = value;
		Il2CppCodeGenWriteBarrier((&____comment_2), value);
	}

	inline static int32_t get_offset_of__commentUri_3() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____commentUri_3)); }
	inline Uri_t100236324 * get__commentUri_3() const { return ____commentUri_3; }
	inline Uri_t100236324 ** get_address_of__commentUri_3() { return &____commentUri_3; }
	inline void set__commentUri_3(Uri_t100236324 * value)
	{
		____commentUri_3 = value;
		Il2CppCodeGenWriteBarrier((&____commentUri_3), value);
	}

	inline static int32_t get_offset_of__discard_4() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____discard_4)); }
	inline bool get__discard_4() const { return ____discard_4; }
	inline bool* get_address_of__discard_4() { return &____discard_4; }
	inline void set__discard_4(bool value)
	{
		____discard_4 = value;
	}

	inline static int32_t get_offset_of__domain_5() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____domain_5)); }
	inline String_t* get__domain_5() const { return ____domain_5; }
	inline String_t** get_address_of__domain_5() { return &____domain_5; }
	inline void set__domain_5(String_t* value)
	{
		____domain_5 = value;
		Il2CppCodeGenWriteBarrier((&____domain_5), value);
	}

	inline static int32_t get_offset_of__expires_6() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____expires_6)); }
	inline DateTime_t3738529785  get__expires_6() const { return ____expires_6; }
	inline DateTime_t3738529785 * get_address_of__expires_6() { return &____expires_6; }
	inline void set__expires_6(DateTime_t3738529785  value)
	{
		____expires_6 = value;
	}

	inline static int32_t get_offset_of__httpOnly_7() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____httpOnly_7)); }
	inline bool get__httpOnly_7() const { return ____httpOnly_7; }
	inline bool* get_address_of__httpOnly_7() { return &____httpOnly_7; }
	inline void set__httpOnly_7(bool value)
	{
		____httpOnly_7 = value;
	}

	inline static int32_t get_offset_of__name_8() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____name_8)); }
	inline String_t* get__name_8() const { return ____name_8; }
	inline String_t** get_address_of__name_8() { return &____name_8; }
	inline void set__name_8(String_t* value)
	{
		____name_8 = value;
		Il2CppCodeGenWriteBarrier((&____name_8), value);
	}

	inline static int32_t get_offset_of__path_9() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____path_9)); }
	inline String_t* get__path_9() const { return ____path_9; }
	inline String_t** get_address_of__path_9() { return &____path_9; }
	inline void set__path_9(String_t* value)
	{
		____path_9 = value;
		Il2CppCodeGenWriteBarrier((&____path_9), value);
	}

	inline static int32_t get_offset_of__port_10() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____port_10)); }
	inline String_t* get__port_10() const { return ____port_10; }
	inline String_t** get_address_of__port_10() { return &____port_10; }
	inline void set__port_10(String_t* value)
	{
		____port_10 = value;
		Il2CppCodeGenWriteBarrier((&____port_10), value);
	}

	inline static int32_t get_offset_of__ports_11() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____ports_11)); }
	inline Int32U5BU5D_t385246372* get__ports_11() const { return ____ports_11; }
	inline Int32U5BU5D_t385246372** get_address_of__ports_11() { return &____ports_11; }
	inline void set__ports_11(Int32U5BU5D_t385246372* value)
	{
		____ports_11 = value;
		Il2CppCodeGenWriteBarrier((&____ports_11), value);
	}

	inline static int32_t get_offset_of__secure_12() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____secure_12)); }
	inline bool get__secure_12() const { return ____secure_12; }
	inline bool* get_address_of__secure_12() { return &____secure_12; }
	inline void set__secure_12(bool value)
	{
		____secure_12 = value;
	}

	inline static int32_t get_offset_of__timestamp_13() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____timestamp_13)); }
	inline DateTime_t3738529785  get__timestamp_13() const { return ____timestamp_13; }
	inline DateTime_t3738529785 * get_address_of__timestamp_13() { return &____timestamp_13; }
	inline void set__timestamp_13(DateTime_t3738529785  value)
	{
		____timestamp_13 = value;
	}

	inline static int32_t get_offset_of__value_14() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____value_14)); }
	inline String_t* get__value_14() const { return ____value_14; }
	inline String_t** get_address_of__value_14() { return &____value_14; }
	inline void set__value_14(String_t* value)
	{
		____value_14 = value;
		Il2CppCodeGenWriteBarrier((&____value_14), value);
	}

	inline static int32_t get_offset_of__version_15() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____version_15)); }
	inline int32_t get__version_15() const { return ____version_15; }
	inline int32_t* get_address_of__version_15() { return &____version_15; }
	inline void set__version_15(int32_t value)
	{
		____version_15 = value;
	}

	inline static int32_t get_offset_of_U3CExactDomainU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ___U3CExactDomainU3Ek__BackingField_16)); }
	inline bool get_U3CExactDomainU3Ek__BackingField_16() const { return ___U3CExactDomainU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CExactDomainU3Ek__BackingField_16() { return &___U3CExactDomainU3Ek__BackingField_16; }
	inline void set_U3CExactDomainU3Ek__BackingField_16(bool value)
	{
		___U3CExactDomainU3Ek__BackingField_16 = value;
	}
};

struct Cookie_t4203102285_StaticFields
{
public:
	// System.Char[] WebSocketSharp.Net.Cookie::_reservedCharsForName
	CharU5BU5D_t3528271667* ____reservedCharsForName_0;
	// System.Char[] WebSocketSharp.Net.Cookie::_reservedCharsForValue
	CharU5BU5D_t3528271667* ____reservedCharsForValue_1;

public:
	inline static int32_t get_offset_of__reservedCharsForName_0() { return static_cast<int32_t>(offsetof(Cookie_t4203102285_StaticFields, ____reservedCharsForName_0)); }
	inline CharU5BU5D_t3528271667* get__reservedCharsForName_0() const { return ____reservedCharsForName_0; }
	inline CharU5BU5D_t3528271667** get_address_of__reservedCharsForName_0() { return &____reservedCharsForName_0; }
	inline void set__reservedCharsForName_0(CharU5BU5D_t3528271667* value)
	{
		____reservedCharsForName_0 = value;
		Il2CppCodeGenWriteBarrier((&____reservedCharsForName_0), value);
	}

	inline static int32_t get_offset_of__reservedCharsForValue_1() { return static_cast<int32_t>(offsetof(Cookie_t4203102285_StaticFields, ____reservedCharsForValue_1)); }
	inline CharU5BU5D_t3528271667* get__reservedCharsForValue_1() const { return ____reservedCharsForValue_1; }
	inline CharU5BU5D_t3528271667** get_address_of__reservedCharsForValue_1() { return &____reservedCharsForValue_1; }
	inline void set__reservedCharsForValue_1(CharU5BU5D_t3528271667* value)
	{
		____reservedCharsForValue_1 = value;
		Il2CppCodeGenWriteBarrier((&____reservedCharsForValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIE_T4203102285_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef LAYOUTGROUP_T2436138090_H
#define LAYOUTGROUP_T2436138090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t2436138090  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_t1369453676 * ___m_Padding_2;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_3;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t3704657025 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_5;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_t2156229523  ___m_TotalMinSize_6;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_t2156229523  ___m_TotalPreferredSize_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_t2156229523  ___m_TotalFlexibleSize_8;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t881764471 * ___m_RectChildren_9;

public:
	inline static int32_t get_offset_of_m_Padding_2() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Padding_2)); }
	inline RectOffset_t1369453676 * get_m_Padding_2() const { return ___m_Padding_2; }
	inline RectOffset_t1369453676 ** get_address_of_m_Padding_2() { return &___m_Padding_2; }
	inline void set_m_Padding_2(RectOffset_t1369453676 * value)
	{
		___m_Padding_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_2), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_3() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_ChildAlignment_3)); }
	inline int32_t get_m_ChildAlignment_3() const { return ___m_ChildAlignment_3; }
	inline int32_t* get_address_of_m_ChildAlignment_3() { return &___m_ChildAlignment_3; }
	inline void set_m_ChildAlignment_3(int32_t value)
	{
		___m_ChildAlignment_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Rect_4)); }
	inline RectTransform_t3704657025 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3704657025 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_5 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalMinSize_6)); }
	inline Vector2_t2156229523  get_m_TotalMinSize_6() const { return ___m_TotalMinSize_6; }
	inline Vector2_t2156229523 * get_address_of_m_TotalMinSize_6() { return &___m_TotalMinSize_6; }
	inline void set_m_TotalMinSize_6(Vector2_t2156229523  value)
	{
		___m_TotalMinSize_6 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalPreferredSize_7)); }
	inline Vector2_t2156229523  get_m_TotalPreferredSize_7() const { return ___m_TotalPreferredSize_7; }
	inline Vector2_t2156229523 * get_address_of_m_TotalPreferredSize_7() { return &___m_TotalPreferredSize_7; }
	inline void set_m_TotalPreferredSize_7(Vector2_t2156229523  value)
	{
		___m_TotalPreferredSize_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalFlexibleSize_8)); }
	inline Vector2_t2156229523  get_m_TotalFlexibleSize_8() const { return ___m_TotalFlexibleSize_8; }
	inline Vector2_t2156229523 * get_address_of_m_TotalFlexibleSize_8() { return &___m_TotalFlexibleSize_8; }
	inline void set_m_TotalFlexibleSize_8(Vector2_t2156229523  value)
	{
		___m_TotalFlexibleSize_8 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_RectChildren_9)); }
	inline List_1_t881764471 * get_m_RectChildren_9() const { return ___m_RectChildren_9; }
	inline List_1_t881764471 ** get_address_of_m_RectChildren_9() { return &___m_RectChildren_9; }
	inline void set_m_RectChildren_9(List_1_t881764471 * value)
	{
		___m_RectChildren_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T2436138090_H
#ifndef BASEMESHEFFECT_T2440176439_H
#define BASEMESHEFFECT_T2440176439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t2440176439  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t1660335611 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t2440176439, ___m_Graphic_2)); }
	inline Graphic_t1660335611 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t1660335611 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t1660335611 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T2440176439_H
#ifndef HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#define HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct  HorizontalOrVerticalLayoutGroup_t729725570  : public LayoutGroup_t2436138090
{
public:
	// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_Spacing
	float ___m_Spacing_10;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandWidth
	bool ___m_ChildForceExpandWidth_11;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandHeight
	bool ___m_ChildForceExpandHeight_12;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlWidth
	bool ___m_ChildControlWidth_13;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlHeight
	bool ___m_ChildControlHeight_14;

public:
	inline static int32_t get_offset_of_m_Spacing_10() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_Spacing_10)); }
	inline float get_m_Spacing_10() const { return ___m_Spacing_10; }
	inline float* get_address_of_m_Spacing_10() { return &___m_Spacing_10; }
	inline void set_m_Spacing_10(float value)
	{
		___m_Spacing_10 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandWidth_11() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildForceExpandWidth_11)); }
	inline bool get_m_ChildForceExpandWidth_11() const { return ___m_ChildForceExpandWidth_11; }
	inline bool* get_address_of_m_ChildForceExpandWidth_11() { return &___m_ChildForceExpandWidth_11; }
	inline void set_m_ChildForceExpandWidth_11(bool value)
	{
		___m_ChildForceExpandWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandHeight_12() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildForceExpandHeight_12)); }
	inline bool get_m_ChildForceExpandHeight_12() const { return ___m_ChildForceExpandHeight_12; }
	inline bool* get_address_of_m_ChildForceExpandHeight_12() { return &___m_ChildForceExpandHeight_12; }
	inline void set_m_ChildForceExpandHeight_12(bool value)
	{
		___m_ChildForceExpandHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlWidth_13() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildControlWidth_13)); }
	inline bool get_m_ChildControlWidth_13() const { return ___m_ChildControlWidth_13; }
	inline bool* get_address_of_m_ChildControlWidth_13() { return &___m_ChildControlWidth_13; }
	inline void set_m_ChildControlWidth_13(bool value)
	{
		___m_ChildControlWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlHeight_14() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildControlHeight_14)); }
	inline bool get_m_ChildControlHeight_14() const { return ___m_ChildControlHeight_14; }
	inline bool* get_address_of_m_ChildControlHeight_14() { return &___m_ChildControlHeight_14; }
	inline void set_m_ChildControlHeight_14(bool value)
	{
		___m_ChildControlHeight_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#ifndef SHADOW_T773074319_H
#define SHADOW_T773074319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t773074319  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2555686324  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2156229523  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectColor_3)); }
	inline Color_t2555686324  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2555686324 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2555686324  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectDistance_4)); }
	inline Vector2_t2156229523  get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline Vector2_t2156229523 * get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(Vector2_t2156229523  value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_5() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_UseGraphicAlpha_5)); }
	inline bool get_m_UseGraphicAlpha_5() const { return ___m_UseGraphicAlpha_5; }
	inline bool* get_address_of_m_UseGraphicAlpha_5() { return &___m_UseGraphicAlpha_5; }
	inline void set_m_UseGraphicAlpha_5(bool value)
	{
		___m_UseGraphicAlpha_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T773074319_H
#ifndef POSITIONASUV1_T3991086357_H
#define POSITIONASUV1_T3991086357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t3991086357  : public BaseMeshEffect_t2440176439
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T3991086357_H
#ifndef VERTICALLAYOUTGROUP_T923838031_H
#define VERTICALLAYOUTGROUP_T923838031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VerticalLayoutGroup
struct  VerticalLayoutGroup_t923838031  : public HorizontalOrVerticalLayoutGroup_t729725570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALLAYOUTGROUP_T923838031_H
#ifndef OUTLINE_T2536100125_H
#define OUTLINE_T2536100125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_t2536100125  : public Shadow_t773074319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_T2536100125_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (LayoutRebuilder_t541313304), -1, sizeof(LayoutRebuilder_t541313304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3000[9] = 
{
	LayoutRebuilder_t541313304::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t541313304::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (LayoutUtility_t2745813735), -1, sizeof(LayoutUtility_t2745813735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3001[8] = 
{
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (VerticalLayoutGroup_t923838031), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3004[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3005[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3006[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (ReflectionMethodsCache_t2103211062), -1, sizeof(ReflectionMethodsCache_t2103211062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3007[7] = 
{
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_t2103211062_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (Raycast3DCallback_t701940803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (Raycast2DCallback_t768590915), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (RaycastAllCallback_t1884415901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (GetRayIntersectionAllCallback_t3913627115), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (GetRayIntersectionAllNonAllocCallback_t2311174851), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (GetRaycastNonAllocCallback_t3841783507), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (VertexHelper_t2453304189), -1, sizeof(VertexHelper_t2453304189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3014[11] = 
{
	VertexHelper_t2453304189::get_offset_of_m_Positions_0(),
	VertexHelper_t2453304189::get_offset_of_m_Colors_1(),
	VertexHelper_t2453304189::get_offset_of_m_Uv0S_2(),
	VertexHelper_t2453304189::get_offset_of_m_Uv1S_3(),
	VertexHelper_t2453304189::get_offset_of_m_Uv2S_4(),
	VertexHelper_t2453304189::get_offset_of_m_Uv3S_5(),
	VertexHelper_t2453304189::get_offset_of_m_Normals_6(),
	VertexHelper_t2453304189::get_offset_of_m_Tangents_7(),
	VertexHelper_t2453304189::get_offset_of_m_Indices_8(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (BaseVertexEffect_t2675891272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (BaseMeshEffect_t2440176439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3016[1] = 
{
	BaseMeshEffect_t2440176439::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (Outline_t2536100125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (PositionAsUV1_t3991086357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (Shadow_t773074319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3021[4] = 
{
	Shadow_t773074319::get_offset_of_m_EffectColor_3(),
	Shadow_t773074319::get_offset_of_m_EffectDistance_4(),
	Shadow_t773074319::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255366), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3022[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (U24ArrayTypeU3D12_t2488454196)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454196 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (U3CModuleU3E_t692745552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (Ext_t2749166018), -1, sizeof(Ext_t2749166018_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3025[1] = 
{
	Ext_t2749166018_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (U3CContainsTwiceU3Ec__AnonStorey8_t2970119324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3026[3] = 
{
	U3CContainsTwiceU3Ec__AnonStorey8_t2970119324::get_offset_of_len_0(),
	U3CContainsTwiceU3Ec__AnonStorey8_t2970119324::get_offset_of_values_1(),
	U3CContainsTwiceU3Ec__AnonStorey8_t2970119324::get_offset_of_contains_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (U3CReadBytesAsyncU3Ec__AnonStorey9_t1780370735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3027[5] = 
{
	U3CReadBytesAsyncU3Ec__AnonStorey9_t1780370735::get_offset_of_stream_0(),
	U3CReadBytesAsyncU3Ec__AnonStorey9_t1780370735::get_offset_of_length_1(),
	U3CReadBytesAsyncU3Ec__AnonStorey9_t1780370735::get_offset_of_buffer_2(),
	U3CReadBytesAsyncU3Ec__AnonStorey9_t1780370735::get_offset_of_completed_3(),
	U3CReadBytesAsyncU3Ec__AnonStorey9_t1780370735::get_offset_of_error_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (U3CSplitHeaderValueU3Ec__Iterator0_t2991988224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3028[13] = 
{
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_value_0(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U3ClenU3E__0_1(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_separator_2(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U3CseparatorsU3E__1_3(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U3CbufferU3E__2_4(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U3CquotedU3E__3_5(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U3CescapedU3E__4_6(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U3CiU3E__5_7(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U3CcU3E__6_8(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U24PC_9(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U24current_10(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U3CU24U3Evalue_11(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U3CU24U3Eseparator_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3029[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (MessageEventArgs_t2225057723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3030[3] = 
{
	MessageEventArgs_t2225057723::get_offset_of__data_1(),
	MessageEventArgs_t2225057723::get_offset_of__opcode_2(),
	MessageEventArgs_t2225057723::get_offset_of__rawData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (CloseEventArgs_t1876714021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3031[3] = 
{
	CloseEventArgs_t1876714021::get_offset_of__clean_1(),
	CloseEventArgs_t1876714021::get_offset_of__code_2(),
	CloseEventArgs_t1876714021::get_offset_of__reason_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (ByteOrder_t2496067899)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3032[3] = 
{
	ByteOrder_t2496067899::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (ErrorEventArgs_t3420679011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3033[1] = 
{
	ErrorEventArgs_t3420679011::get_offset_of_U3CMessageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { sizeof (WebSocket_t62038747), -1, sizeof(WebSocket_t62038747_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3034[31] = 
{
	WebSocket_t62038747::get_offset_of__authChallenge_0(),
	WebSocket_t62038747::get_offset_of__base64Key_1(),
	WebSocket_t62038747::get_offset_of__certValidationCallback_2(),
	WebSocket_t62038747::get_offset_of__client_3(),
	WebSocket_t62038747::get_offset_of__closeContext_4(),
	WebSocket_t62038747::get_offset_of__compression_5(),
	WebSocket_t62038747::get_offset_of__context_6(),
	WebSocket_t62038747::get_offset_of__cookies_7(),
	WebSocket_t62038747::get_offset_of__credentials_8(),
	WebSocket_t62038747::get_offset_of__extensions_9(),
	WebSocket_t62038747::get_offset_of__exitReceiving_10(),
	WebSocket_t62038747::get_offset_of__forConn_11(),
	WebSocket_t62038747::get_offset_of__forSend_12(),
	WebSocket_t62038747::get_offset_of__handshakeRequestChecker_13(),
	WebSocket_t62038747::get_offset_of__logger_14(),
	WebSocket_t62038747::get_offset_of__nonceCount_15(),
	WebSocket_t62038747::get_offset_of__origin_16(),
	WebSocket_t62038747::get_offset_of__preAuth_17(),
	WebSocket_t62038747::get_offset_of__protocol_18(),
	WebSocket_t62038747::get_offset_of__protocols_19(),
	WebSocket_t62038747::get_offset_of__readyState_20(),
	WebSocket_t62038747::get_offset_of__receivePong_21(),
	WebSocket_t62038747::get_offset_of__secure_22(),
	WebSocket_t62038747::get_offset_of__stream_23(),
	WebSocket_t62038747::get_offset_of__tcpClient_24(),
	WebSocket_t62038747::get_offset_of__uri_25(),
	WebSocket_t62038747::get_offset_of_OnClose_26(),
	WebSocket_t62038747::get_offset_of_OnError_27(),
	WebSocket_t62038747::get_offset_of_OnMessage_28(),
	WebSocket_t62038747::get_offset_of_OnOpen_29(),
	WebSocket_t62038747_StaticFields::get_offset_of_U3CU3Ef__amU24cache1E_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { sizeof (U3CstartReceivingU3Ec__AnonStoreyE_t1317827957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3035[2] = 
{
	U3CstartReceivingU3Ec__AnonStoreyE_t1317827957::get_offset_of_receive_0(),
	U3CstartReceivingU3Ec__AnonStoreyE_t1317827957::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { sizeof (U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStoreyF_t3346908998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3036[1] = 
{
	U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStoreyF_t3346908998::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (U3CConnectAsyncU3Ec__AnonStorey10_t2977358739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3037[2] = 
{
	U3CConnectAsyncU3Ec__AnonStorey10_t2977358739::get_offset_of_connector_0(),
	U3CConnectAsyncU3Ec__AnonStorey10_t2977358739::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (WsStream_t2500460924), -1, sizeof(WsStream_t2500460924_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3038[4] = 
{
	WsStream_t2500460924::get_offset_of__forWrite_0(),
	WsStream_t2500460924::get_offset_of__innerStream_1(),
	WsStream_t2500460924::get_offset_of__secure_2(),
	WsStream_t2500460924_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (U3CreadHandshakeHeadersU3Ec__AnonStorey14_t4227143414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3039[2] = 
{
	U3CreadHandshakeHeadersU3Ec__AnonStorey14_t4227143414::get_offset_of_buffer_0(),
	U3CreadHandshakeHeadersU3Ec__AnonStorey14_t4227143414::get_offset_of_count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3040[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (Cookie_t4203102285), -1, sizeof(Cookie_t4203102285_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3041[17] = 
{
	Cookie_t4203102285_StaticFields::get_offset_of__reservedCharsForName_0(),
	Cookie_t4203102285_StaticFields::get_offset_of__reservedCharsForValue_1(),
	Cookie_t4203102285::get_offset_of__comment_2(),
	Cookie_t4203102285::get_offset_of__commentUri_3(),
	Cookie_t4203102285::get_offset_of__discard_4(),
	Cookie_t4203102285::get_offset_of__domain_5(),
	Cookie_t4203102285::get_offset_of__expires_6(),
	Cookie_t4203102285::get_offset_of__httpOnly_7(),
	Cookie_t4203102285::get_offset_of__name_8(),
	Cookie_t4203102285::get_offset_of__path_9(),
	Cookie_t4203102285::get_offset_of__port_10(),
	Cookie_t4203102285::get_offset_of__ports_11(),
	Cookie_t4203102285::get_offset_of__secure_12(),
	Cookie_t4203102285::get_offset_of__timestamp_13(),
	Cookie_t4203102285::get_offset_of__value_14(),
	Cookie_t4203102285::get_offset_of__version_15(),
	Cookie_t4203102285::get_offset_of_U3CExactDomainU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (CookieCollection_t962330244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3042[2] = 
{
	CookieCollection_t962330244::get_offset_of__list_0(),
	CookieCollection_t962330244::get_offset_of__sync_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (CookieException_t1899604716), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (HttpUtility_t3452211165), -1, sizeof(HttpUtility_t3452211165_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3044[2] = 
{
	HttpUtility_t3452211165_StaticFields::get_offset_of__hexChars_0(),
	HttpUtility_t3452211165_StaticFields::get_offset_of__sync_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (WebHeaderCollection_t1205255311), -1, sizeof(WebHeaderCollection_t1205255311_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3045[3] = 
{
	WebHeaderCollection_t1205255311_StaticFields::get_offset_of_headers_12(),
	WebHeaderCollection_t1205255311::get_offset_of_internallyCreated_13(),
	WebHeaderCollection_t1205255311::get_offset_of_state_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (U3CGetHeaderInfoU3Ec__AnonStorey17_t3185675556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3046[1] = 
{
	U3CGetHeaderInfoU3Ec__AnonStorey17_t3185675556::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (U3CGetObjectDataU3Ec__AnonStorey19_t3093865635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3047[3] = 
{
	U3CGetObjectDataU3Ec__AnonStorey19_t3093865635::get_offset_of_serializationInfo_0(),
	U3CGetObjectDataU3Ec__AnonStorey19_t3093865635::get_offset_of_count_1(),
	U3CGetObjectDataU3Ec__AnonStorey19_t3093865635::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { sizeof (U3CToStringU3Ec__AnonStorey1A_t546952984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3048[2] = 
{
	U3CToStringU3Ec__AnonStorey1A_t546952984::get_offset_of_sb_0(),
	U3CToStringU3Ec__AnonStorey1A_t546952984::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (HttpVersion_t1629733732), -1, sizeof(HttpVersion_t1629733732_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3049[2] = 
{
	HttpVersion_t1629733732_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t1629733732_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (HttpStatusCode_t4029129360)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3050[47] = 
{
	HttpStatusCode_t4029129360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (SslStream_t3289593333), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { sizeof (CloseStatusCode_t3786097442)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3052[14] = 
{
	CloseStatusCode_t3786097442::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { sizeof (Fin_t411169233)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3053[3] = 
{
	Fin_t411169233::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { sizeof (Mask_t3471462035)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3054[3] = 
{
	Mask_t3471462035::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (Opcode_t2755924248)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3055[7] = 
{
	Opcode_t2755924248::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (PayloadData_t688932160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3056[3] = 
{
	PayloadData_t688932160::get_offset_of_U3CIsMaskedU3Ek__BackingField_0(),
	PayloadData_t688932160::get_offset_of_U3CExtensionDataU3Ek__BackingField_1(),
	PayloadData_t688932160::get_offset_of_U3CApplicationDataU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (U3CGetEnumeratorU3Ec__Iterator2_t269000295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3057[9] = 
{
	U3CGetEnumeratorU3Ec__Iterator2_t269000295::get_offset_of_U3CU3CU24U24U3EU3E__0_0(),
	U3CGetEnumeratorU3Ec__Iterator2_t269000295::get_offset_of_U3CU3CU24U24U3EU3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator2_t269000295::get_offset_of_U3CbU3E__2_2(),
	U3CGetEnumeratorU3Ec__Iterator2_t269000295::get_offset_of_U3CU3CU24U24U3EU3E__3_3(),
	U3CGetEnumeratorU3Ec__Iterator2_t269000295::get_offset_of_U3CU3CU24U24U3EU3E__4_4(),
	U3CGetEnumeratorU3Ec__Iterator2_t269000295::get_offset_of_U3CbU3E__5_5(),
	U3CGetEnumeratorU3Ec__Iterator2_t269000295::get_offset_of_U24PC_6(),
	U3CGetEnumeratorU3Ec__Iterator2_t269000295::get_offset_of_U24current_7(),
	U3CGetEnumeratorU3Ec__Iterator2_t269000295::get_offset_of_U3CU3Ef__this_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { sizeof (Rsv_t2704667083)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3058[3] = 
{
	Rsv_t2704667083::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { sizeof (WsFrame_t4110797288), -1, sizeof(WsFrame_t4110797288_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3059[11] = 
{
	WsFrame_t4110797288_StaticFields::get_offset_of_EmptyUnmaskPingData_0(),
	WsFrame_t4110797288::get_offset_of_U3CFinU3Ek__BackingField_1(),
	WsFrame_t4110797288::get_offset_of_U3CRsv1U3Ek__BackingField_2(),
	WsFrame_t4110797288::get_offset_of_U3CRsv2U3Ek__BackingField_3(),
	WsFrame_t4110797288::get_offset_of_U3CRsv3U3Ek__BackingField_4(),
	WsFrame_t4110797288::get_offset_of_U3COpcodeU3Ek__BackingField_5(),
	WsFrame_t4110797288::get_offset_of_U3CMaskU3Ek__BackingField_6(),
	WsFrame_t4110797288::get_offset_of_U3CPayloadLenU3Ek__BackingField_7(),
	WsFrame_t4110797288::get_offset_of_U3CExtPayloadLenU3Ek__BackingField_8(),
	WsFrame_t4110797288::get_offset_of_U3CMaskingKeyU3Ek__BackingField_9(),
	WsFrame_t4110797288::get_offset_of_U3CPayloadDataU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { sizeof (U3CdumpU3Ec__AnonStorey1C_t1594701605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3060[2] = 
{
	U3CdumpU3Ec__AnonStorey1C_t1594701605::get_offset_of_countFmt_0(),
	U3CdumpU3Ec__AnonStorey1C_t1594701605::get_offset_of_buffer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { sizeof (U3CdumpU3Ec__AnonStorey1D_t905098148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3061[3] = 
{
	U3CdumpU3Ec__AnonStorey1D_t905098148::get_offset_of_lineFmt_0(),
	U3CdumpU3Ec__AnonStorey1D_t905098148::get_offset_of_lineCount_1(),
	U3CdumpU3Ec__AnonStorey1D_t905098148::get_offset_of_U3CU3Ef__refU2428_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { sizeof (U3CGetEnumeratorU3Ec__Iterator3_t1159708565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3062[6] = 
{
	U3CGetEnumeratorU3Ec__Iterator3_t1159708565::get_offset_of_U3CU3CU24U24U3EU3E__0_0(),
	U3CGetEnumeratorU3Ec__Iterator3_t1159708565::get_offset_of_U3CU3CU24U24U3EU3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator3_t1159708565::get_offset_of_U3CbU3E__2_2(),
	U3CGetEnumeratorU3Ec__Iterator3_t1159708565::get_offset_of_U24PC_3(),
	U3CGetEnumeratorU3Ec__Iterator3_t1159708565::get_offset_of_U24current_4(),
	U3CGetEnumeratorU3Ec__Iterator3_t1159708565::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { sizeof (U3CParseAsyncU3Ec__AnonStorey1E_t3168773810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3063[3] = 
{
	U3CParseAsyncU3Ec__AnonStorey1E_t3168773810::get_offset_of_stream_0(),
	U3CParseAsyncU3Ec__AnonStorey1E_t3168773810::get_offset_of_unmask_1(),
	U3CParseAsyncU3Ec__AnonStorey1E_t3168773810::get_offset_of_completed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { sizeof (WebSocketContext_t619421455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { sizeof (HttpHeaderType_t2889339440)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3065[8] = 
{
	HttpHeaderType_t2889339440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { sizeof (HttpHeaderInfo_t2062258289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3066[2] = 
{
	HttpHeaderInfo_t2062258289::get_offset_of_U3CNameU3Ek__BackingField_0(),
	HttpHeaderInfo_t2062258289::get_offset_of_U3CTypeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { sizeof (CompressionMethod_t1062973517)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3067[3] = 
{
	CompressionMethod_t1062973517::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { sizeof (WebSocketException_t618477455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3068[1] = 
{
	WebSocketException_t618477455::get_offset_of_U3CCodeU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { sizeof (AuthenticationChallenge_t3974775170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3069[2] = 
{
	AuthenticationChallenge_t3974775170::get_offset_of__params_0(),
	AuthenticationChallenge_t3974775170::get_offset_of__scheme_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { sizeof (AuthenticationResponse_t4290196037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3070[3] = 
{
	AuthenticationResponse_t4290196037::get_offset_of__nonceCount_0(),
	AuthenticationResponse_t4290196037::get_offset_of__params_1(),
	AuthenticationResponse_t4290196037::get_offset_of__scheme_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { sizeof (LogData_t2329603299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3071[4] = 
{
	LogData_t2329603299::get_offset_of__caller_0(),
	LogData_t2329603299::get_offset_of__date_1(),
	LogData_t2329603299::get_offset_of__level_2(),
	LogData_t2329603299::get_offset_of__message_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { sizeof (LogLevel_t2581836550)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3072[7] = 
{
	LogLevel_t2581836550::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { sizeof (Logger_t4025333586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3073[4] = 
{
	Logger_t4025333586::get_offset_of__file_0(),
	Logger_t4025333586::get_offset_of__level_1(),
	Logger_t4025333586::get_offset_of__output_2(),
	Logger_t4025333586::get_offset_of__sync_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { sizeof (HandshakeBase_t1142904083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3074[3] = 
{
	HandshakeBase_t1142904083::get_offset_of__entity_0(),
	HandshakeBase_t1142904083::get_offset_of__headers_1(),
	HandshakeBase_t1142904083::get_offset_of__version_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { sizeof (HandshakeRequest_t3322584187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3075[3] = 
{
	HandshakeRequest_t3322584187::get_offset_of__method_3(),
	HandshakeRequest_t3322584187::get_offset_of__rawUrl_4(),
	HandshakeRequest_t3322584187::get_offset_of__uri_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { sizeof (HandshakeResponse_t1669986296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3076[2] = 
{
	HandshakeResponse_t1669986296::get_offset_of__code_3(),
	HandshakeResponse_t1669986296::get_offset_of__reason_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { sizeof (WebSocketState_t45461673)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3077[5] = 
{
	WebSocketState_t45461673::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { sizeof (NetworkCredential_t1094796801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3078[3] = 
{
	NetworkCredential_t1094796801::get_offset_of__domain_0(),
	NetworkCredential_t1094796801::get_offset_of__password_1(),
	NetworkCredential_t1094796801::get_offset_of__username_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (U3CPrivateImplementationDetailsU3EU7Bbe551f8dU2Daed3U2D403cU2Da1e7U2D93fc616e7213U7D_t3120359854), -1, sizeof(U3CPrivateImplementationDetailsU3EU7Bbe551f8dU2Daed3U2D403cU2Da1e7U2D93fc616e7213U7D_t3120359854_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3079[1] = 
{
	U3CPrivateImplementationDetailsU3EU7Bbe551f8dU2Daed3U2D403cU2Da1e7U2D93fc616e7213U7D_t3120359854_StaticFields::get_offset_of_U24fieldU2D0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (U24ArrayTypeU3D16_t901447706)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D16_t901447706 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { sizeof (U3CModuleU3E_t692745553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (ArabicFixer_t565187177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (ArabicMapping_t3619416640), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3083[2] = 
{
	ArabicMapping_t3619416640::get_offset_of_from_0(),
	ArabicMapping_t3619416640::get_offset_of_to_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (ArabicTable_t2015225755), -1, sizeof(ArabicTable_t2015225755_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3084[2] = 
{
	ArabicTable_t2015225755_StaticFields::get_offset_of_mapList_0(),
	ArabicTable_t2015225755_StaticFields::get_offset_of_arabicMapper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { sizeof (TashkeelLocation_t3651416022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3085[2] = 
{
	TashkeelLocation_t3651416022::get_offset_of_tashkeel_0(),
	TashkeelLocation_t3651416022::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { sizeof (ArabicFixerTool_t2520528640), -1, sizeof(ArabicFixerTool_t2520528640_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3086[2] = 
{
	ArabicFixerTool_t2520528640_StaticFields::get_offset_of_showTashkeel_0(),
	ArabicFixerTool_t2520528640_StaticFields::get_offset_of_useHinduNumbers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { sizeof (U3CPrivateImplementationDetailsU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_t293356573), -1, sizeof(U3CPrivateImplementationDetailsU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_t293356573_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3087[1] = 
{
	U3CPrivateImplementationDetailsU3EU7BCC42C19AU2D51E0U2D4351U2D874BU2D4EFF206CE97FU7D_t293356573_StaticFields::get_offset_of_U24U24method0x6000009U2D1_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { sizeof (__StaticArrayInitTypeSizeU3D18_t270274279)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D18_t270274279 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { sizeof (U3CModuleU3E_t692745554), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { sizeof (FirebaseMessagingPINVOKE_t151993683), -1, sizeof(FirebaseMessagingPINVOKE_t151993683_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3090[2] = 
{
	FirebaseMessagingPINVOKE_t151993683_StaticFields::get_offset_of_swigExceptionHelper_0(),
	FirebaseMessagingPINVOKE_t151993683_StaticFields::get_offset_of_swigStringHelper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { sizeof (SWIGExceptionHelper_t2033454307), -1, sizeof(SWIGExceptionHelper_t2033454307_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3091[14] = 
{
	SWIGExceptionHelper_t2033454307_StaticFields::get_offset_of_applicationDelegate_0(),
	SWIGExceptionHelper_t2033454307_StaticFields::get_offset_of_arithmeticDelegate_1(),
	SWIGExceptionHelper_t2033454307_StaticFields::get_offset_of_divideByZeroDelegate_2(),
	SWIGExceptionHelper_t2033454307_StaticFields::get_offset_of_indexOutOfRangeDelegate_3(),
	SWIGExceptionHelper_t2033454307_StaticFields::get_offset_of_invalidCastDelegate_4(),
	SWIGExceptionHelper_t2033454307_StaticFields::get_offset_of_invalidOperationDelegate_5(),
	SWIGExceptionHelper_t2033454307_StaticFields::get_offset_of_ioDelegate_6(),
	SWIGExceptionHelper_t2033454307_StaticFields::get_offset_of_nullReferenceDelegate_7(),
	SWIGExceptionHelper_t2033454307_StaticFields::get_offset_of_outOfMemoryDelegate_8(),
	SWIGExceptionHelper_t2033454307_StaticFields::get_offset_of_overflowDelegate_9(),
	SWIGExceptionHelper_t2033454307_StaticFields::get_offset_of_systemDelegate_10(),
	SWIGExceptionHelper_t2033454307_StaticFields::get_offset_of_argumentDelegate_11(),
	SWIGExceptionHelper_t2033454307_StaticFields::get_offset_of_argumentNullDelegate_12(),
	SWIGExceptionHelper_t2033454307_StaticFields::get_offset_of_argumentOutOfRangeDelegate_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { sizeof (ExceptionDelegate_t1699519678), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (ExceptionArgumentDelegate_t1079801895), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { sizeof (SWIGPendingException_t717714779), -1, sizeof(SWIGPendingException_t717714779_StaticFields), sizeof(SWIGPendingException_t717714779_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable3094[2] = 
{
	THREAD_STATIC_FIELD_OFFSET,
	SWIGPendingException_t717714779_StaticFields::get_offset_of_numExceptionsPending_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { sizeof (SWIGStringHelper_t1399197998), -1, sizeof(SWIGStringHelper_t1399197998_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3095[1] = 
{
	SWIGStringHelper_t1399197998_StaticFields::get_offset_of_stringDelegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { sizeof (SWIGStringDelegate_t3107596264), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { sizeof (FirebaseMessaging_t2538764775), -1, sizeof(FirebaseMessaging_t2538764775_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3097[3] = 
{
	FirebaseMessaging_t2538764775_StaticFields::get_offset_of_MessageReceivedInternal_0(),
	FirebaseMessaging_t2538764775_StaticFields::get_offset_of_TokenReceivedInternal_1(),
	FirebaseMessaging_t2538764775_StaticFields::get_offset_of_listener_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { sizeof (Listener_t3000907612), -1, sizeof(Listener_t3000907612_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3098[4] = 
{
	Listener_t3000907612::get_offset_of_messageReceivedDelegate_0(),
	Listener_t3000907612::get_offset_of_tokenReceivedDelegate_1(),
	Listener_t3000907612::get_offset_of_app_2(),
	Listener_t3000907612_StaticFields::get_offset_of_listener_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { sizeof (MessageReceivedDelegate_t2474724020), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
