﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuildingModel
struct BuildingModel_t2830632387;

#include "codegen/il2cpp-codegen.h"

// System.Void BuildingModel::.ctor()
extern "C"  void BuildingModel__ctor_m3638352788 (BuildingModel_t2830632387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
