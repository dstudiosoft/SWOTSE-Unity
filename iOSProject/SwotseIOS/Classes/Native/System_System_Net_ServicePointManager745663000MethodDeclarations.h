﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.ServicePointManager
struct ServicePointManager_t745663000;
// System.Net.ICertificatePolicy
struct ICertificatePolicy_t1916536542;
// System.Exception
struct Exception_t1927440687;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t2756269959;
// System.Net.ServicePoint
struct ServicePoint_t2765344313;
// System.Uri
struct Uri_t19570940;
// System.String
struct String_t;
// System.Net.IWebProxy
struct IWebProxy_t3916853445;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_SecurityProtocolType3099771628.h"
#include "System_System_Net_Security_RemoteCertificateValida2756269959.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Net.ServicePointManager::.ctor()
extern "C"  void ServicePointManager__ctor_m2814420450 (ServicePointManager_t745663000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::.cctor()
extern "C"  void ServicePointManager__cctor_m2216295373 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ICertificatePolicy System.Net.ServicePointManager::get_CertificatePolicy()
extern "C"  Il2CppObject * ServicePointManager_get_CertificatePolicy_m345233313 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_CertificatePolicy(System.Net.ICertificatePolicy)
extern "C"  void ServicePointManager_set_CertificatePolicy_m1373804120 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.ServicePointManager::get_CheckCertificateRevocationList()
extern "C"  bool ServicePointManager_get_CheckCertificateRevocationList_m415857194 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_CheckCertificateRevocationList(System.Boolean)
extern "C"  void ServicePointManager_set_CheckCertificateRevocationList_m3121516469 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.ServicePointManager::get_DefaultConnectionLimit()
extern "C"  int32_t ServicePointManager_get_DefaultConnectionLimit_m2343567033 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_DefaultConnectionLimit(System.Int32)
extern "C"  void ServicePointManager_set_DefaultConnectionLimit_m2323020224 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Net.ServicePointManager::GetMustImplement()
extern "C"  Exception_t1927440687 * ServicePointManager_GetMustImplement_m3773187183 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.ServicePointManager::get_DnsRefreshTimeout()
extern "C"  int32_t ServicePointManager_get_DnsRefreshTimeout_m2957475852 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_DnsRefreshTimeout(System.Int32)
extern "C"  void ServicePointManager_set_DnsRefreshTimeout_m96212681 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.ServicePointManager::get_EnableDnsRoundRobin()
extern "C"  bool ServicePointManager_get_EnableDnsRoundRobin_m1904743977 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_EnableDnsRoundRobin(System.Boolean)
extern "C"  void ServicePointManager_set_EnableDnsRoundRobin_m116220310 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.ServicePointManager::get_MaxServicePointIdleTime()
extern "C"  int32_t ServicePointManager_get_MaxServicePointIdleTime_m485652025 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_MaxServicePointIdleTime(System.Int32)
extern "C"  void ServicePointManager_set_MaxServicePointIdleTime_m1456116142 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.ServicePointManager::get_MaxServicePoints()
extern "C"  int32_t ServicePointManager_get_MaxServicePoints_m590810495 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_MaxServicePoints(System.Int32)
extern "C"  void ServicePointManager_set_MaxServicePoints_m3843558982 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.SecurityProtocolType System.Net.ServicePointManager::get_SecurityProtocol()
extern "C"  int32_t ServicePointManager_get_SecurityProtocol_m1763625218 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_SecurityProtocol(System.Net.SecurityProtocolType)
extern "C"  void ServicePointManager_set_SecurityProtocol_m782164561 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Security.RemoteCertificateValidationCallback System.Net.ServicePointManager::get_ServerCertificateValidationCallback()
extern "C"  RemoteCertificateValidationCallback_t2756269959 * ServicePointManager_get_ServerCertificateValidationCallback_m1409951209 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_ServerCertificateValidationCallback(System.Net.Security.RemoteCertificateValidationCallback)
extern "C"  void ServicePointManager_set_ServerCertificateValidationCallback_m1450099506 (Il2CppObject * __this /* static, unused */, RemoteCertificateValidationCallback_t2756269959 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.ServicePointManager::get_Expect100Continue()
extern "C"  bool ServicePointManager_get_Expect100Continue_m1989363864 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_Expect100Continue(System.Boolean)
extern "C"  void ServicePointManager_set_Expect100Continue_m912104655 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.ServicePointManager::get_UseNagleAlgorithm()
extern "C"  bool ServicePointManager_get_UseNagleAlgorithm_m622761288 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_UseNagleAlgorithm(System.Boolean)
extern "C"  void ServicePointManager_set_UseNagleAlgorithm_m1193764713 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ServicePoint System.Net.ServicePointManager::FindServicePoint(System.Uri)
extern "C"  ServicePoint_t2765344313 * ServicePointManager_FindServicePoint_m890985371 (Il2CppObject * __this /* static, unused */, Uri_t19570940 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ServicePoint System.Net.ServicePointManager::FindServicePoint(System.String,System.Net.IWebProxy)
extern "C"  ServicePoint_t2765344313 * ServicePointManager_FindServicePoint_m2153109767 (Il2CppObject * __this /* static, unused */, String_t* ___uriString0, Il2CppObject * ___proxy1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ServicePoint System.Net.ServicePointManager::FindServicePoint(System.Uri,System.Net.IWebProxy)
extern "C"  ServicePoint_t2765344313 * ServicePointManager_FindServicePoint_m3137356168 (Il2CppObject * __this /* static, unused */, Uri_t19570940 * ___address0, Il2CppObject * ___proxy1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::RecycleServicePoints()
extern "C"  void ServicePointManager_RecycleServicePoints_m2772618783 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
