﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.CharVector
struct CharVector_t100493339;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t1158628263;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Firebase.CharVector/CharVectorEnumerator
struct CharVectorEnumerator_t2036478274;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Firebase_App_Firebase_CharVector100493339.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"

// System.Void Firebase.CharVector::.ctor(System.IntPtr,System.Boolean)
extern "C"  void CharVector__ctor_m405035648 (CharVector_t100493339 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::.ctor(System.Collections.ICollection)
extern "C"  void CharVector__ctor_m2829139272 (CharVector_t100493339 * __this, Il2CppObject * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::.ctor()
extern "C"  void CharVector__ctor_m2824687087 (CharVector_t100493339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::.ctor(Firebase.CharVector)
extern "C"  void CharVector__ctor_m3941811537 (CharVector_t100493339 * __this, CharVector_t100493339 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::.ctor(System.Int32)
extern "C"  void CharVector__ctor_m2758809780 (CharVector_t100493339 * __this, int32_t ___capacity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef Firebase.CharVector::getCPtr(Firebase.CharVector)
extern "C"  HandleRef_t2419939847  CharVector_getCPtr_m635066920 (Il2CppObject * __this /* static, unused */, CharVector_t100493339 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::Finalize()
extern "C"  void CharVector_Finalize_m3259868725 (CharVector_t100493339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::Dispose()
extern "C"  void CharVector_Dispose_m1302934648 (CharVector_t100493339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.CharVector::get_IsFixedSize()
extern "C"  bool CharVector_get_IsFixedSize_m345393155 (CharVector_t100493339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.CharVector::get_IsReadOnly()
extern "C"  bool CharVector_get_IsReadOnly_m476869892 (CharVector_t100493339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Firebase.CharVector::get_Item(System.Int32)
extern "C"  uint8_t CharVector_get_Item_m4193397146 (CharVector_t100493339 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::set_Item(System.Int32,System.Byte)
extern "C"  void CharVector_set_Item_m1187648559 (CharVector_t100493339 * __this, int32_t ___index0, uint8_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.CharVector::get_Capacity()
extern "C"  int32_t CharVector_get_Capacity_m2929193850 (CharVector_t100493339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::set_Capacity(System.Int32)
extern "C"  void CharVector_set_Capacity_m3630821279 (CharVector_t100493339 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.CharVector::get_Count()
extern "C"  int32_t CharVector_get_Count_m4039697447 (CharVector_t100493339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.CharVector::get_IsSynchronized()
extern "C"  bool CharVector_get_IsSynchronized_m1641836212 (CharVector_t100493339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::CopyTo(System.Byte[])
extern "C"  void CharVector_CopyTo_m3559695208 (CharVector_t100493339 * __this, ByteU5BU5D_t3397334013* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::CopyTo(System.Byte[],System.Int32)
extern "C"  void CharVector_CopyTo_m3786990279 (CharVector_t100493339 * __this, ByteU5BU5D_t3397334013* ___array0, int32_t ___arrayIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::CopyTo(System.Int32,System.Byte[],System.Int32,System.Int32)
extern "C"  void CharVector_CopyTo_m1263167855 (CharVector_t100493339 * __this, int32_t ___index0, ByteU5BU5D_t3397334013* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Byte> Firebase.CharVector::System.Collections.Generic.IEnumerable<byte>.GetEnumerator()
extern "C"  Il2CppObject* CharVector_System_Collections_Generic_IEnumerableU3CbyteU3E_GetEnumerator_m1784402620 (CharVector_t100493339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Firebase.CharVector::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * CharVector_System_Collections_IEnumerable_GetEnumerator_m1319468512 (CharVector_t100493339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.CharVector/CharVectorEnumerator Firebase.CharVector::GetEnumerator()
extern "C"  CharVectorEnumerator_t2036478274 * CharVector_GetEnumerator_m2630931990 (CharVector_t100493339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::Clear()
extern "C"  void CharVector_Clear_m3486209096 (CharVector_t100493339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::Add(System.Byte)
extern "C"  void CharVector_Add_m1543824239 (CharVector_t100493339 * __this, uint8_t ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Firebase.CharVector::size()
extern "C"  uint32_t CharVector_size_m62581169 (CharVector_t100493339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Firebase.CharVector::capacity()
extern "C"  uint32_t CharVector_capacity_m3935473554 (CharVector_t100493339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::reserve(System.UInt32)
extern "C"  void CharVector_reserve_m1645305395 (CharVector_t100493339 * __this, uint32_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Firebase.CharVector::getitemcopy(System.Int32)
extern "C"  uint8_t CharVector_getitemcopy_m211287402 (CharVector_t100493339 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Firebase.CharVector::getitem(System.Int32)
extern "C"  uint8_t CharVector_getitem_m629676035 (CharVector_t100493339 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::setitem(System.Int32,System.Byte)
extern "C"  void CharVector_setitem_m3819660678 (CharVector_t100493339 * __this, int32_t ___index0, uint8_t ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::AddRange(Firebase.CharVector)
extern "C"  void CharVector_AddRange_m1323328771 (CharVector_t100493339 * __this, CharVector_t100493339 * ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.CharVector Firebase.CharVector::GetRange(System.Int32,System.Int32)
extern "C"  CharVector_t100493339 * CharVector_GetRange_m662911479 (CharVector_t100493339 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::Insert(System.Int32,System.Byte)
extern "C"  void CharVector_Insert_m3638319084 (CharVector_t100493339 * __this, int32_t ___index0, uint8_t ___x1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::InsertRange(System.Int32,Firebase.CharVector)
extern "C"  void CharVector_InsertRange_m1081687124 (CharVector_t100493339 * __this, int32_t ___index0, CharVector_t100493339 * ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::RemoveAt(System.Int32)
extern "C"  void CharVector_RemoveAt_m3868280593 (CharVector_t100493339 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::RemoveRange(System.Int32,System.Int32)
extern "C"  void CharVector_RemoveRange_m835001268 (CharVector_t100493339 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.CharVector Firebase.CharVector::Repeat(System.Byte,System.Int32)
extern "C"  CharVector_t100493339 * CharVector_Repeat_m207293633 (Il2CppObject * __this /* static, unused */, uint8_t ___value0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::Reverse()
extern "C"  void CharVector_Reverse_m4055113935 (CharVector_t100493339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::Reverse(System.Int32,System.Int32)
extern "C"  void CharVector_Reverse_m162890179 (CharVector_t100493339 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector::SetRange(System.Int32,Firebase.CharVector)
extern "C"  void CharVector_SetRange_m517012081 (CharVector_t100493339 * __this, int32_t ___index0, CharVector_t100493339 * ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.CharVector::Contains(System.Byte)
extern "C"  bool CharVector_Contains_m3352553669 (CharVector_t100493339 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.CharVector::IndexOf(System.Byte)
extern "C"  int32_t CharVector_IndexOf_m1349922419 (CharVector_t100493339 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.CharVector::LastIndexOf(System.Byte)
extern "C"  int32_t CharVector_LastIndexOf_m2665269793 (CharVector_t100493339 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.CharVector::Remove(System.Byte)
extern "C"  bool CharVector_Remove_m2775487586 (CharVector_t100493339 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
