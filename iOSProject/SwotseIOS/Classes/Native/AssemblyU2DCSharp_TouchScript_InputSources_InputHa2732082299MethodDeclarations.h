﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.InputSources.InputHandlers.TouchHandler/TouchState
struct TouchState_t2732082299;
struct TouchState_t2732082299_marshaled_pinvoke;
struct TouchState_t2732082299_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_InputSources_InputHa2732082299.h"
#include "UnityEngine_UnityEngine_TouchPhase2458120420.h"

// System.Void TouchScript.InputSources.InputHandlers.TouchHandler/TouchState::.ctor(System.Int32,UnityEngine.TouchPhase)
extern "C"  void TouchState__ctor_m4169137556 (TouchState_t2732082299 * __this, int32_t ___id0, int32_t ___phase1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct TouchState_t2732082299;
struct TouchState_t2732082299_marshaled_pinvoke;

extern "C" void TouchState_t2732082299_marshal_pinvoke(const TouchState_t2732082299& unmarshaled, TouchState_t2732082299_marshaled_pinvoke& marshaled);
extern "C" void TouchState_t2732082299_marshal_pinvoke_back(const TouchState_t2732082299_marshaled_pinvoke& marshaled, TouchState_t2732082299& unmarshaled);
extern "C" void TouchState_t2732082299_marshal_pinvoke_cleanup(TouchState_t2732082299_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct TouchState_t2732082299;
struct TouchState_t2732082299_marshaled_com;

extern "C" void TouchState_t2732082299_marshal_com(const TouchState_t2732082299& unmarshaled, TouchState_t2732082299_marshaled_com& marshaled);
extern "C" void TouchState_t2732082299_marshal_com_back(const TouchState_t2732082299_marshaled_com& marshaled, TouchState_t2732082299& unmarshaled);
extern "C" void TouchState_t2732082299_marshal_com_cleanup(TouchState_t2732082299_marshaled_com& marshaled);
