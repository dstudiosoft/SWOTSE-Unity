﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator1
struct U3CDragObjectU3Ec__Iterator1_t2509163240;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator1::.ctor()
extern "C"  void U3CDragObjectU3Ec__Iterator1__ctor_m1693171087 (U3CDragObjectU3Ec__Iterator1_t2509163240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDragObjectU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m261727385 (U3CDragObjectU3Ec__Iterator1_t2509163240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDragObjectU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3191535265 (U3CDragObjectU3Ec__Iterator1_t2509163240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator1::MoveNext()
extern "C"  bool U3CDragObjectU3Ec__Iterator1_MoveNext_m3732987113 (U3CDragObjectU3Ec__Iterator1_t2509163240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator1::Dispose()
extern "C"  void U3CDragObjectU3Ec__Iterator1_Dispose_m3073477624 (U3CDragObjectU3Ec__Iterator1_t2509163240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator1::Reset()
extern "C"  void U3CDragObjectU3Ec__Iterator1_Reset_m2488591086 (U3CDragObjectU3Ec__Iterator1_t2509163240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
