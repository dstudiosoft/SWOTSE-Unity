﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t1030026315;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuyShillingsScript
struct  BuyShillingsScript_t814468168  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.ToggleGroup BuyShillingsScript::choisesGroup
	ToggleGroup_t1030026315 * ___choisesGroup_2;

public:
	inline static int32_t get_offset_of_choisesGroup_2() { return static_cast<int32_t>(offsetof(BuyShillingsScript_t814468168, ___choisesGroup_2)); }
	inline ToggleGroup_t1030026315 * get_choisesGroup_2() const { return ___choisesGroup_2; }
	inline ToggleGroup_t1030026315 ** get_address_of_choisesGroup_2() { return &___choisesGroup_2; }
	inline void set_choisesGroup_2(ToggleGroup_t1030026315 * value)
	{
		___choisesGroup_2 = value;
		Il2CppCodeGenWriteBarrier(&___choisesGroup_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
