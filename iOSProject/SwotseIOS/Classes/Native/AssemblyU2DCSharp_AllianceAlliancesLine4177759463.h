﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// AllianceAlliancesTableController
struct AllianceAlliancesTableController_t831003683;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceAlliancesLine
struct  AllianceAlliancesLine_t4177759463  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text AllianceAlliancesLine::allianceName
	Text_t356221433 * ___allianceName_2;
	// UnityEngine.UI.Text AllianceAlliancesLine::founder
	Text_t356221433 * ___founder_3;
	// UnityEngine.UI.Text AllianceAlliancesLine::leader
	Text_t356221433 * ___leader_4;
	// UnityEngine.UI.Text AllianceAlliancesLine::experience
	Text_t356221433 * ___experience_5;
	// UnityEngine.UI.Text AllianceAlliancesLine::rank
	Text_t356221433 * ___rank_6;
	// UnityEngine.UI.Text AllianceAlliancesLine::members
	Text_t356221433 * ___members_7;
	// UnityEngine.GameObject AllianceAlliancesLine::joinBtn
	GameObject_t1756533147 * ___joinBtn_8;
	// System.Int64 AllianceAlliancesLine::allianceId
	int64_t ___allianceId_9;
	// AllianceAlliancesTableController AllianceAlliancesLine::owner
	AllianceAlliancesTableController_t831003683 * ___owner_10;

public:
	inline static int32_t get_offset_of_allianceName_2() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t4177759463, ___allianceName_2)); }
	inline Text_t356221433 * get_allianceName_2() const { return ___allianceName_2; }
	inline Text_t356221433 ** get_address_of_allianceName_2() { return &___allianceName_2; }
	inline void set_allianceName_2(Text_t356221433 * value)
	{
		___allianceName_2 = value;
		Il2CppCodeGenWriteBarrier(&___allianceName_2, value);
	}

	inline static int32_t get_offset_of_founder_3() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t4177759463, ___founder_3)); }
	inline Text_t356221433 * get_founder_3() const { return ___founder_3; }
	inline Text_t356221433 ** get_address_of_founder_3() { return &___founder_3; }
	inline void set_founder_3(Text_t356221433 * value)
	{
		___founder_3 = value;
		Il2CppCodeGenWriteBarrier(&___founder_3, value);
	}

	inline static int32_t get_offset_of_leader_4() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t4177759463, ___leader_4)); }
	inline Text_t356221433 * get_leader_4() const { return ___leader_4; }
	inline Text_t356221433 ** get_address_of_leader_4() { return &___leader_4; }
	inline void set_leader_4(Text_t356221433 * value)
	{
		___leader_4 = value;
		Il2CppCodeGenWriteBarrier(&___leader_4, value);
	}

	inline static int32_t get_offset_of_experience_5() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t4177759463, ___experience_5)); }
	inline Text_t356221433 * get_experience_5() const { return ___experience_5; }
	inline Text_t356221433 ** get_address_of_experience_5() { return &___experience_5; }
	inline void set_experience_5(Text_t356221433 * value)
	{
		___experience_5 = value;
		Il2CppCodeGenWriteBarrier(&___experience_5, value);
	}

	inline static int32_t get_offset_of_rank_6() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t4177759463, ___rank_6)); }
	inline Text_t356221433 * get_rank_6() const { return ___rank_6; }
	inline Text_t356221433 ** get_address_of_rank_6() { return &___rank_6; }
	inline void set_rank_6(Text_t356221433 * value)
	{
		___rank_6 = value;
		Il2CppCodeGenWriteBarrier(&___rank_6, value);
	}

	inline static int32_t get_offset_of_members_7() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t4177759463, ___members_7)); }
	inline Text_t356221433 * get_members_7() const { return ___members_7; }
	inline Text_t356221433 ** get_address_of_members_7() { return &___members_7; }
	inline void set_members_7(Text_t356221433 * value)
	{
		___members_7 = value;
		Il2CppCodeGenWriteBarrier(&___members_7, value);
	}

	inline static int32_t get_offset_of_joinBtn_8() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t4177759463, ___joinBtn_8)); }
	inline GameObject_t1756533147 * get_joinBtn_8() const { return ___joinBtn_8; }
	inline GameObject_t1756533147 ** get_address_of_joinBtn_8() { return &___joinBtn_8; }
	inline void set_joinBtn_8(GameObject_t1756533147 * value)
	{
		___joinBtn_8 = value;
		Il2CppCodeGenWriteBarrier(&___joinBtn_8, value);
	}

	inline static int32_t get_offset_of_allianceId_9() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t4177759463, ___allianceId_9)); }
	inline int64_t get_allianceId_9() const { return ___allianceId_9; }
	inline int64_t* get_address_of_allianceId_9() { return &___allianceId_9; }
	inline void set_allianceId_9(int64_t value)
	{
		___allianceId_9 = value;
	}

	inline static int32_t get_offset_of_owner_10() { return static_cast<int32_t>(offsetof(AllianceAlliancesLine_t4177759463, ___owner_10)); }
	inline AllianceAlliancesTableController_t831003683 * get_owner_10() const { return ___owner_10; }
	inline AllianceAlliancesTableController_t831003683 ** get_address_of_owner_10() { return &___owner_10; }
	inline void set_owner_10(AllianceAlliancesTableController_t831003683 * value)
	{
		___owner_10 = value;
		Il2CppCodeGenWriteBarrier(&___owner_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
