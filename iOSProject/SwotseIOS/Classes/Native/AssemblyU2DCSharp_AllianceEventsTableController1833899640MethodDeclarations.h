﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceEventsTableController
struct AllianceEventsTableController_t1833899640;
// SimpleEvent
struct SimpleEvent_t1509017202;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AllianceEventsTableController::.ctor()
extern "C"  void AllianceEventsTableController__ctor_m3013535323 (AllianceEventsTableController_t1833899640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceEventsTableController::add_onGetAllianceEvents(SimpleEvent)
extern "C"  void AllianceEventsTableController_add_onGetAllianceEvents_m3721983816 (AllianceEventsTableController_t1833899640 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceEventsTableController::remove_onGetAllianceEvents(SimpleEvent)
extern "C"  void AllianceEventsTableController_remove_onGetAllianceEvents_m1608093469 (AllianceEventsTableController_t1833899640 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceEventsTableController::OnEnable()
extern "C"  void AllianceEventsTableController_OnEnable_m2617860351 (AllianceEventsTableController_t1833899640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AllianceEventsTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t AllianceEventsTableController_GetNumberOfRowsForTableView_m2084325935 (AllianceEventsTableController_t1833899640 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AllianceEventsTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float AllianceEventsTableController_GetHeightForRowInTableView_m190698007 (AllianceEventsTableController_t1833899640 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell AllianceEventsTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * AllianceEventsTableController_GetCellForRowInTableView_m1491619366 (AllianceEventsTableController_t1833899640 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AllianceEventsTableController::GetAllianceEvents()
extern "C"  Il2CppObject * AllianceEventsTableController_GetAllianceEvents_m1954473353 (AllianceEventsTableController_t1833899640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceEventsTableController::GotAllianceEvents(System.Object,System.String)
extern "C"  void AllianceEventsTableController_GotAllianceEvents_m1298400481 (AllianceEventsTableController_t1833899640 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
