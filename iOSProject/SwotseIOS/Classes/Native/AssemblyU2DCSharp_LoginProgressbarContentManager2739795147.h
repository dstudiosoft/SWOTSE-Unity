﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// LoginManager
struct LoginManager_t973619992;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginProgressbarContentManager
struct  LoginProgressbarContentManager_t2739795147  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform LoginProgressbarContentManager::progressTransform
	RectTransform_t3349966182 * ___progressTransform_2;
	// LoginManager LoginProgressbarContentManager::loginManager
	LoginManager_t973619992 * ___loginManager_3;
	// System.Int64 LoginProgressbarContentManager::loadCount
	int64_t ___loadCount_4;
	// System.Single LoginProgressbarContentManager::step
	float ___step_5;

public:
	inline static int32_t get_offset_of_progressTransform_2() { return static_cast<int32_t>(offsetof(LoginProgressbarContentManager_t2739795147, ___progressTransform_2)); }
	inline RectTransform_t3349966182 * get_progressTransform_2() const { return ___progressTransform_2; }
	inline RectTransform_t3349966182 ** get_address_of_progressTransform_2() { return &___progressTransform_2; }
	inline void set_progressTransform_2(RectTransform_t3349966182 * value)
	{
		___progressTransform_2 = value;
		Il2CppCodeGenWriteBarrier(&___progressTransform_2, value);
	}

	inline static int32_t get_offset_of_loginManager_3() { return static_cast<int32_t>(offsetof(LoginProgressbarContentManager_t2739795147, ___loginManager_3)); }
	inline LoginManager_t973619992 * get_loginManager_3() const { return ___loginManager_3; }
	inline LoginManager_t973619992 ** get_address_of_loginManager_3() { return &___loginManager_3; }
	inline void set_loginManager_3(LoginManager_t973619992 * value)
	{
		___loginManager_3 = value;
		Il2CppCodeGenWriteBarrier(&___loginManager_3, value);
	}

	inline static int32_t get_offset_of_loadCount_4() { return static_cast<int32_t>(offsetof(LoginProgressbarContentManager_t2739795147, ___loadCount_4)); }
	inline int64_t get_loadCount_4() const { return ___loadCount_4; }
	inline int64_t* get_address_of_loadCount_4() { return &___loadCount_4; }
	inline void set_loadCount_4(int64_t value)
	{
		___loadCount_4 = value;
	}

	inline static int32_t get_offset_of_step_5() { return static_cast<int32_t>(offsetof(LoginProgressbarContentManager_t2739795147, ___step_5)); }
	inline float get_step_5() const { return ___step_5; }
	inline float* get_address_of_step_5() { return &___step_5; }
	inline void set_step_5(float value)
	{
		___step_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
