﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FinancialContentChanger
struct FinancialContentChanger_t2753898366;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleEvent1509017202.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void FinancialContentChanger::.ctor()
extern "C"  void FinancialContentChanger__ctor_m3128522015 (FinancialContentChanger_t2753898366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialContentChanger::add_gotOffers(SimpleEvent)
extern "C"  void FinancialContentChanger_add_gotOffers_m2962103372 (FinancialContentChanger_t2753898366 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialContentChanger::remove_gotOffers(SimpleEvent)
extern "C"  void FinancialContentChanger_remove_gotOffers_m1465483303 (FinancialContentChanger_t2753898366 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialContentChanger::add_offerCanceled(SimpleEvent)
extern "C"  void FinancialContentChanger_add_offerCanceled_m2996736320 (FinancialContentChanger_t2753898366 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialContentChanger::remove_offerCanceled(SimpleEvent)
extern "C"  void FinancialContentChanger_remove_offerCanceled_m659925079 (FinancialContentChanger_t2753898366 * __this, SimpleEvent_t1509017202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialContentChanger::Start()
extern "C"  void FinancialContentChanger_Start_m1300920067 (FinancialContentChanger_t2753898366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialContentChanger::FixedUpdate()
extern "C"  void FinancialContentChanger_FixedUpdate_m1550192402 (FinancialContentChanger_t2753898366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialContentChanger::openPanel(System.String)
extern "C"  void FinancialContentChanger_openPanel_m4249207431 (FinancialContentChanger_t2753898366 * __this, String_t* ___panel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FinancialContentChanger::GetFinanceReports()
extern "C"  Il2CppObject * FinancialContentChanger_GetFinanceReports_m2006807452 (FinancialContentChanger_t2753898366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FinancialContentChanger::GetFinanceOffers()
extern "C"  Il2CppObject * FinancialContentChanger_GetFinanceOffers_m2242905424 (FinancialContentChanger_t2753898366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialContentChanger::OnGetOffers(System.Object,System.String)
extern "C"  void FinancialContentChanger_OnGetOffers_m3058123323 (FinancialContentChanger_t2753898366 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FinancialContentChanger::CancelOfferRequest(System.String)
extern "C"  Il2CppObject * FinancialContentChanger_CancelOfferRequest_m2998068370 (FinancialContentChanger_t2753898366 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialContentChanger::OnOfferCancelled(System.Object,System.String)
extern "C"  void FinancialContentChanger_OnOfferCancelled_m3764352401 (FinancialContentChanger_t2753898366 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialContentChanger::CancelOffer(System.String)
extern "C"  void FinancialContentChanger_CancelOffer_m3853635551 (FinancialContentChanger_t2753898366 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
