﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceEventsLine
struct  AllianceEventsLine_t326631842  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text AllianceEventsLine::date
	Text_t356221433 * ___date_2;
	// UnityEngine.UI.Text AllianceEventsLine::remark
	Text_t356221433 * ___remark_3;

public:
	inline static int32_t get_offset_of_date_2() { return static_cast<int32_t>(offsetof(AllianceEventsLine_t326631842, ___date_2)); }
	inline Text_t356221433 * get_date_2() const { return ___date_2; }
	inline Text_t356221433 ** get_address_of_date_2() { return &___date_2; }
	inline void set_date_2(Text_t356221433 * value)
	{
		___date_2 = value;
		Il2CppCodeGenWriteBarrier(&___date_2, value);
	}

	inline static int32_t get_offset_of_remark_3() { return static_cast<int32_t>(offsetof(AllianceEventsLine_t326631842, ___remark_3)); }
	inline Text_t356221433 * get_remark_3() const { return ___remark_3; }
	inline Text_t356221433 ** get_address_of_remark_3() { return &___remark_3; }
	inline void set_remark_3(Text_t356221433 * value)
	{
		___remark_3 = value;
		Il2CppCodeGenWriteBarrier(&___remark_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
