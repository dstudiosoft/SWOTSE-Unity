﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Win32TcpStatistics
struct Win32TcpStatistics_t3798324233;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_Win32_MIB_TCP1219941615.h"

// System.Void System.Net.NetworkInformation.Win32TcpStatistics::.ctor(System.Net.NetworkInformation.Win32_MIB_TCPSTATS)
extern "C"  void Win32TcpStatistics__ctor_m3362329620 (Win32TcpStatistics_t3798324233 * __this, Win32_MIB_TCPSTATS_t1219941615  ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32TcpStatistics::get_ConnectionsAccepted()
extern "C"  int64_t Win32TcpStatistics_get_ConnectionsAccepted_m2137159907 (Win32TcpStatistics_t3798324233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32TcpStatistics::get_ConnectionsInitiated()
extern "C"  int64_t Win32TcpStatistics_get_ConnectionsInitiated_m1969900857 (Win32TcpStatistics_t3798324233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32TcpStatistics::get_CumulativeConnections()
extern "C"  int64_t Win32TcpStatistics_get_CumulativeConnections_m1871088111 (Win32TcpStatistics_t3798324233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32TcpStatistics::get_CurrentConnections()
extern "C"  int64_t Win32TcpStatistics_get_CurrentConnections_m309222803 (Win32TcpStatistics_t3798324233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32TcpStatistics::get_ErrorsReceived()
extern "C"  int64_t Win32TcpStatistics_get_ErrorsReceived_m2418666615 (Win32TcpStatistics_t3798324233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32TcpStatistics::get_FailedConnectionAttempts()
extern "C"  int64_t Win32TcpStatistics_get_FailedConnectionAttempts_m2446599438 (Win32TcpStatistics_t3798324233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32TcpStatistics::get_MaximumConnections()
extern "C"  int64_t Win32TcpStatistics_get_MaximumConnections_m3323214426 (Win32TcpStatistics_t3798324233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32TcpStatistics::get_MaximumTransmissionTimeout()
extern "C"  int64_t Win32TcpStatistics_get_MaximumTransmissionTimeout_m2784652984 (Win32TcpStatistics_t3798324233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32TcpStatistics::get_MinimumTransmissionTimeout()
extern "C"  int64_t Win32TcpStatistics_get_MinimumTransmissionTimeout_m3762194034 (Win32TcpStatistics_t3798324233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32TcpStatistics::get_ResetConnections()
extern "C"  int64_t Win32TcpStatistics_get_ResetConnections_m3511670027 (Win32TcpStatistics_t3798324233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32TcpStatistics::get_ResetsSent()
extern "C"  int64_t Win32TcpStatistics_get_ResetsSent_m2469341981 (Win32TcpStatistics_t3798324233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32TcpStatistics::get_SegmentsReceived()
extern "C"  int64_t Win32TcpStatistics_get_SegmentsReceived_m630861108 (Win32TcpStatistics_t3798324233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32TcpStatistics::get_SegmentsResent()
extern "C"  int64_t Win32TcpStatistics_get_SegmentsResent_m2128141932 (Win32TcpStatistics_t3798324233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32TcpStatistics::get_SegmentsSent()
extern "C"  int64_t Win32TcpStatistics_get_SegmentsSent_m3581602607 (Win32TcpStatistics_t3798324233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
