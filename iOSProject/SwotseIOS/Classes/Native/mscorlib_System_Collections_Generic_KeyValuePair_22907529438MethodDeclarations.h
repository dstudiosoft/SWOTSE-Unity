﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2649760902(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2907529438 *, TuioBlob_t2046943414 *, TouchPoint_t959629083 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::get_Key()
#define KeyValuePair_2_get_Key_m4052689373(__this, method) ((  TuioBlob_t2046943414 * (*) (KeyValuePair_2_t2907529438 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1314419075(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2907529438 *, TuioBlob_t2046943414 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::get_Value()
#define KeyValuePair_2_get_Value_m358479986(__this, method) ((  TouchPoint_t959629083 * (*) (KeyValuePair_2_t2907529438 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2445643131(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2907529438 *, TouchPoint_t959629083 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::ToString()
#define KeyValuePair_2_ToString_m3005142089(__this, method) ((  String_t* (*) (KeyValuePair_2_t2907529438 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
