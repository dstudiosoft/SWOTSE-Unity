﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FinanceTableController
struct FinanceTableController_t1083841530;
// Tacticsoft.TableView
struct TableView_t3179510217;
// Tacticsoft.TableViewCell
struct TableViewCell_t1276614623;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView3179510217.h"

// System.Void FinanceTableController::.ctor()
extern "C"  void FinanceTableController__ctor_m2575669485 (FinanceTableController_t1083841530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinanceTableController::Start()
extern "C"  void FinanceTableController_Start_m4020131301 (FinanceTableController_t1083841530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FinanceTableController::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t FinanceTableController_GetNumberOfRowsForTableView_m1926064949 (FinanceTableController_t1083841530 * __this, TableView_t3179510217 * ___tableView0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FinanceTableController::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float FinanceTableController_GetHeightForRowInTableView_m4090043665 (FinanceTableController_t1083841530 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tacticsoft.TableViewCell FinanceTableController::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t1276614623 * FinanceTableController_GetCellForRowInTableView_m2373691920 (FinanceTableController_t1083841530 * __this, TableView_t3179510217 * ___tableView0, int32_t ___row1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
