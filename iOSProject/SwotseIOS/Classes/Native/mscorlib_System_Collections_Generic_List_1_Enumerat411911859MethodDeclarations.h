﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<CityTreasureModel>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3782487606(__this, ___l0, method) ((  void (*) (Enumerator_t411911859 *, List_1_t877182185 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CityTreasureModel>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3832472996(__this, method) ((  void (*) (Enumerator_t411911859 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<CityTreasureModel>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1478657916(__this, method) ((  Il2CppObject * (*) (Enumerator_t411911859 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CityTreasureModel>::Dispose()
#define Enumerator_Dispose_m2112362057(__this, method) ((  void (*) (Enumerator_t411911859 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CityTreasureModel>::VerifyState()
#define Enumerator_VerifyState_m1818596000(__this, method) ((  void (*) (Enumerator_t411911859 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<CityTreasureModel>::MoveNext()
#define Enumerator_MoveNext_m1359177016(__this, method) ((  bool (*) (Enumerator_t411911859 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<CityTreasureModel>::get_Current()
#define Enumerator_get_Current_m737849057(__this, method) ((  CityTreasureModel_t1508061053 * (*) (Enumerator_t411911859 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
