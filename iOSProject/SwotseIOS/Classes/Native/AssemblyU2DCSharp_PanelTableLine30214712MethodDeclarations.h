﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PanelTableLine
struct PanelTableLine_t30214712;
// PanelTableController
struct PanelTableController_t1117991214;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PanelTableController1117991214.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void PanelTableLine::.ctor()
extern "C"  void PanelTableLine__ctor_m2952312389 (PanelTableLine_t30214712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableLine::SetEventId(System.Int64)
extern "C"  void PanelTableLine_SetEventId_m737652530 (PanelTableLine_t30214712 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableLine::SetItemId(System.Int64)
extern "C"  void PanelTableLine_SetItemId_m4075617493 (PanelTableLine_t30214712 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableLine::SetOwner(PanelTableController)
extern "C"  void PanelTableLine_SetOwner_m1106708074 (PanelTableLine_t30214712 * __this, PanelTableController_t1117991214 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableLine::SetActionImage(System.String)
extern "C"  void PanelTableLine_SetActionImage_m624184902 (PanelTableLine_t30214712 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableLine::SetItemImage(System.String)
extern "C"  void PanelTableLine_SetItemImage_m1524989391 (PanelTableLine_t30214712 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableLine::SetItemText(System.String)
extern "C"  void PanelTableLine_SetItemText_m2285711397 (PanelTableLine_t30214712 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableLine::SetInteractable(System.Boolean)
extern "C"  void PanelTableLine_SetInteractable_m1829724032 (PanelTableLine_t30214712 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableLine::SetCancelable(System.Boolean)
extern "C"  void PanelTableLine_SetCancelable_m1737738116 (PanelTableLine_t30214712 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableLine::SetCityBuilding(System.Boolean)
extern "C"  void PanelTableLine_SetCityBuilding_m3626811073 (PanelTableLine_t30214712 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableLine::OpenSpeedUp()
extern "C"  void PanelTableLine_OpenSpeedUp_m658035979 (PanelTableLine_t30214712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableLine::CancelAction()
extern "C"  void PanelTableLine_CancelAction_m2956149377 (PanelTableLine_t30214712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableLine::ActionCancelled(System.Object,System.String)
extern "C"  void PanelTableLine_ActionCancelled_m1798871586 (PanelTableLine_t30214712 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableLine::SetTimer(System.DateTime,System.DateTime)
extern "C"  void PanelTableLine_SetTimer_m1899502492 (PanelTableLine_t30214712 * __this, DateTime_t693205669  ___start0, DateTime_t693205669  ___finish1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelTableLine::FixedUpdate()
extern "C"  void PanelTableLine_FixedUpdate_m1139939344 (PanelTableLine_t30214712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
