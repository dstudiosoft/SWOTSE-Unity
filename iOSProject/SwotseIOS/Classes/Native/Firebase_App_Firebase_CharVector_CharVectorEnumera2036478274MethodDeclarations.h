﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.CharVector/CharVectorEnumerator
struct CharVectorEnumerator_t2036478274;
// Firebase.CharVector
struct CharVector_t100493339;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_CharVector100493339.h"

// System.Void Firebase.CharVector/CharVectorEnumerator::.ctor(Firebase.CharVector)
extern "C"  void CharVectorEnumerator__ctor_m15624803 (CharVectorEnumerator_t2036478274 * __this, CharVector_t100493339 * ___collection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Firebase.CharVector/CharVectorEnumerator::get_Current()
extern "C"  uint8_t CharVectorEnumerator_get_Current_m3663387711 (CharVectorEnumerator_t2036478274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Firebase.CharVector/CharVectorEnumerator::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * CharVectorEnumerator_System_Collections_IEnumerator_get_Current_m3380069119 (CharVectorEnumerator_t2036478274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.CharVector/CharVectorEnumerator::MoveNext()
extern "C"  bool CharVectorEnumerator_MoveNext_m3382221503 (CharVectorEnumerator_t2036478274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector/CharVectorEnumerator::Reset()
extern "C"  void CharVectorEnumerator_Reset_m3283971310 (CharVectorEnumerator_t2036478274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.CharVector/CharVectorEnumerator::Dispose()
extern "C"  void CharVectorEnumerator_Dispose_m2062463788 (CharVectorEnumerator_t2036478274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
