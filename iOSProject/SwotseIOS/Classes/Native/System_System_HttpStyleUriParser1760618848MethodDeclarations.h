﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.HttpStyleUriParser
struct HttpStyleUriParser_t1760618848;

#include "codegen/il2cpp-codegen.h"

// System.Void System.HttpStyleUriParser::.ctor()
extern "C"  void HttpStyleUriParser__ctor_m718473207 (HttpStyleUriParser_t1760618848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
