﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// Tacticsoft.Examples.DynamicHeightCell/CellHeightChangedEvent
struct CellHeightChangedEvent_t3730749755;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.Examples.DynamicHeightCell
struct  DynamicHeightCell_t1164024910  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text Tacticsoft.Examples.DynamicHeightCell::m_rowNumberText
	Text_t356221433 * ___m_rowNumberText_2;
	// UnityEngine.UI.Slider Tacticsoft.Examples.DynamicHeightCell::m_cellHeightSlider
	Slider_t297367283 * ___m_cellHeightSlider_3;
	// Tacticsoft.Examples.DynamicHeightCell/CellHeightChangedEvent Tacticsoft.Examples.DynamicHeightCell::onCellHeightChanged
	CellHeightChangedEvent_t3730749755 * ___onCellHeightChanged_4;
	// System.Int32 Tacticsoft.Examples.DynamicHeightCell::<rowNumber>k__BackingField
	int32_t ___U3CrowNumberU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_m_rowNumberText_2() { return static_cast<int32_t>(offsetof(DynamicHeightCell_t1164024910, ___m_rowNumberText_2)); }
	inline Text_t356221433 * get_m_rowNumberText_2() const { return ___m_rowNumberText_2; }
	inline Text_t356221433 ** get_address_of_m_rowNumberText_2() { return &___m_rowNumberText_2; }
	inline void set_m_rowNumberText_2(Text_t356221433 * value)
	{
		___m_rowNumberText_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_rowNumberText_2, value);
	}

	inline static int32_t get_offset_of_m_cellHeightSlider_3() { return static_cast<int32_t>(offsetof(DynamicHeightCell_t1164024910, ___m_cellHeightSlider_3)); }
	inline Slider_t297367283 * get_m_cellHeightSlider_3() const { return ___m_cellHeightSlider_3; }
	inline Slider_t297367283 ** get_address_of_m_cellHeightSlider_3() { return &___m_cellHeightSlider_3; }
	inline void set_m_cellHeightSlider_3(Slider_t297367283 * value)
	{
		___m_cellHeightSlider_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellHeightSlider_3, value);
	}

	inline static int32_t get_offset_of_onCellHeightChanged_4() { return static_cast<int32_t>(offsetof(DynamicHeightCell_t1164024910, ___onCellHeightChanged_4)); }
	inline CellHeightChangedEvent_t3730749755 * get_onCellHeightChanged_4() const { return ___onCellHeightChanged_4; }
	inline CellHeightChangedEvent_t3730749755 ** get_address_of_onCellHeightChanged_4() { return &___onCellHeightChanged_4; }
	inline void set_onCellHeightChanged_4(CellHeightChangedEvent_t3730749755 * value)
	{
		___onCellHeightChanged_4 = value;
		Il2CppCodeGenWriteBarrier(&___onCellHeightChanged_4, value);
	}

	inline static int32_t get_offset_of_U3CrowNumberU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DynamicHeightCell_t1164024910, ___U3CrowNumberU3Ek__BackingField_5)); }
	inline int32_t get_U3CrowNumberU3Ek__BackingField_5() const { return ___U3CrowNumberU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CrowNumberU3Ek__BackingField_5() { return &___U3CrowNumberU3Ek__BackingField_5; }
	inline void set_U3CrowNumberU3Ek__BackingField_5(int32_t value)
	{
		___U3CrowNumberU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
