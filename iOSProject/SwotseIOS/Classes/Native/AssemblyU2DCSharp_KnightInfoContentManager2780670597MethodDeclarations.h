﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// KnightInfoContentManager
struct KnightInfoContentManager_t2780670597;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void KnightInfoContentManager::.ctor()
extern "C"  void KnightInfoContentManager__ctor_m3493529264 (KnightInfoContentManager_t2780670597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KnightInfoContentManager::OnEnable()
extern "C"  void KnightInfoContentManager_OnEnable_m3981927984 (KnightInfoContentManager_t2780670597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KnightInfoContentManager::IncreaseMarches()
extern "C"  void KnightInfoContentManager_IncreaseMarches_m338505259 (KnightInfoContentManager_t2780670597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KnightInfoContentManager::IncreasePolitics()
extern "C"  void KnightInfoContentManager_IncreasePolitics_m1682916303 (KnightInfoContentManager_t2780670597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KnightInfoContentManager::IncreaseSpeed()
extern "C"  void KnightInfoContentManager_IncreaseSpeed_m2635009999 (KnightInfoContentManager_t2780670597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KnightInfoContentManager::IncreaseSalary()
extern "C"  void KnightInfoContentManager_IncreaseSalary_m3849861924 (KnightInfoContentManager_t2780670597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KnightInfoContentManager::IncreaseLoyalty()
extern "C"  void KnightInfoContentManager_IncreaseLoyalty_m1992159950 (KnightInfoContentManager_t2780670597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KnightInfoContentManager::Save()
extern "C"  void KnightInfoContentManager_Save_m2412062241 (KnightInfoContentManager_t2780670597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void KnightInfoContentManager::KnightUpgraded(System.Object,System.String)
extern "C"  void KnightInfoContentManager_KnightUpgraded_m202746511 (KnightInfoContentManager_t2780670597 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
