﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LanguageChangerContentManager
struct  LanguageChangerContentManager_t3463970044  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Dropdown LanguageChangerContentManager::language
	Dropdown_t1985816271 * ___language_2;
	// System.Int32 LanguageChangerContentManager::previousValue
	int32_t ___previousValue_3;

public:
	inline static int32_t get_offset_of_language_2() { return static_cast<int32_t>(offsetof(LanguageChangerContentManager_t3463970044, ___language_2)); }
	inline Dropdown_t1985816271 * get_language_2() const { return ___language_2; }
	inline Dropdown_t1985816271 ** get_address_of_language_2() { return &___language_2; }
	inline void set_language_2(Dropdown_t1985816271 * value)
	{
		___language_2 = value;
		Il2CppCodeGenWriteBarrier(&___language_2, value);
	}

	inline static int32_t get_offset_of_previousValue_3() { return static_cast<int32_t>(offsetof(LanguageChangerContentManager_t3463970044, ___previousValue_3)); }
	inline int32_t get_previousValue_3() const { return ___previousValue_3; }
	inline int32_t* get_address_of_previousValue_3() { return &___previousValue_3; }
	inline void set_previousValue_3(int32_t value)
	{
		___previousValue_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
