﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialPageModel
struct TutorialPageModel_t2508244306;

#include "codegen/il2cpp-codegen.h"

// System.Void TutorialPageModel::.ctor()
extern "C"  void TutorialPageModel__ctor_m3000587083 (TutorialPageModel_t2508244306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
