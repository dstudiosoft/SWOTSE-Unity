﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSCsharp.Net.OscPacketReceivedEventArgs
struct OscPacketReceivedEventArgs_t3282045269;
// OSCsharp.Data.OscPacket
struct OscPacket_t504761797;

#include "codegen/il2cpp-codegen.h"
#include "OSCsharp_OSCsharp_Data_OscPacket504761797.h"

// System.Void OSCsharp.Net.OscPacketReceivedEventArgs::set_Packet(OSCsharp.Data.OscPacket)
extern "C"  void OscPacketReceivedEventArgs_set_Packet_m2983320626 (OscPacketReceivedEventArgs_t3282045269 * __this, OscPacket_t504761797 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.OscPacketReceivedEventArgs::.ctor(OSCsharp.Data.OscPacket)
extern "C"  void OscPacketReceivedEventArgs__ctor_m3190781335 (OscPacketReceivedEventArgs_t3282045269 * __this, OscPacket_t504761797 * ___packet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
