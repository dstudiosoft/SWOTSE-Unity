﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m976411660(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2175241622 *, Dictionary_2_t855216920 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m253848081(__this, method) ((  Il2CppObject * (*) (Enumerator_t2175241622 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3937436493(__this, method) ((  void (*) (Enumerator_t2175241622 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2138925064(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2175241622 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1471781091(__this, method) ((  Il2CppObject * (*) (Enumerator_t2175241622 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1258377779(__this, method) ((  Il2CppObject * (*) (Enumerator_t2175241622 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::MoveNext()
#define Enumerator_MoveNext_m2706634366(__this, method) ((  bool (*) (Enumerator_t2175241622 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::get_Current()
#define Enumerator_get_Current_m3710855063(__this, method) ((  KeyValuePair_2_t2907529438  (*) (Enumerator_t2175241622 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2453459812(__this, method) ((  TuioBlob_t2046943414 * (*) (Enumerator_t2175241622 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1882477476(__this, method) ((  TouchPoint_t959629083 * (*) (Enumerator_t2175241622 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::Reset()
#define Enumerator_Reset_m1160127594(__this, method) ((  void (*) (Enumerator_t2175241622 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::VerifyState()
#define Enumerator_VerifyState_m26284695(__this, method) ((  void (*) (Enumerator_t2175241622 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m222352533(__this, method) ((  void (*) (Enumerator_t2175241622 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>::Dispose()
#define Enumerator_Dispose_m2417487724(__this, method) ((  void (*) (Enumerator_t2175241622 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
