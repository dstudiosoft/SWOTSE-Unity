﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSCsharp.Net.UDPReceiver
struct UDPReceiver_t3846109956;
// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>
struct EventHandler_1_t3778988004;
// System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>
struct EventHandler_1_t4149316328;
// System.Net.IPAddress
struct IPAddress_t1399971723;
// System.Net.IPEndPoint
struct IPEndPoint_t2615413766;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// OSCsharp.Data.OscPacket
struct OscPacket_t504761797;
// OSCsharp.Data.OscBundle
struct OscBundle_t1126010605;
// OSCsharp.Data.OscMessage
struct OscMessage_t2764280154;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_IPAddress1399971723.h"
#include "System_System_Net_IPEndPoint2615413766.h"
#include "OSCsharp_OSCsharp_Net_TransmissionType529366678.h"
#include "OSCsharp_OSCsharp_Data_OscPacket504761797.h"
#include "OSCsharp_OSCsharp_Data_OscBundle1126010605.h"
#include "OSCsharp_OSCsharp_Data_OscMessage2764280154.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Void OSCsharp.Net.UDPReceiver::add_ErrorOccured(System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>)
extern "C"  void UDPReceiver_add_ErrorOccured_m1212653731 (UDPReceiver_t3846109956 * __this, EventHandler_1_t3778988004 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::remove_ErrorOccured(System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>)
extern "C"  void UDPReceiver_remove_ErrorOccured_m1224019382 (UDPReceiver_t3846109956 * __this, EventHandler_1_t3778988004 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::add_MessageReceived(System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>)
extern "C"  void UDPReceiver_add_MessageReceived_m3470532992 (UDPReceiver_t3846109956 * __this, EventHandler_1_t4149316328 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::remove_MessageReceived(System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>)
extern "C"  void UDPReceiver_remove_MessageReceived_m3894025529 (UDPReceiver_t3846109956 * __this, EventHandler_1_t4149316328 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress OSCsharp.Net.UDPReceiver::get_IPAddress()
extern "C"  IPAddress_t1399971723 * UDPReceiver_get_IPAddress_m257810772 (UDPReceiver_t3846109956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_IPAddress(System.Net.IPAddress)
extern "C"  void UDPReceiver_set_IPAddress_m3792884775 (UDPReceiver_t3846109956 * __this, IPAddress_t1399971723 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 OSCsharp.Net.UDPReceiver::get_Port()
extern "C"  int32_t UDPReceiver_get_Port_m3797322946 (UDPReceiver_t3846109956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_Port(System.Int32)
extern "C"  void UDPReceiver_set_Port_m2491900965 (UDPReceiver_t3846109956 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress OSCsharp.Net.UDPReceiver::get_MulticastAddress()
extern "C"  IPAddress_t1399971723 * UDPReceiver_get_MulticastAddress_m3372256997 (UDPReceiver_t3846109956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_MulticastAddress(System.Net.IPAddress)
extern "C"  void UDPReceiver_set_MulticastAddress_m988773996 (UDPReceiver_t3846109956 * __this, IPAddress_t1399971723 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver::get_IPEndPoint()
extern "C"  IPEndPoint_t2615413766 * UDPReceiver_get_IPEndPoint_m1093884654 (UDPReceiver_t3846109956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_IPEndPoint(System.Net.IPEndPoint)
extern "C"  void UDPReceiver_set_IPEndPoint_m4018663211 (UDPReceiver_t3846109956 * __this, IPEndPoint_t2615413766 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OSCsharp.Net.TransmissionType OSCsharp.Net.UDPReceiver::get_TransmissionType()
extern "C"  int32_t UDPReceiver_get_TransmissionType_m4100389850 (UDPReceiver_t3846109956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_TransmissionType(OSCsharp.Net.TransmissionType)
extern "C"  void UDPReceiver_set_TransmissionType_m1709185389 (UDPReceiver_t3846109956 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSCsharp.Net.UDPReceiver::get_ConsumeParsingExceptions()
extern "C"  bool UDPReceiver_get_ConsumeParsingExceptions_m2647385705 (UDPReceiver_t3846109956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_ConsumeParsingExceptions(System.Boolean)
extern "C"  void UDPReceiver_set_ConsumeParsingExceptions_m2781480188 (UDPReceiver_t3846109956 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSCsharp.Net.UDPReceiver::get_IsRunning()
extern "C"  bool UDPReceiver_get_IsRunning_m2474408206 (UDPReceiver_t3846109956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::.ctor(System.Int32,System.Boolean)
extern "C"  void UDPReceiver__ctor_m3124846498 (UDPReceiver_t3846109956 * __this, int32_t ___port0, bool ___consumeParsingExceptions1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::.ctor(System.Net.IPAddress,System.Int32,System.Boolean)
extern "C"  void UDPReceiver__ctor_m3514168723 (UDPReceiver_t3846109956 * __this, IPAddress_t1399971723 * ___ipAddress0, int32_t ___port1, bool ___consumeParsingExceptions2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::.ctor(System.Net.IPAddress,System.Int32,OSCsharp.Net.TransmissionType,System.Net.IPAddress,System.Boolean)
extern "C"  void UDPReceiver__ctor_m896685042 (UDPReceiver_t3846109956 * __this, IPAddress_t1399971723 * ___ipAddress0, int32_t ___port1, int32_t ___transmissionType2, IPAddress_t1399971723 * ___multicastAddress3, bool ___consumeParsingExceptions4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::Start()
extern "C"  void UDPReceiver_Start_m1033630334 (UDPReceiver_t3846109956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::Stop()
extern "C"  void UDPReceiver_Stop_m3061802084 (UDPReceiver_t3846109956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::endReceive(System.IAsyncResult)
extern "C"  void UDPReceiver_endReceive_m4073855157 (UDPReceiver_t3846109956 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::parseData(System.Net.IPEndPoint,System.Byte[])
extern "C"  void UDPReceiver_parseData_m902928170 (UDPReceiver_t3846109956 * __this, IPEndPoint_t2615413766 * ___sourceEndPoint0, ByteU5BU5D_t3397334013* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::onPacketReceived(OSCsharp.Data.OscPacket)
extern "C"  void UDPReceiver_onPacketReceived_m3250243160 (UDPReceiver_t3846109956 * __this, OscPacket_t504761797 * ___packet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::onBundleReceived(OSCsharp.Data.OscBundle)
extern "C"  void UDPReceiver_onBundleReceived_m562189912 (UDPReceiver_t3846109956 * __this, OscBundle_t1126010605 * ___bundle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::onMessageReceived(OSCsharp.Data.OscMessage)
extern "C"  void UDPReceiver_onMessageReceived_m24838384 (UDPReceiver_t3846109956 * __this, OscMessage_t2764280154 * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::onError(System.Exception)
extern "C"  void UDPReceiver_onError_m295686389 (UDPReceiver_t3846109956 * __this, Exception_t1927440687 * ___ex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
