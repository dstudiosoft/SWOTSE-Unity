﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ResourcesModel
struct ResourcesModel_t2978985958;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColonyModel
struct  ColonyModel_t2271864985  : public Il2CppObject
{
public:
	// System.Int64 ColonyModel::id
	int64_t ___id_0;
	// System.String ColonyModel::city_name
	String_t* ___city_name_1;
	// System.String ColonyModel::username
	String_t* ___username_2;
	// System.Int64 ColonyModel::posX
	int64_t ___posX_3;
	// System.Int64 ColonyModel::posY
	int64_t ___posY_4;
	// System.String ColonyModel::region
	String_t* ___region_5;
	// System.Int64 ColonyModel::mayer_residence_level
	int64_t ___mayer_residence_level_6;
	// ResourcesModel ColonyModel::production
	ResourcesModel_t2978985958 * ___production_7;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ColonyModel_t2271864985, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_city_name_1() { return static_cast<int32_t>(offsetof(ColonyModel_t2271864985, ___city_name_1)); }
	inline String_t* get_city_name_1() const { return ___city_name_1; }
	inline String_t** get_address_of_city_name_1() { return &___city_name_1; }
	inline void set_city_name_1(String_t* value)
	{
		___city_name_1 = value;
		Il2CppCodeGenWriteBarrier(&___city_name_1, value);
	}

	inline static int32_t get_offset_of_username_2() { return static_cast<int32_t>(offsetof(ColonyModel_t2271864985, ___username_2)); }
	inline String_t* get_username_2() const { return ___username_2; }
	inline String_t** get_address_of_username_2() { return &___username_2; }
	inline void set_username_2(String_t* value)
	{
		___username_2 = value;
		Il2CppCodeGenWriteBarrier(&___username_2, value);
	}

	inline static int32_t get_offset_of_posX_3() { return static_cast<int32_t>(offsetof(ColonyModel_t2271864985, ___posX_3)); }
	inline int64_t get_posX_3() const { return ___posX_3; }
	inline int64_t* get_address_of_posX_3() { return &___posX_3; }
	inline void set_posX_3(int64_t value)
	{
		___posX_3 = value;
	}

	inline static int32_t get_offset_of_posY_4() { return static_cast<int32_t>(offsetof(ColonyModel_t2271864985, ___posY_4)); }
	inline int64_t get_posY_4() const { return ___posY_4; }
	inline int64_t* get_address_of_posY_4() { return &___posY_4; }
	inline void set_posY_4(int64_t value)
	{
		___posY_4 = value;
	}

	inline static int32_t get_offset_of_region_5() { return static_cast<int32_t>(offsetof(ColonyModel_t2271864985, ___region_5)); }
	inline String_t* get_region_5() const { return ___region_5; }
	inline String_t** get_address_of_region_5() { return &___region_5; }
	inline void set_region_5(String_t* value)
	{
		___region_5 = value;
		Il2CppCodeGenWriteBarrier(&___region_5, value);
	}

	inline static int32_t get_offset_of_mayer_residence_level_6() { return static_cast<int32_t>(offsetof(ColonyModel_t2271864985, ___mayer_residence_level_6)); }
	inline int64_t get_mayer_residence_level_6() const { return ___mayer_residence_level_6; }
	inline int64_t* get_address_of_mayer_residence_level_6() { return &___mayer_residence_level_6; }
	inline void set_mayer_residence_level_6(int64_t value)
	{
		___mayer_residence_level_6 = value;
	}

	inline static int32_t get_offset_of_production_7() { return static_cast<int32_t>(offsetof(ColonyModel_t2271864985, ___production_7)); }
	inline ResourcesModel_t2978985958 * get_production_7() const { return ___production_7; }
	inline ResourcesModel_t2978985958 ** get_address_of_production_7() { return &___production_7; }
	inline void set_production_7(ResourcesModel_t2978985958 * value)
	{
		___production_7 = value;
		Il2CppCodeGenWriteBarrier(&___production_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
