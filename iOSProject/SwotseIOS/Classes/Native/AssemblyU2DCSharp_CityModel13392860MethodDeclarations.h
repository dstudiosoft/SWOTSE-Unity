﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CityModel
struct CityModel_t13392860;

#include "codegen/il2cpp-codegen.h"

// System.Void CityModel::.ctor()
extern "C"  void CityModel__ctor_m3531547031 (CityModel_t13392860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
