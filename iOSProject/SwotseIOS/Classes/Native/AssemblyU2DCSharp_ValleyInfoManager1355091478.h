﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// UnityEngine.Sprite
struct Sprite_t309593783;
// WorldFieldModel
struct WorldFieldModel_t3469935653;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ValleyInfoManager
struct  ValleyInfoManager_t1355091478  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ValleyInfoManager::valleyName
	Text_t356221433 * ___valleyName_2;
	// UnityEngine.UI.Text ValleyInfoManager::product
	Text_t356221433 * ___product_3;
	// UnityEngine.UI.Text ValleyInfoManager::coords
	Text_t356221433 * ___coords_4;
	// UnityEngine.UI.Image ValleyInfoManager::valleyImage
	Image_t2042527209 * ___valleyImage_5;
	// UnityEngine.GameObject ValleyInfoManager::marchBtn
	GameObject_t1756533147 * ___marchBtn_6;
	// UnityEngine.GameObject ValleyInfoManager::buildCityBtn
	GameObject_t1756533147 * ___buildCityBtn_7;
	// UnityEngine.GameObject ValleyInfoManager::recruitBtn
	GameObject_t1756533147 * ___recruitBtn_8;
	// UnityEngine.UI.Image[] ValleyInfoManager::tabs
	ImageU5BU5D_t590162004* ___tabs_9;
	// UnityEngine.Sprite ValleyInfoManager::tabSelectedSprite
	Sprite_t309593783 * ___tabSelectedSprite_10;
	// UnityEngine.Sprite ValleyInfoManager::tabUnselectedSprite
	Sprite_t309593783 * ___tabUnselectedSprite_11;
	// WorldFieldModel ValleyInfoManager::field
	WorldFieldModel_t3469935653 * ___field_12;

public:
	inline static int32_t get_offset_of_valleyName_2() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t1355091478, ___valleyName_2)); }
	inline Text_t356221433 * get_valleyName_2() const { return ___valleyName_2; }
	inline Text_t356221433 ** get_address_of_valleyName_2() { return &___valleyName_2; }
	inline void set_valleyName_2(Text_t356221433 * value)
	{
		___valleyName_2 = value;
		Il2CppCodeGenWriteBarrier(&___valleyName_2, value);
	}

	inline static int32_t get_offset_of_product_3() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t1355091478, ___product_3)); }
	inline Text_t356221433 * get_product_3() const { return ___product_3; }
	inline Text_t356221433 ** get_address_of_product_3() { return &___product_3; }
	inline void set_product_3(Text_t356221433 * value)
	{
		___product_3 = value;
		Il2CppCodeGenWriteBarrier(&___product_3, value);
	}

	inline static int32_t get_offset_of_coords_4() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t1355091478, ___coords_4)); }
	inline Text_t356221433 * get_coords_4() const { return ___coords_4; }
	inline Text_t356221433 ** get_address_of_coords_4() { return &___coords_4; }
	inline void set_coords_4(Text_t356221433 * value)
	{
		___coords_4 = value;
		Il2CppCodeGenWriteBarrier(&___coords_4, value);
	}

	inline static int32_t get_offset_of_valleyImage_5() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t1355091478, ___valleyImage_5)); }
	inline Image_t2042527209 * get_valleyImage_5() const { return ___valleyImage_5; }
	inline Image_t2042527209 ** get_address_of_valleyImage_5() { return &___valleyImage_5; }
	inline void set_valleyImage_5(Image_t2042527209 * value)
	{
		___valleyImage_5 = value;
		Il2CppCodeGenWriteBarrier(&___valleyImage_5, value);
	}

	inline static int32_t get_offset_of_marchBtn_6() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t1355091478, ___marchBtn_6)); }
	inline GameObject_t1756533147 * get_marchBtn_6() const { return ___marchBtn_6; }
	inline GameObject_t1756533147 ** get_address_of_marchBtn_6() { return &___marchBtn_6; }
	inline void set_marchBtn_6(GameObject_t1756533147 * value)
	{
		___marchBtn_6 = value;
		Il2CppCodeGenWriteBarrier(&___marchBtn_6, value);
	}

	inline static int32_t get_offset_of_buildCityBtn_7() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t1355091478, ___buildCityBtn_7)); }
	inline GameObject_t1756533147 * get_buildCityBtn_7() const { return ___buildCityBtn_7; }
	inline GameObject_t1756533147 ** get_address_of_buildCityBtn_7() { return &___buildCityBtn_7; }
	inline void set_buildCityBtn_7(GameObject_t1756533147 * value)
	{
		___buildCityBtn_7 = value;
		Il2CppCodeGenWriteBarrier(&___buildCityBtn_7, value);
	}

	inline static int32_t get_offset_of_recruitBtn_8() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t1355091478, ___recruitBtn_8)); }
	inline GameObject_t1756533147 * get_recruitBtn_8() const { return ___recruitBtn_8; }
	inline GameObject_t1756533147 ** get_address_of_recruitBtn_8() { return &___recruitBtn_8; }
	inline void set_recruitBtn_8(GameObject_t1756533147 * value)
	{
		___recruitBtn_8 = value;
		Il2CppCodeGenWriteBarrier(&___recruitBtn_8, value);
	}

	inline static int32_t get_offset_of_tabs_9() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t1355091478, ___tabs_9)); }
	inline ImageU5BU5D_t590162004* get_tabs_9() const { return ___tabs_9; }
	inline ImageU5BU5D_t590162004** get_address_of_tabs_9() { return &___tabs_9; }
	inline void set_tabs_9(ImageU5BU5D_t590162004* value)
	{
		___tabs_9 = value;
		Il2CppCodeGenWriteBarrier(&___tabs_9, value);
	}

	inline static int32_t get_offset_of_tabSelectedSprite_10() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t1355091478, ___tabSelectedSprite_10)); }
	inline Sprite_t309593783 * get_tabSelectedSprite_10() const { return ___tabSelectedSprite_10; }
	inline Sprite_t309593783 ** get_address_of_tabSelectedSprite_10() { return &___tabSelectedSprite_10; }
	inline void set_tabSelectedSprite_10(Sprite_t309593783 * value)
	{
		___tabSelectedSprite_10 = value;
		Il2CppCodeGenWriteBarrier(&___tabSelectedSprite_10, value);
	}

	inline static int32_t get_offset_of_tabUnselectedSprite_11() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t1355091478, ___tabUnselectedSprite_11)); }
	inline Sprite_t309593783 * get_tabUnselectedSprite_11() const { return ___tabUnselectedSprite_11; }
	inline Sprite_t309593783 ** get_address_of_tabUnselectedSprite_11() { return &___tabUnselectedSprite_11; }
	inline void set_tabUnselectedSprite_11(Sprite_t309593783 * value)
	{
		___tabUnselectedSprite_11 = value;
		Il2CppCodeGenWriteBarrier(&___tabUnselectedSprite_11, value);
	}

	inline static int32_t get_offset_of_field_12() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t1355091478, ___field_12)); }
	inline WorldFieldModel_t3469935653 * get_field_12() const { return ___field_12; }
	inline WorldFieldModel_t3469935653 ** get_address_of_field_12() { return &___field_12; }
	inline void set_field_12(WorldFieldModel_t3469935653 * value)
	{
		___field_12 = value;
		Il2CppCodeGenWriteBarrier(&___field_12, value);
	}
};

struct ValleyInfoManager_t1355091478_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ValleyInfoManager::<>f__switch$map2C
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map2C_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2C_13() { return static_cast<int32_t>(offsetof(ValleyInfoManager_t1355091478_StaticFields, ___U3CU3Ef__switchU24map2C_13)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map2C_13() const { return ___U3CU3Ef__switchU24map2C_13; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map2C_13() { return &___U3CU3Ef__switchU24map2C_13; }
	inline void set_U3CU3Ef__switchU24map2C_13(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map2C_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2C_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
