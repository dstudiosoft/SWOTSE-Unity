﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Win32_MIBICMPINFO
struct Win32_MIBICMPINFO_t3825151335;
struct Win32_MIBICMPINFO_t3825151335_marshaled_pinvoke;
struct Win32_MIBICMPINFO_t3825151335_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct Win32_MIBICMPINFO_t3825151335;
struct Win32_MIBICMPINFO_t3825151335_marshaled_pinvoke;

extern "C" void Win32_MIBICMPINFO_t3825151335_marshal_pinvoke(const Win32_MIBICMPINFO_t3825151335& unmarshaled, Win32_MIBICMPINFO_t3825151335_marshaled_pinvoke& marshaled);
extern "C" void Win32_MIBICMPINFO_t3825151335_marshal_pinvoke_back(const Win32_MIBICMPINFO_t3825151335_marshaled_pinvoke& marshaled, Win32_MIBICMPINFO_t3825151335& unmarshaled);
extern "C" void Win32_MIBICMPINFO_t3825151335_marshal_pinvoke_cleanup(Win32_MIBICMPINFO_t3825151335_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Win32_MIBICMPINFO_t3825151335;
struct Win32_MIBICMPINFO_t3825151335_marshaled_com;

extern "C" void Win32_MIBICMPINFO_t3825151335_marshal_com(const Win32_MIBICMPINFO_t3825151335& unmarshaled, Win32_MIBICMPINFO_t3825151335_marshaled_com& marshaled);
extern "C" void Win32_MIBICMPINFO_t3825151335_marshal_com_back(const Win32_MIBICMPINFO_t3825151335_marshaled_com& marshaled, Win32_MIBICMPINFO_t3825151335& unmarshaled);
extern "C" void Win32_MIBICMPINFO_t3825151335_marshal_com_cleanup(Win32_MIBICMPINFO_t3825151335_marshaled_com& marshaled);
