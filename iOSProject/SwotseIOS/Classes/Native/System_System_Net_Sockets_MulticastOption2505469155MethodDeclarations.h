﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Sockets.MulticastOption
struct MulticastOption_t2505469155;
// System.Net.IPAddress
struct IPAddress_t1399971723;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_IPAddress1399971723.h"

// System.Void System.Net.Sockets.MulticastOption::.ctor(System.Net.IPAddress)
extern "C"  void MulticastOption__ctor_m35383266 (MulticastOption_t2505469155 * __this, IPAddress_t1399971723 * ___group0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.MulticastOption::.ctor(System.Net.IPAddress,System.Int32)
extern "C"  void MulticastOption__ctor_m3371161391 (MulticastOption_t2505469155 * __this, IPAddress_t1399971723 * ___group0, int32_t ___interfaceIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.MulticastOption::.ctor(System.Net.IPAddress,System.Net.IPAddress)
extern "C"  void MulticastOption__ctor_m1873360103 (MulticastOption_t2505469155 * __this, IPAddress_t1399971723 * ___group0, IPAddress_t1399971723 * ___mcint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress System.Net.Sockets.MulticastOption::get_Group()
extern "C"  IPAddress_t1399971723 * MulticastOption_get_Group_m645653699 (MulticastOption_t2505469155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.MulticastOption::set_Group(System.Net.IPAddress)
extern "C"  void MulticastOption_set_Group_m3775943942 (MulticastOption_t2505469155 * __this, IPAddress_t1399971723 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress System.Net.Sockets.MulticastOption::get_LocalAddress()
extern "C"  IPAddress_t1399971723 * MulticastOption_get_LocalAddress_m572840675 (MulticastOption_t2505469155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.MulticastOption::set_LocalAddress(System.Net.IPAddress)
extern "C"  void MulticastOption_set_LocalAddress_m3085474606 (MulticastOption_t2505469155 * __this, IPAddress_t1399971723 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.Sockets.MulticastOption::get_InterfaceIndex()
extern "C"  int32_t MulticastOption_get_InterfaceIndex_m4089411363 (MulticastOption_t2505469155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.MulticastOption::set_InterfaceIndex(System.Int32)
extern "C"  void MulticastOption_set_InterfaceIndex_m4283866348 (MulticastOption_t2505469155 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
