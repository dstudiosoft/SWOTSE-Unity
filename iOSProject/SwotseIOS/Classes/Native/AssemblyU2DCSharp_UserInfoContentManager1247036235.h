﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;
// UserModel
struct UserModel_t3025569216;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserInfoContentManager
struct  UserInfoContentManager_t1247036235  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject UserInfoContentManager::currentUserPanel
	GameObject_t1756533147 * ___currentUserPanel_2;
	// UnityEngine.GameObject UserInfoContentManager::otherUserPanel
	GameObject_t1756533147 * ___otherUserPanel_3;
	// UnityEngine.GameObject UserInfoContentManager::otherPlayerActions
	GameObject_t1756533147 * ___otherPlayerActions_4;
	// UnityEngine.GameObject UserInfoContentManager::allianceMemberActions
	GameObject_t1756533147 * ___allianceMemberActions_5;
	// UnityEngine.UI.InputField UserInfoContentManager::currentUserNick
	InputField_t1631627530 * ___currentUserNick_6;
	// UnityEngine.UI.InputField UserInfoContentManager::currentUserEmail
	InputField_t1631627530 * ___currentUserEmail_7;
	// UnityEngine.UI.InputField UserInfoContentManager::currentUserMobile
	InputField_t1631627530 * ___currentUserMobile_8;
	// UnityEngine.UI.InputField UserInfoContentManager::currentUserFlag
	InputField_t1631627530 * ___currentUserFlag_9;
	// UnityEngine.UI.Image UserInfoContentManager::currentUserImage
	Image_t2042527209 * ___currentUserImage_10;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserNick
	Text_t356221433 * ___otherUserNick_11;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserExperience
	Text_t356221433 * ___otherUserExperience_12;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserRank
	Text_t356221433 * ___otherUserRank_13;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserCities
	Text_t356221433 * ___otherUserCities_14;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserCapital
	Text_t356221433 * ___otherUserCapital_15;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserCapitalCoords
	Text_t356221433 * ___otherUserCapitalCoords_16;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserLastOnline
	Text_t356221433 * ___otherUserLastOnline_17;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserFlag
	Text_t356221433 * ___otherUserFlag_18;
	// UnityEngine.UI.Image UserInfoContentManager::otherUserImage
	Image_t2042527209 * ___otherUserImage_19;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserPosition
	Text_t356221433 * ___otherUserPosition_20;
	// UnityEngine.UI.Text UserInfoContentManager::otherUserAlliance
	Text_t356221433 * ___otherUserAlliance_21;
	// UnityEngine.UI.Dropdown UserInfoContentManager::allianceMemberPosition
	Dropdown_t1985816271 * ___allianceMemberPosition_22;
	// UnityEngine.GameObject UserInfoContentManager::changeBtn
	GameObject_t1756533147 * ___changeBtn_23;
	// UnityEngine.GameObject UserInfoContentManager::excludeBtn
	GameObject_t1756533147 * ___excludeBtn_24;
	// UserModel UserInfoContentManager::user
	UserModel_t3025569216 * ___user_25;
	// System.Int64 UserInfoContentManager::iconIndex
	int64_t ___iconIndex_26;
	// System.String[] UserInfoContentManager::ranks
	StringU5BU5D_t1642385972* ___ranks_27;

public:
	inline static int32_t get_offset_of_currentUserPanel_2() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___currentUserPanel_2)); }
	inline GameObject_t1756533147 * get_currentUserPanel_2() const { return ___currentUserPanel_2; }
	inline GameObject_t1756533147 ** get_address_of_currentUserPanel_2() { return &___currentUserPanel_2; }
	inline void set_currentUserPanel_2(GameObject_t1756533147 * value)
	{
		___currentUserPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___currentUserPanel_2, value);
	}

	inline static int32_t get_offset_of_otherUserPanel_3() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___otherUserPanel_3)); }
	inline GameObject_t1756533147 * get_otherUserPanel_3() const { return ___otherUserPanel_3; }
	inline GameObject_t1756533147 ** get_address_of_otherUserPanel_3() { return &___otherUserPanel_3; }
	inline void set_otherUserPanel_3(GameObject_t1756533147 * value)
	{
		___otherUserPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___otherUserPanel_3, value);
	}

	inline static int32_t get_offset_of_otherPlayerActions_4() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___otherPlayerActions_4)); }
	inline GameObject_t1756533147 * get_otherPlayerActions_4() const { return ___otherPlayerActions_4; }
	inline GameObject_t1756533147 ** get_address_of_otherPlayerActions_4() { return &___otherPlayerActions_4; }
	inline void set_otherPlayerActions_4(GameObject_t1756533147 * value)
	{
		___otherPlayerActions_4 = value;
		Il2CppCodeGenWriteBarrier(&___otherPlayerActions_4, value);
	}

	inline static int32_t get_offset_of_allianceMemberActions_5() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___allianceMemberActions_5)); }
	inline GameObject_t1756533147 * get_allianceMemberActions_5() const { return ___allianceMemberActions_5; }
	inline GameObject_t1756533147 ** get_address_of_allianceMemberActions_5() { return &___allianceMemberActions_5; }
	inline void set_allianceMemberActions_5(GameObject_t1756533147 * value)
	{
		___allianceMemberActions_5 = value;
		Il2CppCodeGenWriteBarrier(&___allianceMemberActions_5, value);
	}

	inline static int32_t get_offset_of_currentUserNick_6() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___currentUserNick_6)); }
	inline InputField_t1631627530 * get_currentUserNick_6() const { return ___currentUserNick_6; }
	inline InputField_t1631627530 ** get_address_of_currentUserNick_6() { return &___currentUserNick_6; }
	inline void set_currentUserNick_6(InputField_t1631627530 * value)
	{
		___currentUserNick_6 = value;
		Il2CppCodeGenWriteBarrier(&___currentUserNick_6, value);
	}

	inline static int32_t get_offset_of_currentUserEmail_7() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___currentUserEmail_7)); }
	inline InputField_t1631627530 * get_currentUserEmail_7() const { return ___currentUserEmail_7; }
	inline InputField_t1631627530 ** get_address_of_currentUserEmail_7() { return &___currentUserEmail_7; }
	inline void set_currentUserEmail_7(InputField_t1631627530 * value)
	{
		___currentUserEmail_7 = value;
		Il2CppCodeGenWriteBarrier(&___currentUserEmail_7, value);
	}

	inline static int32_t get_offset_of_currentUserMobile_8() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___currentUserMobile_8)); }
	inline InputField_t1631627530 * get_currentUserMobile_8() const { return ___currentUserMobile_8; }
	inline InputField_t1631627530 ** get_address_of_currentUserMobile_8() { return &___currentUserMobile_8; }
	inline void set_currentUserMobile_8(InputField_t1631627530 * value)
	{
		___currentUserMobile_8 = value;
		Il2CppCodeGenWriteBarrier(&___currentUserMobile_8, value);
	}

	inline static int32_t get_offset_of_currentUserFlag_9() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___currentUserFlag_9)); }
	inline InputField_t1631627530 * get_currentUserFlag_9() const { return ___currentUserFlag_9; }
	inline InputField_t1631627530 ** get_address_of_currentUserFlag_9() { return &___currentUserFlag_9; }
	inline void set_currentUserFlag_9(InputField_t1631627530 * value)
	{
		___currentUserFlag_9 = value;
		Il2CppCodeGenWriteBarrier(&___currentUserFlag_9, value);
	}

	inline static int32_t get_offset_of_currentUserImage_10() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___currentUserImage_10)); }
	inline Image_t2042527209 * get_currentUserImage_10() const { return ___currentUserImage_10; }
	inline Image_t2042527209 ** get_address_of_currentUserImage_10() { return &___currentUserImage_10; }
	inline void set_currentUserImage_10(Image_t2042527209 * value)
	{
		___currentUserImage_10 = value;
		Il2CppCodeGenWriteBarrier(&___currentUserImage_10, value);
	}

	inline static int32_t get_offset_of_otherUserNick_11() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___otherUserNick_11)); }
	inline Text_t356221433 * get_otherUserNick_11() const { return ___otherUserNick_11; }
	inline Text_t356221433 ** get_address_of_otherUserNick_11() { return &___otherUserNick_11; }
	inline void set_otherUserNick_11(Text_t356221433 * value)
	{
		___otherUserNick_11 = value;
		Il2CppCodeGenWriteBarrier(&___otherUserNick_11, value);
	}

	inline static int32_t get_offset_of_otherUserExperience_12() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___otherUserExperience_12)); }
	inline Text_t356221433 * get_otherUserExperience_12() const { return ___otherUserExperience_12; }
	inline Text_t356221433 ** get_address_of_otherUserExperience_12() { return &___otherUserExperience_12; }
	inline void set_otherUserExperience_12(Text_t356221433 * value)
	{
		___otherUserExperience_12 = value;
		Il2CppCodeGenWriteBarrier(&___otherUserExperience_12, value);
	}

	inline static int32_t get_offset_of_otherUserRank_13() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___otherUserRank_13)); }
	inline Text_t356221433 * get_otherUserRank_13() const { return ___otherUserRank_13; }
	inline Text_t356221433 ** get_address_of_otherUserRank_13() { return &___otherUserRank_13; }
	inline void set_otherUserRank_13(Text_t356221433 * value)
	{
		___otherUserRank_13 = value;
		Il2CppCodeGenWriteBarrier(&___otherUserRank_13, value);
	}

	inline static int32_t get_offset_of_otherUserCities_14() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___otherUserCities_14)); }
	inline Text_t356221433 * get_otherUserCities_14() const { return ___otherUserCities_14; }
	inline Text_t356221433 ** get_address_of_otherUserCities_14() { return &___otherUserCities_14; }
	inline void set_otherUserCities_14(Text_t356221433 * value)
	{
		___otherUserCities_14 = value;
		Il2CppCodeGenWriteBarrier(&___otherUserCities_14, value);
	}

	inline static int32_t get_offset_of_otherUserCapital_15() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___otherUserCapital_15)); }
	inline Text_t356221433 * get_otherUserCapital_15() const { return ___otherUserCapital_15; }
	inline Text_t356221433 ** get_address_of_otherUserCapital_15() { return &___otherUserCapital_15; }
	inline void set_otherUserCapital_15(Text_t356221433 * value)
	{
		___otherUserCapital_15 = value;
		Il2CppCodeGenWriteBarrier(&___otherUserCapital_15, value);
	}

	inline static int32_t get_offset_of_otherUserCapitalCoords_16() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___otherUserCapitalCoords_16)); }
	inline Text_t356221433 * get_otherUserCapitalCoords_16() const { return ___otherUserCapitalCoords_16; }
	inline Text_t356221433 ** get_address_of_otherUserCapitalCoords_16() { return &___otherUserCapitalCoords_16; }
	inline void set_otherUserCapitalCoords_16(Text_t356221433 * value)
	{
		___otherUserCapitalCoords_16 = value;
		Il2CppCodeGenWriteBarrier(&___otherUserCapitalCoords_16, value);
	}

	inline static int32_t get_offset_of_otherUserLastOnline_17() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___otherUserLastOnline_17)); }
	inline Text_t356221433 * get_otherUserLastOnline_17() const { return ___otherUserLastOnline_17; }
	inline Text_t356221433 ** get_address_of_otherUserLastOnline_17() { return &___otherUserLastOnline_17; }
	inline void set_otherUserLastOnline_17(Text_t356221433 * value)
	{
		___otherUserLastOnline_17 = value;
		Il2CppCodeGenWriteBarrier(&___otherUserLastOnline_17, value);
	}

	inline static int32_t get_offset_of_otherUserFlag_18() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___otherUserFlag_18)); }
	inline Text_t356221433 * get_otherUserFlag_18() const { return ___otherUserFlag_18; }
	inline Text_t356221433 ** get_address_of_otherUserFlag_18() { return &___otherUserFlag_18; }
	inline void set_otherUserFlag_18(Text_t356221433 * value)
	{
		___otherUserFlag_18 = value;
		Il2CppCodeGenWriteBarrier(&___otherUserFlag_18, value);
	}

	inline static int32_t get_offset_of_otherUserImage_19() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___otherUserImage_19)); }
	inline Image_t2042527209 * get_otherUserImage_19() const { return ___otherUserImage_19; }
	inline Image_t2042527209 ** get_address_of_otherUserImage_19() { return &___otherUserImage_19; }
	inline void set_otherUserImage_19(Image_t2042527209 * value)
	{
		___otherUserImage_19 = value;
		Il2CppCodeGenWriteBarrier(&___otherUserImage_19, value);
	}

	inline static int32_t get_offset_of_otherUserPosition_20() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___otherUserPosition_20)); }
	inline Text_t356221433 * get_otherUserPosition_20() const { return ___otherUserPosition_20; }
	inline Text_t356221433 ** get_address_of_otherUserPosition_20() { return &___otherUserPosition_20; }
	inline void set_otherUserPosition_20(Text_t356221433 * value)
	{
		___otherUserPosition_20 = value;
		Il2CppCodeGenWriteBarrier(&___otherUserPosition_20, value);
	}

	inline static int32_t get_offset_of_otherUserAlliance_21() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___otherUserAlliance_21)); }
	inline Text_t356221433 * get_otherUserAlliance_21() const { return ___otherUserAlliance_21; }
	inline Text_t356221433 ** get_address_of_otherUserAlliance_21() { return &___otherUserAlliance_21; }
	inline void set_otherUserAlliance_21(Text_t356221433 * value)
	{
		___otherUserAlliance_21 = value;
		Il2CppCodeGenWriteBarrier(&___otherUserAlliance_21, value);
	}

	inline static int32_t get_offset_of_allianceMemberPosition_22() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___allianceMemberPosition_22)); }
	inline Dropdown_t1985816271 * get_allianceMemberPosition_22() const { return ___allianceMemberPosition_22; }
	inline Dropdown_t1985816271 ** get_address_of_allianceMemberPosition_22() { return &___allianceMemberPosition_22; }
	inline void set_allianceMemberPosition_22(Dropdown_t1985816271 * value)
	{
		___allianceMemberPosition_22 = value;
		Il2CppCodeGenWriteBarrier(&___allianceMemberPosition_22, value);
	}

	inline static int32_t get_offset_of_changeBtn_23() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___changeBtn_23)); }
	inline GameObject_t1756533147 * get_changeBtn_23() const { return ___changeBtn_23; }
	inline GameObject_t1756533147 ** get_address_of_changeBtn_23() { return &___changeBtn_23; }
	inline void set_changeBtn_23(GameObject_t1756533147 * value)
	{
		___changeBtn_23 = value;
		Il2CppCodeGenWriteBarrier(&___changeBtn_23, value);
	}

	inline static int32_t get_offset_of_excludeBtn_24() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___excludeBtn_24)); }
	inline GameObject_t1756533147 * get_excludeBtn_24() const { return ___excludeBtn_24; }
	inline GameObject_t1756533147 ** get_address_of_excludeBtn_24() { return &___excludeBtn_24; }
	inline void set_excludeBtn_24(GameObject_t1756533147 * value)
	{
		___excludeBtn_24 = value;
		Il2CppCodeGenWriteBarrier(&___excludeBtn_24, value);
	}

	inline static int32_t get_offset_of_user_25() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___user_25)); }
	inline UserModel_t3025569216 * get_user_25() const { return ___user_25; }
	inline UserModel_t3025569216 ** get_address_of_user_25() { return &___user_25; }
	inline void set_user_25(UserModel_t3025569216 * value)
	{
		___user_25 = value;
		Il2CppCodeGenWriteBarrier(&___user_25, value);
	}

	inline static int32_t get_offset_of_iconIndex_26() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___iconIndex_26)); }
	inline int64_t get_iconIndex_26() const { return ___iconIndex_26; }
	inline int64_t* get_address_of_iconIndex_26() { return &___iconIndex_26; }
	inline void set_iconIndex_26(int64_t value)
	{
		___iconIndex_26 = value;
	}

	inline static int32_t get_offset_of_ranks_27() { return static_cast<int32_t>(offsetof(UserInfoContentManager_t1247036235, ___ranks_27)); }
	inline StringU5BU5D_t1642385972* get_ranks_27() const { return ___ranks_27; }
	inline StringU5BU5D_t1642385972** get_address_of_ranks_27() { return &___ranks_27; }
	inline void set_ranks_27(StringU5BU5D_t1642385972* value)
	{
		___ranks_27 = value;
		Il2CppCodeGenWriteBarrier(&___ranks_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
