﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>
struct Dictionary_2_t2888425610;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4208450312.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_645770832.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_UI_UIGestur3880599975.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1264359115_gshared (Enumerator_t4208450312 * __this, Dictionary_2_t2888425610 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1264359115(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4208450312 *, Dictionary_2_t2888425610 *, const MethodInfo*))Enumerator__ctor_m1264359115_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1399391498_gshared (Enumerator_t4208450312 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1399391498(__this, method) ((  Il2CppObject * (*) (Enumerator_t4208450312 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1399391498_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1065448140_gshared (Enumerator_t4208450312 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1065448140(__this, method) ((  void (*) (Enumerator_t4208450312 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1065448140_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m394746173_gshared (Enumerator_t4208450312 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m394746173(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t4208450312 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m394746173_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m511606344_gshared (Enumerator_t4208450312 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m511606344(__this, method) ((  Il2CppObject * (*) (Enumerator_t4208450312 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m511606344_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1497205338_gshared (Enumerator_t4208450312 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1497205338(__this, method) ((  Il2CppObject * (*) (Enumerator_t4208450312 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1497205338_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1580513092_gshared (Enumerator_t4208450312 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1580513092(__this, method) ((  bool (*) (Enumerator_t4208450312 *, const MethodInfo*))Enumerator_MoveNext_m1580513092_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::get_Current()
extern "C"  KeyValuePair_2_t645770832  Enumerator_get_Current_m2240751636_gshared (Enumerator_t4208450312 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2240751636(__this, method) ((  KeyValuePair_2_t645770832  (*) (Enumerator_t4208450312 *, const MethodInfo*))Enumerator_get_Current_m2240751636_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m3603754319_gshared (Enumerator_t4208450312 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3603754319(__this, method) ((  int32_t (*) (Enumerator_t4208450312 *, const MethodInfo*))Enumerator_get_CurrentKey_m3603754319_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::get_CurrentValue()
extern "C"  TouchData_t3880599975  Enumerator_get_CurrentValue_m2926303207_gshared (Enumerator_t4208450312 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2926303207(__this, method) ((  TouchData_t3880599975  (*) (Enumerator_t4208450312 *, const MethodInfo*))Enumerator_get_CurrentValue_m2926303207_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::Reset()
extern "C"  void Enumerator_Reset_m2966190933_gshared (Enumerator_t4208450312 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2966190933(__this, method) ((  void (*) (Enumerator_t4208450312 *, const MethodInfo*))Enumerator_Reset_m2966190933_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1516856288_gshared (Enumerator_t4208450312 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1516856288(__this, method) ((  void (*) (Enumerator_t4208450312 *, const MethodInfo*))Enumerator_VerifyState_m1516856288_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m52252188_gshared (Enumerator_t4208450312 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m52252188(__this, method) ((  void (*) (Enumerator_t4208450312 *, const MethodInfo*))Enumerator_VerifyCurrent_m52252188_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>::Dispose()
extern "C"  void Enumerator_Dispose_m1474040583_gshared (Enumerator_t4208450312 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1474040583(__this, method) ((  void (*) (Enumerator_t4208450312 *, const MethodInfo*))Enumerator_Dispose_m1474040583_gshared)(__this, method)
