﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Behaviors.Visualizer.TouchProxy
struct TouchProxy_t1093150977;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"

// System.Void TouchScript.Behaviors.Visualizer.TouchProxy::.ctor()
extern "C"  void TouchProxy__ctor_m1780114441 (TouchProxy_t1093150977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Behaviors.Visualizer.TouchProxy::updateOnce(TouchScript.TouchPoint)
extern "C"  void TouchProxy_updateOnce_m2221566120 (TouchProxy_t1093150977 * __this, TouchPoint_t959629083 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
