﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t1971882247;
// FinanceReportModel
struct FinanceReportModel_t93490403;
// System.Object
struct Il2CppObject;
// FinancialContentChanger
struct FinancialContentChanger_t2753898366;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat875733053.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FinancialContentChanger/<GetFinanceReports>c__IteratorB
struct  U3CGetFinanceReportsU3Ec__IteratorB_t1286715107  : public Il2CppObject
{
public:
	// UnityEngine.Networking.UnityWebRequest FinancialContentChanger/<GetFinanceReports>c__IteratorB::<request>__0
	UnityWebRequest_t254341728 * ___U3CrequestU3E__0_0;
	// System.String FinancialContentChanger/<GetFinanceReports>c__IteratorB::<decodedString>__1
	String_t* ___U3CdecodedStringU3E__1_1;
	// JSONObject FinancialContentChanger/<GetFinanceReports>c__IteratorB::<itemArray>__2
	JSONObject_t1971882247 * ___U3CitemArrayU3E__2_2;
	// System.Collections.Generic.List`1/Enumerator<JSONObject> FinancialContentChanger/<GetFinanceReports>c__IteratorB::<$s_32>__3
	Enumerator_t875733053  ___U3CU24s_32U3E__3_3;
	// JSONObject FinancialContentChanger/<GetFinanceReports>c__IteratorB::<item>__4
	JSONObject_t1971882247 * ___U3CitemU3E__4_4;
	// FinanceReportModel FinancialContentChanger/<GetFinanceReports>c__IteratorB::<tempItem>__5
	FinanceReportModel_t93490403 * ___U3CtempItemU3E__5_5;
	// System.Int32 FinancialContentChanger/<GetFinanceReports>c__IteratorB::$PC
	int32_t ___U24PC_6;
	// System.Object FinancialContentChanger/<GetFinanceReports>c__IteratorB::$current
	Il2CppObject * ___U24current_7;
	// FinancialContentChanger FinancialContentChanger/<GetFinanceReports>c__IteratorB::<>f__this
	FinancialContentChanger_t2753898366 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetFinanceReportsU3Ec__IteratorB_t1286715107, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequestU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CdecodedStringU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetFinanceReportsU3Ec__IteratorB_t1286715107, ___U3CdecodedStringU3E__1_1)); }
	inline String_t* get_U3CdecodedStringU3E__1_1() const { return ___U3CdecodedStringU3E__1_1; }
	inline String_t** get_address_of_U3CdecodedStringU3E__1_1() { return &___U3CdecodedStringU3E__1_1; }
	inline void set_U3CdecodedStringU3E__1_1(String_t* value)
	{
		___U3CdecodedStringU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdecodedStringU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CitemArrayU3E__2_2() { return static_cast<int32_t>(offsetof(U3CGetFinanceReportsU3Ec__IteratorB_t1286715107, ___U3CitemArrayU3E__2_2)); }
	inline JSONObject_t1971882247 * get_U3CitemArrayU3E__2_2() const { return ___U3CitemArrayU3E__2_2; }
	inline JSONObject_t1971882247 ** get_address_of_U3CitemArrayU3E__2_2() { return &___U3CitemArrayU3E__2_2; }
	inline void set_U3CitemArrayU3E__2_2(JSONObject_t1971882247 * value)
	{
		___U3CitemArrayU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CitemArrayU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CU24s_32U3E__3_3() { return static_cast<int32_t>(offsetof(U3CGetFinanceReportsU3Ec__IteratorB_t1286715107, ___U3CU24s_32U3E__3_3)); }
	inline Enumerator_t875733053  get_U3CU24s_32U3E__3_3() const { return ___U3CU24s_32U3E__3_3; }
	inline Enumerator_t875733053 * get_address_of_U3CU24s_32U3E__3_3() { return &___U3CU24s_32U3E__3_3; }
	inline void set_U3CU24s_32U3E__3_3(Enumerator_t875733053  value)
	{
		___U3CU24s_32U3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CitemU3E__4_4() { return static_cast<int32_t>(offsetof(U3CGetFinanceReportsU3Ec__IteratorB_t1286715107, ___U3CitemU3E__4_4)); }
	inline JSONObject_t1971882247 * get_U3CitemU3E__4_4() const { return ___U3CitemU3E__4_4; }
	inline JSONObject_t1971882247 ** get_address_of_U3CitemU3E__4_4() { return &___U3CitemU3E__4_4; }
	inline void set_U3CitemU3E__4_4(JSONObject_t1971882247 * value)
	{
		___U3CitemU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CitemU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CtempItemU3E__5_5() { return static_cast<int32_t>(offsetof(U3CGetFinanceReportsU3Ec__IteratorB_t1286715107, ___U3CtempItemU3E__5_5)); }
	inline FinanceReportModel_t93490403 * get_U3CtempItemU3E__5_5() const { return ___U3CtempItemU3E__5_5; }
	inline FinanceReportModel_t93490403 ** get_address_of_U3CtempItemU3E__5_5() { return &___U3CtempItemU3E__5_5; }
	inline void set_U3CtempItemU3E__5_5(FinanceReportModel_t93490403 * value)
	{
		___U3CtempItemU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtempItemU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetFinanceReportsU3Ec__IteratorB_t1286715107, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CGetFinanceReportsU3Ec__IteratorB_t1286715107, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CGetFinanceReportsU3Ec__IteratorB_t1286715107, ___U3CU3Ef__this_8)); }
	inline FinancialContentChanger_t2753898366 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline FinancialContentChanger_t2753898366 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(FinancialContentChanger_t2753898366 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
