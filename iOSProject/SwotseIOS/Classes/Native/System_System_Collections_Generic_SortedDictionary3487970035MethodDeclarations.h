﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t3487970035;
// System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>
struct SortedDictionary_2_t3931121312;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_System_Collections_Generic_SortedDictionary_774927050.h"

// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1223068580_gshared (ValueCollection_t3487970035 * __this, SortedDictionary_2_t3931121312 * ___dic0, const MethodInfo* method);
#define ValueCollection__ctor_m1223068580(__this, ___dic0, method) ((  void (*) (ValueCollection_t3487970035 *, SortedDictionary_2_t3931121312 *, const MethodInfo*))ValueCollection__ctor_m1223068580_gshared)(__this, ___dic0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4179047417_gshared (ValueCollection_t3487970035 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4179047417(__this, ___item0, method) ((  void (*) (ValueCollection_t3487970035 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4179047417_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2071226972_gshared (ValueCollection_t3487970035 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2071226972(__this, method) ((  void (*) (ValueCollection_t3487970035 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2071226972_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1926908183_gshared (ValueCollection_t3487970035 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1926908183(__this, ___item0, method) ((  bool (*) (ValueCollection_t3487970035 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1926908183_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m761436074_gshared (ValueCollection_t3487970035 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m761436074(__this, method) ((  bool (*) (ValueCollection_t3487970035 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m761436074_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3095904206_gshared (ValueCollection_t3487970035 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3095904206(__this, ___item0, method) ((  bool (*) (ValueCollection_t3487970035 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3095904206_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1586722196_gshared (ValueCollection_t3487970035 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1586722196(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3487970035 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1586722196_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2938747680_gshared (ValueCollection_t3487970035 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2938747680(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3487970035 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2938747680_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m893939700_gshared (ValueCollection_t3487970035 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m893939700(__this, method) ((  bool (*) (ValueCollection_t3487970035 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m893939700_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1773089920_gshared (ValueCollection_t3487970035 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1773089920(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3487970035 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1773089920_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m895059825_gshared (ValueCollection_t3487970035 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m895059825(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3487970035 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m895059825_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m290463532_gshared (ValueCollection_t3487970035 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ValueCollection_CopyTo_m290463532(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ValueCollection_t3487970035 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m290463532_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3968444332_gshared (ValueCollection_t3487970035 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3968444332(__this, method) ((  int32_t (*) (ValueCollection_t3487970035 *, const MethodInfo*))ValueCollection_get_Count_m3968444332_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2/ValueCollection<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t774927050  ValueCollection_GetEnumerator_m914262970_gshared (ValueCollection_t3487970035 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m914262970(__this, method) ((  Enumerator_t774927050  (*) (ValueCollection_t3487970035 *, const MethodInfo*))ValueCollection_GetEnumerator_m914262970_gshared)(__this, method)
