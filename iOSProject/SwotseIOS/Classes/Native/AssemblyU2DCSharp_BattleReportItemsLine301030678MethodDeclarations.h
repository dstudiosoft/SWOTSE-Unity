﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BattleReportItemsLine
struct BattleReportItemsLine_t301030678;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void BattleReportItemsLine::.ctor()
extern "C"  void BattleReportItemsLine__ctor_m230717095 (BattleReportItemsLine_t301030678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportItemsLine::SetItemName(System.String)
extern "C"  void BattleReportItemsLine_SetItemName_m1675244969 (BattleReportItemsLine_t301030678 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportItemsLine::SetItemPrice(System.Int64)
extern "C"  void BattleReportItemsLine_SetItemPrice_m2192582423 (BattleReportItemsLine_t301030678 * __this, int64_t ___price0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportItemsLine::SetItemCount(System.Int64)
extern "C"  void BattleReportItemsLine_SetItemCount_m3905777421 (BattleReportItemsLine_t301030678 * __this, int64_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
