﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ShopItemModel
struct ShopItemModel_t96241056;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FinanceReportModel
struct  FinanceReportModel_t93490403  : public Il2CppObject
{
public:
	// System.String FinanceReportModel::id
	String_t* ___id_0;
	// System.Int64 FinanceReportModel::user
	int64_t ___user_1;
	// System.Int64 FinanceReportModel::city
	int64_t ___city_2;
	// ShopItemModel FinanceReportModel::item_constant
	ShopItemModel_t96241056 * ___item_constant_3;
	// System.Int64 FinanceReportModel::count
	int64_t ___count_4;
	// System.DateTime FinanceReportModel::date
	DateTime_t693205669  ___date_5;
	// System.Int64 FinanceReportModel::price
	int64_t ___price_6;
	// System.String FinanceReportModel::type
	String_t* ___type_7;
	// System.String FinanceReportModel::fromUser
	String_t* ___fromUser_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(FinanceReportModel_t93490403, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_user_1() { return static_cast<int32_t>(offsetof(FinanceReportModel_t93490403, ___user_1)); }
	inline int64_t get_user_1() const { return ___user_1; }
	inline int64_t* get_address_of_user_1() { return &___user_1; }
	inline void set_user_1(int64_t value)
	{
		___user_1 = value;
	}

	inline static int32_t get_offset_of_city_2() { return static_cast<int32_t>(offsetof(FinanceReportModel_t93490403, ___city_2)); }
	inline int64_t get_city_2() const { return ___city_2; }
	inline int64_t* get_address_of_city_2() { return &___city_2; }
	inline void set_city_2(int64_t value)
	{
		___city_2 = value;
	}

	inline static int32_t get_offset_of_item_constant_3() { return static_cast<int32_t>(offsetof(FinanceReportModel_t93490403, ___item_constant_3)); }
	inline ShopItemModel_t96241056 * get_item_constant_3() const { return ___item_constant_3; }
	inline ShopItemModel_t96241056 ** get_address_of_item_constant_3() { return &___item_constant_3; }
	inline void set_item_constant_3(ShopItemModel_t96241056 * value)
	{
		___item_constant_3 = value;
		Il2CppCodeGenWriteBarrier(&___item_constant_3, value);
	}

	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(FinanceReportModel_t93490403, ___count_4)); }
	inline int64_t get_count_4() const { return ___count_4; }
	inline int64_t* get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(int64_t value)
	{
		___count_4 = value;
	}

	inline static int32_t get_offset_of_date_5() { return static_cast<int32_t>(offsetof(FinanceReportModel_t93490403, ___date_5)); }
	inline DateTime_t693205669  get_date_5() const { return ___date_5; }
	inline DateTime_t693205669 * get_address_of_date_5() { return &___date_5; }
	inline void set_date_5(DateTime_t693205669  value)
	{
		___date_5 = value;
	}

	inline static int32_t get_offset_of_price_6() { return static_cast<int32_t>(offsetof(FinanceReportModel_t93490403, ___price_6)); }
	inline int64_t get_price_6() const { return ___price_6; }
	inline int64_t* get_address_of_price_6() { return &___price_6; }
	inline void set_price_6(int64_t value)
	{
		___price_6 = value;
	}

	inline static int32_t get_offset_of_type_7() { return static_cast<int32_t>(offsetof(FinanceReportModel_t93490403, ___type_7)); }
	inline String_t* get_type_7() const { return ___type_7; }
	inline String_t** get_address_of_type_7() { return &___type_7; }
	inline void set_type_7(String_t* value)
	{
		___type_7 = value;
		Il2CppCodeGenWriteBarrier(&___type_7, value);
	}

	inline static int32_t get_offset_of_fromUser_8() { return static_cast<int32_t>(offsetof(FinanceReportModel_t93490403, ___fromUser_8)); }
	inline String_t* get_fromUser_8() const { return ___fromUser_8; }
	inline String_t** get_address_of_fromUser_8() { return &___fromUser_8; }
	inline void set_fromUser_8(String_t* value)
	{
		___fromUser_8 = value;
		Il2CppCodeGenWriteBarrier(&___fromUser_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
