﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.VariantList
struct VariantList_t2408673687;
// System.Collections.ICollection
struct ICollection_t91669223;
// Firebase.Variant
struct Variant_t4275788079;
// Firebase.Variant[]
struct VariantU5BU5D_t268110134;
// System.Collections.Generic.IEnumerator`1<Firebase.Variant>
struct IEnumerator_1_t1751311906;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Firebase.VariantList/VariantListEnumerator
struct VariantListEnumerator_t3934991014;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Firebase_App_Firebase_VariantList2408673687.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"
#include "Firebase_App_Firebase_Variant4275788079.h"

// System.Void Firebase.VariantList::.ctor(System.IntPtr,System.Boolean)
extern "C"  void VariantList__ctor_m1417979012 (VariantList_t2408673687 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::.ctor(System.Collections.ICollection)
extern "C"  void VariantList__ctor_m9736560 (VariantList_t2408673687 * __this, Il2CppObject * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::.ctor()
extern "C"  void VariantList__ctor_m4182630999 (VariantList_t2408673687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::.ctor(Firebase.VariantList)
extern "C"  void VariantList__ctor_m1967901197 (VariantList_t2408673687 * __this, VariantList_t2408673687 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::.ctor(System.Int32)
extern "C"  void VariantList__ctor_m1345920640 (VariantList_t2408673687 * __this, int32_t ___capacity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef Firebase.VariantList::getCPtr(Firebase.VariantList)
extern "C"  HandleRef_t2419939847  VariantList_getCPtr_m1463826510 (Il2CppObject * __this /* static, unused */, VariantList_t2408673687 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::Finalize()
extern "C"  void VariantList_Finalize_m1999037121 (VariantList_t2408673687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::Dispose()
extern "C"  void VariantList_Dispose_m3401762696 (VariantList_t2408673687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.VariantList::get_IsFixedSize()
extern "C"  bool VariantList_get_IsFixedSize_m138626059 (VariantList_t2408673687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.VariantList::get_IsReadOnly()
extern "C"  bool VariantList_get_IsReadOnly_m1817973784 (VariantList_t2408673687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Variant Firebase.VariantList::get_Item(System.Int32)
extern "C"  Variant_t4275788079 * VariantList_get_Item_m2646944945 (VariantList_t2408673687 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::set_Item(System.Int32,Firebase.Variant)
extern "C"  void VariantList_set_Item_m3021212390 (VariantList_t2408673687 * __this, int32_t ___index0, Variant_t4275788079 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.VariantList::get_Capacity()
extern "C"  int32_t VariantList_get_Capacity_m24044662 (VariantList_t2408673687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::set_Capacity(System.Int32)
extern "C"  void VariantList_set_Capacity_m2114565775 (VariantList_t2408673687 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.VariantList::get_Count()
extern "C"  int32_t VariantList_get_Count_m1853805447 (VariantList_t2408673687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.VariantList::get_IsSynchronized()
extern "C"  bool VariantList_get_IsSynchronized_m74547888 (VariantList_t2408673687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::CopyTo(Firebase.Variant[])
extern "C"  void VariantList_CopyTo_m816588823 (VariantList_t2408673687 * __this, VariantU5BU5D_t268110134* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::CopyTo(Firebase.Variant[],System.Int32)
extern "C"  void VariantList_CopyTo_m2695015734 (VariantList_t2408673687 * __this, VariantU5BU5D_t268110134* ___array0, int32_t ___arrayIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::CopyTo(System.Int32,Firebase.Variant[],System.Int32,System.Int32)
extern "C"  void VariantList_CopyTo_m3827267326 (VariantList_t2408673687 * __this, int32_t ___index0, VariantU5BU5D_t268110134* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Firebase.Variant> Firebase.VariantList::System.Collections.Generic.IEnumerable<Firebase.Variant>.GetEnumerator()
extern "C"  Il2CppObject* VariantList_System_Collections_Generic_IEnumerableU3CFirebase_VariantU3E_GetEnumerator_m3816757279 (VariantList_t2408673687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Firebase.VariantList::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * VariantList_System_Collections_IEnumerable_GetEnumerator_m2564796740 (VariantList_t2408673687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.VariantList/VariantListEnumerator Firebase.VariantList::GetEnumerator()
extern "C"  VariantListEnumerator_t3934991014 * VariantList_GetEnumerator_m690810856 (VariantList_t2408673687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::Clear()
extern "C"  void VariantList_Clear_m1556607116 (VariantList_t2408673687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::Add(Firebase.Variant)
extern "C"  void VariantList_Add_m3720545160 (VariantList_t2408673687 * __this, Variant_t4275788079 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Firebase.VariantList::size()
extern "C"  uint32_t VariantList_size_m3859306253 (VariantList_t2408673687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Firebase.VariantList::capacity()
extern "C"  uint32_t VariantList_capacity_m354950576 (VariantList_t2408673687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::reserve(System.UInt32)
extern "C"  void VariantList_reserve_m1317623011 (VariantList_t2408673687 * __this, uint32_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Variant Firebase.VariantList::getitemcopy(System.Int32)
extern "C"  Variant_t4275788079 * VariantList_getitemcopy_m3089337395 (VariantList_t2408673687 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Variant Firebase.VariantList::getitem(System.Int32)
extern "C"  Variant_t4275788079 * VariantList_getitem_m2243161108 (VariantList_t2408673687 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::setitem(System.Int32,Firebase.Variant)
extern "C"  void VariantList_setitem_m3173218701 (VariantList_t2408673687 * __this, int32_t ___index0, Variant_t4275788079 * ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::AddRange(Firebase.VariantList)
extern "C"  void VariantList_AddRange_m1466796835 (VariantList_t2408673687 * __this, VariantList_t2408673687 * ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.VariantList Firebase.VariantList::GetRange(System.Int32,System.Int32)
extern "C"  VariantList_t2408673687 * VariantList_GetRange_m3198774839 (VariantList_t2408673687 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::Insert(System.Int32,Firebase.Variant)
extern "C"  void VariantList_Insert_m2355674417 (VariantList_t2408673687 * __this, int32_t ___index0, Variant_t4275788079 * ___x1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::InsertRange(System.Int32,Firebase.VariantList)
extern "C"  void VariantList_InsertRange_m1974943558 (VariantList_t2408673687 * __this, int32_t ___index0, VariantList_t2408673687 * ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::RemoveAt(System.Int32)
extern "C"  void VariantList_RemoveAt_m913423749 (VariantList_t2408673687 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::RemoveRange(System.Int32,System.Int32)
extern "C"  void VariantList_RemoveRange_m1711981552 (VariantList_t2408673687 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.VariantList Firebase.VariantList::Repeat(Firebase.Variant,System.Int32)
extern "C"  VariantList_t2408673687 * VariantList_Repeat_m2896143980 (Il2CppObject * __this /* static, unused */, Variant_t4275788079 * ___value0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::Reverse()
extern "C"  void VariantList_Reverse_m149790263 (VariantList_t2408673687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::Reverse(System.Int32,System.Int32)
extern "C"  void VariantList_Reverse_m597990059 (VariantList_t2408673687 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.VariantList::SetRange(System.Int32,Firebase.VariantList)
extern "C"  void VariantList_SetRange_m4061832997 (VariantList_t2408673687 * __this, int32_t ___index0, VariantList_t2408673687 * ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
