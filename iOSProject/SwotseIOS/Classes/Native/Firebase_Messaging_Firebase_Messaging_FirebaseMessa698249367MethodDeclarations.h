﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate
struct MessageReceivedDelegate_t3038239007;
// Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate
struct TokenReceivedDelegate_t1095792431;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"
#include "Firebase_Messaging_Firebase_Messaging_FirebaseMess3038239007.h"
#include "Firebase_Messaging_Firebase_Messaging_FirebaseMess1095792431.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::.cctor()
extern "C"  void FirebaseMessagingPINVOKE__cctor_m3587434657 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_FirebaseNotification_Body_get(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* FirebaseMessagingPINVOKE_Firebase_Messaging_FirebaseNotification_Body_get_m1057178107 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_delete_FirebaseNotification(System.Runtime.InteropServices.HandleRef)
extern "C"  void FirebaseMessagingPINVOKE_Firebase_Messaging_delete_FirebaseNotification_m3938588314 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_delete_FirebaseMessage(System.Runtime.InteropServices.HandleRef)
extern "C"  void FirebaseMessagingPINVOKE_Firebase_Messaging_delete_FirebaseMessage_m1658331736 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_SetListenerCallbacks(Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate,Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate)
extern "C"  void FirebaseMessagingPINVOKE_Firebase_Messaging_SetListenerCallbacks_m2687428259 (Il2CppObject * __this /* static, unused */, MessageReceivedDelegate_t3038239007 * ___jarg10, TokenReceivedDelegate_t1095792431 * ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_SetListenerCallbacksEnabled(System.Boolean,System.Boolean)
extern "C"  void FirebaseMessagingPINVOKE_Firebase_Messaging_SetListenerCallbacksEnabled_m633290538 (Il2CppObject * __this /* static, unused */, bool ___jarg10, bool ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_SendPendingEvents()
extern "C"  void FirebaseMessagingPINVOKE_Firebase_Messaging_SendPendingEvents_m696476573 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.Messaging.FirebaseMessagingPINVOKE::Firebase_Messaging_MessageCopyNotification(System.IntPtr)
extern "C"  IntPtr_t FirebaseMessagingPINVOKE_Firebase_Messaging_MessageCopyNotification_m4017059625 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
