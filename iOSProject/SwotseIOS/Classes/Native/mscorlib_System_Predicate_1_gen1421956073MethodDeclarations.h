﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"

// System.Void System.Predicate`1<ResourcesModel>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m3654314955(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t1421956073 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2289454599_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<ResourcesModel>::Invoke(T)
#define Predicate_1_Invoke_m3802196835(__this, ___obj0, method) ((  bool (*) (Predicate_1_t1421956073 *, ResourcesModel_t2978985958 *, const MethodInfo*))Predicate_1_Invoke_m4047721271_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<ResourcesModel>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m69984746(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t1421956073 *, ResourcesModel_t2978985958 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m3556950370_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<ResourcesModel>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m3311048385(__this, ___result0, method) ((  bool (*) (Predicate_1_t1421956073 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3656575065_gshared)(__this, ___result0, method)
