﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2875234987MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m294546815(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2547511429 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3671019970_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1246567993(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2547511429 *, SmartCultureInfo_t2361725737 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2989589458_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2957164293(__this, method) ((  void (*) (ReadOnlyCollection_1_t2547511429 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m454937302_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m325940508(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2547511429 *, int32_t, SmartCultureInfo_t2361725737 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4272763307_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3560619038(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2547511429 *, SmartCultureInfo_t2361725737 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3199809075_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3012834112(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2547511429 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m962041751_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3563919876(__this, ___index0, method) ((  SmartCultureInfo_t2361725737 * (*) (ReadOnlyCollection_1_t2547511429 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m70085287_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2699270713(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2547511429 *, int32_t, SmartCultureInfo_t2361725737 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1547026160_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m874181581(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2547511429 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4041967064_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m273772594(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2547511429 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3664791405_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m931320509(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2547511429 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m531171980_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2858125286(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2547511429 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3780136817_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1321612936(__this, method) ((  void (*) (ReadOnlyCollection_1_t2547511429 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3983677501_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m462070652(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2547511429 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1990607517_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m450620020(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2547511429 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m606942423_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m4280232025(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2547511429 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m691705570_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1817542137(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2547511429 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3182494192_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m366397443(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2547511429 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m572840272_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1657947346(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2547511429 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2871048729_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2247017052(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2547511429 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m769863805_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3828242013(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2547511429 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m942145650_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3433947222(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2547511429 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1367736517_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2779688073(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2547511429 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3336878134_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2953241496(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2547511429 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1799572719_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::Contains(T)
#define ReadOnlyCollection_1_Contains_m34277591(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2547511429 *, SmartCultureInfo_t2361725737 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m1227826160_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m1627331837(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2547511429 *, SmartCultureInfoU5BU5D_t1421956628*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m4257276542_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m1161580402(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2547511429 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1627519329_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m2903717933(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2547511429 *, SmartCultureInfo_t2361725737 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1981423404_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::get_Count()
#define ReadOnlyCollection_1_get_Count_m2322505366(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2547511429 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2562379905_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SmartLocalization.SmartCultureInfo>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m1475915656(__this, ___index0, method) ((  SmartCultureInfo_t2361725737 * (*) (ReadOnlyCollection_1_t2547511429 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m191392387_gshared)(__this, ___index0, method)
