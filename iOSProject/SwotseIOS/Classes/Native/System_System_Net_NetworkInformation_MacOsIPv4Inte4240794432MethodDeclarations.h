﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.MacOsIPv4InterfaceStatistics
struct MacOsIPv4InterfaceStatistics_t4240794432;
// System.Net.NetworkInformation.MacOsNetworkInterface
struct MacOsNetworkInterface_t1454185290;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_MacOsNetworkI1454185290.h"

// System.Void System.Net.NetworkInformation.MacOsIPv4InterfaceStatistics::.ctor(System.Net.NetworkInformation.MacOsNetworkInterface)
extern "C"  void MacOsIPv4InterfaceStatistics__ctor_m3293870546 (MacOsIPv4InterfaceStatistics_t4240794432 * __this, MacOsNetworkInterface_t1454185290 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.MacOsIPv4InterfaceStatistics::get_BytesReceived()
extern "C"  int64_t MacOsIPv4InterfaceStatistics_get_BytesReceived_m4144544576 (MacOsIPv4InterfaceStatistics_t4240794432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.MacOsIPv4InterfaceStatistics::get_BytesSent()
extern "C"  int64_t MacOsIPv4InterfaceStatistics_get_BytesSent_m1986669405 (MacOsIPv4InterfaceStatistics_t4240794432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.MacOsIPv4InterfaceStatistics::get_IncomingPacketsDiscarded()
extern "C"  int64_t MacOsIPv4InterfaceStatistics_get_IncomingPacketsDiscarded_m864435930 (MacOsIPv4InterfaceStatistics_t4240794432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.MacOsIPv4InterfaceStatistics::get_IncomingPacketsWithErrors()
extern "C"  int64_t MacOsIPv4InterfaceStatistics_get_IncomingPacketsWithErrors_m797261570 (MacOsIPv4InterfaceStatistics_t4240794432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.MacOsIPv4InterfaceStatistics::get_IncomingUnknownProtocolPackets()
extern "C"  int64_t MacOsIPv4InterfaceStatistics_get_IncomingUnknownProtocolPackets_m3869666651 (MacOsIPv4InterfaceStatistics_t4240794432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.MacOsIPv4InterfaceStatistics::get_NonUnicastPacketsReceived()
extern "C"  int64_t MacOsIPv4InterfaceStatistics_get_NonUnicastPacketsReceived_m1795872622 (MacOsIPv4InterfaceStatistics_t4240794432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.MacOsIPv4InterfaceStatistics::get_NonUnicastPacketsSent()
extern "C"  int64_t MacOsIPv4InterfaceStatistics_get_NonUnicastPacketsSent_m407275279 (MacOsIPv4InterfaceStatistics_t4240794432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.MacOsIPv4InterfaceStatistics::get_OutgoingPacketsDiscarded()
extern "C"  int64_t MacOsIPv4InterfaceStatistics_get_OutgoingPacketsDiscarded_m3134385816 (MacOsIPv4InterfaceStatistics_t4240794432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.MacOsIPv4InterfaceStatistics::get_OutgoingPacketsWithErrors()
extern "C"  int64_t MacOsIPv4InterfaceStatistics_get_OutgoingPacketsWithErrors_m1787186192 (MacOsIPv4InterfaceStatistics_t4240794432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.MacOsIPv4InterfaceStatistics::get_OutputQueueLength()
extern "C"  int64_t MacOsIPv4InterfaceStatistics_get_OutputQueueLength_m894463868 (MacOsIPv4InterfaceStatistics_t4240794432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.MacOsIPv4InterfaceStatistics::get_UnicastPacketsReceived()
extern "C"  int64_t MacOsIPv4InterfaceStatistics_get_UnicastPacketsReceived_m3686900495 (MacOsIPv4InterfaceStatistics_t4240794432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.MacOsIPv4InterfaceStatistics::get_UnicastPacketsSent()
extern "C"  int64_t MacOsIPv4InterfaceStatistics_get_UnicastPacketsSent_m2001495326 (MacOsIPv4InterfaceStatistics_t4240794432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
