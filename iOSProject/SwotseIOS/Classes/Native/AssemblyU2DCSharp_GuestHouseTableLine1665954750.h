﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// GuestFeastingTableController
struct GuestFeastingTableController_t60500861;
// ArmyGeneralModel
struct ArmyGeneralModel_t609759248;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuestHouseTableLine
struct  GuestHouseTableLine_t1665954750  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text GuestHouseTableLine::knightName
	Text_t356221433 * ___knightName_2;
	// UnityEngine.UI.Text GuestHouseTableLine::politicsLevel
	Text_t356221433 * ___politicsLevel_3;
	// UnityEngine.UI.Text GuestHouseTableLine::marchesLevel
	Text_t356221433 * ___marchesLevel_4;
	// UnityEngine.UI.Text GuestHouseTableLine::speedLevel
	Text_t356221433 * ___speedLevel_5;
	// UnityEngine.UI.Text GuestHouseTableLine::loyaltyLevel
	Text_t356221433 * ___loyaltyLevel_6;
	// UnityEngine.UI.Text GuestHouseTableLine::knightLevel
	Text_t356221433 * ___knightLevel_7;
	// UnityEngine.UI.Text GuestHouseTableLine::salary
	Text_t356221433 * ___salary_8;
	// UnityEngine.UI.Text GuestHouseTableLine::status
	Text_t356221433 * ___status_9;
	// UnityEngine.UI.Text GuestHouseTableLine::actionText
	Text_t356221433 * ___actionText_10;
	// UnityEngine.GameObject GuestHouseTableLine::nameBackground
	GameObject_t1756533147 * ___nameBackground_11;
	// GuestFeastingTableController GuestHouseTableLine::owner
	GuestFeastingTableController_t60500861 * ___owner_12;
	// ArmyGeneralModel GuestHouseTableLine::general
	ArmyGeneralModel_t609759248 * ___general_13;

public:
	inline static int32_t get_offset_of_knightName_2() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t1665954750, ___knightName_2)); }
	inline Text_t356221433 * get_knightName_2() const { return ___knightName_2; }
	inline Text_t356221433 ** get_address_of_knightName_2() { return &___knightName_2; }
	inline void set_knightName_2(Text_t356221433 * value)
	{
		___knightName_2 = value;
		Il2CppCodeGenWriteBarrier(&___knightName_2, value);
	}

	inline static int32_t get_offset_of_politicsLevel_3() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t1665954750, ___politicsLevel_3)); }
	inline Text_t356221433 * get_politicsLevel_3() const { return ___politicsLevel_3; }
	inline Text_t356221433 ** get_address_of_politicsLevel_3() { return &___politicsLevel_3; }
	inline void set_politicsLevel_3(Text_t356221433 * value)
	{
		___politicsLevel_3 = value;
		Il2CppCodeGenWriteBarrier(&___politicsLevel_3, value);
	}

	inline static int32_t get_offset_of_marchesLevel_4() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t1665954750, ___marchesLevel_4)); }
	inline Text_t356221433 * get_marchesLevel_4() const { return ___marchesLevel_4; }
	inline Text_t356221433 ** get_address_of_marchesLevel_4() { return &___marchesLevel_4; }
	inline void set_marchesLevel_4(Text_t356221433 * value)
	{
		___marchesLevel_4 = value;
		Il2CppCodeGenWriteBarrier(&___marchesLevel_4, value);
	}

	inline static int32_t get_offset_of_speedLevel_5() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t1665954750, ___speedLevel_5)); }
	inline Text_t356221433 * get_speedLevel_5() const { return ___speedLevel_5; }
	inline Text_t356221433 ** get_address_of_speedLevel_5() { return &___speedLevel_5; }
	inline void set_speedLevel_5(Text_t356221433 * value)
	{
		___speedLevel_5 = value;
		Il2CppCodeGenWriteBarrier(&___speedLevel_5, value);
	}

	inline static int32_t get_offset_of_loyaltyLevel_6() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t1665954750, ___loyaltyLevel_6)); }
	inline Text_t356221433 * get_loyaltyLevel_6() const { return ___loyaltyLevel_6; }
	inline Text_t356221433 ** get_address_of_loyaltyLevel_6() { return &___loyaltyLevel_6; }
	inline void set_loyaltyLevel_6(Text_t356221433 * value)
	{
		___loyaltyLevel_6 = value;
		Il2CppCodeGenWriteBarrier(&___loyaltyLevel_6, value);
	}

	inline static int32_t get_offset_of_knightLevel_7() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t1665954750, ___knightLevel_7)); }
	inline Text_t356221433 * get_knightLevel_7() const { return ___knightLevel_7; }
	inline Text_t356221433 ** get_address_of_knightLevel_7() { return &___knightLevel_7; }
	inline void set_knightLevel_7(Text_t356221433 * value)
	{
		___knightLevel_7 = value;
		Il2CppCodeGenWriteBarrier(&___knightLevel_7, value);
	}

	inline static int32_t get_offset_of_salary_8() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t1665954750, ___salary_8)); }
	inline Text_t356221433 * get_salary_8() const { return ___salary_8; }
	inline Text_t356221433 ** get_address_of_salary_8() { return &___salary_8; }
	inline void set_salary_8(Text_t356221433 * value)
	{
		___salary_8 = value;
		Il2CppCodeGenWriteBarrier(&___salary_8, value);
	}

	inline static int32_t get_offset_of_status_9() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t1665954750, ___status_9)); }
	inline Text_t356221433 * get_status_9() const { return ___status_9; }
	inline Text_t356221433 ** get_address_of_status_9() { return &___status_9; }
	inline void set_status_9(Text_t356221433 * value)
	{
		___status_9 = value;
		Il2CppCodeGenWriteBarrier(&___status_9, value);
	}

	inline static int32_t get_offset_of_actionText_10() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t1665954750, ___actionText_10)); }
	inline Text_t356221433 * get_actionText_10() const { return ___actionText_10; }
	inline Text_t356221433 ** get_address_of_actionText_10() { return &___actionText_10; }
	inline void set_actionText_10(Text_t356221433 * value)
	{
		___actionText_10 = value;
		Il2CppCodeGenWriteBarrier(&___actionText_10, value);
	}

	inline static int32_t get_offset_of_nameBackground_11() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t1665954750, ___nameBackground_11)); }
	inline GameObject_t1756533147 * get_nameBackground_11() const { return ___nameBackground_11; }
	inline GameObject_t1756533147 ** get_address_of_nameBackground_11() { return &___nameBackground_11; }
	inline void set_nameBackground_11(GameObject_t1756533147 * value)
	{
		___nameBackground_11 = value;
		Il2CppCodeGenWriteBarrier(&___nameBackground_11, value);
	}

	inline static int32_t get_offset_of_owner_12() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t1665954750, ___owner_12)); }
	inline GuestFeastingTableController_t60500861 * get_owner_12() const { return ___owner_12; }
	inline GuestFeastingTableController_t60500861 ** get_address_of_owner_12() { return &___owner_12; }
	inline void set_owner_12(GuestFeastingTableController_t60500861 * value)
	{
		___owner_12 = value;
		Il2CppCodeGenWriteBarrier(&___owner_12, value);
	}

	inline static int32_t get_offset_of_general_13() { return static_cast<int32_t>(offsetof(GuestHouseTableLine_t1665954750, ___general_13)); }
	inline ArmyGeneralModel_t609759248 * get_general_13() const { return ___general_13; }
	inline ArmyGeneralModel_t609759248 ** get_address_of_general_13() { return &___general_13; }
	inline void set_general_13(ArmyGeneralModel_t609759248 * value)
	{
		___general_13 = value;
		Il2CppCodeGenWriteBarrier(&___general_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
