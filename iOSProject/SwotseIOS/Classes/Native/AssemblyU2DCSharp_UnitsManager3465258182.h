﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnitsModel
struct UnitsModel_t1926818124;
// System.Collections.Generic.Dictionary`2<System.Int64,ArmyGeneralModel>
struct Dictionary_2_t1599277650;
// SimpleEvent
struct SimpleEvent_t1509017202;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsManager
struct  UnitsManager_t3465258182  : public Il2CppObject
{
public:
	// UnitsModel UnitsManager::cityUnits
	UnitsModel_t1926818124 * ___cityUnits_0;
	// System.Collections.Generic.Dictionary`2<System.Int64,ArmyGeneralModel> UnitsManager::cityGenerals
	Dictionary_2_t1599277650 * ___cityGenerals_1;
	// SimpleEvent UnitsManager::onUnitsHired
	SimpleEvent_t1509017202 * ___onUnitsHired_2;
	// SimpleEvent UnitsManager::onGeneralHired
	SimpleEvent_t1509017202 * ___onGeneralHired_3;
	// SimpleEvent UnitsManager::onGeneralDismissed
	SimpleEvent_t1509017202 * ___onGeneralDismissed_4;
	// SimpleEvent UnitsManager::onUnitsUpdated
	SimpleEvent_t1509017202 * ___onUnitsUpdated_5;
	// SimpleEvent UnitsManager::onGeneralUpgraded
	SimpleEvent_t1509017202 * ___onGeneralUpgraded_6;
	// SimpleEvent UnitsManager::onTrainingCancelled
	SimpleEvent_t1509017202 * ___onTrainingCancelled_7;
	// SimpleEvent UnitsManager::onUnitsDismissed
	SimpleEvent_t1509017202 * ___onUnitsDismissed_8;
	// SimpleEvent UnitsManager::onGetGenerals
	SimpleEvent_t1509017202 * ___onGetGenerals_9;

public:
	inline static int32_t get_offset_of_cityUnits_0() { return static_cast<int32_t>(offsetof(UnitsManager_t3465258182, ___cityUnits_0)); }
	inline UnitsModel_t1926818124 * get_cityUnits_0() const { return ___cityUnits_0; }
	inline UnitsModel_t1926818124 ** get_address_of_cityUnits_0() { return &___cityUnits_0; }
	inline void set_cityUnits_0(UnitsModel_t1926818124 * value)
	{
		___cityUnits_0 = value;
		Il2CppCodeGenWriteBarrier(&___cityUnits_0, value);
	}

	inline static int32_t get_offset_of_cityGenerals_1() { return static_cast<int32_t>(offsetof(UnitsManager_t3465258182, ___cityGenerals_1)); }
	inline Dictionary_2_t1599277650 * get_cityGenerals_1() const { return ___cityGenerals_1; }
	inline Dictionary_2_t1599277650 ** get_address_of_cityGenerals_1() { return &___cityGenerals_1; }
	inline void set_cityGenerals_1(Dictionary_2_t1599277650 * value)
	{
		___cityGenerals_1 = value;
		Il2CppCodeGenWriteBarrier(&___cityGenerals_1, value);
	}

	inline static int32_t get_offset_of_onUnitsHired_2() { return static_cast<int32_t>(offsetof(UnitsManager_t3465258182, ___onUnitsHired_2)); }
	inline SimpleEvent_t1509017202 * get_onUnitsHired_2() const { return ___onUnitsHired_2; }
	inline SimpleEvent_t1509017202 ** get_address_of_onUnitsHired_2() { return &___onUnitsHired_2; }
	inline void set_onUnitsHired_2(SimpleEvent_t1509017202 * value)
	{
		___onUnitsHired_2 = value;
		Il2CppCodeGenWriteBarrier(&___onUnitsHired_2, value);
	}

	inline static int32_t get_offset_of_onGeneralHired_3() { return static_cast<int32_t>(offsetof(UnitsManager_t3465258182, ___onGeneralHired_3)); }
	inline SimpleEvent_t1509017202 * get_onGeneralHired_3() const { return ___onGeneralHired_3; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGeneralHired_3() { return &___onGeneralHired_3; }
	inline void set_onGeneralHired_3(SimpleEvent_t1509017202 * value)
	{
		___onGeneralHired_3 = value;
		Il2CppCodeGenWriteBarrier(&___onGeneralHired_3, value);
	}

	inline static int32_t get_offset_of_onGeneralDismissed_4() { return static_cast<int32_t>(offsetof(UnitsManager_t3465258182, ___onGeneralDismissed_4)); }
	inline SimpleEvent_t1509017202 * get_onGeneralDismissed_4() const { return ___onGeneralDismissed_4; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGeneralDismissed_4() { return &___onGeneralDismissed_4; }
	inline void set_onGeneralDismissed_4(SimpleEvent_t1509017202 * value)
	{
		___onGeneralDismissed_4 = value;
		Il2CppCodeGenWriteBarrier(&___onGeneralDismissed_4, value);
	}

	inline static int32_t get_offset_of_onUnitsUpdated_5() { return static_cast<int32_t>(offsetof(UnitsManager_t3465258182, ___onUnitsUpdated_5)); }
	inline SimpleEvent_t1509017202 * get_onUnitsUpdated_5() const { return ___onUnitsUpdated_5; }
	inline SimpleEvent_t1509017202 ** get_address_of_onUnitsUpdated_5() { return &___onUnitsUpdated_5; }
	inline void set_onUnitsUpdated_5(SimpleEvent_t1509017202 * value)
	{
		___onUnitsUpdated_5 = value;
		Il2CppCodeGenWriteBarrier(&___onUnitsUpdated_5, value);
	}

	inline static int32_t get_offset_of_onGeneralUpgraded_6() { return static_cast<int32_t>(offsetof(UnitsManager_t3465258182, ___onGeneralUpgraded_6)); }
	inline SimpleEvent_t1509017202 * get_onGeneralUpgraded_6() const { return ___onGeneralUpgraded_6; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGeneralUpgraded_6() { return &___onGeneralUpgraded_6; }
	inline void set_onGeneralUpgraded_6(SimpleEvent_t1509017202 * value)
	{
		___onGeneralUpgraded_6 = value;
		Il2CppCodeGenWriteBarrier(&___onGeneralUpgraded_6, value);
	}

	inline static int32_t get_offset_of_onTrainingCancelled_7() { return static_cast<int32_t>(offsetof(UnitsManager_t3465258182, ___onTrainingCancelled_7)); }
	inline SimpleEvent_t1509017202 * get_onTrainingCancelled_7() const { return ___onTrainingCancelled_7; }
	inline SimpleEvent_t1509017202 ** get_address_of_onTrainingCancelled_7() { return &___onTrainingCancelled_7; }
	inline void set_onTrainingCancelled_7(SimpleEvent_t1509017202 * value)
	{
		___onTrainingCancelled_7 = value;
		Il2CppCodeGenWriteBarrier(&___onTrainingCancelled_7, value);
	}

	inline static int32_t get_offset_of_onUnitsDismissed_8() { return static_cast<int32_t>(offsetof(UnitsManager_t3465258182, ___onUnitsDismissed_8)); }
	inline SimpleEvent_t1509017202 * get_onUnitsDismissed_8() const { return ___onUnitsDismissed_8; }
	inline SimpleEvent_t1509017202 ** get_address_of_onUnitsDismissed_8() { return &___onUnitsDismissed_8; }
	inline void set_onUnitsDismissed_8(SimpleEvent_t1509017202 * value)
	{
		___onUnitsDismissed_8 = value;
		Il2CppCodeGenWriteBarrier(&___onUnitsDismissed_8, value);
	}

	inline static int32_t get_offset_of_onGetGenerals_9() { return static_cast<int32_t>(offsetof(UnitsManager_t3465258182, ___onGetGenerals_9)); }
	inline SimpleEvent_t1509017202 * get_onGetGenerals_9() const { return ___onGetGenerals_9; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGetGenerals_9() { return &___onGetGenerals_9; }
	inline void set_onGetGenerals_9(SimpleEvent_t1509017202 * value)
	{
		___onGetGenerals_9 = value;
		Il2CppCodeGenWriteBarrier(&___onGetGenerals_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
