﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>
struct Dictionary_2_t1739907934;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t1284510226;
// System.Collections.Generic.IDictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>
struct IDictionary_2_t4033958651;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>[]
struct KeyValuePair_2U5BU5D_t3400552909;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>>
struct IEnumerator_1_t1267744279;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>
struct KeyCollection_t4223405705;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>
struct ValueCollection_t442967777;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23792220452.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_TouchScript_InputSources_InputHa2732082299.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3059932636.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::.ctor()
extern "C"  void Dictionary_2__ctor_m3831290740_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3831290740(__this, method) ((  void (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2__ctor_m3831290740_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m414327578_gshared (Dictionary_2_t1739907934 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m414327578(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1739907934 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m414327578_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m3489178181_gshared (Dictionary_2_t1739907934 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m3489178181(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t1739907934 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3489178181_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m957633282_gshared (Dictionary_2_t1739907934 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m957633282(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1739907934 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m957633282_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3047708946_gshared (Dictionary_2_t1739907934 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m3047708946(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t1739907934 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3047708946_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m905794101_gshared (Dictionary_2_t1739907934 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m905794101(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t1739907934 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m905794101_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2544849908_gshared (Dictionary_2_t1739907934 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2544849908(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1739907934 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m2544849908_gshared)(__this, ___info0, ___context1, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m442454461_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m442454461(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m442454461_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m341618301_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m341618301(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m341618301_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1160832934_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1160832934(__this, method) ((  bool (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1160832934_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3098677553_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3098677553(__this, method) ((  bool (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3098677553_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2638094523_gshared (Dictionary_2_t1739907934 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2638094523(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1739907934 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2638094523_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3292118112_gshared (Dictionary_2_t1739907934 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3292118112(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1739907934 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3292118112_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m4005099293_gshared (Dictionary_2_t1739907934 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m4005099293(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1739907934 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m4005099293_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2285067393_gshared (Dictionary_2_t1739907934 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2285067393(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1739907934 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2285067393_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m3485464844_gshared (Dictionary_2_t1739907934 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m3485464844(__this, ___key0, method) ((  void (*) (Dictionary_2_t1739907934 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3485464844_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3176717631_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3176717631(__this, method) ((  bool (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3176717631_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3487847719_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3487847719(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3487847719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1935151565_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1935151565(__this, method) ((  bool (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1935151565_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3499503502_gshared (Dictionary_2_t1739907934 * __this, KeyValuePair_2_t3792220452  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3499503502(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1739907934 *, KeyValuePair_2_t3792220452 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3499503502_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4179106802_gshared (Dictionary_2_t1739907934 * __this, KeyValuePair_2_t3792220452  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4179106802(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1739907934 *, KeyValuePair_2_t3792220452 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m4179106802_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4293353442_gshared (Dictionary_2_t1739907934 * __this, KeyValuePair_2U5BU5D_t3400552909* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4293353442(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1739907934 *, KeyValuePair_2U5BU5D_t3400552909*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4293353442_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2962133051_gshared (Dictionary_2_t1739907934 * __this, KeyValuePair_2_t3792220452  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2962133051(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1739907934 *, KeyValuePair_2_t3792220452 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2962133051_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1759764455_gshared (Dictionary_2_t1739907934 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1759764455(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1739907934 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1759764455_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1569554012_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1569554012(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1569554012_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18366229_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18366229(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18366229_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4154450906_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4154450906(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4154450906_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m1363077527_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1363077527(__this, method) ((  int32_t (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2_get_Count_m1363077527_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_Item(TKey)
extern "C"  TouchState_t2732082299  Dictionary_2_get_Item_m3764306490_gshared (Dictionary_2_t1739907934 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3764306490(__this, ___key0, method) ((  TouchState_t2732082299  (*) (Dictionary_2_t1739907934 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m3764306490_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3734143989_gshared (Dictionary_2_t1739907934 * __this, int32_t ___key0, TouchState_t2732082299  ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3734143989(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1739907934 *, int32_t, TouchState_t2732082299 , const MethodInfo*))Dictionary_2_set_Item_m3734143989_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3572679547_gshared (Dictionary_2_t1739907934 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3572679547(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1739907934 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3572679547_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m1509539406_gshared (Dictionary_2_t1739907934 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1509539406(__this, ___size0, method) ((  void (*) (Dictionary_2_t1739907934 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1509539406_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m378837124_gshared (Dictionary_2_t1739907934 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m378837124(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1739907934 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m378837124_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3792220452  Dictionary_2_make_pair_m463201494_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, TouchState_t2732082299  ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m463201494(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3792220452  (*) (Il2CppObject * /* static, unused */, int32_t, TouchState_t2732082299 , const MethodInfo*))Dictionary_2_make_pair_m463201494_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m2712245952_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, TouchState_t2732082299  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m2712245952(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, TouchState_t2732082299 , const MethodInfo*))Dictionary_2_pick_key_m2712245952_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::pick_value(TKey,TValue)
extern "C"  TouchState_t2732082299  Dictionary_2_pick_value_m796693480_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, TouchState_t2732082299  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m796693480(__this /* static, unused */, ___key0, ___value1, method) ((  TouchState_t2732082299  (*) (Il2CppObject * /* static, unused */, int32_t, TouchState_t2732082299 , const MethodInfo*))Dictionary_2_pick_value_m796693480_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3896750383_gshared (Dictionary_2_t1739907934 * __this, KeyValuePair_2U5BU5D_t3400552909* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3896750383(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1739907934 *, KeyValuePair_2U5BU5D_t3400552909*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3896750383_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::Resize()
extern "C"  void Dictionary_2_Resize_m3756606157_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3756606157(__this, method) ((  void (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2_Resize_m3756606157_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1518043388_gshared (Dictionary_2_t1739907934 * __this, int32_t ___key0, TouchState_t2732082299  ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1518043388(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1739907934 *, int32_t, TouchState_t2732082299 , const MethodInfo*))Dictionary_2_Add_m1518043388_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::Clear()
extern "C"  void Dictionary_2_Clear_m3011685973_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3011685973(__this, method) ((  void (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2_Clear_m3011685973_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2954533146_gshared (Dictionary_2_t1739907934 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2954533146(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1739907934 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2954533146_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1075025330_gshared (Dictionary_2_t1739907934 * __this, TouchState_t2732082299  ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1075025330(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1739907934 *, TouchState_t2732082299 , const MethodInfo*))Dictionary_2_ContainsValue_m1075025330_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m1039054739_gshared (Dictionary_2_t1739907934 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1039054739(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1739907934 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m1039054739_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m156658891_gshared (Dictionary_2_t1739907934 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m156658891(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1739907934 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m156658891_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2174548707_gshared (Dictionary_2_t1739907934 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2174548707(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1739907934 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2174548707_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2485549465_gshared (Dictionary_2_t1739907934 * __this, int32_t ___key0, TouchState_t2732082299 * ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2485549465(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1739907934 *, int32_t, TouchState_t2732082299 *, const MethodInfo*))Dictionary_2_TryGetValue_m2485549465_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_Keys()
extern "C"  KeyCollection_t4223405705 * Dictionary_2_get_Keys_m513974950_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m513974950(__this, method) ((  KeyCollection_t4223405705 * (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2_get_Keys_m513974950_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::get_Values()
extern "C"  ValueCollection_t442967777 * Dictionary_2_get_Values_m404679846_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m404679846(__this, method) ((  ValueCollection_t442967777 * (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2_get_Values_m404679846_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m926231173_gshared (Dictionary_2_t1739907934 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m926231173(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1739907934 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m926231173_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::ToTValue(System.Object)
extern "C"  TouchState_t2732082299  Dictionary_2_ToTValue_m1060618597_gshared (Dictionary_2_t1739907934 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1060618597(__this, ___value0, method) ((  TouchState_t2732082299  (*) (Dictionary_2_t1739907934 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1060618597_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1604958387_gshared (Dictionary_2_t1739907934 * __this, KeyValuePair_2_t3792220452  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1604958387(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1739907934 *, KeyValuePair_2_t3792220452 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1604958387_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::GetEnumerator()
extern "C"  Enumerator_t3059932636  Dictionary_2_GetEnumerator_m1527226002_gshared (Dictionary_2_t1739907934 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1527226002(__this, method) ((  Enumerator_t3059932636  (*) (Dictionary_2_t1739907934 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1527226002_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m3449483165_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, TouchState_t2732082299  ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m3449483165(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, TouchState_t2732082299 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3449483165_gshared)(__this /* static, unused */, ___key0, ___value1, method)
