﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Dns/GetHostEntryNameCallback
struct GetHostEntryNameCallback_t1577659696;
// System.Object
struct Il2CppObject;
// System.Net.IPHostEntry
struct IPHostEntry_t994738509;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Net.Dns/GetHostEntryNameCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetHostEntryNameCallback__ctor_m643483004 (GetHostEntryNameCallback_t1577659696 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPHostEntry System.Net.Dns/GetHostEntryNameCallback::Invoke(System.String)
extern "C"  IPHostEntry_t994738509 * GetHostEntryNameCallback_Invoke_m2641985228 (GetHostEntryNameCallback_t1577659696 * __this, String_t* ___hostName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.Dns/GetHostEntryNameCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetHostEntryNameCallback_BeginInvoke_m3826273199 (GetHostEntryNameCallback_t1577659696 * __this, String_t* ___hostName0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPHostEntry System.Net.Dns/GetHostEntryNameCallback::EndInvoke(System.IAsyncResult)
extern "C"  IPHostEntry_t994738509 * GetHostEntryNameCallback_EndInvoke_m2715312842 (GetHostEntryNameCallback_t1577659696 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
