﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceEventModel
struct AllianceEventModel_t1540824908;

#include "codegen/il2cpp-codegen.h"

// System.Void AllianceEventModel::.ctor()
extern "C"  void AllianceEventModel__ctor_m414474545 (AllianceEventModel_t1540824908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
