﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// ConfirmationEvent
struct ConfirmationEvent_t4112571757;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllianceInformationContentManager
struct  AllianceInformationContentManager_t3239275125  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text AllianceInformationContentManager::allianceName
	Text_t356221433 * ___allianceName_2;
	// UnityEngine.UI.Text AllianceInformationContentManager::allianceFounder
	Text_t356221433 * ___allianceFounder_3;
	// UnityEngine.UI.Text AllianceInformationContentManager::allianceLeader
	Text_t356221433 * ___allianceLeader_4;
	// UnityEngine.UI.Text AllianceInformationContentManager::allianceRank
	Text_t356221433 * ___allianceRank_5;
	// UnityEngine.UI.Text AllianceInformationContentManager::allianceExperiance
	Text_t356221433 * ___allianceExperiance_6;
	// UnityEngine.UI.Text AllianceInformationContentManager::allianceMembersCount
	Text_t356221433 * ___allianceMembersCount_7;
	// UnityEngine.UI.Text AllianceInformationContentManager::allianceGuideline
	Text_t356221433 * ___allianceGuideline_8;
	// ConfirmationEvent AllianceInformationContentManager::confirmed
	ConfirmationEvent_t4112571757 * ___confirmed_9;

public:
	inline static int32_t get_offset_of_allianceName_2() { return static_cast<int32_t>(offsetof(AllianceInformationContentManager_t3239275125, ___allianceName_2)); }
	inline Text_t356221433 * get_allianceName_2() const { return ___allianceName_2; }
	inline Text_t356221433 ** get_address_of_allianceName_2() { return &___allianceName_2; }
	inline void set_allianceName_2(Text_t356221433 * value)
	{
		___allianceName_2 = value;
		Il2CppCodeGenWriteBarrier(&___allianceName_2, value);
	}

	inline static int32_t get_offset_of_allianceFounder_3() { return static_cast<int32_t>(offsetof(AllianceInformationContentManager_t3239275125, ___allianceFounder_3)); }
	inline Text_t356221433 * get_allianceFounder_3() const { return ___allianceFounder_3; }
	inline Text_t356221433 ** get_address_of_allianceFounder_3() { return &___allianceFounder_3; }
	inline void set_allianceFounder_3(Text_t356221433 * value)
	{
		___allianceFounder_3 = value;
		Il2CppCodeGenWriteBarrier(&___allianceFounder_3, value);
	}

	inline static int32_t get_offset_of_allianceLeader_4() { return static_cast<int32_t>(offsetof(AllianceInformationContentManager_t3239275125, ___allianceLeader_4)); }
	inline Text_t356221433 * get_allianceLeader_4() const { return ___allianceLeader_4; }
	inline Text_t356221433 ** get_address_of_allianceLeader_4() { return &___allianceLeader_4; }
	inline void set_allianceLeader_4(Text_t356221433 * value)
	{
		___allianceLeader_4 = value;
		Il2CppCodeGenWriteBarrier(&___allianceLeader_4, value);
	}

	inline static int32_t get_offset_of_allianceRank_5() { return static_cast<int32_t>(offsetof(AllianceInformationContentManager_t3239275125, ___allianceRank_5)); }
	inline Text_t356221433 * get_allianceRank_5() const { return ___allianceRank_5; }
	inline Text_t356221433 ** get_address_of_allianceRank_5() { return &___allianceRank_5; }
	inline void set_allianceRank_5(Text_t356221433 * value)
	{
		___allianceRank_5 = value;
		Il2CppCodeGenWriteBarrier(&___allianceRank_5, value);
	}

	inline static int32_t get_offset_of_allianceExperiance_6() { return static_cast<int32_t>(offsetof(AllianceInformationContentManager_t3239275125, ___allianceExperiance_6)); }
	inline Text_t356221433 * get_allianceExperiance_6() const { return ___allianceExperiance_6; }
	inline Text_t356221433 ** get_address_of_allianceExperiance_6() { return &___allianceExperiance_6; }
	inline void set_allianceExperiance_6(Text_t356221433 * value)
	{
		___allianceExperiance_6 = value;
		Il2CppCodeGenWriteBarrier(&___allianceExperiance_6, value);
	}

	inline static int32_t get_offset_of_allianceMembersCount_7() { return static_cast<int32_t>(offsetof(AllianceInformationContentManager_t3239275125, ___allianceMembersCount_7)); }
	inline Text_t356221433 * get_allianceMembersCount_7() const { return ___allianceMembersCount_7; }
	inline Text_t356221433 ** get_address_of_allianceMembersCount_7() { return &___allianceMembersCount_7; }
	inline void set_allianceMembersCount_7(Text_t356221433 * value)
	{
		___allianceMembersCount_7 = value;
		Il2CppCodeGenWriteBarrier(&___allianceMembersCount_7, value);
	}

	inline static int32_t get_offset_of_allianceGuideline_8() { return static_cast<int32_t>(offsetof(AllianceInformationContentManager_t3239275125, ___allianceGuideline_8)); }
	inline Text_t356221433 * get_allianceGuideline_8() const { return ___allianceGuideline_8; }
	inline Text_t356221433 ** get_address_of_allianceGuideline_8() { return &___allianceGuideline_8; }
	inline void set_allianceGuideline_8(Text_t356221433 * value)
	{
		___allianceGuideline_8 = value;
		Il2CppCodeGenWriteBarrier(&___allianceGuideline_8, value);
	}

	inline static int32_t get_offset_of_confirmed_9() { return static_cast<int32_t>(offsetof(AllianceInformationContentManager_t3239275125, ___confirmed_9)); }
	inline ConfirmationEvent_t4112571757 * get_confirmed_9() const { return ___confirmed_9; }
	inline ConfirmationEvent_t4112571757 ** get_address_of_confirmed_9() { return &___confirmed_9; }
	inline void set_confirmed_9(ConfirmationEvent_t4112571757 * value)
	{
		___confirmed_9 = value;
		Il2CppCodeGenWriteBarrier(&___confirmed_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
