﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.PingCompletedEventArgs
struct PingCompletedEventArgs_t841787612;
// System.Exception
struct Exception_t1927440687;
// System.Object
struct Il2CppObject;
// System.Net.NetworkInformation.PingReply
struct PingReply_t1978568794;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_System_Net_NetworkInformation_PingReply1978568794.h"

// System.Void System.Net.NetworkInformation.PingCompletedEventArgs::.ctor(System.Exception,System.Boolean,System.Object,System.Net.NetworkInformation.PingReply)
extern "C"  void PingCompletedEventArgs__ctor_m2695527771 (PingCompletedEventArgs_t841787612 * __this, Exception_t1927440687 * ___ex0, bool ___cancelled1, Il2CppObject * ___userState2, PingReply_t1978568794 * ___reply3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.PingReply System.Net.NetworkInformation.PingCompletedEventArgs::get_Reply()
extern "C"  PingReply_t1978568794 * PingCompletedEventArgs_get_Reply_m4263240260 (PingCompletedEventArgs_t841787612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
