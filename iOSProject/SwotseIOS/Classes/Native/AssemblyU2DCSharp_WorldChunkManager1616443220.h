﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// WorldFieldModel[,]
struct WorldFieldModelU5BU2CU5D_t2403726761;
// SimpleEvent
struct SimpleEvent_t1509017202;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldChunkManager
struct  WorldChunkManager_t1616443220  : public MonoBehaviour_t1158329972
{
public:
	// System.Int64 WorldChunkManager::startX
	int64_t ___startX_2;
	// System.Int64 WorldChunkManager::startY
	int64_t ___startY_3;
	// WorldFieldModel[,] WorldChunkManager::chunkFields
	WorldFieldModelU5BU2CU5D_t2403726761* ___chunkFields_4;
	// SimpleEvent WorldChunkManager::onGetChunkInfo
	SimpleEvent_t1509017202 * ___onGetChunkInfo_5;

public:
	inline static int32_t get_offset_of_startX_2() { return static_cast<int32_t>(offsetof(WorldChunkManager_t1616443220, ___startX_2)); }
	inline int64_t get_startX_2() const { return ___startX_2; }
	inline int64_t* get_address_of_startX_2() { return &___startX_2; }
	inline void set_startX_2(int64_t value)
	{
		___startX_2 = value;
	}

	inline static int32_t get_offset_of_startY_3() { return static_cast<int32_t>(offsetof(WorldChunkManager_t1616443220, ___startY_3)); }
	inline int64_t get_startY_3() const { return ___startY_3; }
	inline int64_t* get_address_of_startY_3() { return &___startY_3; }
	inline void set_startY_3(int64_t value)
	{
		___startY_3 = value;
	}

	inline static int32_t get_offset_of_chunkFields_4() { return static_cast<int32_t>(offsetof(WorldChunkManager_t1616443220, ___chunkFields_4)); }
	inline WorldFieldModelU5BU2CU5D_t2403726761* get_chunkFields_4() const { return ___chunkFields_4; }
	inline WorldFieldModelU5BU2CU5D_t2403726761** get_address_of_chunkFields_4() { return &___chunkFields_4; }
	inline void set_chunkFields_4(WorldFieldModelU5BU2CU5D_t2403726761* value)
	{
		___chunkFields_4 = value;
		Il2CppCodeGenWriteBarrier(&___chunkFields_4, value);
	}

	inline static int32_t get_offset_of_onGetChunkInfo_5() { return static_cast<int32_t>(offsetof(WorldChunkManager_t1616443220, ___onGetChunkInfo_5)); }
	inline SimpleEvent_t1509017202 * get_onGetChunkInfo_5() const { return ___onGetChunkInfo_5; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGetChunkInfo_5() { return &___onGetChunkInfo_5; }
	inline void set_onGetChunkInfo_5(SimpleEvent_t1509017202 * value)
	{
		___onGetChunkInfo_5 = value;
		Il2CppCodeGenWriteBarrier(&___onGetChunkInfo_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
