﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// MarketManager
struct MarketManager_t2047363881;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketSellTableLine
struct  MarketSellTableLine_t2218356634  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text MarketSellTableLine::quantity
	Text_t356221433 * ___quantity_2;
	// UnityEngine.UI.Text MarketSellTableLine::unitPrice
	Text_t356221433 * ___unitPrice_3;
	// System.Int64 MarketSellTableLine::lotId
	int64_t ___lotId_4;
	// MarketManager MarketSellTableLine::owner
	MarketManager_t2047363881 * ___owner_5;

public:
	inline static int32_t get_offset_of_quantity_2() { return static_cast<int32_t>(offsetof(MarketSellTableLine_t2218356634, ___quantity_2)); }
	inline Text_t356221433 * get_quantity_2() const { return ___quantity_2; }
	inline Text_t356221433 ** get_address_of_quantity_2() { return &___quantity_2; }
	inline void set_quantity_2(Text_t356221433 * value)
	{
		___quantity_2 = value;
		Il2CppCodeGenWriteBarrier(&___quantity_2, value);
	}

	inline static int32_t get_offset_of_unitPrice_3() { return static_cast<int32_t>(offsetof(MarketSellTableLine_t2218356634, ___unitPrice_3)); }
	inline Text_t356221433 * get_unitPrice_3() const { return ___unitPrice_3; }
	inline Text_t356221433 ** get_address_of_unitPrice_3() { return &___unitPrice_3; }
	inline void set_unitPrice_3(Text_t356221433 * value)
	{
		___unitPrice_3 = value;
		Il2CppCodeGenWriteBarrier(&___unitPrice_3, value);
	}

	inline static int32_t get_offset_of_lotId_4() { return static_cast<int32_t>(offsetof(MarketSellTableLine_t2218356634, ___lotId_4)); }
	inline int64_t get_lotId_4() const { return ___lotId_4; }
	inline int64_t* get_address_of_lotId_4() { return &___lotId_4; }
	inline void set_lotId_4(int64_t value)
	{
		___lotId_4 = value;
	}

	inline static int32_t get_offset_of_owner_5() { return static_cast<int32_t>(offsetof(MarketSellTableLine_t2218356634, ___owner_5)); }
	inline MarketManager_t2047363881 * get_owner_5() const { return ___owner_5; }
	inline MarketManager_t2047363881 ** get_address_of_owner_5() { return &___owner_5; }
	inline void set_owner_5(MarketManager_t2047363881 * value)
	{
		___owner_5 = value;
		Il2CppCodeGenWriteBarrier(&___owner_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
