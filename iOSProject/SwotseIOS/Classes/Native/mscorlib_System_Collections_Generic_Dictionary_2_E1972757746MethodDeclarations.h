﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En156532019MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.FirebaseApp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2482277094(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1972757746 *, Dictionary_2_t652733044 *, const MethodInfo*))Enumerator__ctor_m1897170543_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.FirebaseApp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1014993639(__this, method) ((  Il2CppObject * (*) (Enumerator_t1972757746 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2389891262_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.FirebaseApp>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1823278863(__this, method) ((  void (*) (Enumerator_t1972757746 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3302454658_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.FirebaseApp>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m819378034(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1972757746 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4077548725_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.FirebaseApp>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2567919057(__this, method) ((  Il2CppObject * (*) (Enumerator_t1972757746 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2319419488_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.FirebaseApp>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2182514473(__this, method) ((  Il2CppObject * (*) (Enumerator_t1972757746 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m120250090_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.FirebaseApp>::MoveNext()
#define Enumerator_MoveNext_m860532459(__this, method) ((  bool (*) (Enumerator_t1972757746 *, const MethodInfo*))Enumerator_MoveNext_m266619810_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.FirebaseApp>::get_Current()
#define Enumerator_get_Current_m2896167891(__this, method) ((  KeyValuePair_2_t2705045562  (*) (Enumerator_t1972757746 *, const MethodInfo*))Enumerator_get_Current_m3216113338_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.FirebaseApp>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m735930166(__this, method) ((  IntPtr_t (*) (Enumerator_t1972757746 *, const MethodInfo*))Enumerator_get_CurrentKey_m2387752419_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.FirebaseApp>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3865391014(__this, method) ((  FirebaseApp_t210707726 * (*) (Enumerator_t1972757746 *, const MethodInfo*))Enumerator_get_CurrentValue_m3147984131_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.FirebaseApp>::Reset()
#define Enumerator_Reset_m1788315388(__this, method) ((  void (*) (Enumerator_t1972757746 *, const MethodInfo*))Enumerator_Reset_m1147555085_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.FirebaseApp>::VerifyState()
#define Enumerator_VerifyState_m1172734129(__this, method) ((  void (*) (Enumerator_t1972757746 *, const MethodInfo*))Enumerator_VerifyState_m1547102902_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.FirebaseApp>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m529486663(__this, method) ((  void (*) (Enumerator_t1972757746 *, const MethodInfo*))Enumerator_VerifyCurrent_m213906106_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.FirebaseApp>::Dispose()
#define Enumerator_Dispose_m1554792938(__this, method) ((  void (*) (Enumerator_t1972757746 *, const MethodInfo*))Enumerator_Dispose_m2117437651_gshared)(__this, method)
