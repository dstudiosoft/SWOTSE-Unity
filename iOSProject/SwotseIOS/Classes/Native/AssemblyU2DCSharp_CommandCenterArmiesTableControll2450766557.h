﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CommandCenterArmyTableLine
struct CommandCenterArmyTableLine_t1553486509;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.Generic.List`1<ArmyModel>
struct List_1_t1526242070;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandCenterArmiesTableController
struct  CommandCenterArmiesTableController_t2450766557  : public MonoBehaviour_t1158329972
{
public:
	// CommandCenterArmyTableLine CommandCenterArmiesTableController::m_cellPrefab
	CommandCenterArmyTableLine_t1553486509 * ___m_cellPrefab_2;
	// Tacticsoft.TableView CommandCenterArmiesTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Collections.Generic.List`1<ArmyModel> CommandCenterArmiesTableController::cityArmies
	List_1_t1526242070 * ___cityArmies_4;
	// System.Int32 CommandCenterArmiesTableController::m_numRows
	int32_t ___m_numRows_5;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(CommandCenterArmiesTableController_t2450766557, ___m_cellPrefab_2)); }
	inline CommandCenterArmyTableLine_t1553486509 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline CommandCenterArmyTableLine_t1553486509 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(CommandCenterArmyTableLine_t1553486509 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(CommandCenterArmiesTableController_t2450766557, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_cityArmies_4() { return static_cast<int32_t>(offsetof(CommandCenterArmiesTableController_t2450766557, ___cityArmies_4)); }
	inline List_1_t1526242070 * get_cityArmies_4() const { return ___cityArmies_4; }
	inline List_1_t1526242070 ** get_address_of_cityArmies_4() { return &___cityArmies_4; }
	inline void set_cityArmies_4(List_1_t1526242070 * value)
	{
		___cityArmies_4 = value;
		Il2CppCodeGenWriteBarrier(&___cityArmies_4, value);
	}

	inline static int32_t get_offset_of_m_numRows_5() { return static_cast<int32_t>(offsetof(CommandCenterArmiesTableController_t2450766557, ___m_numRows_5)); }
	inline int32_t get_m_numRows_5() const { return ___m_numRows_5; }
	inline int32_t* get_address_of_m_numRows_5() { return &___m_numRows_5; }
	inline void set_m_numRows_5(int32_t value)
	{
		___m_numRows_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
