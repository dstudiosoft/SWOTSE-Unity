﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.GestureManager
struct GestureManager_t1999815568;
// TouchScript.IGestureManager
struct IGestureManager_t4266705231;

#include "codegen/il2cpp-codegen.h"

// System.Void TouchScript.GestureManager::.ctor()
extern "C"  void GestureManager__ctor_m1493539603 (GestureManager_t1999815568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.IGestureManager TouchScript.GestureManager::get_Instance()
extern "C"  Il2CppObject * GestureManager_get_Instance_m1878441333 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
