﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Gestures.GestureStateChangeEventArgs
struct GestureStateChangeEventArgs_t3499981191;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Gestures_Gesture_Ges2128095272.h"

// System.Void TouchScript.Gestures.GestureStateChangeEventArgs::.ctor(TouchScript.Gestures.Gesture/GestureState,TouchScript.Gestures.Gesture/GestureState)
extern "C"  void GestureStateChangeEventArgs__ctor_m3387983986 (GestureStateChangeEventArgs_t3499981191 * __this, int32_t ___state0, int32_t ___previousState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.GestureStateChangeEventArgs::get_PreviousState()
extern "C"  int32_t GestureStateChangeEventArgs_get_PreviousState_m1511732166 (GestureStateChangeEventArgs_t3499981191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.GestureStateChangeEventArgs::set_PreviousState(TouchScript.Gestures.Gesture/GestureState)
extern "C"  void GestureStateChangeEventArgs_set_PreviousState_m543635449 (GestureStateChangeEventArgs_t3499981191 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.GestureStateChangeEventArgs::get_State()
extern "C"  int32_t GestureStateChangeEventArgs_get_State_m3617736647 (GestureStateChangeEventArgs_t3499981191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.GestureStateChangeEventArgs::set_State(TouchScript.Gestures.Gesture/GestureState)
extern "C"  void GestureStateChangeEventArgs_set_State_m350232808 (GestureStateChangeEventArgs_t3499981191 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
