﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmartLocalization.Editor.LocalizedText
struct LocalizedText_t1514653696;
// SmartLocalization.LanguageManager
struct LanguageManager_t132697751;

#include "codegen/il2cpp-codegen.h"
#include "SmartLocalization_Runtime_SmartLocalization_Languag132697751.h"

// System.Void SmartLocalization.Editor.LocalizedText::.ctor()
extern "C"  void LocalizedText__ctor_m772387038 (LocalizedText_t1514653696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.Editor.LocalizedText::Start()
extern "C"  void LocalizedText_Start_m804042466 (LocalizedText_t1514653696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.Editor.LocalizedText::OnDestroy()
extern "C"  void LocalizedText_OnDestroy_m593264657 (LocalizedText_t1514653696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.Editor.LocalizedText::OnChangeLanguage(SmartLocalization.LanguageManager)
extern "C"  void LocalizedText_OnChangeLanguage_m1513996450 (LocalizedText_t1514653696 * __this, LanguageManager_t132697751 * ___languageManager0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
