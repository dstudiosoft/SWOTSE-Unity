﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FinancialContentChanger/<GetFinanceOffers>c__IteratorC
struct U3CGetFinanceOffersU3Ec__IteratorC_t564612360;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FinancialContentChanger/<GetFinanceOffers>c__IteratorC::.ctor()
extern "C"  void U3CGetFinanceOffersU3Ec__IteratorC__ctor_m3241323105 (U3CGetFinanceOffersU3Ec__IteratorC_t564612360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FinancialContentChanger/<GetFinanceOffers>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetFinanceOffersU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2024223083 (U3CGetFinanceOffersU3Ec__IteratorC_t564612360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FinancialContentChanger/<GetFinanceOffers>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetFinanceOffersU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m2148173427 (U3CGetFinanceOffersU3Ec__IteratorC_t564612360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FinancialContentChanger/<GetFinanceOffers>c__IteratorC::MoveNext()
extern "C"  bool U3CGetFinanceOffersU3Ec__IteratorC_MoveNext_m2637855059 (U3CGetFinanceOffersU3Ec__IteratorC_t564612360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialContentChanger/<GetFinanceOffers>c__IteratorC::Dispose()
extern "C"  void U3CGetFinanceOffersU3Ec__IteratorC_Dispose_m1270211010 (U3CGetFinanceOffersU3Ec__IteratorC_t564612360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FinancialContentChanger/<GetFinanceOffers>c__IteratorC::Reset()
extern "C"  void U3CGetFinanceOffersU3Ec__IteratorC_Reset_m391694776 (U3CGetFinanceOffersU3Ec__IteratorC_t564612360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
