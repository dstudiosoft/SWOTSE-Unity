﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.AsnEncodedDataCollection
struct AsnEncodedDataCollection_t912862782;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t463456204;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Security.Cryptography.AsnEncodedData[]
struct AsnEncodedDataU5BU5D_t727122821;
// System.Security.Cryptography.AsnEncodedDataEnumerator
struct AsnEncodedDataEnumerator_t884119648;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Security_Cryptography_AsnEncodedData463456204.h"
#include "mscorlib_System_Array3829468939.h"

// System.Void System.Security.Cryptography.AsnEncodedDataCollection::.ctor()
extern "C"  void AsnEncodedDataCollection__ctor_m3380189169 (AsnEncodedDataCollection_t912862782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.AsnEncodedDataCollection::.ctor(System.Security.Cryptography.AsnEncodedData)
extern "C"  void AsnEncodedDataCollection__ctor_m2374727324 (AsnEncodedDataCollection_t912862782 * __this, AsnEncodedData_t463456204 * ___asnEncodedData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.AsnEncodedDataCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void AsnEncodedDataCollection_System_Collections_ICollection_CopyTo_m2716985841 (AsnEncodedDataCollection_t912862782 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.Cryptography.AsnEncodedDataCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * AsnEncodedDataCollection_System_Collections_IEnumerable_GetEnumerator_m2677836900 (AsnEncodedDataCollection_t912862782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.AsnEncodedDataCollection::get_Count()
extern "C"  int32_t AsnEncodedDataCollection_get_Count_m519420221 (AsnEncodedDataCollection_t912862782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.AsnEncodedDataCollection::get_IsSynchronized()
extern "C"  bool AsnEncodedDataCollection_get_IsSynchronized_m768216708 (AsnEncodedDataCollection_t912862782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.AsnEncodedDataCollection::get_Item(System.Int32)
extern "C"  AsnEncodedData_t463456204 * AsnEncodedDataCollection_get_Item_m2639530986 (AsnEncodedDataCollection_t912862782 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Cryptography.AsnEncodedDataCollection::get_SyncRoot()
extern "C"  Il2CppObject * AsnEncodedDataCollection_get_SyncRoot_m929504528 (AsnEncodedDataCollection_t912862782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.AsnEncodedDataCollection::Add(System.Security.Cryptography.AsnEncodedData)
extern "C"  int32_t AsnEncodedDataCollection_Add_m2785372735 (AsnEncodedDataCollection_t912862782 * __this, AsnEncodedData_t463456204 * ___asnEncodedData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.AsnEncodedDataCollection::CopyTo(System.Security.Cryptography.AsnEncodedData[],System.Int32)
extern "C"  void AsnEncodedDataCollection_CopyTo_m995702517 (AsnEncodedDataCollection_t912862782 * __this, AsnEncodedDataU5BU5D_t727122821* ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedDataEnumerator System.Security.Cryptography.AsnEncodedDataCollection::GetEnumerator()
extern "C"  AsnEncodedDataEnumerator_t884119648 * AsnEncodedDataCollection_GetEnumerator_m3614585941 (AsnEncodedDataCollection_t912862782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.AsnEncodedDataCollection::Remove(System.Security.Cryptography.AsnEncodedData)
extern "C"  void AsnEncodedDataCollection_Remove_m3382169436 (AsnEncodedDataCollection_t912862782 * __this, AsnEncodedData_t463456204 * ___asnEncodedData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
