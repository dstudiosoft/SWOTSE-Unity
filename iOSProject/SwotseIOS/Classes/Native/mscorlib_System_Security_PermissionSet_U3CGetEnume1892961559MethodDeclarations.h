﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.PermissionSet/<GetEnumerator>c__Iterator1
struct U3CGetEnumeratorU3Ec__Iterator1_t1892961559;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.PermissionSet/<GetEnumerator>c__Iterator1::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1__ctor_m64415825 (U3CGetEnumeratorU3Ec__Iterator1_t1892961559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.PermissionSet/<GetEnumerator>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m553293619 (U3CGetEnumeratorU3Ec__Iterator1_t1892961559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.PermissionSet/<GetEnumerator>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1452027451 (U3CGetEnumeratorU3Ec__Iterator1_t1892961559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.PermissionSet/<GetEnumerator>c__Iterator1::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator1_MoveNext_m2947907 (U3CGetEnumeratorU3Ec__Iterator1_t1892961559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet/<GetEnumerator>c__Iterator1::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1_Dispose_m2505226822 (U3CGetEnumeratorU3Ec__Iterator1_t1892961559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet/<GetEnumerator>c__Iterator1::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1_Reset_m2918421568 (U3CGetEnumeratorU3Ec__Iterator1_t1892961559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
