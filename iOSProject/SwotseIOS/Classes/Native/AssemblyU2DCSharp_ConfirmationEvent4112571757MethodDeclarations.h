﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConfirmationEvent
struct ConfirmationEvent_t4112571757;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void ConfirmationEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void ConfirmationEvent__ctor_m1745379274 (ConfirmationEvent_t4112571757 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConfirmationEvent::Invoke(System.Boolean)
extern "C"  void ConfirmationEvent_Invoke_m2163172727 (ConfirmationEvent_t4112571757 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ConfirmationEvent::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ConfirmationEvent_BeginInvoke_m3806882792 (ConfirmationEvent_t4112571757 * __this, bool ___value0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConfirmationEvent::EndInvoke(System.IAsyncResult)
extern "C"  void ConfirmationEvent_EndInvoke_m4281033344 (ConfirmationEvent_t4112571757 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
