﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextLocalizer
struct  TextLocalizer_t2861965306  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TextLocalizer::uppercase
	bool ___uppercase_2;
	// System.String TextLocalizer::key
	String_t* ___key_3;
	// System.String TextLocalizer::appendText
	String_t* ___appendText_4;

public:
	inline static int32_t get_offset_of_uppercase_2() { return static_cast<int32_t>(offsetof(TextLocalizer_t2861965306, ___uppercase_2)); }
	inline bool get_uppercase_2() const { return ___uppercase_2; }
	inline bool* get_address_of_uppercase_2() { return &___uppercase_2; }
	inline void set_uppercase_2(bool value)
	{
		___uppercase_2 = value;
	}

	inline static int32_t get_offset_of_key_3() { return static_cast<int32_t>(offsetof(TextLocalizer_t2861965306, ___key_3)); }
	inline String_t* get_key_3() const { return ___key_3; }
	inline String_t** get_address_of_key_3() { return &___key_3; }
	inline void set_key_3(String_t* value)
	{
		___key_3 = value;
		Il2CppCodeGenWriteBarrier(&___key_3, value);
	}

	inline static int32_t get_offset_of_appendText_4() { return static_cast<int32_t>(offsetof(TextLocalizer_t2861965306, ___appendText_4)); }
	inline String_t* get_appendText_4() const { return ___appendText_4; }
	inline String_t** get_address_of_appendText_4() { return &___appendText_4; }
	inline void set_appendText_4(String_t* value)
	{
		___appendText_4 = value;
		Il2CppCodeGenWriteBarrier(&___appendText_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
