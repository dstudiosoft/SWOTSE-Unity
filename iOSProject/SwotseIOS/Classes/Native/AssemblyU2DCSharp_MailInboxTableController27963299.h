﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MailInboxLine
struct MailInboxLine_t3727558719;
// Tacticsoft.TableView
struct TableView_t3179510217;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// MessageHeader[]
struct MessageHeaderU5BU5D_t2670624987;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MailInboxTableController
struct  MailInboxTableController_t27963299  : public MonoBehaviour_t1158329972
{
public:
	// MailInboxLine MailInboxTableController::m_cellPrefab
	MailInboxLine_t3727558719 * ___m_cellPrefab_2;
	// Tacticsoft.TableView MailInboxTableController::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// UnityEngine.UI.InputField MailInboxTableController::username
	InputField_t1631627530 * ___username_4;
	// MessageHeader[] MailInboxTableController::allMessages
	MessageHeaderU5BU5D_t2670624987* ___allMessages_5;
	// System.Collections.ArrayList MailInboxTableController::searchResults
	ArrayList_t4252133567 * ___searchResults_6;
	// System.Boolean MailInboxTableController::search
	bool ___search_7;
	// System.Int32 MailInboxTableController::m_numRows
	int32_t ___m_numRows_8;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(MailInboxTableController_t27963299, ___m_cellPrefab_2)); }
	inline MailInboxLine_t3727558719 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline MailInboxLine_t3727558719 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(MailInboxLine_t3727558719 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(MailInboxTableController_t27963299, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_username_4() { return static_cast<int32_t>(offsetof(MailInboxTableController_t27963299, ___username_4)); }
	inline InputField_t1631627530 * get_username_4() const { return ___username_4; }
	inline InputField_t1631627530 ** get_address_of_username_4() { return &___username_4; }
	inline void set_username_4(InputField_t1631627530 * value)
	{
		___username_4 = value;
		Il2CppCodeGenWriteBarrier(&___username_4, value);
	}

	inline static int32_t get_offset_of_allMessages_5() { return static_cast<int32_t>(offsetof(MailInboxTableController_t27963299, ___allMessages_5)); }
	inline MessageHeaderU5BU5D_t2670624987* get_allMessages_5() const { return ___allMessages_5; }
	inline MessageHeaderU5BU5D_t2670624987** get_address_of_allMessages_5() { return &___allMessages_5; }
	inline void set_allMessages_5(MessageHeaderU5BU5D_t2670624987* value)
	{
		___allMessages_5 = value;
		Il2CppCodeGenWriteBarrier(&___allMessages_5, value);
	}

	inline static int32_t get_offset_of_searchResults_6() { return static_cast<int32_t>(offsetof(MailInboxTableController_t27963299, ___searchResults_6)); }
	inline ArrayList_t4252133567 * get_searchResults_6() const { return ___searchResults_6; }
	inline ArrayList_t4252133567 ** get_address_of_searchResults_6() { return &___searchResults_6; }
	inline void set_searchResults_6(ArrayList_t4252133567 * value)
	{
		___searchResults_6 = value;
		Il2CppCodeGenWriteBarrier(&___searchResults_6, value);
	}

	inline static int32_t get_offset_of_search_7() { return static_cast<int32_t>(offsetof(MailInboxTableController_t27963299, ___search_7)); }
	inline bool get_search_7() const { return ___search_7; }
	inline bool* get_address_of_search_7() { return &___search_7; }
	inline void set_search_7(bool value)
	{
		___search_7 = value;
	}

	inline static int32_t get_offset_of_m_numRows_8() { return static_cast<int32_t>(offsetof(MailInboxTableController_t27963299, ___m_numRows_8)); }
	inline int32_t get_m_numRows_8() const { return ___m_numRows_8; }
	inline int32_t* get_address_of_m_numRows_8() { return &___m_numRows_8; }
	inline void set_m_numRows_8(int32_t value)
	{
		___m_numRows_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
