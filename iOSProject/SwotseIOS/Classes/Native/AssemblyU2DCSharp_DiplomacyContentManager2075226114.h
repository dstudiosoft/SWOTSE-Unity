﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Toggle
struct Toggle_t3976754468;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DiplomacyContentManager
struct  DiplomacyContentManager_t2075226114  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Toggle DiplomacyContentManager::allowTroopsTGL
	Toggle_t3976754468 * ___allowTroopsTGL_2;

public:
	inline static int32_t get_offset_of_allowTroopsTGL_2() { return static_cast<int32_t>(offsetof(DiplomacyContentManager_t2075226114, ___allowTroopsTGL_2)); }
	inline Toggle_t3976754468 * get_allowTroopsTGL_2() const { return ___allowTroopsTGL_2; }
	inline Toggle_t3976754468 ** get_address_of_allowTroopsTGL_2() { return &___allowTroopsTGL_2; }
	inline void set_allowTroopsTGL_2(Toggle_t3976754468 * value)
	{
		___allowTroopsTGL_2 = value;
		Il2CppCodeGenWriteBarrier(&___allowTroopsTGL_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
