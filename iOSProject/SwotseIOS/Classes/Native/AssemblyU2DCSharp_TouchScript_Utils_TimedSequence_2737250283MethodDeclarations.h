﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TouchScript_Utils_TimedSequence_1172103199MethodDeclarations.h"

// System.Void TouchScript.Utils.TimedSequence`1<TouchScript.TouchPoint>::.ctor()
#define TimedSequence_1__ctor_m3386061368(__this, method) ((  void (*) (TimedSequence_1_t2737250283 *, const MethodInfo*))TimedSequence_1__ctor_m3458058545_gshared)(__this, method)
// System.Void TouchScript.Utils.TimedSequence`1<TouchScript.TouchPoint>::Add(T)
#define TimedSequence_1_Add_m3676247559(__this, ___element0, method) ((  void (*) (TimedSequence_1_t2737250283 *, TouchPoint_t959629083 *, const MethodInfo*))TimedSequence_1_Add_m2046982790_gshared)(__this, ___element0, method)
// System.Void TouchScript.Utils.TimedSequence`1<TouchScript.TouchPoint>::Add(T,System.Single)
#define TimedSequence_1_Add_m391708036(__this, ___element0, ___time1, method) ((  void (*) (TimedSequence_1_t2737250283 *, TouchPoint_t959629083 *, float, const MethodInfo*))TimedSequence_1_Add_m1389480837_gshared)(__this, ___element0, ___time1, method)
// System.Void TouchScript.Utils.TimedSequence`1<TouchScript.TouchPoint>::Clear()
#define TimedSequence_1_Clear_m3682561267(__this, method) ((  void (*) (TimedSequence_1_t2737250283 *, const MethodInfo*))TimedSequence_1_Clear_m2637068538_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> TouchScript.Utils.TimedSequence`1<TouchScript.TouchPoint>::FindElementsLaterThan(System.Single)
#define TimedSequence_1_FindElementsLaterThan_m3293929796(__this, ___time0, method) ((  Il2CppObject* (*) (TimedSequence_1_t2737250283 *, float, const MethodInfo*))TimedSequence_1_FindElementsLaterThan_m1419642373_gshared)(__this, ___time0, method)
// System.Collections.Generic.IList`1<T> TouchScript.Utils.TimedSequence`1<TouchScript.TouchPoint>::FindElementsLaterThan(System.Single,System.Single&)
#define TimedSequence_1_FindElementsLaterThan_m3113087177(__this, ___time0, ___lastTime1, method) ((  Il2CppObject* (*) (TimedSequence_1_t2737250283 *, float, float*, const MethodInfo*))TimedSequence_1_FindElementsLaterThan_m543828678_gshared)(__this, ___time0, ___lastTime1, method)
// System.Collections.Generic.IList`1<T> TouchScript.Utils.TimedSequence`1<TouchScript.TouchPoint>::FindElementsLaterThan(System.Single,System.Predicate`1<T>)
#define TimedSequence_1_FindElementsLaterThan_m3665241035(__this, ___time0, ___predicate1, method) ((  Il2CppObject* (*) (TimedSequence_1_t2737250283 *, float, Predicate_1_t3697566494 *, const MethodInfo*))TimedSequence_1_FindElementsLaterThan_m1289799938_gshared)(__this, ___time0, ___predicate1, method)
