﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BattleReportContentManager/<GetBattleReport>c__Iterator7
struct U3CGetBattleReportU3Ec__Iterator7_t407393645;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BattleReportContentManager/<GetBattleReport>c__Iterator7::.ctor()
extern "C"  void U3CGetBattleReportU3Ec__Iterator7__ctor_m1509253818 (U3CGetBattleReportU3Ec__Iterator7_t407393645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BattleReportContentManager/<GetBattleReport>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetBattleReportU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1580917590 (U3CGetBattleReportU3Ec__Iterator7_t407393645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BattleReportContentManager/<GetBattleReport>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetBattleReportU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m367399118 (U3CGetBattleReportU3Ec__Iterator7_t407393645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BattleReportContentManager/<GetBattleReport>c__Iterator7::MoveNext()
extern "C"  bool U3CGetBattleReportU3Ec__Iterator7_MoveNext_m397785794 (U3CGetBattleReportU3Ec__Iterator7_t407393645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportContentManager/<GetBattleReport>c__Iterator7::Dispose()
extern "C"  void U3CGetBattleReportU3Ec__Iterator7_Dispose_m357654503 (U3CGetBattleReportU3Ec__Iterator7_t407393645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleReportContentManager/<GetBattleReport>c__Iterator7::Reset()
extern "C"  void U3CGetBattleReportU3Ec__Iterator7_Reset_m1323403873 (U3CGetBattleReportU3Ec__Iterator7_t407393645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
