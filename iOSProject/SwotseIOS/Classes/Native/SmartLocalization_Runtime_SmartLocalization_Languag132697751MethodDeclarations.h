﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmartLocalization.LanguageManager
struct LanguageManager_t132697751;
// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject>
struct SortedDictionary_2_t1165316627;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.String
struct String_t;
// SmartLocalization.SmartCultureInfo
struct SmartCultureInfo_t2361725737;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo>
struct List_1_t1730846869;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Texture
struct Texture_t2243626319;
// SmartLocalization.LocalizedObject
struct LocalizedObject_t1895892772;

#include "codegen/il2cpp-codegen.h"
#include "SmartLocalization_Runtime_SmartLocalization_SmartC2361725737.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SmartLocalization.LanguageManager::.ctor()
extern "C"  void LanguageManager__ctor_m4088089411 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.LanguageManager SmartLocalization.LanguageManager::get_Instance()
extern "C"  LanguageManager_t132697751 * LanguageManager_get_Instance_m2875764269 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageManager::SetDontDestroyOnLoad()
extern "C"  void LanguageManager_SetDontDestroyOnLoad_m2617601001 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::get_HasInstance()
extern "C"  bool LanguageManager_get_HasInstance_m206740141 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageManager::OnAfterDeserialize()
extern "C"  void LanguageManager_OnAfterDeserialize_m527312453 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageManager::OnBeforeSerialize()
extern "C"  void LanguageManager_OnBeforeSerialize_m3829736889 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.SortedDictionary`2<System.String,SmartLocalization.LocalizedObject> SmartLocalization.LanguageManager::get_LanguageDatabase()
extern "C"  SortedDictionary_2_t1165316627 * LanguageManager_get_LanguageDatabase_m1793999086 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> SmartLocalization.LanguageManager::get_RawTextDatabase()
extern "C"  Dictionary_2_t3943999495 * LanguageManager_get_RawTextDatabase_m1938546828 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SmartLocalization.LanguageManager::get_NumberOfSupportedLanguages()
extern "C"  int32_t LanguageManager_get_NumberOfSupportedLanguages_m1755821309 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LanguageManager::get_LoadedLanguage()
extern "C"  String_t* LanguageManager_get_LoadedLanguage_m3381770834 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageManager::get_CurrentlyLoadedCulture()
extern "C"  SmartCultureInfo_t2361725737 * LanguageManager_get_CurrentlyLoadedCulture_m1292866637 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::get_VerboseLogging()
extern "C"  bool LanguageManager_get_VerboseLogging_m3869483067 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageManager::set_VerboseLogging(System.Boolean)
extern "C"  void LanguageManager_set_VerboseLogging_m2012681704 (LanguageManager_t132697751 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageManager::Awake()
extern "C"  void LanguageManager_Awake_m2615260972 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageManager::OnDestroy()
extern "C"  void LanguageManager_OnDestroy_m1052292842 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageManager::OnApplicationQuit()
extern "C"  void LanguageManager_OnApplicationQuit_m4235799921 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::LoadAvailableCultures()
extern "C"  bool LanguageManager_LoadAvailableCultures_m1963961867 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> SmartLocalization.LanguageManager::GetAllKeys()
extern "C"  List_1_t1398341365 * LanguageManager_GetAllKeys_m1355126019 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageManager::ChangeLanguage(SmartLocalization.SmartCultureInfo)
extern "C"  void LanguageManager_ChangeLanguage_m4053385628 (LanguageManager_t132697751 * __this, SmartCultureInfo_t2361725737 * ___cultureInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageManager::ChangeLanguage(System.String)
extern "C"  void LanguageManager_ChangeLanguage_m2181317469 (LanguageManager_t132697751 * __this, String_t* ___languageCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageManager::ChangeLanguageWithData(System.String,System.String)
extern "C"  void LanguageManager_ChangeLanguageWithData_m1866284381 (LanguageManager_t132697751 * __this, String_t* ___languageDataInResX0, String_t* ___languageCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::AppendLanguageWithTextData(System.String)
extern "C"  bool LanguageManager_AppendLanguageWithTextData_m1627605888 (LanguageManager_t132697751 * __this, String_t* ___languageDataInResX0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::LoadLanguage(System.String,System.String)
extern "C"  bool LanguageManager_LoadLanguage_m2984433719 (LanguageManager_t132697751 * __this, String_t* ___languageData0, String_t* ___languageCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::IsLanguageSupported(System.String)
extern "C"  bool LanguageManager_IsLanguageSupported_m1386301863 (LanguageManager_t132697751 * __this, String_t* ___languageCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::IsLanguageSupported(SmartLocalization.SmartCultureInfo)
extern "C"  bool LanguageManager_IsLanguageSupported_m2539554054 (LanguageManager_t132697751 * __this, SmartCultureInfo_t2361725737 * ___cultureInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageManager::GetSupportedSystemLanguage()
extern "C"  SmartCultureInfo_t2361725737 * LanguageManager_GetSupportedSystemLanguage_m369865526 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LanguageManager::GetSupportedSystemLanguageCode()
extern "C"  String_t* LanguageManager_GetSupportedSystemLanguageCode_m3281160292 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageManager::GetDeviceCultureIfSupported()
extern "C"  SmartCultureInfo_t2361725737 * LanguageManager_GetDeviceCultureIfSupported_m2565994852 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::IsCultureSupported(System.String)
extern "C"  bool LanguageManager_IsCultureSupported_m2114536949 (LanguageManager_t132697751 * __this, String_t* ___languageCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::IsCultureSupported(SmartLocalization.SmartCultureInfo)
extern "C"  bool LanguageManager_IsCultureSupported_m588104114 (LanguageManager_t132697751 * __this, SmartCultureInfo_t2361725737 * ___cultureInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::IsLanguageSupportedEnglishName(System.String)
extern "C"  bool LanguageManager_IsLanguageSupportedEnglishName_m2156161210 (LanguageManager_t132697751 * __this, String_t* ___englishName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.SmartCultureInfo SmartLocalization.LanguageManager::GetCultureInfo(System.String)
extern "C"  SmartCultureInfo_t2361725737 * LanguageManager_GetCultureInfo_m927369937 (LanguageManager_t132697751 * __this, String_t* ___languageCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LanguageManager::GetSystemLanguageEnglishName()
extern "C"  String_t* LanguageManager_GetSystemLanguageEnglishName_m2820364698 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<SmartLocalization.SmartCultureInfo> SmartLocalization.LanguageManager::GetSupportedLanguages()
extern "C"  List_1_t1730846869 * LanguageManager_GetSupportedLanguages_m3382466416 (LanguageManager_t132697751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> SmartLocalization.LanguageManager::GetKeysWithinCategory(System.String)
extern "C"  List_1_t1398341365 * LanguageManager_GetKeysWithinCategory_m1336090217 (LanguageManager_t132697751 * __this, String_t* ___category0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SmartLocalization.LanguageManager::GetTextValue(System.String)
extern "C"  String_t* LanguageManager_GetTextValue_m2026310594 (LanguageManager_t132697751 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip SmartLocalization.LanguageManager::GetAudioClip(System.String)
extern "C"  AudioClip_t1932558630 * LanguageManager_GetAudioClip_m1526950403 (LanguageManager_t132697751 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject SmartLocalization.LanguageManager::GetPrefab(System.String)
extern "C"  GameObject_t1756533147 * LanguageManager_GetPrefab_m4033481276 (LanguageManager_t132697751 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture SmartLocalization.LanguageManager::GetTexture(System.String)
extern "C"  Texture_t2243626319 * LanguageManager_GetTexture_m1189186051 (LanguageManager_t132697751 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::HasKey(System.String)
extern "C"  bool LanguageManager_HasKey_m729408602 (LanguageManager_t132697751 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SmartLocalization.LocalizedObject SmartLocalization.LanguageManager::GetLocalizedObject(System.String)
extern "C"  LocalizedObject_t1895892772 * LanguageManager_GetLocalizedObject_m2863814112 (LanguageManager_t132697751 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmartLocalization.LanguageManager::.cctor()
extern "C"  void LanguageManager__cctor_m218978862 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SmartLocalization.LanguageManager::<Awake>m__0(SmartLocalization.SmartCultureInfo)
extern "C"  bool LanguageManager_U3CAwakeU3Em__0_m359043638 (LanguageManager_t132697751 * __this, SmartCultureInfo_t2361725737 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
