﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Win32IPGlobalStatistics
struct Win32IPGlobalStatistics_t3349972664;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_Win32_MIB_IPS1777025181.h"

// System.Void System.Net.NetworkInformation.Win32IPGlobalStatistics::.ctor(System.Net.NetworkInformation.Win32_MIB_IPSTATS)
extern "C"  void Win32IPGlobalStatistics__ctor_m980078935 (Win32IPGlobalStatistics_t3349972664 * __this, Win32_MIB_IPSTATS_t1777025181  ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_DefaultTtl()
extern "C"  int32_t Win32IPGlobalStatistics_get_DefaultTtl_m1837145886 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.Win32IPGlobalStatistics::get_ForwardingEnabled()
extern "C"  bool Win32IPGlobalStatistics_get_ForwardingEnabled_m4185187105 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_NumberOfInterfaces()
extern "C"  int32_t Win32IPGlobalStatistics_get_NumberOfInterfaces_m2375261005 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_NumberOfIPAddresses()
extern "C"  int32_t Win32IPGlobalStatistics_get_NumberOfIPAddresses_m1990415442 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_NumberOfRoutes()
extern "C"  int32_t Win32IPGlobalStatistics_get_NumberOfRoutes_m63756765 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_OutputPacketRequests()
extern "C"  int64_t Win32IPGlobalStatistics_get_OutputPacketRequests_m2109763071 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_OutputPacketRoutingDiscards()
extern "C"  int64_t Win32IPGlobalStatistics_get_OutputPacketRoutingDiscards_m74845450 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_OutputPacketsDiscarded()
extern "C"  int64_t Win32IPGlobalStatistics_get_OutputPacketsDiscarded_m3933999513 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_OutputPacketsWithNoRoute()
extern "C"  int64_t Win32IPGlobalStatistics_get_OutputPacketsWithNoRoute_m2248419240 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_PacketFragmentFailures()
extern "C"  int64_t Win32IPGlobalStatistics_get_PacketFragmentFailures_m3061997469 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_PacketReassembliesRequired()
extern "C"  int64_t Win32IPGlobalStatistics_get_PacketReassembliesRequired_m1529125746 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_PacketReassemblyFailures()
extern "C"  int64_t Win32IPGlobalStatistics_get_PacketReassemblyFailures_m2744278200 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_PacketReassemblyTimeout()
extern "C"  int64_t Win32IPGlobalStatistics_get_PacketReassemblyTimeout_m1067166200 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_PacketsFragmented()
extern "C"  int64_t Win32IPGlobalStatistics_get_PacketsFragmented_m142491256 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_PacketsReassembled()
extern "C"  int64_t Win32IPGlobalStatistics_get_PacketsReassembled_m2495688294 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_ReceivedPackets()
extern "C"  int64_t Win32IPGlobalStatistics_get_ReceivedPackets_m3578634908 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_ReceivedPacketsDelivered()
extern "C"  int64_t Win32IPGlobalStatistics_get_ReceivedPacketsDelivered_m3059117906 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_ReceivedPacketsDiscarded()
extern "C"  int64_t Win32IPGlobalStatistics_get_ReceivedPacketsDiscarded_m790834721 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_ReceivedPacketsForwarded()
extern "C"  int64_t Win32IPGlobalStatistics_get_ReceivedPacketsForwarded_m4187420190 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_ReceivedPacketsWithAddressErrors()
extern "C"  int64_t Win32IPGlobalStatistics_get_ReceivedPacketsWithAddressErrors_m590403717 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_ReceivedPacketsWithHeadersErrors()
extern "C"  int64_t Win32IPGlobalStatistics_get_ReceivedPacketsWithHeadersErrors_m728738799 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.Win32IPGlobalStatistics::get_ReceivedPacketsWithUnknownProtocol()
extern "C"  int64_t Win32IPGlobalStatistics_get_ReceivedPacketsWithUnknownProtocol_m3621042184 (Win32IPGlobalStatistics_t3349972664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
