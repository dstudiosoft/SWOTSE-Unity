﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Layers.ProjectionParams
struct ProjectionParams_t2712959773;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Plane3727654732.h"

// System.Void TouchScript.Layers.ProjectionParams::.ctor()
extern "C"  void ProjectionParams__ctor_m1113291784 (ProjectionParams_t2712959773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TouchScript.Layers.ProjectionParams::ProjectTo(UnityEngine.Vector2,UnityEngine.Plane)
extern "C"  Vector3_t2243707580  ProjectionParams_ProjectTo_m1662947769 (ProjectionParams_t2712959773 * __this, Vector2_t2243707579  ___screenPosition0, Plane_t3727654732  ___projectionPlane1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Layers.ProjectionParams::ProjectFrom(UnityEngine.Vector3)
extern "C"  Vector2_t2243707579  ProjectionParams_ProjectFrom_m3928074933 (ProjectionParams_t2712959773 * __this, Vector3_t2243707580  ___worldPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
