﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShellContentManager
struct ShellContentManager_t407410174;
// System.String
struct String_t;
// WorldFieldModel
struct WorldFieldModel_t3469935653;
// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_WorldFieldModel3469935653.h"

// System.Void ShellContentManager::.ctor()
extern "C"  void ShellContentManager__ctor_m2132663935 (ShellContentManager_t407410174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShellContentManager::UpdateInfo(System.String)
extern "C"  void ShellContentManager_UpdateInfo_m279305574 (ShellContentManager_t407410174 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShellContentManager::UpdateBuildingInfo(System.Int64,System.String)
extern "C"  void ShellContentManager_UpdateBuildingInfo_m3273482868 (ShellContentManager_t407410174 * __this, int64_t ___pitId0, String_t* ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShellContentManager::UpdateDispatchWindow(WorldFieldModel,System.Boolean)
extern "C"  void ShellContentManager_UpdateDispatchWindow_m3039545998 (ShellContentManager_t407410174 * __this, WorldFieldModel_t3469935653 * ___worldField0, bool ___fromMap1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShellContentManager::TabOnClick(System.Int32)
extern "C"  void ShellContentManager_TabOnClick_m263847894 (ShellContentManager_t407410174 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShellContentManager::SetTabs(System.String[],UnityEngine.GameObject[])
extern "C"  void ShellContentManager_SetTabs_m2743754727 (ShellContentManager_t407410174 * __this, StringU5BU5D_t1642385972* ___tabsNames0, GameObjectU5BU5D_t3057952154* ___contents1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShellContentManager::MoreOnClick()
extern "C"  void ShellContentManager_MoreOnClick_m3287949037 (ShellContentManager_t407410174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
