﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// MarketBuyTableController
struct MarketBuyTableController_t1709776860;
// MarketSellTableController
struct MarketSellTableController_t4072176656;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.String
struct String_t;
// SimpleEvent
struct SimpleEvent_t1509017202;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarketManager
struct  MarketManager_t2047363881  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text MarketManager::goldValue
	Text_t356221433 * ___goldValue_2;
	// UnityEngine.UI.Text MarketManager::silverValue
	Text_t356221433 * ___silverValue_3;
	// UnityEngine.UI.Text MarketManager::resourceLabel
	Text_t356221433 * ___resourceLabel_4;
	// UnityEngine.UI.InputField MarketManager::buyCount
	InputField_t1631627530 * ___buyCount_5;
	// UnityEngine.UI.InputField MarketManager::sellCount
	InputField_t1631627530 * ___sellCount_6;
	// UnityEngine.UI.InputField MarketManager::sellUnitPrice
	InputField_t1631627530 * ___sellUnitPrice_7;
	// UnityEngine.UI.Text MarketManager::buyUnitPrice
	Text_t356221433 * ___buyUnitPrice_8;
	// UnityEngine.UI.Text MarketManager::buyComission
	Text_t356221433 * ___buyComission_9;
	// UnityEngine.UI.Text MarketManager::buyTotal
	Text_t356221433 * ___buyTotal_10;
	// UnityEngine.UI.Text MarketManager::sellComission
	Text_t356221433 * ___sellComission_11;
	// UnityEngine.UI.Text MarketManager::sellTotal
	Text_t356221433 * ___sellTotal_12;
	// MarketBuyTableController MarketManager::buyTable
	MarketBuyTableController_t1709776860 * ___buyTable_13;
	// MarketSellTableController MarketManager::sellTable
	MarketSellTableController_t4072176656 * ___sellTable_14;
	// System.Collections.ArrayList MarketManager::lotsToBuy
	ArrayList_t4252133567 * ___lotsToBuy_15;
	// System.Collections.ArrayList MarketManager::lotsToSell
	ArrayList_t4252133567 * ___lotsToSell_16;
	// System.String MarketManager::currentResource
	String_t* ___currentResource_17;
	// SimpleEvent MarketManager::onBuyLotsUpdated
	SimpleEvent_t1509017202 * ___onBuyLotsUpdated_18;
	// SimpleEvent MarketManager::onSellLotsUpdated
	SimpleEvent_t1509017202 * ___onSellLotsUpdated_19;
	// SimpleEvent MarketManager::onResourceBought
	SimpleEvent_t1509017202 * ___onResourceBought_20;
	// SimpleEvent MarketManager::onResourceSold
	SimpleEvent_t1509017202 * ___onResourceSold_21;
	// SimpleEvent MarketManager::onLotRetracted
	SimpleEvent_t1509017202 * ___onLotRetracted_22;

public:
	inline static int32_t get_offset_of_goldValue_2() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___goldValue_2)); }
	inline Text_t356221433 * get_goldValue_2() const { return ___goldValue_2; }
	inline Text_t356221433 ** get_address_of_goldValue_2() { return &___goldValue_2; }
	inline void set_goldValue_2(Text_t356221433 * value)
	{
		___goldValue_2 = value;
		Il2CppCodeGenWriteBarrier(&___goldValue_2, value);
	}

	inline static int32_t get_offset_of_silverValue_3() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___silverValue_3)); }
	inline Text_t356221433 * get_silverValue_3() const { return ___silverValue_3; }
	inline Text_t356221433 ** get_address_of_silverValue_3() { return &___silverValue_3; }
	inline void set_silverValue_3(Text_t356221433 * value)
	{
		___silverValue_3 = value;
		Il2CppCodeGenWriteBarrier(&___silverValue_3, value);
	}

	inline static int32_t get_offset_of_resourceLabel_4() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___resourceLabel_4)); }
	inline Text_t356221433 * get_resourceLabel_4() const { return ___resourceLabel_4; }
	inline Text_t356221433 ** get_address_of_resourceLabel_4() { return &___resourceLabel_4; }
	inline void set_resourceLabel_4(Text_t356221433 * value)
	{
		___resourceLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___resourceLabel_4, value);
	}

	inline static int32_t get_offset_of_buyCount_5() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___buyCount_5)); }
	inline InputField_t1631627530 * get_buyCount_5() const { return ___buyCount_5; }
	inline InputField_t1631627530 ** get_address_of_buyCount_5() { return &___buyCount_5; }
	inline void set_buyCount_5(InputField_t1631627530 * value)
	{
		___buyCount_5 = value;
		Il2CppCodeGenWriteBarrier(&___buyCount_5, value);
	}

	inline static int32_t get_offset_of_sellCount_6() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___sellCount_6)); }
	inline InputField_t1631627530 * get_sellCount_6() const { return ___sellCount_6; }
	inline InputField_t1631627530 ** get_address_of_sellCount_6() { return &___sellCount_6; }
	inline void set_sellCount_6(InputField_t1631627530 * value)
	{
		___sellCount_6 = value;
		Il2CppCodeGenWriteBarrier(&___sellCount_6, value);
	}

	inline static int32_t get_offset_of_sellUnitPrice_7() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___sellUnitPrice_7)); }
	inline InputField_t1631627530 * get_sellUnitPrice_7() const { return ___sellUnitPrice_7; }
	inline InputField_t1631627530 ** get_address_of_sellUnitPrice_7() { return &___sellUnitPrice_7; }
	inline void set_sellUnitPrice_7(InputField_t1631627530 * value)
	{
		___sellUnitPrice_7 = value;
		Il2CppCodeGenWriteBarrier(&___sellUnitPrice_7, value);
	}

	inline static int32_t get_offset_of_buyUnitPrice_8() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___buyUnitPrice_8)); }
	inline Text_t356221433 * get_buyUnitPrice_8() const { return ___buyUnitPrice_8; }
	inline Text_t356221433 ** get_address_of_buyUnitPrice_8() { return &___buyUnitPrice_8; }
	inline void set_buyUnitPrice_8(Text_t356221433 * value)
	{
		___buyUnitPrice_8 = value;
		Il2CppCodeGenWriteBarrier(&___buyUnitPrice_8, value);
	}

	inline static int32_t get_offset_of_buyComission_9() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___buyComission_9)); }
	inline Text_t356221433 * get_buyComission_9() const { return ___buyComission_9; }
	inline Text_t356221433 ** get_address_of_buyComission_9() { return &___buyComission_9; }
	inline void set_buyComission_9(Text_t356221433 * value)
	{
		___buyComission_9 = value;
		Il2CppCodeGenWriteBarrier(&___buyComission_9, value);
	}

	inline static int32_t get_offset_of_buyTotal_10() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___buyTotal_10)); }
	inline Text_t356221433 * get_buyTotal_10() const { return ___buyTotal_10; }
	inline Text_t356221433 ** get_address_of_buyTotal_10() { return &___buyTotal_10; }
	inline void set_buyTotal_10(Text_t356221433 * value)
	{
		___buyTotal_10 = value;
		Il2CppCodeGenWriteBarrier(&___buyTotal_10, value);
	}

	inline static int32_t get_offset_of_sellComission_11() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___sellComission_11)); }
	inline Text_t356221433 * get_sellComission_11() const { return ___sellComission_11; }
	inline Text_t356221433 ** get_address_of_sellComission_11() { return &___sellComission_11; }
	inline void set_sellComission_11(Text_t356221433 * value)
	{
		___sellComission_11 = value;
		Il2CppCodeGenWriteBarrier(&___sellComission_11, value);
	}

	inline static int32_t get_offset_of_sellTotal_12() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___sellTotal_12)); }
	inline Text_t356221433 * get_sellTotal_12() const { return ___sellTotal_12; }
	inline Text_t356221433 ** get_address_of_sellTotal_12() { return &___sellTotal_12; }
	inline void set_sellTotal_12(Text_t356221433 * value)
	{
		___sellTotal_12 = value;
		Il2CppCodeGenWriteBarrier(&___sellTotal_12, value);
	}

	inline static int32_t get_offset_of_buyTable_13() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___buyTable_13)); }
	inline MarketBuyTableController_t1709776860 * get_buyTable_13() const { return ___buyTable_13; }
	inline MarketBuyTableController_t1709776860 ** get_address_of_buyTable_13() { return &___buyTable_13; }
	inline void set_buyTable_13(MarketBuyTableController_t1709776860 * value)
	{
		___buyTable_13 = value;
		Il2CppCodeGenWriteBarrier(&___buyTable_13, value);
	}

	inline static int32_t get_offset_of_sellTable_14() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___sellTable_14)); }
	inline MarketSellTableController_t4072176656 * get_sellTable_14() const { return ___sellTable_14; }
	inline MarketSellTableController_t4072176656 ** get_address_of_sellTable_14() { return &___sellTable_14; }
	inline void set_sellTable_14(MarketSellTableController_t4072176656 * value)
	{
		___sellTable_14 = value;
		Il2CppCodeGenWriteBarrier(&___sellTable_14, value);
	}

	inline static int32_t get_offset_of_lotsToBuy_15() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___lotsToBuy_15)); }
	inline ArrayList_t4252133567 * get_lotsToBuy_15() const { return ___lotsToBuy_15; }
	inline ArrayList_t4252133567 ** get_address_of_lotsToBuy_15() { return &___lotsToBuy_15; }
	inline void set_lotsToBuy_15(ArrayList_t4252133567 * value)
	{
		___lotsToBuy_15 = value;
		Il2CppCodeGenWriteBarrier(&___lotsToBuy_15, value);
	}

	inline static int32_t get_offset_of_lotsToSell_16() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___lotsToSell_16)); }
	inline ArrayList_t4252133567 * get_lotsToSell_16() const { return ___lotsToSell_16; }
	inline ArrayList_t4252133567 ** get_address_of_lotsToSell_16() { return &___lotsToSell_16; }
	inline void set_lotsToSell_16(ArrayList_t4252133567 * value)
	{
		___lotsToSell_16 = value;
		Il2CppCodeGenWriteBarrier(&___lotsToSell_16, value);
	}

	inline static int32_t get_offset_of_currentResource_17() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___currentResource_17)); }
	inline String_t* get_currentResource_17() const { return ___currentResource_17; }
	inline String_t** get_address_of_currentResource_17() { return &___currentResource_17; }
	inline void set_currentResource_17(String_t* value)
	{
		___currentResource_17 = value;
		Il2CppCodeGenWriteBarrier(&___currentResource_17, value);
	}

	inline static int32_t get_offset_of_onBuyLotsUpdated_18() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___onBuyLotsUpdated_18)); }
	inline SimpleEvent_t1509017202 * get_onBuyLotsUpdated_18() const { return ___onBuyLotsUpdated_18; }
	inline SimpleEvent_t1509017202 ** get_address_of_onBuyLotsUpdated_18() { return &___onBuyLotsUpdated_18; }
	inline void set_onBuyLotsUpdated_18(SimpleEvent_t1509017202 * value)
	{
		___onBuyLotsUpdated_18 = value;
		Il2CppCodeGenWriteBarrier(&___onBuyLotsUpdated_18, value);
	}

	inline static int32_t get_offset_of_onSellLotsUpdated_19() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___onSellLotsUpdated_19)); }
	inline SimpleEvent_t1509017202 * get_onSellLotsUpdated_19() const { return ___onSellLotsUpdated_19; }
	inline SimpleEvent_t1509017202 ** get_address_of_onSellLotsUpdated_19() { return &___onSellLotsUpdated_19; }
	inline void set_onSellLotsUpdated_19(SimpleEvent_t1509017202 * value)
	{
		___onSellLotsUpdated_19 = value;
		Il2CppCodeGenWriteBarrier(&___onSellLotsUpdated_19, value);
	}

	inline static int32_t get_offset_of_onResourceBought_20() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___onResourceBought_20)); }
	inline SimpleEvent_t1509017202 * get_onResourceBought_20() const { return ___onResourceBought_20; }
	inline SimpleEvent_t1509017202 ** get_address_of_onResourceBought_20() { return &___onResourceBought_20; }
	inline void set_onResourceBought_20(SimpleEvent_t1509017202 * value)
	{
		___onResourceBought_20 = value;
		Il2CppCodeGenWriteBarrier(&___onResourceBought_20, value);
	}

	inline static int32_t get_offset_of_onResourceSold_21() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___onResourceSold_21)); }
	inline SimpleEvent_t1509017202 * get_onResourceSold_21() const { return ___onResourceSold_21; }
	inline SimpleEvent_t1509017202 ** get_address_of_onResourceSold_21() { return &___onResourceSold_21; }
	inline void set_onResourceSold_21(SimpleEvent_t1509017202 * value)
	{
		___onResourceSold_21 = value;
		Il2CppCodeGenWriteBarrier(&___onResourceSold_21, value);
	}

	inline static int32_t get_offset_of_onLotRetracted_22() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881, ___onLotRetracted_22)); }
	inline SimpleEvent_t1509017202 * get_onLotRetracted_22() const { return ___onLotRetracted_22; }
	inline SimpleEvent_t1509017202 ** get_address_of_onLotRetracted_22() { return &___onLotRetracted_22; }
	inline void set_onLotRetracted_22(SimpleEvent_t1509017202 * value)
	{
		___onLotRetracted_22 = value;
		Il2CppCodeGenWriteBarrier(&___onLotRetracted_22, value);
	}
};

struct MarketManager_t2047363881_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> MarketManager::<>f__switch$mapB
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24mapB_23;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> MarketManager::<>f__switch$mapC
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24mapC_24;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> MarketManager::<>f__switch$mapD
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24mapD_25;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> MarketManager::<>f__switch$mapE
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24mapE_26;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapB_23() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881_StaticFields, ___U3CU3Ef__switchU24mapB_23)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24mapB_23() const { return ___U3CU3Ef__switchU24mapB_23; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24mapB_23() { return &___U3CU3Ef__switchU24mapB_23; }
	inline void set_U3CU3Ef__switchU24mapB_23(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24mapB_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapB_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapC_24() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881_StaticFields, ___U3CU3Ef__switchU24mapC_24)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24mapC_24() const { return ___U3CU3Ef__switchU24mapC_24; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24mapC_24() { return &___U3CU3Ef__switchU24mapC_24; }
	inline void set_U3CU3Ef__switchU24mapC_24(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24mapC_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapC_24, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapD_25() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881_StaticFields, ___U3CU3Ef__switchU24mapD_25)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24mapD_25() const { return ___U3CU3Ef__switchU24mapD_25; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24mapD_25() { return &___U3CU3Ef__switchU24mapD_25; }
	inline void set_U3CU3Ef__switchU24mapD_25(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24mapD_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapD_25, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapE_26() { return static_cast<int32_t>(offsetof(MarketManager_t2047363881_StaticFields, ___U3CU3Ef__switchU24mapE_26)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24mapE_26() const { return ___U3CU3Ef__switchU24mapE_26; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24mapE_26() { return &___U3CU3Ef__switchU24mapE_26; }
	inline void set_U3CU3Ef__switchU24mapE_26(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24mapE_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapE_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
