﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// UnityEngine.Transform
struct Transform_t3275118058;
// TouchScript.Layers.TouchLayer
struct TouchLayer_t2635439978;
// TouchScript.InputSources.IInputSource
struct IInputSource_t3266560338;
// TouchScript.Layers.ProjectionParams
struct ProjectionParams_t2712959773;
// TouchScript.Tags
struct Tags_t1265380163;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_TouchScript_Hit_TouchHit4186847494.h"
#include "AssemblyU2DCSharp_TouchScript_Layers_TouchLayer2635439978.h"
#include "AssemblyU2DCSharp_TouchScript_Tags1265380163.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"

// System.Void TouchScript.TouchPoint::.ctor()
extern "C"  void TouchPoint__ctor_m1886716144 (TouchPoint_t959629083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TouchScript.TouchPoint::get_Id()
extern "C"  int32_t TouchPoint_get_Id_m1684689884 (TouchPoint_t959629083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchPoint::set_Id(System.Int32)
extern "C"  void TouchPoint_set_Id_m4123639295 (TouchPoint_t959629083 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform TouchScript.TouchPoint::get_Target()
extern "C"  Transform_t3275118058 * TouchPoint_get_Target_m2850604728 (TouchPoint_t959629083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchPoint::set_Target(UnityEngine.Transform)
extern "C"  void TouchPoint_set_Target_m2656979761 (TouchPoint_t959629083 * __this, Transform_t3275118058 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.TouchPoint::get_Position()
extern "C"  Vector2_t2243707579  TouchPoint_get_Position_m3500874649 (TouchPoint_t959629083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.TouchPoint::get_PreviousPosition()
extern "C"  Vector2_t2243707579  TouchPoint_get_PreviousPosition_m4110451066 (TouchPoint_t959629083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchPoint::set_PreviousPosition(UnityEngine.Vector2)
extern "C"  void TouchPoint_set_PreviousPosition_m926571083 (TouchPoint_t959629083 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Hit.TouchHit TouchScript.TouchPoint::get_Hit()
extern "C"  TouchHit_t4186847494  TouchPoint_get_Hit_m1350248960 (TouchPoint_t959629083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchPoint::set_Hit(TouchScript.Hit.TouchHit)
extern "C"  void TouchPoint_set_Hit_m4067094689 (TouchPoint_t959629083 * __this, TouchHit_t4186847494  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Layers.TouchLayer TouchScript.TouchPoint::get_Layer()
extern "C"  TouchLayer_t2635439978 * TouchPoint_get_Layer_m3772015011 (TouchPoint_t959629083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchPoint::set_Layer(TouchScript.Layers.TouchLayer)
extern "C"  void TouchPoint_set_Layer_m345391970 (TouchPoint_t959629083 * __this, TouchLayer_t2635439978 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.InputSources.IInputSource TouchScript.TouchPoint::get_InputSource()
extern "C"  Il2CppObject * TouchPoint_get_InputSource_m3363769671 (TouchPoint_t959629083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchPoint::set_InputSource(TouchScript.InputSources.IInputSource)
extern "C"  void TouchPoint_set_InputSource_m1788011602 (TouchPoint_t959629083 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Layers.ProjectionParams TouchScript.TouchPoint::get_ProjectionParams()
extern "C"  ProjectionParams_t2712959773 * TouchPoint_get_ProjectionParams_m1020173930 (TouchPoint_t959629083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchScript.Tags TouchScript.TouchPoint::get_Tags()
extern "C"  Tags_t1265380163 * TouchPoint_get_Tags_m4011083836 (TouchPoint_t959629083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchPoint::set_Tags(TouchScript.Tags)
extern "C"  void TouchPoint_set_Tags_m2287840575 (TouchPoint_t959629083 * __this, Tags_t1265380163 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> TouchScript.TouchPoint::get_Properties()
extern "C"  Dictionary_2_t309261261 * TouchPoint_get_Properties_m60390530 (TouchPoint_t959629083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchPoint::set_Properties(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void TouchPoint_set_Properties_m3548166157 (TouchPoint_t959629083 * __this, Dictionary_2_t309261261 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.TouchPoint::Equals(System.Object)
extern "C"  bool TouchPoint_Equals_m4091861039 (TouchPoint_t959629083 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.TouchPoint::Equals(TouchScript.TouchPoint)
extern "C"  bool TouchPoint_Equals_m4259314706 (TouchPoint_t959629083 * __this, TouchPoint_t959629083 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TouchScript.TouchPoint::GetHashCode()
extern "C"  int32_t TouchPoint_GetHashCode_m425208697 (TouchPoint_t959629083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchPoint::INTERNAL_Init(System.Int32,UnityEngine.Vector2,TouchScript.InputSources.IInputSource,TouchScript.Tags)
extern "C"  void TouchPoint_INTERNAL_Init_m2914723228 (TouchPoint_t959629083 * __this, int32_t ___id0, Vector2_t2243707579  ___position1, Il2CppObject * ___input2, Tags_t1265380163 * ___tags3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchPoint::INTERNAL_Reset()
extern "C"  void TouchPoint_INTERNAL_Reset_m3493844531 (TouchPoint_t959629083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchPoint::INTERNAL_ResetPosition()
extern "C"  void TouchPoint_INTERNAL_ResetPosition_m129931680 (TouchPoint_t959629083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchPoint::INTERNAL_SetPosition(UnityEngine.Vector2)
extern "C"  void TouchPoint_INTERNAL_SetPosition_m2401435625 (TouchPoint_t959629083 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.TouchPoint::INTERNAL_Retain()
extern "C"  void TouchPoint_INTERNAL_Retain_m3052421647 (TouchPoint_t959629083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TouchScript.TouchPoint::INTERNAL_Release()
extern "C"  int32_t TouchPoint_INTERNAL_Release_m2696812305 (TouchPoint_t959629083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
