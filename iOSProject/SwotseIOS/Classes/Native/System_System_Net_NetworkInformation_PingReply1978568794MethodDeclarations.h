﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.PingReply
struct PingReply_t1978568794;
// System.Net.IPAddress
struct IPAddress_t1399971723;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Net.NetworkInformation.PingOptions
struct PingOptions_t1261881264;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_IPAddress1399971723.h"
#include "System_System_Net_NetworkInformation_PingOptions1261881264.h"
#include "System_System_Net_NetworkInformation_IPStatus1105460603.h"

// System.Void System.Net.NetworkInformation.PingReply::.ctor(System.Net.IPAddress,System.Byte[],System.Net.NetworkInformation.PingOptions,System.Int64,System.Net.NetworkInformation.IPStatus)
extern "C"  void PingReply__ctor_m208657219 (PingReply_t1978568794 * __this, IPAddress_t1399971723 * ___address0, ByteU5BU5D_t3397334013* ___buffer1, PingOptions_t1261881264 * ___options2, int64_t ___roundtripTime3, int32_t ___status4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress System.Net.NetworkInformation.PingReply::get_Address()
extern "C"  IPAddress_t1399971723 * PingReply_get_Address_m4130397987 (PingReply_t1978568794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.NetworkInformation.PingReply::get_Buffer()
extern "C"  ByteU5BU5D_t3397334013* PingReply_get_Buffer_m2958119327 (PingReply_t1978568794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.PingOptions System.Net.NetworkInformation.PingReply::get_Options()
extern "C"  PingOptions_t1261881264 * PingReply_get_Options_m471476170 (PingReply_t1978568794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.NetworkInformation.PingReply::get_RoundtripTime()
extern "C"  int64_t PingReply_get_RoundtripTime_m2294175924 (PingReply_t1978568794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.NetworkInformation.IPStatus System.Net.NetworkInformation.PingReply::get_Status()
extern "C"  int32_t PingReply_get_Status_m2648230811 (PingReply_t1978568794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
