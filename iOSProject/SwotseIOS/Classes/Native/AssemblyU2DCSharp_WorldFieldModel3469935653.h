﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// CityModel
struct CityModel_t13392860;
// SimpleUserModel
struct SimpleUserModel_t3156372614;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldFieldModel
struct  WorldFieldModel_t3469935653  : public Il2CppObject
{
public:
	// System.Int64 WorldFieldModel::id
	int64_t ___id_0;
	// System.Int64 WorldFieldModel::posX
	int64_t ___posX_1;
	// System.Int64 WorldFieldModel::posY
	int64_t ___posY_2;
	// System.String WorldFieldModel::cityType
	String_t* ___cityType_3;
	// CityModel WorldFieldModel::city
	CityModel_t13392860 * ___city_4;
	// SimpleUserModel WorldFieldModel::owner
	SimpleUserModel_t3156372614 * ___owner_5;
	// System.Int64 WorldFieldModel::level
	int64_t ___level_6;
	// System.Int64 WorldFieldModel::production
	int64_t ___production_7;
	// System.String WorldFieldModel::region
	String_t* ___region_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(WorldFieldModel_t3469935653, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_posX_1() { return static_cast<int32_t>(offsetof(WorldFieldModel_t3469935653, ___posX_1)); }
	inline int64_t get_posX_1() const { return ___posX_1; }
	inline int64_t* get_address_of_posX_1() { return &___posX_1; }
	inline void set_posX_1(int64_t value)
	{
		___posX_1 = value;
	}

	inline static int32_t get_offset_of_posY_2() { return static_cast<int32_t>(offsetof(WorldFieldModel_t3469935653, ___posY_2)); }
	inline int64_t get_posY_2() const { return ___posY_2; }
	inline int64_t* get_address_of_posY_2() { return &___posY_2; }
	inline void set_posY_2(int64_t value)
	{
		___posY_2 = value;
	}

	inline static int32_t get_offset_of_cityType_3() { return static_cast<int32_t>(offsetof(WorldFieldModel_t3469935653, ___cityType_3)); }
	inline String_t* get_cityType_3() const { return ___cityType_3; }
	inline String_t** get_address_of_cityType_3() { return &___cityType_3; }
	inline void set_cityType_3(String_t* value)
	{
		___cityType_3 = value;
		Il2CppCodeGenWriteBarrier(&___cityType_3, value);
	}

	inline static int32_t get_offset_of_city_4() { return static_cast<int32_t>(offsetof(WorldFieldModel_t3469935653, ___city_4)); }
	inline CityModel_t13392860 * get_city_4() const { return ___city_4; }
	inline CityModel_t13392860 ** get_address_of_city_4() { return &___city_4; }
	inline void set_city_4(CityModel_t13392860 * value)
	{
		___city_4 = value;
		Il2CppCodeGenWriteBarrier(&___city_4, value);
	}

	inline static int32_t get_offset_of_owner_5() { return static_cast<int32_t>(offsetof(WorldFieldModel_t3469935653, ___owner_5)); }
	inline SimpleUserModel_t3156372614 * get_owner_5() const { return ___owner_5; }
	inline SimpleUserModel_t3156372614 ** get_address_of_owner_5() { return &___owner_5; }
	inline void set_owner_5(SimpleUserModel_t3156372614 * value)
	{
		___owner_5 = value;
		Il2CppCodeGenWriteBarrier(&___owner_5, value);
	}

	inline static int32_t get_offset_of_level_6() { return static_cast<int32_t>(offsetof(WorldFieldModel_t3469935653, ___level_6)); }
	inline int64_t get_level_6() const { return ___level_6; }
	inline int64_t* get_address_of_level_6() { return &___level_6; }
	inline void set_level_6(int64_t value)
	{
		___level_6 = value;
	}

	inline static int32_t get_offset_of_production_7() { return static_cast<int32_t>(offsetof(WorldFieldModel_t3469935653, ___production_7)); }
	inline int64_t get_production_7() const { return ___production_7; }
	inline int64_t* get_address_of_production_7() { return &___production_7; }
	inline void set_production_7(int64_t value)
	{
		___production_7 = value;
	}

	inline static int32_t get_offset_of_region_8() { return static_cast<int32_t>(offsetof(WorldFieldModel_t3469935653, ___region_8)); }
	inline String_t* get_region_8() const { return ___region_8; }
	inline String_t** get_address_of_region_8() { return &___region_8; }
	inline void set_region_8(String_t* value)
	{
		___region_8 = value;
		Il2CppCodeGenWriteBarrier(&___region_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
