﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.NetworkInformation.Win32LengthFlagsUnion
struct Win32LengthFlagsUnion_t1910118479;
struct Win32LengthFlagsUnion_t1910118479_marshaled_pinvoke;
struct Win32LengthFlagsUnion_t1910118479_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_NetworkInformation_Win32LengthFl1910118479.h"

// System.Boolean System.Net.NetworkInformation.Win32LengthFlagsUnion::get_IsDnsEligible()
extern "C"  bool Win32LengthFlagsUnion_get_IsDnsEligible_m1710307390 (Win32LengthFlagsUnion_t1910118479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.NetworkInformation.Win32LengthFlagsUnion::get_IsTransient()
extern "C"  bool Win32LengthFlagsUnion_get_IsTransient_m3516609582 (Win32LengthFlagsUnion_t1910118479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Win32LengthFlagsUnion_t1910118479;
struct Win32LengthFlagsUnion_t1910118479_marshaled_pinvoke;

extern "C" void Win32LengthFlagsUnion_t1910118479_marshal_pinvoke(const Win32LengthFlagsUnion_t1910118479& unmarshaled, Win32LengthFlagsUnion_t1910118479_marshaled_pinvoke& marshaled);
extern "C" void Win32LengthFlagsUnion_t1910118479_marshal_pinvoke_back(const Win32LengthFlagsUnion_t1910118479_marshaled_pinvoke& marshaled, Win32LengthFlagsUnion_t1910118479& unmarshaled);
extern "C" void Win32LengthFlagsUnion_t1910118479_marshal_pinvoke_cleanup(Win32LengthFlagsUnion_t1910118479_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Win32LengthFlagsUnion_t1910118479;
struct Win32LengthFlagsUnion_t1910118479_marshaled_com;

extern "C" void Win32LengthFlagsUnion_t1910118479_marshal_com(const Win32LengthFlagsUnion_t1910118479& unmarshaled, Win32LengthFlagsUnion_t1910118479_marshaled_com& marshaled);
extern "C" void Win32LengthFlagsUnion_t1910118479_marshal_com_back(const Win32LengthFlagsUnion_t1910118479_marshaled_com& marshaled, Win32LengthFlagsUnion_t1910118479& unmarshaled);
extern "C" void Win32LengthFlagsUnion_t1910118479_marshal_com_cleanup(Win32LengthFlagsUnion_t1910118479_marshaled_com& marshaled);
