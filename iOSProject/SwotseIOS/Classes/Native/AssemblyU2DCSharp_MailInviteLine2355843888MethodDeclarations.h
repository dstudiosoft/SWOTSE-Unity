﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MailInviteLine
struct MailInviteLine_t2355843888;
// MailInviteTableController
struct MailInviteTableController_t1221845970;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MailInviteTableController1221845970.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MailInviteLine::.ctor()
extern "C"  void MailInviteLine__ctor_m3697811149 (MailInviteLine_t2355843888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInviteLine::SetInviteId(System.Int64)
extern "C"  void MailInviteLine_SetInviteId_m3152241177 (MailInviteLine_t2355843888 * __this, int64_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInviteLine::SetOwner(MailInviteTableController)
extern "C"  void MailInviteLine_SetOwner_m411550200 (MailInviteLine_t2355843888 * __this, MailInviteTableController_t1221845970 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInviteLine::SetName(System.String)
extern "C"  void MailInviteLine_SetName_m172196830 (MailInviteLine_t2355843888 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInviteLine::SetMailDate(System.String)
extern "C"  void MailInviteLine_SetMailDate_m4176685410 (MailInviteLine_t2355843888 * __this, String_t* ___dateValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInviteLine::AcceptInvite()
extern "C"  void MailInviteLine_AcceptInvite_m3190218134 (MailInviteLine_t2355843888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MailInviteLine::RejectInvite()
extern "C"  void MailInviteLine_RejectInvite_m4085823879 (MailInviteLine_t2355843888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
