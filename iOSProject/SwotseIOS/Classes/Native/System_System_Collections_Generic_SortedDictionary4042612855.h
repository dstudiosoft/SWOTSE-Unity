﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// SmartLocalization.LocalizedObject
struct LocalizedObject_t1895892772;

#include "System_System_Collections_Generic_RBTree_Node2499136326.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedDictionary`2/Node<System.String,SmartLocalization.LocalizedObject>
struct  Node_t4042612855  : public Node_t2499136326
{
public:
	// TKey System.Collections.Generic.SortedDictionary`2/Node::key
	String_t* ___key_5;
	// TValue System.Collections.Generic.SortedDictionary`2/Node::value
	LocalizedObject_t1895892772 * ___value_6;

public:
	inline static int32_t get_offset_of_key_5() { return static_cast<int32_t>(offsetof(Node_t4042612855, ___key_5)); }
	inline String_t* get_key_5() const { return ___key_5; }
	inline String_t** get_address_of_key_5() { return &___key_5; }
	inline void set_key_5(String_t* value)
	{
		___key_5 = value;
		Il2CppCodeGenWriteBarrier(&___key_5, value);
	}

	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(Node_t4042612855, ___value_6)); }
	inline LocalizedObject_t1895892772 * get_value_6() const { return ___value_6; }
	inline LocalizedObject_t1895892772 ** get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(LocalizedObject_t1895892772 * value)
	{
		___value_6 = value;
		Il2CppCodeGenWriteBarrier(&___value_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
