﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllianceChat/<BeginChat>c__Iterator4
struct U3CBeginChatU3Ec__Iterator4_t3031594800;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AllianceChat/<BeginChat>c__Iterator4::.ctor()
extern "C"  void U3CBeginChatU3Ec__Iterator4__ctor_m533727109 (U3CBeginChatU3Ec__Iterator4_t3031594800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AllianceChat/<BeginChat>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBeginChatU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1894312131 (U3CBeginChatU3Ec__Iterator4_t3031594800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AllianceChat/<BeginChat>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBeginChatU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m4210034811 (U3CBeginChatU3Ec__Iterator4_t3031594800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AllianceChat/<BeginChat>c__Iterator4::MoveNext()
extern "C"  bool U3CBeginChatU3Ec__Iterator4_MoveNext_m1266349619 (U3CBeginChatU3Ec__Iterator4_t3031594800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceChat/<BeginChat>c__Iterator4::Dispose()
extern "C"  void U3CBeginChatU3Ec__Iterator4_Dispose_m2272474146 (U3CBeginChatU3Ec__Iterator4_t3031594800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllianceChat/<BeginChat>c__Iterator4::Reset()
extern "C"  void U3CBeginChatU3Ec__Iterator4_Reset_m3387961772 (U3CBeginChatU3Ec__Iterator4_t3031594800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
