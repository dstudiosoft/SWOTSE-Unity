﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CityChangerContentManager
struct CityChangerContentManager_t4168518203;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void CityChangerContentManager::.ctor()
extern "C"  void CityChangerContentManager__ctor_m3471345846 (CityChangerContentManager_t4168518203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityChangerContentManager::Start()
extern "C"  void CityChangerContentManager_Start_m1842974210 (CityChangerContentManager_t4168518203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityChangerContentManager::OnEnable()
extern "C"  void CityChangerContentManager_OnEnable_m2377758338 (CityChangerContentManager_t4168518203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityChangerContentManager::OnDisable()
extern "C"  void CityChangerContentManager_OnDisable_m1084979091 (CityChangerContentManager_t4168518203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityChangerContentManager::UpdateCityImage(System.Object,System.String)
extern "C"  void CityChangerContentManager_UpdateCityImage_m1856056907 (CityChangerContentManager_t4168518203 * __this, Il2CppObject * ___sender0, String_t* ___imageName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityChangerContentManager::UpdateCityCoords(System.Int64,System.Int64)
extern "C"  void CityChangerContentManager_UpdateCityCoords_m1237778142 (CityChangerContentManager_t4168518203 * __this, int64_t ___x0, int64_t ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CityChangerContentManager::ChangeCity(System.Int64)
extern "C"  Il2CppObject * CityChangerContentManager_ChangeCity_m2167429643 (CityChangerContentManager_t4168518203 * __this, int64_t ___cityId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityChangerContentManager::BuildNewCity(System.Int64,System.Int64,System.String)
extern "C"  void CityChangerContentManager_BuildNewCity_m1498680317 (CityChangerContentManager_t4168518203 * __this, int64_t ___x0, int64_t ___y1, String_t* ___name2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityChangerContentManager::NewCityHandler(System.Object,System.String)
extern "C"  void CityChangerContentManager_NewCityHandler_m1068254353 (CityChangerContentManager_t4168518203 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityChangerContentManager::UpdateViews(System.Object,System.String)
extern "C"  void CityChangerContentManager_UpdateViews_m2430794481 (CityChangerContentManager_t4168518203 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CityChangerContentManager::UpdateWorldMap(System.Object,System.String)
extern "C"  void CityChangerContentManager_UpdateWorldMap_m3893529077 (CityChangerContentManager_t4168518203 * __this, Il2CppObject * ___sender0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
