﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.DebuggableMonoBehaviour
struct DebuggableMonoBehaviour_t3136086048;

#include "codegen/il2cpp-codegen.h"

// System.Void TouchScript.DebuggableMonoBehaviour::.ctor()
extern "C"  void DebuggableMonoBehaviour__ctor_m1205059189 (DebuggableMonoBehaviour_t3136086048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.DebuggableMonoBehaviour::get_DebugMode()
extern "C"  bool DebuggableMonoBehaviour_get_DebugMode_m3301597266 (DebuggableMonoBehaviour_t3136086048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.DebuggableMonoBehaviour::set_DebugMode(System.Boolean)
extern "C"  void DebuggableMonoBehaviour_set_DebugMode_m3300469429 (DebuggableMonoBehaviour_t3136086048 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
