﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Clusters.Clusters
struct Clusters_t4089973855;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_TouchScript_TouchPoint959629083.h"

// System.Void TouchScript.Clusters.Clusters::.ctor()
extern "C"  void Clusters__ctor_m1026185145 (Clusters_t4089973855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TouchScript.Clusters.Clusters::get_PointsCount()
extern "C"  int32_t Clusters_get_PointsCount_m1866699450 (Clusters_t4089973855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Clusters.Clusters::get_MinPointsDistance()
extern "C"  float Clusters_get_MinPointsDistance_m2926211608 (Clusters_t4089973855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Clusters.Clusters::set_MinPointsDistance(System.Single)
extern "C"  void Clusters_set_MinPointsDistance_m733595071 (Clusters_t4089973855 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Clusters.Clusters::get_HasClusters()
extern "C"  bool Clusters_get_HasClusters_m1226368095 (Clusters_t4089973855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Clusters.Clusters::GetCenterPosition(System.Int32)
extern "C"  Vector2_t2243707579  Clusters_GetCenterPosition_m100688803 (Clusters_t4089973855 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TouchScript.Clusters.Clusters::GetPreviousCenterPosition(System.Int32)
extern "C"  Vector2_t2243707579  Clusters_GetPreviousCenterPosition_m2702146100 (Clusters_t4089973855 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Clusters.Clusters::AddPoint(TouchScript.TouchPoint)
extern "C"  void Clusters_AddPoint_m357524325 (Clusters_t4089973855 * __this, TouchPoint_t959629083 * ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Clusters.Clusters::AddPoints(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void Clusters_AddPoints_m3957830529 (Clusters_t4089973855 * __this, Il2CppObject* ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Clusters.Clusters::RemovePoint(TouchScript.TouchPoint)
extern "C"  void Clusters_RemovePoint_m417945588 (Clusters_t4089973855 * __this, TouchPoint_t959629083 * ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Clusters.Clusters::RemovePoints(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void Clusters_RemovePoints_m4255581984 (Clusters_t4089973855 * __this, Il2CppObject* ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Clusters.Clusters::RemoveAllPoints()
extern "C"  void Clusters_RemoveAllPoints_m2166957369 (Clusters_t4089973855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Clusters.Clusters::Invalidate()
extern "C"  void Clusters_Invalidate_m133446184 (Clusters_t4089973855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Clusters.Clusters::distributePoints()
extern "C"  void Clusters_distributePoints_m2308333949 (Clusters_t4089973855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TouchScript.Clusters.Clusters::checkClusters()
extern "C"  bool Clusters_checkClusters_m438313428 (Clusters_t4089973855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Clusters.Clusters::markDirty()
extern "C"  void Clusters_markDirty_m928748886 (Clusters_t4089973855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Clusters.Clusters::markClean()
extern "C"  void Clusters_markClean_m302027157 (Clusters_t4089973855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
