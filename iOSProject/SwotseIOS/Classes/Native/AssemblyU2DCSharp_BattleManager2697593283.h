﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int64,ArmyModel>
struct Dictionary_2_t3146639340;
// SimpleEvent
struct SimpleEvent_t1509017202;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleManager
struct  BattleManager_t2697593283  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int64,ArmyModel> BattleManager::cityArmies
	Dictionary_2_t3146639340 * ___cityArmies_0;
	// SimpleEvent BattleManager::onGetCityArmies
	SimpleEvent_t1509017202 * ___onGetCityArmies_1;
	// SimpleEvent BattleManager::onMarchLaunched
	SimpleEvent_t1509017202 * ___onMarchLaunched_2;
	// SimpleEvent BattleManager::onMarchCanceled
	SimpleEvent_t1509017202 * ___onMarchCanceled_3;
	// SimpleEvent BattleManager::onArmyReturnInitiated
	SimpleEvent_t1509017202 * ___onArmyReturnInitiated_4;

public:
	inline static int32_t get_offset_of_cityArmies_0() { return static_cast<int32_t>(offsetof(BattleManager_t2697593283, ___cityArmies_0)); }
	inline Dictionary_2_t3146639340 * get_cityArmies_0() const { return ___cityArmies_0; }
	inline Dictionary_2_t3146639340 ** get_address_of_cityArmies_0() { return &___cityArmies_0; }
	inline void set_cityArmies_0(Dictionary_2_t3146639340 * value)
	{
		___cityArmies_0 = value;
		Il2CppCodeGenWriteBarrier(&___cityArmies_0, value);
	}

	inline static int32_t get_offset_of_onGetCityArmies_1() { return static_cast<int32_t>(offsetof(BattleManager_t2697593283, ___onGetCityArmies_1)); }
	inline SimpleEvent_t1509017202 * get_onGetCityArmies_1() const { return ___onGetCityArmies_1; }
	inline SimpleEvent_t1509017202 ** get_address_of_onGetCityArmies_1() { return &___onGetCityArmies_1; }
	inline void set_onGetCityArmies_1(SimpleEvent_t1509017202 * value)
	{
		___onGetCityArmies_1 = value;
		Il2CppCodeGenWriteBarrier(&___onGetCityArmies_1, value);
	}

	inline static int32_t get_offset_of_onMarchLaunched_2() { return static_cast<int32_t>(offsetof(BattleManager_t2697593283, ___onMarchLaunched_2)); }
	inline SimpleEvent_t1509017202 * get_onMarchLaunched_2() const { return ___onMarchLaunched_2; }
	inline SimpleEvent_t1509017202 ** get_address_of_onMarchLaunched_2() { return &___onMarchLaunched_2; }
	inline void set_onMarchLaunched_2(SimpleEvent_t1509017202 * value)
	{
		___onMarchLaunched_2 = value;
		Il2CppCodeGenWriteBarrier(&___onMarchLaunched_2, value);
	}

	inline static int32_t get_offset_of_onMarchCanceled_3() { return static_cast<int32_t>(offsetof(BattleManager_t2697593283, ___onMarchCanceled_3)); }
	inline SimpleEvent_t1509017202 * get_onMarchCanceled_3() const { return ___onMarchCanceled_3; }
	inline SimpleEvent_t1509017202 ** get_address_of_onMarchCanceled_3() { return &___onMarchCanceled_3; }
	inline void set_onMarchCanceled_3(SimpleEvent_t1509017202 * value)
	{
		___onMarchCanceled_3 = value;
		Il2CppCodeGenWriteBarrier(&___onMarchCanceled_3, value);
	}

	inline static int32_t get_offset_of_onArmyReturnInitiated_4() { return static_cast<int32_t>(offsetof(BattleManager_t2697593283, ___onArmyReturnInitiated_4)); }
	inline SimpleEvent_t1509017202 * get_onArmyReturnInitiated_4() const { return ___onArmyReturnInitiated_4; }
	inline SimpleEvent_t1509017202 ** get_address_of_onArmyReturnInitiated_4() { return &___onArmyReturnInitiated_4; }
	inline void set_onArmyReturnInitiated_4(SimpleEvent_t1509017202 * value)
	{
		___onArmyReturnInitiated_4 = value;
		Il2CppCodeGenWriteBarrier(&___onArmyReturnInitiated_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
