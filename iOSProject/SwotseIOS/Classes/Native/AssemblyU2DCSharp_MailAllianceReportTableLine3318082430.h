﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// MailAllianceReportTableController
struct MailAllianceReportTableController_t518573504;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell1276614623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MailAllianceReportTableLine
struct  MailAllianceReportTableLine_t3318082430  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Text MailAllianceReportTableLine::userName
	Text_t356221433 * ___userName_2;
	// UnityEngine.UI.Text MailAllianceReportTableLine::type
	Text_t356221433 * ___type_3;
	// UnityEngine.UI.Text MailAllianceReportTableLine::source
	Text_t356221433 * ___source_4;
	// UnityEngine.UI.Text MailAllianceReportTableLine::target
	Text_t356221433 * ___target_5;
	// UnityEngine.UI.Text MailAllianceReportTableLine::time
	Text_t356221433 * ___time_6;
	// System.Int64 MailAllianceReportTableLine::reportId
	int64_t ___reportId_7;
	// MailAllianceReportTableController MailAllianceReportTableLine::owner
	MailAllianceReportTableController_t518573504 * ___owner_8;

public:
	inline static int32_t get_offset_of_userName_2() { return static_cast<int32_t>(offsetof(MailAllianceReportTableLine_t3318082430, ___userName_2)); }
	inline Text_t356221433 * get_userName_2() const { return ___userName_2; }
	inline Text_t356221433 ** get_address_of_userName_2() { return &___userName_2; }
	inline void set_userName_2(Text_t356221433 * value)
	{
		___userName_2 = value;
		Il2CppCodeGenWriteBarrier(&___userName_2, value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(MailAllianceReportTableLine_t3318082430, ___type_3)); }
	inline Text_t356221433 * get_type_3() const { return ___type_3; }
	inline Text_t356221433 ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Text_t356221433 * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier(&___type_3, value);
	}

	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(MailAllianceReportTableLine_t3318082430, ___source_4)); }
	inline Text_t356221433 * get_source_4() const { return ___source_4; }
	inline Text_t356221433 ** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(Text_t356221433 * value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier(&___source_4, value);
	}

	inline static int32_t get_offset_of_target_5() { return static_cast<int32_t>(offsetof(MailAllianceReportTableLine_t3318082430, ___target_5)); }
	inline Text_t356221433 * get_target_5() const { return ___target_5; }
	inline Text_t356221433 ** get_address_of_target_5() { return &___target_5; }
	inline void set_target_5(Text_t356221433 * value)
	{
		___target_5 = value;
		Il2CppCodeGenWriteBarrier(&___target_5, value);
	}

	inline static int32_t get_offset_of_time_6() { return static_cast<int32_t>(offsetof(MailAllianceReportTableLine_t3318082430, ___time_6)); }
	inline Text_t356221433 * get_time_6() const { return ___time_6; }
	inline Text_t356221433 ** get_address_of_time_6() { return &___time_6; }
	inline void set_time_6(Text_t356221433 * value)
	{
		___time_6 = value;
		Il2CppCodeGenWriteBarrier(&___time_6, value);
	}

	inline static int32_t get_offset_of_reportId_7() { return static_cast<int32_t>(offsetof(MailAllianceReportTableLine_t3318082430, ___reportId_7)); }
	inline int64_t get_reportId_7() const { return ___reportId_7; }
	inline int64_t* get_address_of_reportId_7() { return &___reportId_7; }
	inline void set_reportId_7(int64_t value)
	{
		___reportId_7 = value;
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(MailAllianceReportTableLine_t3318082430, ___owner_8)); }
	inline MailAllianceReportTableController_t518573504 * get_owner_8() const { return ___owner_8; }
	inline MailAllianceReportTableController_t518573504 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(MailAllianceReportTableController_t518573504 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier(&___owner_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
