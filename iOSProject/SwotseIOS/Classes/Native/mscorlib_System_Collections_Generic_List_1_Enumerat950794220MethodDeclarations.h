﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<TUIOsharp.Entities.TuioBlob>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1211636821(__this, ___l0, method) ((  void (*) (Enumerator_t950794220 *, List_1_t1416064546 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TUIOsharp.Entities.TuioBlob>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3603198269(__this, method) ((  void (*) (Enumerator_t950794220 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<TUIOsharp.Entities.TuioBlob>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m157585005(__this, method) ((  Il2CppObject * (*) (Enumerator_t950794220 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TUIOsharp.Entities.TuioBlob>::Dispose()
#define Enumerator_Dispose_m1870742088(__this, method) ((  void (*) (Enumerator_t950794220 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<TUIOsharp.Entities.TuioBlob>::VerifyState()
#define Enumerator_VerifyState_m3924507299(__this, method) ((  void (*) (Enumerator_t950794220 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<TUIOsharp.Entities.TuioBlob>::MoveNext()
#define Enumerator_MoveNext_m694422565(__this, method) ((  bool (*) (Enumerator_t950794220 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<TUIOsharp.Entities.TuioBlob>::get_Current()
#define Enumerator_get_Current_m69166866(__this, method) ((  TuioBlob_t2046943414 * (*) (Enumerator_t950794220 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
