﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// OSCsharp.Data.OscBundle
struct OscBundle_t1226152694;
// OSCsharp.Data.OscTimeTag
struct OscTimeTag_t749151765;
// OSCsharp.Data.OscPacket
struct OscPacket_t3204151022;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.ArgumentException
struct ArgumentException_t132251570;
// System.Exception
struct Exception_t;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// OSCsharp.Data.OscMessage
struct OscMessage_t3323977005;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3384741;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t600458651;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t4292682451;
// OSCsharp.Net.OscBundleReceivedEventArgs
struct OscBundleReceivedEventArgs_t574491439;
// System.EventArgs
struct EventArgs_t3591816995;
// OSCsharp.Net.OscMessageReceivedEventArgs
struct OscMessageReceivedEventArgs_t2390219542;
// OSCsharp.Net.OscPacketReceivedEventArgs
struct OscPacketReceivedEventArgs_t663085507;
// OSCsharp.Net.UDPReceiver
struct UDPReceiver_t1881969775;
// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>
struct EventHandler_1_t319478644;
// System.Delegate
struct Delegate_t1188392813;
// System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>
struct EventHandler_1_t314378975;
// System.Net.IPAddress
struct IPAddress_t241777590;
// System.Net.IPEndPoint
struct IPEndPoint_t3791887218;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Net.Sockets.UdpClient
struct UdpClient_t967282006;
// System.Net.Sockets.Socket
struct Socket_t1119025450;
// System.Net.EndPoint
struct EndPoint_t982345378;
// OSCsharp.Net.UDPReceiver/UdpState
struct UdpState_t629681666;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.EventHandler`1<OSCsharp.Net.OscPacketReceivedEventArgs>
struct EventHandler_1_t2882212236;
// System.EventHandler`1<System.Object>
struct EventHandler_1_t1004265597;
// System.EventHandler`1<OSCsharp.Net.OscBundleReceivedEventArgs>
struct EventHandler_1_t2793618168;
// OSCsharp.Utils.ExceptionEventArgs
struct ExceptionEventArgs_t2395319211;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t3530625384;
// System.Void
struct Void_t1185182177;
// System.Byte
struct Byte_t1134296376;
// System.Double
struct Double_t594665363;
// System.UInt16
struct UInt16_t2177724958;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// System.Collections.Queue
struct Queue_t3637523393;
// System.Threading.Thread
struct Thread_t2300836069;
// System.UInt16[]
struct UInt16U5BU5D_t3326319531;

extern RuntimeClass* OscPacket_t3204151022_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3032477599;
extern const uint32_t OscBundle__ctor_m645648858_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentException_t132251570_il2cpp_TypeInfo_var;
extern RuntimeClass* OscBundle_t1226152694_il2cpp_TypeInfo_var;
extern const RuntimeMethod* OscPacket_ValueFromByteArray_TisString_t_m2385610542_RuntimeMethod_var;
extern const RuntimeMethod* OscPacket_ValueFromByteArray_TisOscTimeTag_t749151765_m1850822955_RuntimeMethod_var;
extern const RuntimeMethod* OscPacket_ValueFromByteArray_TisInt32_t2950945753_m1459680167_RuntimeMethod_var;
extern const uint32_t OscBundle_FromByteArray_m76398600_MetadataUsageId;
extern RuntimeClass* OscTimeTag_t749151765_il2cpp_TypeInfo_var;
extern RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_Add_m3338814081_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m2934127733_RuntimeMethod_var;
extern String_t* _stringLiteral2056298437;
extern const uint32_t OscBundle_Append_m1737276556_MetadataUsageId;
extern String_t* _stringLiteral3452614529;
extern String_t* _stringLiteral758506801;
extern const uint32_t OscMessage__ctor_m1172728952_MetadataUsageId;
extern RuntimeClass* OscMessage_t3323977005_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* Int64_t3736567304_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern RuntimeClass* Double_t594665363_il2cpp_TypeInfo_var;
extern RuntimeClass* Char_t3634460470_il2cpp_TypeInfo_var;
extern RuntimeClass* Boolean_t97287965_il2cpp_TypeInfo_var;
extern const RuntimeMethod* OscPacket_ValueFromByteArray_TisInt64_t3736567304_m4196370337_RuntimeMethod_var;
extern const RuntimeMethod* OscPacket_ValueFromByteArray_TisSingle_t1397266774_m1683906482_RuntimeMethod_var;
extern const RuntimeMethod* OscPacket_ValueFromByteArray_TisDouble_t594665363_m997755250_RuntimeMethod_var;
extern const RuntimeMethod* OscPacket_ValueFromByteArray_TisByteU5BU5D_t4116647657_m2669022459_RuntimeMethod_var;
extern const RuntimeMethod* OscPacket_ValueFromByteArray_TisChar_t3634460470_m3452883113_RuntimeMethod_var;
extern const uint32_t OscMessage_FromByteArray_m1008684133_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t2736202052_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m2392909825_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m282647386_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m1013208020_RuntimeMethod_var;
extern String_t* _stringLiteral4176330965;
extern String_t* _stringLiteral1455341775;
extern String_t* _stringLiteral1234456403;
extern String_t* _stringLiteral1235498031;
extern String_t* _stringLiteral1236129805;
extern String_t* _stringLiteral133939039;
extern String_t* _stringLiteral2800413247;
extern String_t* _stringLiteral3873632002;
extern String_t* _stringLiteral1586259007;
extern String_t* _stringLiteral228530734;
extern String_t* _stringLiteral2615941665;
extern const uint32_t OscMessage_Append_m2539754167_MetadataUsageId;
extern const uint32_t OscPacket_get_LittleEndianByteOrder_m412490820_MetadataUsageId;
extern const uint32_t OscPacket_set_Address_m2086307222_MetadataUsageId;
extern const RuntimeMethod* List_1_AsReadOnly_m1069347492_RuntimeMethod_var;
extern const uint32_t OscPacket_get_Data_m3397942478_MetadataUsageId;
extern const uint32_t OscPacket__cctor_m4081103440_MetadataUsageId;
extern RuntimeClass* List_1_t257213610_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2321703786_RuntimeMethod_var;
extern const uint32_t OscPacket__ctor_m1212150461_MetadataUsageId;
extern const uint32_t OscPacket_FromByteArray_m2774387762_MetadataUsageId;
extern RuntimeClass* BitConverter_t3118986983_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Utility_CopySubArray_TisByte_t1134296376_m796220017_RuntimeMethod_var;
extern const uint32_t OscTimeTag__ctor_m313460281_MetadataUsageId;
extern RuntimeClass* DateTime_t3738529785_il2cpp_TypeInfo_var;
extern const uint32_t OscTimeTag_op_Equality_m793844905_MetadataUsageId;
extern const uint32_t OscTimeTag_op_LessThan_m2293409653_MetadataUsageId;
extern RuntimeClass* TimeSpan_t881159249_il2cpp_TypeInfo_var;
extern const uint32_t OscTimeTag_IsValidTime_m2642829427_MetadataUsageId;
extern String_t* _stringLiteral3480154658;
extern const uint32_t OscTimeTag_Set_m2513023378_MetadataUsageId;
extern const uint32_t OscTimeTag_Equals_m2737513310_MetadataUsageId;
extern const uint32_t OscTimeTag__cctor_m317014173_MetadataUsageId;
extern RuntimeClass* EventArgs_t3591816995_il2cpp_TypeInfo_var;
extern const uint32_t OscBundleReceivedEventArgs__ctor_m3816053266_MetadataUsageId;
extern const uint32_t OscMessageReceivedEventArgs__ctor_m3694517805_MetadataUsageId;
extern const uint32_t OscPacketReceivedEventArgs__ctor_m3956995085_MetadataUsageId;
extern RuntimeClass* EventHandler_1_t319478644_il2cpp_TypeInfo_var;
extern const uint32_t UDPReceiver_add_ErrorOccured_m2838983283_MetadataUsageId;
extern const uint32_t UDPReceiver_remove_ErrorOccured_m4285225404_MetadataUsageId;
extern RuntimeClass* EventHandler_1_t314378975_il2cpp_TypeInfo_var;
extern const uint32_t UDPReceiver_add_MessageReceived_m2005693360_MetadataUsageId;
extern const uint32_t UDPReceiver_remove_MessageReceived_m3207071580_MetadataUsageId;
extern RuntimeClass* IPAddress_t241777590_il2cpp_TypeInfo_var;
extern const uint32_t UDPReceiver__ctor_m588051875_MetadataUsageId;
extern RuntimeClass* AsyncCallback_t3962456242_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UDPReceiver_endReceive_m703031579_RuntimeMethod_var;
extern String_t* _stringLiteral2077443138;
extern const uint32_t UDPReceiver__ctor_m2102639691_MetadataUsageId;
extern RuntimeClass* IPEndPoint_t3791887218_il2cpp_TypeInfo_var;
extern RuntimeClass* UdpClient_t967282006_il2cpp_TypeInfo_var;
extern RuntimeClass* Socket_t1119025450_il2cpp_TypeInfo_var;
extern RuntimeClass* UdpState_t629681666_il2cpp_TypeInfo_var;
extern const uint32_t UDPReceiver_Start_m2568122436_MetadataUsageId;
extern RuntimeClass* IAsyncResult_t767004451_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectDisposedException_t21392786_il2cpp_TypeInfo_var;
extern const uint32_t UDPReceiver_endReceive_m703031579_MetadataUsageId;
extern const uint32_t UDPReceiver_parseData_m1225544979_MetadataUsageId;
extern RuntimeClass* OscPacketReceivedEventArgs_t663085507_il2cpp_TypeInfo_var;
extern const RuntimeMethod* EventHandler_1_Invoke_m866284127_RuntimeMethod_var;
extern const uint32_t UDPReceiver_onPacketReceived_m1435591783_MetadataUsageId;
extern RuntimeClass* OscBundleReceivedEventArgs_t574491439_il2cpp_TypeInfo_var;
extern RuntimeClass* ICollection_1_t1613291102_il2cpp_TypeInfo_var;
extern RuntimeClass* IList_1_t600458651_il2cpp_TypeInfo_var;
extern const RuntimeMethod* EventHandler_1_Invoke_m2727238407_RuntimeMethod_var;
extern const uint32_t UDPReceiver_onBundleReceived_m1364519174_MetadataUsageId;
extern RuntimeClass* OscMessageReceivedEventArgs_t2390219542_il2cpp_TypeInfo_var;
extern const RuntimeMethod* EventHandler_1_Invoke_m2312074494_RuntimeMethod_var;
extern const uint32_t UDPReceiver_onMessageReceived_m1127692520_MetadataUsageId;
extern RuntimeClass* ExceptionEventArgs_t2395319211_il2cpp_TypeInfo_var;
extern const RuntimeMethod* EventHandler_1_Invoke_m3384041340_RuntimeMethod_var;
extern const uint32_t UDPReceiver_onError_m4129978076_MetadataUsageId;
extern const uint32_t ExceptionEventArgs__ctor_m1472460080_MetadataUsageId;
extern RuntimeClass* ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var;
extern const uint32_t Utility_SwapEndian_m1429200246_MetadataUsageId;

struct ByteU5BU5D_t4116647657;
struct CharU5BU5D_t3528271667;


#ifndef U3CMODULEU3E_T692745549_H
#define U3CMODULEU3E_T692745549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745549 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745549_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef LIST_1_T257213610_H
#define LIST_1_T257213610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Object>
struct  List_1_t257213610  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t2843939325* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____items_1)); }
	inline ObjectU5BU5D_t2843939325* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t2843939325** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t2843939325* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t257213610_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ObjectU5BU5D_t2843939325* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t257213610_StaticFields, ___EmptyArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T257213610_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef ENDPOINT_T982345378_H
#define ENDPOINT_T982345378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.EndPoint
struct  EndPoint_t982345378  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINT_T982345378_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef READONLYCOLLECTION_1_T4292682451_H
#define READONLYCOLLECTION_1_T4292682451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct  ReadOnlyCollection_1_t4292682451  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1::list
	RuntimeObject* ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(ReadOnlyCollection_1_t4292682451, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYCOLLECTION_1_T4292682451_H
#ifndef BITCONVERTER_T3118986983_H
#define BITCONVERTER_T3118986983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.BitConverter
struct  BitConverter_t3118986983  : public RuntimeObject
{
public:

public:
};

struct BitConverter_t3118986983_StaticFields
{
public:
	// System.Boolean System.BitConverter::SwappedWordsInDouble
	bool ___SwappedWordsInDouble_0;
	// System.Boolean System.BitConverter::IsLittleEndian
	bool ___IsLittleEndian_1;

public:
	inline static int32_t get_offset_of_SwappedWordsInDouble_0() { return static_cast<int32_t>(offsetof(BitConverter_t3118986983_StaticFields, ___SwappedWordsInDouble_0)); }
	inline bool get_SwappedWordsInDouble_0() const { return ___SwappedWordsInDouble_0; }
	inline bool* get_address_of_SwappedWordsInDouble_0() { return &___SwappedWordsInDouble_0; }
	inline void set_SwappedWordsInDouble_0(bool value)
	{
		___SwappedWordsInDouble_0 = value;
	}

	inline static int32_t get_offset_of_IsLittleEndian_1() { return static_cast<int32_t>(offsetof(BitConverter_t3118986983_StaticFields, ___IsLittleEndian_1)); }
	inline bool get_IsLittleEndian_1() const { return ___IsLittleEndian_1; }
	inline bool* get_address_of_IsLittleEndian_1() { return &___IsLittleEndian_1; }
	inline void set_IsLittleEndian_1(bool value)
	{
		___IsLittleEndian_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITCONVERTER_T3118986983_H
#ifndef DICTIONARY_2_T2736202052_H
#define DICTIONARY_2_T2736202052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct  Dictionary_2_t2736202052  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	Int32U5BU5D_t385246372* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___valueSlots_7)); }
	inline Int32U5BU5D_t385246372* get_valueSlots_7() const { return ___valueSlots_7; }
	inline Int32U5BU5D_t385246372** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(Int32U5BU5D_t385246372* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2736202052_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3530625384 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2736202052_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3530625384 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3530625384 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3530625384 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2736202052_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef UTILITY_T2342964370_H
#define UTILITY_T2342964370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Utils.Utility
struct  Utility_t2342964370  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILITY_T2342964370_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef OSCPACKET_T3204151022_H
#define OSCPACKET_T3204151022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscPacket
struct  OscPacket_t3204151022  : public RuntimeObject
{
public:
	// System.String OSCsharp.Data.OscPacket::address
	String_t* ___address_1;
	// System.Collections.Generic.List`1<System.Object> OSCsharp.Data.OscPacket::data
	List_1_t257213610 * ___data_2;

public:
	inline static int32_t get_offset_of_address_1() { return static_cast<int32_t>(offsetof(OscPacket_t3204151022, ___address_1)); }
	inline String_t* get_address_1() const { return ___address_1; }
	inline String_t** get_address_of_address_1() { return &___address_1; }
	inline void set_address_1(String_t* value)
	{
		___address_1 = value;
		Il2CppCodeGenWriteBarrier((&___address_1), value);
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(OscPacket_t3204151022, ___data_2)); }
	inline List_1_t257213610 * get_data_2() const { return ___data_2; }
	inline List_1_t257213610 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(List_1_t257213610 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

struct OscPacket_t3204151022_StaticFields
{
public:
	// System.Boolean OSCsharp.Data.OscPacket::littleEndianByteOrder
	bool ___littleEndianByteOrder_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> OSCsharp.Data.OscPacket::<>f__switch$map1
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1_3;

public:
	inline static int32_t get_offset_of_littleEndianByteOrder_0() { return static_cast<int32_t>(offsetof(OscPacket_t3204151022_StaticFields, ___littleEndianByteOrder_0)); }
	inline bool get_littleEndianByteOrder_0() const { return ___littleEndianByteOrder_0; }
	inline bool* get_address_of_littleEndianByteOrder_0() { return &___littleEndianByteOrder_0; }
	inline void set_littleEndianByteOrder_0(bool value)
	{
		___littleEndianByteOrder_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_3() { return static_cast<int32_t>(offsetof(OscPacket_t3204151022_StaticFields, ___U3CU3Ef__switchU24map1_3)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1_3() const { return ___U3CU3Ef__switchU24map1_3; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1_3() { return &___U3CU3Ef__switchU24map1_3; }
	inline void set_U3CU3Ef__switchU24map1_3(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCPACKET_T3204151022_H
#ifndef UDPSTATE_T629681666_H
#define UDPSTATE_T629681666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.UDPReceiver/UdpState
struct  UdpState_t629681666  : public RuntimeObject
{
public:
	// System.Net.Sockets.UdpClient OSCsharp.Net.UDPReceiver/UdpState::<Client>k__BackingField
	UdpClient_t967282006 * ___U3CClientU3Ek__BackingField_0;
	// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver/UdpState::<IPEndPoint>k__BackingField
	IPEndPoint_t3791887218 * ___U3CIPEndPointU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CClientU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UdpState_t629681666, ___U3CClientU3Ek__BackingField_0)); }
	inline UdpClient_t967282006 * get_U3CClientU3Ek__BackingField_0() const { return ___U3CClientU3Ek__BackingField_0; }
	inline UdpClient_t967282006 ** get_address_of_U3CClientU3Ek__BackingField_0() { return &___U3CClientU3Ek__BackingField_0; }
	inline void set_U3CClientU3Ek__BackingField_0(UdpClient_t967282006 * value)
	{
		___U3CClientU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIPEndPointU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UdpState_t629681666, ___U3CIPEndPointU3Ek__BackingField_1)); }
	inline IPEndPoint_t3791887218 * get_U3CIPEndPointU3Ek__BackingField_1() const { return ___U3CIPEndPointU3Ek__BackingField_1; }
	inline IPEndPoint_t3791887218 ** get_address_of_U3CIPEndPointU3Ek__BackingField_1() { return &___U3CIPEndPointU3Ek__BackingField_1; }
	inline void set_U3CIPEndPointU3Ek__BackingField_1(IPEndPoint_t3791887218 * value)
	{
		___U3CIPEndPointU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIPEndPointU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPSTATE_T629681666_H
#ifndef OSCMESSAGERECEIVEDEVENTARGS_T2390219542_H
#define OSCMESSAGERECEIVEDEVENTARGS_T2390219542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.OscMessageReceivedEventArgs
struct  OscMessageReceivedEventArgs_t2390219542  : public EventArgs_t3591816995
{
public:
	// OSCsharp.Data.OscMessage OSCsharp.Net.OscMessageReceivedEventArgs::<Message>k__BackingField
	OscMessage_t3323977005 * ___U3CMessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OscMessageReceivedEventArgs_t2390219542, ___U3CMessageU3Ek__BackingField_1)); }
	inline OscMessage_t3323977005 * get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline OscMessage_t3323977005 ** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(OscMessage_t3323977005 * value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCMESSAGERECEIVEDEVENTARGS_T2390219542_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef IPENDPOINT_T3791887218_H
#define IPENDPOINT_T3791887218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPEndPoint
struct  IPEndPoint_t3791887218  : public EndPoint_t982345378
{
public:
	// System.Net.IPAddress System.Net.IPEndPoint::address
	IPAddress_t241777590 * ___address_2;
	// System.Int32 System.Net.IPEndPoint::port
	int32_t ___port_3;

public:
	inline static int32_t get_offset_of_address_2() { return static_cast<int32_t>(offsetof(IPEndPoint_t3791887218, ___address_2)); }
	inline IPAddress_t241777590 * get_address_2() const { return ___address_2; }
	inline IPAddress_t241777590 ** get_address_of_address_2() { return &___address_2; }
	inline void set_address_2(IPAddress_t241777590 * value)
	{
		___address_2 = value;
		Il2CppCodeGenWriteBarrier((&___address_2), value);
	}

	inline static int32_t get_offset_of_port_3() { return static_cast<int32_t>(offsetof(IPEndPoint_t3791887218, ___port_3)); }
	inline int32_t get_port_3() const { return ___port_3; }
	inline int32_t* get_address_of_port_3() { return &___port_3; }
	inline void set_port_3(int32_t value)
	{
		___port_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPENDPOINT_T3791887218_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef OSCPACKETRECEIVEDEVENTARGS_T663085507_H
#define OSCPACKETRECEIVEDEVENTARGS_T663085507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.OscPacketReceivedEventArgs
struct  OscPacketReceivedEventArgs_t663085507  : public EventArgs_t3591816995
{
public:
	// OSCsharp.Data.OscPacket OSCsharp.Net.OscPacketReceivedEventArgs::<Packet>k__BackingField
	OscPacket_t3204151022 * ___U3CPacketU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CPacketU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OscPacketReceivedEventArgs_t663085507, ___U3CPacketU3Ek__BackingField_1)); }
	inline OscPacket_t3204151022 * get_U3CPacketU3Ek__BackingField_1() const { return ___U3CPacketU3Ek__BackingField_1; }
	inline OscPacket_t3204151022 ** get_address_of_U3CPacketU3Ek__BackingField_1() { return &___U3CPacketU3Ek__BackingField_1; }
	inline void set_U3CPacketU3Ek__BackingField_1(OscPacket_t3204151022 * value)
	{
		___U3CPacketU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPacketU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCPACKETRECEIVEDEVENTARGS_T663085507_H
#ifndef OSCBUNDLERECEIVEDEVENTARGS_T574491439_H
#define OSCBUNDLERECEIVEDEVENTARGS_T574491439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.OscBundleReceivedEventArgs
struct  OscBundleReceivedEventArgs_t574491439  : public EventArgs_t3591816995
{
public:
	// OSCsharp.Data.OscBundle OSCsharp.Net.OscBundleReceivedEventArgs::<Bundle>k__BackingField
	OscBundle_t1226152694 * ___U3CBundleU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CBundleU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OscBundleReceivedEventArgs_t574491439, ___U3CBundleU3Ek__BackingField_1)); }
	inline OscBundle_t1226152694 * get_U3CBundleU3Ek__BackingField_1() const { return ___U3CBundleU3Ek__BackingField_1; }
	inline OscBundle_t1226152694 ** get_address_of_U3CBundleU3Ek__BackingField_1() { return &___U3CBundleU3Ek__BackingField_1; }
	inline void set_U3CBundleU3Ek__BackingField_1(OscBundle_t1226152694 * value)
	{
		___U3CBundleU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBundleU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCBUNDLERECEIVEDEVENTARGS_T574491439_H
#ifndef EXCEPTIONEVENTARGS_T2395319211_H
#define EXCEPTIONEVENTARGS_T2395319211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Utils.ExceptionEventArgs
struct  ExceptionEventArgs_t2395319211  : public EventArgs_t3591816995
{
public:
	// System.Exception OSCsharp.Utils.ExceptionEventArgs::<Exception>k__BackingField
	Exception_t * ___U3CExceptionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ExceptionEventArgs_t2395319211, ___U3CExceptionU3Ek__BackingField_1)); }
	inline Exception_t * get_U3CExceptionU3Ek__BackingField_1() const { return ___U3CExceptionU3Ek__BackingField_1; }
	inline Exception_t ** get_address_of_U3CExceptionU3Ek__BackingField_1() { return &___U3CExceptionU3Ek__BackingField_1; }
	inline void set_U3CExceptionU3Ek__BackingField_1(Exception_t * value)
	{
		___U3CExceptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONEVENTARGS_T2395319211_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef OSCMESSAGE_T3323977005_H
#define OSCMESSAGE_T3323977005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscMessage
struct  OscMessage_t3323977005  : public OscPacket_t3204151022
{
public:
	// System.String OSCsharp.Data.OscMessage::typeTag
	String_t* ___typeTag_20;

public:
	inline static int32_t get_offset_of_typeTag_20() { return static_cast<int32_t>(offsetof(OscMessage_t3323977005, ___typeTag_20)); }
	inline String_t* get_typeTag_20() const { return ___typeTag_20; }
	inline String_t** get_address_of_typeTag_20() { return &___typeTag_20; }
	inline void set_typeTag_20(String_t* value)
	{
		___typeTag_20 = value;
		Il2CppCodeGenWriteBarrier((&___typeTag_20), value);
	}
};

struct OscMessage_t3323977005_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> OSCsharp.Data.OscMessage::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_21;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_21() { return static_cast<int32_t>(offsetof(OscMessage_t3323977005_StaticFields, ___U3CU3Ef__switchU24map0_21)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_21() const { return ___U3CU3Ef__switchU24map0_21; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_21() { return &___U3CU3Ef__switchU24map0_21; }
	inline void set_U3CU3Ef__switchU24map0_21(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCMESSAGE_T3323977005_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef OSCBUNDLE_T1226152694_H
#define OSCBUNDLE_T1226152694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscBundle
struct  OscBundle_t1226152694  : public OscPacket_t3204151022
{
public:
	// OSCsharp.Data.OscTimeTag OSCsharp.Data.OscBundle::timeStamp
	OscTimeTag_t749151765 * ___timeStamp_5;

public:
	inline static int32_t get_offset_of_timeStamp_5() { return static_cast<int32_t>(offsetof(OscBundle_t1226152694, ___timeStamp_5)); }
	inline OscTimeTag_t749151765 * get_timeStamp_5() const { return ___timeStamp_5; }
	inline OscTimeTag_t749151765 ** get_address_of_timeStamp_5() { return &___timeStamp_5; }
	inline void set_timeStamp_5(OscTimeTag_t749151765 * value)
	{
		___timeStamp_5 = value;
		Il2CppCodeGenWriteBarrier((&___timeStamp_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCBUNDLE_T1226152694_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef PROTOCOLTYPE_T303635025_H
#define PROTOCOLTYPE_T303635025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.ProtocolType
struct  ProtocolType_t303635025 
{
public:
	// System.Int32 System.Net.Sockets.ProtocolType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProtocolType_t303635025, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLTYPE_T303635025_H
#ifndef SOCKETOPTIONNAME_T403346465_H
#define SOCKETOPTIONNAME_T403346465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketOptionName
struct  SocketOptionName_t403346465 
{
public:
	// System.Int32 System.Net.Sockets.SocketOptionName::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SocketOptionName_t403346465, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETOPTIONNAME_T403346465_H
#ifndef SOCKETOPTIONLEVEL_T201167901_H
#define SOCKETOPTIONLEVEL_T201167901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketOptionLevel
struct  SocketOptionLevel_t201167901 
{
public:
	// System.Int32 System.Net.Sockets.SocketOptionLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SocketOptionLevel_t201167901, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETOPTIONLEVEL_T201167901_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef INVALIDOPERATIONEXCEPTION_T56020091_H
#define INVALIDOPERATIONEXCEPTION_T56020091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t56020091  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T56020091_H
#ifndef ARGUMENTEXCEPTION_T132251570_H
#define ARGUMENTEXCEPTION_T132251570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t132251570  : public SystemException_t176217640
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t132251570, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T132251570_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef TRANSMISSIONTYPE_T3895608725_H
#define TRANSMISSIONTYPE_T3895608725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.TransmissionType
struct  TransmissionType_t3895608725 
{
public:
	// System.Int32 OSCsharp.Net.TransmissionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransmissionType_t3895608725, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSMISSIONTYPE_T3895608725_H
#ifndef SOCKETTYPE_T2175930299_H
#define SOCKETTYPE_T2175930299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketType
struct  SocketType_t2175930299 
{
public:
	// System.Int32 System.Net.Sockets.SocketType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SocketType_t2175930299, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETTYPE_T2175930299_H
#ifndef ADDRESSFAMILY_T2612549059_H
#define ADDRESSFAMILY_T2612549059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.AddressFamily
struct  AddressFamily_t2612549059 
{
public:
	// System.Int32 System.Net.Sockets.AddressFamily::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AddressFamily_t2612549059, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDRESSFAMILY_T2612549059_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef UDPRECEIVER_T1881969775_H
#define UDPRECEIVER_T1881969775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.UDPReceiver
struct  UDPReceiver_t1881969775  : public RuntimeObject
{
public:
	// System.EventHandler`1<OSCsharp.Net.OscPacketReceivedEventArgs> OSCsharp.Net.UDPReceiver::PacketReceived
	EventHandler_1_t2882212236 * ___PacketReceived_0;
	// System.EventHandler`1<OSCsharp.Net.OscBundleReceivedEventArgs> OSCsharp.Net.UDPReceiver::BundleReceived
	EventHandler_1_t2793618168 * ___BundleReceived_1;
	// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs> OSCsharp.Net.UDPReceiver::ErrorOccured
	EventHandler_1_t319478644 * ___ErrorOccured_2;
	// System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs> OSCsharp.Net.UDPReceiver::messageReceivedInvoker
	EventHandler_1_t314378975 * ___messageReceivedInvoker_3;
	// System.Net.IPAddress OSCsharp.Net.UDPReceiver::<IPAddress>k__BackingField
	IPAddress_t241777590 * ___U3CIPAddressU3Ek__BackingField_4;
	// System.Int32 OSCsharp.Net.UDPReceiver::<Port>k__BackingField
	int32_t ___U3CPortU3Ek__BackingField_5;
	// System.Net.IPAddress OSCsharp.Net.UDPReceiver::<MulticastAddress>k__BackingField
	IPAddress_t241777590 * ___U3CMulticastAddressU3Ek__BackingField_6;
	// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver::<IPEndPoint>k__BackingField
	IPEndPoint_t3791887218 * ___U3CIPEndPointU3Ek__BackingField_7;
	// OSCsharp.Net.TransmissionType OSCsharp.Net.UDPReceiver::<TransmissionType>k__BackingField
	int32_t ___U3CTransmissionTypeU3Ek__BackingField_8;
	// System.Boolean OSCsharp.Net.UDPReceiver::<ConsumeParsingExceptions>k__BackingField
	bool ___U3CConsumeParsingExceptionsU3Ek__BackingField_9;
	// System.Net.Sockets.UdpClient OSCsharp.Net.UDPReceiver::udpClient
	UdpClient_t967282006 * ___udpClient_10;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) OSCsharp.Net.UDPReceiver::acceptingConnections
	bool ___acceptingConnections_11;
	// System.AsyncCallback OSCsharp.Net.UDPReceiver::callback
	AsyncCallback_t3962456242 * ___callback_12;

public:
	inline static int32_t get_offset_of_PacketReceived_0() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___PacketReceived_0)); }
	inline EventHandler_1_t2882212236 * get_PacketReceived_0() const { return ___PacketReceived_0; }
	inline EventHandler_1_t2882212236 ** get_address_of_PacketReceived_0() { return &___PacketReceived_0; }
	inline void set_PacketReceived_0(EventHandler_1_t2882212236 * value)
	{
		___PacketReceived_0 = value;
		Il2CppCodeGenWriteBarrier((&___PacketReceived_0), value);
	}

	inline static int32_t get_offset_of_BundleReceived_1() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___BundleReceived_1)); }
	inline EventHandler_1_t2793618168 * get_BundleReceived_1() const { return ___BundleReceived_1; }
	inline EventHandler_1_t2793618168 ** get_address_of_BundleReceived_1() { return &___BundleReceived_1; }
	inline void set_BundleReceived_1(EventHandler_1_t2793618168 * value)
	{
		___BundleReceived_1 = value;
		Il2CppCodeGenWriteBarrier((&___BundleReceived_1), value);
	}

	inline static int32_t get_offset_of_ErrorOccured_2() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___ErrorOccured_2)); }
	inline EventHandler_1_t319478644 * get_ErrorOccured_2() const { return ___ErrorOccured_2; }
	inline EventHandler_1_t319478644 ** get_address_of_ErrorOccured_2() { return &___ErrorOccured_2; }
	inline void set_ErrorOccured_2(EventHandler_1_t319478644 * value)
	{
		___ErrorOccured_2 = value;
		Il2CppCodeGenWriteBarrier((&___ErrorOccured_2), value);
	}

	inline static int32_t get_offset_of_messageReceivedInvoker_3() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___messageReceivedInvoker_3)); }
	inline EventHandler_1_t314378975 * get_messageReceivedInvoker_3() const { return ___messageReceivedInvoker_3; }
	inline EventHandler_1_t314378975 ** get_address_of_messageReceivedInvoker_3() { return &___messageReceivedInvoker_3; }
	inline void set_messageReceivedInvoker_3(EventHandler_1_t314378975 * value)
	{
		___messageReceivedInvoker_3 = value;
		Il2CppCodeGenWriteBarrier((&___messageReceivedInvoker_3), value);
	}

	inline static int32_t get_offset_of_U3CIPAddressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___U3CIPAddressU3Ek__BackingField_4)); }
	inline IPAddress_t241777590 * get_U3CIPAddressU3Ek__BackingField_4() const { return ___U3CIPAddressU3Ek__BackingField_4; }
	inline IPAddress_t241777590 ** get_address_of_U3CIPAddressU3Ek__BackingField_4() { return &___U3CIPAddressU3Ek__BackingField_4; }
	inline void set_U3CIPAddressU3Ek__BackingField_4(IPAddress_t241777590 * value)
	{
		___U3CIPAddressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIPAddressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CPortU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___U3CPortU3Ek__BackingField_5)); }
	inline int32_t get_U3CPortU3Ek__BackingField_5() const { return ___U3CPortU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CPortU3Ek__BackingField_5() { return &___U3CPortU3Ek__BackingField_5; }
	inline void set_U3CPortU3Ek__BackingField_5(int32_t value)
	{
		___U3CPortU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CMulticastAddressU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___U3CMulticastAddressU3Ek__BackingField_6)); }
	inline IPAddress_t241777590 * get_U3CMulticastAddressU3Ek__BackingField_6() const { return ___U3CMulticastAddressU3Ek__BackingField_6; }
	inline IPAddress_t241777590 ** get_address_of_U3CMulticastAddressU3Ek__BackingField_6() { return &___U3CMulticastAddressU3Ek__BackingField_6; }
	inline void set_U3CMulticastAddressU3Ek__BackingField_6(IPAddress_t241777590 * value)
	{
		___U3CMulticastAddressU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMulticastAddressU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CIPEndPointU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___U3CIPEndPointU3Ek__BackingField_7)); }
	inline IPEndPoint_t3791887218 * get_U3CIPEndPointU3Ek__BackingField_7() const { return ___U3CIPEndPointU3Ek__BackingField_7; }
	inline IPEndPoint_t3791887218 ** get_address_of_U3CIPEndPointU3Ek__BackingField_7() { return &___U3CIPEndPointU3Ek__BackingField_7; }
	inline void set_U3CIPEndPointU3Ek__BackingField_7(IPEndPoint_t3791887218 * value)
	{
		___U3CIPEndPointU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIPEndPointU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CTransmissionTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___U3CTransmissionTypeU3Ek__BackingField_8)); }
	inline int32_t get_U3CTransmissionTypeU3Ek__BackingField_8() const { return ___U3CTransmissionTypeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CTransmissionTypeU3Ek__BackingField_8() { return &___U3CTransmissionTypeU3Ek__BackingField_8; }
	inline void set_U3CTransmissionTypeU3Ek__BackingField_8(int32_t value)
	{
		___U3CTransmissionTypeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CConsumeParsingExceptionsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___U3CConsumeParsingExceptionsU3Ek__BackingField_9)); }
	inline bool get_U3CConsumeParsingExceptionsU3Ek__BackingField_9() const { return ___U3CConsumeParsingExceptionsU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CConsumeParsingExceptionsU3Ek__BackingField_9() { return &___U3CConsumeParsingExceptionsU3Ek__BackingField_9; }
	inline void set_U3CConsumeParsingExceptionsU3Ek__BackingField_9(bool value)
	{
		___U3CConsumeParsingExceptionsU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_udpClient_10() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___udpClient_10)); }
	inline UdpClient_t967282006 * get_udpClient_10() const { return ___udpClient_10; }
	inline UdpClient_t967282006 ** get_address_of_udpClient_10() { return &___udpClient_10; }
	inline void set_udpClient_10(UdpClient_t967282006 * value)
	{
		___udpClient_10 = value;
		Il2CppCodeGenWriteBarrier((&___udpClient_10), value);
	}

	inline static int32_t get_offset_of_acceptingConnections_11() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___acceptingConnections_11)); }
	inline bool get_acceptingConnections_11() const { return ___acceptingConnections_11; }
	inline bool* get_address_of_acceptingConnections_11() { return &___acceptingConnections_11; }
	inline void set_acceptingConnections_11(bool value)
	{
		___acceptingConnections_11 = value;
	}

	inline static int32_t get_offset_of_callback_12() { return static_cast<int32_t>(offsetof(UDPReceiver_t1881969775, ___callback_12)); }
	inline AsyncCallback_t3962456242 * get_callback_12() const { return ___callback_12; }
	inline AsyncCallback_t3962456242 ** get_address_of_callback_12() { return &___callback_12; }
	inline void set_callback_12(AsyncCallback_t3962456242 * value)
	{
		___callback_12 = value;
		Il2CppCodeGenWriteBarrier((&___callback_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPRECEIVER_T1881969775_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t426314064 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t426314064 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef SOCKET_T1119025450_H
#define SOCKET_T1119025450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.Socket
struct  Socket_t1119025450  : public RuntimeObject
{
public:
	// System.Collections.Queue System.Net.Sockets.Socket::readQ
	Queue_t3637523393 * ___readQ_0;
	// System.Collections.Queue System.Net.Sockets.Socket::writeQ
	Queue_t3637523393 * ___writeQ_1;
	// System.Boolean System.Net.Sockets.Socket::islistening
	bool ___islistening_2;
	// System.Boolean System.Net.Sockets.Socket::useoverlappedIO
	bool ___useoverlappedIO_3;
	// System.Int32 System.Net.Sockets.Socket::MinListenPort
	int32_t ___MinListenPort_4;
	// System.Int32 System.Net.Sockets.Socket::MaxListenPort
	int32_t ___MaxListenPort_5;
	// System.Int32 System.Net.Sockets.Socket::linger_timeout
	int32_t ___linger_timeout_8;
	// System.IntPtr System.Net.Sockets.Socket::socket
	intptr_t ___socket_9;
	// System.Net.Sockets.AddressFamily System.Net.Sockets.Socket::address_family
	int32_t ___address_family_10;
	// System.Net.Sockets.SocketType System.Net.Sockets.Socket::socket_type
	int32_t ___socket_type_11;
	// System.Net.Sockets.ProtocolType System.Net.Sockets.Socket::protocol_type
	int32_t ___protocol_type_12;
	// System.Boolean System.Net.Sockets.Socket::blocking
	bool ___blocking_13;
	// System.Threading.Thread System.Net.Sockets.Socket::blocking_thread
	Thread_t2300836069 * ___blocking_thread_14;
	// System.Boolean System.Net.Sockets.Socket::isbound
	bool ___isbound_15;
	// System.Int32 System.Net.Sockets.Socket::max_bind_count
	int32_t ___max_bind_count_17;
	// System.Boolean System.Net.Sockets.Socket::connected
	bool ___connected_18;
	// System.Boolean System.Net.Sockets.Socket::closed
	bool ___closed_19;
	// System.Boolean System.Net.Sockets.Socket::disposed
	bool ___disposed_20;
	// System.Net.EndPoint System.Net.Sockets.Socket::seed_endpoint
	EndPoint_t982345378 * ___seed_endpoint_21;

public:
	inline static int32_t get_offset_of_readQ_0() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___readQ_0)); }
	inline Queue_t3637523393 * get_readQ_0() const { return ___readQ_0; }
	inline Queue_t3637523393 ** get_address_of_readQ_0() { return &___readQ_0; }
	inline void set_readQ_0(Queue_t3637523393 * value)
	{
		___readQ_0 = value;
		Il2CppCodeGenWriteBarrier((&___readQ_0), value);
	}

	inline static int32_t get_offset_of_writeQ_1() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___writeQ_1)); }
	inline Queue_t3637523393 * get_writeQ_1() const { return ___writeQ_1; }
	inline Queue_t3637523393 ** get_address_of_writeQ_1() { return &___writeQ_1; }
	inline void set_writeQ_1(Queue_t3637523393 * value)
	{
		___writeQ_1 = value;
		Il2CppCodeGenWriteBarrier((&___writeQ_1), value);
	}

	inline static int32_t get_offset_of_islistening_2() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___islistening_2)); }
	inline bool get_islistening_2() const { return ___islistening_2; }
	inline bool* get_address_of_islistening_2() { return &___islistening_2; }
	inline void set_islistening_2(bool value)
	{
		___islistening_2 = value;
	}

	inline static int32_t get_offset_of_useoverlappedIO_3() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___useoverlappedIO_3)); }
	inline bool get_useoverlappedIO_3() const { return ___useoverlappedIO_3; }
	inline bool* get_address_of_useoverlappedIO_3() { return &___useoverlappedIO_3; }
	inline void set_useoverlappedIO_3(bool value)
	{
		___useoverlappedIO_3 = value;
	}

	inline static int32_t get_offset_of_MinListenPort_4() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___MinListenPort_4)); }
	inline int32_t get_MinListenPort_4() const { return ___MinListenPort_4; }
	inline int32_t* get_address_of_MinListenPort_4() { return &___MinListenPort_4; }
	inline void set_MinListenPort_4(int32_t value)
	{
		___MinListenPort_4 = value;
	}

	inline static int32_t get_offset_of_MaxListenPort_5() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___MaxListenPort_5)); }
	inline int32_t get_MaxListenPort_5() const { return ___MaxListenPort_5; }
	inline int32_t* get_address_of_MaxListenPort_5() { return &___MaxListenPort_5; }
	inline void set_MaxListenPort_5(int32_t value)
	{
		___MaxListenPort_5 = value;
	}

	inline static int32_t get_offset_of_linger_timeout_8() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___linger_timeout_8)); }
	inline int32_t get_linger_timeout_8() const { return ___linger_timeout_8; }
	inline int32_t* get_address_of_linger_timeout_8() { return &___linger_timeout_8; }
	inline void set_linger_timeout_8(int32_t value)
	{
		___linger_timeout_8 = value;
	}

	inline static int32_t get_offset_of_socket_9() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___socket_9)); }
	inline intptr_t get_socket_9() const { return ___socket_9; }
	inline intptr_t* get_address_of_socket_9() { return &___socket_9; }
	inline void set_socket_9(intptr_t value)
	{
		___socket_9 = value;
	}

	inline static int32_t get_offset_of_address_family_10() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___address_family_10)); }
	inline int32_t get_address_family_10() const { return ___address_family_10; }
	inline int32_t* get_address_of_address_family_10() { return &___address_family_10; }
	inline void set_address_family_10(int32_t value)
	{
		___address_family_10 = value;
	}

	inline static int32_t get_offset_of_socket_type_11() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___socket_type_11)); }
	inline int32_t get_socket_type_11() const { return ___socket_type_11; }
	inline int32_t* get_address_of_socket_type_11() { return &___socket_type_11; }
	inline void set_socket_type_11(int32_t value)
	{
		___socket_type_11 = value;
	}

	inline static int32_t get_offset_of_protocol_type_12() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___protocol_type_12)); }
	inline int32_t get_protocol_type_12() const { return ___protocol_type_12; }
	inline int32_t* get_address_of_protocol_type_12() { return &___protocol_type_12; }
	inline void set_protocol_type_12(int32_t value)
	{
		___protocol_type_12 = value;
	}

	inline static int32_t get_offset_of_blocking_13() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___blocking_13)); }
	inline bool get_blocking_13() const { return ___blocking_13; }
	inline bool* get_address_of_blocking_13() { return &___blocking_13; }
	inline void set_blocking_13(bool value)
	{
		___blocking_13 = value;
	}

	inline static int32_t get_offset_of_blocking_thread_14() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___blocking_thread_14)); }
	inline Thread_t2300836069 * get_blocking_thread_14() const { return ___blocking_thread_14; }
	inline Thread_t2300836069 ** get_address_of_blocking_thread_14() { return &___blocking_thread_14; }
	inline void set_blocking_thread_14(Thread_t2300836069 * value)
	{
		___blocking_thread_14 = value;
		Il2CppCodeGenWriteBarrier((&___blocking_thread_14), value);
	}

	inline static int32_t get_offset_of_isbound_15() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___isbound_15)); }
	inline bool get_isbound_15() const { return ___isbound_15; }
	inline bool* get_address_of_isbound_15() { return &___isbound_15; }
	inline void set_isbound_15(bool value)
	{
		___isbound_15 = value;
	}

	inline static int32_t get_offset_of_max_bind_count_17() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___max_bind_count_17)); }
	inline int32_t get_max_bind_count_17() const { return ___max_bind_count_17; }
	inline int32_t* get_address_of_max_bind_count_17() { return &___max_bind_count_17; }
	inline void set_max_bind_count_17(int32_t value)
	{
		___max_bind_count_17 = value;
	}

	inline static int32_t get_offset_of_connected_18() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___connected_18)); }
	inline bool get_connected_18() const { return ___connected_18; }
	inline bool* get_address_of_connected_18() { return &___connected_18; }
	inline void set_connected_18(bool value)
	{
		___connected_18 = value;
	}

	inline static int32_t get_offset_of_closed_19() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___closed_19)); }
	inline bool get_closed_19() const { return ___closed_19; }
	inline bool* get_address_of_closed_19() { return &___closed_19; }
	inline void set_closed_19(bool value)
	{
		___closed_19 = value;
	}

	inline static int32_t get_offset_of_disposed_20() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___disposed_20)); }
	inline bool get_disposed_20() const { return ___disposed_20; }
	inline bool* get_address_of_disposed_20() { return &___disposed_20; }
	inline void set_disposed_20(bool value)
	{
		___disposed_20 = value;
	}

	inline static int32_t get_offset_of_seed_endpoint_21() { return static_cast<int32_t>(offsetof(Socket_t1119025450, ___seed_endpoint_21)); }
	inline EndPoint_t982345378 * get_seed_endpoint_21() const { return ___seed_endpoint_21; }
	inline EndPoint_t982345378 ** get_address_of_seed_endpoint_21() { return &___seed_endpoint_21; }
	inline void set_seed_endpoint_21(EndPoint_t982345378 * value)
	{
		___seed_endpoint_21 = value;
		Il2CppCodeGenWriteBarrier((&___seed_endpoint_21), value);
	}
};

struct Socket_t1119025450_StaticFields
{
public:
	// System.Int32 System.Net.Sockets.Socket::ipv4Supported
	int32_t ___ipv4Supported_6;
	// System.Int32 System.Net.Sockets.Socket::ipv6Supported
	int32_t ___ipv6Supported_7;
	// System.Int32 System.Net.Sockets.Socket::current_bind_count
	int32_t ___current_bind_count_16;
	// System.Reflection.MethodInfo System.Net.Sockets.Socket::check_socket_policy
	MethodInfo_t * ___check_socket_policy_22;

public:
	inline static int32_t get_offset_of_ipv4Supported_6() { return static_cast<int32_t>(offsetof(Socket_t1119025450_StaticFields, ___ipv4Supported_6)); }
	inline int32_t get_ipv4Supported_6() const { return ___ipv4Supported_6; }
	inline int32_t* get_address_of_ipv4Supported_6() { return &___ipv4Supported_6; }
	inline void set_ipv4Supported_6(int32_t value)
	{
		___ipv4Supported_6 = value;
	}

	inline static int32_t get_offset_of_ipv6Supported_7() { return static_cast<int32_t>(offsetof(Socket_t1119025450_StaticFields, ___ipv6Supported_7)); }
	inline int32_t get_ipv6Supported_7() const { return ___ipv6Supported_7; }
	inline int32_t* get_address_of_ipv6Supported_7() { return &___ipv6Supported_7; }
	inline void set_ipv6Supported_7(int32_t value)
	{
		___ipv6Supported_7 = value;
	}

	inline static int32_t get_offset_of_current_bind_count_16() { return static_cast<int32_t>(offsetof(Socket_t1119025450_StaticFields, ___current_bind_count_16)); }
	inline int32_t get_current_bind_count_16() const { return ___current_bind_count_16; }
	inline int32_t* get_address_of_current_bind_count_16() { return &___current_bind_count_16; }
	inline void set_current_bind_count_16(int32_t value)
	{
		___current_bind_count_16 = value;
	}

	inline static int32_t get_offset_of_check_socket_policy_22() { return static_cast<int32_t>(offsetof(Socket_t1119025450_StaticFields, ___check_socket_policy_22)); }
	inline MethodInfo_t * get_check_socket_policy_22() const { return ___check_socket_policy_22; }
	inline MethodInfo_t ** get_address_of_check_socket_policy_22() { return &___check_socket_policy_22; }
	inline void set_check_socket_policy_22(MethodInfo_t * value)
	{
		___check_socket_policy_22 = value;
		Il2CppCodeGenWriteBarrier((&___check_socket_policy_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKET_T1119025450_H
#ifndef IPADDRESS_T241777590_H
#define IPADDRESS_T241777590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPAddress
struct  IPAddress_t241777590  : public RuntimeObject
{
public:
	// System.Int64 System.Net.IPAddress::m_Address
	int64_t ___m_Address_0;
	// System.Net.Sockets.AddressFamily System.Net.IPAddress::m_Family
	int32_t ___m_Family_1;
	// System.UInt16[] System.Net.IPAddress::m_Numbers
	UInt16U5BU5D_t3326319531* ___m_Numbers_2;
	// System.Int64 System.Net.IPAddress::m_ScopeId
	int64_t ___m_ScopeId_3;
	// System.Int32 System.Net.IPAddress::m_HashCode
	int32_t ___m_HashCode_11;

public:
	inline static int32_t get_offset_of_m_Address_0() { return static_cast<int32_t>(offsetof(IPAddress_t241777590, ___m_Address_0)); }
	inline int64_t get_m_Address_0() const { return ___m_Address_0; }
	inline int64_t* get_address_of_m_Address_0() { return &___m_Address_0; }
	inline void set_m_Address_0(int64_t value)
	{
		___m_Address_0 = value;
	}

	inline static int32_t get_offset_of_m_Family_1() { return static_cast<int32_t>(offsetof(IPAddress_t241777590, ___m_Family_1)); }
	inline int32_t get_m_Family_1() const { return ___m_Family_1; }
	inline int32_t* get_address_of_m_Family_1() { return &___m_Family_1; }
	inline void set_m_Family_1(int32_t value)
	{
		___m_Family_1 = value;
	}

	inline static int32_t get_offset_of_m_Numbers_2() { return static_cast<int32_t>(offsetof(IPAddress_t241777590, ___m_Numbers_2)); }
	inline UInt16U5BU5D_t3326319531* get_m_Numbers_2() const { return ___m_Numbers_2; }
	inline UInt16U5BU5D_t3326319531** get_address_of_m_Numbers_2() { return &___m_Numbers_2; }
	inline void set_m_Numbers_2(UInt16U5BU5D_t3326319531* value)
	{
		___m_Numbers_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Numbers_2), value);
	}

	inline static int32_t get_offset_of_m_ScopeId_3() { return static_cast<int32_t>(offsetof(IPAddress_t241777590, ___m_ScopeId_3)); }
	inline int64_t get_m_ScopeId_3() const { return ___m_ScopeId_3; }
	inline int64_t* get_address_of_m_ScopeId_3() { return &___m_ScopeId_3; }
	inline void set_m_ScopeId_3(int64_t value)
	{
		___m_ScopeId_3 = value;
	}

	inline static int32_t get_offset_of_m_HashCode_11() { return static_cast<int32_t>(offsetof(IPAddress_t241777590, ___m_HashCode_11)); }
	inline int32_t get_m_HashCode_11() const { return ___m_HashCode_11; }
	inline int32_t* get_address_of_m_HashCode_11() { return &___m_HashCode_11; }
	inline void set_m_HashCode_11(int32_t value)
	{
		___m_HashCode_11 = value;
	}
};

struct IPAddress_t241777590_StaticFields
{
public:
	// System.Net.IPAddress System.Net.IPAddress::Any
	IPAddress_t241777590 * ___Any_4;
	// System.Net.IPAddress System.Net.IPAddress::Broadcast
	IPAddress_t241777590 * ___Broadcast_5;
	// System.Net.IPAddress System.Net.IPAddress::Loopback
	IPAddress_t241777590 * ___Loopback_6;
	// System.Net.IPAddress System.Net.IPAddress::None
	IPAddress_t241777590 * ___None_7;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Any
	IPAddress_t241777590 * ___IPv6Any_8;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Loopback
	IPAddress_t241777590 * ___IPv6Loopback_9;
	// System.Net.IPAddress System.Net.IPAddress::IPv6None
	IPAddress_t241777590 * ___IPv6None_10;

public:
	inline static int32_t get_offset_of_Any_4() { return static_cast<int32_t>(offsetof(IPAddress_t241777590_StaticFields, ___Any_4)); }
	inline IPAddress_t241777590 * get_Any_4() const { return ___Any_4; }
	inline IPAddress_t241777590 ** get_address_of_Any_4() { return &___Any_4; }
	inline void set_Any_4(IPAddress_t241777590 * value)
	{
		___Any_4 = value;
		Il2CppCodeGenWriteBarrier((&___Any_4), value);
	}

	inline static int32_t get_offset_of_Broadcast_5() { return static_cast<int32_t>(offsetof(IPAddress_t241777590_StaticFields, ___Broadcast_5)); }
	inline IPAddress_t241777590 * get_Broadcast_5() const { return ___Broadcast_5; }
	inline IPAddress_t241777590 ** get_address_of_Broadcast_5() { return &___Broadcast_5; }
	inline void set_Broadcast_5(IPAddress_t241777590 * value)
	{
		___Broadcast_5 = value;
		Il2CppCodeGenWriteBarrier((&___Broadcast_5), value);
	}

	inline static int32_t get_offset_of_Loopback_6() { return static_cast<int32_t>(offsetof(IPAddress_t241777590_StaticFields, ___Loopback_6)); }
	inline IPAddress_t241777590 * get_Loopback_6() const { return ___Loopback_6; }
	inline IPAddress_t241777590 ** get_address_of_Loopback_6() { return &___Loopback_6; }
	inline void set_Loopback_6(IPAddress_t241777590 * value)
	{
		___Loopback_6 = value;
		Il2CppCodeGenWriteBarrier((&___Loopback_6), value);
	}

	inline static int32_t get_offset_of_None_7() { return static_cast<int32_t>(offsetof(IPAddress_t241777590_StaticFields, ___None_7)); }
	inline IPAddress_t241777590 * get_None_7() const { return ___None_7; }
	inline IPAddress_t241777590 ** get_address_of_None_7() { return &___None_7; }
	inline void set_None_7(IPAddress_t241777590 * value)
	{
		___None_7 = value;
		Il2CppCodeGenWriteBarrier((&___None_7), value);
	}

	inline static int32_t get_offset_of_IPv6Any_8() { return static_cast<int32_t>(offsetof(IPAddress_t241777590_StaticFields, ___IPv6Any_8)); }
	inline IPAddress_t241777590 * get_IPv6Any_8() const { return ___IPv6Any_8; }
	inline IPAddress_t241777590 ** get_address_of_IPv6Any_8() { return &___IPv6Any_8; }
	inline void set_IPv6Any_8(IPAddress_t241777590 * value)
	{
		___IPv6Any_8 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Any_8), value);
	}

	inline static int32_t get_offset_of_IPv6Loopback_9() { return static_cast<int32_t>(offsetof(IPAddress_t241777590_StaticFields, ___IPv6Loopback_9)); }
	inline IPAddress_t241777590 * get_IPv6Loopback_9() const { return ___IPv6Loopback_9; }
	inline IPAddress_t241777590 ** get_address_of_IPv6Loopback_9() { return &___IPv6Loopback_9; }
	inline void set_IPv6Loopback_9(IPAddress_t241777590 * value)
	{
		___IPv6Loopback_9 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Loopback_9), value);
	}

	inline static int32_t get_offset_of_IPv6None_10() { return static_cast<int32_t>(offsetof(IPAddress_t241777590_StaticFields, ___IPv6None_10)); }
	inline IPAddress_t241777590 * get_IPv6None_10() const { return ___IPv6None_10; }
	inline IPAddress_t241777590 ** get_address_of_IPv6None_10() { return &___IPv6None_10; }
	inline void set_IPv6None_10(IPAddress_t241777590 * value)
	{
		___IPv6None_10 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6None_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPADDRESS_T241777590_H
#ifndef UDPCLIENT_T967282006_H
#define UDPCLIENT_T967282006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.UdpClient
struct  UdpClient_t967282006  : public RuntimeObject
{
public:
	// System.Boolean System.Net.Sockets.UdpClient::disposed
	bool ___disposed_0;
	// System.Boolean System.Net.Sockets.UdpClient::active
	bool ___active_1;
	// System.Net.Sockets.Socket System.Net.Sockets.UdpClient::socket
	Socket_t1119025450 * ___socket_2;
	// System.Net.Sockets.AddressFamily System.Net.Sockets.UdpClient::family
	int32_t ___family_3;
	// System.Byte[] System.Net.Sockets.UdpClient::recvbuffer
	ByteU5BU5D_t4116647657* ___recvbuffer_4;

public:
	inline static int32_t get_offset_of_disposed_0() { return static_cast<int32_t>(offsetof(UdpClient_t967282006, ___disposed_0)); }
	inline bool get_disposed_0() const { return ___disposed_0; }
	inline bool* get_address_of_disposed_0() { return &___disposed_0; }
	inline void set_disposed_0(bool value)
	{
		___disposed_0 = value;
	}

	inline static int32_t get_offset_of_active_1() { return static_cast<int32_t>(offsetof(UdpClient_t967282006, ___active_1)); }
	inline bool get_active_1() const { return ___active_1; }
	inline bool* get_address_of_active_1() { return &___active_1; }
	inline void set_active_1(bool value)
	{
		___active_1 = value;
	}

	inline static int32_t get_offset_of_socket_2() { return static_cast<int32_t>(offsetof(UdpClient_t967282006, ___socket_2)); }
	inline Socket_t1119025450 * get_socket_2() const { return ___socket_2; }
	inline Socket_t1119025450 ** get_address_of_socket_2() { return &___socket_2; }
	inline void set_socket_2(Socket_t1119025450 * value)
	{
		___socket_2 = value;
		Il2CppCodeGenWriteBarrier((&___socket_2), value);
	}

	inline static int32_t get_offset_of_family_3() { return static_cast<int32_t>(offsetof(UdpClient_t967282006, ___family_3)); }
	inline int32_t get_family_3() const { return ___family_3; }
	inline int32_t* get_address_of_family_3() { return &___family_3; }
	inline void set_family_3(int32_t value)
	{
		___family_3 = value;
	}

	inline static int32_t get_offset_of_recvbuffer_4() { return static_cast<int32_t>(offsetof(UdpClient_t967282006, ___recvbuffer_4)); }
	inline ByteU5BU5D_t4116647657* get_recvbuffer_4() const { return ___recvbuffer_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_recvbuffer_4() { return &___recvbuffer_4; }
	inline void set_recvbuffer_4(ByteU5BU5D_t4116647657* value)
	{
		___recvbuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___recvbuffer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPCLIENT_T967282006_H
#ifndef OBJECTDISPOSEDEXCEPTION_T21392786_H
#define OBJECTDISPOSEDEXCEPTION_T21392786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ObjectDisposedException
struct  ObjectDisposedException_t21392786  : public InvalidOperationException_t56020091
{
public:
	// System.String System.ObjectDisposedException::obj_name
	String_t* ___obj_name_12;
	// System.String System.ObjectDisposedException::msg
	String_t* ___msg_13;

public:
	inline static int32_t get_offset_of_obj_name_12() { return static_cast<int32_t>(offsetof(ObjectDisposedException_t21392786, ___obj_name_12)); }
	inline String_t* get_obj_name_12() const { return ___obj_name_12; }
	inline String_t** get_address_of_obj_name_12() { return &___obj_name_12; }
	inline void set_obj_name_12(String_t* value)
	{
		___obj_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___obj_name_12), value);
	}

	inline static int32_t get_offset_of_msg_13() { return static_cast<int32_t>(offsetof(ObjectDisposedException_t21392786, ___msg_13)); }
	inline String_t* get_msg_13() const { return ___msg_13; }
	inline String_t** get_address_of_msg_13() { return &___msg_13; }
	inline void set_msg_13(String_t* value)
	{
		___msg_13 = value;
		Il2CppCodeGenWriteBarrier((&___msg_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDISPOSEDEXCEPTION_T21392786_H
#ifndef EVENTHANDLER_1_T2793618168_H
#define EVENTHANDLER_1_T2793618168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<OSCsharp.Net.OscBundleReceivedEventArgs>
struct  EventHandler_1_t2793618168  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T2793618168_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef EVENTHANDLER_1_T319478644_H
#define EVENTHANDLER_1_T319478644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>
struct  EventHandler_1_t319478644  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T319478644_H
#ifndef EVENTHANDLER_1_T2882212236_H
#define EVENTHANDLER_1_T2882212236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<OSCsharp.Net.OscPacketReceivedEventArgs>
struct  EventHandler_1_t2882212236  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T2882212236_H
#ifndef EVENTHANDLER_1_T314378975_H
#define EVENTHANDLER_1_T314378975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>
struct  EventHandler_1_t314378975  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T314378975_H
#ifndef OSCTIMETAG_T749151765_H
#define OSCTIMETAG_T749151765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscTimeTag
struct  OscTimeTag_t749151765  : public RuntimeObject
{
public:
	// System.DateTime OSCsharp.Data.OscTimeTag::timeStamp
	DateTime_t3738529785  ___timeStamp_2;

public:
	inline static int32_t get_offset_of_timeStamp_2() { return static_cast<int32_t>(offsetof(OscTimeTag_t749151765, ___timeStamp_2)); }
	inline DateTime_t3738529785  get_timeStamp_2() const { return ___timeStamp_2; }
	inline DateTime_t3738529785 * get_address_of_timeStamp_2() { return &___timeStamp_2; }
	inline void set_timeStamp_2(DateTime_t3738529785  value)
	{
		___timeStamp_2 = value;
	}
};

struct OscTimeTag_t749151765_StaticFields
{
public:
	// System.DateTime OSCsharp.Data.OscTimeTag::Epoch
	DateTime_t3738529785  ___Epoch_0;
	// OSCsharp.Data.OscTimeTag OSCsharp.Data.OscTimeTag::MinValue
	OscTimeTag_t749151765 * ___MinValue_1;

public:
	inline static int32_t get_offset_of_Epoch_0() { return static_cast<int32_t>(offsetof(OscTimeTag_t749151765_StaticFields, ___Epoch_0)); }
	inline DateTime_t3738529785  get_Epoch_0() const { return ___Epoch_0; }
	inline DateTime_t3738529785 * get_address_of_Epoch_0() { return &___Epoch_0; }
	inline void set_Epoch_0(DateTime_t3738529785  value)
	{
		___Epoch_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(OscTimeTag_t749151765_StaticFields, ___MinValue_1)); }
	inline OscTimeTag_t749151765 * get_MinValue_1() const { return ___MinValue_1; }
	inline OscTimeTag_t749151765 ** get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(OscTimeTag_t749151765 * value)
	{
		___MinValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___MinValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCTIMETAG_T749151765_H
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Char[]
struct CharU5BU5D_t3528271667  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};


// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Object>(System.Byte[],System.Int32&)
extern "C"  RuntimeObject * OscPacket_ValueFromByteArray_TisRuntimeObject_m3985474922_gshared (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, int32_t* ___start1, const RuntimeMethod* method);
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Int32>(System.Byte[],System.Int32&)
extern "C"  int32_t OscPacket_ValueFromByteArray_TisInt32_t2950945753_m1459680167_gshared (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, int32_t* ___start1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Int64>(System.Byte[],System.Int32&)
extern "C"  int64_t OscPacket_ValueFromByteArray_TisInt64_t3736567304_m4196370337_gshared (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, int32_t* ___start1, const RuntimeMethod* method);
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Single>(System.Byte[],System.Int32&)
extern "C"  float OscPacket_ValueFromByteArray_TisSingle_t1397266774_m1683906482_gshared (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, int32_t* ___start1, const RuntimeMethod* method);
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Double>(System.Byte[],System.Int32&)
extern "C"  double OscPacket_ValueFromByteArray_TisDouble_t594665363_m997755250_gshared (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, int32_t* ___start1, const RuntimeMethod* method);
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Char>(System.Byte[],System.Int32&)
extern "C"  Il2CppChar OscPacket_ValueFromByteArray_TisChar_t3634460470_m3452883113_gshared (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, int32_t* ___start1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m182537451_gshared (Dictionary_2_t3384741 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m1279427033_gshared (Dictionary_2_t3384741 * __this, RuntimeObject * p0, int32_t p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3959998165_gshared (Dictionary_2_t3384741 * __this, RuntimeObject * p0, int32_t* p1, const RuntimeMethod* method);
// System.Collections.ObjectModel.ReadOnlyCollection`1<!0> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t4292682451 * List_1_AsReadOnly_m1069347492_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// T[] OSCsharp.Utils.Utility::CopySubArray<System.Byte>(T[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t4116647657* Utility_CopySubArray_TisByte_t1134296376_m796220017_gshared (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___source0, int32_t ___start1, int32_t ___length2, const RuntimeMethod* method);
// System.Void System.EventHandler`1<System.Object>::Invoke(System.Object,!0)
extern "C"  void EventHandler_1_Invoke_m4124830036_gshared (EventHandler_1_t1004265597 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);

// System.Void OSCsharp.Data.OscPacket::.ctor(System.String)
extern "C"  void OscPacket__ctor_m1212150461 (OscPacket_t3204151022 * __this, String_t* ___address0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.String>(System.Byte[],System.Int32&)
#define OscPacket_ValueFromByteArray_TisString_t_m2385610542(__this /* static, unused */, ___data0, ___start1, method) ((  String_t* (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t4116647657*, int32_t*, const RuntimeMethod*))OscPacket_ValueFromByteArray_TisRuntimeObject_m3985474922_gshared)(__this /* static, unused */, ___data0, ___start1, method)
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m215368492 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor()
extern "C"  void ArgumentException__ctor_m3698743796 (ArgumentException_t132251570 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T OSCsharp.Data.OscPacket::ValueFromByteArray<OSCsharp.Data.OscTimeTag>(System.Byte[],System.Int32&)
#define OscPacket_ValueFromByteArray_TisOscTimeTag_t749151765_m1850822955(__this /* static, unused */, ___data0, ___start1, method) ((  OscTimeTag_t749151765 * (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t4116647657*, int32_t*, const RuntimeMethod*))OscPacket_ValueFromByteArray_TisRuntimeObject_m3985474922_gshared)(__this /* static, unused */, ___data0, ___start1, method)
// System.Void OSCsharp.Data.OscBundle::.ctor(OSCsharp.Data.OscTimeTag)
extern "C"  void OscBundle__ctor_m645648858 (OscBundle_t1226152694 * __this, OscTimeTag_t749151765 * ___timeStamp0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Int32>(System.Byte[],System.Int32&)
#define OscPacket_ValueFromByteArray_TisInt32_t2950945753_m1459680167(__this /* static, unused */, ___data0, ___start1, method) ((  int32_t (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t4116647657*, int32_t*, const RuntimeMethod*))OscPacket_ValueFromByteArray_TisInt32_t2950945753_m1459680167_gshared)(__this /* static, unused */, ___data0, ___start1, method)
// OSCsharp.Data.OscPacket OSCsharp.Data.OscPacket::FromByteArray(System.Byte[],System.Int32&,System.Int32)
extern "C"  OscPacket_t3204151022 * OscPacket_FromByteArray_m4260015432 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, int32_t* ___start1, int32_t ___end2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSCsharp.Data.OscTimeTag::op_LessThan(OSCsharp.Data.OscTimeTag,OSCsharp.Data.OscTimeTag)
extern "C"  bool OscTimeTag_op_LessThan_m2293409653 (RuntimeObject * __this /* static, unused */, OscTimeTag_t749151765 * ___lhs0, OscTimeTag_t749151765 * ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String)
extern "C"  void Exception__ctor_m1152696503 (Exception_t * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
#define List_1_Add_m3338814081(__this, p0, method) ((  void (*) (List_1_t257213610 *, RuntimeObject *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
#define List_1_get_Count_m2934127733(__this, method) ((  int32_t (*) (List_1_t257213610 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Boolean System.String::StartsWith(System.String)
extern "C"  bool String_StartsWith_m1759067526 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m1312628991 (ArgumentException_t132251570 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Char::ToString()
extern "C"  String_t* Char_ToString_m3588025615 (Il2CppChar* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Data.OscMessage::.ctor(System.String)
extern "C"  void OscMessage__ctor_m1172728952 (OscMessage_t3323977005 * __this, String_t* ___address0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char[] System.String::ToCharArray()
extern "C"  CharU5BU5D_t3528271667* String_ToCharArray_m1492846834 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Int64>(System.Byte[],System.Int32&)
#define OscPacket_ValueFromByteArray_TisInt64_t3736567304_m4196370337(__this /* static, unused */, ___data0, ___start1, method) ((  int64_t (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t4116647657*, int32_t*, const RuntimeMethod*))OscPacket_ValueFromByteArray_TisInt64_t3736567304_m4196370337_gshared)(__this /* static, unused */, ___data0, ___start1, method)
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Single>(System.Byte[],System.Int32&)
#define OscPacket_ValueFromByteArray_TisSingle_t1397266774_m1683906482(__this /* static, unused */, ___data0, ___start1, method) ((  float (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t4116647657*, int32_t*, const RuntimeMethod*))OscPacket_ValueFromByteArray_TisSingle_t1397266774_m1683906482_gshared)(__this /* static, unused */, ___data0, ___start1, method)
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Double>(System.Byte[],System.Int32&)
#define OscPacket_ValueFromByteArray_TisDouble_t594665363_m997755250(__this /* static, unused */, ___data0, ___start1, method) ((  double (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t4116647657*, int32_t*, const RuntimeMethod*))OscPacket_ValueFromByteArray_TisDouble_t594665363_m997755250_gshared)(__this /* static, unused */, ___data0, ___start1, method)
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Byte[]>(System.Byte[],System.Int32&)
#define OscPacket_ValueFromByteArray_TisByteU5BU5D_t4116647657_m2669022459(__this /* static, unused */, ___data0, ___start1, method) ((  ByteU5BU5D_t4116647657* (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t4116647657*, int32_t*, const RuntimeMethod*))OscPacket_ValueFromByteArray_TisRuntimeObject_m3985474922_gshared)(__this /* static, unused */, ___data0, ___start1, method)
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Char>(System.Byte[],System.Int32&)
#define OscPacket_ValueFromByteArray_TisChar_t3634460470_m3452883113(__this /* static, unused */, ___data0, ___start1, method) ((  Il2CppChar (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t4116647657*, int32_t*, const RuntimeMethod*))OscPacket_ValueFromByteArray_TisChar_t3634460470_m3452883113_gshared)(__this /* static, unused */, ___data0, ___start1, method)
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m88164663 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::.ctor(System.Int32)
#define Dictionary_2__ctor_m2392909825(__this, p0, method) ((  void (*) (Dictionary_2_t2736202052 *, int32_t, const RuntimeMethod*))Dictionary_2__ctor_m182537451_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1)
#define Dictionary_2_Add_m282647386(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2736202052 *, String_t*, int32_t, const RuntimeMethod*))Dictionary_2_Add_m1279427033_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m1013208020(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t2736202052 *, String_t*, int32_t*, const RuntimeMethod*))Dictionary_2_TryGetValue_m3959998165_gshared)(__this, p0, p1, method)
// System.Boolean System.Single::IsPositiveInfinity(System.Single)
extern "C"  bool Single_IsPositiveInfinity_m1411272350 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<!0> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
#define List_1_AsReadOnly_m1069347492(__this, method) ((  ReadOnlyCollection_1_t4292682451 * (*) (List_1_t257213610 *, const RuntimeMethod*))List_1_AsReadOnly_m1069347492_gshared)(__this, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Data.OscPacket::set_Address(System.String)
extern "C"  void OscPacket_set_Address_m2086307222 (OscPacket_t3204151022 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
#define List_1__ctor_m2321703786(__this, method) ((  void (*) (List_1_t257213610 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// OSCsharp.Data.OscBundle OSCsharp.Data.OscBundle::FromByteArray(System.Byte[],System.Int32&,System.Int32)
extern "C"  OscBundle_t1226152694 * OscBundle_FromByteArray_m76398600 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, int32_t* ___start1, int32_t ___end2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// OSCsharp.Data.OscMessage OSCsharp.Data.OscMessage::FromByteArray(System.Byte[],System.Int32&)
extern "C"  OscMessage_t3323977005 * OscMessage_FromByteArray_m1008684133 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, int32_t* ___start1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Data.OscTimeTag::Set(System.DateTime)
extern "C"  void OscTimeTag_Set_m2513023378 (OscTimeTag_t749151765 * __this, DateTime_t3738529785  ___timeStamp0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T[] OSCsharp.Utils.Utility::CopySubArray<System.Byte>(T[],System.Int32,System.Int32)
#define Utility_CopySubArray_TisByte_t1134296376_m796220017(__this /* static, unused */, ___source0, ___start1, ___length2, method) ((  ByteU5BU5D_t4116647657* (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t4116647657*, int32_t, int32_t, const RuntimeMethod*))Utility_CopySubArray_TisByte_t1134296376_m796220017_gshared)(__this /* static, unused */, ___source0, ___start1, ___length2, method)
// System.Boolean OSCsharp.Data.OscPacket::get_LittleEndianByteOrder()
extern "C"  bool OscPacket_get_LittleEndianByteOrder_m412490820 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] OSCsharp.Utils.Utility::SwapEndian(System.Byte[])
extern "C"  ByteU5BU5D_t4116647657* Utility_SwapEndian_m1429200246 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.BitConverter::ToUInt32(System.Byte[],System.Int32)
extern "C"  uint32_t BitConverter_ToUInt32_m3737646381 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTime::AddSeconds(System.Double)
extern "C"  DateTime_t3738529785  DateTime_AddSeconds_m332574389 (DateTime_t3738529785 * __this, double p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTime::AddMilliseconds(System.Double)
extern "C"  DateTime_t3738529785  DateTime_AddMilliseconds_m3713972790 (DateTime_t3738529785 * __this, double p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m610702577 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.DateTime OSCsharp.Data.OscTimeTag::get_DateTime()
extern "C"  DateTime_t3738529785  OscTimeTag_get_DateTime_m3628459398 (OscTimeTag_t749151765 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTime::op_Equality(System.DateTime,System.DateTime)
extern "C"  bool DateTime_op_Equality_m1022058599 (RuntimeObject * __this /* static, unused */, DateTime_t3738529785  p0, DateTime_t3738529785  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTime::op_LessThan(System.DateTime,System.DateTime)
extern "C"  bool DateTime_op_LessThan_m2497205152 (RuntimeObject * __this /* static, unused */, DateTime_t3738529785  p0, DateTime_t3738529785  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.TimeSpan::FromMilliseconds(System.Double)
extern "C"  TimeSpan_t881159249  TimeSpan_FromMilliseconds_m579366253 (RuntimeObject * __this /* static, unused */, double p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTime::op_Addition(System.DateTime,System.TimeSpan)
extern "C"  DateTime_t3738529785  DateTime_op_Addition_m1857121695 (RuntimeObject * __this /* static, unused */, DateTime_t3738529785  p0, TimeSpan_t881159249  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTime::op_GreaterThanOrEqual(System.DateTime,System.DateTime)
extern "C"  bool DateTime_op_GreaterThanOrEqual_m674703316 (RuntimeObject * __this /* static, unused */, DateTime_t3738529785  p0, DateTime_t3738529785  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.DateTime::get_Ticks()
extern "C"  int64_t DateTime_get_Ticks_m1550640881 (DateTime_t3738529785 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.DateTimeKind System.DateTime::get_Kind()
extern "C"  int32_t DateTime_get_Kind_m2154871796 (DateTime_t3738529785 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTime::.ctor(System.Int64,System.DateTimeKind)
extern "C"  void DateTime__ctor_m1095105629 (DateTime_t3738529785 * __this, int64_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSCsharp.Data.OscTimeTag::IsValidTime(System.DateTime)
extern "C"  bool OscTimeTag_IsValidTime_m2642829427 (RuntimeObject * __this /* static, unused */, DateTime_t3738529785  ___timeStamp0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSCsharp.Data.OscTimeTag::op_Equality(OSCsharp.Data.OscTimeTag,OSCsharp.Data.OscTimeTag)
extern "C"  bool OscTimeTag_op_Equality_m793844905 (RuntimeObject * __this /* static, unused */, OscTimeTag_t749151765 * ___lhs0, OscTimeTag_t749151765 * ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTime::Equals(System.DateTime)
extern "C"  bool DateTime_Equals_m4001498422 (DateTime_t3738529785 * __this, DateTime_t3738529785  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTime::GetHashCode()
extern "C"  int32_t DateTime_GetHashCode_m2261847002 (DateTime_t3738529785 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.DateTime::ToString()
extern "C"  String_t* DateTime_ToString_m884486936 (DateTime_t3738529785 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTime::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void DateTime__ctor_m2030998145 (DateTime_t3738529785 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Data.OscTimeTag::.ctor(System.DateTime)
extern "C"  void OscTimeTag__ctor_m3799858592 (OscTimeTag_t749151765 * __this, DateTime_t3738529785  ___timeStamp0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventArgs::.ctor()
extern "C"  void EventArgs__ctor_m32674013 (EventArgs_t3591816995 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.OscBundleReceivedEventArgs::set_Bundle(OSCsharp.Data.OscBundle)
extern "C"  void OscBundleReceivedEventArgs_set_Bundle_m2295498377 (OscBundleReceivedEventArgs_t574491439 * __this, OscBundle_t1226152694 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.OscMessageReceivedEventArgs::set_Message(OSCsharp.Data.OscMessage)
extern "C"  void OscMessageReceivedEventArgs_set_Message_m517697176 (OscMessageReceivedEventArgs_t2390219542 * __this, OscMessage_t3323977005 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.OscPacketReceivedEventArgs::set_Packet(OSCsharp.Data.OscPacket)
extern "C"  void OscPacketReceivedEventArgs_set_Packet_m3963054541 (OscPacketReceivedEventArgs_t663085507 * __this, OscPacket_t3204151022 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Combine_m1859655160 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Remove_m334097152 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::.ctor(System.Net.IPAddress,System.Int32,System.Boolean)
extern "C"  void UDPReceiver__ctor_m4189065602 (UDPReceiver_t1881969775 * __this, IPAddress_t241777590 * ___ipAddress0, int32_t ___port1, bool ___consumeParsingExceptions2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::.ctor(System.Net.IPAddress,System.Int32,OSCsharp.Net.TransmissionType,System.Net.IPAddress,System.Boolean)
extern "C"  void UDPReceiver__ctor_m2102639691 (UDPReceiver_t1881969775 * __this, IPAddress_t241777590 * ___ipAddress0, int32_t ___port1, int32_t ___transmissionType2, IPAddress_t241777590 * ___multicastAddress3, bool ___consumeParsingExceptions4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_IPAddress(System.Net.IPAddress)
extern "C"  void UDPReceiver_set_IPAddress_m1713128732 (UDPReceiver_t1881969775 * __this, IPAddress_t241777590 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_Port(System.Int32)
extern "C"  void UDPReceiver_set_Port_m2702541475 (UDPReceiver_t1881969775 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_TransmissionType(OSCsharp.Net.TransmissionType)
extern "C"  void UDPReceiver_set_TransmissionType_m2455005189 (UDPReceiver_t1881969775 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// OSCsharp.Net.TransmissionType OSCsharp.Net.UDPReceiver::get_TransmissionType()
extern "C"  int32_t UDPReceiver_get_TransmissionType_m1649715736 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_MulticastAddress(System.Net.IPAddress)
extern "C"  void UDPReceiver_set_MulticastAddress_m1606247330 (UDPReceiver_t1881969775 * __this, IPAddress_t241777590 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_ConsumeParsingExceptions(System.Boolean)
extern "C"  void UDPReceiver_set_ConsumeParsingExceptions_m2769974665 (UDPReceiver_t1881969775 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.AsyncCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AsyncCallback__ctor_m530647953 (AsyncCallback_t3962456242 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress OSCsharp.Net.UDPReceiver::get_IPAddress()
extern "C"  IPAddress_t241777590 * UDPReceiver_get_IPAddress_m4104541328 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 OSCsharp.Net.UDPReceiver::get_Port()
extern "C"  int32_t UDPReceiver_get_Port_m21013429 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.IPEndPoint::.ctor(System.Net.IPAddress,System.Int32)
extern "C"  void IPEndPoint__ctor_m2833647099 (IPEndPoint_t3791887218 * __this, IPAddress_t241777590 * p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_IPEndPoint(System.Net.IPEndPoint)
extern "C"  void UDPReceiver_set_IPEndPoint_m3483499756 (UDPReceiver_t1881969775 * __this, IPEndPoint_t3791887218 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver::get_IPEndPoint()
extern "C"  IPEndPoint_t3791887218 * UDPReceiver_get_IPEndPoint_m715339074 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::.ctor(System.Net.IPEndPoint)
extern "C"  void UdpClient__ctor_m536015148 (UdpClient_t967282006 * __this, IPEndPoint_t3791887218 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.Socket::.ctor(System.Net.Sockets.AddressFamily,System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType)
extern "C"  void Socket__ctor_m3479084642 (Socket_t1119025450 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.Socket::SetSocketOption(System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,System.Int32)
extern "C"  void Socket_SetSocketOption_m483522974 (Socket_t1119025450 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.Socket::Bind(System.Net.EndPoint)
extern "C"  void Socket_Bind_m1387808352 (Socket_t1119025450 * __this, EndPoint_t982345378 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::.ctor()
extern "C"  void UdpClient__ctor_m4070023699 (UdpClient_t967282006 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::set_Client(System.Net.Sockets.Socket)
extern "C"  void UdpClient_set_Client_m1161482078 (UdpClient_t967282006 * __this, Socket_t1119025450 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress OSCsharp.Net.UDPReceiver::get_MulticastAddress()
extern "C"  IPAddress_t241777590 * UDPReceiver_get_MulticastAddress_m1408152394 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::JoinMulticastGroup(System.Net.IPAddress)
extern "C"  void UdpClient_JoinMulticastGroup_m2638505107 (UdpClient_t967282006 * __this, IPAddress_t241777590 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor()
extern "C"  void Exception__ctor_m213470898 (Exception_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver/UdpState::.ctor(System.Net.Sockets.UdpClient,System.Net.IPEndPoint)
extern "C"  void UdpState__ctor_m2410451527 (UdpState_t629681666 * __this, UdpClient_t967282006 * ___client0, IPEndPoint_t3791887218 * ___ipEndPoint1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.Sockets.UdpClient::BeginReceive(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UdpClient_BeginReceive_m3486162304 (UdpClient_t967282006 * __this, AsyncCallback_t3962456242 * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::DropMulticastGroup(System.Net.IPAddress)
extern "C"  void UdpClient_DropMulticastGroup_m3633561453 (UdpClient_t967282006 * __this, IPAddress_t241777590 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::Close()
extern "C"  void UdpClient_Close_m4217442468 (UdpClient_t967282006 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Net.Sockets.UdpClient OSCsharp.Net.UDPReceiver/UdpState::get_Client()
extern "C"  UdpClient_t967282006 * UdpState_get_Client_m2855211709 (UdpState_t629681666 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver/UdpState::get_IPEndPoint()
extern "C"  IPEndPoint_t3791887218 * UdpState_get_IPEndPoint_m3884943819 (UdpState_t629681666 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.Sockets.UdpClient::EndReceive(System.IAsyncResult,System.Net.IPEndPoint&)
extern "C"  ByteU5BU5D_t4116647657* UdpClient_EndReceive_m765429616 (UdpClient_t967282006 * __this, RuntimeObject* p0, IPEndPoint_t3791887218 ** p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::parseData(System.Net.IPEndPoint,System.Byte[])
extern "C"  void UDPReceiver_parseData_m1225544979 (UDPReceiver_t1881969775 * __this, IPEndPoint_t3791887218 * ___sourceEndPoint0, ByteU5BU5D_t4116647657* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// OSCsharp.Data.OscPacket OSCsharp.Data.OscPacket::FromByteArray(System.Byte[])
extern "C"  OscPacket_t3204151022 * OscPacket_FromByteArray_m2774387762 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::onPacketReceived(OSCsharp.Data.OscPacket)
extern "C"  void UDPReceiver_onPacketReceived_m1435591783 (UDPReceiver_t1881969775 * __this, OscPacket_t3204151022 * ___packet0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::onBundleReceived(OSCsharp.Data.OscBundle)
extern "C"  void UDPReceiver_onBundleReceived_m1364519174 (UDPReceiver_t1881969775 * __this, OscBundle_t1226152694 * ___bundle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::onMessageReceived(OSCsharp.Data.OscMessage)
extern "C"  void UDPReceiver_onMessageReceived_m1127692520 (UDPReceiver_t1881969775 * __this, OscMessage_t3323977005 * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSCsharp.Net.UDPReceiver::get_ConsumeParsingExceptions()
extern "C"  bool UDPReceiver_get_ConsumeParsingExceptions_m2693528941 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::onError(System.Exception)
extern "C"  void UDPReceiver_onError_m4129978076 (UDPReceiver_t1881969775 * __this, Exception_t * ___ex0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.OscPacketReceivedEventArgs::.ctor(OSCsharp.Data.OscPacket)
extern "C"  void OscPacketReceivedEventArgs__ctor_m3956995085 (OscPacketReceivedEventArgs_t663085507 * __this, OscPacket_t3204151022 * ___packet0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<OSCsharp.Net.OscPacketReceivedEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m866284127(__this, p0, p1, method) ((  void (*) (EventHandler_1_t2882212236 *, RuntimeObject *, OscPacketReceivedEventArgs_t663085507 *, const RuntimeMethod*))EventHandler_1_Invoke_m4124830036_gshared)(__this, p0, p1, method)
// System.Void OSCsharp.Net.OscBundleReceivedEventArgs::.ctor(OSCsharp.Data.OscBundle)
extern "C"  void OscBundleReceivedEventArgs__ctor_m3816053266 (OscBundleReceivedEventArgs_t574491439 * __this, OscBundle_t1226152694 * ___bundle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<OSCsharp.Net.OscBundleReceivedEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m2727238407(__this, p0, p1, method) ((  void (*) (EventHandler_1_t2793618168 *, RuntimeObject *, OscBundleReceivedEventArgs_t574491439 *, const RuntimeMethod*))EventHandler_1_Invoke_m4124830036_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IList`1<System.Object> OSCsharp.Data.OscPacket::get_Data()
extern "C"  RuntimeObject* OscPacket_get_Data_m3397942478 (OscPacket_t3204151022 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.OscMessageReceivedEventArgs::.ctor(OSCsharp.Data.OscMessage)
extern "C"  void OscMessageReceivedEventArgs__ctor_m3694517805 (OscMessageReceivedEventArgs_t2390219542 * __this, OscMessage_t3323977005 * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m2312074494(__this, p0, p1, method) ((  void (*) (EventHandler_1_t314378975 *, RuntimeObject *, OscMessageReceivedEventArgs_t2390219542 *, const RuntimeMethod*))EventHandler_1_Invoke_m4124830036_gshared)(__this, p0, p1, method)
// System.Void OSCsharp.Utils.ExceptionEventArgs::.ctor(System.Exception)
extern "C"  void ExceptionEventArgs__ctor_m1472460080 (ExceptionEventArgs_t2395319211 * __this, Exception_t * ___ex0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m3384041340(__this, p0, p1, method) ((  void (*) (EventHandler_1_t319478644 *, RuntimeObject *, ExceptionEventArgs_t2395319211 *, const RuntimeMethod*))EventHandler_1_Invoke_m4124830036_gshared)(__this, p0, p1, method)
// System.Void OSCsharp.Net.UDPReceiver/UdpState::set_Client(System.Net.Sockets.UdpClient)
extern "C"  void UdpState_set_Client_m3591208944 (UdpState_t629681666 * __this, UdpClient_t967282006 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver/UdpState::set_IPEndPoint(System.Net.IPEndPoint)
extern "C"  void UdpState_set_IPEndPoint_m1779452615 (UdpState_t629681666 * __this, IPEndPoint_t3791887218 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Utils.ExceptionEventArgs::set_Exception(System.Exception)
extern "C"  void ExceptionEventArgs_set_Exception_m1841099795 (ExceptionEventArgs_t2395319211 * __this, Exception_t * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean OSCsharp.Data.OscBundle::get_IsBundle()
extern "C"  bool OscBundle_get_IsBundle_m1135786325 (OscBundle_t1226152694 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Void OSCsharp.Data.OscBundle::.ctor(OSCsharp.Data.OscTimeTag)
extern "C"  void OscBundle__ctor_m645648858 (OscBundle_t1226152694 * __this, OscTimeTag_t749151765 * ___timeStamp0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscBundle__ctor_m645648858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		OscPacket__ctor_m1212150461(__this, _stringLiteral3032477599, /*hidden argument*/NULL);
		OscTimeTag_t749151765 * L_0 = ___timeStamp0;
		__this->set_timeStamp_5(L_0);
		return;
	}
}
// OSCsharp.Data.OscBundle OSCsharp.Data.OscBundle::FromByteArray(System.Byte[],System.Int32&,System.Int32)
extern "C"  OscBundle_t1226152694 * OscBundle_FromByteArray_m76398600 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, int32_t* ___start1, int32_t ___end2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscBundle_FromByteArray_m76398600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	OscTimeTag_t749151765 * V_1 = NULL;
	OscBundle_t1226152694 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		ByteU5BU5D_t4116647657* L_0 = ___data0;
		int32_t* L_1 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		String_t* L_2 = OscPacket_ValueFromByteArray_TisString_t_m2385610542(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/OscPacket_ValueFromByteArray_TisString_t_m2385610542_RuntimeMethod_var);
		V_0 = L_2;
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_3, _stringLiteral3032477599, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001e;
		}
	}
	{
		ArgumentException_t132251570 * L_5 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3698743796(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_001e:
	{
		ByteU5BU5D_t4116647657* L_6 = ___data0;
		int32_t* L_7 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		OscTimeTag_t749151765 * L_8 = OscPacket_ValueFromByteArray_TisOscTimeTag_t749151765_m1850822955(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/OscPacket_ValueFromByteArray_TisOscTimeTag_t749151765_m1850822955_RuntimeMethod_var);
		V_1 = L_8;
		OscTimeTag_t749151765 * L_9 = V_1;
		OscBundle_t1226152694 * L_10 = (OscBundle_t1226152694 *)il2cpp_codegen_object_new(OscBundle_t1226152694_il2cpp_TypeInfo_var);
		OscBundle__ctor_m645648858(L_10, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		goto IL_0050;
	}

IL_0032:
	{
		ByteU5BU5D_t4116647657* L_11 = ___data0;
		int32_t* L_12 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		int32_t L_13 = OscPacket_ValueFromByteArray_TisInt32_t2950945753_m1459680167(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/OscPacket_ValueFromByteArray_TisInt32_t2950945753_m1459680167_RuntimeMethod_var);
		V_3 = L_13;
		int32_t* L_14 = ___start1;
		int32_t L_15 = V_3;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_14)), (int32_t)L_15));
		OscBundle_t1226152694 * L_16 = V_2;
		ByteU5BU5D_t4116647657* L_17 = ___data0;
		int32_t* L_18 = ___start1;
		int32_t L_19 = V_4;
		OscPacket_t3204151022 * L_20 = OscPacket_FromByteArray_m4260015432(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(5 /* System.Int32 OSCsharp.Data.OscPacket::Append(System.Object) */, L_16, L_20);
	}

IL_0050:
	{
		int32_t* L_21 = ___start1;
		int32_t L_22 = ___end2;
		if ((((int32_t)(*((int32_t*)L_21))) < ((int32_t)L_22)))
		{
			goto IL_0032;
		}
	}
	{
		OscBundle_t1226152694 * L_23 = V_2;
		return L_23;
	}
}
// System.Int32 OSCsharp.Data.OscBundle::Append(System.Object)
extern "C"  int32_t OscBundle_Append_m1737276556 (OscBundle_t1226152694 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscBundle_Append_m1737276556_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OscBundle_t1226152694 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___value0;
		if (((OscPacket_t3204151022 *)IsInstClass((RuntimeObject*)L_0, OscPacket_t3204151022_il2cpp_TypeInfo_var)))
		{
			goto IL_0011;
		}
	}
	{
		ArgumentException_t132251570 * L_1 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3698743796(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		RuntimeObject * L_2 = ___value0;
		V_0 = ((OscBundle_t1226152694 *)IsInstSealed((RuntimeObject*)L_2, OscBundle_t1226152694_il2cpp_TypeInfo_var));
		OscBundle_t1226152694 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		OscBundle_t1226152694 * L_4 = V_0;
		NullCheck(L_4);
		OscTimeTag_t749151765 * L_5 = L_4->get_timeStamp_5();
		OscTimeTag_t749151765 * L_6 = __this->get_timeStamp_5();
		IL2CPP_RUNTIME_CLASS_INIT(OscTimeTag_t749151765_il2cpp_TypeInfo_var);
		bool L_7 = OscTimeTag_op_LessThan_m2293409653(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003f;
		}
	}
	{
		Exception_t * L_8 = (Exception_t *)il2cpp_codegen_object_new(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m1152696503(L_8, _stringLiteral2056298437, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003f:
	{
		List_1_t257213610 * L_9 = ((OscPacket_t3204151022 *)__this)->get_data_2();
		RuntimeObject * L_10 = ___value0;
		NullCheck(L_9);
		List_1_Add_m3338814081(L_9, L_10, /*hidden argument*/List_1_Add_m3338814081_RuntimeMethod_var);
		List_1_t257213610 * L_11 = ((OscPacket_t3204151022 *)__this)->get_data_2();
		NullCheck(L_11);
		int32_t L_12 = List_1_get_Count_m2934127733(L_11, /*hidden argument*/List_1_get_Count_m2934127733_RuntimeMethod_var);
		return ((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)1));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean OSCsharp.Data.OscMessage::get_IsBundle()
extern "C"  bool OscMessage_get_IsBundle_m139492459 (OscMessage_t3323977005 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Void OSCsharp.Data.OscMessage::.ctor(System.String)
extern "C"  void OscMessage__ctor_m1172728952 (OscMessage_t3323977005 * __this, String_t* ___address0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscMessage__ctor_m1172728952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	{
		String_t* L_0 = ___address0;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		OscPacket__ctor_m1212150461(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___address0;
		NullCheck(L_1);
		bool L_2 = String_StartsWith_m1759067526(L_1, _stringLiteral3452614529, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentException_t132251570 * L_3 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_3, _stringLiteral758506801, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		V_0 = ((int32_t)44);
		String_t* L_4 = Char_ToString_m3588025615((&V_0), /*hidden argument*/NULL);
		__this->set_typeTag_20(L_4);
		return;
	}
}
// OSCsharp.Data.OscMessage OSCsharp.Data.OscMessage::FromByteArray(System.Byte[],System.Int32&)
extern "C"  OscMessage_t3323977005 * OscMessage_FromByteArray_m1008684133 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, int32_t* ___start1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscMessage_FromByteArray_m1008684133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	OscMessage_t3323977005 * V_1 = NULL;
	CharU5BU5D_t3528271667* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Il2CppChar V_5 = 0x0;
	RuntimeObject * V_6 = NULL;
	{
		ByteU5BU5D_t4116647657* L_0 = ___data0;
		int32_t* L_1 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		String_t* L_2 = OscPacket_ValueFromByteArray_TisString_t_m2385610542(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/OscPacket_ValueFromByteArray_TisString_t_m2385610542_RuntimeMethod_var);
		V_0 = L_2;
		String_t* L_3 = V_0;
		OscMessage_t3323977005 * L_4 = (OscMessage_t3323977005 *)il2cpp_codegen_object_new(OscMessage_t3323977005_il2cpp_TypeInfo_var);
		OscMessage__ctor_m1172728952(L_4, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		ByteU5BU5D_t4116647657* L_5 = ___data0;
		int32_t* L_6 = ___start1;
		String_t* L_7 = OscPacket_ValueFromByteArray_TisString_t_m2385610542(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/OscPacket_ValueFromByteArray_TisString_t_m2385610542_RuntimeMethod_var);
		NullCheck(L_7);
		CharU5BU5D_t3528271667* L_8 = String_ToCharArray_m1492846834(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		CharU5BU5D_t3528271667* L_9 = V_2;
		NullCheck(L_9);
		V_3 = (((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length))));
		V_4 = 0;
		goto IL_0182;
	}

IL_0028:
	{
		CharU5BU5D_t3528271667* L_10 = V_2;
		int32_t L_11 = V_4;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		uint16_t L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_5 = L_13;
		Il2CppChar L_14 = V_5;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)((int32_t)98))))
		{
			case 0:
			{
				goto IL_010c;
			}
			case 1:
			{
				goto IL_0128;
			}
			case 2:
			{
				goto IL_00eb;
			}
			case 3:
			{
				goto IL_0058;
			}
			case 4:
			{
				goto IL_00d8;
			}
			case 5:
			{
				goto IL_0058;
			}
			case 6:
			{
				goto IL_00c5;
			}
			case 7:
			{
				goto IL_00b2;
			}
		}
	}

IL_0058:
	{
		Il2CppChar L_15 = V_5;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_15, (int32_t)((int32_t)70))))
		{
			case 0:
			{
				goto IL_0148;
			}
			case 1:
			{
				goto IL_0072;
			}
			case 2:
			{
				goto IL_0072;
			}
			case 3:
			{
				goto IL_015d;
			}
		}
	}

IL_0072:
	{
		Il2CppChar L_16 = V_5;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)83))))
		{
			goto IL_00fe;
		}
	}
	{
		Il2CppChar L_17 = V_5;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)84))))
		{
			goto IL_013b;
		}
	}
	{
		Il2CppChar L_18 = V_5;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)115))))
		{
			goto IL_00fe;
		}
	}
	{
		Il2CppChar L_19 = V_5;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)116))))
		{
			goto IL_011a;
		}
	}
	{
		Il2CppChar L_20 = V_5;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)44))))
		{
			goto IL_00ad;
		}
	}
	{
		Il2CppChar L_21 = V_5;
		if ((((int32_t)L_21) == ((int32_t)((int32_t)78))))
		{
			goto IL_0155;
		}
	}
	{
		goto IL_016e;
	}

IL_00ad:
	{
		goto IL_017c;
	}

IL_00b2:
	{
		ByteU5BU5D_t4116647657* L_22 = ___data0;
		int32_t* L_23 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		int32_t L_24 = OscPacket_ValueFromByteArray_TisInt32_t2950945753_m1459680167(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/OscPacket_ValueFromByteArray_TisInt32_t2950945753_m1459680167_RuntimeMethod_var);
		int32_t L_25 = L_24;
		RuntimeObject * L_26 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_25);
		V_6 = L_26;
		goto IL_0173;
	}

IL_00c5:
	{
		ByteU5BU5D_t4116647657* L_27 = ___data0;
		int32_t* L_28 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		int64_t L_29 = OscPacket_ValueFromByteArray_TisInt64_t3736567304_m4196370337(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/OscPacket_ValueFromByteArray_TisInt64_t3736567304_m4196370337_RuntimeMethod_var);
		int64_t L_30 = L_29;
		RuntimeObject * L_31 = Box(Int64_t3736567304_il2cpp_TypeInfo_var, &L_30);
		V_6 = L_31;
		goto IL_0173;
	}

IL_00d8:
	{
		ByteU5BU5D_t4116647657* L_32 = ___data0;
		int32_t* L_33 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		float L_34 = OscPacket_ValueFromByteArray_TisSingle_t1397266774_m1683906482(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/OscPacket_ValueFromByteArray_TisSingle_t1397266774_m1683906482_RuntimeMethod_var);
		float L_35 = L_34;
		RuntimeObject * L_36 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_35);
		V_6 = L_36;
		goto IL_0173;
	}

IL_00eb:
	{
		ByteU5BU5D_t4116647657* L_37 = ___data0;
		int32_t* L_38 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		double L_39 = OscPacket_ValueFromByteArray_TisDouble_t594665363_m997755250(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/OscPacket_ValueFromByteArray_TisDouble_t594665363_m997755250_RuntimeMethod_var);
		double L_40 = L_39;
		RuntimeObject * L_41 = Box(Double_t594665363_il2cpp_TypeInfo_var, &L_40);
		V_6 = L_41;
		goto IL_0173;
	}

IL_00fe:
	{
		ByteU5BU5D_t4116647657* L_42 = ___data0;
		int32_t* L_43 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		String_t* L_44 = OscPacket_ValueFromByteArray_TisString_t_m2385610542(NULL /*static, unused*/, L_42, L_43, /*hidden argument*/OscPacket_ValueFromByteArray_TisString_t_m2385610542_RuntimeMethod_var);
		V_6 = L_44;
		goto IL_0173;
	}

IL_010c:
	{
		ByteU5BU5D_t4116647657* L_45 = ___data0;
		int32_t* L_46 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4116647657* L_47 = OscPacket_ValueFromByteArray_TisByteU5BU5D_t4116647657_m2669022459(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/OscPacket_ValueFromByteArray_TisByteU5BU5D_t4116647657_m2669022459_RuntimeMethod_var);
		V_6 = (RuntimeObject *)L_47;
		goto IL_0173;
	}

IL_011a:
	{
		ByteU5BU5D_t4116647657* L_48 = ___data0;
		int32_t* L_49 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		OscTimeTag_t749151765 * L_50 = OscPacket_ValueFromByteArray_TisOscTimeTag_t749151765_m1850822955(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/OscPacket_ValueFromByteArray_TisOscTimeTag_t749151765_m1850822955_RuntimeMethod_var);
		V_6 = L_50;
		goto IL_0173;
	}

IL_0128:
	{
		ByteU5BU5D_t4116647657* L_51 = ___data0;
		int32_t* L_52 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		Il2CppChar L_53 = OscPacket_ValueFromByteArray_TisChar_t3634460470_m3452883113(NULL /*static, unused*/, L_51, L_52, /*hidden argument*/OscPacket_ValueFromByteArray_TisChar_t3634460470_m3452883113_RuntimeMethod_var);
		Il2CppChar L_54 = L_53;
		RuntimeObject * L_55 = Box(Char_t3634460470_il2cpp_TypeInfo_var, &L_54);
		V_6 = L_55;
		goto IL_0173;
	}

IL_013b:
	{
		bool L_56 = ((bool)1);
		RuntimeObject * L_57 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_56);
		V_6 = L_57;
		goto IL_0173;
	}

IL_0148:
	{
		bool L_58 = ((bool)0);
		RuntimeObject * L_59 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_58);
		V_6 = L_59;
		goto IL_0173;
	}

IL_0155:
	{
		V_6 = NULL;
		goto IL_0173;
	}

IL_015d:
	{
		float L_60 = (std::numeric_limits<float>::infinity());
		RuntimeObject * L_61 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_60);
		V_6 = L_61;
		goto IL_0173;
	}

IL_016e:
	{
		goto IL_017c;
	}

IL_0173:
	{
		OscMessage_t3323977005 * L_62 = V_1;
		RuntimeObject * L_63 = V_6;
		NullCheck(L_62);
		VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(5 /* System.Int32 OSCsharp.Data.OscPacket::Append(System.Object) */, L_62, L_63);
	}

IL_017c:
	{
		int32_t L_64 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_64, (int32_t)1));
	}

IL_0182:
	{
		int32_t L_65 = V_4;
		int32_t L_66 = V_3;
		if ((((int32_t)L_65) < ((int32_t)L_66)))
		{
			goto IL_0028;
		}
	}
	{
		OscMessage_t3323977005 * L_67 = V_1;
		return L_67;
	}
}
// System.Int32 OSCsharp.Data.OscMessage::Append(System.Object)
extern "C"  int32_t OscMessage_Append_m2539754167 (OscMessage_t3323977005 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscMessage_Append_m2539754167_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	Type_t * V_1 = NULL;
	String_t* V_2 = NULL;
	Dictionary_2_t2736202052 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B23_0 = 0;
	{
		RuntimeObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		V_0 = ((int32_t)78);
		goto IL_0181;
	}

IL_000e:
	{
		RuntimeObject * L_1 = ___value0;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m88164663(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		Type_t * L_3 = V_1;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_3);
		V_2 = L_4;
		String_t* L_5 = V_2;
		if (!L_5)
		{
			goto IL_0176;
		}
	}
	{
		Dictionary_2_t2736202052 * L_6 = ((OscMessage_t3323977005_StaticFields*)il2cpp_codegen_static_fields_for(OscMessage_t3323977005_il2cpp_TypeInfo_var))->get_U3CU3Ef__switchU24map0_21();
		if (L_6)
		{
			goto IL_00b3;
		}
	}
	{
		Dictionary_2_t2736202052 * L_7 = (Dictionary_2_t2736202052 *)il2cpp_codegen_object_new(Dictionary_2_t2736202052_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2392909825(L_7, ((int32_t)10), /*hidden argument*/Dictionary_2__ctor_m2392909825_RuntimeMethod_var);
		V_3 = L_7;
		Dictionary_2_t2736202052 * L_8 = V_3;
		NullCheck(L_8);
		Dictionary_2_Add_m282647386(L_8, _stringLiteral4176330965, 0, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_9 = V_3;
		NullCheck(L_9);
		Dictionary_2_Add_m282647386(L_9, _stringLiteral1455341775, 1, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_10 = V_3;
		NullCheck(L_10);
		Dictionary_2_Add_m282647386(L_10, _stringLiteral1234456403, 2, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_11 = V_3;
		NullCheck(L_11);
		Dictionary_2_Add_m282647386(L_11, _stringLiteral1235498031, 3, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_12 = V_3;
		NullCheck(L_12);
		Dictionary_2_Add_m282647386(L_12, _stringLiteral1236129805, 4, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_13 = V_3;
		NullCheck(L_13);
		Dictionary_2_Add_m282647386(L_13, _stringLiteral133939039, 5, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_14 = V_3;
		NullCheck(L_14);
		Dictionary_2_Add_m282647386(L_14, _stringLiteral2800413247, 6, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_15 = V_3;
		NullCheck(L_15);
		Dictionary_2_Add_m282647386(L_15, _stringLiteral3873632002, 7, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_16 = V_3;
		NullCheck(L_16);
		Dictionary_2_Add_m282647386(L_16, _stringLiteral1586259007, 8, /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_17 = V_3;
		NullCheck(L_17);
		Dictionary_2_Add_m282647386(L_17, _stringLiteral228530734, ((int32_t)9), /*hidden argument*/Dictionary_2_Add_m282647386_RuntimeMethod_var);
		Dictionary_2_t2736202052 * L_18 = V_3;
		((OscMessage_t3323977005_StaticFields*)il2cpp_codegen_static_fields_for(OscMessage_t3323977005_il2cpp_TypeInfo_var))->set_U3CU3Ef__switchU24map0_21(L_18);
	}

IL_00b3:
	{
		Dictionary_2_t2736202052 * L_19 = ((OscMessage_t3323977005_StaticFields*)il2cpp_codegen_static_fields_for(OscMessage_t3323977005_il2cpp_TypeInfo_var))->get_U3CU3Ef__switchU24map0_21();
		String_t* L_20 = V_2;
		NullCheck(L_19);
		bool L_21 = Dictionary_2_TryGetValue_m1013208020(L_19, L_20, (&V_4), /*hidden argument*/Dictionary_2_TryGetValue_m1013208020_RuntimeMethod_var);
		if (!L_21)
		{
			goto IL_0176;
		}
	}
	{
		int32_t L_22 = V_4;
		switch (L_22)
		{
			case 0:
			{
				goto IL_00fd;
			}
			case 1:
			{
				goto IL_0105;
			}
			case 2:
			{
				goto IL_010d;
			}
			case 3:
			{
				goto IL_012c;
			}
			case 4:
			{
				goto IL_0134;
			}
			case 5:
			{
				goto IL_013c;
			}
			case 6:
			{
				goto IL_0144;
			}
			case 7:
			{
				goto IL_014c;
			}
			case 8:
			{
				goto IL_0154;
			}
			case 9:
			{
				goto IL_015c;
			}
			case 10:
			{
				goto IL_0176;
			}
		}
	}
	{
		goto IL_0176;
	}

IL_00fd:
	{
		V_0 = ((int32_t)105);
		goto IL_0181;
	}

IL_0105:
	{
		V_0 = ((int32_t)104);
		goto IL_0181;
	}

IL_010d:
	{
		RuntimeObject * L_23 = ___value0;
		bool L_24 = Single_IsPositiveInfinity_m1411272350(NULL /*static, unused*/, ((*(float*)((float*)UnBox(L_23, Single_t1397266774_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0124;
		}
	}
	{
		G_B13_0 = ((int32_t)73);
		goto IL_0126;
	}

IL_0124:
	{
		G_B13_0 = ((int32_t)102);
	}

IL_0126:
	{
		V_0 = G_B13_0;
		goto IL_0181;
	}

IL_012c:
	{
		V_0 = ((int32_t)100);
		goto IL_0181;
	}

IL_0134:
	{
		V_0 = ((int32_t)115);
		goto IL_0181;
	}

IL_013c:
	{
		V_0 = ((int32_t)98);
		goto IL_0181;
	}

IL_0144:
	{
		V_0 = ((int32_t)116);
		goto IL_0181;
	}

IL_014c:
	{
		V_0 = ((int32_t)99);
		goto IL_0181;
	}

IL_0154:
	{
		V_0 = ((int32_t)114);
		goto IL_0181;
	}

IL_015c:
	{
		RuntimeObject * L_25 = ___value0;
		if (!((*(bool*)((bool*)UnBox(L_25, Boolean_t97287965_il2cpp_TypeInfo_var)))))
		{
			goto IL_016e;
		}
	}
	{
		G_B23_0 = ((int32_t)84);
		goto IL_0170;
	}

IL_016e:
	{
		G_B23_0 = ((int32_t)70);
	}

IL_0170:
	{
		V_0 = G_B23_0;
		goto IL_0181;
	}

IL_0176:
	{
		Exception_t * L_26 = (Exception_t *)il2cpp_codegen_object_new(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m1152696503(L_26, _stringLiteral2615941665, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26);
	}

IL_0181:
	{
		String_t* L_27 = __this->get_typeTag_20();
		Il2CppChar L_28 = V_0;
		Il2CppChar L_29 = L_28;
		RuntimeObject * L_30 = Box(Char_t3634460470_il2cpp_TypeInfo_var, &L_29);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m904156431(NULL /*static, unused*/, L_27, L_30, /*hidden argument*/NULL);
		__this->set_typeTag_20(L_31);
		List_1_t257213610 * L_32 = ((OscPacket_t3204151022 *)__this)->get_data_2();
		RuntimeObject * L_33 = ___value0;
		NullCheck(L_32);
		List_1_Add_m3338814081(L_32, L_33, /*hidden argument*/List_1_Add_m3338814081_RuntimeMethod_var);
		List_1_t257213610 * L_34 = ((OscPacket_t3204151022 *)__this)->get_data_2();
		NullCheck(L_34);
		int32_t L_35 = List_1_get_Count_m2934127733(L_34, /*hidden argument*/List_1_get_Count_m2934127733_RuntimeMethod_var);
		return ((int32_t)il2cpp_codegen_subtract((int32_t)L_35, (int32_t)1));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean OSCsharp.Data.OscPacket::get_LittleEndianByteOrder()
extern "C"  bool OscPacket_get_LittleEndianByteOrder_m412490820 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscPacket_get_LittleEndianByteOrder_m412490820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		bool L_0 = ((OscPacket_t3204151022_StaticFields*)il2cpp_codegen_static_fields_for(OscPacket_t3204151022_il2cpp_TypeInfo_var))->get_littleEndianByteOrder_0();
		return L_0;
	}
}
// System.String OSCsharp.Data.OscPacket::get_Address()
extern "C"  String_t* OscPacket_get_Address_m2654232420 (OscPacket_t3204151022 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_address_1();
		return L_0;
	}
}
// System.Void OSCsharp.Data.OscPacket::set_Address(System.String)
extern "C"  void OscPacket_set_Address_m2086307222 (OscPacket_t3204151022 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscPacket_set_Address_m2086307222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		___value0 = L_1;
	}

IL_000d:
	{
		String_t* L_2 = ___value0;
		__this->set_address_1(L_2);
		return;
	}
}
// System.Collections.Generic.IList`1<System.Object> OSCsharp.Data.OscPacket::get_Data()
extern "C"  RuntimeObject* OscPacket_get_Data_m3397942478 (OscPacket_t3204151022 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscPacket_get_Data_m3397942478_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t257213610 * L_0 = __this->get_data_2();
		NullCheck(L_0);
		ReadOnlyCollection_1_t4292682451 * L_1 = List_1_AsReadOnly_m1069347492(L_0, /*hidden argument*/List_1_AsReadOnly_m1069347492_RuntimeMethod_var);
		return L_1;
	}
}
// System.Void OSCsharp.Data.OscPacket::.cctor()
extern "C"  void OscPacket__cctor_m4081103440 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscPacket__cctor_m4081103440_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((OscPacket_t3204151022_StaticFields*)il2cpp_codegen_static_fields_for(OscPacket_t3204151022_il2cpp_TypeInfo_var))->set_littleEndianByteOrder_0((bool)0);
		return;
	}
}
// System.Void OSCsharp.Data.OscPacket::.ctor(System.String)
extern "C"  void OscPacket__ctor_m1212150461 (OscPacket_t3204151022 * __this, String_t* ___address0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscPacket__ctor_m1212150461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___address0;
		OscPacket_set_Address_m2086307222(__this, L_0, /*hidden argument*/NULL);
		List_1_t257213610 * L_1 = (List_1_t257213610 *)il2cpp_codegen_object_new(List_1_t257213610_il2cpp_TypeInfo_var);
		List_1__ctor_m2321703786(L_1, /*hidden argument*/List_1__ctor_m2321703786_RuntimeMethod_var);
		__this->set_data_2(L_1);
		return;
	}
}
// OSCsharp.Data.OscPacket OSCsharp.Data.OscPacket::FromByteArray(System.Byte[])
extern "C"  OscPacket_t3204151022 * OscPacket_FromByteArray_m2774387762 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscPacket_FromByteArray_m2774387762_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		ByteU5BU5D_t4116647657* L_0 = ___data0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentException_t132251570 * L_1 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3698743796(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		V_0 = 0;
		ByteU5BU5D_t4116647657* L_2 = ___data0;
		ByteU5BU5D_t4116647657* L_3 = ___data0;
		NullCheck(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		OscPacket_t3204151022 * L_4 = OscPacket_FromByteArray_m4260015432(NULL /*static, unused*/, L_2, (&V_0), (((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		return L_4;
	}
}
// OSCsharp.Data.OscPacket OSCsharp.Data.OscPacket::FromByteArray(System.Byte[],System.Int32&,System.Int32)
extern "C"  OscPacket_t3204151022 * OscPacket_FromByteArray_m4260015432 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, int32_t* ___start1, int32_t ___end2, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_t4116647657* L_0 = ___data0;
		int32_t* L_1 = ___start1;
		NullCheck(L_0);
		int32_t L_2 = (*((int32_t*)L_1));
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)35)))))
		{
			goto IL_0014;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_4 = ___data0;
		int32_t* L_5 = ___start1;
		int32_t L_6 = ___end2;
		OscBundle_t1226152694 * L_7 = OscBundle_FromByteArray_m76398600(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0014:
	{
		ByteU5BU5D_t4116647657* L_8 = ___data0;
		int32_t* L_9 = ___start1;
		OscMessage_t3323977005 * L_10 = OscMessage_FromByteArray_m1008684133(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.DateTime OSCsharp.Data.OscTimeTag::get_DateTime()
extern "C"  DateTime_t3738529785  OscTimeTag_get_DateTime_m3628459398 (OscTimeTag_t749151765 * __this, const RuntimeMethod* method)
{
	{
		DateTime_t3738529785  L_0 = __this->get_timeStamp_2();
		return L_0;
	}
}
// System.Void OSCsharp.Data.OscTimeTag::.ctor(System.DateTime)
extern "C"  void OscTimeTag__ctor_m3799858592 (OscTimeTag_t749151765 * __this, DateTime_t3738529785  ___timeStamp0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		DateTime_t3738529785  L_0 = ___timeStamp0;
		OscTimeTag_Set_m2513023378(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OSCsharp.Data.OscTimeTag::.ctor(System.Byte[])
extern "C"  void OscTimeTag__ctor_m313460281 (OscTimeTag_t749151765 * __this, ByteU5BU5D_t4116647657* ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscTimeTag__ctor_m313460281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t4116647657* V_0 = NULL;
	ByteU5BU5D_t4116647657* V_1 = NULL;
	uint32_t V_2 = 0;
	uint32_t V_3 = 0;
	DateTime_t3738529785  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_0 = ___data0;
		ByteU5BU5D_t4116647657* L_1 = Utility_CopySubArray_TisByte_t1134296376_m796220017(NULL /*static, unused*/, L_0, 0, 4, /*hidden argument*/Utility_CopySubArray_TisByte_t1134296376_m796220017_RuntimeMethod_var);
		V_0 = L_1;
		ByteU5BU5D_t4116647657* L_2 = ___data0;
		ByteU5BU5D_t4116647657* L_3 = Utility_CopySubArray_TisByte_t1134296376_m796220017(NULL /*static, unused*/, L_2, 4, 4, /*hidden argument*/Utility_CopySubArray_TisByte_t1134296376_m796220017_RuntimeMethod_var);
		V_1 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3118986983_il2cpp_TypeInfo_var);
		bool L_4 = ((BitConverter_t3118986983_StaticFields*)il2cpp_codegen_static_fields_for(BitConverter_t3118986983_il2cpp_TypeInfo_var))->get_IsLittleEndian_1();
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
		bool L_5 = OscPacket_get_LittleEndianByteOrder_m412490820(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0035;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_6 = V_0;
		ByteU5BU5D_t4116647657* L_7 = Utility_SwapEndian_m1429200246(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		ByteU5BU5D_t4116647657* L_8 = V_1;
		ByteU5BU5D_t4116647657* L_9 = Utility_SwapEndian_m1429200246(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0035:
	{
		ByteU5BU5D_t4116647657* L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3118986983_il2cpp_TypeInfo_var);
		uint32_t L_11 = BitConverter_ToUInt32_m3737646381(NULL /*static, unused*/, L_10, 0, /*hidden argument*/NULL);
		V_2 = L_11;
		ByteU5BU5D_t4116647657* L_12 = V_1;
		uint32_t L_13 = BitConverter_ToUInt32_m3737646381(NULL /*static, unused*/, L_12, 0, /*hidden argument*/NULL);
		V_3 = L_13;
		IL2CPP_RUNTIME_CLASS_INIT(OscTimeTag_t749151765_il2cpp_TypeInfo_var);
		DateTime_t3738529785  L_14 = ((OscTimeTag_t749151765_StaticFields*)il2cpp_codegen_static_fields_for(OscTimeTag_t749151765_il2cpp_TypeInfo_var))->get_Epoch_0();
		V_4 = L_14;
		uint32_t L_15 = V_2;
		DateTime_t3738529785  L_16 = DateTime_AddSeconds_m332574389((&V_4), (((double)((double)(((double)((uint32_t)L_15)))))), /*hidden argument*/NULL);
		V_4 = L_16;
		uint32_t L_17 = V_3;
		DateTime_t3738529785  L_18 = DateTime_AddMilliseconds_m3713972790((&V_4), (((double)((double)(((double)((uint32_t)L_17)))))), /*hidden argument*/NULL);
		__this->set_timeStamp_2(L_18);
		return;
	}
}
// System.Boolean OSCsharp.Data.OscTimeTag::op_Equality(OSCsharp.Data.OscTimeTag,OSCsharp.Data.OscTimeTag)
extern "C"  bool OscTimeTag_op_Equality_m793844905 (RuntimeObject * __this /* static, unused */, OscTimeTag_t749151765 * ___lhs0, OscTimeTag_t749151765 * ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscTimeTag_op_Equality_m793844905_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OscTimeTag_t749151765 * L_0 = ___lhs0;
		OscTimeTag_t749151765 * L_1 = ___rhs1;
		bool L_2 = Object_ReferenceEquals_m610702577(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)1;
	}

IL_000e:
	{
		OscTimeTag_t749151765 * L_3 = ___lhs0;
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		OscTimeTag_t749151765 * L_4 = ___rhs1;
		if (L_4)
		{
			goto IL_001c;
		}
	}

IL_001a:
	{
		return (bool)0;
	}

IL_001c:
	{
		OscTimeTag_t749151765 * L_5 = ___lhs0;
		NullCheck(L_5);
		DateTime_t3738529785  L_6 = OscTimeTag_get_DateTime_m3628459398(L_5, /*hidden argument*/NULL);
		OscTimeTag_t749151765 * L_7 = ___rhs1;
		NullCheck(L_7);
		DateTime_t3738529785  L_8 = OscTimeTag_get_DateTime_m3628459398(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t3738529785_il2cpp_TypeInfo_var);
		bool L_9 = DateTime_op_Equality_m1022058599(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Boolean OSCsharp.Data.OscTimeTag::op_LessThan(OSCsharp.Data.OscTimeTag,OSCsharp.Data.OscTimeTag)
extern "C"  bool OscTimeTag_op_LessThan_m2293409653 (RuntimeObject * __this /* static, unused */, OscTimeTag_t749151765 * ___lhs0, OscTimeTag_t749151765 * ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscTimeTag_op_LessThan_m2293409653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OscTimeTag_t749151765 * L_0 = ___lhs0;
		NullCheck(L_0);
		DateTime_t3738529785  L_1 = OscTimeTag_get_DateTime_m3628459398(L_0, /*hidden argument*/NULL);
		OscTimeTag_t749151765 * L_2 = ___rhs1;
		NullCheck(L_2);
		DateTime_t3738529785  L_3 = OscTimeTag_get_DateTime_m3628459398(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t3738529785_il2cpp_TypeInfo_var);
		bool L_4 = DateTime_op_LessThan_m2497205152(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean OSCsharp.Data.OscTimeTag::IsValidTime(System.DateTime)
extern "C"  bool OscTimeTag_IsValidTime_m2642829427 (RuntimeObject * __this /* static, unused */, DateTime_t3738529785  ___timeStamp0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscTimeTag_IsValidTime_m2642829427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DateTime_t3738529785  L_0 = ___timeStamp0;
		IL2CPP_RUNTIME_CLASS_INIT(OscTimeTag_t749151765_il2cpp_TypeInfo_var);
		DateTime_t3738529785  L_1 = ((OscTimeTag_t749151765_StaticFields*)il2cpp_codegen_static_fields_for(OscTimeTag_t749151765_il2cpp_TypeInfo_var))->get_Epoch_0();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t881159249_il2cpp_TypeInfo_var);
		TimeSpan_t881159249  L_2 = TimeSpan_FromMilliseconds_m579366253(NULL /*static, unused*/, (1.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t3738529785_il2cpp_TypeInfo_var);
		DateTime_t3738529785  L_3 = DateTime_op_Addition_m1857121695(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		bool L_4 = DateTime_op_GreaterThanOrEqual_m674703316(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void OSCsharp.Data.OscTimeTag::Set(System.DateTime)
extern "C"  void OscTimeTag_Set_m2513023378 (OscTimeTag_t749151765 * __this, DateTime_t3738529785  ___timeStamp0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscTimeTag_Set_m2513023378_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int64_t L_0 = DateTime_get_Ticks_m1550640881((&___timeStamp0), /*hidden argument*/NULL);
		int64_t L_1 = DateTime_get_Ticks_m1550640881((&___timeStamp0), /*hidden argument*/NULL);
		int32_t L_2 = DateTime_get_Kind_m2154871796((&___timeStamp0), /*hidden argument*/NULL);
		DateTime__ctor_m1095105629((&___timeStamp0), ((int64_t)il2cpp_codegen_subtract((int64_t)L_0, (int64_t)((int64_t)((int64_t)L_1%(int64_t)(((int64_t)((int64_t)((int32_t)10000)))))))), L_2, /*hidden argument*/NULL);
		DateTime_t3738529785  L_3 = ___timeStamp0;
		IL2CPP_RUNTIME_CLASS_INIT(OscTimeTag_t749151765_il2cpp_TypeInfo_var);
		bool L_4 = OscTimeTag_IsValidTime_m2642829427(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		ArgumentException_t132251570 * L_5 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_5, _stringLiteral3480154658, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_003a:
	{
		DateTime_t3738529785  L_6 = ___timeStamp0;
		__this->set_timeStamp_2(L_6);
		return;
	}
}
// System.Boolean OSCsharp.Data.OscTimeTag::Equals(System.Object)
extern "C"  bool OscTimeTag_Equals_m2737513310 (OscTimeTag_t749151765 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscTimeTag_Equals_m2737513310_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OscTimeTag_t749151765 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		RuntimeObject * L_1 = ___value0;
		V_0 = ((OscTimeTag_t749151765 *)IsInstClass((RuntimeObject*)L_1, OscTimeTag_t749151765_il2cpp_TypeInfo_var));
		OscTimeTag_t749151765 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(OscTimeTag_t749151765_il2cpp_TypeInfo_var);
		bool L_3 = OscTimeTag_op_Equality_m793844905(NULL /*static, unused*/, L_2, (OscTimeTag_t749151765 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		DateTime_t3738529785 * L_4 = __this->get_address_of_timeStamp_2();
		OscTimeTag_t749151765 * L_5 = V_0;
		NullCheck(L_5);
		DateTime_t3738529785  L_6 = L_5->get_timeStamp_2();
		bool L_7 = DateTime_Equals_m4001498422(L_4, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 OSCsharp.Data.OscTimeTag::GetHashCode()
extern "C"  int32_t OscTimeTag_GetHashCode_m348511320 (OscTimeTag_t749151765 * __this, const RuntimeMethod* method)
{
	{
		DateTime_t3738529785 * L_0 = __this->get_address_of_timeStamp_2();
		int32_t L_1 = DateTime_GetHashCode_m2261847002(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String OSCsharp.Data.OscTimeTag::ToString()
extern "C"  String_t* OscTimeTag_ToString_m2888347313 (OscTimeTag_t749151765 * __this, const RuntimeMethod* method)
{
	{
		DateTime_t3738529785 * L_0 = __this->get_address_of_timeStamp_2();
		String_t* L_1 = DateTime_ToString_m884486936(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void OSCsharp.Data.OscTimeTag::.cctor()
extern "C"  void OscTimeTag__cctor_m317014173 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscTimeTag__cctor_m317014173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DateTime_t3738529785  L_0;
		memset(&L_0, 0, sizeof(L_0));
		DateTime__ctor_m2030998145((&L_0), ((int32_t)1900), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		((OscTimeTag_t749151765_StaticFields*)il2cpp_codegen_static_fields_for(OscTimeTag_t749151765_il2cpp_TypeInfo_var))->set_Epoch_0(L_0);
		DateTime_t3738529785  L_1 = ((OscTimeTag_t749151765_StaticFields*)il2cpp_codegen_static_fields_for(OscTimeTag_t749151765_il2cpp_TypeInfo_var))->get_Epoch_0();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t881159249_il2cpp_TypeInfo_var);
		TimeSpan_t881159249  L_2 = TimeSpan_FromMilliseconds_m579366253(NULL /*static, unused*/, (1.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t3738529785_il2cpp_TypeInfo_var);
		DateTime_t3738529785  L_3 = DateTime_op_Addition_m1857121695(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		OscTimeTag_t749151765 * L_4 = (OscTimeTag_t749151765 *)il2cpp_codegen_object_new(OscTimeTag_t749151765_il2cpp_TypeInfo_var);
		OscTimeTag__ctor_m3799858592(L_4, L_3, /*hidden argument*/NULL);
		((OscTimeTag_t749151765_StaticFields*)il2cpp_codegen_static_fields_for(OscTimeTag_t749151765_il2cpp_TypeInfo_var))->set_MinValue_1(L_4);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OSCsharp.Net.OscBundleReceivedEventArgs::set_Bundle(OSCsharp.Data.OscBundle)
extern "C"  void OscBundleReceivedEventArgs_set_Bundle_m2295498377 (OscBundleReceivedEventArgs_t574491439 * __this, OscBundle_t1226152694 * ___value0, const RuntimeMethod* method)
{
	{
		OscBundle_t1226152694 * L_0 = ___value0;
		__this->set_U3CBundleU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void OSCsharp.Net.OscBundleReceivedEventArgs::.ctor(OSCsharp.Data.OscBundle)
extern "C"  void OscBundleReceivedEventArgs__ctor_m3816053266 (OscBundleReceivedEventArgs_t574491439 * __this, OscBundle_t1226152694 * ___bundle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscBundleReceivedEventArgs__ctor_m3816053266_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs__ctor_m32674013(__this, /*hidden argument*/NULL);
		OscBundle_t1226152694 * L_0 = ___bundle0;
		OscBundleReceivedEventArgs_set_Bundle_m2295498377(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// OSCsharp.Data.OscMessage OSCsharp.Net.OscMessageReceivedEventArgs::get_Message()
extern "C"  OscMessage_t3323977005 * OscMessageReceivedEventArgs_get_Message_m2617929226 (OscMessageReceivedEventArgs_t2390219542 * __this, const RuntimeMethod* method)
{
	{
		OscMessage_t3323977005 * L_0 = __this->get_U3CMessageU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void OSCsharp.Net.OscMessageReceivedEventArgs::set_Message(OSCsharp.Data.OscMessage)
extern "C"  void OscMessageReceivedEventArgs_set_Message_m517697176 (OscMessageReceivedEventArgs_t2390219542 * __this, OscMessage_t3323977005 * ___value0, const RuntimeMethod* method)
{
	{
		OscMessage_t3323977005 * L_0 = ___value0;
		__this->set_U3CMessageU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void OSCsharp.Net.OscMessageReceivedEventArgs::.ctor(OSCsharp.Data.OscMessage)
extern "C"  void OscMessageReceivedEventArgs__ctor_m3694517805 (OscMessageReceivedEventArgs_t2390219542 * __this, OscMessage_t3323977005 * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscMessageReceivedEventArgs__ctor_m3694517805_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs__ctor_m32674013(__this, /*hidden argument*/NULL);
		OscMessage_t3323977005 * L_0 = ___message0;
		OscMessageReceivedEventArgs_set_Message_m517697176(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OSCsharp.Net.OscPacketReceivedEventArgs::set_Packet(OSCsharp.Data.OscPacket)
extern "C"  void OscPacketReceivedEventArgs_set_Packet_m3963054541 (OscPacketReceivedEventArgs_t663085507 * __this, OscPacket_t3204151022 * ___value0, const RuntimeMethod* method)
{
	{
		OscPacket_t3204151022 * L_0 = ___value0;
		__this->set_U3CPacketU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void OSCsharp.Net.OscPacketReceivedEventArgs::.ctor(OSCsharp.Data.OscPacket)
extern "C"  void OscPacketReceivedEventArgs__ctor_m3956995085 (OscPacketReceivedEventArgs_t663085507 * __this, OscPacket_t3204151022 * ___packet0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscPacketReceivedEventArgs__ctor_m3956995085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs__ctor_m32674013(__this, /*hidden argument*/NULL);
		OscPacket_t3204151022 * L_0 = ___packet0;
		OscPacketReceivedEventArgs_set_Packet_m3963054541(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OSCsharp.Net.UDPReceiver::add_ErrorOccured(System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>)
extern "C"  void UDPReceiver_add_ErrorOccured_m2838983283 (UDPReceiver_t1881969775 * __this, EventHandler_1_t319478644 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_add_ErrorOccured_m2838983283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t319478644 * V_0 = NULL;
	EventHandler_1_t319478644 * V_1 = NULL;
	{
		EventHandler_1_t319478644 * L_0 = __this->get_ErrorOccured_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t319478644 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t319478644 ** L_2 = __this->get_address_of_ErrorOccured_2();
		EventHandler_1_t319478644 * L_3 = V_1;
		EventHandler_1_t319478644 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t319478644 * L_6 = V_0;
		EventHandler_1_t319478644 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t319478644 *>(L_2, ((EventHandler_1_t319478644 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t319478644_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t319478644 * L_8 = V_0;
		EventHandler_1_t319478644 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t319478644 *)L_8) == ((RuntimeObject*)(EventHandler_1_t319478644 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::remove_ErrorOccured(System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>)
extern "C"  void UDPReceiver_remove_ErrorOccured_m4285225404 (UDPReceiver_t1881969775 * __this, EventHandler_1_t319478644 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_remove_ErrorOccured_m4285225404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t319478644 * V_0 = NULL;
	EventHandler_1_t319478644 * V_1 = NULL;
	{
		EventHandler_1_t319478644 * L_0 = __this->get_ErrorOccured_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t319478644 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t319478644 ** L_2 = __this->get_address_of_ErrorOccured_2();
		EventHandler_1_t319478644 * L_3 = V_1;
		EventHandler_1_t319478644 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_1_t319478644 * L_6 = V_0;
		EventHandler_1_t319478644 * L_7 = InterlockedCompareExchangeImpl<EventHandler_1_t319478644 *>(L_2, ((EventHandler_1_t319478644 *)CastclassSealed((RuntimeObject*)L_5, EventHandler_1_t319478644_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_1_t319478644 * L_8 = V_0;
		EventHandler_1_t319478644 * L_9 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t319478644 *)L_8) == ((RuntimeObject*)(EventHandler_1_t319478644 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::add_MessageReceived(System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>)
extern "C"  void UDPReceiver_add_MessageReceived_m2005693360 (UDPReceiver_t1881969775 * __this, EventHandler_1_t314378975 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_add_MessageReceived_m2005693360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EventHandler_1_t314378975 * L_0 = __this->get_messageReceivedInvoker_3();
		EventHandler_1_t314378975 * L_1 = ___value0;
		Delegate_t1188392813 * L_2 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_messageReceivedInvoker_3(((EventHandler_1_t314378975 *)CastclassSealed((RuntimeObject*)L_2, EventHandler_1_t314378975_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::remove_MessageReceived(System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>)
extern "C"  void UDPReceiver_remove_MessageReceived_m3207071580 (UDPReceiver_t1881969775 * __this, EventHandler_1_t314378975 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_remove_MessageReceived_m3207071580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EventHandler_1_t314378975 * L_0 = __this->get_messageReceivedInvoker_3();
		EventHandler_1_t314378975 * L_1 = ___value0;
		Delegate_t1188392813 * L_2 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_messageReceivedInvoker_3(((EventHandler_1_t314378975 *)CastclassSealed((RuntimeObject*)L_2, EventHandler_1_t314378975_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Net.IPAddress OSCsharp.Net.UDPReceiver::get_IPAddress()
extern "C"  IPAddress_t241777590 * UDPReceiver_get_IPAddress_m4104541328 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method)
{
	{
		IPAddress_t241777590 * L_0 = __this->get_U3CIPAddressU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::set_IPAddress(System.Net.IPAddress)
extern "C"  void UDPReceiver_set_IPAddress_m1713128732 (UDPReceiver_t1881969775 * __this, IPAddress_t241777590 * ___value0, const RuntimeMethod* method)
{
	{
		IPAddress_t241777590 * L_0 = ___value0;
		__this->set_U3CIPAddressU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Int32 OSCsharp.Net.UDPReceiver::get_Port()
extern "C"  int32_t UDPReceiver_get_Port_m21013429 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CPortU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::set_Port(System.Int32)
extern "C"  void UDPReceiver_set_Port_m2702541475 (UDPReceiver_t1881969775 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CPortU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Net.IPAddress OSCsharp.Net.UDPReceiver::get_MulticastAddress()
extern "C"  IPAddress_t241777590 * UDPReceiver_get_MulticastAddress_m1408152394 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method)
{
	{
		IPAddress_t241777590 * L_0 = __this->get_U3CMulticastAddressU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::set_MulticastAddress(System.Net.IPAddress)
extern "C"  void UDPReceiver_set_MulticastAddress_m1606247330 (UDPReceiver_t1881969775 * __this, IPAddress_t241777590 * ___value0, const RuntimeMethod* method)
{
	{
		IPAddress_t241777590 * L_0 = ___value0;
		__this->set_U3CMulticastAddressU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver::get_IPEndPoint()
extern "C"  IPEndPoint_t3791887218 * UDPReceiver_get_IPEndPoint_m715339074 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method)
{
	{
		IPEndPoint_t3791887218 * L_0 = __this->get_U3CIPEndPointU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::set_IPEndPoint(System.Net.IPEndPoint)
extern "C"  void UDPReceiver_set_IPEndPoint_m3483499756 (UDPReceiver_t1881969775 * __this, IPEndPoint_t3791887218 * ___value0, const RuntimeMethod* method)
{
	{
		IPEndPoint_t3791887218 * L_0 = ___value0;
		__this->set_U3CIPEndPointU3Ek__BackingField_7(L_0);
		return;
	}
}
// OSCsharp.Net.TransmissionType OSCsharp.Net.UDPReceiver::get_TransmissionType()
extern "C"  int32_t UDPReceiver_get_TransmissionType_m1649715736 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CTransmissionTypeU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::set_TransmissionType(OSCsharp.Net.TransmissionType)
extern "C"  void UDPReceiver_set_TransmissionType_m2455005189 (UDPReceiver_t1881969775 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CTransmissionTypeU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Boolean OSCsharp.Net.UDPReceiver::get_ConsumeParsingExceptions()
extern "C"  bool UDPReceiver_get_ConsumeParsingExceptions_m2693528941 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CConsumeParsingExceptionsU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::set_ConsumeParsingExceptions(System.Boolean)
extern "C"  void UDPReceiver_set_ConsumeParsingExceptions_m2769974665 (UDPReceiver_t1881969775 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CConsumeParsingExceptionsU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Boolean OSCsharp.Net.UDPReceiver::get_IsRunning()
extern "C"  bool UDPReceiver_get_IsRunning_m1711451 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_acceptingConnections_11();
		il2cpp_codegen_memory_barrier();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::.ctor(System.Int32,System.Boolean)
extern "C"  void UDPReceiver__ctor_m588051875 (UDPReceiver_t1881969775 * __this, int32_t ___port0, bool ___consumeParsingExceptions1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver__ctor_m588051875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IPAddress_t241777590_il2cpp_TypeInfo_var);
		IPAddress_t241777590 * L_0 = ((IPAddress_t241777590_StaticFields*)il2cpp_codegen_static_fields_for(IPAddress_t241777590_il2cpp_TypeInfo_var))->get_Any_4();
		int32_t L_1 = ___port0;
		bool L_2 = ___consumeParsingExceptions1;
		UDPReceiver__ctor_m4189065602(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::.ctor(System.Net.IPAddress,System.Int32,System.Boolean)
extern "C"  void UDPReceiver__ctor_m4189065602 (UDPReceiver_t1881969775 * __this, IPAddress_t241777590 * ___ipAddress0, int32_t ___port1, bool ___consumeParsingExceptions2, const RuntimeMethod* method)
{
	{
		IPAddress_t241777590 * L_0 = ___ipAddress0;
		int32_t L_1 = ___port1;
		bool L_2 = ___consumeParsingExceptions2;
		UDPReceiver__ctor_m2102639691(__this, L_0, L_1, 0, (IPAddress_t241777590 *)NULL, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::.ctor(System.Net.IPAddress,System.Int32,OSCsharp.Net.TransmissionType,System.Net.IPAddress,System.Boolean)
extern "C"  void UDPReceiver__ctor_m2102639691 (UDPReceiver_t1881969775 * __this, IPAddress_t241777590 * ___ipAddress0, int32_t ___port1, int32_t ___transmissionType2, IPAddress_t241777590 * ___multicastAddress3, bool ___consumeParsingExceptions4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver__ctor_m2102639691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		IPAddress_t241777590 * L_0 = ___ipAddress0;
		UDPReceiver_set_IPAddress_m1713128732(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___port1;
		UDPReceiver_set_Port_m2702541475(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___transmissionType2;
		UDPReceiver_set_TransmissionType_m2455005189(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = UDPReceiver_get_TransmissionType_m1649715736(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0041;
		}
	}
	{
		IPAddress_t241777590 * L_4 = ___multicastAddress3;
		if (L_4)
		{
			goto IL_0039;
		}
	}
	{
		ArgumentException_t132251570 * L_5 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_5, _stringLiteral2077443138, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0039:
	{
		IPAddress_t241777590 * L_6 = ___multicastAddress3;
		UDPReceiver_set_MulticastAddress_m1606247330(__this, L_6, /*hidden argument*/NULL);
	}

IL_0041:
	{
		bool L_7 = ___consumeParsingExceptions4;
		UDPReceiver_set_ConsumeParsingExceptions_m2769974665(__this, L_7, /*hidden argument*/NULL);
		intptr_t L_8 = (intptr_t)UDPReceiver_endReceive_m703031579_RuntimeMethod_var;
		AsyncCallback_t3962456242 * L_9 = (AsyncCallback_t3962456242 *)il2cpp_codegen_object_new(AsyncCallback_t3962456242_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m530647953(L_9, __this, L_8, /*hidden argument*/NULL);
		__this->set_callback_12(L_9);
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::Start()
extern "C"  void UDPReceiver_Start_m2568122436 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_Start_m2568122436_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Socket_t1119025450 * V_1 = NULL;
	UdpState_t629681666 * V_2 = NULL;
	{
		int32_t L_0 = UDPReceiver_get_TransmissionType_m1649715736(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_004f;
			}
			case 2:
			{
				goto IL_00b5;
			}
			case 3:
			{
				goto IL_00b5;
			}
		}
	}
	{
		goto IL_00e1;
	}

IL_0022:
	{
		IPAddress_t241777590 * L_2 = UDPReceiver_get_IPAddress_m4104541328(__this, /*hidden argument*/NULL);
		int32_t L_3 = UDPReceiver_get_Port_m21013429(__this, /*hidden argument*/NULL);
		IPEndPoint_t3791887218 * L_4 = (IPEndPoint_t3791887218 *)il2cpp_codegen_object_new(IPEndPoint_t3791887218_il2cpp_TypeInfo_var);
		IPEndPoint__ctor_m2833647099(L_4, L_2, L_3, /*hidden argument*/NULL);
		UDPReceiver_set_IPEndPoint_m3483499756(__this, L_4, /*hidden argument*/NULL);
		IPEndPoint_t3791887218 * L_5 = UDPReceiver_get_IPEndPoint_m715339074(__this, /*hidden argument*/NULL);
		UdpClient_t967282006 * L_6 = (UdpClient_t967282006 *)il2cpp_codegen_object_new(UdpClient_t967282006_il2cpp_TypeInfo_var);
		UdpClient__ctor_m536015148(L_6, L_5, /*hidden argument*/NULL);
		__this->set_udpClient_10(L_6);
		goto IL_00e7;
	}

IL_004f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IPAddress_t241777590_il2cpp_TypeInfo_var);
		IPAddress_t241777590 * L_7 = ((IPAddress_t241777590_StaticFields*)il2cpp_codegen_static_fields_for(IPAddress_t241777590_il2cpp_TypeInfo_var))->get_Any_4();
		int32_t L_8 = UDPReceiver_get_Port_m21013429(__this, /*hidden argument*/NULL);
		IPEndPoint_t3791887218 * L_9 = (IPEndPoint_t3791887218 *)il2cpp_codegen_object_new(IPEndPoint_t3791887218_il2cpp_TypeInfo_var);
		IPEndPoint__ctor_m2833647099(L_9, L_7, L_8, /*hidden argument*/NULL);
		UDPReceiver_set_IPEndPoint_m3483499756(__this, L_9, /*hidden argument*/NULL);
		Socket_t1119025450 * L_10 = (Socket_t1119025450 *)il2cpp_codegen_object_new(Socket_t1119025450_il2cpp_TypeInfo_var);
		Socket__ctor_m3479084642(L_10, 2, 2, ((int32_t)17), /*hidden argument*/NULL);
		V_1 = L_10;
		Socket_t1119025450 * L_11 = V_1;
		NullCheck(L_11);
		Socket_SetSocketOption_m483522974(L_11, ((int32_t)65535), 4, 1, /*hidden argument*/NULL);
		Socket_t1119025450 * L_12 = V_1;
		IPEndPoint_t3791887218 * L_13 = UDPReceiver_get_IPEndPoint_m715339074(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Socket_Bind_m1387808352(L_12, L_13, /*hidden argument*/NULL);
		UdpClient_t967282006 * L_14 = (UdpClient_t967282006 *)il2cpp_codegen_object_new(UdpClient_t967282006_il2cpp_TypeInfo_var);
		UdpClient__ctor_m4070023699(L_14, /*hidden argument*/NULL);
		__this->set_udpClient_10(L_14);
		UdpClient_t967282006 * L_15 = __this->get_udpClient_10();
		Socket_t1119025450 * L_16 = V_1;
		NullCheck(L_15);
		UdpClient_set_Client_m1161482078(L_15, L_16, /*hidden argument*/NULL);
		UdpClient_t967282006 * L_17 = __this->get_udpClient_10();
		IPAddress_t241777590 * L_18 = UDPReceiver_get_MulticastAddress_m1408152394(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		UdpClient_JoinMulticastGroup_m2638505107(L_17, L_18, /*hidden argument*/NULL);
		goto IL_00e7;
	}

IL_00b5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IPAddress_t241777590_il2cpp_TypeInfo_var);
		IPAddress_t241777590 * L_19 = ((IPAddress_t241777590_StaticFields*)il2cpp_codegen_static_fields_for(IPAddress_t241777590_il2cpp_TypeInfo_var))->get_Any_4();
		int32_t L_20 = UDPReceiver_get_Port_m21013429(__this, /*hidden argument*/NULL);
		IPEndPoint_t3791887218 * L_21 = (IPEndPoint_t3791887218 *)il2cpp_codegen_object_new(IPEndPoint_t3791887218_il2cpp_TypeInfo_var);
		IPEndPoint__ctor_m2833647099(L_21, L_19, L_20, /*hidden argument*/NULL);
		UDPReceiver_set_IPEndPoint_m3483499756(__this, L_21, /*hidden argument*/NULL);
		IPEndPoint_t3791887218 * L_22 = UDPReceiver_get_IPEndPoint_m715339074(__this, /*hidden argument*/NULL);
		UdpClient_t967282006 * L_23 = (UdpClient_t967282006 *)il2cpp_codegen_object_new(UdpClient_t967282006_il2cpp_TypeInfo_var);
		UdpClient__ctor_m536015148(L_23, L_22, /*hidden argument*/NULL);
		__this->set_udpClient_10(L_23);
		goto IL_00e7;
	}

IL_00e1:
	{
		Exception_t * L_24 = (Exception_t *)il2cpp_codegen_object_new(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m213470898(L_24, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}

IL_00e7:
	{
		UdpClient_t967282006 * L_25 = __this->get_udpClient_10();
		IPEndPoint_t3791887218 * L_26 = UDPReceiver_get_IPEndPoint_m715339074(__this, /*hidden argument*/NULL);
		UdpState_t629681666 * L_27 = (UdpState_t629681666 *)il2cpp_codegen_object_new(UdpState_t629681666_il2cpp_TypeInfo_var);
		UdpState__ctor_m2410451527(L_27, L_25, L_26, /*hidden argument*/NULL);
		V_2 = L_27;
		il2cpp_codegen_memory_barrier();
		__this->set_acceptingConnections_11(1);
		UdpClient_t967282006 * L_28 = __this->get_udpClient_10();
		AsyncCallback_t3962456242 * L_29 = __this->get_callback_12();
		UdpState_t629681666 * L_30 = V_2;
		NullCheck(L_28);
		UdpClient_BeginReceive_m3486162304(L_28, L_29, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::Stop()
extern "C"  void UDPReceiver_Stop_m2310297977 (UDPReceiver_t1881969775 * __this, const RuntimeMethod* method)
{
	{
		il2cpp_codegen_memory_barrier();
		__this->set_acceptingConnections_11(0);
		UdpClient_t967282006 * L_0 = __this->get_udpClient_10();
		if (!L_0)
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_1 = UDPReceiver_get_TransmissionType_m1649715736(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0031;
		}
	}
	{
		UdpClient_t967282006 * L_2 = __this->get_udpClient_10();
		IPAddress_t241777590 * L_3 = UDPReceiver_get_MulticastAddress_m1408152394(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		UdpClient_DropMulticastGroup_m3633561453(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0031:
	{
		UdpClient_t967282006 * L_4 = __this->get_udpClient_10();
		NullCheck(L_4);
		UdpClient_Close_m4217442468(L_4, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::endReceive(System.IAsyncResult)
extern "C"  void UDPReceiver_endReceive_m703031579 (UDPReceiver_t1881969775 * __this, RuntimeObject* ___asyncResult0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_endReceive_m703031579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UdpState_t629681666 * V_0 = NULL;
	UdpClient_t967282006 * V_1 = NULL;
	IPEndPoint_t3791887218 * V_2 = NULL;
	ByteU5BU5D_t4116647657* V_3 = NULL;
	int32_t V_4 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject* L_0 = ___asyncResult0;
			NullCheck(L_0);
			RuntimeObject * L_1 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.IAsyncResult::get_AsyncState() */, IAsyncResult_t767004451_il2cpp_TypeInfo_var, L_0);
			V_0 = ((UdpState_t629681666 *)CastclassClass((RuntimeObject*)L_1, UdpState_t629681666_il2cpp_TypeInfo_var));
			UdpState_t629681666 * L_2 = V_0;
			NullCheck(L_2);
			UdpClient_t967282006 * L_3 = UdpState_get_Client_m2855211709(L_2, /*hidden argument*/NULL);
			V_1 = L_3;
			UdpState_t629681666 * L_4 = V_0;
			NullCheck(L_4);
			IPEndPoint_t3791887218 * L_5 = UdpState_get_IPEndPoint_m3884943819(L_4, /*hidden argument*/NULL);
			V_2 = L_5;
			UdpClient_t967282006 * L_6 = V_1;
			RuntimeObject* L_7 = ___asyncResult0;
			NullCheck(L_6);
			ByteU5BU5D_t4116647657* L_8 = UdpClient_EndReceive_m765429616(L_6, L_7, (&V_2), /*hidden argument*/NULL);
			V_3 = L_8;
			ByteU5BU5D_t4116647657* L_9 = V_3;
			if (!L_9)
			{
				goto IL_003b;
			}
		}

IL_002a:
		{
			ByteU5BU5D_t4116647657* L_10 = V_3;
			NullCheck(L_10);
			if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length))))) <= ((int32_t)0)))
			{
				goto IL_003b;
			}
		}

IL_0033:
		{
			IPEndPoint_t3791887218 * L_11 = V_2;
			ByteU5BU5D_t4116647657* L_12 = V_3;
			UDPReceiver_parseData_m1225544979(__this, L_11, L_12, /*hidden argument*/NULL);
		}

IL_003b:
		{
			bool L_13 = __this->get_acceptingConnections_11();
			il2cpp_codegen_memory_barrier();
			if (!L_13)
			{
				goto IL_0056;
			}
		}

IL_0048:
		{
			UdpClient_t967282006 * L_14 = V_1;
			AsyncCallback_t3962456242 * L_15 = __this->get_callback_12();
			UdpState_t629681666 * L_16 = V_0;
			NullCheck(L_14);
			UdpClient_BeginReceive_m3486162304(L_14, L_15, L_16, /*hidden argument*/NULL);
		}

IL_0056:
		{
			goto IL_0064;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ObjectDisposedException_t21392786_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_005b;
		throw e;
	}

CATCH_005b:
	{ // begin catch(System.ObjectDisposedException)
		V_4 = 2;
		goto IL_0064;
	} // end catch (depth: 1)

IL_0064:
	{
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::parseData(System.Net.IPEndPoint,System.Byte[])
extern "C"  void UDPReceiver_parseData_m1225544979 (UDPReceiver_t1881969775 * __this, IPEndPoint_t3791887218 * ___sourceEndPoint0, ByteU5BU5D_t4116647657* ___data1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_parseData_m1225544979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OscPacket_t3204151022 * V_0 = NULL;
	Exception_t * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			ByteU5BU5D_t4116647657* L_0 = ___data1;
			IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t3204151022_il2cpp_TypeInfo_var);
			OscPacket_t3204151022 * L_1 = OscPacket_FromByteArray_m2774387762(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			OscPacket_t3204151022 * L_2 = V_0;
			UDPReceiver_onPacketReceived_m1435591783(__this, L_2, /*hidden argument*/NULL);
			OscPacket_t3204151022 * L_3 = V_0;
			NullCheck(L_3);
			bool L_4 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean OSCsharp.Data.OscPacket::get_IsBundle() */, L_3);
			if (!L_4)
			{
				goto IL_002a;
			}
		}

IL_0019:
		{
			OscPacket_t3204151022 * L_5 = V_0;
			UDPReceiver_onBundleReceived_m1364519174(__this, ((OscBundle_t1226152694 *)IsInstSealed((RuntimeObject*)L_5, OscBundle_t1226152694_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			goto IL_0036;
		}

IL_002a:
		{
			OscPacket_t3204151022 * L_6 = V_0;
			UDPReceiver_onMessageReceived_m1127692520(__this, ((OscMessage_t3323977005 *)IsInstClass((RuntimeObject*)L_6, OscMessage_t3323977005_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		}

IL_0036:
		{
			goto IL_0053;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_003b;
		throw e;
	}

CATCH_003b:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t *)__exception_local);
			bool L_7 = UDPReceiver_get_ConsumeParsingExceptions_m2693528941(__this, /*hidden argument*/NULL);
			if (L_7)
			{
				goto IL_004e;
			}
		}

IL_0047:
		{
			Exception_t * L_8 = V_1;
			UDPReceiver_onError_m4129978076(__this, L_8, /*hidden argument*/NULL);
		}

IL_004e:
		{
			goto IL_0053;
		}
	} // end catch (depth: 1)

IL_0053:
	{
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::onPacketReceived(OSCsharp.Data.OscPacket)
extern "C"  void UDPReceiver_onPacketReceived_m1435591783 (UDPReceiver_t1881969775 * __this, OscPacket_t3204151022 * ___packet0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_onPacketReceived_m1435591783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EventHandler_1_t2882212236 * L_0 = __this->get_PacketReceived_0();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		EventHandler_1_t2882212236 * L_1 = __this->get_PacketReceived_0();
		OscPacket_t3204151022 * L_2 = ___packet0;
		OscPacketReceivedEventArgs_t663085507 * L_3 = (OscPacketReceivedEventArgs_t663085507 *)il2cpp_codegen_object_new(OscPacketReceivedEventArgs_t663085507_il2cpp_TypeInfo_var);
		OscPacketReceivedEventArgs__ctor_m3956995085(L_3, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		EventHandler_1_Invoke_m866284127(L_1, __this, L_3, /*hidden argument*/EventHandler_1_Invoke_m866284127_RuntimeMethod_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::onBundleReceived(OSCsharp.Data.OscBundle)
extern "C"  void UDPReceiver_onBundleReceived_m1364519174 (UDPReceiver_t1881969775 * __this, OscBundle_t1226152694 * ___bundle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_onBundleReceived_m1364519174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RuntimeObject * V_2 = NULL;
	OscMessage_t3323977005 * V_3 = NULL;
	{
		EventHandler_1_t2793618168 * L_0 = __this->get_BundleReceived_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		EventHandler_1_t2793618168 * L_1 = __this->get_BundleReceived_1();
		OscBundle_t1226152694 * L_2 = ___bundle0;
		OscBundleReceivedEventArgs_t574491439 * L_3 = (OscBundleReceivedEventArgs_t574491439 *)il2cpp_codegen_object_new(OscBundleReceivedEventArgs_t574491439_il2cpp_TypeInfo_var);
		OscBundleReceivedEventArgs__ctor_m3816053266(L_3, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		EventHandler_1_Invoke_m2727238407(L_1, __this, L_3, /*hidden argument*/EventHandler_1_Invoke_m2727238407_RuntimeMethod_var);
	}

IL_001d:
	{
		OscBundle_t1226152694 * L_4 = ___bundle0;
		NullCheck(L_4);
		RuntimeObject* L_5 = OscPacket_get_Data_m3397942478(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t1613291102_il2cpp_TypeInfo_var, L_5);
		V_0 = L_6;
		V_1 = 0;
		goto IL_0076;
	}

IL_0030:
	{
		OscBundle_t1226152694 * L_7 = ___bundle0;
		NullCheck(L_7);
		RuntimeObject* L_8 = OscPacket_get_Data_m3397942478(L_7, /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		NullCheck(L_8);
		RuntimeObject * L_10 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t600458651_il2cpp_TypeInfo_var, L_8, L_9);
		V_2 = L_10;
		RuntimeObject * L_11 = V_2;
		if (!((OscBundle_t1226152694 *)IsInstSealed((RuntimeObject*)L_11, OscBundle_t1226152694_il2cpp_TypeInfo_var)))
		{
			goto IL_0059;
		}
	}
	{
		RuntimeObject * L_12 = V_2;
		UDPReceiver_onBundleReceived_m1364519174(__this, ((OscBundle_t1226152694 *)CastclassSealed((RuntimeObject*)L_12, OscBundle_t1226152694_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_0072;
	}

IL_0059:
	{
		RuntimeObject * L_13 = V_2;
		if (!((OscMessage_t3323977005 *)IsInstClass((RuntimeObject*)L_13, OscMessage_t3323977005_il2cpp_TypeInfo_var)))
		{
			goto IL_0072;
		}
	}
	{
		RuntimeObject * L_14 = V_2;
		V_3 = ((OscMessage_t3323977005 *)CastclassClass((RuntimeObject*)L_14, OscMessage_t3323977005_il2cpp_TypeInfo_var));
		OscMessage_t3323977005 * L_15 = V_3;
		UDPReceiver_onMessageReceived_m1127692520(__this, L_15, /*hidden argument*/NULL);
	}

IL_0072:
	{
		int32_t L_16 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
	}

IL_0076:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0030;
		}
	}
	{
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::onMessageReceived(OSCsharp.Data.OscMessage)
extern "C"  void UDPReceiver_onMessageReceived_m1127692520 (UDPReceiver_t1881969775 * __this, OscMessage_t3323977005 * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_onMessageReceived_m1127692520_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EventHandler_1_t314378975 * L_0 = __this->get_messageReceivedInvoker_3();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		EventHandler_1_t314378975 * L_1 = __this->get_messageReceivedInvoker_3();
		OscMessage_t3323977005 * L_2 = ___message0;
		OscMessageReceivedEventArgs_t2390219542 * L_3 = (OscMessageReceivedEventArgs_t2390219542 *)il2cpp_codegen_object_new(OscMessageReceivedEventArgs_t2390219542_il2cpp_TypeInfo_var);
		OscMessageReceivedEventArgs__ctor_m3694517805(L_3, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		EventHandler_1_Invoke_m2312074494(L_1, __this, L_3, /*hidden argument*/EventHandler_1_Invoke_m2312074494_RuntimeMethod_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::onError(System.Exception)
extern "C"  void UDPReceiver_onError_m4129978076 (UDPReceiver_t1881969775 * __this, Exception_t * ___ex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_onError_m4129978076_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EventHandler_1_t319478644 * L_0 = __this->get_ErrorOccured_2();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		EventHandler_1_t319478644 * L_1 = __this->get_ErrorOccured_2();
		Exception_t * L_2 = ___ex0;
		ExceptionEventArgs_t2395319211 * L_3 = (ExceptionEventArgs_t2395319211 *)il2cpp_codegen_object_new(ExceptionEventArgs_t2395319211_il2cpp_TypeInfo_var);
		ExceptionEventArgs__ctor_m1472460080(L_3, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		EventHandler_1_Invoke_m3384041340(L_1, __this, L_3, /*hidden argument*/EventHandler_1_Invoke_m3384041340_RuntimeMethod_var);
	}

IL_001d:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Net.Sockets.UdpClient OSCsharp.Net.UDPReceiver/UdpState::get_Client()
extern "C"  UdpClient_t967282006 * UdpState_get_Client_m2855211709 (UdpState_t629681666 * __this, const RuntimeMethod* method)
{
	{
		UdpClient_t967282006 * L_0 = __this->get_U3CClientU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver/UdpState::set_Client(System.Net.Sockets.UdpClient)
extern "C"  void UdpState_set_Client_m3591208944 (UdpState_t629681666 * __this, UdpClient_t967282006 * ___value0, const RuntimeMethod* method)
{
	{
		UdpClient_t967282006 * L_0 = ___value0;
		__this->set_U3CClientU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver/UdpState::get_IPEndPoint()
extern "C"  IPEndPoint_t3791887218 * UdpState_get_IPEndPoint_m3884943819 (UdpState_t629681666 * __this, const RuntimeMethod* method)
{
	{
		IPEndPoint_t3791887218 * L_0 = __this->get_U3CIPEndPointU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver/UdpState::set_IPEndPoint(System.Net.IPEndPoint)
extern "C"  void UdpState_set_IPEndPoint_m1779452615 (UdpState_t629681666 * __this, IPEndPoint_t3791887218 * ___value0, const RuntimeMethod* method)
{
	{
		IPEndPoint_t3791887218 * L_0 = ___value0;
		__this->set_U3CIPEndPointU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver/UdpState::.ctor(System.Net.Sockets.UdpClient,System.Net.IPEndPoint)
extern "C"  void UdpState__ctor_m2410451527 (UdpState_t629681666 * __this, UdpClient_t967282006 * ___client0, IPEndPoint_t3791887218 * ___ipEndPoint1, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		UdpClient_t967282006 * L_0 = ___client0;
		UdpState_set_Client_m3591208944(__this, L_0, /*hidden argument*/NULL);
		IPEndPoint_t3791887218 * L_1 = ___ipEndPoint1;
		UdpState_set_IPEndPoint_m1779452615(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OSCsharp.Utils.ExceptionEventArgs::set_Exception(System.Exception)
extern "C"  void ExceptionEventArgs_set_Exception_m1841099795 (ExceptionEventArgs_t2395319211 * __this, Exception_t * ___value0, const RuntimeMethod* method)
{
	{
		Exception_t * L_0 = ___value0;
		__this->set_U3CExceptionU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void OSCsharp.Utils.ExceptionEventArgs::.ctor(System.Exception)
extern "C"  void ExceptionEventArgs__ctor_m1472460080 (ExceptionEventArgs_t2395319211 * __this, Exception_t * ___ex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExceptionEventArgs__ctor_m1472460080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3591816995_il2cpp_TypeInfo_var);
		EventArgs__ctor_m32674013(__this, /*hidden argument*/NULL);
		Exception_t * L_0 = ___ex0;
		ExceptionEventArgs_set_Exception_m1841099795(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Byte[] OSCsharp.Utils.Utility::SwapEndian(System.Byte[])
extern "C"  ByteU5BU5D_t4116647657* Utility_SwapEndian_m1429200246 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utility_SwapEndian_m1429200246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t4116647657* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		ByteU5BU5D_t4116647657* L_0 = ___data0;
		NullCheck(L_0);
		V_0 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))));
		ByteU5BU5D_t4116647657* L_1 = ___data0;
		NullCheck(L_1);
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))), (int32_t)1));
		V_2 = 0;
		goto IL_0024;
	}

IL_0016:
	{
		ByteU5BU5D_t4116647657* L_2 = V_0;
		int32_t L_3 = V_2;
		ByteU5BU5D_t4116647657* L_4 = ___data0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (uint8_t)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1));
		int32_t L_9 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0024:
	{
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) >= ((int32_t)0)))
		{
			goto IL_0016;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_11 = V_0;
		return L_11;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
