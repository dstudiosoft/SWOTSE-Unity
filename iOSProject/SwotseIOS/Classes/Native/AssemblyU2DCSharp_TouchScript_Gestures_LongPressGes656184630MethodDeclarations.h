﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchScript.Gestures.LongPressGesture
struct LongPressGesture_t656184630;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void TouchScript.Gestures.LongPressGesture::.ctor()
extern "C"  void LongPressGesture__ctor_m99376347 (LongPressGesture_t656184630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.LongPressGesture::add_LongPressed(System.EventHandler`1<System.EventArgs>)
extern "C"  void LongPressGesture_add_LongPressed_m953410605 (LongPressGesture_t656184630 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.LongPressGesture::remove_LongPressed(System.EventHandler`1<System.EventArgs>)
extern "C"  void LongPressGesture_remove_LongPressed_m3477308252 (LongPressGesture_t656184630 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.LongPressGesture::get_TimeToPress()
extern "C"  float LongPressGesture_get_TimeToPress_m1231207429 (LongPressGesture_t656184630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.LongPressGesture::set_TimeToPress(System.Single)
extern "C"  void LongPressGesture_set_TimeToPress_m3174460728 (LongPressGesture_t656184630 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TouchScript.Gestures.LongPressGesture::get_DistanceLimit()
extern "C"  float LongPressGesture_get_DistanceLimit_m650606236 (LongPressGesture_t656184630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.LongPressGesture::set_DistanceLimit(System.Single)
extern "C"  void LongPressGesture_set_DistanceLimit_m2329475769 (LongPressGesture_t656184630 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.LongPressGesture::OnEnable()
extern "C"  void LongPressGesture_OnEnable_m2439278399 (LongPressGesture_t656184630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.LongPressGesture::touchesBegan(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void LongPressGesture_touchesBegan_m3504328765 (LongPressGesture_t656184630 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.LongPressGesture::touchesMoved(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void LongPressGesture_touchesMoved_m3187141767 (LongPressGesture_t656184630 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.LongPressGesture::touchesEnded(System.Collections.Generic.IList`1<TouchScript.TouchPoint>)
extern "C"  void LongPressGesture_touchesEnded_m188301244 (LongPressGesture_t656184630 * __this, Il2CppObject* ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.LongPressGesture::onRecognized()
extern "C"  void LongPressGesture_onRecognized_m2021490678 (LongPressGesture_t656184630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchScript.Gestures.LongPressGesture::reset()
extern "C"  void LongPressGesture_reset_m3232270856 (LongPressGesture_t656184630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TouchScript.Gestures.LongPressGesture::wait()
extern "C"  Il2CppObject * LongPressGesture_wait_m4288533790 (LongPressGesture_t656184630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
