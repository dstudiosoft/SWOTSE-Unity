﻿using UnityEngine;
using System;
using System.Collections;
using Tacticsoft;
using UnityEngine.UI;


public class ShopTableCell : TableViewCell
{
    public Text itemLabel1;
    public Text itemDescription1;
    public Text itemCount1;
    public Image itemImage1;
    public Text itemCost1;
    public Text itemAction1;
    public GameObject UseBtn1;
    private string itemId1;
    private string constantId1;

    public Text itemLabel2;
    public Text itemDescription2;
    public Text itemCount2;
    public Image itemImage2;
    public Text itemCost2;
    public Text itemAction2;
    public GameObject UseBtn2;
    private string itemId2;
    private string constantId2;

//    public Text itemLabel3;
//    public Text itemDescription3;
//    public Text itemCount3;
//    public Image itemImage3;
//    public Text itemCost3;
//    public Text itemAction3;
//    public GameObject UseBtn3;
//    private string itemId3;
//    private string constantId3;

    private IReloadable owner;
    private string type;
    private long evendId;

    public void SetOwner(IReloadable controller)
    {
        owner = controller;
    }

    public void SetEvent(long id)
    {
        evendId = id;
    }

    public void SetType(string value)
    {
        type = value;
    }

    public void SetUsable(bool usable)
    {
        UseBtn1.SetActive(usable);
        UseBtn2.SetActive(usable);
    }

    public void SetLabel1(string label) 
    {
        itemLabel1.text = label;
    }

    public void SetDescription1(string description) 
    {
        if (SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
        {
            StartCoroutine(TextLocalizer.WrapMultiline(itemDescription1, description));
            itemDescription1.alignment = TextAnchor.UpperRight;
        }
        else
        {
            itemDescription1.text = description;
        }
    }

    public void SetCount1(long count) 
    {
        itemCount1.text = count.ToString();
    }

    public void SetImage1(string path) 
    {
		Sprite buildingSpriteFromLibrary = Resources.Load<Sprite>(path);
		itemImage1.sprite = buildingSpriteFromLibrary;
    }

    public void SetCost1(long cost) 
    {
        itemCost1.text = cost.ToString();
    }

    public void SetId1(string id)
    {
        itemId1 = id;
    }

    public void SetConstantId1(string id)
    {
        constantId1 = id;
    }

    public void SetLabel2(string label) 
    {
        itemLabel2.text = label;
    }

    public void SetDescription2(string description) 
    {
        if (SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
        {
            StartCoroutine(TextLocalizer.WrapMultiline(itemDescription2, description));
            itemDescription2.alignment = TextAnchor.UpperRight;
        }
        else
        {
            itemDescription2.text = description;
        }
    }

    public void SetCount2(long count) 
    {
        itemCount2.text = count.ToString();
    }

    public void SetImage2(string path) 
    {
        Sprite buildingSpriteFromLibrary = Resources.Load<Sprite>(path);
        itemImage2.sprite = buildingSpriteFromLibrary;
    }

    public void SetCost2(long cost) 
    {
        itemCost2.text = cost.ToString();
    }

    public void SetId2(string id)
    {
        itemId2 = id;
    }

    public void SetConstantId2(string id)
    {
        constantId2 = id;
    }

//    public void SetLabel3(string label) 
//    {
//        itemLabel3.text = label;
//    }
//
//    public void SetDescription3(string description) 
//    {
//        itemDescription3.text = description;
//    }
//
//    public void SetCount3(long count) 
//    {
//        itemCount3.text = count.ToString();
//    }
//
//    public void SetImage3(string path) 
//    {
//        Sprite buildingSpriteFromLibrary = Resources.Load<Sprite>(path);
//        itemImage3.sprite = buildingSpriteFromLibrary;
//    }
//
//    public void SetCost3(long cost) 
//    {
//        itemCost3.text = cost.ToString();
//    }
//
//    public void SetId3(string id)
//    {
//        itemId3 = id;
//    }
//
//    public void SetConstantId3(string id)
//    {
//        constantId3 = id;
//    }

    public void SetAction(string action)
    {
        itemAction1.text = action;
        itemAction2.text = action;
//        itemAction3.text = action;
    }

    public void PerformAction1()
    {
        if (owner.GetType() == typeof(ShopTableController))
        {
            ShopTableController cntr = (ShopTableController)owner;
            if (!cntr.shop)
            {
                if (cntr.cityId != -1)
                {
                    GlobalGOScript.instance.windowInstanceManager.openBuyItem(itemImage1.sprite, itemLabel1.text, itemCount1.text, itemCost1.text, itemId1, owner, false);
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openSellItem(itemImage1.sprite, itemLabel1.text, itemCount1.text, itemId1, owner);
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openBuyItem(itemImage1.sprite, itemLabel1.text, itemCount1.text, itemCost1.text, itemId1, owner, true);
            }
        }
        else if (owner.GetType() == typeof(SpeedUpTableController))
        {
            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += owner.ReloadTable;
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.UseSpeedUp(itemId1, evendId));
        }
    }

    public void PerformAction2()
    {
        if (owner.GetType() == typeof(ShopTableController))
        {
            ShopTableController cntr = (ShopTableController)owner;
            if (!cntr.shop)
            {
                if (cntr.cityId != -1)
                {
                    GlobalGOScript.instance.windowInstanceManager.openBuyItem(itemImage2.sprite, itemLabel2.text, itemCount2.text, itemCost2.text, itemId2, owner, false);
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openSellItem(itemImage2.sprite, itemLabel2.text, itemCount2.text, itemId2, owner);
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openBuyItem(itemImage2.sprite, itemLabel2.text, itemCount2.text, itemCost2.text, itemId2, owner, true);
            }
        }
        else if (owner.GetType() == typeof(SpeedUpTableController))
        {
            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += owner.ReloadTable;
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.UseSpeedUp(itemId2, evendId));
        }
    }

//    public void PerformAction3()
//    {
//        if (owner.GetType() == typeof(ShopTableController))
//        {
//            ShopTableController cntr = (ShopTableController)owner;
//            if (!cntr.shop)
//            {
//                GlobalGOScript.instance.windowInstanceManager.openSellItem(itemImage3.sprite, itemLabel3.text, itemCount3.text, itemId3, owner);
//            }
//            else
//            {
//                GlobalGOScript.instance.windowInstanceManager.openBuyItem(itemImage3.sprite, itemLabel3.text, itemCount3.text, itemCost3.text, itemId3, owner);
//            }
//        }
//        else if (owner.GetType() == typeof(SpeedUpTableController))
//        {
//            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += owner.ReloadTable;
//            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.UseSpeedUp(itemId3, evendId));
//        }
//    }

    public void Use1()
    {
        if (owner.GetType() == typeof(ShopTableController))
        {
            if (!((ShopTableController)owner).shop)
            {
                switch (type)
                {
                    case "productions":
                    case "battalions":
                    case "scripts":
                        if (string.Equals(constantId1, "teleport_script"))
                        {
                            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += owner.TeleportUsed;
                        }

                        if (constantId1.Contains("new"))
                        {
                            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += GlobalGOScript.instance.cityChanger.NewCityHandler;
                            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += GlobalGOScript.instance.GetCities;
                        }

                        if (constantId1.Contains("expantion"))
                        {
                            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += owner.ExpandUsed;
                        }

                        if (string.Equals(constantId1, "lvl_10_upgrade"))
                        {
                            GlobalGOScript.instance.windowInstanceManager.openUpgrade(itemId1, owner);
                        }
                        else if (string.Equals(constantId1, "advance_teleport_script"))
                        {
                            GlobalGOScript.instance.windowInstanceManager.openTeleport(itemId1, owner);
                        }
                        else if (string.Equals(constantId1, "city_deed"))
                        {
                            GlobalGOScript.instance.windowInstanceManager.openCityDeed(itemId1, owner);
                        }
                        else if (string.Equals(constantId1, "shield_hour"))
                        {
                            GlobalGOScript.instance.windowInstanceManager.openShieldTimeWindow(itemId1, true, owner);
                        }
                        else if (string.Equals(constantId1, "shield_day"))
                        {
                            GlobalGOScript.instance.windowInstanceManager.openShieldTimeWindow(itemId1, false, owner);
                        }
                        else
                        {
                            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += owner.ReloadTable;
                            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.UseItem(itemId1));
                        }

                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void Use2()
    {
        if (owner.GetType() == typeof(ShopTableController))
        {
            if (!((ShopTableController)owner).shop)
            {
                switch (type)
                {
                    case "productions":
                    case "battalions":
                    case "scripts":
                        if (string.Equals(constantId2, "teleport_script"))
                        {
                            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += owner.TeleportUsed;
                        }

                        if (constantId2.Contains("new"))
                        {
                            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += GlobalGOScript.instance.cityChanger.NewCityHandler;
                            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += GlobalGOScript.instance.GetCities;
                        }

                        if (constantId2.Contains("expantion"))
                        {
                            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += owner.ExpandUsed;
                        }

                        if (string.Equals(constantId2, "lvl_10_upgrade"))
                        {
                            GlobalGOScript.instance.windowInstanceManager.openUpgrade(itemId2, owner);
                        }
                        else if (string.Equals(constantId2, "advance_teleport_script"))
                        {
                            GlobalGOScript.instance.windowInstanceManager.openTeleport(itemId2, owner);
                        }
                        else if (string.Equals(constantId2, "city_deed"))
                        {
                            GlobalGOScript.instance.windowInstanceManager.openCityDeed(itemId2, owner);
                        }
                        else if (string.Equals(constantId2, "shield_hour"))
                        {
                            GlobalGOScript.instance.windowInstanceManager.openShieldTimeWindow(itemId2, true, owner);
                        }
                        else if (string.Equals(constantId2, "shield_day"))
                        {
                            GlobalGOScript.instance.windowInstanceManager.openShieldTimeWindow(itemId2, false, owner);
                        }
                        else
                        {
                            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += owner.ReloadTable;
                            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.UseItem(itemId2));
                        }

                        break;
                    default:
                        break;
                }
            }
        }
    }

//    public void Use3()
//    {
//        if (owner.GetType() == typeof(ShopTableController))
//        {
//            if (!((ShopTableController)owner).shop)
//            {
//                switch (type)
//                {
//                    case "productions":
//                    case "battalions":
//                    case "scripts":
//                        if (string.Equals(constantId3, "teleport_script"))
//                        {
//                            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += owner.TeleportUsed;
//                        }
//
//                        if (constantId3.Contains("new"))
//                        {
//                            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += GlobalGOScript.instance.cityChanger.NewCityHandler;
//                        }
//
//                        if (constantId3.Contains("expantion"))
//                        {
//                            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += owner.ExpandUsed;
//                        }
//
//                        if (string.Equals(constantId3, "lvl_10_upgrade"))
//                        {
//                            GlobalGOScript.instance.windowInstanceManager.openUpgrade(itemId3, owner);
//                        }
//                        else if (string.Equals(constantId3, "advance_teleport_script"))
//                        {
//                            GlobalGOScript.instance.windowInstanceManager.openTeleport(itemId3, owner);
//                        }
//                        else if (string.Equals(constantId3, "city_deed"))
//                        {
//                            GlobalGOScript.instance.windowInstanceManager.openCityDeed(itemId3, owner);
//                        }
//                        else
//                        {
//                            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += owner.ReloadTable;
//                            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.UseItem(itemId3));
//                        }
//                        break;
//                    default:
//                        break;
//                }
//            }
//        }
//    }
}
