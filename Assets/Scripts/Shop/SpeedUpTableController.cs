﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using Tacticsoft;
using UnityEngine.Networking;
using SmartLocalization;

public class SpeedUpTableController : MonoBehaviour, ITableViewDataSource, IReloadable {

    private ArrayList currentSourceArray = new ArrayList ();

    public ShopTableCell m_cellPrefab;
    public TableView m_tableView;

    public Image itemImage;
    public Text timeLeft;
    private long eventId;
    private DateTime startTime;
    private DateTime finishTime;
    private PanelTableController owner;
    private TimeSpan timespan;

    public void SetContent(Sprite image, long id, DateTime start_time, DateTime finish_time, PanelTableController controller)
    {
        itemImage.sprite = image;
        eventId = id;
        startTime = start_time;
        finishTime = finish_time;
        owner = controller;
    }

    void OnEnable()
    {
        timespan = finishTime - startTime;
        timespan -= GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update - startTime;
        timeLeft.text = timespan.Hours.ToString() + ":" + timespan.Minutes.ToString() + ":" + timespan.Seconds.ToString();
    }

    void FixedUpdate()
    {
        if (finishTime <= GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update)
        {
            GlobalGOScript.instance.windowInstanceManager.closeSpeedUp();
        }
        else
        {
            timespan = timespan.Subtract(new TimeSpan(0, 0, 1));
            timeLeft.text = timespan.Hours.ToString() + ":" + timespan.Minutes.ToString() + ":" + timespan.Seconds.ToString();
        }
    }

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start() {
        m_tableView.dataSource = this;
        currentSourceArray = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.treasureManager.speedUpTreasures.Values);
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (currentSourceArray == null)
        {
            return 0;
        }
        else
        {
//            return Mathf.CeilToInt(this.currentSourceArray.Count / 3.0f);
            return Mathf.CeilToInt(this.currentSourceArray.Count / 2.0f);
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform.GetChild(0) as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        ShopTableCell cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as ShopTableCell;
        if (cell == null) 
        {
            cell = (ShopTableCell)GameObject.Instantiate(m_cellPrefab);
        }

//        int index = row * 3;
        int index = row * 2;

        CityTreasureModel item = (CityTreasureModel)(currentSourceArray[index]);

        cell.SetType(item.item_constant.type);

        cell.transform.GetChild(0).gameObject.SetActive(true);
        cell.SetLabel1(item.item_constant.name);
        cell.SetCost1(item.item_constant.price_sh);
        cell.SetDescription1(item.item_constant.description);
        cell.SetCount1(item.count);
        cell.SetImage1("UI/ShopItems/" + item.item_constant.type + "/" + item.item_constant.id);
        cell.SetId1(item.id);
        cell.SetEvent(eventId);

        if (index + 1 < currentSourceArray.Count)
        {
            item = (CityTreasureModel)(currentSourceArray[index + 1]);
            cell.transform.GetChild(1).gameObject.SetActive(true);
            cell.SetLabel2(item.item_constant.name);
            cell.SetCost2(item.item_constant.price_sh);
            cell.SetDescription2(item.item_constant.description);
            cell.SetCount2(item.count);
            cell.SetImage2("UI/ShopItems/" + item.item_constant.type + "/" + item.item_constant.id);
            cell.SetId2(item.id);
        }
        else
        {
            cell.transform.GetChild(1).gameObject.SetActive(false);
        }

//        if (index + 2 < currentSourceArray.Count)
//        {
//            item = (CityTreasureModel)(currentSourceArray[index + 2]);
//            cell.transform.GetChild(2).gameObject.SetActive(true);
//            cell.SetLabel3(item.item_constant.name);
//            cell.SetCost3(item.item_constant.price_sh);
//            cell.SetDescription3(item.item_constant.description);
//            cell.SetCount3(item.count);
//            cell.SetImage3("UI/ShopItems/" + item.item_constant.type + "/" + item.item_constant.id);
//            cell.SetId3(item.id);
//        }
//        else
//        {
//            cell.transform.GetChild(2).gameObject.SetActive(false);
//        }

        cell.SetAction(LanguageManager.Instance.GetTextValue("SWOTSELocalization.Use").ToUpper());
        cell.SetUsable(false);
        cell.SetOwner(this);

        return cell;
    }

    #endregion

    public void TeleportUsed(object sender, string value)
    {
    }

    public void ExpandUsed(object sender, string value)
    {
    }

    public void GotShopItems(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onGetItems -= GotShopItems;

        if (string.Equals(value, "success"))
        {
            currentSourceArray = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.treasureManager.speedUpTreasures.Values);
            if (string.Equals(owner.content, "BUILDINGS"))
            {
                GlobalGOScript.instance.playerManager.reportManager.onGetBuildingTimers += UpdateTime;
                GlobalGOScript.instance.playerManager.reportManager.onGetBuildingTimers += owner.ReloadTable;
                StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetBuildingTimers(true));
            }

            if (string.Equals(owner.content, "UNITS"))
            {
                GlobalGOScript.instance.playerManager.reportManager.onGetUnitTimers += UpdateTime;
                GlobalGOScript.instance.playerManager.reportManager.onGetUnitTimers += owner.ReloadTable;
                StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetUnitTimers(true));
            }
        }

        m_tableView.ReloadData();
    }

    public void UpdateTime(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.reportManager.onGetBuildingTimers -= UpdateTime;
        GlobalGOScript.instance.playerManager.reportManager.onGetUnitTimers -= UpdateTime;

        if (string.Equals(value, "success"))
        {
            if (string.Equals(owner.content, "BUILDINGS"))
            {
                if (GlobalGOScript.instance.playerManager.reportManager.buildingTimers.ContainsKey(eventId))
                {
                    BuildingTimer timer = GlobalGOScript.instance.playerManager.reportManager.buildingTimers[eventId];
                    startTime = timer.start_time;
                    finishTime = timer.finish_time;
                    timespan = finishTime - startTime;
                    timespan -= GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update - startTime;
                    timeLeft.text = timespan.Hours.ToString() + ":" + timespan.Minutes.ToString() + ":" + timespan.Seconds.ToString();
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.closeSpeedUp();
                }
            }

            if (string.Equals(owner.content, "UNITS"))
            {
                if (GlobalGOScript.instance.playerManager.reportManager.unitTimers.ContainsKey(eventId))
                {
                    UnitTimer timer = GlobalGOScript.instance.playerManager.reportManager.unitTimers[eventId];
                    startTime = timer.start_time;
                    finishTime = timer.finish_time;
                    timespan = finishTime - startTime;
                    timespan -= GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update - startTime;
                    timeLeft.text = timespan.Hours.ToString() + ":" + timespan.Minutes.ToString() + ":" + timespan.Seconds.ToString();
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.closeSpeedUp();
                }
            }
        }
    }

    public void ReloadTable(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed -= ReloadTable;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onGetItems += GotShopItems;
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.GetCityTreasures(false));
        }
    }
}
