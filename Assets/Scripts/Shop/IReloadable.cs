﻿using UnityEngine;
using System.Collections;

public interface IReloadable {

    void ReloadTable(object sender, string value);
    void TeleportUsed(object sender, string value);
    void ExpandUsed(object sender, string value);
}
