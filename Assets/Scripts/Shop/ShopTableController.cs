﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;
using UnityEngine.Networking;
using SmartLocalization;
using ArabicSupport;

public class ShopTableController : MonoBehaviour, ITableViewDataSource, IReloadable {

	private ArrayList scriptShopCells = new ArrayList ();
	private ArrayList speedUpShopCells = new ArrayList ();
	private ArrayList productionShopCells = new ArrayList ();
	private ArrayList diamondShopCells = new ArrayList ();
	private ArrayList promotionsShopCells = new ArrayList ();
	private ArrayList armyShopCells = new ArrayList ();

	private ArrayList currentSourceArray = new ArrayList ();

    private bool _shop = true;

    public bool shop
    {
        set
        {
            _shop = value;
            icon.sprite = value ? shopIcon : treasureIcon;
        }
        get { return _shop; }
    }
    public long cityId = -1;
    private string content = "scripts";
    private LanguageManager langManager;

    public event SimpleEvent onGetShopItems;

    public ShopTableCell m_cellPrefab;
	public TableView m_tableView;
    public Text Label;
    public Text shillingsCount;
    public Image icon;

    public Sprite tabSelectedTop;
    public Sprite tabSelectedBottom;
    public Sprite tabUnselectedTop;
    public Sprite tabUnselectedBottom;
    public Sprite shopIcon;
    public Sprite treasureIcon;

    public GameObject buyMoreBtn;
    public GameObject buyMoreTxt;

    public Image[] tabs;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start() {
		m_tableView.dataSource = this;
        langManager = LanguageManager.Instance;

        #if UNITY_IOS
            buyMoreBtn.SetActive(false);
        #else
            buyMoreBtn.SetActive(true);
        #endif

        shillingsCount.text = GlobalGOScript.instance.playerManager.user.shillings.ToString();
        string localizedValue;
        if (cityId != -1)
        {
            if (shop)
            {
                localizedValue = LanguageManager.Instance.GetTextValue("SWOTSELocalization.Shop");
                onGetShopItems += GotShopItems;
                StartCoroutine(GetPlayerShopItems());
            }
            else
            {
                localizedValue = LanguageManager.Instance.GetTextValue("SWOTSELocalization.Treasure");
                GlobalGOScript.instance.gameManager.cityManager.treasureManager.onGetItems += GotShopItems;
                StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.GetCityTreasures(false));
            }
        }
        else if (shop)
        {
            localizedValue = LanguageManager.Instance.GetTextValue("SWOTSELocalization.Shop");
            onGetShopItems += GotShopItems;
            StartCoroutine(GetShopItems());
        }
        else
        {
            localizedValue = LanguageManager.Instance.GetTextValue("SWOTSELocalization.Treasure");
            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onGetItems += GotShopItems;
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.GetCityTreasures(false));
        }

        if (langManager.CurrentlyLoadedCulture.languageCode == "ar")
        {
            localizedValue = ArabicFixer.Fix(localizedValue);
        }
        localizedValue = localizedValue.ToUpper();

        Label.text = localizedValue;
	}

    void FixedUpdate()
    {
        shillingsCount.text = GlobalGOScript.instance.playerManager.user.shillings.ToString();
    }

    public void BuyMoreShillings()
    {
        GlobalGOScript.instance.windowInstanceManager.openBuyShillings();
    }
        
	#region ITableViewDataSource

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfRowsForTableView(TableView tableView) {
        if (currentSourceArray == null)
        {
            return 0;
        }
        else
        {
//            return Mathf.CeilToInt(this.currentSourceArray.Count / 3.0f);
            return Mathf.CeilToInt(this.currentSourceArray.Count / 2.0f);
        }
	}

	//Will be called by the TableView to know what is the height of each row
	public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform.GetChild(0) as RectTransform).rect.height;
	}

	//Will be called by the TableView when a cell needs to be created for display
	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
		ShopTableCell cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as ShopTableCell;
        string localizedName;
        string localizedDescription;
		if (cell == null) 
		{
			cell = (ShopTableCell)GameObject.Instantiate(m_cellPrefab);
		}

        cell.gameObject.SetActive(true);

        int index = row * 2;

        if (cityId != -1)
        {
            CityTreasureModel item = (CityTreasureModel)(currentSourceArray[index]);

            cell.SetType(item.item_constant.type);

            cell.transform.GetChild(0).gameObject.SetActive(true);
            localizedName = item.item_constant.name;
            //localizedDescription = item.item_constant.description;
            cell.SetCount1(item.count);
            cell.SetImage1("UI/ShopItems/" + item.item_constant.type + "/" + item.item_constant.id);
            cell.SetId1(item.id);
            cell.SetConstantId1(item.item_constant.id);
            //cell.SetDescription1(item.item_constant.description);
			if (langManager.CurrentlyLoadedCulture.languageCode == "ar")
            {
				Debug.Log("ARAB CityTreasureModel");
				cell.itemDescription1.text = item.item_constant.description;
				//StartCoroutine(TextLocalizer.WrapMultiline(cell.itemDescription1, item.item_constant.description));
                cell.itemDescription1.alignment = TextAnchor.UpperRight;
            }
            else
            {
                cell.itemDescription1.text = item.item_constant.description;
            }
            if (shop)
            {
                cell.SetCost1(item.price);
            }
            else
            {
                cell.SetCost1(item.item_constant.price_sh);
            }
        }
        else if (shop)
        {
            ShopItemModel item = (ShopItemModel)(currentSourceArray[index]);

            cell.SetType(item.type);

            cell.transform.GetChild(0).gameObject.SetActive(true);
            localizedName = item.name;

            //localizedDescription = item.description;
            cell.SetCost1(item.price_sh);
            switch (content)
            {
                case "productions":
                    if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.productionTreasures.Contains(item.id))
                    {
                        cell.SetCount1(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.productionTreasures[item.id])).count);
                    }
                    else
                    {
                        cell.SetCount1(0);
                    }
                    break;
                case "battalions":
                    if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.armyTreasures.Contains(item.id))
                    {
                        cell.SetCount1(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.armyTreasures[item.id])).count);
                    }
                    else
                    {
                        cell.SetCount1(0);
                    }
                    break;
                case "speed_up":
                    if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.speedUpTreasures.Contains(item.id))
                    {
                        cell.SetCount1(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.speedUpTreasures[item.id])).count);
                    }
                    else
                    {
                        cell.SetCount1(0);
                    }
                    break;
                case "promotions":
                    if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.promotionsTreasures.Contains(item.id))
                    {
                        cell.SetCount1(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.promotionsTreasures[item.id])).count);
                    }
                    else
                    {
                        cell.SetCount1(0);
                    }
                    break;
                case "scripts":
                    if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.scriptTreasures.Contains(item.id))
                    {
                        cell.SetCount1(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.scriptTreasures[item.id])).count);
                    }
                    else
                    {
                        cell.SetCount1(0);
                    }
                    break;
                case "diamonds":
                    if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.diamondTreasures.Contains(item.id))
                    {
                        cell.SetCount1(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.diamondTreasures[item.id])).count);
                    }
                    else
                    {
                        cell.SetCount1(0);
                    }
                    break;
                default:
                    break;
            }
            cell.SetImage1("UI/ShopItems/" + item.type + "/" + item.id);
            cell.SetId1(item.id);
            cell.SetConstantId1(item.id);
            //cell.SetDescription1(item.description);
			if (langManager.CurrentlyLoadedCulture.languageCode == "ar")
            {
				Debug.Log("ARAB ShopItemModel");
				//cell.itemDescription1.text = ArabicFixer.Fix (item.description);
				StartCoroutine(TextLocalizer.WrapMultiline(cell.itemDescription1, item.description));
                cell.itemDescription1.alignment = TextAnchor.UpperRight;
            }
            else
            {
                cell.itemDescription1.text = item.description;
            }
        }
        else
        {
            CityTreasureModel item = (CityTreasureModel)(currentSourceArray[index]);

            cell.SetType(item.item_constant.type);

            cell.transform.GetChild(0).gameObject.SetActive(true);
            localizedName =  item.item_constant.name;
			Debug.Log("CityTreasureModel name = " + localizedName);
			Debug.Log("CityTreasureModel name FIX = " + ArabicFixer.Fix(localizedName));
			Debug.Log("CityTreasureModel name FIX FALSE = " + ArabicFixer.Fix(localizedName, false, false));
			Debug.Log("CityTreasureModel name FIX TRUE = " + ArabicFixer.Fix(localizedName, true, true));
            cell.SetCost1(item.item_constant.price_sh);
            localizedDescription = item.item_constant.description;
            cell.SetCount1(item.count);
            cell.SetImage1("UI/ShopItems/" + item.item_constant.type + "/" + item.item_constant.id);
            cell.SetId1(item.id);
            cell.SetConstantId1(item.item_constant.id);
            //cell.SetDescription1(item.item_constant.description);
			if (langManager.CurrentlyLoadedCulture.languageCode == "ar")
            {
				Debug.Log("ARAB CityTreasureModel1 = " + item.item_constant.description);
				cell.itemDescription1.text = item.item_constant.description;
				//Debug.Log("ARAB CityTreasureModel1 FIX = " + cell.itemDescription1.text);
				//StartCoroutine(TextLocalizer.WrapMultiline(cell.itemDescription1, item.item_constant.description));
                cell.itemDescription1.alignment = TextAnchor.UpperRight;
            }
            else
            {
                cell.itemDescription1.text = item.item_constant.description;
            }
        }

        if (langManager.CurrentlyLoadedCulture.languageCode == "ar")
        {
			Debug.Log ("LocalizedNAME 1 = " + localizedName);
            localizedName = ArabicFixer.Fix(localizedName);
			Debug.Log ("LocalizedNAME FIX 1 = " + localizedName);
            //localizedDescription = ArabicFixer.Fix(localizedDescription, false, false);
        }

        cell.SetLabel1(localizedName);

        if (index + 1 < currentSourceArray.Count)
        {
            if (cityId != -1)
            {
                CityTreasureModel item = (CityTreasureModel)(currentSourceArray[index + 1]);
                cell.transform.GetChild(1).gameObject.SetActive(true);
                localizedName = item.item_constant.name;
                //localizedDescription = item.item_constant.description;
                cell.SetCount2(item.count);
                cell.SetImage2("UI/ShopItems/" + item.item_constant.type + "/" + item.item_constant.id);
                cell.SetId2(item.id);
                cell.SetConstantId2(item.item_constant.id);
                //cell.SetDescription2(item.item_constant.description);
				if (langManager.CurrentlyLoadedCulture.languageCode == "ar")
                {
					Debug.Log("ARAB CityTreasureModel2");
					cell.itemDescription2.text = item.item_constant.description;
					//StartCoroutine(TextLocalizer.WrapMultiline(cell.itemDescription2, item.item_constant.description));
                    cell.itemDescription2.alignment = TextAnchor.UpperRight;
                }
                else
                {
                    cell.itemDescription2.text = item.item_constant.description;
                }

                if (shop)
                {
                    cell.SetCost2(item.price);
                }
                else
                {
                    cell.SetCost2(item.item_constant.price_sh);
                }
            }
            else if (shop)
            {
                ShopItemModel item = (ShopItemModel)(currentSourceArray[index + 1]);
                cell.transform.GetChild(1).gameObject.SetActive(true);
                localizedName =  item.name;
                cell.SetCost2(item.price_sh);
                //localizedDescription = item.description;
                switch (content)
                {
                    case "productions":
                        if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.productionTreasures.Contains(item.id))
                        {
                            cell.SetCount2(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.productionTreasures[item.id])).count);
                        }
                        else
                        {
                            cell.SetCount2(0);
                        }
                        break;
                    case "battalions":
                        if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.armyTreasures.Contains(item.id))
                        {
                            cell.SetCount2(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.armyTreasures[item.id])).count);
                        }
                        else
                        {
                            cell.SetCount2(0);
                        }
                        break;
                    case "speed_up":
                        if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.speedUpTreasures.Contains(item.id))
                        {
                            cell.SetCount2(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.speedUpTreasures[item.id])).count);
                        }
                        else
                        {
                            cell.SetCount2(0);
                        }
                        break;
                    case "promotions":
                        if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.promotionsTreasures.Contains(item.id))
                        {
                            cell.SetCount2(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.promotionsTreasures[item.id])).count);
                        }
                        else
                        {
                            cell.SetCount2(0);
                        }
                        break;
                    case "scripts":
                        if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.scriptTreasures.Contains(item.id))
                        {
                            cell.SetCount2(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.scriptTreasures[item.id])).count);
                        }
                        else
                        {
                            cell.SetCount2(0);
                        }
                        break;
                    case "diamonds":
                        if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.diamondTreasures.Contains(item.id))
                        {
                            cell.SetCount2(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.diamondTreasures[item.id])).count);
                        }
                        else
                        {
                            cell.SetCount2(0);
                        }
                        break;
                    default:
                        break;
                }
                cell.SetImage2("UI/ShopItems/" + item.type + "/" + item.id);
                cell.SetId2(item.id);
                cell.SetConstantId2(item.id);
                //cell.SetDescription2(item.description);
				if (langManager.CurrentlyLoadedCulture.languageCode == "ar")
                {
					Debug.Log("ARAB shop2");
					cell.itemDescription2.text = item.description;
					//StartCoroutine(TextLocalizer.WrapMultiline(cell.itemDescription2, item.description));
                    cell.itemDescription2.alignment = TextAnchor.UpperRight;
                }
                else
                {
                    cell.itemDescription2.text = item.description;
                }
            }
            else
            {
                CityTreasureModel item = (CityTreasureModel)(currentSourceArray[index + 1]);
                cell.transform.GetChild(1).gameObject.SetActive(true);
                localizedName = item.item_constant.name;
                cell.SetCost2(item.item_constant.price_sh);
                localizedDescription = item.item_constant.description;
                cell.SetCount2(item.count);
                cell.SetImage2("UI/ShopItems/" + item.item_constant.type + "/" + item.item_constant.id);
                cell.SetId2(item.id);
                cell.SetConstantId2(item.item_constant.id);
                //cell.SetDescription2(item.item_constant.description);

                if (SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
				{  
					cell.itemDescription2.text = item.item_constant.description;
					Debug.Log("ARAB CityTreasureModel3 = " + cell.itemDescription2.text);
					//StartCoroutine(TextLocalizer.WrapMultiline(cell.itemDescription2, item.item_constant.description));
                    cell.itemDescription2.alignment = TextAnchor.UpperRight;
                }
                else
                {
                    cell.itemDescription2.text = item.item_constant.description;
                }
            }

            if (langManager.CurrentlyLoadedCulture.languageCode == "ar")
            {
				Debug.Log ("LocalizedNAME 2 = " + localizedName);
                localizedName = ArabicFixer.Fix(localizedName);
				Debug.Log ("LocalizedNAME FIX 2 = " + localizedName);
                //localizedDescription = ArabicFixer.Fix(localizedDescription, false, false);
            }

            cell.SetLabel2(localizedName);
        }
        else
        {
            cell.transform.GetChild(1).gameObject.SetActive(false);
        }

        /*if (index + 2 < currentSourceArray.Count)
        {
            if (cityId != -1)
            {
                CityTreasureModel item = (CityTreasureModel)(currentSourceArray[index + 2]);
                cell.transform.GetChild(2).gameObject.SetActive(true);
                cell.SetLabel3(item.item_constant.name);
                cell.SetCost3(item.price);
                cell.SetDescription3(item.item_constant.description);
                cell.SetCount3(item.count);
                cell.SetImage3("UI/ShopItems/" + item.item_constant.type + "/" + item.item_constant.id);
                cell.SetId3(item.id);
                cell.SetConstantId3(item.item_constant.id);
            }
            else if (shop)
            {
                ShopItemModel item = (ShopItemModel)(currentSourceArray[index + 2]);
                cell.transform.GetChild(2).gameObject.SetActive(true);
                cell.SetLabel3(item.name);
                cell.SetCost3(item.price_sh);
                cell.SetDescription3(item.description);
                switch (content)
                {
                    case "productions":
                        if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.productionTreasures.Contains(item.id))
                        {
                            cell.SetCount3(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.productionTreasures[item.id])).count);
                        }
                        else
                        {
                            cell.SetCount3(0);
                        }
                        break;
                    case "battalions":
                        if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.armyTreasures.Contains(item.id))
                        {
                            cell.SetCount3(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.armyTreasures[item.id])).count);
                        }
                        else
                        {
                            cell.SetCount3(0);
                        }
                        break;
                    case "speed_up":
                        if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.speedUpTreasures.Contains(item.id))
                        {
                            cell.SetCount3(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.speedUpTreasures[item.id])).count);
                        }
                        else
                        {
                            cell.SetCount3(0);
                        }
                        break;
                    case "promotions":
                        if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.promotionsTreasures.Contains(item.id))
                        {
                            cell.SetCount3(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.promotionsTreasures[item.id])).count);
                        }
                        else
                        {
                            cell.SetCount3(0);
                        }
                        break;
                    case "scripts":
                        if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.scriptTreasures.Contains(item.id))
                        {
                            cell.SetCount3(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.scriptTreasures[item.id])).count);
                        }
                        else
                        {
                            cell.SetCount3(0);
                        }
                        break;
                    case "diamonds":
                        if (GlobalGOScript.instance.gameManager.cityManager.treasureManager.diamondTreasures.Contains(item.id))
                        {
                            cell.SetCount3(((CityTreasureModel)(GlobalGOScript.instance.gameManager.cityManager.treasureManager.diamondTreasures[item.id])).count);
                        }
                        else
                        {
                            cell.SetCount3(0);
                        }
                        break;
                    default:
                        break;
                }
                cell.SetImage3("UI/ShopItems/" + item.type + "/" + item.id);
                cell.SetId3(item.id);
                cell.SetConstantId3(item.id);
            }
            else
            {
                CityTreasureModel item = (CityTreasureModel)(currentSourceArray[index + 2]);
                cell.transform.GetChild(2).gameObject.SetActive(true);
                cell.SetLabel3(item.item_constant.name);
                cell.SetCost3(item.item_constant.price_sh);
                cell.SetDescription3(item.item_constant.description);
                cell.SetCount3(item.count);
                cell.SetImage3("UI/ShopItems/" + item.item_constant.type + "/" + item.item_constant.id);
                cell.SetId3(item.id);
                cell.SetConstantId3(item.item_constant.id);
            }
        }
        else
        {
            cell.transform.GetChild(2).gameObject.SetActive(false);
        }*/

        if (cityId != -1)
        {
            if (shop)
            {
                localizedName = LanguageManager.Instance.GetTextValue("SWOTSELocalization.Buy");
            }
            else
            {
                localizedName = LanguageManager.Instance.GetTextValue("SWOTSELocalization.Gift");
            }

            cell.SetUsable(false);
        }
        else if (shop)
        {
            localizedName = LanguageManager.Instance.GetTextValue("SWOTSELocalization.Buy");

            cell.SetUsable(false);
        }
        else
        {
            localizedName = LanguageManager.Instance.GetTextValue("SWOTSELocalization.Sell");

            cell.SetUsable(true);
        }

        if (langManager.CurrentlyLoadedCulture.languageCode == "ar")
        {
            localizedName = ArabicFixer.Fix(localizedName);
        }
        localizedName = localizedName.ToUpper();
        cell.SetAction(localizedName);
        cell.SetOwner(this);

		return cell;
	}

	#endregion

	public void categoryButtonClicked(string category)
	{
        for(int i = 0; i < tabs.Length; i++)
        {
            if (i < 4)
                tabs[i].sprite = tabUnselectedTop;
            else
                tabs[i].sprite = tabUnselectedBottom;
        }

        switch (category)
		{
            case "productions":
                content = category;
                if (shop)
                {
                    currentSourceArray = productionShopCells;
                }
                else
                {
                    currentSourceArray = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.treasureManager.productionTreasures.Values);
                }
                tabs[2].sprite = tabSelectedTop;
			break;
		case "battalions":
                content = category;
                if (shop)
                {
                    currentSourceArray = armyShopCells;
                }
                else
                {
                    currentSourceArray = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.treasureManager.armyTreasures.Values);
                }
                tabs[5].sprite = tabSelectedBottom;
			break;
		case "speed_up":
                content = category;
                if (shop)
                {
                    currentSourceArray = speedUpShopCells;
                }
                else
                {
                    currentSourceArray = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.treasureManager.speedUpTreasures.Values);
                }
                tabs[1].sprite = tabSelectedTop;
			break;
		case "promotions":
                content = category;
                if (shop)
                {
                    currentSourceArray = promotionsShopCells;
                }
                else
                {
                    currentSourceArray = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.treasureManager.promotionsTreasures.Values);
                }
                tabs[4].sprite = tabSelectedBottom;
			break;
		case "scripts":
                content = category;
                if (shop)
                {
                    currentSourceArray = scriptShopCells;
                }
                else
                {
                    currentSourceArray = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.treasureManager.scriptTreasures.Values);
                }
                tabs[0].sprite = tabSelectedTop;
			break;
		case "diamonds":
                content = category;
                if (shop)
                {
                    currentSourceArray = diamondShopCells;
                }
                else
                {
                    currentSourceArray = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.treasureManager.diamondTreasures.Values);
                }
                tabs[3].sprite = tabSelectedTop;
			break;
		default:
			break;
		}
		m_tableView.ReloadData();
	}

    public IEnumerator GetShopItems()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/shop/items_constants/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            string decodedString = System.Text.Encoding.UTF8.GetString(request.downloadHandler.data);
            JSONObject itemArray = JSONObject.Create(decodedString);

            scriptShopCells = new ArrayList ();
            speedUpShopCells = new ArrayList ();
            productionShopCells = new ArrayList ();
            diamondShopCells = new ArrayList ();
            promotionsShopCells = new ArrayList ();
            armyShopCells = new ArrayList ();

            foreach (JSONObject item in itemArray.list)
            {
                ShopItemModel shopItem = JsonUtility.FromJson<ShopItemModel>(item.Print());

//                if (string.Equals(LanguageManager.Instance.CurrentlyLoadedCulture.languageCode, "ar"))
//                {
//                    char[] name = shopItem.name.ToCharArray();
//                    System.Array.Reverse(name);
//                    shopItem.name = new string(name);
//                    char[] description = shopItem.description.ToCharArray();
//                    System.Array.Reverse(description);
//                    shopItem.description = new string(description); 
//                }
                    
                switch (shopItem.type)
                {
                    case "scripts":
                        scriptShopCells.Add(shopItem);
                        break;

                    case "speed_up":
                        speedUpShopCells.Add(shopItem);
                        break;

                    case "productions":
                        productionShopCells.Add(shopItem);
                        break;

                    case "diamonds":
                        diamondShopCells.Add(shopItem);
                        break;

                    case "battalions":
                        armyShopCells.Add(shopItem);
                        break;

                    case "promotion":
                        promotionsShopCells.Add(shopItem);
                        break;

                    default:
                        break;
                }
            }

            if (onGetShopItems != null)
            {
                onGetShopItems(this, "success");
            }
        }
    }

    public IEnumerator GetPlayerShopItems()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://" + GlobalGOScript.instance.host + ":8889/shop/getCityLots/" + cityId.ToString() + "/");
        request.SetRequestHeader("authorization", "Token " + GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            string decodedString = System.Text.Encoding.UTF8.GetString(request.downloadHandler.data);
            JSONObject itemArray = JSONObject.Create(decodedString);
            scriptShopCells = new ArrayList();

            foreach (JSONObject item in itemArray.list)
            {
                CityTreasureModel shopItem = JsonUtility.FromJson<CityTreasureModel>(item.Print());
                shopItem.item_constant = JsonUtility.FromJson<ShopItemModel>(item["item_constant"].Print());

//                if (string.Equals(LanguageManager.Instance.CurrentlyLoadedCulture.languageCode, "ar"))
//                {
//                    char[] name = shopItem.item_constant.name.ToCharArray();
//                    System.Array.Reverse(name);
//                    shopItem.item_constant.name = new string(name);
//                    char[] description = shopItem.item_constant.description.ToCharArray();
//                    System.Array.Reverse(description);
//                    shopItem.item_constant.description = new string(description);
//                }

                switch (shopItem.item_constant.type)
                {
                    case "scripts":
                        scriptShopCells.Add(shopItem);
                        break;

                    case "speed_up":
                        speedUpShopCells.Add(shopItem);
                        break;

                    case "productions":
                        productionShopCells.Add(shopItem);
                        break;

                    case "diamonds":
                        diamondShopCells.Add(shopItem);
                        break;

                    case "battalions":
                        armyShopCells.Add(shopItem);
                        break;

                    case "promotion":
                        promotionsShopCells.Add(shopItem);
                        break;

                    default:
                        break;
                }
            }

            if (onGetShopItems != null)
            {
                onGetShopItems(this, "success");
            }
        }
    }

    public void GotShopItems(object sender, string value)
    {
        onGetShopItems -= GotShopItems;
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onGetItems -= GotShopItems;
        if (string.Equals(value, "success"))
        {
            switch (content)
            {
                case "productions":
                    if (shop)
                    {
                        currentSourceArray = productionShopCells;
                    }
                    else
                    {
                        currentSourceArray = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.treasureManager.productionTreasures.Values);
                    }
                    break;
                case "battalions":
                    if (shop)
                    {
                        currentSourceArray = armyShopCells;
                    }
                    else
                    {
                        currentSourceArray = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.treasureManager.armyTreasures.Values);
                    }
                    break;
                case "speed_up":
                    if (shop)
                    {
                        currentSourceArray = speedUpShopCells;
                    }
                    else
                    {
                        currentSourceArray = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.treasureManager.speedUpTreasures.Values);
                    }
                    break;
                case "promotions":
                    if (shop)
                    {
                        currentSourceArray = promotionsShopCells;
                    }
                    else
                    {
                        currentSourceArray = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.treasureManager.promotionsTreasures.Values);
                    }
                    break;
                case "scripts":
                    if (shop)
                    {
                        currentSourceArray = scriptShopCells;
                    }
                    else
                    {
                        currentSourceArray = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.treasureManager.scriptTreasures.Values);
                    }
                    break;
                case "diamonds":
                    if (shop)
                    {
                        currentSourceArray = diamondShopCells;
                    }
                    else
                    {
                        currentSourceArray = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.treasureManager.diamondTreasures.Values);
                    }
                    break;
                default:
                    break;
            }
        }
        m_tableView.ReloadData();
    }

    public void TeleportUsed(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed -= TeleportUsed;

        if (string.Equals(value, "success"))
        {
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.UpdateCurrentCityCoords());
        }
    }

    public void ExpandUsed(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed -= ExpandUsed;

        if (string.Equals(value, "success"))
        {
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.UpdateCityExpantion());
        }
    }

    public void ReloadTable(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onGetItems -= ReloadTable;
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemBought -= ReloadTable;
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemSold -= ReloadTable;
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed -= ReloadTable;
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemGifted -= ReloadTable;
        onGetShopItems -= ReloadTable;

        if (string.Equals(value, "success"))
        {
            if (cityId != -1)
            {
                if (shop)
                {
                    onGetShopItems += GotShopItems;
                    StartCoroutine(GetPlayerShopItems());
                }
                else
                {
                    GlobalGOScript.instance.gameManager.cityManager.treasureManager.onGetItems += GotShopItems;
                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.GetCityTreasures(false));
                }

            }
            else if (shop)
            {
                m_tableView.ReloadData();
            }
            else
            {
                GlobalGOScript.instance.gameManager.cityManager.treasureManager.onGetItems += GotShopItems;
                StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.GetCityTreasures(false));
            }
        }
    }
}
