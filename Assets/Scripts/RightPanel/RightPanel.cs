﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RightPanel : MonoBehaviour {

    public static RightPanel instance;

    public RectTransform RightButtonsTransform;
    public Image cityIcon;
    public Text cityCoords;
    public Text citiesCount;

    public bool isOpened = true;

    public void Start()
    {
        if (!instance)
        {
            instance = this;
        }
    }

    void OnEnable()
    {
        if (GlobalGOScript.instance == null)
            return;
        CityManager manager = GlobalGOScript.instance.gameManager.cityManager;

        Sprite icon = Resources.Load<Sprite>("UI/CityIcon/" + manager.currentCity.image_name);
        cityIcon.sprite = icon;
        cityCoords.text = manager.currentCity.posX.ToString() + "," + manager.currentCity.posY.ToString();
        citiesCount.text = "+" + (manager.myCities.Count - 1).ToString();
    }

    public void openOrClose()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        if(isOpened)
        {
            rectTransform.anchoredPosition += Vector2.right * 157f;
            RightButtonsTransform.offsetMin = new Vector2(-10f, 0);
            RightButtonsTransform.offsetMax = new Vector2(-10f, 0);
            isOpened = false;
            return;
        }
        else
        {
            rectTransform.anchoredPosition = rectTransform.anchoredPosition.y * Vector2.up;
            RightButtonsTransform.offsetMin = new Vector2(-167f, 0);
            RightButtonsTransform.offsetMax = new Vector2(-167f, 0);
            isOpened = true;
            return;
        }
    }
}
