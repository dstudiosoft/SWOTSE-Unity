﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class WorldMapTimer {
    public long id;
    public long worldMap;
    public string status;
    public long event_id;
    public DateTime start_time;
    public DateTime finish_time;
    public string type;
}