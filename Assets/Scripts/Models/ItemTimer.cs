﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ItemTimer {
    public long id;
    public long event_id;
    public string itemID;
    public DateTime start_time;
    public DateTime finish_time;
    public string name;
    public string type;
}