﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class BattleReportModel {
    public long id;
    public UserModel attackUser;
    public UserModel defenceUser;
    public MyCityModel attackersCity;
    public MyCityModel defendersCity;
    public ResourcesModel attackArmyResources;
    public ResourcesModel defenceArmyResources;
    public List<CityTreasureModel> stolenCityTreasures;
    public JSONObject attackArmyBeforeBattle;
    public JSONObject defenceArmyBeforeBattle;
    public JSONObject attackArmyAfterBattle;
    public JSONObject defenceArmyAfterBattle;
    #region scout
    public ResourcesModel defendersResources;
    public List<CityTreasureModel> defendersCityItems;
    #endregion
    public bool isSuccessAttack;
    public DateTime creation_date;
    public string attack_type;
    public string log;
}
