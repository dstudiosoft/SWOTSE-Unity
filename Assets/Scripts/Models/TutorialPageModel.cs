﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class TutorialPageModel {
    public long id;
    public string title;
    public long page;
    public string text;
    public string image;
}
