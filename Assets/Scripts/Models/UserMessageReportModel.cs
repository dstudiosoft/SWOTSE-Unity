﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class UserMessageReportModel {
    public long id;
    public UserModel fromUser;
    public string subject;
    public string message;
    public bool isRead;
    public DateTime creation_date;
}
