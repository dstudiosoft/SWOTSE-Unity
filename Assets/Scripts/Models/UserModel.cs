﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class UserModel {
    public long id;
    public string username;
    public string status;
    public string image_name;
    public string rank;
    public long emperor_id;
    public string email;
    public long experience;
    public string alliance_rank;
    public DateTime last_login;
    public long city_count;
    public AllianceModel alliance;
    public MyCityModel capital;
    public long shillings;
    public string language;
    public string flag;
    public long players_wins;
    public long players_losses;
    public long barbarians_wins;
    public long barbarians_losses;
}