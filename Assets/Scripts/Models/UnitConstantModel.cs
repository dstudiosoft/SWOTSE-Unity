﻿using System;

[Serializable]
public class UnitConstantModel {
    public string name;
    public string unit_name;
    public long food_price;
    public long wood_price;
    public long stone_price;
    public long iron_price;
    public long cooper_price;
    public long gold_price;
    public long silver_price;
    public long time_seconds;
    public long population_price;
    public long speed;
    public long range;
    public long attack;
    public long defence;
    public long life;
    public long capacity;
    public string description;
}
