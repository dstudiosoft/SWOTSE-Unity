﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class AllianceModel {
    public long id;
    public string alliance_name;
    public long founderId;
    public string founderName;
    public long leaderId;
    public string leaderName;
    public List<long> allies;
    public List<long> enemies;
    public long rank;
    public long members_count;
    public long alliance_experience;
    public string guideline;
}
