﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ColonyModel {
    public long id;
    public string city_name;
    public string username;
    public long posX;
    public long posY;
    public string region;
    public long mayer_residence_level;
    public ResourcesModel production;
}
