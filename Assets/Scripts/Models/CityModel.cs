﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class CityModel {
    public long id;
    public long owner;
    public string city_name;
    public string image_name;
    public string occupantCityOwnerName;
    public string occupantCityName;
}