﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ResourcesModel {
    public long city;
    public long food;
    public long wood;
    public long stone;
    public long iron;
    public long cooper;
    public long gold;
    public long silver;
    public long population;
    public DateTime last_update;
}
