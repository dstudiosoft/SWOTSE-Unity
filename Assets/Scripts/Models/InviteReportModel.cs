﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class InviteReportModel {
    public long id;
    public long inviteFromId;
    public string inviteFromName;
    public long inviteToId;
    public string inviteToName;
    public bool isRead;
    public long allianceId;
    public string allianceName;
    public long allianceRank;
    public string type;
    public DateTime creationDate;
}
