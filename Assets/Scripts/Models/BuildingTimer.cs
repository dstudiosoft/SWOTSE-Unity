﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class BuildingTimer {
    public long id;
    public BuildingModel building;
    public string status;
    public long event_id;
    public DateTime start_time;
    public DateTime finish_time;
}