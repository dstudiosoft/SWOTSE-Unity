﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class FinanceReportModel
{
    public string id;
    public long user;
    public long city;
    public ShopItemModel item_constant;
    public long count;
    public DateTime date;
    public long price;
    public string type;
    public string fromUser;
}