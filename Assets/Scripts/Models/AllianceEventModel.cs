﻿using System;

[Serializable]
public class AllianceEventModel {
	public long id;
	public long alliance;
	public string message;
	public DateTime date;
}
