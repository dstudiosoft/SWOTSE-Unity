﻿using System;

[Serializable]
public class BattleReportHeader {
    public long id;
    public string attackUserName;
    public string defenceUserName;
    public bool isSuccessAttack;
    public DateTime creation_date;
    public bool isReadByAttacker;
    public bool isReadByDefender;
    public string attack_type;
    public string defenceCityName;
    public string attackCityName;
}