﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class ArmyGeneralModel {
    public long id;
    public string name;
    public long cityID;
    public long politics;
    public long marches;
    public long speed;
    public long loyalty;
    public long level;
    public long salary;
    public long experience;
    public long number_of_battles;
    public string status;
}
