﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class MarketLotModel {
    public long id;
    public long owner;
    public long city;
    public string resource_type;
    public long count;
    public long price;
}
