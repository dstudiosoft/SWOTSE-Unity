﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class MyCityModel {
    public long id;
    public string city_name;
    public long posX;
    public long posY;
    public string region;
    public string image_name;
    public long max_population;
    public long current_population;
    public long happiness;
    public long courage;
    public bool isExpantion;
    public bool reinforce_flag;
    public long gold_tax_rate;
    public long fields_count;
    public long valleys_count;
    public long mayer_residence_level;
}
