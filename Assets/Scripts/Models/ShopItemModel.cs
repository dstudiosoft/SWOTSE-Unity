﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ShopItemModel {
    public string id;
    public string name;
    public string rank;
    public string description;
    public long price_sh;
    public string type;
}
