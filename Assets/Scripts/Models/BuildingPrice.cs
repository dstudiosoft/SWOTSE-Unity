﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class BuildingPrice {
    public string building;
    public long level;
    public long food_price;
    public long wood_price;
    public long stone_price;
    public long iron_price;
    public long cooper_price;
    public long gold_price;
    public long silver_price;
    public long time_seconds;
    public long population_price;
}
