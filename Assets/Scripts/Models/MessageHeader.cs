﻿using System;

[Serializable]
public class MessageHeader {
    public long id;
    public string fromUserName;
    public string subject;
    public bool isRead;
    public DateTime creation_date;
}
