﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class CityTreasureModel {
    public string id;
    public long city;
    public ShopItemModel item_constant;
    public long count;
    public long price;
}
