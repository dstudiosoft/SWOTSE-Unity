﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class BuildingModel {
    public long id;
    public string building;
    public long level;
    public long pit_id;
    public long city;
    public string status;
    public long population;
    public string location;
}
