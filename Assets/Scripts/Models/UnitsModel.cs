﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class UnitsModel {
    public long city;
    public long workers_count;
    public long spy_count;
    public long swards_man_count;
    public long spear_man_count;
    public long pike_man_count;
    public long scout_rider_count;
    public long light_cavalry_count;
    public long heavy_cavalry_count;
    public long archer_count;
    public long archer_rider_count;
    public long holly_man_count;
    public long wagon_count;
    public long trebuchet_count;
    public long siege_towers_count;
    public long battering_ram_count;
    public long ballista_count;
    public string army_status;
}
