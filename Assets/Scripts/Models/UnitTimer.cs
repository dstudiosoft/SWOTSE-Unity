﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class UnitTimer {
    public long id;
    public string unit;
    public long event_id;
    public DateTime start_time;
    public DateTime finish_time;
}