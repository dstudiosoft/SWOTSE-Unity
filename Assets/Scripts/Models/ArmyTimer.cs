﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ArmyTimer {
    public long id;
    public long army;
    public long event_id;
    public DateTime start_time;
    public DateTime finish_time;
    public string army_status;
    public bool enemy;
    public string targetType;
    public string old_army_status;
}