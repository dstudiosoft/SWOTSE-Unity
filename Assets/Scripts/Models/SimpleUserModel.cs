﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class SimpleUserModel {
    public long id;
    public string username;
    public string rank;
    public long emperor_id;
    public string status;
    public long experience;
    public string alliance_name;
    public long allianceID;
    public long capitalID;
    public string image_name;
    public string flag;
}
