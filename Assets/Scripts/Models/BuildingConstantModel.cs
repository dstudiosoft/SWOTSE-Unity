﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class BuildingConstantModel {
    public string id;
    public string building_name;
    public string description;
    public ArrayList building_prices;
}
