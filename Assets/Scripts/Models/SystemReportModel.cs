﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class SystemReportModel {
    public long id;
    public string subject;
    public string message;
    public bool isRead;
    public DateTime creation_date;
}
