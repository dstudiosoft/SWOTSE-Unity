﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class WorldFieldModel {
    public long id;
    public long posX;
    public long posY;
    public string cityType;
    public CityModel city;
    public SimpleUserModel owner;
    public long level;
    public long production;
    public string region;
}
