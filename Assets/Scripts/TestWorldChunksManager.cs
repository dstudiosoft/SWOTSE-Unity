﻿using UnityEngine;
using System.Collections;

public class TestWorldChunksManager : MonoBehaviour {

    public GameObject[,] worldChunks;

    public long targetX;
    public long targetY;

    public void GoToCoords(long x, long y)
    {
        targetX = x;
        targetY = y;
        UpdateWorldMap();
    }

    public void InitWorldMap()
    {
        worldChunks = new GameObject[3, 3];

        worldChunks[1, 1] = transform.FindChild("Chunk (0)").gameObject;
        worldChunks[0, 1] = transform.FindChild("Chunk (1)").gameObject;
        worldChunks[0, 2] = transform.FindChild("Chunk (2)").gameObject;
        worldChunks[1, 2] = transform.FindChild("Chunk (3)").gameObject;
        worldChunks[2, 2] = transform.FindChild("Chunk (4)").gameObject;
        worldChunks[2, 1] = transform.FindChild("Chunk (5)").gameObject;
        worldChunks[2, 0] = transform.FindChild("Chunk (6)").gameObject;
        worldChunks[1, 0] = transform.FindChild("Chunk (7)").gameObject;
        worldChunks[0, 0] = transform.FindChild("Chunk (8)").gameObject;

        WorldChunkManager centerManager = worldChunks[1, 1].GetComponent<WorldChunkManager>();
        centerManager.startX = (targetX - targetX % 10) % 800;
        centerManager.startY = (targetY - targetY % 10) % 800;

        if (centerManager.startX < 0)
        {
            centerManager.startX = 800 + centerManager.startX;
        }

        if (centerManager.startY < 0)
        {
            centerManager.startY = 800 + centerManager.startY;
        }

        int xOffset;
        int yOffset;
        WorldChunkManager manager;       

        for (int y = 0; y < 3; y++)
        {
            for (int x = 0; x < 3; x++)
            {
                yOffset = -1 + y;
                xOffset = x - 1;

                manager = worldChunks[y, x].GetComponent<WorldChunkManager>();
                manager.startX = (centerManager.startX + (xOffset * 10)) % 800;
                manager.startY = (centerManager.startY + (yOffset * 10)) % 800;

                if (manager.startX < 0)
                {
                    manager.startX = 800 + manager.startX;
                }

                if (manager.startY < 0)
                {
                    manager.startY = 800 + manager.startY;
                }

                manager.ResetFieldsState();
            }
        }

        long pitId = targetX % 10 + (targetY % 10) * 10;

        for (int i = 0; i < centerManager.transform.childCount; i++)
        {
            if (centerManager.transform.GetChild(i).GetComponent<WorldFieldManager>().internalPitId == pitId)
            {
                Vector3 disposition = transform.position - centerManager.transform.GetChild(i).position;
                transform.Translate(disposition);
            }
        }
    }

    public void UpdateWorldMap()
    {
        transform.position = new Vector3(0.0f, 0.0f, 0.0f);
        WorldChunkManager centerManager = worldChunks[1, 1].GetComponent<WorldChunkManager>();
        centerManager.startX = (targetX - targetX % 10) % 800;
        centerManager.startY = (targetY - targetY % 10) % 800;

        if (centerManager.startX < 0)
        {
            centerManager.startX = 800 + centerManager.startX;
        }

        if (centerManager.startY < 0)
        {
            centerManager.startY = 800 + centerManager.startY;
        }

        int xOffset;
        int yOffset;
        WorldChunkManager manager;       

        for (int y = 0; y < 3; y++)
        {
            for (int x = 0; x < 3; x++)
            {
                yOffset = -1 + y;
                xOffset = x - 1;

                manager = worldChunks[y, x].GetComponent<WorldChunkManager>();
                manager.startX = (centerManager.startX + (xOffset * 10)) % 800;
                manager.startY = (centerManager.startY + (yOffset * 10)) % 800;

                if (manager.startX < 0)
                {
                    manager.startX = 800 + manager.startX;
                }

                if (manager.startY < 0)
                {
                    manager.startY = 800 + manager.startY;
                }

                manager.ResetFieldsState();
            }
        }

        long pitId = targetX % 10 + (targetY % 10) * 10;

        for (int i = 0; i < centerManager.transform.childCount; i++)
        {
            if (centerManager.transform.GetChild(i).GetComponent<WorldFieldManager>().internalPitId == pitId)
            {
                Vector3 disposition = transform.position - centerManager.transform.GetChild(i).position;
                transform.Translate(disposition);
            }
        }
    }

    void UpdateChunkCoords()
    {
        Vector3 newCoords;

        int xOffset;
        int yOffset;

        for (int y = 0; y < 3; y++)
        {
            for (int x = 0; x < 3; x++)
            {
                xOffset = x - 1;
                yOffset = 1 - y;

                newCoords = new Vector3(worldChunks[1, 1].transform.position.x + (xOffset * 25.0f), worldChunks[1, 1].transform.position.y + (yOffset * 15.0f), 0.0f);
                worldChunks[y, x].transform.position = newCoords;
            }
        }
    }

    void RightTransform()
    {
        GameObject temp;

        for (int x = 0; x < 2; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                temp = worldChunks[y, x];
                worldChunks[y, x] = worldChunks[y, x + 1];
                worldChunks[y, x + 1] = temp;
            }
        }

        int yOffset;
        WorldChunkManager center = worldChunks[1, 1].GetComponent<WorldChunkManager>();

        for (int y = 0; y < 3; y++)
        {
            yOffset = -1 + y;
            WorldChunkManager manager = worldChunks[y, 2].GetComponent<WorldChunkManager>();
            manager.startX = (center.startX + 10) % 800;
            manager.startY = (center.startY + (10 * yOffset)) % 800;

            if (manager.startX < 0)
            {
                manager.startX = 800 + manager.startX;
            }

            if (manager.startY < 0)
            {
                manager.startY = 800 + manager.startY;
            }

            manager.ResetFieldsState();

        }
    }

    void LeftTransform()
    {
        GameObject temp;

        for (int x = 2; x > 0; x--)
        {
            for (int y = 0; y < 3; y++)
            {
                temp = worldChunks[y, x];
                worldChunks[y, x] = worldChunks[y, x - 1];
                worldChunks[y, x - 1] = temp;
            }
        }

        int yOffset;
        WorldChunkManager center = worldChunks[1, 1].GetComponent<WorldChunkManager>();

        for (int y = 0; y < 3; y++)
        {
            yOffset = -1 + y;
            WorldChunkManager manager = worldChunks[y, 0].GetComponent<WorldChunkManager>();
            manager.startX = (center.startX - 10) % 800;
            manager.startY = (center.startY + (10 * yOffset)) % 800;

            if (manager.startX < 0)
            {
                manager.startX = 800 + manager.startX;
            }

            if (manager.startY < 0)
            {
                manager.startY = 800 + manager.startY;
            }

            manager.ResetFieldsState();
        }
    }

    void UpTransform()
    {
        GameObject temp;

        for (int y = 2; y > 0; y--)
        {
            for (int x = 0; x < 3; x++)
            {
                temp = worldChunks[y, x];
                worldChunks[y, x] = worldChunks[y - 1, x];
                worldChunks[y - 1, x] = temp;
            }
        }

        int xOffset;
        WorldChunkManager center = worldChunks[1, 1].GetComponent<WorldChunkManager>();

        for (int x = 0; x < 3; x++)
        {
            xOffset = x - 1;
            WorldChunkManager manager = worldChunks[0, x].GetComponent<WorldChunkManager>();
            manager.startX = (center.startX + (10 * xOffset)) % 800;
            manager.startY = (center.startY - 10) % 800;

            if (manager.startX < 0)
            {
                manager.startX = 800 + manager.startX;
            }

            if (manager.startY < 0)
            {
                manager.startY = 800 + manager.startY;
            }

            manager.ResetFieldsState();
        }
    }

    void DownTransform()
    {
        GameObject temp;

        for (int y = 0; y < 2; y++)
        {
            for (int x = 0; x < 3; x++)
            {
                temp = worldChunks[y, x];
                worldChunks[y, x] = worldChunks[y + 1, x];
                worldChunks[y + 1, x] = temp;
            }
        }

        int xOffset;
        WorldChunkManager center = worldChunks[1, 1].GetComponent<WorldChunkManager>();

        for (int x = 0; x < 3; x++)
        {
            xOffset = x - 1;
            WorldChunkManager manager = worldChunks[2, x].GetComponent<WorldChunkManager>();
            manager.startX = (center.startX + (10 * xOffset)) % 800;
            manager.startY = (center.startY + 10) % 800;

            if (manager.startX < 0)
            {
                manager.startX = 800 + manager.startX;
            }

            if (manager.startY < 0)
            {
                manager.startY = 800 + manager.startY;
            }

            manager.ResetFieldsState();
        }
    }

    void LowerRightTransform()
    {
        GameObject temp;

        for (int y = 0; y < 2; y++)
        {
            for (int x = 0; x < 2; x++)
            {
                temp = worldChunks[y, x];
                worldChunks[y, x] = worldChunks[y + 1, x + 1];
                worldChunks[y + 1, x + 1] = temp;
            }
        }

        int xOffset;
        int yOffset;
        WorldChunkManager center = worldChunks[1, 1].GetComponent<WorldChunkManager>();

        //update y
        for (int y = 0; y < 3; y++)
        {
            yOffset = -1 + y;
            WorldChunkManager manager = worldChunks[y, 2].GetComponent<WorldChunkManager>();
            manager.startX = (center.startX + 10) % 800;
            manager.startY = (center.startY + (10 * yOffset)) % 800;

            if (manager.startX < 0)
            {
                manager.startX = 800 + manager.startX;
            }

            if (manager.startY < 0)
            {
                manager.startY = 800 + manager.startY;
            }

            manager.ResetFieldsState();
        }

        //update x
        for (int x = 0; x < 2; x++)
        {
            xOffset = x - 1;
            WorldChunkManager manager = worldChunks[2, x].GetComponent<WorldChunkManager>();
            manager.startX = (center.startX + (10 * xOffset)) % 800;
            manager.startY = (center.startY + 10) % 800;

            if (manager.startX < 0)
            {
                manager.startX = 800 + manager.startX;
            }

            if (manager.startY < 0)
            {
                manager.startY = 800 + manager.startY;
            }

            manager.ResetFieldsState();
        }
    }

    void LowerLeftTransform()
    {
        GameObject temp;

        for (int y = 0; y < 2; y++)
        {
            for (int x = 2; x > 0; x--)
            {
                temp = worldChunks[y, x];
                worldChunks[y, x] = worldChunks[y + 1, x - 1];
                worldChunks[y + 1, x - 1] = temp;
            }
        }

        int xOffset;
        int yOffset;
        WorldChunkManager center = worldChunks[1, 1].GetComponent<WorldChunkManager>();

        //update y
        for (int y = 0; y < 3; y++)
        {
            yOffset = -1 + y;
            WorldChunkManager manager = worldChunks[y, 0].GetComponent<WorldChunkManager>();
            manager.startX = (center.startX - 10) % 800;
            manager.startY = (center.startY + (10 * yOffset)) % 800;

            if (manager.startX < 0)
            {
                manager.startX = 800 + manager.startX;
            }

            if (manager.startY < 0)
            {
                manager.startY = 800 + manager.startY;
            }

            manager.ResetFieldsState();
        }

        //update x
        for (int x = 2; x > 0; x--)
        {
            xOffset = x - 1;
            WorldChunkManager manager = worldChunks[2, x].GetComponent<WorldChunkManager>();
            manager.startX = (center.startX + (10 * xOffset)) % 800;
            manager.startY = (center.startY + 10) % 800;

            if (manager.startX < 0)
            {
                manager.startX = 800 + manager.startX;
            }

            if (manager.startY < 0)
            {
                manager.startY = 800 + manager.startY;
            }

            manager.ResetFieldsState();
        }
    }

    void UpperRightTransform()
    {
        GameObject temp;

        for (int y = 2; y > 0; y--)
        {
            for (int x = 0; x < 2; x++)
            {
                temp = worldChunks[y, x];
                worldChunks[y, x] = worldChunks[y - 1, x + 1];
                worldChunks[y - 1, x + 1] = temp;
            }
        }

        int xOffset;
        int yOffset;
        WorldChunkManager center = worldChunks[1, 1].GetComponent<WorldChunkManager>();


        //update y
        for (int y = 0; y < 3; y++)
        {
            yOffset = -1 + y;
            WorldChunkManager manager = worldChunks[y, 2].GetComponent<WorldChunkManager>();
            manager.startX = (center.startX + 10) % 800;
            manager.startY = (center.startY + (10 * yOffset)) % 800;

            if (manager.startX < 0)
            {
                manager.startX = 800 + manager.startX;
            }

            if (manager.startY < 0)
            {
                manager.startY = 800 + manager.startY;
            }

            manager.ResetFieldsState();
        }

        //update x
        for (int x = 0; x < 2; x++)
        {
            xOffset = x - 1;
            WorldChunkManager manager = worldChunks[0, x].GetComponent<WorldChunkManager>();
            manager.startX = (center.startX + (10 * xOffset)) % 800;
            manager.startY = (center.startY - 10) % 800;
            manager.ResetFieldsState();
        }
    }

    void UpperLeftTransform()
    {
        GameObject temp;

        for (int y = 2; y > 0; y--)
        {
            for (int x = 2; x > 0; x--)
            {
                temp = worldChunks[y, x];
                worldChunks[y, x] = worldChunks[y - 1, x - 1];
                worldChunks[y - 1, x - 1] = temp;
            }
        }

        int xOffset;
        int yOffset;
        WorldChunkManager center = worldChunks[1, 1].GetComponent<WorldChunkManager>();

        //update y
        for (int y = 0; y < 3; y++)
        {
            yOffset = -1 + y;
            WorldChunkManager manager = worldChunks[y, 0].GetComponent<WorldChunkManager>();
            manager.startX = (center.startX - 10) % 800;
            manager.startY = (center.startY + (10 * yOffset)) % 800;

            if (manager.startX < 0)
            {
                manager.startX = 800 + manager.startX;
            }

            if (manager.startY < 0)
            {
                manager.startY = 800 + manager.startY;
            }

            manager.ResetFieldsState();
        }

        //update x
        for (int x = 2; x > 0; x--)
        {
            xOffset = x - 1;
            WorldChunkManager manager = worldChunks[0, x].GetComponent<WorldChunkManager>();
            manager.startX = (center.startX + (10 * xOffset)) % 800;
            manager.startY = (center.startY - 10) % 800;

            if (manager.startX < 0)
            {
                manager.startX = 800 + manager.startX;
            }

            if (manager.startY < 0)
            {
                manager.startY = 800 + manager.startY;
            }

            manager.ResetFieldsState();
        }
    }

    public void CheckChunkGrid()
    {
        RaycastHit2D hit;
        hit = Physics2D.Raycast(Vector2.zero, Vector2.zero);

        if (hit.collider != null)
        {
            GameObject centerChunck;

            if (string.Equals(hit.transform.tag, "WorldChunk"))
            {
                centerChunck = hit.collider.gameObject;
            }
            else
            {
                centerChunck = hit.transform.parent.gameObject;
            }
                
            if (worldChunks[1, 1] != centerChunck)
            {
                //begin transformations
                if (centerChunck.transform.position.x > worldChunks[1, 1].transform.position.x)
                {
                    if (centerChunck.transform.position.y > worldChunks[1, 1].transform.position.y)
                    {
                        // upper right
                        UpperRightTransform();
                    }
                    else if (centerChunck.transform.position.y == worldChunks[1, 1].transform.position.y)
                    {
                        // right
                        RightTransform();
                    }
                    else
                    {
                        // lower right
                        LowerRightTransform();
                    }
                }
                else if (centerChunck.transform.position.x == worldChunks[1, 1].transform.position.x)
                {
                    if (centerChunck.transform.position.y > worldChunks[1, 1].transform.position.y)
                    {
                        // up
                        UpTransform();
                    }
                    else if (centerChunck.transform.position.y == worldChunks[1, 1].transform.position.y)
                    {
                        // same chunk, can't happen
                        return;
                    }
                    else
                    {
                        // down
                        DownTransform();
                    }
                }
                else
                {
                    if (centerChunck.transform.position.y > worldChunks[1, 1].transform.position.y)
                    {
                        // upper left
                        UpperLeftTransform();
                    }
                    else if (centerChunck.transform.position.y == worldChunks[1, 1].transform.position.y)
                    {
                        // left
                        LeftTransform();
                    }
                    else
                    {
                        // lower left
                        LowerLeftTransform();
                    }
                }

                UpdateChunkCoords();
            
            }
        }
    }
}
