﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;
    public AudioSource music;

	void Start ()
    {
        instance = this;
	}
	
	public void switchMusic (bool on)
    {
        if (on)
            music.Play();
        else
            music.Pause();
	}
}
