﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MapCityInfoManager : MonoBehaviour {

    public Text rank;
    public Text experience;
    public Text region;
    public Text coords;
    public Text alliance;
    public Text status;

    public Text cityName;
    public Text playerName;
    public Text occupationLabel;

    public GameObject marchBtn;
    public GameObject revoltBtn;
    public GameObject nominateBtn;
    public GameObject shopBtn;
    public GameObject giftBtn;

    public Image playerImage;
    public Image cityImage;

    public WorldFieldModel field;

    void OnEnable()
    {
        coords.text = field.posX.ToString() + ", " + field.posY.ToString();
        cityName.text = field.city.city_name.ToUpper();
        Sprite citySprite = Resources.Load<Sprite>("UI/CityIcon/" + field.city.image_name);
        cityImage.sprite = citySprite;
        playerName.text = field.owner.username.ToUpper();
        Sprite playerSprite = Resources.Load<Sprite>("UI/UserIcon/" + field.owner.image_name);
        playerImage.sprite = playerSprite;
        if (!string.IsNullOrEmpty(field.city.occupantCityOwnerName))
        {
            occupationLabel.text = occupationLabel.text.Replace("#USERNAME#", field.city.occupantCityOwnerName.ToUpper());
            occupationLabel.gameObject.SetActive(true);
        }
        if (!string.IsNullOrEmpty(field.owner.alliance_name))
        {
            alliance.text = field.owner.alliance_name.ToUpper();
        }
        else
        {
            alliance.text = "None";
        }
        rank.text = field.owner.rank;
        experience.text = field.owner.experience.ToString();
        status.text = field.owner.status;
        region.text = field.region;

        if (field.owner == null)
        {
            marchBtn.SetActive(true);
        }
        else
        {
            if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.IsBuildingPresent("Command_Center"))
            {
                if (GlobalGOScript.instance.playerManager.allianceManager.allianceMembers != null)
                {
                    if (!GlobalGOScript.instance.playerManager.allianceManager.allianceMembers.ContainsKey(field.owner.id))
                    {
                        marchBtn.SetActive(true);
                    }
                }
                else
                {
                    if (GlobalGOScript.instance.playerManager.user.id != field.owner.id)
                    {
                        marchBtn.SetActive(true);
                        shopBtn.SetActive(true);
                    }
                    else
                    {
                        if (field.city.occupantCityOwnerName != null)
                        {
                            revoltBtn.gameObject.SetActive(true);
                        }
                    }
                }
            }

            if (string.Equals(GlobalGOScript.instance.playerManager.user.rank, "Grand Prince") && GlobalGOScript.instance.playerManager.user.id != field.owner.id)
            {
                nominateBtn.SetActive(true);
            }

            if (GlobalGOScript.instance.playerManager.user.id != field.owner.id)
            {
                shopBtn.SetActive(true);
                giftBtn.SetActive(true);
            }
        }
    }

    public void Revolt()
    {
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.StartRevolution(field.city.id));
    }

    public void Nominate()
    {
        StartCoroutine(GlobalGOScript.instance.playerManager.NominateUser(field.owner.id));
    }

    public void March()
    {
        GlobalGOScript.instance.windowInstanceManager.openDispatchWindow(field, true);
        GlobalGOScript.instance.windowInstanceManager.closeCityInfo();
    }

    public void OpenPlayerShop()
    {
        GlobalGOScript.instance.windowInstanceManager.openPlayerShop(field.city.id, true);
    }

    public void OpenGiftItem()
    {
        GlobalGOScript.instance.windowInstanceManager.openPlayerShop(field.city.id, false);
    }
}
