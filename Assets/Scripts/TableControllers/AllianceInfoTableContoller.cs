﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;

public class AllianceInfoTableContoller : MonoBehaviour, ITableViewDataSource {

    public AllianceInfoAlliancesLine m_cellPrefab;
    public TableView m_tableView;

    private bool neutral = true;
    private bool ally = false;
    private bool enemy = false;

    private List<AllianceModel> neutralAlliances;
    private List<AllianceModel> allyAlliances;
    private List<AllianceModel> enemyAlliances;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start() {
        m_tableView.dataSource = this;
    }

    void OnEnable()
    {
        if (GlobalGOScript.instance.playerManager.user.alliance != null)
        {
            GlobalGOScript.instance.playerManager.allianceManager.onGetAlliances += GotAlliances;
            StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.AllAlliances());
        }
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {

        if (neutral)
        {
            if (neutralAlliances == null)
            {
                return 0;
            }
            else
            {
                return neutralAlliances.Count;
            }
        }
        else if (ally)
        {
            if (allyAlliances == null)
            {
                return 0;
            }
            else
            {
                return allyAlliances.Count;
            }
        }
        else
        {
            if (enemyAlliances == null)
            {
                return 0;
            }
            else
            {
                return enemyAlliances.Count;
            }
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        AllianceInfoAlliancesLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as AllianceInfoAlliancesLine;
        if (cell == null) {
            cell = (AllianceInfoAlliancesLine)GameObject.Instantiate(m_cellPrefab);
        }

        AllianceModel alliance = null;

        if (neutral)
        {
            alliance = neutralAlliances[row];
            cell.SetNeutral(true);
        }
        else if (ally)
        {
            alliance = allyAlliances[row];
            cell.SetNeutral(false);
        }
        else if (enemy)
        {
            alliance = enemyAlliances[row];
            cell.SetNeutral(false);
        }
            
        cell.SetAllianceName(alliance.alliance_name);
        cell.SetMembers(alliance.members_count);
        cell.SetRank(alliance.rank);
        cell.SetAllianceId(alliance.id);
        cell.SetOwner(this);

        return cell;
    }

    public void GotAlliances(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.allianceManager.onGetAlliances -= GotAlliances;

        if (string.Equals(value, "success"))
        {
            neutralAlliances = new List<AllianceModel>();
            allyAlliances = new List<AllianceModel>();
            enemyAlliances = new List<AllianceModel>();

            foreach (long key in GlobalGOScript.instance.playerManager.allianceManager.allAlliances.Keys)
            {
                if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance.allies.Contains(key))
                {
                    allyAlliances.Add(GlobalGOScript.instance.playerManager.allianceManager.allAlliances[key]);
                }
                else if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance.enemies.Contains(key))
                {
                    enemyAlliances.Add(GlobalGOScript.instance.playerManager.allianceManager.allAlliances[key]);
                }
                else
                {
                    if (key != GlobalGOScript.instance.playerManager.allianceManager.userAlliance.id)
                    {
                        neutralAlliances.Add(GlobalGOScript.instance.playerManager.allianceManager.allAlliances[key]);   
                    }
                }
            }

            m_tableView.ReloadData();
        }
    }

    public void ShowAlly()
    {
        ally = true;
        enemy = false;
        neutral = false;

        m_tableView.ReloadData();
    }

    public void ShowEnemy()
    {
        ally = false;
        enemy = true;
        neutral = false;

        m_tableView.ReloadData();
    }

    public void ShowNeutral()
    {
        ally = false;
        enemy = false;
        neutral = true;

        m_tableView.ReloadData();
    }

    public void MakeAlly(long id)
    {
        UserModel myUser = GlobalGOScript.instance.playerManager.user;

        if (!string.Equals(myUser.alliance_rank, "Soldier") && !string.Equals(myUser.alliance_rank, "Commander") && !string.Equals(myUser.alliance_rank, "Vice Host"))
        {
            GlobalGOScript.instance.playerManager.allianceManager.onAddAlly += MadeAlly;
            StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.AddAlly(id));
        }
    }

    public void MadeAlly(object sender, string value)
    {
        //done in the request method, need to move it here
        GlobalGOScript.instance.playerManager.allianceManager.onAddAlly -= MadeAlly;

        if (string.Equals(value, "success"))
        {
            neutralAlliances.Remove(GlobalGOScript.instance.playerManager.allianceManager.allAlliances[(long)sender]);
            allyAlliances.Add(GlobalGOScript.instance.playerManager.allianceManager.allAlliances[(long)sender]);
            m_tableView.ReloadData();
        }
    }

    public void MakeEnemy(long id)
    {
        UserModel myUser = GlobalGOScript.instance.playerManager.user;

        if (!string.Equals(myUser.alliance_rank, "Soldier") && !string.Equals(myUser.alliance_rank, "Commander"))
        {
            GlobalGOScript.instance.playerManager.allianceManager.onAddEnemy += MadeEnemy;
            StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.AddEnemy(id));
        }
    }

    public void MadeEnemy(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.allianceManager.onAddEnemy -= MadeEnemy;

        if (string.Equals(value, "success"))
        {
            neutralAlliances.Remove(GlobalGOScript.instance.playerManager.allianceManager.allAlliances[(long)sender]);
            enemyAlliances.Add(GlobalGOScript.instance.playerManager.allianceManager.allAlliances[(long)sender]);
            m_tableView.ReloadData();
        }
    }

    public void MakeNeutral(long id)
    {
        UserModel myUser = GlobalGOScript.instance.playerManager.user;

        if (ally)
        {
            if (!string.Equals(myUser.alliance_rank, "Soldier") && !string.Equals(myUser.alliance_rank, "Commander"))
            {
                GlobalGOScript.instance.playerManager.allianceManager.onRemoveAlly += MadeNeutral;
                StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.RemoveAlly(id));
            }
        }
        else if (enemy)
        {
            if (!string.Equals(myUser.alliance_rank, "Soldier") && !string.Equals(myUser.alliance_rank, "Commander"))
            {
                GlobalGOScript.instance.playerManager.allianceManager.onRemoveEnemy += MadeNeutral;
                StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.RemoveEnemy(id));
            }
        }
    }

    public void MadeNeutral(object sender, string value)
    {
        if (ally)
        {
            GlobalGOScript.instance.playerManager.allianceManager.onRemoveAlly -= MadeNeutral;
        }
        else if (enemy)
        {
            GlobalGOScript.instance.playerManager.allianceManager.onRemoveEnemy -= MadeNeutral;
        }

        if (string.Equals(value, "success"))
        {
            if (ally)
            {
                allyAlliances.Remove(GlobalGOScript.instance.playerManager.allianceManager.allAlliances[(long)sender]);
                neutralAlliances.Add(GlobalGOScript.instance.playerManager.allianceManager.allAlliances[(long)sender]);
                m_tableView.ReloadData();
            }
            else if (enemy)
            {
                enemyAlliances.Remove(GlobalGOScript.instance.playerManager.allianceManager.allAlliances[(long)sender]);
                neutralAlliances.Add(GlobalGOScript.instance.playerManager.allianceManager.allAlliances[(long)sender]);
                m_tableView.ReloadData();
            }
        }
    }

    #endregion
}