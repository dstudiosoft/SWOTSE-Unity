﻿using UnityEngine;
using System.Collections;
using Tacticsoft;

public class ResidenceCitiesTableController : MonoBehaviour, ITableViewDataSource {

    public ResidenceCitiesTableLine m_cellPrefab;
    public TableView m_tableView;
    private ArrayList citiesList;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void OnEnable() {
        m_tableView.dataSource = this;
        citiesList = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.myCities.Count);
        foreach (MyCityModel city in GlobalGOScript.instance.gameManager.cityManager.myCities.Values)
        {
            citiesList.Add(city);
        }
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (citiesList == null)
        {
            return 0;
        }
        else
        {
            return citiesList.Count;
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        ResidenceCitiesTableLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as ResidenceCitiesTableLine;
        if (cell == null) {
            cell = (ResidenceCitiesTableLine)GameObject.Instantiate(m_cellPrefab);
        }
            
        MyCityModel city = (MyCityModel)citiesList[row];
        cell.SetOwner(this);
        cell.SetCityName(city.city_name);
        cell.SetCoords("(" + city.posX.ToString() + ", " + city.posY.ToString() + ")");
        cell.SetFieldsCount(city.fields_count);
        cell.SetMayerLevel(city.mayer_residence_level);
        cell.SetRegion(TextLocalizer.LocalizeString(city.region));
        cell.SetValleysCount(city.valleys_count);

        return cell;
    }

    public void OpenBuildingWindow(long pitId)
    {
        //        if (buildings)
        //        {
        //            GlobalGOScript.instance.windowInstanceManager.openBuildingInformation(GlobalGOScript.ins)
        //        }
        //        else
        //        {
        //        }
    }

    #endregion
}
