﻿using UnityEngine;
using System.Collections;
using Tacticsoft;
using SmartLocalization;

public class AllianceMembersPermisionsTableController : MonoBehaviour, ITableViewDataSource {

    public AllianceMembersPermissionsLine m_cellPrefab;
    public TableView m_tableView;
    private UserModel[] allianceMembers;

    public int m_numRows;

    private 

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start() {
        m_tableView.dataSource = this;
    }

    void OnEnable()
    {
        if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance != null)
        {
            GlobalGOScript.instance.playerManager.allianceManager.onGetAllianceMembers += GotAllianceMembers;
            StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.AllianceMemebers());
        }
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (allianceMembers == null)
        {
            return 0;
        }
        else
        {
            return allianceMembers.GetLength(0);
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        AllianceMembersPermissionsLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as AllianceMembersPermissionsLine;
        if (cell == null) {
            cell = (AllianceMembersPermissionsLine)GameObject.Instantiate(m_cellPrefab);
        }
            
        UserModel member = allianceMembers[row];
        cell.SetUser(member);
		cell.SetRank(member.rank);
        cell.SetPosition(TextLocalizer.LocalizeString(member.alliance_rank));
        cell.SetMemberName(member.username);
        cell.SetExperience(member.experience);
        cell.SetCitiesCount(member.city_count);
        cell.SetCapitalName(member.capital.city_name);
        cell.SetLastOnline(member.last_login.ToString());

        return cell;
    }

    public void GotAllianceMembers(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.allianceManager.onGetAllianceMembers -= GotAllianceMembers;

        if (string.Equals(value, "success"))
        {
            allianceMembers = new UserModel[GlobalGOScript.instance.playerManager.allianceManager.allianceMembers.Count];
            GlobalGOScript.instance.playerManager.allianceManager.allianceMembers.Values.CopyTo(allianceMembers, 0);

            m_tableView.ReloadData();
        }
    }

    #endregion
}
