﻿using UnityEngine;
using System.Collections;
using Tacticsoft;
using SmartLocalization;

public class ResidenceValleysTableController : MonoBehaviour, ITableViewDataSource {

    public ResidenceValleysTableLine m_cellPrefab;
    public TableView m_tableView;
    private ArrayList buildingsList;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void OnEnable() {
        m_tableView.dataSource = this;
        buildingsList = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityValleys.Count);
        foreach (WorldFieldModel building in GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityValleys.Values)
        {
            buildingsList.Add(building);
        }
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (buildingsList == null)
        {
            return 0;
        }
        else
        {
            return buildingsList.Count;
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        ResidenceValleysTableLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as ResidenceValleysTableLine;
        if (cell == null) {
            cell = (ResidenceValleysTableLine)GameObject.Instantiate(m_cellPrefab);
        }

        WorldFieldModel valley = (WorldFieldModel)buildingsList[row];
        string localizedValue = TextLocalizer.LocalizeString(valley.cityType);
        if (string.IsNullOrEmpty(localizedValue))
        {
            cell.SetValleyName(valley.cityType.Replace('_', ' '));
        }
        else
        {
            cell.SetValleyName(localizedValue);
        }
		cell.setWorldFieldModel (valley);
        cell.SetLevel(valley.level);
        cell.SetOwner(this);
        cell.SetCoords("(" + valley.posX + "," + valley.posY + ")");
        
        switch (valley.cityType)
        {
            case "Mountin_Cooper":
                cell.SetResource(TextLocalizer.LocalizeString("Copper"));
                break;
            case "Mountin_Iron":
                cell.SetResource(TextLocalizer.LocalizeString("Iron"));
                break;
            case "Hill":
                cell.SetResource(TextLocalizer.LocalizeString("Stone"));
                break;
            case "Lake":
                cell.SetResource(TextLocalizer.LocalizeString("Food"));
                break;
            case "Forest":
                cell.SetResource(TextLocalizer.LocalizeString("Wood"));
                break;
            default:
                break;
        }

        cell.SetRegion(TextLocalizer.LocalizeString(valley.region));
        cell.SetProduction(valley.production);

        return cell;
    }

    public void OpenBuildingWindow(long pitId)
    {
        //        if (buildings)
        //        {
        //            GlobalGOScript.instance.windowInstanceManager.openBuildingInformation(GlobalGOScript.ins)
        //        }
        //        else
        //        {
        //        }
    }

    #endregion
}
