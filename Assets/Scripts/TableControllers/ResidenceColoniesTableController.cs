﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using Tacticsoft;
using SmartLocalization;

public class ResidenceColoniesTableController : MonoBehaviour, ITableViewDataSource {

    public ResidenceColoniesTableLine m_cellPrefab;
    public TableView m_tableView;
    private ArrayList citiesList;

    private SimpleEvent gotCityColonies;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void OnEnable() {
        m_tableView.dataSource = this;
        gotCityColonies += OnGetColonies;
        StartCoroutine(GetCityColonies());
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (citiesList == null)
        {
            return 0;
        }
        else
        {
            return citiesList.Count;
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        ResidenceColoniesTableLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as ResidenceColoniesTableLine;
        if (cell == null)
        {
            cell = (ResidenceColoniesTableLine)GameObject.Instantiate(m_cellPrefab);
        }

        ColonyModel city = (ColonyModel)citiesList[row];
        cell.SetOwner(this);
        cell.SetCityName(city.city_name);
        cell.SetCoords("(" + city.posX.ToString() + ", " + city.posY.ToString() + ")");
        cell.SetMayerLevel(city.mayer_residence_level);
        cell.SetRegion(city.region);
        cell.SetPlayerName(city.username);
        cell.SetResources(city.production);

        return cell;
    }

    #endregion

    public IEnumerator GetCityColonies()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://" + GlobalGOScript.instance.host + ":8889/cities/getColonies/" + GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString() + "/");
        request.SetRequestHeader("authorization", "Token " + GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            JSONObject response = JSONObject.Create(request.downloadHandler.text);

            if (response.IsString)
            {
                //fail
            }
            else
            {
                citiesList = new ArrayList(response.list.Count);

                foreach (JSONObject colony in response.list)
                {
                    ColonyModel temp = JsonUtility.FromJson<ColonyModel>(colony.Print());
                    temp.production = JsonUtility.FromJson<ResourcesModel>(colony["production"].Print());
                    citiesList.Add(temp);
                }
            }
        }
    }

    void OnGetColonies(object sender, string value)
    {
        gotCityColonies -= OnGetColonies;

        if (string.Equals(value, "success"))
        {
            m_tableView.ReloadData();
        }
    }
}
