﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using Tacticsoft;

public class CommandCenterArmiesTableController : MonoBehaviour, ITableViewDataSource {

    public CommandCenterArmyTableLine m_cellPrefab;
    public TableView m_tableView;
    private List<ArmyModel> cityArmies;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start() {
        m_tableView.dataSource = this;
        GlobalGOScript.instance.gameManager.battleManager.onGetCityArmies += GotArmies;
        StartCoroutine(GlobalGOScript.instance.gameManager.battleManager.GetCityArmies());
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (cityArmies == null)
        {
            return 0;
        }
        else
        {
            return cityArmies.Count;
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        CommandCenterArmyTableLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as CommandCenterArmyTableLine;
        if (cell == null) {
            cell = (CommandCenterArmyTableLine)GameObject.Instantiate(m_cellPrefab);
        }

        ArmyModel army = cityArmies[row];
        if (army.hero != null)
        {
            cell.SetKnightName(army.hero.name);
        }
        else
        {
            cell.SetKnightName(army.id.ToString());
        }
        string localizedValue = SmartLocalization.LanguageManager.Instance.GetTextValue("SWOTSELocalization." + army.army_status);
        if (SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
        {
            localizedValue = ArabicSupport.ArabicFixer.Fix(localizedValue);
        }
        cell.SetStatus(localizedValue);
        cell.SetArmyId(army.id);
        cell.SetOwner(this);

        return cell;
    }

    public void ReloadData(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.battleManager.onArmyReturnInitiated -= ReloadData;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.gameManager.battleManager.onGetCityArmies += GotArmies;
            StartCoroutine(GlobalGOScript.instance.gameManager.battleManager.GetCityArmies());
        }
    }

    public void GotArmies(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.battleManager.onGetCityArmies -= GotArmies;

        if (string.Equals(value, "success"))
        {
            cityArmies = new List<ArmyModel>(GlobalGOScript.instance.gameManager.battleManager.cityArmies.Values);
            m_tableView.ReloadData();
        }
    }

    #endregion
}
