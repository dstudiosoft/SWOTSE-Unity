﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;
using ArabicSupport;
using SmartLocalization;

public class GuestFeastingTableController : MonoBehaviour, ITableViewDataSource {

    public GuestHouseTableLine m_cellPrefab;
    public TableView m_tableView;

    public bool guest;
    private List<ArmyGeneralModel> generalsContainer;
    private ArmyGeneralModel requestedGeneral;
    private int displayedHeroesCount = 0;
    private event ConfirmationEvent confirmed;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start() {
        m_tableView.dataSource = this;

        if (guest)
        {
            StartCoroutine(GetAllAvailableGenerals());
            BuildingInformationManager manager = gameObject.GetComponentInParent<BuildingInformationManager>();
            displayedHeroesCount = (int)GlobalGOScript.instance.gameManager.cityManager.buildingsManager.BuildingsLevelSum("Guest_House");
        }
        else
        {
            if (GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals != null)
            {
                generalsContainer = new List<ArmyGeneralModel>();

                foreach (ArmyGeneralModel general in GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals.Values)
                {
                    if (string.Equals(general.status, "passive"))
                    {
                        generalsContainer.Add(general);
                    }
                }
                m_tableView.ReloadData();
            }
        }
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        
        if (generalsContainer == null)
        {
            return 0;
        }
        else
        {
            if (guest)
            {
                return displayedHeroesCount;
            }
            else
            {
                return generalsContainer.Count;
            }
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        GuestHouseTableLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as GuestHouseTableLine;
        string localizedText;
        if (cell == null) {
            cell = (GuestHouseTableLine)GameObject.Instantiate(m_cellPrefab);
        }

        ArmyGeneralModel general = generalsContainer[row];
        if (guest)
        {
            localizedText = LanguageManager.Instance.GetTextValue("SWOTSELocalization.Recruit");
            if (LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
            {
                localizedText = ArabicFixer.Fix(localizedText);
            }
            cell.SetActionText(localizedText);
            cell.SetPressable(false);
        }
        else
        {
            localizedText = LanguageManager.Instance.GetTextValue("SWOTSELocalization.Dismiss");
            if (LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
            {
                localizedText = ArabicFixer.Fix(localizedText);
            }
            cell.SetActionText(localizedText);
            cell.SetPressable(true);
        }
        cell.SetKnightName(general.name);
        cell.SetLevel(general.level);
        cell.SetLoyalty(general.loyalty);
        cell.SetMarches(general.marches);
        cell.SetPolitics(general.politics);
        cell.SetSpeed(general.speed);

        localizedText = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + general.status);
        if (LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
        {
            localizedText = ArabicFixer.Fix(localizedText);
        }
        cell.SetStatus(localizedText);
        cell.SetSalary(general.salary);

        cell.SetGeneralModel(general);
        cell.SetOwner(this);

        return cell;
    }

    public void PerformAction(ArmyGeneralModel general)
    {
        requestedGeneral = general;

        if (guest)
        {
            GlobalGOScript.instance.gameManager.cityManager.unitsManager.onGeneralHired += ActionPerformed;
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.HireGeneral(requestedGeneral));
        }
        else
        {
            string localizedValue = LanguageManager.Instance.GetTextValue("SWOTSELocalization.ConfirmKnightDismiss");

            if (LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
            {
                localizedValue = ArabicFixer.Fix(localizedValue);
            }

            confirmed += DismissConfrirmed;
            GlobalGOScript.instance.windowInstanceManager.openConfirmationWindow(localizedValue, confirmed);
        }
    }

    void DismissConfrirmed(bool value)
    {
        confirmed -= DismissConfrirmed;

        if (value)
        {
            GlobalGOScript.instance.gameManager.cityManager.unitsManager.onGeneralDismissed += ActionPerformed;
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.DismissGeneral(requestedGeneral));
        }

        GlobalGOScript.instance.windowInstanceManager.closeConfirmationWindow();
    }

    public void ActionPerformed(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.unitsManager.onGeneralHired -= ActionPerformed;
        GlobalGOScript.instance.gameManager.cityManager.unitsManager.onGeneralDismissed -= ActionPerformed;

        if (string.Equals(value, "success"))
        {
            if (guest)
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Knight hired");
//                GlobalGOScript.instance.gameManager.cityManager.unitsManager.onGetGenerals += UpdatePanelKnights;
                StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.UpdateCityGenerals());
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Knight dismissed");
                StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.UpdateCityGenerals());
            }

            if (string.Equals(value, "success"))
            {
                generalsContainer.Remove(requestedGeneral);
                requestedGeneral = null;
                m_tableView.ReloadData();
            }
        }
        else
        {
            if (guest)
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Knight not hired");
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Knight not dismissed");
            }
        }
    }

//    void UpdatePanelKnights(object sender, string value)
//    {
//        GlobalGOScript.instance.gameManager.cityManager.unitsManager.onGetGenerals -= UpdatePanelKnights;
//        if (string.Equals(value, "success"))
//        {
//            GlobalGOScript.instance.knightsTable.ReloadData();
//        }
//    }

    public IEnumerator GetAllAvailableGenerals()
    {
        WWWForm postforn = new WWWForm();
        postforn.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());
        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/heroes/getAllAvailableHeroes/", postforn);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                //failed
                Debug.Log(request.downloadHandler.text);
            }
            else
            {
                //success
                JSONObject generalsArray = JSONObject.Create(request.downloadHandler.text);

                if (generalsArray.list.Count > 0)
                {
                    generalsContainer = new List<ArmyGeneralModel>();

                    foreach (JSONObject general in generalsArray.list)
                    {
                        ArmyGeneralModel availableGeneral = JsonUtility.FromJson<ArmyGeneralModel>(general.Print());
                        generalsContainer.Add(availableGeneral);
                    }

                    m_tableView.ReloadData();
                }
            }    
        }
    }

    #endregion
}
