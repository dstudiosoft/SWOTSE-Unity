﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Globalization;
using Tacticsoft;

public class AllianceInviteTableController : MonoBehaviour, ITableViewDataSource {

    public AllianceInviteLine m_cellPrefab;
    public TableView m_tableView;
    private List<UserModel> allUsers;

    public int m_numRows;

    private event SimpleEvent onGetAllUsers;

    private string dateFormat = "yyyy-MM-ddTHH:mm:ss.ffffffZ";

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start() {
        m_tableView.dataSource = this;

        if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance != null)
        {
            onGetAllUsers += GotAllUsers;
            StartCoroutine(GetAllUsers());
        }
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (allUsers == null)
        {
            return 0;
        }
        else
        {
            return allUsers.Count;
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        AllianceInviteLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as AllianceInviteLine;
        if (cell == null) {
            cell = (AllianceInviteLine)GameObject.Instantiate(m_cellPrefab);
        }

        UserModel user = allUsers[row];
        cell.SetRank(user.rank);
        cell.SetMemberName(user.username);
        cell.SetLastOnline(user.last_login.ToString());
        cell.SetExperience(user.experience);
        cell.SetCities(user.city_count);
        cell.SetCapitalName(user.capital.city_name);
        cell.SetCapitalCoords(user.capital.posX, user.capital.posY);
        cell.SetAllianceName(user.alliance.alliance_name);
        cell.SetUserId(user);
        cell.SetOwner(this);

        return cell;
    }

    public IEnumerator GetAllUsers()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/authorization/allUsers/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                //failed
                if (onGetAllUsers != null)
                {
                    onGetAllUsers(this, "fail");
                }
            }
            else
            {
                //success
                JSONObject usersArray = JSONObject.Create(request.downloadHandler.text);

                allUsers = new List<UserModel>();

                foreach (JSONObject user in usersArray.list)
                {
                    UserModel player = JsonUtility.FromJson<UserModel>(user.Print());

                    if (!user["alliance"].IsNull)
                    {
                        AllianceModel userAlliance = JsonUtility.FromJson<AllianceModel>(user["alliance"].Print());
                        player.alliance = userAlliance;
                    }
                    else
                    {
                        player.alliance = new AllianceModel();
                        player.alliance.alliance_name = "none";
                    }

                    if (!user["last_login"].IsNull)
                    {
                        player.last_login = DateTime.ParseExact(user["last_login"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                    }

                    MyCityModel memberCity = JsonUtility.FromJson<MyCityModel>(user["capital"].Print());
                    player.capital = memberCity;

                    if (player.alliance.id != GlobalGOScript.instance.playerManager.allianceManager.userAlliance.id)
                    {
                        allUsers.Add(player);
                    }
                }

                if (onGetAllUsers != null)
                {
                    onGetAllUsers(this, "success");
                }
            }    

        }
    }

    public void GotAllUsers(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.allianceManager.onGetAllianceMembers -= GotAllUsers;

        if (string.Equals(value, "success"))
        {
            m_tableView.ReloadData();
        }
    }

    public void RemoveUser(UserModel user)
    {
        allUsers.Remove(user);
        m_tableView.ReloadData();
    }

    #endregion
}
