﻿using UnityEngine;
using System.Collections;
using Tacticsoft;
using SmartLocalization;

public class ResidenceBuildingsTableController : MonoBehaviour, ITableViewDataSource {

    public ResidenceBuildingsTableLine m_cellPrefab;
    public TableView m_tableView;
    public bool buildings;
    private ArrayList buildingsList;


    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void OnEnable() {
        m_tableView.dataSource = this;
        if (buildings)
        {
            buildingsList = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings.Count);
            foreach (BuildingModel building in GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings.Values)
            {
                if (!string.Equals(building.building, "Mayor_Residence"))
                {
                    buildingsList.Add(building);
                }
            }
        }
        else
        {
            buildingsList = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields.Count);
            foreach (BuildingModel field in GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields.Values)
            {
                buildingsList.Add(field);
            }
        }
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (buildingsList == null)
        {
            return 0;
        }
        else
        {
            return buildingsList.Count;
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        ResidenceBuildingsTableLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as ResidenceBuildingsTableLine;
        if (cell == null) {
            cell = (ResidenceBuildingsTableLine)GameObject.Instantiate(m_cellPrefab);
        }

        BuildingModel building = (BuildingModel)buildingsList[row];
        cell.SetBuildingName(GlobalGOScript.instance.gameManager.buildingConstants[building.building].building_name.Replace("_", " "));
        cell.SetLevel(building.level);
        cell.SetOwner(this);
        cell.SetPitId(building.pit_id);
        cell.SetStatus(LanguageManager.Instance.GetTextValue("SWOTSELocalization." + building.status));

        if (buildings)
		{	
			cell.SetType ("Buildings");
            cell.SetIcon("UI/BuildingsIcon/" + building.building);
        }
        else
        {
			cell.SetType ("Fields");
            cell.SetIcon("UI/FieldsIcon/" + building.building);
        }

        return cell;
    }

    public void OpenBuildingWindow(long pitId)
    {
//        if (buildings)
//        {
//            GlobalGOScript.instance.windowInstanceManager.openBuildingInformation(GlobalGOScript.ins)
//        }
//        else
//        {
//        }
    }

    #endregion
}
