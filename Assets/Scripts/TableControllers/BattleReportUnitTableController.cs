﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;

public class BattleReportUnitTableController : MonoBehaviour, ITableViewDataSource {

    public BattleReportUnitsLine m_cellPrefab;
    public TableView m_tableView;

    public List<string> unitNames;
    public List<long> unitBeforeCount;
    public List<long> unitAfterCount;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start() {
        m_tableView.dataSource = this;
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (unitNames == null || unitBeforeCount == null || unitAfterCount == null)
        {
            return 0;
        }
        else
        {
            return unitNames.Count;
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        BattleReportUnitsLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as BattleReportUnitsLine;
        if (cell == null) {
            cell = (BattleReportUnitsLine)GameObject.Instantiate(m_cellPrefab);
        }

        cell.SetUnitName(unitNames[row]);
        cell.SetBeforeCount(unitBeforeCount[row]);
        cell.SetRemainingCount(unitAfterCount[row]);
        cell.SetLostCount(unitBeforeCount[row] - unitAfterCount[row]);

        return cell;
    }

    #endregion
}
