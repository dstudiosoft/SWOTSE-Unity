﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;

public class BattleReportResourcesTableController : MonoBehaviour, ITableViewDataSource
{

    public BattleReportResourcesLine m_cellPrefab;
    public TableView m_tableView;

    public List<ResourcesModel> armyResources;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start()
    {
        m_tableView.dataSource = this;
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView)
    {
        if (armyResources == null)
        {
            return 0;
        }
        else
        {
            return armyResources.Count;
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row)
    {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row)
    {
        BattleReportResourcesLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as BattleReportResourcesLine;
        if (cell == null)
        {
            cell = (BattleReportResourcesLine)GameObject.Instantiate(m_cellPrefab);
        }

        ResourcesModel temp = armyResources[row];

        cell.SetCopper(Format(temp.cooper));
        cell.SetFood(Format(temp.food));
        cell.SetGold(Format(temp.gold));
        cell.SetIron(Format(temp.iron));
        cell.SetSilver(Format(temp.silver));
        cell.SetStone(Format(temp.stone));
        cell.SetWood(Format(temp.wood));

        return cell;
    }

    #endregion

    private string Format(long value)
    {
        if (value > 999999)
            return (value / 1000000).ToString() + "m";
        if (value > 999)
            return (value / 1000).ToString() + "k";
        return value.ToString();
    }
}
