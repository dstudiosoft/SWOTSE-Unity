﻿using UnityEngine;
using System.Collections;
using Tacticsoft;

public class MailAllianceReportTableController : MonoBehaviour, ITableViewDataSource {

    public MailAllianceReportTableLine m_cellPrefab;
    public TableView m_tableView;
    public bool alliance;
    private BattleReportHeader[] allReports;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start() {
        m_tableView.dataSource = this;
    }

    void OnEnable()
    {
        if (alliance)
        {
            GlobalGOScript.instance.playerManager.reportManager.onGetAllianceBattles += GotAllReports;
            StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetUnreadAllianceBattleReports());
        }
        else
        {
            GlobalGOScript.instance.playerManager.reportManager.onGetUnreadReports += GotAllReports;
            StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetUnreadReports());
        }
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (allReports == null)
        {
            return 0;
        }
        else
        {
            return allReports.GetLength(0);
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        MailAllianceReportTableLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as MailAllianceReportTableLine;
        if (cell == null) {
            cell = (MailAllianceReportTableLine)GameObject.Instantiate(m_cellPrefab);
        }
            
        BattleReportHeader report = allReports[row];
        cell.SetId(report.id);
        if (string.Equals(report.attack_type, "defend"))
        {
            cell.SetUserName(report.defenceUserName);
        }
        else
        {
            cell.SetUserName(report.attackUserName);
        }
        cell.SetOwner(this);
        cell.SetType(TextLocalizer.LocalizeString(report.attack_type));
        cell.SetSource(report.attackCityName);
        cell.SetTarget(report.defenceCityName);
        cell.SetTime(report.creation_date.ToString());

        return cell;
    }

    public void DeleteReport(long id)
    {
        if (alliance)
        {
            if (GlobalGOScript.instance.playerManager.reportManager.battles[id].attack_type.Equals("scout"))
            {
                GlobalGOScript.instance.playerManager.reportManager.onBattleReportDeleted += ReportDeleted;
                StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.DeleteScoutReport(id));
            }
            else
            {
                GlobalGOScript.instance.playerManager.reportManager.onBattleReportDeleted += ReportDeleted;
                StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.DeleteBattleReport(id));
            }
        }
    }

    public void ReportDeleted(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.reportManager.onBattleReportDeleted -= ReportDeleted;

        if (string.Equals(value, "success"))
        {
            ReloadTable();
        }
    }

    public void ReloadTable()
    {
        if (alliance)
        {
            if (GlobalGOScript.instance.playerManager.reportManager.allianceBattles != null)
            {
                allReports = new BattleReportHeader[GlobalGOScript.instance.playerManager.reportManager.allianceBattles.Count];
                GlobalGOScript.instance.playerManager.reportManager.allianceBattles.Values.CopyTo(allReports, 0);
                m_tableView.ReloadData();
            }
        }
        else
        {
            if (GlobalGOScript.instance.playerManager.reportManager.battles != null)
            {
                allReports = new BattleReportHeader[GlobalGOScript.instance.playerManager.reportManager.battles.Count];
                GlobalGOScript.instance.playerManager.reportManager.battles.Values.CopyTo(allReports, 0);
                m_tableView.ReloadData();
            }
        }
    }

    public void GotAllReports(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.reportManager.onGetUnreadReports -= GotAllReports;
        GlobalGOScript.instance.playerManager.reportManager.onGetAllianceBattles -= GotAllReports;

        if (string.Equals(value, "success"))
        {
            ReloadTable();
        }
    }

    #endregion
}