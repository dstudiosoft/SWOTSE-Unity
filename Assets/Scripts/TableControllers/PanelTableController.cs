﻿using UnityEngine;
using System.Collections;
using Tacticsoft;
using SmartLocalization;

public class PanelTableController : MonoBehaviour, ITableViewDataSource {

    public PanelTableLine m_cellPrefab;
    public TableView m_tableView;
    private ArrayList actionList;

    public string content;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void OnEnable() {
        m_tableView.dataSource = this;

        switch (content)
        {
            case "BUILDINGS":
                GlobalGOScript.instance.playerManager.reportManager.onGetBuildingTimers += ReloadTable;
                GlobalGOScript.instance.playerManager.reportManager.buildingTimers = null;
                StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetBuildingTimers(true));
                GlobalGOScript.instance.playerManager.reportManager.onGetWorldMapTimers += ReloadTable;
                GlobalGOScript.instance.playerManager.reportManager.worldMapTimers = null;
                StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetWorldMapTimers(true));
                break;
            case "UNITS":
                GlobalGOScript.instance.playerManager.reportManager.onGetUnitTimers += ReloadTable;
                StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetUnitTimers(true));
                break;
            case "ARMY":
                GlobalGOScript.instance.playerManager.reportManager.onGetArmyTimers += ReloadTable;
                StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetArmyTimers(true));
                break;
            case "ITEMS":
                GlobalGOScript.instance.playerManager.reportManager.onGetItemTimers += ReloadTable;
                StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetItemTimers(true));
                break;
            default:
                break;
        }

//        if (GlobalGOScript.instance.playerManager.reportManager.buildingTimers == null)
//        {
//            GlobalGOScript.instance.playerManager.reportManager.onGetBuildingTimers += ReloadTable;
//            StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetBuildingTimers(true));  
//        }
//
//        if (GlobalGOScript.instance.playerManager.reportManager.worldMapTimers == null)
//        {
//            GlobalGOScript.instance.playerManager.reportManager.onGetWorldMapTimers += ReloadTable;
//            StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetWorldMapTimers(true));
//        }
//
//        if (GlobalGOScript.instance.playerManager.reportManager.unitTimers == null)
//        {
//            GlobalGOScript.instance.playerManager.reportManager.onGetUnitTimers += ReloadTable;
//            StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetUnitTimers(true));
//        }
//
//        if (GlobalGOScript.instance.playerManager.reportManager.armyTimers == null)
//        {
//            GlobalGOScript.instance.playerManager.reportManager.onGetArmyTimers += ReloadTable;
//            StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetArmyTimers(true));
//        }
//
//        if (GlobalGOScript.instance.playerManager.reportManager.itemTimers == null)
//        {
//            GlobalGOScript.instance.playerManager.reportManager.onGetItemTimers += ReloadTable;
//            StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetItemTimers(true));
//        }
//
//        switch (content)
//        {
//            case "BUILDINGS":
//                if (GlobalGOScript.instance.playerManager.reportManager.buildingTimers != null)
//                {
//                    actionList = new ArrayList(GlobalGOScript.instance.playerManager.reportManager.buildingTimers.Count);
//
//                    foreach (BuildingTimer timer in GlobalGOScript.instance.playerManager.reportManager.buildingTimers.Values)
//                    {
//                        actionList.Add(timer);
//                    }
//                }
//                
//                if (GlobalGOScript.instance.playerManager.reportManager.worldMapTimers != null)
//                {
//                    if (actionList == null)
//                    {
//                        actionList = new ArrayList(GlobalGOScript.instance.playerManager.reportManager.buildingTimers.Count);
//
//                        foreach (BuildingTimer timer in GlobalGOScript.instance.playerManager.reportManager.buildingTimers.Values)
//                        {
//                            actionList.Add(timer);
//                        }
//                    }
//                    else
//                    {
//                        foreach (WorldMapTimer timer in GlobalGOScript.instance.playerManager.reportManager.worldMapTimers.Values)
//                        {
//                            actionList.Add(timer);
//                        }
//                    }
//                }
//
//                break;
//            case "UNITS":
//                if (GlobalGOScript.instance.playerManager.reportManager.unitTimers != null)
//                {
//                    actionList = new ArrayList(GlobalGOScript.instance.playerManager.reportManager.unitTimers.Count);
//
//                    foreach (UnitTimer timer in GlobalGOScript.instance.playerManager.reportManager.unitTimers.Values)
//                    {
//                        actionList.Add(timer);
//                    }
//                }
//                break;
//            case "ARMY":
//                if (GlobalGOScript.instance.playerManager.reportManager.armyTimers != null)
//                {
//                    actionList = new ArrayList(GlobalGOScript.instance.playerManager.reportManager.armyTimers.Count);
//
//                    foreach (ArmyTimer timer in GlobalGOScript.instance.playerManager.reportManager.armyTimers.Values)
//                    {
//                        actionList.Add(timer);
//                    }
//                }
//                break;
//            case "ITEMS":
//                if (GlobalGOScript.instance.playerManager.reportManager.itemTimers != null)
//                {
//                    actionList = new ArrayList(GlobalGOScript.instance.playerManager.reportManager.itemTimers.Count);
//
//                    foreach (ItemTimer timer in GlobalGOScript.instance.playerManager.reportManager.itemTimers.Values)
//                    {
//                        actionList.Add(timer);
//                    }
//                }
//                break;
//            default:
//                break;
//        }

    //clean outdated
//        if (actionList != null)
//        {
//            switch (content)
//            {
//                case "BUILDINGS":
//                    for (int row = 0; row < actionList.Count; row++)
//                    {
//                        if (actionList[row].GetType() == typeof(BuildingTimer))
//                        {
//                            BuildingTimer buildingTimer = (BuildingTimer)(actionList[row]);
//                            if (buildingTimer.finish_time <= GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update)
//                            {
//                                RemoveTimer(buildingTimer.event_id);
//                            }
//                        }
//                        else
//                        {
//                            WorldMapTimer worldTimer = (WorldMapTimer)(actionList[row]);
//                            if (worldTimer.finish_time <= GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update)
//                            {
//                                RemoveTimer(worldTimer.event_id);
//                            }
//                        }
//                    }
//                    break;
//                case "UNITS":
//                    for (int row = 0; row < actionList.Count; row++)
//                    {
//                        UnitTimer unitTimer = (UnitTimer)(actionList[row]);
//                        if (unitTimer.finish_time <= GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update)
//                        {
//                            RemoveTimer(unitTimer.event_id);
//                        }
//                    }
//                    break;
//                case "ARMY":
//                    for (int row = 0; row < actionList.Count; row++)
//                    {
//                        ArmyTimer armyTimer = (ArmyTimer)(actionList[row]);
//                        if (armyTimer.finish_time <= GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update)
//                        {
//                            RemoveTimer(armyTimer.event_id);
//                        }
//                    }
//                    break;
//                case "ITEMS":
//                    for (int row = 0; row < actionList.Count; row++)
//                    {
//                        ItemTimer itemTimer = (ItemTimer)(actionList[row]);
//                        if (itemTimer.finish_time <= GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update)
//                        {
//                            RemoveTimer(itemTimer.event_id);
//                        }
//                    }
//                    break;
//                default:
//                    break;
//            }
//        }
    }

    #region ITableViewDataSource

//    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (actionList == null)
        {
            return 0;
        }
        else
        {
            return actionList.Count;
        }
    }

//    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

	public static string Reverse( string s )
	{
		char[] charArray = s.ToCharArray();
		System.Array.Reverse( charArray );
		return new string( charArray );
	}

//
//    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        PanelTableLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as PanelTableLine;
        if (cell == null) {
            cell = (PanelTableLine)GameObject.Instantiate(m_cellPrefab);
        }

        switch (content)
        {
            case "BUILDINGS":

                if (actionList[row].GetType() == typeof(BuildingTimer))
                {
                    BuildingTimer buildingTimer = (BuildingTimer)(actionList[row]);
                
                    if (string.Equals(buildingTimer.building.location, "city"))
                    {
                        cell.SetItemImage("UI/BuildingsIcon/" + buildingTimer.building.building.ToString());
                    }
                    else if (string.Equals(buildingTimer.building.location, "fields"))
                    {
                        cell.SetItemImage("UI/FieldsIcon/" + buildingTimer.building.building.ToString());
                    }
					string buildingName = GlobalGOScript.instance.gameManager.buildingConstants [buildingTimer.building.building].building_name.Replace ("_", "");
					Debug.Log ("Arabic = " + buildingName + ", Reverse = " + Reverse (buildingName));
					cell.SetItemText(Reverse(buildingName));
                    cell.SetTimer(buildingTimer.start_time, buildingTimer.finish_time);
                    cell.SetEventId(buildingTimer.event_id);
                    cell.SetItemId(buildingTimer.building.id);
                    if (string.Equals(buildingTimer.status, "updating"))
                    {
                        cell.SetActionImage("UI/uppointer");
                    }
                    else if (string.Equals(buildingTimer.status, "creating"))
                    {
                        cell.SetActionImage("UI/uppointer");
                    }
                    else if (string.Equals(buildingTimer.status, "downgrading"))
                    {
                        cell.SetActionImage("UI/downpointer");
                    }
                    cell.SetInteractable(true);
                    cell.SetCancelable(true);
                    cell.SetCityBuilding(true);
                    cell.SetOwner(this);
                }
                else
                {
                    WorldMapTimer worldTimer = (WorldMapTimer)(actionList[row]);
                    WorldFieldModel valley;
                    string localizedValue;

                    if (string.Equals(worldTimer.status, "creating"))
                    {
                        cell.SetActionImage("UI/uppointer");
                        cell.SetItemImage("WorldMapBuildings/" + worldTimer.type + "/" + "1");
                        localizedValue = "New City";
                        cell.SetItemImage("WorldMapBuildings/City/1");
                    }
                    else
                    {
                        if (string.Equals(worldTimer.status, "updating"))
                        {
                            cell.SetActionImage("UI/uppointer");
                        }
                        else if (string.Equals(worldTimer.status, "downgrading"))
                        {
                            cell.SetActionImage("UI/downpointer");
                        }

                        valley = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityValleys[worldTimer.worldMap];
                        cell.SetItemImage("WorldMapBuildings/" + valley.cityType + "/" + valley.level.ToString());
                        localizedValue = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + worldTimer.type.Replace("_", ""));
                    }

                    if (string.IsNullOrEmpty(localizedValue))
                    {
                        cell.SetItemText(worldTimer.type.Replace('_', ' '));
                    }
                    else
                    {
                        cell.SetItemText(localizedValue);
                    }

                    cell.SetTimer(worldTimer.start_time, worldTimer.finish_time);
                    cell.SetEventId(worldTimer.event_id);
                    cell.SetItemId(worldTimer.worldMap);
                    cell.SetInteractable(true);
                    cell.SetCancelable(true);
                    cell.SetCityBuilding(false);
                    cell.SetOwner(this);
                }
                break;
            case "UNITS":
                UnitTimer unitTimer = (UnitTimer)(actionList[row]);
                
                cell.SetItemImage("UI/UnitIcons/" + unitTimer.unit.ToString());
                cell.SetItemText(GlobalGOScript.instance.gameManager.unitConstants[unitTimer.unit].unit_name);
                cell.SetTimer(unitTimer.start_time, unitTimer.finish_time);
                cell.SetEventId(unitTimer.event_id);
                //                cell.SetItemId(unitTimer.id);
                cell.SetActionImage("UI/uppointer");
                cell.SetInteractable(true);
                cell.SetCancelable(true);
                cell.SetOwner(this);
                break;
            case "ARMY":
                ArmyTimer armyTimer = (ArmyTimer)(actionList[row]);

                cell.SetItemText("Army");
                cell.SetTimer(armyTimer.start_time, armyTimer.finish_time);
                cell.SetEventId(armyTimer.event_id);
                cell.SetItemId(armyTimer.army);
                if (string.Equals(armyTimer.army_status, "return"))
                {
                    cell.SetActionImage("UI/March/" + armyTimer.old_army_status);
                    cell.progressBar.gameObject.GetComponent<UnityEngine.UI.Image>().color = Color.yellow;
                    cell.SetCancelable(false);
                }
                else
                {
                    cell.SetActionImage("UI/March/" + armyTimer.army_status);
                    cell.SetCancelable(true);
                }
                cell.SetItemImage("WorldMapBuildings/" + armyTimer.targetType + "/" + "1");
                cell.SetInteractable(false);
                cell.SetOwner(this);

                if (armyTimer.enemy)
                {
                    cell.progressBar.gameObject.GetComponent<UnityEngine.UI.Image>().color = Color.red;
                }
                break;
            case "ITEMS":
                ItemTimer itemTimer = (ItemTimer)(actionList[row]);

                if (itemTimer.itemID.Contains("_cd"))
                {
                    cell.progressBar.gameObject.GetComponent<UnityEngine.UI.Image>().color = Color.red;
                    cell.SetItemImage("UI/ShopItems/" + itemTimer.type + "/" + itemTimer.itemID.Replace("_cd", ""));
                }
                else
                {
                    cell.SetItemImage("UI/ShopItems/" + itemTimer.type + "/" + itemTimer.itemID);
                }
                    
                cell.SetItemText(itemTimer.name.Replace('_', ' '));
                cell.SetTimer(itemTimer.start_time, itemTimer.finish_time);
                cell.SetEventId(itemTimer.event_id);
                //                cell.SetEventId(itemTimer.id);
                cell.SetActionImage("UI/clock");
                cell.SetInteractable(false);
                cell.SetCancelable(false);
                cell.SetOwner(this);
                break;
            default:
                break;
        }

        return cell;
    }

    #endregion

    public void RemoveTimer(long eventId)
    {
        switch (content)
        {
            case "BUILDINGS":
                for (int i = 0; i < actionList.Count; i++)
                {
                    if (actionList[i].GetType() == typeof(BuildingTimer))
                    {
                        if (((BuildingTimer)(actionList[i])).event_id == eventId)
                        {
                            actionList.RemoveAt(i);
                            GlobalGOScript.instance.playerManager.reportManager.buildingTimers.Remove(eventId);
                            break;
                        }
                    }
                    else
                    {
                        if (((WorldMapTimer)(actionList[i])).event_id == eventId)
                        {
                            actionList.RemoveAt(i);
                            GlobalGOScript.instance.playerManager.reportManager.worldMapTimers.Remove(eventId);
                            break;
                        }
                    }
                }

                if (GlobalGOScript.instance.playerManager.reportManager.buildingTimers.ContainsKey(eventId))
                {
                    GlobalGOScript.instance.playerManager.reportManager.buildingTimers.Remove(eventId);
                }

                if (GlobalGOScript.instance.playerManager.reportManager.worldMapTimers.ContainsKey(eventId))
                {
                    GlobalGOScript.instance.playerManager.reportManager.worldMapTimers.Remove(eventId);
                }
                break;
            case "UNITS":
                for (int i = 0; i < actionList.Count; i++)
                {
                    if (((UnitTimer)(actionList[i])).event_id == eventId)
                    {
                        actionList.RemoveAt(i);
                        break;
                    }
                }
                if (GlobalGOScript.instance.playerManager.reportManager.unitTimers.ContainsKey(eventId))
                {
                    GlobalGOScript.instance.playerManager.reportManager.unitTimers.Remove(eventId);
                }
                break;
            case "ARMY":
                for (int i = 0; i < actionList.Count; i++)
                {
                    if (((ArmyTimer)(actionList[i])).event_id == eventId)
                    {
                        actionList.RemoveAt(i);
                        break;
                    }
                }
                if (GlobalGOScript.instance.playerManager.reportManager.armyTimers.ContainsKey(eventId))
                {
                    GlobalGOScript.instance.playerManager.reportManager.armyTimers.Remove(eventId);
                }
                break;
            case "ITEMS":
                for (int i = 0; i < actionList.Count; i++)
                {
                    if (((ItemTimer)(actionList[i])).event_id == eventId)
                    {
                        actionList.RemoveAt(i);
                        break;
                    }
                }
                if (GlobalGOScript.instance.playerManager.reportManager.itemTimers.ContainsKey(eventId))
                {
                    GlobalGOScript.instance.playerManager.reportManager.itemTimers.Remove(eventId);
                }
                break;
            default:
                break;
        }
        m_tableView.ReloadData();
    }

    public void ReloadTable(object sender, string value)
    {
        switch (content)
        {
            case "BUILDINGS":

                if (GlobalGOScript.instance.playerManager.reportManager.buildingTimers != null)
                {
                    GlobalGOScript.instance.playerManager.reportManager.onGetBuildingTimers -= ReloadTable;
                    actionList = new ArrayList(GlobalGOScript.instance.playerManager.reportManager.buildingTimers.Count);

                    foreach (BuildingTimer timer in GlobalGOScript.instance.playerManager.reportManager.buildingTimers.Values)
                    {
                        actionList.Add(timer);
                    }
                    GlobalGOScript.instance.underAttackNotification.SetActive(false);
                }

                if (GlobalGOScript.instance.playerManager.reportManager.worldMapTimers != null)
                {
                    GlobalGOScript.instance.playerManager.reportManager.onGetWorldMapTimers -= ReloadTable;

                    int buildigTimersCount = GlobalGOScript.instance.playerManager.reportManager.buildingTimers == null ? 0 : GlobalGOScript.instance.playerManager.reportManager.buildingTimers.Count;

                    actionList = new ArrayList(GlobalGOScript.instance.playerManager.reportManager.worldMapTimers.Count + buildigTimersCount);

                    if (buildigTimersCount != 0)
                    {
                        foreach (BuildingTimer timer in GlobalGOScript.instance.playerManager.reportManager.buildingTimers.Values)
                        {
                            actionList.Add(timer);
                        }
                    }

                    foreach (WorldMapTimer timer in GlobalGOScript.instance.playerManager.reportManager.worldMapTimers.Values)
                    {
                        actionList.Add(timer);
                    }
                    GlobalGOScript.instance.underAttackNotification.SetActive(false);
                }
                break;
            case "UNITS":
                GlobalGOScript.instance.playerManager.reportManager.onGetUnitTimers -= ReloadTable;

                if (GlobalGOScript.instance.playerManager.reportManager.unitTimers != null)
                {
                    actionList = new ArrayList(GlobalGOScript.instance.playerManager.reportManager.unitTimers.Count);

                    foreach (UnitTimer timer in GlobalGOScript.instance.playerManager.reportManager.unitTimers.Values)
                    {
                        actionList.Add(timer);
                    }
                    GlobalGOScript.instance.underAttackNotification.SetActive(false);
                }
                break;
            case "ARMY":
                GlobalGOScript.instance.playerManager.reportManager.onGetArmyTimers -= ReloadTable;

                if (GlobalGOScript.instance.playerManager.reportManager.armyTimers != null)
                {
                    actionList = new ArrayList(GlobalGOScript.instance.playerManager.reportManager.armyTimers.Count);

                    foreach (ArmyTimer timer in GlobalGOScript.instance.playerManager.reportManager.armyTimers.Values)
                    {
                        actionList.Add(timer);
                    }
                    GlobalGOScript.instance.underAttackNotification.SetActive(false);
                }
                break;
            case "ITEMS":
                GlobalGOScript.instance.playerManager.reportManager.onGetItemTimers -= ReloadTable;

                if (GlobalGOScript.instance.playerManager.reportManager.itemTimers != null)
                {
                    actionList = new ArrayList(GlobalGOScript.instance.playerManager.reportManager.itemTimers.Count);

                    foreach (ItemTimer timer in GlobalGOScript.instance.playerManager.reportManager.itemTimers.Values)
                    {
                        actionList.Add(timer);
                    }
                    GlobalGOScript.instance.underAttackNotification.SetActive(false);
                }
                break;
            default:
                break;
        }
        m_tableView.ReloadData();
    }
}
