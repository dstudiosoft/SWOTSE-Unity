﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using Tacticsoft;
using SmartLocalization;
using ArabicSupport;

public class MailSystemTableController : MonoBehaviour, ITableViewDataSource {

    public MailSystemLine m_cellPrefab;
    public TableView m_tableView;
    private SystemReportModel[] allSystem;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start() {
        m_tableView.dataSource = this;
        //temporary will be here request reports
        GlobalGOScript.instance.playerManager.reportManager.onGetUnreadReports += GotAllReports;

        StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetUnreadReports());
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (allSystem == null)
        {
            return 0;
        }
        else
        {
            return allSystem.GetLength(0);
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        MailSystemLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as MailSystemLine;
        if (cell == null) {
            cell = (MailSystemLine)GameObject.Instantiate(m_cellPrefab);
        }

        SystemReportModel system = allSystem[row];
        cell.SetSubject(system.subject);
        cell.SetMessage(system.message);

        StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.ReadSystemReport(system.id));
        GlobalGOScript.instance.playerManager.reportManager.systemReports[system.id].isRead = true;

        return cell;
    }

    public void GotAllReports(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.reportManager.onGetUnreadReports -= GotAllReports;

        if (string.Equals(value, "success"))
        {
            ReloadTable();
        }
    }

    public void ReloadTable()
    {
        if (GlobalGOScript.instance.playerManager.reportManager.systemReports != null)
        {
            allSystem = new SystemReportModel[GlobalGOScript.instance.playerManager.reportManager.systemReports.Count];
            GlobalGOScript.instance.playerManager.reportManager.systemReports.Values.CopyTo(allSystem, 0);
        }
                
        m_tableView.ReloadData();
    }

    #endregion
}
