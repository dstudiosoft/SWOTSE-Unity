﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;

public class BattleReportItemsTableController : MonoBehaviour, ITableViewDataSource {

    public BattleReportItemsLine m_cellPrefab;
    public TableView m_tableView;

    public List<CityTreasureModel> items;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start() {
        m_tableView.dataSource = this;
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (items == null)
        {
            return 0;
        }
        else
        {
            return items.Count;
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        BattleReportItemsLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as BattleReportItemsLine;
        if (cell == null) {
            cell = (BattleReportItemsLine)GameObject.Instantiate(m_cellPrefab);
        }

        CityTreasureModel item = items[row];
        cell.SetItemName(item.item_constant.name);
        cell.SetItemPrice(item.item_constant.price_sh);
        cell.SetItemCount(item.count);

        return cell;
    }

    #endregion
}
