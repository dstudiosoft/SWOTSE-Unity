﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;
using UnityEngine.Networking;
using SmartLocalization;

public class FinanceTableController : MonoBehaviour, ITableViewDataSource
{
    public FinancialContentChanger owner;
    public ArrayList currentSourceArray;

    public FinancialTableLine m_cellPrefab;
    public TableView m_tableView;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start()
    {
        m_tableView.dataSource = this;
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView)
    {
        if (currentSourceArray == null)
        {
            return 0;
        }
        else
        {
            return currentSourceArray.Count;
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row)
    {
        return (m_cellPrefab.transform.GetChild(0) as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row)
    {
        FinancialTableLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as FinancialTableLine;
        if (cell == null)
        {
            cell = (FinancialTableLine)GameObject.Instantiate(m_cellPrefab);
        }

        FinanceReportModel item = (FinanceReportModel)(currentSourceArray[row]);

        cell.SetDate(item.date.ToLongDateString());
        cell.SetImage("UI/ShopItems/" + item.item_constant.type + "/" + item.item_constant.id);
        cell.SetName(item.item_constant.name);
        cell.SetPrice(item.price);
        cell.SetQuantity(item.count);
        cell.SetFromUser(item.fromUser);
        cell.SetOwner(owner);
        cell.SetLotId(item.id);
        cell.SetCancelable(string.Equals(owner.content, "Offers"));
        
        return cell;
    }

    #endregion
}
