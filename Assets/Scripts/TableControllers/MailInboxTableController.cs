﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class MailInboxTableController : MonoBehaviour, ITableViewDataSource {

    public MailInboxLine m_cellPrefab;
    public TableView m_tableView;
    public InputField username;
    private MessageHeader[] allMessages;
    private ArrayList searchResults;
    private bool search = false;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start() {
        m_tableView.dataSource = this;
    }

    void OnEnable()
    {
        GlobalGOScript.instance.playerManager.reportManager.onGetUnreadReports += GotAllReports;
        StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetUnreadReports());
        if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance != null)
        {
            StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.AllianceMemebers());
        }
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {

        if (search)
        {
            return searchResults == null ? 0 : searchResults.Count;
        }
        else
        {
            return allMessages == null ? 0 : allMessages.GetLength(0);
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        MailInboxLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as MailInboxLine;
        if (cell == null) {
            cell = (MailInboxLine)GameObject.Instantiate(m_cellPrefab);
        }

        MessageHeader message;

        if (search)
        {
            message = (MessageHeader)searchResults[row];
        }
        else
        {
            message = allMessages[row];
        }

        cell.SetOwner(this);
        cell.SetMessageId(message.id);
        cell.SetPlayerName(message.fromUserName);
        cell.SetSubject(message.subject);
        cell.SetDate(message.creation_date.ToString());

        return cell;
    }

    public void SearchUserMessages()
    {
        if (string.IsNullOrEmpty(username.text))
        {
            search = false;
            return;
        }
        else
        {
            search = true;
        }

        searchResults = new ArrayList();
        foreach (object message in allMessages)
        {
            if (((MessageHeader)message).fromUserName.Contains(username.text))
            {
                searchResults.Add(message);
            }
        }

        m_tableView.ReloadData();
    }

    public void DeleteMessage(long messageId)
    {
        GlobalGOScript.instance.playerManager.reportManager.onMessageDeleted += MessageDeleted;
        StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.DeleteMessageReport(messageId));
    }

    public void MessageDeleted(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.reportManager.onMessageDeleted -= MessageDeleted;

        if (string.Equals(value, "success"))
        {
            ReloadTable();
        }
    }

    public void ReloadTable()
    {
        if (GlobalGOScript.instance.playerManager.reportManager.inbox != null)
        {
            allMessages = new MessageHeader[GlobalGOScript.instance.playerManager.reportManager.inbox.Count];
            GlobalGOScript.instance.playerManager.reportManager.inbox.Values.CopyTo(allMessages, 0);

            if (search)
            {
                SearchUserMessages();
            }

            m_tableView.ReloadData();
        }
    }

    public void GotAllReports(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.reportManager.onGetUnreadReports -= GotAllReports;

        if (string.Equals(value, "success"))
        {
            ReloadTable();
        }
    }

    #endregion
}