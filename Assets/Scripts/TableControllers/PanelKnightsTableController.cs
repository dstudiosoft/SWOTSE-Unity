﻿using UnityEngine;
using System.Collections;
using Tacticsoft;

public class PanelKnightsTableController : MonoBehaviour, ITableViewDataSource {

    public PanelKnightTableLine m_cellPrefab;
    public TableView m_tableView;
    private ArrayList knightList;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls

    void OnEnable() {
        m_tableView.dataSource = this;
        if (GlobalGOScript.instance != null && GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals != null)
        {
            knightList = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals.Count);
            long mayorKey = 0;
            long highestPolitics = 0;
            foreach (ArmyGeneralModel general in GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals.Values)
            {
                if (general.politics > highestPolitics)
                {
                    highestPolitics = general.politics;
                    mayorKey = general.id;
                }
            }

            knightList.Add(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals[mayorKey]);
            foreach (ArmyGeneralModel general in GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals.Values)
            {
                if (general.id != mayorKey)
                {
                    knightList.Add(general);
                }
            }
        }
        else
        {
            knightList = null;
        }

        m_tableView.ReloadData();
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (knightList == null)
        {
            return 0;
        }
        else
        {
            return knightList.Count;
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        PanelKnightTableLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as PanelKnightTableLine;
        if (cell == null) {
            cell = (PanelKnightTableLine)GameObject.Instantiate(m_cellPrefab);
        }

        ArmyGeneralModel knight = (ArmyGeneralModel)knightList[row];
        cell.SetKnightId(knight.id);
        cell.SetKnightName(knight.name);
        cell.SetLevel(knight.level);
        cell.SetStatus(knight.status);

        return cell;
    }

    public void ReloadData()
    {
        if (GlobalGOScript.instance != null && GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals != null)
        {
            if (GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals.Count != 0)
            {
                knightList = new ArrayList(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals.Count);
                long mayorKey = 0;
                long highestPolitics = 0;
                foreach (ArmyGeneralModel general in GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals.Values)
                {
                    if (general.politics > highestPolitics)
                    {
                        highestPolitics = general.politics;
                        mayorKey = general.id;
                    }
                }

                knightList.Add(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals[mayorKey]);
                foreach (ArmyGeneralModel general in GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals.Values)
                {
                    if (general.id != mayorKey)
                    {
                        knightList.Add(general);
                    }
                }
            }
            else
            {
                knightList = null;
            }
        }
        else
        {
            knightList = null;
        }

        m_tableView.ReloadData();
    }

    public void GeneralsUpdated(object sender, string value)
    {
        if (string.Equals(value, "success"))
        {
            ReloadData();
        }
    }

    #endregion
}
