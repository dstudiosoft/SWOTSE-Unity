﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using Tacticsoft;

public class NewMailPlayersTableController : MonoBehaviour, ITableViewDataSource {

    public NewMailContentManager owner;
    public NewMailPlayerLine m_cellPrefab;
    public TableView m_tableView;
    private List<UserModel> allUsers;

    public int m_numRows;

    private event SimpleEvent onGetAllUsers;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start() {
        m_tableView.dataSource = this;
        onGetAllUsers += GotAllUsers;
        StartCoroutine(GetAllUsers());
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (allUsers == null)
        {
            return 0;
        }
        else
        {
            return allUsers.Count;
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        NewMailPlayerLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as NewMailPlayerLine;
        if (cell == null) {
            cell = (NewMailPlayerLine)GameObject.Instantiate(m_cellPrefab);
        }

        UserModel user = allUsers[row];
        cell.SetManager(owner);
        cell.SetPlayerName(user.username);
        cell.SetPlayerId(user.id);

        return cell;
    }

    public IEnumerator GetAllUsers()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/authorization/allUsers/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                //failed
                if (onGetAllUsers != null)
                {
                    onGetAllUsers(this, "fail");
                }
            }
            else
            {
                //success
                JSONObject usersArray = JSONObject.Create(request.downloadHandler.text);

                allUsers = new List<UserModel>();

                foreach (JSONObject user in usersArray.list)
                {
                    UserModel player = JsonUtility.FromJson<UserModel>(user.Print());

                    if (!user["alliance"].IsNull)
                    {
                        AllianceModel userAlliance = JsonUtility.FromJson<AllianceModel>(user["alliance"].Print());
                        player.alliance = userAlliance;
                    }
                    else
                    {
                        player.alliance = new AllianceModel();
                        player.alliance.alliance_name = "none";
                    }

                    if (!user["last_login"].IsNull)
                    {
                        player.last_login = DateTime.Parse(user["last_login"].str);
                    }

                    MyCityModel memberCity = JsonUtility.FromJson<MyCityModel>(user["capital"].Print());
                    player.capital = memberCity;

                    if (player.id != GlobalGOScript.instance.playerManager.user.id)
                    {
                        allUsers.Add(player);
                    }
                }

                if (onGetAllUsers != null)
                {
                    onGetAllUsers(this, "success");
                }
            }    

        }
    }

    public void GotAllUsers(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.allianceManager.onGetAllianceMembers -= GotAllUsers;

        if (string.Equals(value, "success"))
        {
            m_tableView.ReloadData();
        }
    }

    public void RemoveUser(UserModel user)
    {
        allUsers.Remove(user);
        m_tableView.ReloadData();
    }

    #endregion
}
