﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using Tacticsoft;

public class MarketSellTableController : MonoBehaviour, ITableViewDataSource {

    public MarketManager owner;
    public MarketSellTableLine m_cellPrefab;
    public TableView m_tableView;
    private ArrayList lots;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start() {
        m_tableView.dataSource = this;
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (lots == null)
        {
            return 0;
        }
        else
        {
            return lots.Count;
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        MarketSellTableLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as MarketSellTableLine;
        if (cell == null) {
            cell = (MarketSellTableLine)GameObject.Instantiate(m_cellPrefab);
        }

        MarketLotModel lot = (MarketLotModel)(lots[row]);
        cell.SetQuantity(lot.count);
        cell.SetUnitPrice(lot.price);
        cell.SetId(lot.id);
        cell.SetOwner(owner);

        return cell;
    }

    public void ReloadTable(ArrayList managerLots)
    {
        lots = managerLots;
        m_tableView.ReloadData();
    }

    #endregion
}
