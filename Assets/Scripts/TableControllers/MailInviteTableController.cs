﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using Tacticsoft;
using SmartLocalization;
using ArabicSupport;

public class MailInviteTableController : MonoBehaviour, ITableViewDataSource {

    public MailInviteLine m_cellPrefab;
    public TableView m_tableView;
    private InviteReportModel[] allInvites;
    public bool mail;
    private event ConfirmationEvent confirmed;
    private long inviteId;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start() {
        m_tableView.dataSource = this;
        //temporary will be here request reports
        GlobalGOScript.instance.playerManager.reportManager.onGetUnreadReports += GotAllReports;

        StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetUnreadReports());
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (allInvites == null)
        {
            return 0;
        }
        else
        {
            return allInvites.GetLength(0);
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        MailInviteLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as MailInviteLine;
        if (cell == null) {
            cell = (MailInviteLine)GameObject.Instantiate(m_cellPrefab);
        }

        InviteReportModel invite = allInvites[row];
        cell.SetInviteId(invite.id);
        if (mail)
        {
            cell.SetName(invite.inviteFromName);
        }
        else
        {
            cell.SetName(invite.inviteToName);
        }
        cell.SetMailDate(invite.creationDate.ToString());
        cell.SetOwner(this);

        return cell;
    }

    public void AcceptInvite(long id)
    {
        inviteId = id;

        string localizedValue = LanguageManager.Instance.GetTextValue("SWOTSELocalization.ConfirmAllianceInvite");

        if (LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
        {
            localizedValue = ArabicFixer.Fix(localizedValue);
        }

        confirmed += AcceptConfirmed;
        GlobalGOScript.instance.windowInstanceManager.openConfirmationWindow(localizedValue, confirmed);
    }

    public void AcceptConfirmed(bool value)
    {
        confirmed -= AcceptConfirmed;
        if (value)
        {
            GlobalGOScript.instance.playerManager.allianceManager.onAcceptInvite += InviteResponded;
            StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.AcceptInvite(inviteId));
        }
        GlobalGOScript.instance.windowInstanceManager.closeConfirmationWindow();
    }

    public void RejectInvite(long id)
    {
        GlobalGOScript.instance.playerManager.allianceManager.onCancelInvite += InviteResponded;
        StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.CancelInvite(id));
    }

    public void InviteResponded(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.allianceManager.onCancelInvite -= InviteResponded;

        if (string.Equals(value, "success"))
        {
            ReloadTable();
        }
    }

    public void GotAllReports(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.reportManager.onGetUnreadReports -= GotAllReports;

        if (string.Equals(value, "success"))
        {
            ReloadTable();
        }
    }

    public void ReloadTable()
    {
        if (mail)
        {
            if (GlobalGOScript.instance.playerManager.reportManager.invites != null)
            {
                allInvites = new InviteReportModel[GlobalGOScript.instance.playerManager.reportManager.invites.Count];
                GlobalGOScript.instance.playerManager.reportManager.invites.Values.CopyTo(allInvites, 0);
            }

        }
        else
        {
            if (GlobalGOScript.instance.playerManager.reportManager.allianceRequsets != null)
            {
                allInvites = new InviteReportModel[GlobalGOScript.instance.playerManager.reportManager.allianceRequsets.Count];
                GlobalGOScript.instance.playerManager.reportManager.allianceRequsets.Values.CopyTo(allInvites, 0);
            }
        }
        m_tableView.ReloadData();
    }

    #endregion
}
