﻿using UnityEngine;
using System.Collections;
using Tacticsoft;

public class AllianceAlliancesTableController : MonoBehaviour, ITableViewDataSource {

    public AllianceAlliancesLine m_cellPrefab;
    public TableView m_tableView;
    private AllianceModel[] alliances;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void Start() {
        m_tableView.dataSource = this;
        alliances = new AllianceModel[GlobalGOScript.instance.playerManager.allianceManager.allAlliances.Count];
        GlobalGOScript.instance.playerManager.allianceManager.allAlliances.Values.CopyTo(alliances, 0);
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (GlobalGOScript.instance.playerManager.allianceManager.allAlliances == null)
        {
            return 0;
        }
        else
        {
            return GlobalGOScript.instance.playerManager.allianceManager.allAlliances.Count;
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        AllianceAlliancesLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as AllianceAlliancesLine;
        if (cell == null) {
            cell = (AllianceAlliancesLine)GameObject.Instantiate(m_cellPrefab);
        }

        AllianceModel alliance = alliances[row];
        cell.SetAllianceName(alliance.alliance_name);
        cell.SetExperience(alliance.alliance_experience);
        cell.SetFounder(alliance.founderName);
        cell.SetLeader(alliance.leaderName);
        cell.SetMembers(alliance.members_count);
        cell.SetRank(alliance.rank);
        if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance != null)
        {
            if (alliance.id == GlobalGOScript.instance.playerManager.allianceManager.userAlliance.id)
            {
                cell.SetJoinable(false);
            }
            else
            {
                cell.SetJoinable(true);
            }
        }
        cell.SetAllianceId(alliance.id);
        cell.SetOwner(this);

        return cell;
    }

    #endregion
}
