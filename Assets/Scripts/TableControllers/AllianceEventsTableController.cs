﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;
using Tacticsoft;

public class AllianceEventsTableController : MonoBehaviour, ITableViewDataSource {

	public AllianceEventsLine m_cellPrefab;
	public TableView m_tableView;

	public int m_numRows;

	private ArrayList allianceEvents;
	private event SimpleEvent onGetAllianceEvents;

	//Register as the TableView's delegate (required) and data source (optional)
	//to receive the calls
	void OnEnable() {
		m_tableView.dataSource = this;
        if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance != null)
        {
            onGetAllianceEvents += GotAllianceEvents;
            StartCoroutine(GetAllianceEvents());   
        }
	}

	#region ITableViewDataSource

	//Will be called by the TableView to know how many rows are in this table
	public int GetNumberOfRowsForTableView(TableView tableView) {
		if (allianceEvents == null)
		{
			return 0;
		}
		else
		{
			return allianceEvents.Count;
		}
	}

	//Will be called by the TableView to know what is the height of each row
	public float GetHeightForRowInTableView(TableView tableView, int row) {
		return (m_cellPrefab.transform as RectTransform).rect.height;
	}

	//Will be called by the TableView when a cell needs to be created for display
	public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
		AllianceEventsLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as AllianceEventsLine;
		if (cell == null) {
			cell = (AllianceEventsLine)GameObject.Instantiate(m_cellPrefab);
		}

        AllianceEventModel eventModel = (AllianceEventModel)allianceEvents[row];

        cell.SetDate(eventModel.date.ToString());
        cell.SetRemark(eventModel.message);

		return cell;
	}

	#endregion

	private IEnumerator GetAllianceEvents()
	{
		UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/reports/getAllianceMessages/");
		request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

		yield return request.Send();

		if(request.isError) {
			Debug.Log(request.error);
		}
		else {

			JSONObject obj = JSONObject.Create(request.downloadHandler.text);
			if (obj.IsString)
			{
				//failed
				if (onGetAllianceEvents != null)
				{
					onGetAllianceEvents(this, "fail");
				}
			}
			else
			{
				//success
				JSONObject eventsArray = JSONObject.Create(request.downloadHandler.text);

				allianceEvents = new ArrayList();

				foreach (JSONObject jEvent in eventsArray.list)
				{
					AllianceEventModel allianceEvent = JsonUtility.FromJson<AllianceEventModel>(jEvent.Print());

					allianceEvent.date = DateTime.Parse(jEvent["date"].str);

					allianceEvents.Add(allianceEvent);
				}

				if (onGetAllianceEvents != null)
				{
					onGetAllianceEvents(this, "success");
				}
			}    

		}
	}

	public void GotAllianceEvents(object sender, string value)
	{
		onGetAllianceEvents -= GotAllianceEvents;

		if (string.Equals(value, "sucess")) 
		{
			m_tableView.ReloadData();
		}
	}
}
