﻿using UnityEngine;
using System.Collections;
using Tacticsoft;

public class UpgradeBuildingTableController : MonoBehaviour, ITableViewDataSource {

    public UpgradeBuildingTableLine m_cellPrefab;
    public TableView m_tableView;
    public string content;
    private ArrayList buildingsList;

    public long item_id;
    public IReloadable owner;
    private long building_id;

    public int m_numRows;

    //Register as the TableView's delegate (required) and data source (optional)
    //to receive the calls
    void OnEnable() {
        m_tableView.dataSource = this;
        buildingsList = new ArrayList();
        switch (content)
        {
            case "BUILDINGS":
                foreach (BuildingModel building in GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings.Values)
                {
                    if (building.level != 10)
                    {
                        buildingsList.Add(building);
                    }
                }
                break;

            case "FIELDS":
                foreach (BuildingModel building in GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields.Values)
                {
                    if (building.level != 10)
                    {
                        buildingsList.Add(building);
                    }
                }
                break;

            case "VALLEYS":
                foreach (WorldFieldModel building in GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityValleys.Values)
                {
                    if (building.level != 10)
                    {
                        buildingsList.Add(building);
                    }
                }
                break;

            default:
                break;
        }
    }

    #region ITableViewDataSource

    //Will be called by the TableView to know how many rows are in this table
    public int GetNumberOfRowsForTableView(TableView tableView) {
        if (buildingsList == null)
        {
            return 0;
        }
        else
        {
            return buildingsList.Count;
        }
    }

    //Will be called by the TableView to know what is the height of each row
    public float GetHeightForRowInTableView(TableView tableView, int row) {
        return (m_cellPrefab.transform as RectTransform).rect.height;
    }

    //Will be called by the TableView when a cell needs to be created for display
    public TableViewCell GetCellForRowInTableView(TableView tableView, int row) {
        UpgradeBuildingTableLine cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as UpgradeBuildingTableLine;
        if (cell == null) {
            cell = (UpgradeBuildingTableLine)GameObject.Instantiate(m_cellPrefab);
        }

        cell.SetOwner(this);

        switch (content)
        {
            case "BUILDINGS":
                BuildingModel building = (BuildingModel)buildingsList[row];
                cell.SetId(building.id);
                cell.SetBuildingName(GlobalGOScript.instance.gameManager.buildingConstants[building.building].building_name.Replace("_", " "));
                cell.SetLevel(building.level);
                cell.SetIcon("UI/BuildingsIcon/" + building.building);
                break;

            case "FIELDS":
                BuildingModel field = (BuildingModel)buildingsList[row];
                cell.SetId(field.id);
                cell.SetBuildingName(GlobalGOScript.instance.gameManager.buildingConstants[field.building].building_name.Replace("_", " "));
                cell.SetLevel(field.level);
                cell.SetIcon("UI/FieldsIcon/" + field.building);
                break;

            case "VALLEYS":
                WorldFieldModel valley = (WorldFieldModel)buildingsList[row];
                cell.SetId(valley.id);
                cell.SetBuildingName(valley.cityType);
                cell.SetLevel(valley.level);
                cell.SetIcon("WorldMapBuildings/" + valley.cityType + "/1");
                break;

            default:
                break;
        }

        return cell;
    }

    public void UpgradeBuilding(long building_id)
    {
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += owner.ReloadTable;
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += BuildingUpgraded;
        this.building_id = building_id;
        if (string.Equals(content, "VALLEYS"))
        {
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.UseUpgrade(item_id, building_id, true));
        }
        else
        {
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.UseUpgrade(item_id, building_id, false));
        }
    }

    public void BuildingUpgraded(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed -= BuildingUpgraded;

        if (string.Equals(value, "success"))
        {
            if (string.Equals(content, "VALLEYS"))
            {
                for (int i = 0; i < buildingsList.Count; i++)
                {
                    if (((WorldFieldModel)buildingsList[i]).id == building_id)
                    {
                        buildingsList.RemoveAt(i);
                        break;
                    }
                }
            }
            else
            {
                for (int i = 0; i < buildingsList.Count; i++)
                {
                    if (((BuildingModel)buildingsList[i]).id == building_id)
                    {
                        buildingsList.RemoveAt(i);
                        break;
                    }
                }
            }

            m_tableView.ReloadData();
        }
    }

    #endregion
}
