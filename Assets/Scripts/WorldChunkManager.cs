﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class WorldChunkManager : MonoBehaviour {

    public long startX;
    public long startY;

    private WorldFieldModel[,] chunkFields;

    private event SimpleEvent onGetChunkInfo;

    public IEnumerator GetChunkInfo()
    {
        WWWForm unitForm = new WWWForm();
        unitForm.AddField("startX", startX.ToString());
        unitForm.AddField("startY", startY.ToString());
        unitForm.AddField("endX", (startX + 9).ToString());
        unitForm.AddField("endY", (startY + 9).ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/cities/allCitiesInRange/", unitForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (!obj.IsArray)
            {
                //failed
            }
            else
            {
                //success
                JSONObject fieldsArray = JSONObject.Create(request.downloadHandler.text);
                chunkFields = new WorldFieldModel[10, 10];

                WorldFieldModel field;

                for (int i = 0; i < fieldsArray.list.Count; i++)
                {
                    field = JsonUtility.FromJson<WorldFieldModel>(fieldsArray.list[i].Print());
                    JSONObject owner = fieldsArray.list[i].GetField("owner");
                    if (!owner.IsNull)
                    {
                        field.owner = JsonUtility.FromJson<SimpleUserModel>(owner.Print());
                    }
                    JSONObject city = fieldsArray.list[i].GetField("city");
                    if (!city.IsNull)
                    {
                        field.city = JsonUtility.FromJson<CityModel>(city.Print());
                    }
                    chunkFields[field.posY % 10, field.posX % 10] = field;
                }

                if (onGetChunkInfo != null)
                {
                    onGetChunkInfo(this, "");
                }
            }    

        }
    }

    private void InitChunkFields(object sender, string value)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject field = transform.GetChild(i).gameObject;
            WorldFieldManager manager = field.GetComponent<WorldFieldManager>();
            if (chunkFields[(manager.internalPitId / 10), (manager.internalPitId % 10)] != null)
            {
                manager.InitFieldState(chunkFields[(manager.internalPitId / 10), (manager.internalPitId % 10)]);
            }
        }
        
        onGetChunkInfo -= InitChunkFields;
    }

    public void ResetFieldsState()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.GetComponent<WorldFieldManager>().DisableField();
        }

        onGetChunkInfo += InitChunkFields;

        StartCoroutine(GetChunkInfo());
    }
}
