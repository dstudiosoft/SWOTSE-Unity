﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WorldMapCoordsChanger : MonoBehaviour {

    public InputField xInput;
    public InputField yInput;

    private TestWorldChunksManager chunksManager;

    void OnEnable()
    {
        GameObject world = GameObject.Find("GameWorld");

        if (world != null)
        {
            chunksManager = world.GetComponent<TestWorldChunksManager>();
        }
    }

    public void GoToCoords()
    {
        if (!string.IsNullOrEmpty(xInput.text) && !string.IsNullOrEmpty(yInput.text))
        {
            chunksManager.GoToCoords(long.Parse(xInput.text), long.Parse(yInput.text));
        }
    }

    public void GoHome()
    {
        long x = GlobalGOScript.instance.gameManager.cityManager.currentCity.posX;
        long y = GlobalGOScript.instance.gameManager.cityManager.currentCity.posY;
        chunksManager.GoToCoords(x, y);
    }
}
