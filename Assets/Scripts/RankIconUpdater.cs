﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RankIconUpdater : MonoBehaviour {

    public Image rankIcon;
    private string rank;

	// Use this for initialization
	void OnEnable () {
		//PlayerManager playerManager = GlobalGOScript.instance.playerManager;
		if (GlobalGOScript.instance == null)
			Debug.LogError ("instance null");
		
        if (GlobalGOScript.instance.playerManager != null)
        {
            rank = GlobalGOScript.instance.playerManager.user.rank;
            Sprite rankSprite = Resources.Load<Sprite>("UI/Rank/" + rank);
            rankIcon.sprite = rankSprite;
        }
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	
        if (!string.Equals(rank, GlobalGOScript.instance.playerManager.user.rank))
        {
            rank = GlobalGOScript.instance.playerManager.user.rank;
            Sprite rankSprite = Resources.Load<Sprite>("UI/Rank/" + rank);
            rankIcon.sprite = rankSprite;
            GlobalGOScript.instance.windowInstanceManager.openNotification("You have been promoted to rank" + GlobalGOScript.instance.playerManager.user.rank);
        }
	}
}
