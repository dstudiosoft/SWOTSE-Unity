﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;

public class BuildingsManager {

    public Dictionary<long, BuildingModel> cityBuildings;
    public Dictionary<long, BuildingModel> cityFields;
    public Dictionary<long, WorldFieldModel> cityValleys;

    public event SimpleEvent onBuildingUpdate;
	public event SimpleEvent onGotValleys;
    public event SimpleEvent onBuildingCancelled;

    public IEnumerator GetInitialCityBuildings()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/buildings/cityBuildingsList/"+GlobalGOScript.instance.gameManager.cityManager.currentCity.id+"/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            cityBuildings = new Dictionary<long, BuildingModel>();
            JSONObject buildingsArray = JSONObject.Create(request.downloadHandler.text, 2);

            foreach (JSONObject building in buildingsArray.list)
            {
                BuildingModel temp = JsonUtility.FromJson<BuildingModel>(building.Print());
                cityBuildings.Add(temp.pit_id, temp);
            }

            GlobalGOScript.instance.playerManager.loginManager.gotCityBuildings = true;

        }
    }

    public IEnumerator UpdateBuildingsAndFieldsState()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/buildings/cityBuildingsList/"+GlobalGOScript.instance.gameManager.cityManager.currentCity.id+"/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);

            if (onBuildingUpdate != null)
            {
                onBuildingUpdate(this, "fail");
            }
        }
        else {
            cityBuildings = new Dictionary<long, BuildingModel>();
            JSONObject buildingsArray = JSONObject.Create(request.downloadHandler.text, 2);

            foreach (JSONObject building in buildingsArray.list)
            {
                BuildingModel temp = JsonUtility.FromJson<BuildingModel>(building.Print());
                cityBuildings.Add(temp.pit_id, temp);
            }
        }

        request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/buildings/fieldsBuildingsList/"+GlobalGOScript.instance.gameManager.cityManager.currentCity.id+"/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
            if (onBuildingUpdate != null)
            {
                onBuildingUpdate(this, "fail");
            }
        }
        else {
            cityFields = new Dictionary<long, BuildingModel>();
            JSONObject fieldsArray = JSONObject.Create(request.downloadHandler.text, 2);

            foreach (JSONObject field in fieldsArray.list)
            {
                BuildingModel temp = JsonUtility.FromJson<BuildingModel>(field.Print());
                cityFields.Add(temp.pit_id, temp);
            }

            if (onBuildingUpdate != null)
            {
                onBuildingUpdate(this, "success");
            }
        }
    }

    public IEnumerator GetInitialCityFields()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/buildings/fieldsBuildingsList/"+GlobalGOScript.instance.gameManager.cityManager.currentCity.id+"/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            cityFields = new Dictionary<long, BuildingModel>();
            JSONObject fieldsArray = JSONObject.Create(request.downloadHandler.text, 2);

            foreach (JSONObject field in fieldsArray.list)
            {
                BuildingModel temp = JsonUtility.FromJson<BuildingModel>(field.Print());
                cityFields.Add(temp.pit_id, temp);
            }

            GlobalGOScript.instance.playerManager.loginManager.gotCityFields = true;

        }
    }

	public IEnumerator GetInitialCityValleys(bool flag)
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/cities/valleys/"+GlobalGOScript.instance.gameManager.cityManager.currentCity.id+"/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
			if (onGotValleys != null) 
			{
				onGotValleys (this, "fail");
			} 
        }
        else {
            cityValleys = new Dictionary<long, WorldFieldModel>();
            JSONObject valleysArray = JSONObject.Create(request.downloadHandler.text, 2);

            foreach (JSONObject valley in valleysArray.list)
			{	
                WorldFieldModel temp = JsonUtility.FromJson<WorldFieldModel>(valley.Print());
                cityValleys.Add(temp.id, temp);
            }

			if (flag) 
			{
				GlobalGOScript.instance.playerManager.loginManager.gotCityValleys = true;
			} 
            else 
			{
				if (onGotValleys != null) 
				{
					onGotValleys (this, "success");
				} 
			}
        }
    }


    public IEnumerator UpdateBuilding(long city, long pit_id, string command, string location)
    {

        string locationAdress = "CityBuilding";

        if (string.Equals(location, "Buildings"))
        {
            locationAdress = "CityBuilding";
        }
        else if (string.Equals(location, "Fields"))
        {
            locationAdress = "FieldsBuilding";
        }

        WWWForm buildForm = new WWWForm();
        buildForm.AddField("city", city.ToString());
        buildForm.AddField("pit_id", pit_id.ToString());
        buildForm.AddField("command", command);

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/buildings/update" + locationAdress + "/", buildForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (string.Equals(obj.str, "fail"))
            {
                //failed
                GlobalGOScript.instance.windowInstanceManager.openNotification("Operation failed!");
                if (onBuildingUpdate != null)
                {
                    onBuildingUpdate(this, "fail");
                }

            }
            else
            {
                //success
                BuildingModel building = null;

                if (string.Equals(location, "Fields"))
                {
                    building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pit_id];
                }
                else
                {
                    building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pit_id];
                }

//                if (string.Equals(command, "upgrade"))
//                {
//                    building.level++;
//                }
//                else
//                {
//                    building.level--;
//                }
                GlobalGOScript.instance.windowInstanceManager.openNotification("Operation successful!");
                if (onBuildingUpdate != null)
                {
                    onBuildingUpdate(this, "success");
                }
            }    

        }
    }

    public IEnumerator UpgradeValley(long valley_id)
    {
        WWWForm upgradeForm = new WWWForm();
        upgradeForm.AddField("valleyID", valley_id.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/cities/upgradeValley/", upgradeForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (string.Equals(obj.str, "sucess"))
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Operation successful!");
                if (onBuildingUpdate != null)
                {
                    onBuildingUpdate(this, "success");
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification(obj.str);
                if (onBuildingUpdate != null)
                {
                    onBuildingUpdate(this, "fail");
                }
            }    
        }
    }

    public IEnumerator DowngradeValley(long valley_id)
    {
        WWWForm downgradeForm = new WWWForm();
        downgradeForm.AddField("valleyID", valley_id.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/cities/downgradeValley/", downgradeForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (string.Equals(obj.str, "sucess"))
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Operation successful!");
                if (onBuildingUpdate != null)
                {
                    onBuildingUpdate(this, "success");
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification(obj.str);
                if (onBuildingUpdate != null)
                {
                    onBuildingUpdate(this, "fail");
                }
            }    
        }
    }


    public IEnumerator AddBuilding(string name, long city_id, long pit_id, string location)
    {
        WWWForm buildForm = new WWWForm();
        buildForm.AddField("building", name);
        buildForm.AddField("city", city_id.ToString());
        buildForm.AddField("pit_id", pit_id.ToString());
        buildForm.AddField("location", location);
        buildForm.AddField("level", 1.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/buildings/add_buildings/", buildForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                //failed
                GlobalGOScript.instance.windowInstanceManager.openNotification("Operation failed!");
                if (onBuildingUpdate != null)
                {
                    onBuildingUpdate(this, "fail");
                }
            }
            else
            {
                //success
                BuildingModel building = JsonUtility.FromJson<BuildingModel>(request.downloadHandler.text);

                if (string.Equals(location, "fields"))
                {
                    GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields.Add(building.pit_id, building);
                }
                else
                {
                    GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings.Add(building.pit_id, building);
                }

                if (onBuildingUpdate != null)
                {
                    onBuildingUpdate(this, "success");
                }
            }    

        }
    }

//    public IEnumerator DestroyBuilding(string location, long pit_id)
//    {
//        string locationAdress = "cityBuildings";
//        BuildingModel building = null;
//
//        if (string.Equals(location, "Buildings"))
//        {
//            locationAdress = "cityBuildings";
//            building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pit_id];
//        }
//        else if (string.Equals(location, "Fields"))
//        {
//            locationAdress = "fieldsBuildings";
//            building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pit_id];
//        }
//            
//
//        UnityWebRequest request = UnityWebRequest.Delete("http://"+GlobalGOScript.instance.host+":8889/buildings/"+locationAdress+"/"+building.id.ToString()+"/");
//        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);
//
//        yield return request.Send();
//
//        if(request.isError) {
//            Debug.Log(request.error);
//        }
//        else {
//
//            if (request.responseCode == 404)
//            {
//                //failed
//                GlobalGOScript.instance.windowInstanceManager.openNotification("Operation failed!");
//                if (onBuildingUpdate != null)
//                {
//                    onBuildingUpdate(this, "fail");
//                }
//            }
//            else if (request.responseCode == 204)
//            {
//                //success
////                if (string.Equals(location, "Fields"))
////                {
////                    GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields.Remove(building.pit_id);
////                }
////                else
////                {
////                    GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings.Remove(building.pit_id);
////                }
//                GlobalGOScript.instance.windowInstanceManager.openNotification("Operation successful!");
//
//                if (onBuildingUpdate != null)
//                {
//                    onBuildingUpdate(this, "success");
//                }
//            }    
//
//        }
//    }

    public IEnumerator DestroyBuilding(long buildingID)
    {
        WWWForm destroyForm = new WWWForm();
        destroyForm.AddField("buildingID", buildingID.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/buildings/delete_building/", destroyForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (!obj.IsString)
            {
                //failed
                if (onBuildingUpdate != null)
                {
                    onBuildingUpdate(this, "fail");
                }
            }
            else
            {
                if (string.Equals(obj.str, "success"))
                {
                    //success
                    if (onBuildingUpdate != null)
                    {
                        onBuildingUpdate(this, "success");
                    }
                }
                else
                {
                    if (onBuildingUpdate != null)
                    {
                        onBuildingUpdate(this, "fail");
                    }
                }
            }    

        }
    }

    public IEnumerator CancelBuilding(long event_id)
    {
        WWWForm cancelForm = new WWWForm();
        cancelForm.AddField("buildingEventID", event_id.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/updateEvents/cancelBuildingsEvent/", cancelForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (!obj.IsString)
            {
                //failed
                if (onBuildingCancelled != null)
                {
                    onBuildingCancelled(this, "fail");
                }
            }
            else
            {
                if (string.Equals(obj.str, "success"))
                {
                    //success
                    if (onBuildingCancelled != null)
                    {
                        onBuildingCancelled(this, "success");
                    }
                }
                else
                {
                    if (onBuildingCancelled != null)
                    {
                        onBuildingCancelled(this, "fail");
                    }
                }
            }    

        }
    }

    public IEnumerator CancelValley(long event_id)
    {
        WWWForm cancelForm = new WWWForm();
        cancelForm.AddField("worldMapEventID", event_id.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/updateEvents/cancelWorldMapEvent/", cancelForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (!obj.IsString)
            {
                //failed
                if (onBuildingCancelled != null)
                {
                    onBuildingCancelled(this, "fail");
                }
            }
            else
            {
                if (string.Equals(obj.str, "success"))
                {
                    //success
                    if (onBuildingCancelled != null)
                    {
                        onBuildingCancelled(this, "success");
                    }
                }
                else
                {
                    if (onBuildingCancelled != null)
                    {
                        onBuildingCancelled(this, "fail");
                    }
                }
            }    

        }
    }

    public bool IsBuildingPresent(string name)
    {
        foreach (BuildingModel building in cityBuildings.Values)
        {
            if (string.Equals(building.building, name))
            {
                if (!string.Equals(building.status, "creating"))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public long BuildingsLevelSum(string name)
    {
        long sum = 0;

        foreach (BuildingModel building in cityBuildings.Values)
        {
            if (string.Equals(building.building, name))
            {
                if (!string.Equals(building.status, "creating"))
                {
                    sum += building.level;
                }
            }
        }

        return sum;
    }
}
