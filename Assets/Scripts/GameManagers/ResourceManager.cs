﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Globalization;

public class ResourceManager : MonoBehaviour {

    public ResourcesModel cityResources;

    public event SimpleEvent onGetInitialResources;

    public Coroutine resourceCoroutine;
    public bool resourcesWorking = true;

    public bool buildingEvents = false;
    public bool unitEvents = false;
    public bool armyEvents = false;
    public bool itemEvents = false;
    public bool worldMapEvents = false;
	public bool newCityEvents = false;

    public bool buildingTimers = false;
    public bool unitTimers = false;
    public bool armyTimers = false;
    public bool itemTimers = false;
    public bool worldMapTimers = false;
	public bool newCityTimers = false;

	private string dateFormat = "yyyy-MM-ddTHH:mm:ss.ffffffZ";

    public IEnumerator GetInitialReourcesForCity()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/resources/resources/"+GlobalGOScript.instance.gameManager.cityManager.currentCity.id+"/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject response = JSONObject.Create(request.downloadHandler.text);
//            cityResources = JsonUtility.FromJson<ResourcesModel>(response["resources"].Print());
            cityResources = JsonUtility.FromJson<ResourcesModel>(response.Print());
            cityResources.city = GlobalGOScript.instance.gameManager.cityManager.currentCity.id;
            JSONObject resourcesObj = JSONObject.Create(request.downloadHandler.text, 2);
			cityResources.last_update = DateTime.ParseExact(resourcesObj.GetField("last_update").str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);

            GlobalGOScript.instance.playerManager.loginManager.gotCityResources = true;

            if (onGetInitialResources != null)
            {
                onGetInitialResources(this, string.Empty);
            }
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            StopCoroutine(resourceCoroutine);
            resourcesWorking = false;
        }
        else
        {
            resourceCoroutine = StartCoroutine(getResources());
        }
    }

    public void StartResources()
    {
        if (resourceCoroutine != null)
        {
            StopCoroutine(resourceCoroutine);
        }

        resourceCoroutine = StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.resourceManager.getResources());
    }

    public IEnumerator getResources()
    {
        JSONObject obj = JSONObject.Create(JSONObject.Type.OBJECT);
        obj.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id);
        obj.AddField("token", GlobalGOScript.instance.token.ToString());
        WebSocket w = new WebSocket(new Uri("ws://"+GlobalGOScript.instance.host+":8890/ws"));
        yield return StartCoroutine(w.Connect());
        resourcesWorking = true;
        while (resourcesWorking)
        {
            w.SendString(obj.Print());
            string reply = w.RecvString();
            if (w.error != null)
            {
                resourceCoroutine = StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.resourceManager.getResources());
                yield break;
            }

            if (reply != null)
            {
                if (string.Equals(reply, "fail"))
                {
                    GlobalGOScript.instance.LogOut();
                    StopAllCoroutines();
                    resourcesWorking = false;
                    yield break;
                }

                JSONObject resourcesObj = JSONObject.Create(reply);

                GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources  = JsonUtility.FromJson<ResourcesModel>(resourcesObj.GetField("resources").Print());
                cityResources.city = GlobalGOScript.instance.gameManager.cityManager.currentCity.id;
				cityResources.last_update = DateTime.ParseExact(resourcesObj["resources"]["last_update"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                GlobalGOScript.instance.gameManager.cityManager.currentCity.happiness = resourcesObj["cityStats"]["happiness"].i;
                GlobalGOScript.instance.gameManager.cityManager.currentCity.courage = resourcesObj["cityStats"]["courage"].i;
                GlobalGOScript.instance.gameManager.cityManager.currentCity.max_population = resourcesObj["cityStats"]["max_population"].i;
                GlobalGOScript.instance.gameManager.cityManager.currentCity.gold_tax_rate = resourcesObj["cityStats"]["gold_tax_rate"].i;
                GlobalGOScript.instance.gameManager.cityManager.currentCity.current_population = resourcesObj["resources"]["population"].i;

                GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusWoodDate = DateTime.ParseExact(resourcesObj["resources"]["next_bonus_date_wood"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusFoodDate = DateTime.ParseExact(resourcesObj["resources"]["next_bonus_date_food"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusStoneDate = DateTime.ParseExact(resourcesObj["resources"]["next_bonus_date_stone"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusIronDate = DateTime.ParseExact(resourcesObj["resources"]["next_bonus_date_iron"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusGoldDate = DateTime.ParseExact(resourcesObj["resources"]["next_bonus_date_gold"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusSilverDate = DateTime.ParseExact(resourcesObj["resources"]["next_bonus_date_silver"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusCopperDate = DateTime.ParseExact(resourcesObj["resources"]["next_bonus_date_cooper"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);

                GlobalGOScript.instance.playerManager.user.rank = resourcesObj["user"]["rank"].str;
                GlobalGOScript.instance.playerManager.user.alliance_rank = resourcesObj["user"]["alliance_rank"].str;
                //add other values

                GlobalGOScript.instance.playerManager.reportManager.unreadBattleCount = resourcesObj["reports"]["battle"].i;
                GlobalGOScript.instance.playerManager.reportManager.unreadInboxCount = resourcesObj["reports"]["message"].i;
                GlobalGOScript.instance.playerManager.reportManager.unreadSystemCount = resourcesObj["reports"]["system"].i;

                if (GlobalGOScript.instance.playerManager.reportManager.unreadInboxCount > 0 || GlobalGOScript.instance.playerManager.reportManager.unreadSystemCount > 0)
                {
                    GlobalGOScript.instance.newMailNotification.SetActive(true);
                }
                else
                {
                    GlobalGOScript.instance.newMailNotification.SetActive(false);
                }

                if (resourcesObj["events"]["isUnreadBuildingEvents"].b)
                {
                    buildingEvents = true;
                    GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate += GlobalGOScript.instance.gameManager.levelLoadManager.UpdateCityBuildings;
                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.UpdateBuildingsAndFieldsState());
                }

                if (resourcesObj["events"]["isUnreadUnitEvents"].b)
                {
                    unitEvents = true;
                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.UpdateCityUnits());
                }

                if (resourcesObj["events"]["isUnreadArmyEvents"].b)
                {
                    armyEvents = true;
                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.UpdateCityUnits());
                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.UpdateCityGenerals());
                }

                if (resourcesObj["events"]["isUnreadCustomEvents"].b)
                {
                    itemEvents = true;
//                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.UpdateCityUnits());
//                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.UpdateCityGenerals());
                }

                if (resourcesObj["events"]["isUnreadWorldMapEvents"].b)
                {
                    worldMapEvents = true;
                    GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onGotValleys += GlobalGOScript.instance.cityChanger.UpdateWorldMap;
                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.GetInitialCityValleys(false));
                }

				if (resourcesObj["events"]["isUnreadCityEvent"].b)
				{
					newCityEvents = true;
					GlobalGOScript.instance.playerManager.onGetMyUserInfo += GlobalGOScript.instance.cityChanger.UpdateWorldMap;
					StartCoroutine(GlobalGOScript.instance.playerManager.getMyUserInfo(false));
				}

                if (resourcesObj["events"]["isNewBuildingEvent"].b)
                {
                    buildingTimers = true;
                    StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetBuildingTimers(false));
                }

                if (resourcesObj["events"]["isNewUnitEvent"].b)
                {
                    unitTimers = true;
                    StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetUnitTimers(false));
                }

                if (resourcesObj["events"]["isNewArmyEvent"].b)
                {
                    armyTimers = true;
                    StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetArmyTimers(false));
                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.UpdateCityGenerals());
                }

                if (resourcesObj["events"]["isNewCustomEvent"].b)
                {
                    itemTimers = true;
                    StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetItemTimers(false));
                }

                if (resourcesObj["events"]["isNewWorldMapEvent"].b)
                {
                    worldMapTimers = true;
                    StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetWorldMapTimers(false));
                }

				if (resourcesObj["events"]["isNewCityEvent"].b)
				{
					newCityTimers = true;
					StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetWorldMapTimers(false));
				}

                if (buildingEvents)
                {
                    StartCoroutine(ReadIsUnreadBuildingEvents());
                }

                if (unitEvents)
                {
                    StartCoroutine(ReadIsUnreadUnitEvents());
                }

                if (armyEvents)
                {
                    StartCoroutine(ReadIsUnreadArmyEvents());
                }

                if (itemEvents)
                {
                    StartCoroutine(ReadIsUnreadCustomEvents());
                }

                if (worldMapEvents)
                {
                    StartCoroutine(ReadIsUnreadWorldMapEvents());
                }

				if (newCityEvents)
				{
					StartCoroutine(ReadIsUnreadNewCityEvents());
				}

                if (buildingTimers)
                {
                    StartCoroutine(ReadIsNewBuildingEvent());
                    StartCoroutine(GlobalGOScript.instance.playerManager.getMyUserInfo(false));
                }

                if (unitTimers)
                {
                    StartCoroutine(ReadIsNewUnitEvent());
                    StartCoroutine(GlobalGOScript.instance.playerManager.getMyUserInfo(false));
                }

                if (armyTimers)
                {
                    StartCoroutine(ReadIsNewArmyEvent());
                    StartCoroutine(GlobalGOScript.instance.playerManager.getMyUserInfo(false));
                }

                if (itemTimers)
                {
                    StartCoroutine(ReadIsNewCustomEvent());
                    StartCoroutine(GlobalGOScript.instance.playerManager.getMyUserInfo(false));
                }

                if (worldMapTimers)
                {
                    StartCoroutine(ReadIsNewWorldMapEvent());
                    StartCoroutine(GlobalGOScript.instance.playerManager.getMyUserInfo(false));
                }

				if (newCityTimers)
				{
					StartCoroutine(ReadIsNewCityEvent());
					StartCoroutine(GlobalGOScript.instance.playerManager.getMyUserInfo(false));
				}
            }
            if (w.error != null)
            {
                Debug.LogError ("Error: "+w.error);
                resourcesWorking = false;
                w.Close();
                resourceCoroutine = null;
                yield break;
            }
            yield return new WaitForSeconds(1.0f);
        }
        w.Close();
        resourceCoroutine = null;
    }

    public IEnumerator ReadIsNewArmyEvent()
    {
        WWWForm readForm = new WWWForm();
        readForm.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());
        readForm.AddField("currentTime", GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/updateEvents/readIsNewArmyEvent/", readForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            armyTimers = false;
        }
    }

    public IEnumerator ReadIsNewBuildingEvent()
    {
        WWWForm readForm = new WWWForm();
        readForm.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());
        readForm.AddField("currentTime", GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/updateEvents/readIsNewBuildingEvent/", readForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            buildingTimers = false;
        }
    }

    public IEnumerator ReadIsNewUnitEvent()
    {
        WWWForm readForm = new WWWForm();
        readForm.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());
        readForm.AddField("currentTime", GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/updateEvents/readIsNewUnitEvent/", readForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            unitTimers = false;
        }
    }

    public IEnumerator ReadIsNewCustomEvent()
    {
        WWWForm readForm = new WWWForm();
        readForm.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());
        readForm.AddField("currentTime", GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/updateEvents/readIsNewCustomEvent/", readForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            itemTimers = false;
        }
    }

    public IEnumerator ReadIsNewWorldMapEvent()
    {
        WWWForm readForm = new WWWForm();
        readForm.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());
        readForm.AddField("currentTime", GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/updateEvents/readIsNewWorldMapEvent/", readForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            worldMapTimers = false;
        }
    }

	public IEnumerator ReadIsNewCityEvent()
	{
		WWWForm readForm = new WWWForm();
		readForm.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());
        readForm.AddField("currentTime", GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/updateEvents/readIsNewCityEvent/", readForm);
		request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

		yield return request.Send();

		if (request.isError)
		{
			Debug.Log(request.error);
		}
		else
		{
			newCityTimers = false;
		}
	}

    public IEnumerator ReadIsUnreadArmyEvents()
    {
        WWWForm readForm = new WWWForm();
        readForm.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());
        readForm.AddField("currentTime", GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/updateEvents/readIsUnreadArmyEvents/", readForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            armyEvents = false;
        }
    }

    public IEnumerator ReadIsUnreadBuildingEvents()
    {
        WWWForm readForm = new WWWForm();
        readForm.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());
        readForm.AddField("currentTime", GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/updateEvents/readIsUnreadBuildingEvents/", readForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            buildingEvents = false;
        }
    }

    public IEnumerator ReadIsUnreadUnitEvents()
    {
        WWWForm readForm = new WWWForm();
        readForm.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());
        readForm.AddField("currentTime", GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/updateEvents/readIsUnreadUnitEvents/", readForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            unitEvents = false;
        }
    }

    public IEnumerator ReadIsUnreadCustomEvents()
    {
        WWWForm readForm = new WWWForm();
        readForm.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());
        readForm.AddField("currentTime", GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/updateEvents/readIsUnreadCustomEvents/", readForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            itemEvents = false;
        }
    }

    public IEnumerator ReadIsUnreadWorldMapEvents()
    {
        WWWForm readForm = new WWWForm();
        readForm.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());
        readForm.AddField("currentTime", GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/updateEvents/readIsUnreadWorldMapEvents/", readForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            worldMapEvents = false;
        }
    }

	public IEnumerator ReadIsUnreadNewCityEvents()
	{
		WWWForm readForm = new WWWForm();
		readForm.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());
        readForm.AddField("currentTime", GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/updateEvents/readIsUnreadCityEvents/", readForm);
		request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

		yield return request.Send();

		if (request.isError)
		{
			Debug.Log(request.error);
		}
		else
		{
			newCityEvents = false;
		}
	}
}
