﻿#if UNITY_IOS || UNITY_ANDROID
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using Firebase.Messaging;

public class GCMManager : MonoBehaviour {

	void Start () 
    {
        if (PlayerPrefs.HasKey("deviceToken"))
        {
            FirebaseMessaging.MessageReceived += OnMessageRecieved;
            StartCoroutine(RegisterDeviceToken(PlayerPrefs.GetString("deviceToken")));
        }
        else
        {
            FirebaseMessaging.TokenReceived += OnTokenRecieved;
        }
	}

    public void OnTokenRecieved(object sender, TokenReceivedEventArgs e)
    {
        Debug.Log("Token Recieved: " + e.Token);
        PlayerPrefs.SetString("deviceToken", e.Token);
        PlayerPrefs.Save();
        StartCoroutine(RegisterDeviceToken(e.Token));
    }

    public void OnMessageRecieved(object sender, MessageReceivedEventArgs e)
    {
        Debug.Log("Message Recieved: " + e.Message.Notification.Body);
    }

    IEnumerator RegisterDeviceToken(string tokenString)
    {
        string addressString = null;

        #if UNITY_ANDROID
        addressString = "register_android_device";
        #elif UNITY_IOS
        addressString = "register_ios_device";
        #endif

        WWWForm userForm = new WWWForm();
        userForm.AddField("token", tokenString);

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/notifications/" + addressString + "/", userForm);
        request.SetRequestHeader("authorization", "Token "+ GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    Debug.Log("Successfully registered device token");
                }
                else
                {
                    Debug.Log(obj.str);
                }
            }
            else
            {
                Debug.Log(request.downloadHandler.text);
            }    
        }
    }
}
#endif