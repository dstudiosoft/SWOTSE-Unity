﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LevelLoadManager : MonoBehaviour {

    public GameObject mainUI;
    public GameObject touchManager;
    public GameObject zoomMenu;
    public GameObject coordsMenu;
    public ADManager adManager;

    void Start()
    {
        SceneManager.sceneLoaded += LevelLoaded;
        adManager = GlobalGOScript.instance.gameObject.GetComponent<ADManager>();
    }

    void LevelLoaded(Scene level, LoadSceneMode mode)
    {
        TouchScript.Gestures.OrthographicCameraTransformGesture transGesture = Camera.main.GetComponent<TouchScript.Gestures.OrthographicCameraTransformGesture>();
        switch (level.buildIndex)
        {
            case 0:
                break;
            case 1:
                LoginRegistrationContentManager loginManager = GameObject.Find("/Canvas/loginSection").GetComponent<LoginRegistrationContentManager>();
                GlobalGOScript.instance.playerManager.loginManager.onGetEmperors += loginManager.InitializeEmperorsDropdown;
                StartCoroutine(GlobalGOScript.instance.playerManager.loginManager.GetEmperors());
                Camera.main.orthographicSize = 5.0f;
                Camera.main.transform.position = new Vector3(0.0f, 0.0f, -10.0f);
                transGesture.worldMap = false;
                adManager.requestBannerAd();
                break;
            case 2:
                if (!mainUI.activeInHierarchy)
                {
                    GlobalGOScript.instance.windowInstanceManager.closeLoginProgressbar();

                    #if UNITY_IOS || UNITY_ANDROID
                    if (!GlobalGOScript.instance.gameObject.GetComponent<GCMManager>())
                    {
                        GlobalGOScript.instance.messagesManager = GlobalGOScript.instance.gameObject.AddComponent<GCMManager>();
                    }
                    #endif

                    mainUI.SetActive(true);
                    if (!PlayerPrefs.HasKey("alreadyLaunched"))
                    {
                        PlayerPrefs.SetInt("alreadyLaunched", 1);
                        GlobalGOScript.instance.windowInstanceManager.openTutorial();
                    }
                }
                transGesture.worldMap = false;
                Camera.main.orthographicSize = 5.0f;
                Camera.main.transform.position = new Vector3(0.0f, 0.0f, -10.0f);
                mainUI.SetActive(true);
                touchManager.SetActive(true);
                if(coordsMenu != null)
                    coordsMenu.SetActive(false);
//                if (zoomMenu != null)
//                    zoomMenu.SetActive(true);
                break;
            case 3:
                transGesture.worldMap = false;
                Camera.main.orthographicSize = 5.0f;
                Camera.main.transform.position = new Vector3(0.0f, 0.0f, -10.0f);
                coordsMenu.SetActive(false);
//                zoomMenu.SetActive(true);
                break;
            case 4:
                Camera.main.orthographicSize = 5.0f;
                Camera.main.transform.position = new Vector3(0.0f, 0.0f, -10.0f);
                GameObject gameWorld = GameObject.Find("GameWorld");
                TestWorldChunksManager chunksManager = gameWorld.GetComponent<TestWorldChunksManager>();
                transGesture.chuncksManager = chunksManager;
                transGesture.worldMap = true;
                transGesture.mapGameWorld = gameWorld;
                transGesture.mapBackground = GameObject.Find("Quad").GetComponent<MeshRenderer>().material;
                chunksManager.targetX = GlobalGOScript.instance.gameManager.cityManager.currentCity.posX;
                chunksManager.targetY = GlobalGOScript.instance.gameManager.cityManager.currentCity.posY;
                chunksManager.InitWorldMap();
                coordsMenu.SetActive(true);
//                zoomMenu.SetActive(false);
                break;
            default:
                break;
        }
    }

    public void UpdateCityBuildings(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate -= UpdateCityBuildings;
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed -= UpdateCityBuildings;

        if (string.Equals(value, "success"))
        {
            if (SceneManager.GetActiveScene().buildIndex != 4)
            {
                GameObject gameworld = GameObject.Find("GameWorld");
                gameworld.SetActive(false);
                gameworld.SetActive(true);
            }
        }
    }

    public void LoadLogin()
    {
        if (SceneManager.GetActiveScene().buildIndex != 1)
        {
            SceneManager.LoadScene(1);
        }
    }

    public void LoadCity()
    {
        if (SceneManager.GetActiveScene().buildIndex != 2)
        {
            SceneManager.LoadScene(2);
        }
    }

    public void LoadFields()
    {
        if (SceneManager.GetActiveScene().buildIndex != 3)
        {
            SceneManager.LoadScene(3);
        }
    }

    public void LoadWorld()
    {
        if (SceneManager.GetActiveScene().buildIndex != 4)
        {
            SceneManager.LoadScene(4);
        }
    }
}
