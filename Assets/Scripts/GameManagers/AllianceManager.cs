﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Globalization;

public class AllianceManager {

    public AllianceModel userAlliance;

    public Dictionary<long, UserModel> allianceMembers;
    public Dictionary<long, AllianceModel> allAlliances;

    public event SimpleEvent onGetAllianceMembers;
    public event SimpleEvent onGetAlliances;
    public event SimpleEvent onExcludeUser;
    public event SimpleEvent onInviteUser;
    public event SimpleEvent onRequestJoin;
    public event SimpleEvent onQuitAlliance;
    public event SimpleEvent onUpdateGuideline;
    public event SimpleEvent onAcceptInvite;
    public event SimpleEvent onCancelInvite;
    public event SimpleEvent onAddAlly;
    public event SimpleEvent onAddEnemy;
    public event SimpleEvent onRemoveAlly;
    public event SimpleEvent onRemoveEnemy;
    public event SimpleEvent onCreateNewAlliance;

    private string dateFormat = "yyyy-MM-ddTHH:mm:ss.ffffffZ";

    public IEnumerator AllianceMemebers()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/alliances/allianceMembers/" + userAlliance.id.ToString() + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                //failed
                if (onGetAllianceMembers != null)
                {
                    onGetAllianceMembers(this, "fail");
                }
            }
            else
            {
                //success
                JSONObject membersArray = JSONObject.Create(request.downloadHandler.text);

                allianceMembers = new Dictionary<long, UserModel>();

                foreach (JSONObject member in membersArray.list)
                {
                    UserModel allianceMember = JsonUtility.FromJson<UserModel>(member.Print());

                    if (allianceMember.id == GlobalGOScript.instance.playerManager.user.id)
                        continue;

//                    AllianceModel userAlliance = JsonUtility.FromJson<AllianceModel>(userObj["alliance"].Print());
//
//                    userAlliance.founderId = userObj["alliance"]["founder"]["id"].i;
//                    userAlliance.founderName = userObj["alliance"]["founder"]["username"].str;
//
//                    userAlliance.leaderId = userObj["alliance"]["leader"]["id"].i;
//                    userAlliance.leaderName = userObj["alliance"]["leader"]["username"].str;
//
//                    user.alliance = userAlliance;
                    if (!member["last_login"].IsNull)
                    {
                        allianceMember.last_login = DateTime.ParseExact(member["last_login"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                    }
                    
                    MyCityModel memberCity = JsonUtility.FromJson<MyCityModel>(member["capital"].Print());
                    allianceMember.capital = memberCity;
					//Debug.Log ("RANK MEMBERS = " + allianceMember.rank);
                    allianceMembers.Add(allianceMember.id, allianceMember);
                }

                if (onGetAllianceMembers != null)
                {
                    onGetAllianceMembers(this, "success");
                }
            }    

        }
    }

    public IEnumerator ExcludeUser(long userId)
    {
        WWWForm excludeForm = new WWWForm();
        excludeForm.AddField("userID", userId.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/alliances/excludeUser/", excludeForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    allianceMembers.Remove(userId);

                    if (onExcludeUser != null)
                    {
                        onExcludeUser(this, "success");
                    }
                }
                else
                {
                    if (onExcludeUser != null)
                    {
                        onExcludeUser(this, "fail");
                    }
                }
            }
            else
            {
                if (onExcludeUser != null)
                {
                    onExcludeUser(this, "fail");
                }
            }    
        }
    }

    public IEnumerator InviteUser(long userId)
    {
        WWWForm inviteForm = new WWWForm();
        inviteForm.AddField("invitedUser", userId.ToString());
        inviteForm.AddField("alliance", userAlliance.id.ToString());


        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/alliances/inviteUser/", inviteForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {

                    if (onInviteUser != null)
                    {
                        onInviteUser(this, "success");
                    }
                }
                else
                {
                    if (onInviteUser != null)
                    {
                        onInviteUser(this, "fail");
                    }
                }
            }
            else
            {
                if (onInviteUser != null)
                {
                    onInviteUser(this, "fail");
                }
            }    
        }
    }

    public IEnumerator RequestJoin(long allianceId)
    {
        WWWForm requestForm = new WWWForm();
        requestForm.AddField("alliance", allianceId.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/alliances/allianceRequest/", requestForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {

                    if (onRequestJoin != null)
                    {
                        GlobalGOScript.instance.windowInstanceManager.openNotification("Join request sent");
                        onRequestJoin(this, "success");
                    }
                }
                else
                {
                    if (onRequestJoin != null)
                    {
                        GlobalGOScript.instance.windowInstanceManager.openNotification("Join request not sent");
                        onRequestJoin(this, "fail");
                    }
                }
            }
            else
            {
                if (onRequestJoin != null)
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Join request not sent");
                    onRequestJoin(this, "fail");
                }
            }    
        }
    }

    public IEnumerator AllAlliances()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/alliances/allAlliances/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                //failed
                if (onGetAlliances != null)
                {
                    onGetAlliances(this, "fail");
                }
            }
            else
            {
                //success
                JSONObject alliancesArray = JSONObject.Create(request.downloadHandler.text);

                allAlliances = new Dictionary<long, AllianceModel>();

                if (alliancesArray.list.Count != 0)
                {
                    foreach (JSONObject allianceObject in alliancesArray.list)
                    {
                        AllianceModel alliance = JsonUtility.FromJson<AllianceModel>(allianceObject.Print());

                        alliance.founderId = allianceObject["founder"]["id"].i;
                        alliance.founderName = allianceObject["founder"]["username"].str;

                        alliance.leaderId = allianceObject["leader"]["id"].i;
                        alliance.leaderName = allianceObject["leader"]["username"].str;

                        allAlliances.Add(alliance.id, alliance);
                    }

                    if (onGetAlliances != null)
                    {
                        onGetAlliances(this, "success");
                    }
                }
            }    
        }
    }

    public IEnumerator QuitAlliance()
    {
        WWWForm quitForm = new WWWForm();
        quitForm.AddField("sonething", "something");

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/alliances/goOutAlliance/", quitForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    userAlliance = null;
                    GlobalGOScript.instance.playerManager.user.alliance = null;
                    GlobalGOScript.instance.playerManager.user.alliance_rank = "none";
                    allianceMembers = null;

                    if (onQuitAlliance != null)
                    {
                        onQuitAlliance(this, "success");
                    }
                }
                else
                {
                    if (onQuitAlliance != null)
                    {
                        onQuitAlliance(this, "fail");
                    }
                }
            }
            else
            {
                if (onQuitAlliance != null)
                {
                    onQuitAlliance(this, "fail");
                }
            }    
        }
    }

    public IEnumerator UpdateGuideline(string guideline)
    {
        WWWForm requestForm = new WWWForm();
        requestForm.AddField("guideline", guideline);

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/alliances/updateGuideline/", requestForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    userAlliance.guideline = guideline;

                    if (onUpdateGuideline != null)
                    {
                        onUpdateGuideline(this, "success");
                    }
                }
                else
                {
                    if (onUpdateGuideline != null)
                    {
                        onUpdateGuideline(this, "fail");
                    }
                }
            }
            else
            {
                if (onUpdateGuideline != null)
                {
                    onUpdateGuideline(this, "fail");
                }
            }    
        }
    }

    public IEnumerator AcceptInvite(long inviteId)
    {
        WWWForm requestForm = new WWWForm();
        requestForm.AddField("inviteID", inviteId.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/alliances/acceptInvite/", requestForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (!obj.IsString)
            {
                //update invites
                if (GlobalGOScript.instance.playerManager.reportManager.invites.ContainsKey(inviteId))
                {
                    GlobalGOScript.instance.playerManager.reportManager.invites.Remove(inviteId);
                }
                else if (GlobalGOScript.instance.playerManager.reportManager.allianceRequsets.ContainsKey(inviteId))
                {
                    GlobalGOScript.instance.playerManager.reportManager.allianceRequsets.Remove(inviteId);
                }

                AllianceModel alliance = JsonUtility.FromJson<AllianceModel>(obj.Print());

                alliance.founderId = obj["founder"]["id"].i;
                alliance.founderName = obj["founder"]["username"].str;

                alliance.leaderId = obj["leader"]["id"].i;
                alliance.leaderName = obj["leader"]["username"].str;

                GlobalGOScript.instance.playerManager.user.alliance = alliance;
                //rank update
                userAlliance = alliance;

                if (onAcceptInvite != null)
                {
                    onAcceptInvite(this, "success");
                }
            }
            else
            {
                if (onAcceptInvite != null)
                {
                    onAcceptInvite(this, "fail");
                }
            }    
        }
    }

    public IEnumerator CancelInvite(long inviteId)
    {
        WWWForm requestForm = new WWWForm();
        requestForm.AddField("inviteID", inviteId.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/alliances/cancelInvite/", requestForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    if (GlobalGOScript.instance.playerManager.reportManager.invites.ContainsKey(inviteId))
                    {
                        GlobalGOScript.instance.playerManager.reportManager.invites.Remove(inviteId);
                    }
                    else if (GlobalGOScript.instance.playerManager.reportManager.allianceRequsets.ContainsKey(inviteId))
                    {
                        GlobalGOScript.instance.playerManager.reportManager.allianceRequsets.Remove(inviteId);
                    }

                    if (onCancelInvite != null)
                    {
                        onCancelInvite(this, "success");
                    }
                }
                else
                {
                    if (onCancelInvite != null)
                    {
                        onCancelInvite(this, "fail");
                    }
                }
            }
            else
            {
                if (onCancelInvite != null)
                {
                    onCancelInvite(this, "fail");
                }
            }    
        }
    }

    public IEnumerator SetMemberRank(string rank, long userId)
    {
        WWWForm rankForm = new WWWForm();
        rankForm.AddField("userID", userId.ToString());
        rankForm.AddField("alliance_rank", rank);

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/alliances/setRank/", rankForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    //success
                    allianceMembers[userId].alliance_rank = rank;
                }
                else
                {
                    //fail
                }
            }
            else
            {
                //fail
            }    
        }
    }

    public IEnumerator ResignRank()
    {
        WWWForm rankForm = new WWWForm();
        rankForm.AddField("alliance_rank", GlobalGOScript.instance.playerManager.user.alliance_rank);

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/alliances/resignRank/", rankForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    //success
                    switch (GlobalGOScript.instance.playerManager.user.alliance_rank)
                    {
                        case "Founder":
                            GlobalGOScript.instance.playerManager.user.alliance_rank = "Leader";
                            break;

                        case "Leader":
                            GlobalGOScript.instance.playerManager.user.alliance_rank = "Vice Host";
                            break;

                        case "Vice Host":
                            GlobalGOScript.instance.playerManager.user.alliance_rank = "Commander";
                            break;

                        case "Commander":
                            GlobalGOScript.instance.playerManager.user.alliance_rank = "Soldier";
                            break;

                        default:
                            break;
                    }
                }
                else
                {
                    //fail
                }
            }
            else
            {
                //fail
            }    
        }
    }

    public IEnumerator AddAlly(long allianceId)
    {
        WWWForm addForm = new WWWForm();
        addForm.AddField("allianceID", userAlliance.id.ToString());
        addForm.AddField("allyID", allianceId.ToString());


        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/alliances/addAlly/", addForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (onAddAlly != null)
                {
                    onAddAlly(this, "fail");
                }
            }
            else
            {
                userAlliance.allies.Add(allianceId);
                if (onAddAlly != null)
                {
                    onAddAlly(allianceId, "success");
                }
            }    
        }
    }

    public IEnumerator RemoveAlly(long allianceId)
    {
        WWWForm removeForm = new WWWForm();
        removeForm.AddField("allianceID", userAlliance.id.ToString());
        removeForm.AddField("allyID", allianceId.ToString());


        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/alliances/removeAlly/", removeForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (onRemoveAlly != null)
                {
                    onRemoveAlly(this, "fail");
                }
            }
            else
            {
                userAlliance.allies.Remove(allianceId);
                if (onRemoveAlly != null)
                {
                    onRemoveAlly(allianceId, "success");
                }
            }    
        }
    }

    public IEnumerator AddEnemy(long allianceId)
    {
        WWWForm addForm = new WWWForm();
        addForm.AddField("allianceID", userAlliance.id.ToString());
        addForm.AddField("enemyID", allianceId.ToString());


        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/alliances/addEnemy/", addForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (onAddEnemy != null)
                {
                    onAddEnemy(this, "fail");
                }
            }
            else
            {
                userAlliance.enemies.Add(allianceId);
                if (onAddEnemy != null)
                {
                    onAddEnemy(allianceId, "success");
                }
            }    
        }
    }

    public IEnumerator RemoveEnemy(long allianceId)
    {
        WWWForm removeForm = new WWWForm();
        removeForm.AddField("allianceID", userAlliance.id.ToString());
        removeForm.AddField("enemyID", allianceId.ToString());


        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/alliances/removeEnemy/", removeForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (onRemoveEnemy != null)
                {
                    onRemoveEnemy(this, "fail");
                }
            }
            else
            {
                userAlliance.enemies.Remove(allianceId);
                if (onRemoveEnemy != null)
                {
                    onRemoveEnemy(allianceId, "success");
                }
            }    
        }
    }

    public IEnumerator CreateNewAllliance(string name)
    {
        WWWForm newForm = new WWWForm();
        newForm.AddField("allianceName", name);

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/alliances/createNewAlliance/", newForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    onCreateNewAlliance.Invoke(this, "success");
                }
                else
                {
                    onCreateNewAlliance.Invoke(this, "fail");
                }
            }
            else
            {
                onCreateNewAlliance.Invoke(this, "fail");
            }    
        }
    }
}
