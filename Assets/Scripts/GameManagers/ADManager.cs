﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.Networking;
using GoogleMobileAds.Api;

public class ADManager : MonoBehaviour {

    #if UNITY_EDITOR
    static string adBannerUnitId = "unused";
    static string adInterstitialUnitId = "unused";
    #elif UNITY_ANDROID
    static string adBannerUnitId = "ca-app-pub-6112664567302645/4199591667";
    static string adInterstitialUnitId = "ca-app-pub-6112664567302645/2630428854";
    #elif UNITY_IOS
    static string adBannerUnitId = "ca-app-pub-6112664567302645/8906011482";
    static string adInterstitialUnitId = "ca-app-pub-6112664567302645/7326731070";
    #else
    static string adBannerUnitId = "unused";
    static string adInterstitialUnitId = "unused";
    #endif

    public GameObject UI;

    BannerView bannerView;
    InterstitialAd interstitial;

	RewardBasedVideoAd rewardBasedVideo;


    public DateTime nextBonusWoodDate = DateTime.Now.AddYears(1);
    public DateTime nextBonusFoodDate = DateTime.Now.AddYears(1);
    public DateTime nextBonusStoneDate = DateTime.Now.AddYears(1);
    public DateTime nextBonusIronDate = DateTime.Now.AddYears(1);
    public DateTime nextBonusGoldDate = DateTime.Now.AddYears(1);
    public DateTime nextBonusSilverDate = DateTime.Now.AddYears(1);
    public DateTime nextBonusCopperDate = DateTime.Now.AddYears(1);

	private String resource;

    public void FixedUpdate()
    {
        if (GlobalGOScript.instance.gameManager.levelLoadManager.mainUI.activeInHierarchy)
        {
            DateTime now = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update;    

            if (nextBonusWoodDate < now || nextBonusFoodDate < now || nextBonusStoneDate < now
                || nextBonusIronDate < now || nextBonusGoldDate < now || nextBonusSilverDate < now
                || nextBonusCopperDate < now)
            {
                GlobalGOScript.instance.adBonusNotification.SetActive(true);
            }
            else
            {
                GlobalGOScript.instance.adBonusNotification.SetActive(false);
            }
        }
    }

    public void requestBannerAd()
    {
        AdRequest request = new AdRequest.Builder().Build();
        this.bannerView = new BannerView(ADManager.adBannerUnitId, AdSize.SmartBanner, AdPosition.Top);
        bannerView.OnAdLoaded += HandleOnAdLoaded;
        bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        bannerView.OnAdOpening += HandleOnAdOpened;
        bannerView.OnAdClosed += HandleOnAdClosed;
        bannerView.OnAdLeavingApplication += HandleOnAdLeavingApplication;
        bannerView.LoadAd(request);
    }

    public void requestInterstitialAd()
    {
		/*
        AdRequest request = new AdRequest.Builder().Build();
        this.interstitial = new InterstitialAd(adInterstitialUnitId);
        interstitial.OnAdClosed += HandleOnAdClosed;
        interstitial.LoadAd(request);
		*/
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-6112664567302645/6045027271";
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-6112664567302645/1284489331";
		#else
		string adUnitId = "unexpected_platform";
		#endif

		
		AdRequest request = new AdRequest.Builder().Build();
		this.rewardBasedVideo = RewardBasedVideoAd.Instance;
		// Load the rewarded video ad with the request.
		this.rewardBasedVideo.OnAdClosed += HandleOnAdClosed;
		this.rewardBasedVideo.OnAdRewarded += HandleOnAdRewarded;
		this.rewardBasedVideo.LoadAd(request, adUnitId);
    }

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        print(args.ToString());
        ShowBanner();
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        print("Ad load failed: " + args.Message);
        //HideBanner();
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        print("Ad opened: " + args.ToString());
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        print("Ad closed: " + args.ToString());
    }

	public void HandleOnAdRewarded(object sender, EventArgs args)
	{
		StartCoroutine(AddResourceBonus(this.resource));
	}

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
        print("Levaing application: " + args.ToString());
    }

    public void ShowBanner()
    {
        //RectTransform UITransform = UI.GetComponent<RectTransform>();
        //UITransform.offsetMax = new Vector2(0, -100f);
        this.bannerView.Show();
		
    }

    public void HideBanner()
    {
        RectTransform UITransform = UI.GetComponent<RectTransform>();
        UITransform.offsetMax = new Vector2(0, 0);
        this.bannerView.Hide();
    }

	public bool IsLoaded()
	{
		//return interstitial.IsLoaded ();
		return this.rewardBasedVideo.IsLoaded ();
	}

    public void ShowInterstitialForResource(string resource)
    {
		if (this.rewardBasedVideo.IsLoaded ()) {
			rewardBasedVideo.Show ();
			this.resource = resource;
		} else {
			GlobalGOScript.instance.windowInstanceManager.openNotification("Wait. Video is not loaded yet.");
		}
    }

    public IEnumerator AddResourceBonus(string resource)
    {
        WWWForm form = new WWWForm();
        form.AddField("resources_type", resource);
        form.AddField("city_id", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());

        UnityWebRequest www = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/resources/addResourcesVideoAdsBonus/", form);
        yield return www.Send();

        if(www.isError) {
            Debug.Log(www.error);
        }
        else {
            JSONObject obj = JSONObject.Create(www.downloadHandler.text);
            Debug.Log(www.downloadHandler.text);
        }
    }
}
