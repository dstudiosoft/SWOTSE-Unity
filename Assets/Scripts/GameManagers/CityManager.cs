﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class CityManager {

    public ResourceManager resourceManager;
    public UnitsManager unitsManager;
    public BuildingsManager buildingsManager;
    public TreasureManager treasureManager;

    public event SimpleEvent onCityBuilt;
    public event SimpleEvent onGotNewCity;
    public event SimpleEvent onCityNameAndIconChanged;
    public event SimpleEvent cityChangerUpdateImage;
    public event SimpleEvent gotAllowTroopsState;
    public event SimpleEvent onTaxUpdated;

    public MyCityModel currentCity;
    public Dictionary<long, MyCityModel> myCities;

    public IEnumerator GetInitialCities()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/cities/my_cities/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            if (myCities == null)
            {
                JSONObject citiesArray = JSONObject.Create(request.downloadHandler.text, 3);
                myCities = new Dictionary<long, MyCityModel>();

                for (int i = 0; i < citiesArray.Count; i++)
                {
                    MyCityModel temp = JsonUtility.FromJson<MyCityModel>(citiesArray[i].Print());
                    myCities.Add(temp.id, temp);
                }

                GlobalGOScript.instance.playerManager.loginManager.gotPlayerCities = true;
            }
            else
            {
                JSONObject citiesArray = JSONObject.Create(request.downloadHandler.text, 3);

                for (int i = 0; i < citiesArray.Count; i++)
                {
                    if (!myCities.ContainsKey(citiesArray[i]["id"].i))
                    {
                        MyCityModel temp = JsonUtility.FromJson<MyCityModel>(citiesArray[i].Print());
                        myCities.Add(temp.id, temp);
                    }
                }
            }

            if (onGotNewCity != null)
            {
                onGotNewCity(this, "success");
            }
        }
    }

    public IEnumerator BuildCity(long x, long y, string cityName)
    {
        WWWForm newCityForm = new WWWForm();
        newCityForm.AddField("posX", x.ToString());
        newCityForm.AddField("posY", y.ToString());
        newCityForm.AddField("cityID", currentCity.id.ToString());
        newCityForm.AddField("cityName", cityName);

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/cities/buildNewCity/", newCityForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject status = JSONObject.Create(request.downloadHandler.text);

            if (status.IsString)
            {
				if (string.Equals (status.str, "success", System.StringComparison.CurrentCultureIgnoreCase)) 
				{
					if (onCityBuilt != null) 
					{
						onCityBuilt (this, "success");
					}
				} 
				else 
				{
					GlobalGOScript.instance.windowInstanceManager.openNotification(status.str);

					if (onCityBuilt != null) 
					{
						onCityBuilt (this, "fail");
					}
				}
            }
            else
            {
                if (onCityBuilt != null)
                {
                    onCityBuilt(this, "fail");
                }
            }
        }
    }

    public IEnumerator ChangeNameAndImage(string cityName, string imageName)
    {
        WWWForm changeForm = new WWWForm();
        changeForm.AddField("city_name", cityName);
        changeForm.AddField("image_name", imageName);
        changeForm.AddField("cityID", currentCity.id.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/cities/updateCity/", changeForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject status = JSONObject.Create(request.downloadHandler.text);

            if (status.IsString)
            {
                if (string.Equals(status.str, "success", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    currentCity.city_name = cityName;
                    currentCity.image_name = imageName;

                    if (cityChangerUpdateImage != null)
                    {
                        cityChangerUpdateImage(this, imageName);
                    }

                    if (onCityNameAndIconChanged != null)
                    {
                        onCityNameAndIconChanged(this, "success");
                    }

                    GlobalGOScript.instance.windowInstanceManager.openNotification("Information updated successfully");
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Information update failed");

                if (onCityNameAndIconChanged != null)
                {
                    onCityNameAndIconChanged(this, "fail");
                }
            }
        }
    }

    public IEnumerator UpdateCurrentCityCoords()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/cities/cities/" + currentCity.city_name + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            MyCityModel city = JsonUtility.FromJson<MyCityModel>(request.downloadHandler.text);
            if (city != null)
            {
                currentCity.posX = city.posX;
                currentCity.posY = city.posY;

                GlobalGOScript.instance.cityChanger.UpdateCityCoords(city.posX, city.posY);
            }
        }
    }

    public IEnumerator UpdateCityExpantion()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/cities/cities/" + currentCity.city_name + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            MyCityModel city = JsonUtility.FromJson<MyCityModel>(request.downloadHandler.text);
            if (city != null)
            {
                currentCity.isExpantion = city.isExpantion;

                GlobalGOScript.instance.gameManager.levelLoadManager.UpdateCityBuildings(this, "success");
            }
        }
    }

    public IEnumerator UpdateCityAllowTroops()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/cities/cities/" + currentCity.city_name + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            MyCityModel city = JsonUtility.FromJson<MyCityModel>(request.downloadHandler.text);
            if (city != null)
            {
                currentCity.reinforce_flag = city.reinforce_flag;
            }
        }
    }

    public IEnumerator SetAllowTroops(bool flag)
    {
        WWWForm setForm = new WWWForm();
        setForm.AddField("cityID", currentCity.id.ToString());
        setForm.AddField("value", flag ? "True" : "False");

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/cities/setReinforceFlag/", setForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject response = JSONObject.Create(request.downloadHandler.text);

            if (string.Equals(response.str, "success"))
            {
                if (gotAllowTroopsState != null)
                {
                    gotAllowTroopsState(this, "success");
                }
            }
            else
            {
                if (gotAllowTroopsState != null)
                {
                    gotAllowTroopsState(this, "fail");
                }
            }
        }
    }

    public IEnumerator UpdateCityTax(long taxRate)
    {
        WWWForm setForm = new WWWForm();
        setForm.AddField("cityID", currentCity.id.ToString());
        setForm.AddField("taxRate", taxRate.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/cities/updateTaxRate/", setForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject response = JSONObject.Create(request.downloadHandler.text);

            if (string.Equals(response.str, "success"))
            {
                if (onTaxUpdated != null)
                {
                    onTaxUpdated(this, "success");
                }
            }
            else
            {
                if (onTaxUpdated != null)
                {
                    onTaxUpdated(this, "fail");
                }
            }
        }
    }

    public IEnumerator StartRevolution(long cityID)
    {
        WWWForm revoltForm = new WWWForm();
        revoltForm.AddField("cityID", cityID.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/cities/revolusion/", revoltForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject response = JSONObject.Create(request.downloadHandler.text);

            if (string.Equals(response.str, "success"))
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Revolution started successfully!");
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification(response.str);
            }
        }
    }
}
