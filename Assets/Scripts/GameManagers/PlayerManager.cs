﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class PlayerManager {

    public ReportManager reportManager;
    public AllianceManager allianceManager;
    public LoginManager loginManager;

    public UserModel user;

    public event SimpleEvent onGetMyUserInfo;
    public event SimpleEvent onLanguageChanged;
    public event SimpleEvent onPasswordChanged;
    public event SimpleEvent onResetPasswordRequested;
    public event SimpleEvent onPasswordReset;

    public IEnumerator getMyUserInfo(bool initial)
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/authorization/my_user/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);

            if (onGetMyUserInfo != null)
            {
                onGetMyUserInfo(this, "fail");
            }
        }
        else {
            
            JSONObject userObj = JSONObject.Create(request.downloadHandler.text);

            if (userObj.HasField("detail"))
            {
                if (onGetMyUserInfo != null)
                {
                    onGetMyUserInfo(this, "fail");
                }
                yield break;
            }

            user = JsonUtility.FromJson<UserModel>(request.downloadHandler.text);

            if (!userObj["alliance"].IsNull)
            {
                AllianceModel userAlliance = JsonUtility.FromJson<AllianceModel>(userObj["alliance"].Print());

                userAlliance.founderId = userObj["alliance"]["founder"]["id"].i;
                userAlliance.founderName = userObj["alliance"]["founder"]["username"].str;

                userAlliance.leaderId = userObj["alliance"]["leader"]["id"].i;
                userAlliance.leaderName = userObj["alliance"]["leader"]["username"].str;

                userAlliance.allies = new System.Collections.Generic.List<long>();
                if (userObj["alliance"]["allies"].list.Count > 0)
                {
                    foreach (JSONObject ally in userObj["alliance"]["allies"].list)
                    {
                        userAlliance.allies.Add(ally["id"].i);
                    }
                }

                userAlliance.enemies = new System.Collections.Generic.List<long>();
                if (userObj["alliance"]["enemies"].list.Count > 0)
                {
                    foreach (JSONObject enemy in userObj["alliance"]["enemies"].list)
                    {
						userAlliance.enemies.Add(enemy["id"].i);
                    }
                }

                user.alliance = userAlliance;

                GlobalGOScript.instance.playerManager.allianceManager.userAlliance = userAlliance;
            }

            user.last_login = DateTime.Parse(userObj["last_login"].str);

            MyCityModel myCity = JsonUtility.FromJson<MyCityModel>(userObj["capital"].Print());
            user.capital = myCity;

            if (initial)
            {
                GlobalGOScript.instance.gameManager.cityManager.currentCity = myCity;

                GlobalGOScript.instance.playerManager.loginManager.gotPlayerInfo = true;
            }

            if (onGetMyUserInfo != null)
            {
                onGetMyUserInfo(this, "success");
            }
        }
    }

    public IEnumerator ChangeUserInfo(string username, string email, string image, string flag)
    {
        WWWForm userForm = new WWWForm();
        userForm.AddField("username", username);
        userForm.AddField("email", email);
        userForm.AddField("image_name", image);
        userForm.AddField("flag", flag);

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/authorization/updateUser/", userForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    //success
                    user.username = username;
                    user.email = email;
                    user.image_name = image;
                    user.flag = flag;
                }
                else
                {
                    //fail
                }
            }
            else
            {
                //fail
            }    
        }
    }

    public IEnumerator NominateUser(long userId)
    {
        WWWForm userForm = new WWWForm();
        userForm.AddField("userID", userId.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/authorization/nominateUser/", userForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            //maybe success
        }
    }

    public IEnumerator ChangeLanguage(string lang)
    {
        WWWForm userForm = new WWWForm();
        userForm.AddField("language", lang);

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/authorization/updateUser/", userForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    //success
                    if (onLanguageChanged != null)
                    {
                        onLanguageChanged(this, "success");
                    }
                }
                else
                {
                    //fail
                    if (onLanguageChanged != null)
                    {
                        onLanguageChanged(this, "fail");
                    }
                }
            }
            else
            {
                //fail
                if (onLanguageChanged != null)
                {
                    onLanguageChanged(this, "fail");
                }
            }    
        }
    }

    public IEnumerator ChangeUserPassword(string oldPass, string newPass)
    {
        WWWForm passForm = new WWWForm();
        passForm.AddField("old_password", oldPass);
        passForm.AddField("new_password", newPass);

        UnityWebRequest request = UnityWebRequest.Post("http://" + GlobalGOScript.instance.host + ":8889/authorization/updatePassword/", passForm);
        request.SetRequestHeader("authorization", "Token " + GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    //success
                    if (onPasswordChanged != null)
                    {
                        onPasswordChanged(this, "success");
                    }
                }
                else
                {
                    //fail
                    if (onPasswordChanged != null)
                    {
                        onPasswordChanged(this, "fail");
                    }
                }
            }
            else
            {
                //fail
                if (onPasswordChanged != null)
                {
                    onPasswordChanged(this, "fail");
                }
            }
        }
    }


    public IEnumerator ResetPassword(string email, string secretKey, string newPassword)
    {
        WWWForm passwordForm = new WWWForm();
        passwordForm.AddField("email", email);
        passwordForm.AddField("secret_key", secretKey);
        passwordForm.AddField("new_password", newPassword);

        UnityWebRequest request = UnityWebRequest.Post("http://" + GlobalGOScript.instance.host + ":8889/authorization/resetPasswordWithSecretKey/", passwordForm);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    //success
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Password has been reset");
                    if (onPasswordReset != null)
                    {
                        onPasswordReset(this, "success");
                    }
                }
                else
                {
                    //fail
                    GlobalGOScript.instance.windowInstanceManager.openNotification(request.downloadHandler.text);
                    if (onPasswordReset != null)
                    {
                        onPasswordReset(this, "fail");
                    }
                }
            }
            else
            {
                //fail
                GlobalGOScript.instance.windowInstanceManager.openNotification(request.downloadHandler.text);
                if (onPasswordReset != null)
                {
                    onPasswordReset(this, "fail");
                }
            }
        }
    }
}
