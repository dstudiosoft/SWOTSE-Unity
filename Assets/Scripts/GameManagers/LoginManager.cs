﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using System;
using SmartLocalization;

public class LoginManager : MonoBehaviour {

    private Dictionary<string, long> emperorsNameToId;

    public event SimpleEvent onGetEmperors;
    public event SimpleEvent onRegister;
    public event SimpleEvent onLogin;

    public bool gotBuildingConstants = false;
    public bool gotUnitConstants = false;
    public bool gotTutorialPages = false;
    public bool gotPlayerInfo = false;
    public bool gotPlayerCities = false;
    public bool gotCityResources = false;
    public bool gotCityBuildings = false;
    public bool gotCityFields = false;
    public bool gotCityValleys = false;
    public bool gotCityUnits = false;
    public bool gotCityGenerals = false;
    public bool gotCityTreasures = false;

    public void LogOut(bool relogin)
    {
        GlobalGOScript.instance.gameManager.levelLoadManager.mainUI.SetActive(false);

        GlobalGOScript.instance.token = string.Empty;
        GlobalGOScript.instance.gameManager.cityManager.resourceManager.resourcesWorking = false;
        GlobalGOScript.instance.gameManager.cityManager.resourceManager.StopAllCoroutines();
        GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources = null;
        GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits = null;
        GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals = null;
        GlobalGOScript.instance.gameManager.battleManager.cityArmies = null;
        GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields = null;
        GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings = null;
        GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityValleys = null;
        GlobalGOScript.instance.gameManager.cityManager.currentCity = null;
        GlobalGOScript.instance.gameManager.cityManager.myCities = null;
        GlobalGOScript.instance.gameManager.buildingConstants = null;
        GlobalGOScript.instance.gameManager.buildingIds = null;
        GlobalGOScript.instance.gameManager.unitConstants = null;
        GlobalGOScript.instance.playerManager.allianceManager.allianceMembers = null;
        GlobalGOScript.instance.playerManager.allianceManager.userAlliance = null;
        GlobalGOScript.instance.playerManager.allianceManager.allAlliances = null;
        GlobalGOScript.instance.playerManager.reportManager.allianceRequsets = null;
        GlobalGOScript.instance.playerManager.reportManager.inbox = null;
        GlobalGOScript.instance.playerManager.reportManager.invites = null;
        GlobalGOScript.instance.playerManager.reportManager.battles = null;
        GlobalGOScript.instance.playerManager.reportManager.buildingTimers = null;
        GlobalGOScript.instance.playerManager.reportManager.unitTimers = null;
        GlobalGOScript.instance.playerManager.reportManager.armyTimers = null;
        GlobalGOScript.instance.playerManager.user = null;

        if (!relogin)
        {
            PlayerPrefs.DeleteKey("token");
        }

        gotBuildingConstants = false;
        gotUnitConstants = false;
        gotTutorialPages = false;
        gotPlayerInfo = false;
        gotPlayerCities = false;
        gotCityResources = false;
        gotCityBuildings = false;
        gotCityFields = false;
        gotCityValleys = false;
        gotCityUnits = false;
        gotCityGenerals = false;
        gotCityTreasures = false;
    }

    public IEnumerator RegisterRequest(string username, string password, string email, string emperor_name, string language)
    {
        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("password", password);
        form.AddField("email", email);
        if (!string.IsNullOrEmpty(emperor_name))
        {
            form.AddField("emperor_id", emperorsNameToId[emperor_name].ToString());
        }
        form.AddField("language", language);

        UnityWebRequest www = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/authorization/registration/", form);

        if (!string.IsNullOrEmpty(emperor_name))
        {
            yield return www.Send();
        }

        if(www.isError) {
            Debug.Log(www.error);
        }
        else {
            Debug.Log(www.downloadHandler.text);
            JSONObject obj = JSONObject.Create(www.downloadHandler.text, 2);
            if (obj.HasField("message"))
            {
                onRegister(this, "FAIL");
                GlobalGOScript.instance.windowInstanceManager.openNotification(obj["message"].str, true);
            }
            else if (obj.HasField("password"))
            {
                onRegister(this, "FAIL");
                GlobalGOScript.instance.windowInstanceManager.openNotification(obj["password"][0].str, true);
            }
            else if (obj.HasField("non_field_errors"))
            {
                Debug.Log(obj["non_field_errors"][0].str);
                GlobalGOScript.instance.windowInstanceManager.openNotification(obj["non_field_errors"][0].str, true);
            }
            else
            {
                onRegister(this, string.Empty);
            }
        }
    }

	public IEnumerator ForgotPassword(string email)
	{
		WWWForm emailForm = new WWWForm();
		emailForm.AddField("email", email);

		UnityWebRequest request = UnityWebRequest.Post("http://" + GlobalGOScript.instance.host + ":8889/authorization/forgotPassword/", emailForm);

		yield return request.Send();

		if (request.isError)
		{
			Debug.Log(request.error);
		}
		else
		{
			JSONObject obj = JSONObject.Create(request.downloadHandler.text);
			if (obj.IsString)
			{
				if (obj.str.StartsWith("success"))
				{
					//success
					Debug.Log("success notif");
					GlobalGOScript.instance.windowInstanceManager.openNotification("Follow instruction in the mail that we sent you to change password.", true);
				}
				else
				{
					//fail
					GlobalGOScript.instance.windowInstanceManager.openNotification(obj.str, true);
				}
			}
			else
			{
				//fail
				GlobalGOScript.instance.windowInstanceManager.openNotification(request.downloadHandler.text, true);
			}
		}
	}

    public IEnumerator LoginRequest(string username, string password)
    {
        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("password", password);

        UnityWebRequest www = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/authorization/login/", form);
        yield return www.Send();

        if(www.isError) {
            Debug.Log(www.error);
        }
        else {
            JSONObject obj = JSONObject.Create(www.downloadHandler.text);
            Debug.Log(www.downloadHandler.text);
            if (obj.HasField("non_field_errors"))
            {
                Debug.Log(obj["non_field_errors"][0].str);
                GlobalGOScript.instance.windowInstanceManager.openNotification(obj["non_field_errors"][0].str, true);
            }
            else if (obj.HasField("password"))
            {
                onRegister(this, "FAIL");
                GlobalGOScript.instance.windowInstanceManager.openNotification(obj["password"][0].str, true);
            }
            else if (obj.HasField("detail"))
            {
                if (string.Equals(obj["detail"].str, "invalid token"))
                {
                    onRegister(this, "FAIL");
                    GlobalGOScript.instance.windowInstanceManager.openNotification("User already logged in", true);
                }
            }
            else
            {
                GlobalGOScript.instance.token = obj["key"].str;
                PlayerPrefs.SetString("token", GlobalGOScript.instance.token);

                if (onLogin != null)
                {
                    onLogin(this, string.Empty);
                }
            }
        }
    }

    public List<string> EmperorNames()
    {
        List<string> emperorNames = new List<string>();
        foreach (string name in emperorsNameToId.Keys)
        {
            emperorNames.Add(name);
        }
        return emperorNames;
    }

    void addEmperor (JSONObject emperor)
    {
        emperorsNameToId.Add(emperor[1].str, emperor[0].i);
    }

    public IEnumerator GetEmperors()
    {
        emperorsNameToId = new Dictionary<string, long>();
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/alliances/allAlliancesShort/");

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            Debug.Log(request.downloadHandler.text);
            JSONObject alliancesObj = JSONObject.Create(request.downloadHandler.text, 2);
            alliancesObj.list.ForEach(addEmperor);

            if (onGetEmperors != null)
            {
                onGetEmperors(this, string.Empty);
            }
        }
    }

    public void GotBuildingNamesHandler(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.onGetBuildingNames -= GotBuildingNamesHandler;
        foreach (string name in GlobalGOScript.instance.gameManager.buildingIds)
        {
            StartCoroutine(GlobalGOScript.instance.gameManager.GetBuildingInitialConstantForName(name));
        }
    }

    public void LoginHandler(object sender, string value)
    {
        onLogin -= LoginHandler;
        GlobalGOScript.instance.playerManager.onGetMyUserInfo += GotUserInformationHandler;
        StartCoroutine(GlobalGOScript.instance.playerManager.getMyUserInfo(true));
    }

    public void GotUserInformationHandler(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.onGetMyUserInfo -= GotUserInformationHandler;
        if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance != null)
        {
            StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.AllianceMemebers());
        }
        LanguageManager.Instance.ChangeLanguage(GlobalGOScript.instance.playerManager.user.language);
        StartCoroutine(loadMainScene());
    }

    IEnumerator loadMainScene()
    {
        GlobalGOScript.instance.windowInstanceManager.openLoginProgressbar();
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.GetInitialCities());
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.resourceManager.GetInitialReourcesForCity());
        GlobalGOScript.instance.gameManager.onGetBuildingNames += GotBuildingNamesHandler;  
        StartCoroutine(GlobalGOScript.instance.gameManager.GetInitialBuildingConstants());
        StartCoroutine(GlobalGOScript.instance.gameManager.GetInitialUnitConstants());
        StartCoroutine(GlobalGOScript.instance.gameManager.GetTutorialPages());
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.GetInitialCityBuildings());
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.GetInitialCityFields());
		StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.GetInitialCityValleys(true));
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.GetInitialCityUnits());
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.GetInitialCityGenerals());
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.GetCityTreasures(true));

        while (!gotBuildingConstants || !gotUnitConstants || !gotTutorialPages || !gotPlayerInfo || !gotPlayerCities || !gotCityResources || !gotCityBuildings || !gotCityFields || !gotCityValleys || !gotCityUnits|| !gotCityGenerals || !gotCityTreasures)
        {
            yield return new WaitForSeconds(1.0f);
        }

        foreach (TutorialPageModel page in GlobalGOScript.instance.gameManager.tutorials)
        {
            StartCoroutine(GlobalGOScript.instance.gameManager.DownloadTutorialImage(page.image, page.id));
        }
            
        GlobalGOScript.instance.gameManager.cityManager.resourceManager.resourceCoroutine = StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.resourceManager.getResources());

        SceneManager.LoadScene(2);
    }
}
