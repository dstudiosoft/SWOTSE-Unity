﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class UnitsManager {

    public UnitsModel cityUnits;
    public Dictionary<long, ArmyGeneralModel> cityGenerals;

    public event SimpleEvent onUnitsHired;
    public event SimpleEvent onGeneralHired;
    public event SimpleEvent onGeneralDismissed;
    public event SimpleEvent onUnitsUpdated;
    public event SimpleEvent onGeneralUpgraded;
    public event SimpleEvent onTrainingCancelled;
    public event SimpleEvent onUnitsDismissed;
    public event SimpleEvent onGetGenerals;

    public IEnumerator GetInitialCityUnits()
    {  
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/army/cityArmy/"+ GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString() + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                //failed
                Debug.Log(request.downloadHandler.text);
            }
            else
            {
                //success
                cityUnits = JsonUtility.FromJson<UnitsModel>(request.downloadHandler.text);

                GlobalGOScript.instance.playerManager.loginManager.gotCityUnits = true;
            }    
        }
    }

    public IEnumerator GetInitialCityGenerals()
    {  
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/heroes/getHeroes/"+ GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString() + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                //failed
                Debug.Log(request.downloadHandler.text);
            }
            else
            {
                //success
                JSONObject generalsArray = JSONObject.Create(request.downloadHandler.text);
                cityGenerals = new Dictionary<long, ArmyGeneralModel>();

                if (generalsArray.list.Count > 0)
                {
                    foreach (JSONObject general in generalsArray.list)
                    {
                        ArmyGeneralModel cityGeneral = JsonUtility.FromJson<ArmyGeneralModel>(general.Print());
                        cityGenerals.Add(cityGeneral.id, cityGeneral);
                    }
                }
                else
                {
                    cityGenerals = null;
                }

                if (onGetGenerals != null)
                {
                    onGetGenerals(this, "success");
                }

                GlobalGOScript.instance.playerManager.loginManager.gotCityGenerals = true;
            }    
        }
    }

    public IEnumerator UpdateCityUnits()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/army/cityArmy/"+ GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString() + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                //failed
                Debug.Log(request.downloadHandler.text);

                if (onUnitsUpdated != null)
                {
                    onUnitsUpdated(this, "fail");
                }
            }
            else
            {
                //success
                cityUnits = JsonUtility.FromJson<UnitsModel>(request.downloadHandler.text);

                if (onUnitsUpdated != null)
                {
                    onUnitsUpdated(this, "success");
                }
            }    
        }
    }

    public IEnumerator UpdateCityGenerals()
    {  
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/heroes/getHeroes/"+ GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString() + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                //failed
                Debug.Log(request.downloadHandler.text);
            }
            else
            {
                //success
                JSONObject generalsArray = JSONObject.Create(request.downloadHandler.text);

                cityGenerals = new Dictionary<long, ArmyGeneralModel>();

                foreach (JSONObject general in generalsArray.list)
                {
                    ArmyGeneralModel cityGeneral = JsonUtility.FromJson<ArmyGeneralModel>(general.Print());
                    cityGenerals.Add(cityGeneral.id, cityGeneral);
                }

                if (onGetGenerals != null)
                {
                    onGetGenerals(this, "success");
                }
            }    
        }
    }


    public IEnumerator HireGeneral(ArmyGeneralModel general)
    {
        WWWForm hireForm = new WWWForm();
        hireForm.AddField("heroID", general.id.ToString());
        hireForm.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/heroes/buyHero/", hireForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (!obj.IsString)
            {
                //failed
                if (onGeneralHired != null)
                {
                    onGeneralHired(this, "fail");
                }
            }
            else
            {
                if (string.Equals(obj.str, "success"))
                {
                    //success
                    if (cityGenerals == null)
                    {
                        cityGenerals = new Dictionary<long, ArmyGeneralModel>();
                    }

                    GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals.Add(general.id, general);

                    if (onGeneralHired != null)
                    {
                        onGeneralHired(this, "success");
                    }
                }
                else
                {
                    if (onGeneralHired != null)
                    {
                        onGeneralHired(this, "fail");
                    }
                }
            }    

        }
    }

    public IEnumerator DismissGeneral(ArmyGeneralModel general)
    {
        WWWForm dismissForm = new WWWForm();
        dismissForm.AddField("heroID", general.id.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/heroes/dismissHero/", dismissForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (!obj.IsString)
            {
                //failed
                if (onGeneralDismissed != null)
                {
                    onGeneralDismissed(this, "fail");
                }
            }
            else
            {
                if (string.Equals(obj.str, "success"))
                {
                    //success
                    GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals.Remove(general.id);
                    if (onGeneralDismissed != null)
                    {
                        onGeneralDismissed(this, "success");
                    }
                }
                else
                {
                    if (onGeneralDismissed != null)
                    {
                        onGeneralDismissed(this, "fail");
                    }
                }
            }    

        }
    }

    public IEnumerator HireUnits(long count, string name)
    {
        WWWForm unitForm = new WWWForm();
        unitForm.AddField("city", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());
        unitForm.AddField("unit", name);
        unitForm.AddField("count", count.ToString());       

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/army/addUnit/", unitForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (!obj.IsString)
            {
                //failed
                if (onUnitsHired != null)
                {
                    onUnitsHired(this, "fail");
                }
            }
            else
            {
                //success
                if (onUnitsHired != null)
                {
                    onUnitsHired(this, obj.str);
                }
            }    

        }
    }

    public IEnumerator UpgradeGeneral(long heroId, long march, long politics, long speed, long salary, long loyalty)
    {
        WWWForm upgradeForm = new WWWForm();
        upgradeForm.AddField("heroID", heroId.ToString());
        upgradeForm.AddField("march", march.ToString());
        upgradeForm.AddField("politics", politics.ToString());
        upgradeForm.AddField("speed", speed.ToString());
        upgradeForm.AddField("salary", salary.ToString());
        upgradeForm.AddField("loyalty", loyalty.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/heroes/updateHero/", upgradeForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (!obj.IsString)
            {
                //failed
                GlobalGOScript.instance.windowInstanceManager.openNotification("Upgrade failed");
                if (onGeneralUpgraded != null)
                {
                    onGeneralUpgraded(this, "fail");
                }
            }
            else
            {
                //success
                if (onGeneralUpgraded != null)
                {
                    onGeneralUpgraded(this, obj.str);
                }
            }    

        }
    }

    public IEnumerator CancelTraining(long event_id)
    {
        WWWForm cancelForm = new WWWForm();
        cancelForm.AddField("unitEventID", event_id.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/updateEvents/cancelUnitEvent/", cancelForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (!obj.IsString)
            {
                //failed
                if (onTrainingCancelled != null)
                {
                    onTrainingCancelled(this, "fail");
                }
            }
            else
            {
                if (string.Equals(obj.str, "success"))
                {
                    //success
                    if (onTrainingCancelled != null)
                    {
                        onTrainingCancelled(this, "success");
                    }
                }
                else
                {
                    if (onTrainingCancelled != null)
                    {
                        onTrainingCancelled(this, "fail");
                    }
                }
            }    

        }
    }

    public IEnumerator DismissUnits(UnitsModel army)
    {
        WWWForm form = new WWWForm();
        form.AddField("cityID", army.city.ToString());
        form.AddField("workers_count", army.workers_count.ToString());
        form.AddField("spy_count", army.spy_count.ToString());
        form.AddField("swards_man_count", army.swards_man_count.ToString());
        form.AddField("spear_man_count", army.spear_man_count.ToString());
        form.AddField("pike_man_count", army.pike_man_count.ToString());
        form.AddField("scout_rider_count", army.scout_rider_count.ToString());
        form.AddField("light_cavalry_count", army.light_cavalry_count.ToString());
        form.AddField("heavy_cavalry_count", army.heavy_cavalry_count.ToString());
        form.AddField("archer_count", army.archer_count.ToString());
        form.AddField("archer_rider_count", army.archer_rider_count.ToString());
        form.AddField("holly_man_count", army.holly_man_count.ToString());
        form.AddField("trebuchet_count", army.trebuchet_count.ToString());
        form.AddField("siege_towers_count", army.siege_towers_count.ToString());
        form.AddField("battering_ram_count", army.battering_ram_count.ToString());
        form.AddField("ballista_count", army.ballista_count.ToString());
		form.AddField("wagon_count", army.wagon_count.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://" + GlobalGOScript.instance.host + ":8889/army/dismissCityArmy/", form);
        request.SetRequestHeader("authorization", "Token " + GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            Debug.Log(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Units dismissed");
                    if (onUnitsDismissed != null)
                    {
                        onUnitsDismissed(this, "success");
                    }
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification(obj.str);
                    if (onUnitsDismissed != null)
                    {
                        onUnitsDismissed(this, "fail");
                    }
                }
            }
        }
    }
}
