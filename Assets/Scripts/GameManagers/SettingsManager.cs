﻿using UnityEngine;
using System.Collections;

public class SettingsManager : MonoBehaviour
{
    public void LogOut()
    {
        GlobalGOScript.instance.playerManager.loginManager.LogOut(false);
        GlobalGOScript.instance.gameManager.levelLoadManager.LoadLogin();
    }
}
