﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Specialized;
using SmartLocalization;
using ArabicSupport;

public class TreasureManager {
    public OrderedDictionary scriptTreasures;
    public OrderedDictionary speedUpTreasures;
    public OrderedDictionary productionTreasures;
    public OrderedDictionary diamondTreasures;
    public OrderedDictionary promotionsTreasures;
    public OrderedDictionary armyTreasures;

    public event SimpleEvent onGetItems;
    public event SimpleEvent onItemBought;
    public event SimpleEvent onItemSold;
    public event SimpleEvent onItemGifted;
    public event SimpleEvent onItemUsed;

    public IEnumerator GetCityTreasures(bool initial)
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/shop/myItems/" + GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString() + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
            if (onGetItems != null)
            {
                onGetItems(this, "fail");
            }
        }
        else {
            scriptTreasures = new OrderedDictionary();
            speedUpTreasures = new OrderedDictionary();
            productionTreasures = new OrderedDictionary();
            diamondTreasures = new OrderedDictionary();
            promotionsTreasures = new OrderedDictionary();
            armyTreasures = new OrderedDictionary();

            JSONObject itemArray = JSONObject.Create(request.downloadHandler.text);

            foreach (JSONObject item in itemArray.list)
            {
                CityTreasureModel treasureItem = JsonUtility.FromJson<CityTreasureModel>(item.Print());
                treasureItem.item_constant = JsonUtility.FromJson<ShopItemModel>(item["item_constant"].Print());

				if (string.Equals (LanguageManager.Instance.CurrentlyLoadedCulture.languageCode, "ar")) {
					
					char[] name = treasureItem.item_constant.name.ToCharArray ();
					//System.Array.Reverse(name);
					treasureItem.item_constant.name = new string (name);
					char[] description = treasureItem.item_constant.description.ToCharArray();
					//System.Array.Reverse(description);
					treasureItem.item_constant.description = new string (description);
					treasureItem.item_constant.description = ArabicFixer.Fix (treasureItem.item_constant.description, false, false);

                }

                switch (treasureItem.item_constant.type)
                {
                    case "scripts":
                        scriptTreasures.Add(treasureItem.item_constant.id, treasureItem);
                        break;

                    case "speed_up":
                        speedUpTreasures.Add(treasureItem.item_constant.id, treasureItem);
                        break;

                    case "productions":
                        productionTreasures.Add(treasureItem.item_constant.id, treasureItem);
                        break;

                    case "diamonds":
                        diamondTreasures.Add(treasureItem.item_constant.id, treasureItem);
                        break;

                    case "battalions":
                        armyTreasures.Add(treasureItem.item_constant.id, treasureItem);
                        break;

                    case "promotion":
                        promotionsTreasures.Add(treasureItem.item_constant.id, treasureItem);
                        break;

                    default:
                        break;
                }
            }
            if (initial)
            {
                GlobalGOScript.instance.playerManager.loginManager.gotCityTreasures = true;
            }
            else
            {
                if (onGetItems != null)
                {
                    onGetItems(this, "success");
                }
            }
        }
    }

    public IEnumerator UseItem(string itemId)
    {
        WWWForm useItemForm = new WWWForm();
        useItemForm.AddField("cityItemID", itemId.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/shop/useItem/", useItemForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject status = JSONObject.Create(request.downloadHandler.text);

            if (status.IsString)
            {
                if (string.Equals(status.str, "success", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Item used successfully");
                    if (onItemUsed != null)
                    {
                        onItemUsed(this, "success");
                    }
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification(status.str);

                    if (onItemUsed != null)
                    {
                        onItemUsed(this, "fail");
                    }
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Item use failed");

                if (onItemUsed != null)
                {
                    onItemUsed(this, "fail");
                }
            }
        }
    }

    public IEnumerator UseUpgrade(long itemId, long building_id, bool valley)
    {
        WWWForm useItemForm = new WWWForm();
        useItemForm.AddField("cityItemID", itemId.ToString());

        if (valley)
        {
            useItemForm.AddField("valleyID", building_id.ToString());
        }
        else
        {
            useItemForm.AddField("buildingID", building_id.ToString());
        }

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/shop/useItem/", useItemForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject status = JSONObject.Create(request.downloadHandler.text);

            if (status.IsString)
            {
                if (string.Equals(status.str, "success", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Item used successfully");
                    if (onItemUsed != null)
                    {
                        onItemUsed(this, "success");
                    }
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification(status.str);

                    if (onItemUsed != null)
                    {
                        onItemUsed(this, "fail");
                    }
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Item use failed");

                if (onItemUsed != null)
                {
                    onItemUsed(this, "fail");
                }
            }
        }
    }

    public IEnumerator UseTeleport(string itemId, long x, long y)
    {
        WWWForm useItemForm = new WWWForm();
        useItemForm.AddField("cityItemID", itemId);
        useItemForm.AddField("posX", x.ToString());
        useItemForm.AddField("posY", y.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/shop/useItem/", useItemForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject status = JSONObject.Create(request.downloadHandler.text);

            if (status.IsString)
            {
                if (string.Equals(status.str, "success", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Item used successfully");
                    if (onItemUsed != null)
                    {
                        onItemUsed(this, "success");
                    }
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification(status.str);

                    if (onItemUsed != null)
                    {
                        onItemUsed(this, "fail");
                    }
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Item use failed");

                if (onItemUsed != null)
                {
                    onItemUsed(this, "fail");
                }
            }
        }
    }

    public IEnumerator UseShieldHour(string itemId, long hour)
    {
        WWWForm useItemForm = new WWWForm();
        useItemForm.AddField("cityItemID", itemId);
        useItemForm.AddField("hour", hour.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/shop/useItem/", useItemForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject status = JSONObject.Create(request.downloadHandler.text);

            if (status.IsString)
            {
                if (string.Equals(status.str, "success", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Item used successfully");
                    if (onItemUsed != null)
                    {
                        onItemUsed(this, "success");
                    }
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification(status.str);

                    if (onItemUsed != null)
                    {
                        onItemUsed(this, "fail");
                    }
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Item use failed");

                if (onItemUsed != null)
                {
                    onItemUsed(this, "fail");
                }
            }
        }
    }

    public IEnumerator UseShieldDay(string itemId, long day)
    {
        WWWForm useItemForm = new WWWForm();
        useItemForm.AddField("cityItemID", itemId);
        useItemForm.AddField("day", day.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/shop/useItem/", useItemForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject status = JSONObject.Create(request.downloadHandler.text);

            if (status.IsString)
            {
                if (string.Equals(status.str, "success", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Item used successfully");
                    if (onItemUsed != null)
                    {
                        onItemUsed(this, "success");
                    }
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification(status.str);

                    if (onItemUsed != null)
                    {
                        onItemUsed(this, "fail");
                    }
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Item use failed");

                if (onItemUsed != null)
                {
                    onItemUsed(this, "fail");
                }
            }
        }
    }

    public IEnumerator UseSpeedUp(string itemId, long eventId)
    {
        WWWForm useSpeedUpForm = new WWWForm();
        useSpeedUpForm.AddField("cityItemID", itemId);
        useSpeedUpForm.AddField("eventID", eventId.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/shop/useItem/", useSpeedUpForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject status = JSONObject.Create(request.downloadHandler.text);

            if (status.IsString)
            {
                if (string.Equals(status.str, "success", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Item used successfully");
                    if (onItemUsed != null)
                    {
                        onItemUsed(this, "success");
                    }
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification(status.str);

                    if (onItemUsed != null)
                    {
                        onItemUsed(this, "fail");
                    }
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Item use failed");

                if (onItemUsed != null)
                {
                    onItemUsed(this, "fail");
                }
            }
        }
    }

    public IEnumerator BuyItem(string itemId, long count)
    {
        WWWForm buyItemForm = new WWWForm();
        buyItemForm.AddField("itemID", itemId);
        buyItemForm.AddField("count", count.ToString());
        buyItemForm.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/shop/buyItem/", buyItemForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject status = JSONObject.Create(request.downloadHandler.text);

            if (status.IsString)
            {
                if (string.Equals(status.str, "success", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Items bought successfully");

                    if (onItemBought != null)
                    {
                        onItemBought(this, "success");
                    }
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification(status.str);

                    if (onItemBought != null)
                    {
                        onItemBought(this, "fail");
                    }
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Items not bought");

                if (onItemBought != null)
                {
                    onItemBought(this, "fail");
                }
            }
        }
    }

    public IEnumerator BuyPlayerItem(string itemId, long count)
    {
        WWWForm buyItemForm = new WWWForm();
        buyItemForm.AddField("city_item_lot_id", itemId);
        buyItemForm.AddField("count", count.ToString());
        buyItemForm.AddField("cityID", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://" + GlobalGOScript.instance.host + ":8889/shop/buyCityItemLot/", buyItemForm);
        request.SetRequestHeader("authorization", "Token " + GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            JSONObject status = JSONObject.Create(request.downloadHandler.text);

            if (status.IsString)
            {
                if (string.Equals(status.str, "success", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Items bought successfully");

                    if (onItemBought != null)
                    {
                        onItemBought(this, "success");
                    }
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification(status.str);

                    if (onItemBought != null)
                    {
                        onItemBought(this, "fail");
                    }
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Items not bought");

                if (onItemBought != null)
                {
                    onItemBought(this, "fail");
                }
            }
        }
    }

    public IEnumerator SellItem(string itemId, long count, long discount)
    {
        WWWForm sellItemForm = new WWWForm();
        sellItemForm.AddField("city_item_id", itemId);
        sellItemForm.AddField("count", count.ToString());
        sellItemForm.AddField("price", discount.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://" + GlobalGOScript.instance.host + ":8889/shop/createCityItemLot/", sellItemForm);
        request.SetRequestHeader("authorization", "Token " + GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            JSONObject status = JSONObject.Create(request.downloadHandler.text);

            if (status.IsString)
            {
                if (string.Equals(status.str, "success", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Items sold successfully");

                    if (onItemSold != null)
                    {
                        onItemSold(this, "success");
                    }
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification(status.str);

                    if (onItemSold != null)
                    {
                        onItemSold(this, "fail");
                    }
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Items not sold");

                if (onItemSold != null)
                {
                    onItemSold(this, "fail");
                }
            }
        }
    }

    public IEnumerator GiftItem(long cityID, string itemId, long count)
    {
        WWWForm giftItemForm = new WWWForm();
        giftItemForm.AddField("cityID", cityID.ToString());
        giftItemForm.AddField("cityItemID", itemId);
        giftItemForm.AddField("count", count.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://" + GlobalGOScript.instance.host + ":8889/shop/giftItem/", giftItemForm);
        request.SetRequestHeader("authorization", "Token " + GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            JSONObject status = JSONObject.Create(request.downloadHandler.text);

            if (status.IsString)
            {
                if (string.Equals(status.str, "success", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Items gifted successfully");

                    if (onItemGifted != null)
                    {
                        onItemGifted(this, "success");
                    }
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification(status.str);

                    if (onItemGifted != null)
                    {
                        onItemGifted(this, "fail");
                    }
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Items not gifted");

                if (onItemGifted != null)
                {
                    onItemGifted(this, "fail");
                }
            }
        }
    }
}
