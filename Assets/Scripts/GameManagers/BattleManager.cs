﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class BattleManager {

    public Dictionary<long, ArmyModel> cityArmies;

    public event SimpleEvent onGetCityArmies;
    public event SimpleEvent onMarchLaunched;
    public event SimpleEvent onMarchCanceled;
    public event SimpleEvent onArmyReturnInitiated;

    public IEnumerator GetCityArmies()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/army/allCityArmies/" + GlobalGOScript.instance.gameManager.cityManager.currentCity.id + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject armiesList = JSONObject.Create(request.downloadHandler.text);

            if (armiesList.IsString)
            {
                //fail
                if (onGetCityArmies != null)
                {
                    onGetCityArmies(this, "fail");
                }
            }
            else
            {
                //success
                cityArmies = new Dictionary<long, ArmyModel>();

                foreach (JSONObject army in armiesList.list)
                {
                    ArmyModel temp = JsonUtility.FromJson<ArmyModel>(army.Print());
                    if (!army["hero"].IsNull)
                    {
                        temp.hero = JsonUtility.FromJson<ArmyGeneralModel>(army["hero"].Print());
                    }

                    if (!army["destination_point"].IsNull)
                    {
                        temp.destination_point = JsonUtility.FromJson<WorldFieldModel>(army["destination_point"].Print());

                        if (!army["destination_point"]["owner"].IsNull)
                        {
                            temp.destination_point.owner = JsonUtility.FromJson<SimpleUserModel>(army["destination_point"]["owner"].Print());
                        }
                        else
                        {
                            temp.destination_point.owner = null;
                        }

                        if (!army["destination_point"]["city"].IsNull)
                        {
                            temp.destination_point.city = JsonUtility.FromJson<CityModel>(army["destination_point"]["city"].Print());
                        }
                        else
                        {
                            temp.destination_point.city = null;
                        }
                    }
                    else
                    {
                        temp.destination_point = null;
                    }

                    cityArmies.Add(temp.id, temp);
                }

                if (onGetCityArmies != null)
                {
                    onGetCityArmies(this, "success");
                }
            }
        }
    }

    public IEnumerator March(UnitsModel army, ResourcesModel resources, string attcakType, long destX, long destY, long knightId)
    {
        WWWForm form = new WWWForm();
        form.AddField("army_status", attcakType);
//        form.AddField("destination_city", destinationCity.ToString());
        form.AddField("cityID", army.city.ToString());
        form.AddField("workers_count", army.workers_count.ToString());
        form.AddField("spy_count", army.spy_count.ToString());
        form.AddField("swards_man_count", army.swards_man_count.ToString());
        form.AddField("spear_man_count", army.spear_man_count.ToString());
        form.AddField("pike_man_count", army.pike_man_count.ToString());
        form.AddField("scout_rider_count", army.scout_rider_count.ToString());
        form.AddField("light_cavalry_count", army.light_cavalry_count.ToString());
        form.AddField("heavy_cavalry_count", army.heavy_cavalry_count.ToString());
        form.AddField("archer_count", army.archer_count.ToString());
        form.AddField("archer_rider_count", army.archer_rider_count.ToString());
        form.AddField("holly_man_count", army.holly_man_count.ToString());
        form.AddField("trebuchet_count", army.trebuchet_count.ToString());
        form.AddField("siege_towers_count", army.siege_towers_count.ToString());
        form.AddField("battering_ram_count", army.battering_ram_count.ToString());
        form.AddField("ballista_count", army.ballista_count.ToString());
		form.AddField("wagon_count", army.wagon_count.ToString());
        form.AddField("food", resources.food.ToString());
        form.AddField("wood", resources.wood.ToString());
        form.AddField("stone", resources.stone.ToString());
        form.AddField("iron", resources.iron.ToString());
        form.AddField("cooper", resources.cooper.ToString());
        form.AddField("silver", resources.silver.ToString());
        form.AddField("gold", resources.gold.ToString());
        form.AddField("posX", destX.ToString());
        form.AddField("posY", destY.ToString());
        form.AddField("hero", knightId.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/army/startAttack/", form);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            Debug.Log(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("March started");
                    if (onMarchLaunched != null)
                    {
                        onMarchLaunched(this, "success");
                    }
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification(obj.str);
                    if (onMarchLaunched != null)
                    {
                        onMarchLaunched(this, "fail");
                    }
                }
            }
        }
    }

    public IEnumerator RecruitBarbarians(UnitsModel army, long destX, long destY, long barbariansId)
    {
        WWWForm form = new WWWForm();
        form.AddField("worldMap", barbariansId.ToString());
        form.AddField("armyEmployerCity", army.city.ToString());
        form.AddField("workers_count", army.workers_count.ToString());
        form.AddField("spy_count", army.spy_count.ToString());
        form.AddField("swards_man_count", army.swards_man_count.ToString());
        form.AddField("spear_man_count", army.spear_man_count.ToString());
        form.AddField("pike_man_count", army.pike_man_count.ToString());
        form.AddField("scout_rider_count", army.scout_rider_count.ToString());
        form.AddField("light_cavalry_count", army.light_cavalry_count.ToString());
        form.AddField("heavy_cavalry_count", army.heavy_cavalry_count.ToString());
        form.AddField("archer_count", army.archer_count.ToString());
        form.AddField("archer_rider_count", army.archer_rider_count.ToString());
        form.AddField("holly_man_count", army.holly_man_count.ToString());
        form.AddField("trebuchet_count", army.trebuchet_count.ToString());
        form.AddField("siege_towers_count", army.siege_towers_count.ToString());
        form.AddField("battering_ram_count", army.battering_ram_count.ToString());
        form.AddField("ballista_count", army.ballista_count.ToString());
		form.AddField("wagon_count", army.wagon_count.ToString());
        form.AddField("posX", destX.ToString());
        form.AddField("posY", destY.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://" + GlobalGOScript.instance.host + ":8889/army/createBarbariansAttack/", form);
        request.SetRequestHeader("authorization", "Token " + GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            Debug.Log(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("March started");
                    if (onMarchLaunched != null)
                    {
                        onMarchLaunched(this, string.Empty);
                    }
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification(obj.str);
                }
            }
        }
    }

    public IEnumerator CancelAttack(long armyId)
    {
        WWWForm form = new WWWForm();
        form.AddField("armyID", armyId.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/army/cancelAttack/", form);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            Debug.Log(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    if (onMarchCanceled != null)
                    {
                        onMarchCanceled(this, "success");
                    }
                }
            }
        }
    }

    public IEnumerator ReturnArmy(long armyId)
    {
        WWWForm form = new WWWForm();
        form.AddField("armyID", armyId.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/army/returnArmy/", form);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            Debug.Log(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    if (onArmyReturnInitiated != null)
                    {
                        onArmyReturnInitiated(this, "success");
                    }
                }
                else
                {
                    if (onArmyReturnInitiated != null)
                    {
                        onArmyReturnInitiated(this, "fail");
                    }
                }
            }
        }
    }
}
