﻿using UnityEngine;
using UnityEngine.Networking;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;
using ArabicSupport;

public class GameManager {

    public CityManager cityManager;
    public BattleManager battleManager;
    public LevelLoadManager levelLoadManager;

    public Dictionary<string, BuildingConstantModel> buildingConstants;
    public Dictionary<string, UnitConstantModel> unitConstants;
    public List<TutorialPageModel> tutorials;
    public long[] residenceFields = {6, 8, 10, 13, 17, 21, 26, 32, 38, 48};

    public event SimpleEvent onGetBuildingNames;

    public List<string> buildingIds;

    public IEnumerator GetInitialBuildingConstants()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/buildings/buildings_constants/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            buildingIds = new List<string>();
            buildingConstants = new Dictionary<string, BuildingConstantModel>();
            JSONObject idsArray = JSONObject.Create(request.downloadHandler.text, 2);
            foreach (JSONObject id in idsArray.list)
            {
                buildingIds.Add(id.str);
            }

            if (onGetBuildingNames != null)
            {
                onGetBuildingNames(this, string.Empty);
            }
        }
    }

    public IEnumerator GetBuildingInitialConstantForName(string id)
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/buildings/buildings_constants/"+id+"/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
            GlobalGOScript.instance.windowInstanceManager.closeLoginProgressbar();
        }
        else {
            JSONObject tempObj = JSONObject.Create(request.downloadHandler.text);
            BuildingConstantModel temp = JsonUtility.FromJson<BuildingConstantModel>(tempObj.Print());
//            if (string.Equals(LanguageManager.Instance.CurrentlyLoadedCulture.languageCode, "ar"))
//            {
//                char[] name = tempObj.GetField("building_name").str.ToCharArray();
//                System.Array.Reverse(name);
//                char[] description = tempObj.GetField("description").str.ToCharArray();
//                System.Array.Reverse(description);
//                temp.building_name = new string(name);
//                temp.description = new string(description);
//            }
//            else
//            {
//                temp.building_name = tempObj.GetField("building_name").str;
//                temp.description = tempObj.GetField("description").str;
//            }

            temp.building_prices = new ArrayList();

            foreach (JSONObject upgradeObj in tempObj["building_prices"].list)
            {
                BuildingPrice cost = JsonUtility.FromJson<BuildingPrice>(upgradeObj.Print());
                temp.building_prices.Add(cost);
            }

            buildingConstants.Add(temp.id, temp);

            if (buildingConstants.Count == buildingIds.Count)
            {
                GlobalGOScript.instance.playerManager.loginManager.gotBuildingConstants = true;
            }
        }
    }

    public IEnumerator GetInitialUnitConstants()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/units/units_constant/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            unitConstants = new Dictionary<string, UnitConstantModel>();
            JSONObject unitsArray = JSONObject.Create(request.downloadHandler.text, 2);
            foreach (JSONObject unit in unitsArray.list)
            {
                UnitConstantModel unitModel = JsonUtility.FromJson<UnitConstantModel>(unit.Print());
                unitConstants.Add(unitModel.name, unitModel);
            }

            GlobalGOScript.instance.playerManager.loginManager.gotUnitConstants = true;
        }
    }

    public IEnumerator GetTutorialPages()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/tutorial/tutorials_list/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            tutorials = new List<TutorialPageModel>();
            JSONObject pagesArray = JSONObject.Create(request.downloadHandler.text, 2);
            foreach (JSONObject page in pagesArray.list)
            {
                TutorialPageModel pageModel = JsonUtility.FromJson<TutorialPageModel>(page.Print());
                tutorials.Add(pageModel);
            }

            GlobalGOScript.instance.playerManager.loginManager.gotTutorialPages = true;
        }
    }

    public IEnumerator DownloadTutorialImage(string url, long tutorialID)
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889"+url);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            File.WriteAllBytes(Application.persistentDataPath+"/"+tutorialID.ToString()+".png", request.downloadHandler.data);
        }
    }
}
