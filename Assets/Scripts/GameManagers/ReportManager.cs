﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Globalization;
using SmartLocalization;
using ArabicSupport;

public class ReportManager {
    public Dictionary<long, InviteReportModel> invites;
    public Dictionary<long, InviteReportModel> allianceRequsets;
    public Dictionary<long, BattleReportHeader> battles;
    public Dictionary<long, BattleReportHeader> allianceBattles;
    public Dictionary<long, MessageHeader> inbox;
    public Dictionary<long, SystemReportModel> systemReports;

    public Dictionary<long, UnitTimer> unitTimers;
    public Dictionary<long, BuildingTimer> buildingTimers;
    public Dictionary<long, ArmyTimer> armyTimers;
    public Dictionary<long, ItemTimer> itemTimers;
    public Dictionary<long, WorldMapTimer> worldMapTimers;

    public event SimpleEvent onGetUnreadReports;
    public event SimpleEvent onGetAllianceBattles;
    public event SimpleEvent onUserMessageSent;
    public event SimpleEvent onGetUnitTimers;
    public event SimpleEvent onGetBuildingTimers;
    public event SimpleEvent onGetArmyTimers;
    public event SimpleEvent onGetItemTimers;
    public event SimpleEvent onGetWorldMapTimers;
    public event SimpleEvent onMessageDeleted;
    public event SimpleEvent onBattleReportDeleted;

    public long unreadInboxCount;
    public long unreadBattleCount;
    public long unreadSystemCount;

	private string dateFormat = "yyyy-MM-ddTHH:mm:ss.ffffffZ";

    public IEnumerator ReadBattleReport(long id)
    {
        WWWForm readForm = new WWWForm();
        readForm.AddField("reportID", id.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/reports/readBattleReport/", readForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            //done for now
        }
    }

    public IEnumerator ReadReportMessage(long id)
    {
        WWWForm readForm = new WWWForm();
        readForm.AddField("reportID", id.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/reports/readReportMessage/", readForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            //done for now
        }
    }

    public IEnumerator ReadSystemReport(long id)
    {
        WWWForm readForm = new WWWForm();
        readForm.AddField("reportID", id.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/reports/readSystemReportMessage/", readForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            //done for now
        }
    }

    public IEnumerator SendUserMessage(long userId, string subject, string message)
    {
        WWWForm sendForm = new WWWForm();
        sendForm.AddField("toUser", userId.ToString());
        sendForm.AddField("subject", subject);
        sendForm.AddField("message", message);

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/reports/createMessage/", sendForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            if (onUserMessageSent != null)
            {
                onUserMessageSent(this, "success");
            }
        }
    }

    public IEnumerator GetUnreadReports()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/reports/allUnreadReportsForUser/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                //failed
                if (onGetUnreadReports != null)
                {
                    onGetUnreadReports(this, "fail");
                }
            }
            else if (obj.Count == 0)
            {
                //empty
                if (onGetUnreadReports != null)
                {
                    onGetUnreadReports(this, "fail");
                }
            }
            else
            {
                //success
                JSONObject reportTypes = JSONObject.Create(request.downloadHandler.text);

                if (reportTypes.HasField("battle"))
                {
                    JSONObject battleReports = reportTypes["battle"];

                    if (battleReports.list.Count != 0)
                    {
                        if (battles == null)
                        {
                            battles = new Dictionary<long, BattleReportHeader>();
                        }

                        foreach (JSONObject battleReport in battleReports.list)
                        {
                            ParseBattleReportHeader(battleReport);
                        }
                    }
                }

                if (reportTypes.HasField("invite"))
                {
                    JSONObject inviteReports = reportTypes["invite"];

                    if (inviteReports.list.Count != 0)
                    {
                        if (invites == null)
                        {
                            invites = new Dictionary<long, InviteReportModel>();
                        }
                        if (allianceRequsets == null)
                        {
                            allianceRequsets = new Dictionary<long, InviteReportModel>();
                        }
                        
                        foreach (JSONObject inviteReport in inviteReports.list)
                        {
                            ParseInviteReport(inviteReport);
                        }   
                    }
                }

                if (reportTypes.HasField("message"))
                {
                    JSONObject messageHeaders = reportTypes["message"];

                    if (messageHeaders.list.Count != 0)
                    {
                        //if (inbox == null)
                        //{
                        inbox = new Dictionary<long, MessageHeader>();
                        //}

                        foreach (JSONObject messageHeader in messageHeaders.list)
                        {
                            ParseMessageReportHeader(messageHeader);
                        }
                    }
                }

                if (reportTypes.HasField("system"))
                {
                    JSONObject systemReports = reportTypes["system"];

                    if (systemReports.list.Count != 0)
                    {
                        if (this.systemReports == null)
                        {
                            this.systemReports = new Dictionary<long, SystemReportModel>();
                        }

                        foreach (JSONObject systemReport in systemReports.list)
                        {
                            ParseSystemReport(systemReport);
                        }   
                    }
                }

                if (onGetUnreadReports != null)
                {
                    onGetUnreadReports(this, "success");
                }
            }    

        }
    }
        
    public IEnumerator GetUnreadAllianceBattleReports()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/reports/allianceBattleReports/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                //failed
                if (onGetUnreadReports != null)
                {
                    onGetUnreadReports(this, "fail");
                }
            }
            else if (obj.Count == 0)
            {
                //empty
                if (onGetUnreadReports != null)
                {
                    onGetUnreadReports(this, "fail");
                }
            }
            else
            {
                //success
                if (obj.list.Count != 0)
                {
                    if (allianceBattles == null)
                    {
                        allianceBattles = new Dictionary<long, BattleReportHeader>();
                    }

                    foreach (JSONObject battleReport in obj.list)
                    {
                        ParseAllianceBattleReportHeader(battleReport);
                    }
                }

                if (onGetAllianceBattles != null)
                {
                    onGetAllianceBattles(this, "success");
                }
            }    

        }
    }


    public void ParseMessageReportHeader(JSONObject messageHeader)
    {
        MessageHeader header = JsonUtility.FromJson<MessageHeader>(messageHeader.Print());

        if (!messageHeader["creation_date"].IsNull)
        {
            header.creation_date = DateTime.ParseExact(messageHeader["creation_date"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
        }

        //if (!inbox.ContainsKey(header.id))
        //{
            inbox.Add(header.id, header);
        //}
    }

//    public void ParseMessageReport(JSONObject messageReport)
//    {
//        UserMessageReportModel message = JsonUtility.FromJson<UserMessageReportModel>(messageReport.Print());
//
//        message.fromUser = JsonUtility.FromJson<UserModel>(messageReport["fromUser"].Print());
//        message.fromUser.capital = new MyCityModel();
//
//        message.fromUser.capital.city_name = messageReport["fromUser"]["capital"]["city_name"].str;
//        message.fromUser.capital.posX = messageReport["fromUser"]["capital"]["posX"].i;
//        message.fromUser.capital.posY = messageReport["fromUser"]["capital"]["posY"].i;
//
//        if (!messageReport["fromUser"]["alliance"].IsNull)
//        {
//            message.fromUser.alliance = new AllianceModel();
//            message.fromUser.alliance.alliance_name = messageReport["fromUser"]["alliance"]["alliance_name"].str;
//            message.fromUser.alliance.id = messageReport["fromUser"]["alliance"]["id"].i;
//        }
//
//        if (!messageReport["fromUser"]["last_login"].IsNull)
//        {
//			message.fromUser.last_login = DateTime.ParseExact(messageReport["fromUser"]["last_login"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
//        }
//
//        if (!messageReport["creation_date"].IsNull)
//        {
//			message.creation_date = DateTime.ParseExact(messageReport["creation_date"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
//        }
//
//        if (!inbox.ContainsKey(message.id))
//        {
//            inbox.Add(message.id, message);
//        }
//    }

    public void ParseInviteReport(JSONObject inviteReport)
    {
        InviteReportModel invite = new InviteReportModel();
        invite.id = inviteReport["id"].i;

        if (!inviteReport["inviteByUser"].IsNull)
        {
            invite.inviteFromId = inviteReport["inviteByUser"]["id"].i;
            invite.inviteFromName = inviteReport["inviteByUser"]["username"].str;
        }

        invite.inviteToId = inviteReport["invitedUser"]["id"].i;
        invite.inviteToName = inviteReport["invitedUser"]["username"].str;

        invite.isRead = inviteReport["isRead"].b;

        invite.allianceId = inviteReport["alliance"]["id"].i;
        invite.allianceName = inviteReport["alliance"]["alliance_name"].str;
        invite.allianceRank = inviteReport["alliance"]["rank"].i;

        invite.type = inviteReport["type"].str;

        if (!inviteReport["creationDate"].IsNull)
        {
			invite.creationDate = DateTime.ParseExact(inviteReport["creationDate"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
        }

        if (string.Equals(invite.type, "invite"))
        {
            if (!invites.ContainsKey(invite.id))
            {
                invites.Add(invite.id, invite);
            }            
        }
        else if (string.Equals(invite.type, "request"))
        {
            if (!allianceRequsets.ContainsKey(invite.id))
            {
                allianceRequsets.Add(invite.id, invite);
            }
        }
    }

    public void ParseSystemReport(JSONObject systemReport)
    {
        SystemReportModel system = new SystemReportModel();
        system.id = systemReport["id"].i;
        system.subject = systemReport["subject"].str;
        system.message = systemReport["message"].str;
        system.isRead = systemReport["isRead"].b;

        if (!systemReport["creation_date"].IsNull)
        {
            system.creation_date = DateTime.ParseExact(systemReport["creation_date"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
        }
            
        if (!systemReports.ContainsKey(system.id))
        {
            systemReports.Add(system.id, system);
        }            
    }
        
    public void ParseBattleReportHeader(JSONObject reportHeader)
    {
        if (!battles.ContainsKey(reportHeader["id"].i))
        {
            BattleReportHeader header = JsonUtility.FromJson<BattleReportHeader>(reportHeader.Print());

            if (!reportHeader["creation_date"].IsNull)
            {
                header.creation_date = DateTime.ParseExact(reportHeader["creation_date"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
            }

            battles.Add(header.id, header);
        }
    }

    public void ParseAllianceBattleReportHeader(JSONObject reportHeader)
    {
        if (!allianceBattles.ContainsKey(reportHeader["id"].i))
        {
            BattleReportHeader header = JsonUtility.FromJson<BattleReportHeader>(reportHeader.Print());

            if (!reportHeader["creation_date"].IsNull)
            {
                header.creation_date = DateTime.ParseExact(reportHeader["creation_date"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
            }

            allianceBattles.Add(header.id, header);
        }
    }

    public IEnumerator GetUnitTimers(bool initial)
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/updateEvents/unitEvents/" + GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString() + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject timerArray = JSONObject.Create(request.downloadHandler.text);
            if (unitTimers == null || initial)
            {
                unitTimers = new Dictionary<long, UnitTimer>();
            }

            foreach (JSONObject timer in timerArray.list)
            {
                if (!unitTimers.ContainsKey(timer["event"]["id"].i))
                {
                    UnitTimer temp = JsonUtility.FromJson<UnitTimer>(timer.Print());
                    temp.event_id = timer["event"]["id"].i;
					temp.start_time = DateTime.ParseExact(timer["event"]["start_time"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
					temp.finish_time = DateTime.ParseExact(timer["event"]["finish_time"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);

                    unitTimers.Add(temp.event_id, temp);
                    GlobalGOScript.instance.underAttackNotification.SetActive(true);
                }
            }

            if (onGetUnitTimers != null)
            {
                onGetUnitTimers(this, "success");
            }
        }
    }

    public IEnumerator GetBuildingTimers(bool initial)
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/updateEvents/buildingsEvents/" + GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString() + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject timerArray = JSONObject.Create(request.downloadHandler.text);
            if (buildingTimers == null || initial)
            {
                buildingTimers = new Dictionary<long, BuildingTimer>();
            }

            foreach (JSONObject timer in timerArray.list)
            {
                if (!buildingTimers.ContainsKey(timer["event"]["id"].i))
                {
                    BuildingTimer temp = JsonUtility.FromJson<BuildingTimer>(timer.Print());
                    temp.event_id = timer["event"]["id"].i;
					temp.start_time = DateTime.ParseExact(timer["event"]["start_time"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
					temp.finish_time = DateTime.ParseExact(timer["event"]["finish_time"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                    temp.building = JsonUtility.FromJson<BuildingModel>(timer["building"].Print());

                    buildingTimers.Add(temp.event_id, temp);
                    GlobalGOScript.instance.underAttackNotification.SetActive(true);
                }
            }

            if (onGetBuildingTimers != null)
            {
                onGetBuildingTimers(this, "success");
            }
        }
    }

    public IEnumerator GetArmyTimers(bool initial)
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/updateEvents/armyEvents/" + GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString() + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject timerObject = JSONObject.Create(request.downloadHandler.text);
            if (armyTimers == null || initial)
            {
                armyTimers = new Dictionary<long, ArmyTimer>();
            }

            foreach (JSONObject timer in timerObject["enemy"].list)
            {
                if (!armyTimers.ContainsKey(timer["event"]["id"].i))
                {
                    ArmyTimer temp = JsonUtility.FromJson<ArmyTimer>(timer.Print());
                    temp.event_id = timer["event"]["id"].i;
					temp.start_time = DateTime.ParseExact(timer["event"]["start_time"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
					temp.finish_time = DateTime.ParseExact(timer["event"]["finish_time"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                    temp.enemy = true;

                    armyTimers.Add(temp.event_id, temp);
                    GlobalGOScript.instance.underAttackNotification.SetActive(true);
                }
            }

            foreach (JSONObject timer in timerObject["my"].list)
            {
                if (!armyTimers.ContainsKey(timer["event"]["id"].i))
                {
                    ArmyTimer temp = JsonUtility.FromJson<ArmyTimer>(timer.Print());
                    temp.event_id = timer["event"]["id"].i;
					temp.start_time = DateTime.ParseExact(timer["event"]["start_time"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
					temp.finish_time = DateTime.ParseExact(timer["event"]["finish_time"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                    temp.enemy = false;

                    armyTimers.Add(temp.event_id, temp);
                    GlobalGOScript.instance.underAttackNotification.SetActive(true);
                }
            }

            foreach (JSONObject timer in timerObject["ally"].list)
            {
                if (!armyTimers.ContainsKey(timer["event"]["id"].i))
                {
                    ArmyTimer temp = JsonUtility.FromJson<ArmyTimer>(timer.Print());
                    temp.event_id = timer["event"]["id"].i;
					temp.start_time = DateTime.ParseExact(timer["event"]["start_time"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
					temp.finish_time = DateTime.ParseExact(timer["event"]["finish_time"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                    temp.enemy = false;

                    armyTimers.Add(temp.event_id, temp);
                    GlobalGOScript.instance.underAttackNotification.SetActive(true);
                }
            }


            if (onGetArmyTimers != null)
            {
                onGetArmyTimers(this, "success");
            }
        }
    }

    public IEnumerator GetItemTimers(bool initial)
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/updateEvents/customEvents/" + GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString() + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject timerArray = JSONObject.Create(request.downloadHandler.text);
            if (itemTimers == null || initial)
            {
                itemTimers = new Dictionary<long, ItemTimer>();
            }

            foreach (JSONObject timer in timerArray.list)
            {
                if (!itemTimers.ContainsKey(timer["event"]["id"].i))
                {
                    ItemTimer temp = JsonUtility.FromJson<ItemTimer>(timer.Print());
                    temp.event_id = timer["event"]["id"].i;
					temp.start_time = DateTime.ParseExact(timer["event"]["start_time"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
					temp.finish_time = DateTime.ParseExact(timer["event"]["finish_time"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                    if (string.Equals(LanguageManager.Instance.CurrentlyLoadedCulture.languageCode, "ar"))
                    {
                        char[] name = temp.name.ToCharArray();
                        System.Array.Reverse(name);
                        temp.name = ArabicFixer.Fix(new string(name));
                    }

                    itemTimers.Add(temp.event_id, temp);
                    GlobalGOScript.instance.underAttackNotification.SetActive(true);
                }
            }


            if (onGetItemTimers != null)
            {
                onGetItemTimers(this, "success");
            }
        }
    }

    public IEnumerator GetWorldMapTimers(bool initial)
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/updateEvents/worldMapEvents/" + GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString() + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject timerArray = JSONObject.Create(request.downloadHandler.text);
            if (worldMapTimers == null || initial)
            {
                worldMapTimers = new Dictionary<long, WorldMapTimer>();
            }

            foreach (JSONObject timer in timerArray.list)
            {
                if (!worldMapTimers.ContainsKey(timer["event"]["id"].i))
                {
                    WorldMapTimer temp = JsonUtility.FromJson<WorldMapTimer>(timer.Print());
                    temp.event_id = timer["event"]["id"].i;
					temp.start_time = DateTime.ParseExact(timer["event"]["start_time"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
					temp.finish_time = DateTime.ParseExact(timer["event"]["finish_time"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);

                    worldMapTimers.Add(temp.event_id, temp);
                }
            }

            if (onGetWorldMapTimers != null)
            {
                onGetWorldMapTimers(this, "success");
            }
        }
    }

    public IEnumerator DeleteMessageReport(long id)
    {
        WWWForm deleteForm = new WWWForm();
        deleteForm.AddField("id", id.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/reports/deleteMessageReports/", deleteForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            JSONObject obj = JSONObject.Create(request.downloadHandler.text);

            if (string.Equals(obj.str, "success"))
            {
                inbox.Remove(id);

                if (onMessageDeleted != null)
                {
                    onMessageDeleted(this, "success");
                }
            }
        }
    }

    public IEnumerator DeleteBattleReport(long id)
    {
        WWWForm deleteForm = new WWWForm();
        deleteForm.AddField("id", id.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/reports/deleteBattleReports/", deleteForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            JSONObject obj = JSONObject.Create(request.downloadHandler.text);

            if (string.Equals(obj.str, "success"))
            {
                battles.Remove(id);

                if (onBattleReportDeleted != null)
                {
                    onBattleReportDeleted(this, "success");
                }
            }
        }
    }

    public IEnumerator DeleteScoutReport(long id)
    {
        WWWForm deleteForm = new WWWForm();
        deleteForm.AddField("id", id.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/reports/deleteScoutReports/", deleteForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            JSONObject obj = JSONObject.Create(request.downloadHandler.text);

            if (string.Equals(obj.str, "success"))
            {
                battles.Remove(id);

                if (onBattleReportDeleted != null)
                {
                    onBattleReportDeleted(this, "success");
                }
            }
        }
    }
}
