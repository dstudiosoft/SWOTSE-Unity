﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using SmartLocalization;

public delegate void SimpleEvent(object sender, string value);
public delegate void ConfirmationEvent(bool value);

public class GlobalGOScript : MonoBehaviour {

    public static GlobalGOScript instance;

    public PlayerManager playerManager;
    public GameManager gameManager;
    #if UNITY_IOS || UNITY_ANDROID
    public GCMManager messagesManager;
    #endif

    public string host;
    public string token;
    public WindowInstanceManager windowInstanceManager;
//    public WorldChat worldChat;
//    public AllianceChat allianceChat;
    public CityChangerContentManager cityChanger;
    public PanelKnightsTableController knightsTable;

    public GameObject newMailNotification;
    public GameObject underAttackNotification;
    public GameObject adBonusNotification;

	void Start () 
    {
        if (instance == null)
		{	Debug.Log ("START GlobalGOScript");
            GameObject.DontDestroyOnLoad(LanguageManager.Instance);
			instance = this;
            DontDestroyOnLoad(gameObject);
            initClassTree();
            StartCoroutine(LoadCoroutine());
        }
	}
		
    IEnumerator LoadCoroutine()
    {
        #if DEBUG
        yield return new WaitForSeconds(0);
        #else
        yield return new WaitForSeconds(2);
        #endif
        SceneManager.LoadScene(1);
    }

    public void initClassTree()
    {
        GlobalGOScript.instance.playerManager = new PlayerManager();
        GlobalGOScript.instance.playerManager.loginManager =  GlobalGOScript.instance.gameObject.AddComponent<LoginManager>();
        GlobalGOScript.instance.playerManager.allianceManager = new AllianceManager();
        GlobalGOScript.instance.playerManager.reportManager = new ReportManager();

        GlobalGOScript.instance.gameManager = new GameManager();
        GlobalGOScript.instance.gameManager.cityManager = new CityManager();
        GlobalGOScript.instance.gameManager.cityManager.buildingsManager = new BuildingsManager();
        GlobalGOScript.instance.gameManager.cityManager.resourceManager = instance.gameObject.AddComponent<ResourceManager>();
        GlobalGOScript.instance.gameManager.cityManager.unitsManager = new UnitsManager();
        GlobalGOScript.instance.gameManager.cityManager.unitsManager.onGetGenerals += knightsTable.GeneralsUpdated;
        GlobalGOScript.instance.gameManager.cityManager.treasureManager = new TreasureManager();
        GlobalGOScript.instance.gameManager.battleManager = new BattleManager();
        GameObject.DontDestroyOnLoad(instance.gameManager.cityManager.resourceManager);
        GlobalGOScript.instance.gameManager.levelLoadManager = instance.gameObject.AddComponent<LevelLoadManager>();
        GameObject.DontDestroyOnLoad(instance.gameManager.levelLoadManager);
        GlobalGOScript.instance.gameManager.levelLoadManager.mainUI = GameObject.Find("UI");
        GlobalGOScript.instance.gameManager.levelLoadManager.zoomMenu = GameObject.Find("UI/zoomMenu");
        GlobalGOScript.instance.gameManager.levelLoadManager.coordsMenu = GameObject.Find("UI/newDesign/coordsMenu");
        GlobalGOScript.instance.gameManager.levelLoadManager.touchManager = GameObject.Find("TouchManager");
        GameObject.DontDestroyOnLoad(GlobalGOScript.instance.gameManager.levelLoadManager.mainUI);
        GameObject.DontDestroyOnLoad(GlobalGOScript.instance.gameManager.levelLoadManager.touchManager);
        GlobalGOScript.instance.gameManager.levelLoadManager.mainUI.SetActive(false);
        if(GlobalGOScript.instance.gameManager.levelLoadManager.coordsMenu != null)
            GlobalGOScript.instance.gameManager.levelLoadManager.coordsMenu.SetActive(false);
        GlobalGOScript.instance.gameManager.levelLoadManager.touchManager.SetActive(false);
        GameObject.DontDestroyOnLoad(GlobalGOScript.instance.windowInstanceManager);
    }
        
    public void GetCities(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed -= instance.GetCities;

        if (string.Equals(value, "success"))
        {
            StartCoroutine(instance.gameManager.cityManager.GetInitialCities());
        }
    }

    public void loadFields()
    {
        instance.gameManager.levelLoadManager.LoadFields();
    }

    public void loadCity()
    {
        instance.gameManager.levelLoadManager.LoadCity();
    }

    public void loadWorld()
    {
        instance.gameManager.levelLoadManager.LoadWorld();
    }
        
    public void LogOut()
    {
        GlobalGOScript.instance.windowInstanceManager.Shell.SetActive(false);
        GlobalGOScript.instance.playerManager.loginManager.LogOut(false);
        instance.gameManager.levelLoadManager.LoadLogin();
    }

    public void Relogin()
    {
        GlobalGOScript.instance.playerManager.loginManager.LogOut(true);
        instance.gameManager.levelLoadManager.LoadLogin();
    }

    public void openMyUserInfo()
    {
        GlobalGOScript.instance.windowInstanceManager.openUserInfo(GlobalGOScript.instance.playerManager.user);
    }
}
