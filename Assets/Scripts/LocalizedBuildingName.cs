﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LocalizedBuildingName : MonoBehaviour {

    public string key;

	void OnEnable () 
    {
        string localizedText = GlobalGOScript.instance.gameManager.buildingConstants[key].building_name.Replace("_", " ");
		if (SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar") {
			localizedText = ArabicSupport.ArabicFixer.Fix (localizedText);
		}
        if (!string.IsNullOrEmpty(localizedText))
        {
            gameObject.GetComponent<Text>().text = localizedText;
        }
	}
}
