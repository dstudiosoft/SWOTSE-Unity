﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;
using ArabicSupport;

public class ShieldTimeContentManager : MonoBehaviour {

    public bool hour;
    public IReloadable owner;
    public string itemId;
    public Text timeLabel;
    public Dropdown timeValue;

    private static List<string> days = new List<string>() {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

    void OnEnable()
    {
        List<string> options = null;
        string localizedValue = string.Empty;

        if (hour)
        {
            timeLabel.text = "HOUR";
            options = new List<string>(24);
            for (int i = 0; i < 24; ++i)
            {
                options.Add(i.ToString());
            }
        }
        else
        {
            timeLabel.text = "DAY";
            options = new List<string>(days.Count);
            foreach (string day in days)
            {
                localizedValue = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + day);

                if (LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
                {
                    localizedValue = ArabicFixer.Fix(localizedValue);
                }

                options.Add(localizedValue);
            }
        }
        timeValue.AddOptions(options);
    }

    void ItemUsed(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed -= ItemUsed;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.closeShieldTimeWindow();
        }
    }

    public void Ok()
    {
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += owner.ReloadTable;
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += ItemUsed;

        if (hour)
        {
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.UseShieldHour(itemId, timeValue.value));
        }
        else
        {
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.UseShieldDay(itemId, timeValue.value));
        }
    }

    public void Cancel()
    {
        GlobalGOScript.instance.windowInstanceManager.closeShieldTimeWindow();
    }
}
