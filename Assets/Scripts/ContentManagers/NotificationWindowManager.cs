﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NotificationWindowManager : MonoBehaviour {

    public Text notificationText;

    public void SetNotificationMessage(string notification)
    {
        notificationText.text = notification;
    }

}
