﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MessageAllianceManager : MonoBehaviour {

    public InputField subject;
    public InputField message;

    private long userId;

    public void SetUserId(long id)
    {
        userId = id;
    }

    public void SendUserMessage()
    {
        if (!string.IsNullOrEmpty(message.text))
        {
            GlobalGOScript.instance.playerManager.reportManager.onUserMessageSent += UserMessageSent;
            StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.SendUserMessage(userId, subject.text, message.text));
        }
    }

    public void UserMessageSent(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.reportManager.onUserMessageSent -= UserMessageSent;

        if (string.Equals(value, "success"))
        {
            subject.text = string.Empty;
            message.text = string.Empty;

            GlobalGOScript.instance.windowInstanceManager.openNotification("Message successfully sent");
            GlobalGOScript.instance.windowInstanceManager.closeMessageAlliance();
        }
    }
}
