﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;

public class UserInfoContentManager : MonoBehaviour {

    public GameObject currentUserPanel;
    public GameObject otherUserPanel;

    public GameObject otherPlayerActions;
    public GameObject allianceMemberActions;

    //current user
    public InputField currentUserNick;
    public InputField currentUserEmail;
    public InputField currentUserMobile;
    public InputField currentUserFlag;
    public Image currentUserImage;

    //other user
    public Text otherUserNick;
    public Text otherUserExperience;
    public Text otherUserRank;
    public Text otherUserCities;
    public Text otherUserCapital;
    public Text otherUserCapitalCoords;
    public Text otherUserLastOnline;
    public Text otherUserFlag;
    public Image otherUserImage;
    //other user specific
    public Text otherUserPosition;
    public Text otherUserAlliance;

    //other user alliance member
    public Dropdown allianceMemberPosition;
    public GameObject changeBtn;
    public GameObject excludeBtn;

    public UserModel user;

    private long iconIndex;
    private string[] ranks;

    void OnEnable()
    {
        if (user.id == GlobalGOScript.instance.playerManager.user.id)
        {
            //init my user
            currentUserNick.text = user.username;
            currentUserEmail.text = user.email;
            //            currentUserMobile.text = user. mobile
            currentUserFlag.text = user.flag;
            if (!string.IsNullOrEmpty(user.image_name))
            {
                iconIndex = long.Parse(user.image_name);
                Sprite userSprite = Resources.Load<Sprite>("UI/UserIcon/" + user.image_name);
                currentUserImage.sprite = userSprite;
            }

            currentUserPanel.SetActive(true);
        }
        else if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance != null)
        {
            if (GlobalGOScript.instance.playerManager.allianceManager.allianceMembers.ContainsKey(user.id))
            {
                //init alliance member
                ranks = new string[5] { "Founder", "Leader", "Vice Host", "Commander", "Soldier" };
                List<string> localizedRanks = new List<string>(5);

                for (int i = 0; i < ranks.Length; i++)
                {
                    localizedRanks.Add(LanguageManager.Instance.GetTextValue("SWOTSELocalization." + ranks[i].Replace(" ", "")));
                }

                allianceMemberPosition.AddOptions(localizedRanks);

                int rankIndex;

                otherUserNick.text = user.username;
                otherUserExperience.text = user.experience.ToString();
//                otherUserRank.text = LanguageManager.Instance.GetTextValue("SWOTSELoclaization." + user.rank);
                otherUserRank.text = user.rank;
                otherUserCities.text = user.city_count.ToString();
                otherUserCapital.text = user.capital.city_name;
                otherUserCapitalCoords.text = user.capital.posX.ToString() + ", " + user.capital.posY.ToString();
                otherUserLastOnline.text = user.last_login.ToString();
                //            otherUserFlag.text = usealliance flag
                Sprite userSprite = Resources.Load<Sprite>("UI/UserIcon/" + user.image_name);
                otherUserImage.sprite = userSprite;

                for (rankIndex = 0; rankIndex < ranks.Length; rankIndex++)
                {
                    if (string.Equals(user.alliance_rank, ranks[rankIndex]))
                    {
                        break;
                    }
                }
                allianceMemberPosition.value = rankIndex;

                if (string.Equals(GlobalGOScript.instance.playerManager.user.alliance_rank, "Soldier"))
                {
                    excludeBtn.SetActive(false);
                }

                otherUserPanel.SetActive(true);
                allianceMemberActions.SetActive(true);
            }
            else
            {
                //init other user
                otherUserNick.text = user.username;
                otherUserExperience.text = user.experience.ToString();
//                otherUserRank.text = LanguageManager.Instance.GetTextValue("SWOTSELoclaization." + user.rank);
                otherUserRank.text = user.rank;
                otherUserCities.text = user.city_count.ToString();
                otherUserCapital.text = user.capital.city_name;
                otherUserCapitalCoords.text = user.capital.posX.ToString() + ", " + user.capital.posY.ToString();
                otherUserLastOnline.text = user.last_login.ToString();
                //            otherUserFlag.text = usealliance flag
                Sprite userSprite = Resources.Load<Sprite>("UI/UserIcon/" + user.image_name);
                otherUserImage.sprite = userSprite;

                if (user.alliance == null)
                {
                    otherUserAlliance.text = "None";
                    otherUserPosition.text = "None";
                }
                else
                {
                    otherUserAlliance.text = user.alliance.alliance_name;
                    otherUserPosition.text = LanguageManager.Instance.GetTextValue("SWOTSELoclaization." + user.alliance_rank.Replace(" ", ""));
                }

                otherUserPanel.SetActive(true);
                otherPlayerActions.SetActive(true);
            }
        }
        else
        {
            //init other user
            otherUserNick.text = user.username;
            otherUserExperience.text = user.experience.ToString();
//            otherUserRank.text = LanguageManager.Instance.GetTextValue("SWOTSELoclaization." + user.rank);
            otherUserRank.text = user.rank;
            otherUserCities.text = user.city_count.ToString();
            otherUserCapital.text = user.capital.city_name;
            otherUserCapitalCoords.text = user.capital.posX.ToString() + ", " + user.capital.posY.ToString();
            otherUserLastOnline.text = user.last_login.ToString();
            otherUserFlag.text = user.flag;
            Sprite userSprite = Resources.Load<Sprite>("UI/UserIcon/" + user.image_name);
            otherUserImage.sprite = userSprite;

            otherUserPosition.text = LanguageManager.Instance.GetTextValue("SWOTSELoclaization." + user.alliance_rank.Replace(" ", ""));

            if (user.alliance == null)
            {
                otherUserAlliance.text = "None";
                otherUserPosition.text = "None";
            }
            else
            {
                otherUserAlliance.text = user.alliance.alliance_name;
                otherUserPosition.text = LanguageManager.Instance.GetTextValue("SWOTSELoclaization." + user.alliance_rank.Replace(" ", ""));
            }

            otherUserPanel.SetActive(true);
            otherPlayerActions.SetActive(true);
        }
    }

    public void SwitchIcon(bool forward)
    {
        Sprite userSprite;

        if (forward)
        {
            if (iconIndex == 20)
            {
                iconIndex = 1;
            }
            else
            {
                iconIndex++;
            }
        }
        else
        {
            if (iconIndex == 1)
            {
                iconIndex = 20;
            }
            else
            {
                iconIndex--;
            }
        }

        userSprite = Resources.Load<Sprite>("UI/UserIcon/" + iconIndex.ToString());

        currentUserImage.sprite = userSprite;
    }

    public void SaveUserData()
    {
        //change user data
        if (!string.IsNullOrEmpty(currentUserNick.text) && !string.IsNullOrEmpty(currentUserEmail.text))
        {
            StartCoroutine(GlobalGOScript.instance.playerManager.ChangeUserInfo(currentUserNick.text, currentUserEmail.text, iconIndex.ToString(), currentUserFlag.text));
        }
    }
        
    public void ChangeMemberPosition()
    {
        if (!string.Equals(GlobalGOScript.instance.playerManager.user.alliance_rank, "Soldier") && !string.Equals(GlobalGOScript.instance.playerManager.user.alliance_rank, "Commander"))
        {
            StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.SetMemberRank(ranks[allianceMemberPosition.value], user.id));
        }
    }

    public void MessageMember()
    {
        GlobalGOScript.instance.windowInstanceManager.openMessageAlliance(user.id);
    }

    public void ExcludeMember()
    {
        if (!string.Equals(GlobalGOScript.instance.playerManager.user.alliance_rank, "Soldier"))
        {
            GlobalGOScript.instance.playerManager.allianceManager.onExcludeUser += MemberExcluded;
            StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.ExcludeUser(user.id));
        }
    }

    public void MemberExcluded(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.allianceManager.onExcludeUser -= MemberExcluded;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.openNotification("Member excluded");
            GlobalGOScript.instance.windowInstanceManager.closeUserInfo();
        }
    }
}