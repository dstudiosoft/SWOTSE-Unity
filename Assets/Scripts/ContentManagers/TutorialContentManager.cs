﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using ArabicSupport;

public class TutorialContentManager : MonoBehaviour {

    public Text pageTitle;
    public Text pageText;
    public Image pageImage;

    int currrentPage = 0;

	void Start () {
        UpdateContent();
	}

    void UpdateContent()
    {
        if (SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
        {            
            pageText.alignment = TextAnchor.UpperRight;
            pageTitle.text = ArabicFixer.Fix(GlobalGOScript.instance.gameManager.tutorials[currrentPage].title);
            StartCoroutine(TextLocalizer.WrapMultiline(pageText, GlobalGOScript.instance.gameManager.tutorials[currrentPage].text));
        }
        else
        {
            pageTitle.text = GlobalGOScript.instance.gameManager.tutorials[currrentPage].title;
            pageText.text = GlobalGOScript.instance.gameManager.tutorials[currrentPage].text;
        }

        if (File.Exists(Application.persistentDataPath+"/"+GlobalGOScript.instance.gameManager.tutorials[currrentPage].id.ToString() + ".png"))
        {
            byte[] imageData = File.ReadAllBytes(Application.persistentDataPath+"/"+GlobalGOScript.instance.gameManager.tutorials[currrentPage].id.ToString()+".png");
            Texture2D imageTexture = new Texture2D(1, 1);
            imageTexture.LoadImage(imageData);
            Sprite imageSprite = Sprite.Create(imageTexture, new Rect(0, 0, imageTexture.width, imageTexture.height), new Vector2(0.5f, 0.5f));
            pageImage.sprite = imageSprite;
        }
    }

    public void NextPage()
    {
        if (currrentPage < GlobalGOScript.instance.gameManager.tutorials.Count - 1)
        {
            currrentPage++;
            UpdateContent();
        }
    }

    public void PreviousPage()
    {
        if (currrentPage > 0)
        {
            currrentPage--;
            UpdateContent();
        }
    }
}
