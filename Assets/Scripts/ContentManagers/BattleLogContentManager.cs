﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BattleLogContentManager : MonoBehaviour {

    public Text content;

    public string log;

	void OnEnable () 
    {
        content.text = log;
	}
}
