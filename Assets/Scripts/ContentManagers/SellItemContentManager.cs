﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SellItemContentManager : MonoBehaviour {

    public Text itemName;
    public Text itemsOwned;
    public Image itemImage;
    public Dropdown discount;
    public InputField quantity;

    private IReloadable owner;
    private long itemcount;
    private string itemId;
    private long toSellCount;

    public void SetContent(Sprite image, string name, string count, string id, IReloadable controller)
    {
        owner = controller;
        itemImage.sprite = image;
        itemName.text = name;
        itemcount = long.Parse(count);
        itemsOwned.text = count;
        itemId = id;
    }

    public void SellItems()
    {
        if (!string.IsNullOrEmpty(quantity.text))
        {
            long.TryParse(quantity.text, out toSellCount);
            if (toSellCount > 0)
            {
                GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemSold += ItemsSold;
                GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemSold += owner.ReloadTable;

                long discountPercent = discount.value * 5;

                StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.SellItem(itemId, toSellCount, discountPercent));
            }
        }
    }

    public void ItemsSold(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemSold -= ItemsSold;
        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.playerManager.onGetMyUserInfo += Close;
            StartCoroutine(GlobalGOScript.instance.playerManager.getMyUserInfo(false));
            itemcount -= toSellCount;
            itemsOwned.text = itemcount.ToString();
        }
    }

    public void Close(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.onGetMyUserInfo -= Close;
        GlobalGOScript.instance.windowInstanceManager.closeSellItem();
    }

    public void Cancel()
    {
        GlobalGOScript.instance.windowInstanceManager.closeSellItem();
    }
}
