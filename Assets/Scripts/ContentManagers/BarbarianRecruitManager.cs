﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class BarbarianRecruitManager : MonoBehaviour
{
    public Text citySilver;
    public Text cityGold;
    public Text priceSilver;
    public Text priceGold;

    public Text cityWorker;
    public Text citySpy;
    public Text citySwordsman;
    public Text citySpearman;
    public Text cityPikeman;
    public Text cityScoutRider;
    public Text cityLightCavalry;
    public Text cityHeavyCavalry;
    public Text cityArcher;
    public Text cityArcherRider;
    public Text cityHollyman;
    public Text cityWagon;
    public Text cityTrebuchet;
    public Text citySiegeTower;
    public Text cityBatteringRam;
    public Text cityBallista;

    public InputField dispatchWorker;
    public InputField dispatchSpy;
    public InputField dispatchSwordsman;
    public InputField dispatchSpearman;
    public InputField dispatchPikeman;
    public InputField dispatchScoutRider;
    public InputField dispatchLightCavalry;
    public InputField dispatchHeavyCavalry;
    public InputField dispatchArcher;
    public InputField dispatchArcherRider;
    public InputField dispatchHollyman;
    public InputField dispatchWagon;
    public InputField dispatchTrebuchet;
    public InputField dispatchSiegeTower;
    public InputField dispatchBatteringRam;
    public InputField dispatchBallista;

    public InputField xInput;
    public InputField yInput;

    public WorldFieldModel targetField;

    private SimpleEvent onGetBarbariansArmy;

    private UnitsModel presentArmy;
    private UnitsModel army;
    private ResourcesModel cityResources;
    private Dictionary<string, UnitConstantModel> unitConstants;

    void OnEnable()
    {
        cityResources = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources;
        unitConstants = GlobalGOScript.instance.gameManager.unitConstants;

        onGetBarbariansArmy += GotBarbariansArmy;
        StartCoroutine(GetBarbariansArmy());

        citySilver.text = cityResources.silver.ToString();
        cityGold.text = cityResources.gold.ToString();

        dispatchWorker.text = 0.ToString();
        dispatchSpy.text = 0.ToString();
        dispatchSwordsman.text = 0.ToString();
        dispatchSpearman.text = 0.ToString();
        dispatchPikeman.text = 0.ToString();
        dispatchScoutRider.text = 0.ToString();
        dispatchLightCavalry.text = 0.ToString();
        dispatchHeavyCavalry.text = 0.ToString();
        dispatchArcher.text = 0.ToString();
        dispatchArcherRider.text = 0.ToString();
        dispatchHollyman.text = 0.ToString();
        dispatchWagon.text = 0.ToString();
        dispatchTrebuchet.text = 0.ToString();
        dispatchSiegeTower.text = 0.ToString();
        dispatchBatteringRam.text = 0.ToString();
        dispatchBallista.text = 0.ToString();

        xInput.text = 0.ToString();
        yInput.text = 0.ToString();
    }

    void FixedUpdate()
    {
        long value;
        long totalSilver = 0;
        long totalGold = 0;

        citySilver.text = cityResources.silver.ToString();
        cityGold.text = cityResources.gold.ToString();

        if (long.TryParse(dispatchWorker.text, out value))
        {
            totalSilver += value * unitConstants["Worker"].silver_price * 3;
            totalGold += value * unitConstants["Worker"].gold_price * 3;
        }
        if (long.TryParse(dispatchSpy.text, out value))
        {
            totalSilver += value * unitConstants["Spy"].silver_price * 3;
            totalGold += value * unitConstants["Spy"].gold_price * 3;
        }
        if (long.TryParse(dispatchSwordsman.text, out value))
        {
            totalSilver += value * unitConstants["Swards_Man"].silver_price * 3;
            totalGold += value * unitConstants["Swards_Man"].gold_price * 3;
        }
        if (long.TryParse(dispatchSpearman.text, out value))
        {
            totalSilver += value * unitConstants["Spear_Man"].silver_price * 3;
            totalGold += value * unitConstants["Spear_Man"].gold_price * 3;
        }
        if (long.TryParse(dispatchPikeman.text, out value))
        {
            totalSilver += value * unitConstants["Pike_Man"].silver_price * 3;
            totalGold += value * unitConstants["Pike_Man"].gold_price * 3;
        }
        if (long.TryParse(dispatchScoutRider.text, out value))
        {
            totalSilver += value * unitConstants["Scout_Rider"].silver_price * 3;
            totalGold += value * unitConstants["Scout_Rider"].gold_price * 3;
        }
        if (long.TryParse(dispatchLightCavalry.text, out value))
        {
            totalSilver += value * unitConstants["Light_Cavalry"].silver_price * 3;
            totalGold += value * unitConstants["Light_Cavalry"].gold_price * 3;
        }
        if (long.TryParse(dispatchHeavyCavalry.text, out value))
        {
            totalSilver += value * unitConstants["Heavy_Cavalry"].silver_price * 3;
            totalGold += value * unitConstants["Heavy_Cavalry"].gold_price * 3;
        }
        if (long.TryParse(dispatchArcher.text, out value))
        {
            totalSilver += value * unitConstants["Archer"].silver_price * 3;
            totalGold += value * unitConstants["Archer"].gold_price * 3;
        }
        if (long.TryParse(dispatchArcherRider.text, out value))
        {
            totalSilver += value * unitConstants["Archer_Rider"].silver_price * 3;
            totalGold += value * unitConstants["Archer_Rider"].gold_price * 3;
        }
        if (long.TryParse(dispatchHollyman.text, out value))
        {
            totalSilver += value * unitConstants["Holly_Man"].silver_price * 3;
            totalGold += value * unitConstants["Holly_Man"].gold_price * 3;
        }
        if (long.TryParse(dispatchWagon.text, out value))
        {
            totalSilver += value * unitConstants["Wagon"].silver_price * 3;
            totalGold += value * unitConstants["Wagon"].gold_price * 3;
        }
        if (long.TryParse(dispatchTrebuchet.text, out value))
        {
            totalSilver += value * unitConstants["Trebuchet"].silver_price * 3;
            totalGold += value * unitConstants["Trebuchet"].gold_price * 3;
        }
        if (long.TryParse(dispatchSiegeTower.text, out value))
        {
            totalSilver += value * unitConstants["Siege_Towers"].silver_price * 3;
            totalGold += value * unitConstants["Siege_Towers"].gold_price * 3;
        }
        if (long.TryParse(dispatchBatteringRam.text, out value))
        {
            totalSilver += value * unitConstants["Battering_Ram"].silver_price * 3;
            totalGold += value * unitConstants["Battering_Ram"].gold_price * 3;
        }
        if (long.TryParse(dispatchBallista.text, out value))
        {
            totalSilver += value * unitConstants["Ballista"].silver_price * 3;
            totalGold += value * unitConstants["Ballista"].gold_price * 3;
        }

        priceSilver.text = totalSilver.ToString();
        priceGold.text = totalGold.ToString();
    }

    public IEnumerator GetBarbariansArmy()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://" + GlobalGOScript.instance.host + ":8889/army/getBarbariansArmy/" + targetField.id.ToString() + "/");
        request.SetRequestHeader("authorization", "Token " + GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                //failed
                Debug.Log(request.downloadHandler.text);

                if (onGetBarbariansArmy != null)
                {
                    onGetBarbariansArmy(this, "fail");
                }
            }
            else
            {
                //success
                presentArmy = JsonUtility.FromJson<UnitsModel>(request.downloadHandler.text);

                if (onGetBarbariansArmy != null)
                {
                    onGetBarbariansArmy(this, "success");
                }
            }
        }
    }

    public void GotBarbariansArmy(object sender, string value)
    {
        onGetBarbariansArmy -= GotBarbariansArmy;

        if (string.Equals(value, "success"))
        {
            cityWorker.text = presentArmy.workers_count.ToString();
            citySpy.text = presentArmy.spy_count.ToString();
            citySwordsman.text = presentArmy.swards_man_count.ToString();
            citySpearman.text = presentArmy.spear_man_count.ToString();
            cityPikeman.text = presentArmy.pike_man_count.ToString();
            cityScoutRider.text = presentArmy.scout_rider_count.ToString();
            cityLightCavalry.text = presentArmy.light_cavalry_count.ToString();
            cityHeavyCavalry.text = presentArmy.heavy_cavalry_count.ToString();
            cityArcher.text = presentArmy.archer_count.ToString();
            cityArcherRider.text = presentArmy.archer_rider_count.ToString();
            cityHollyman.text = presentArmy.holly_man_count.ToString();
            cityWagon.text = presentArmy.wagon_count.ToString();
            cityTrebuchet.text = presentArmy.trebuchet_count.ToString();
            citySiegeTower.text = presentArmy.siege_towers_count.ToString();
            cityBatteringRam.text = presentArmy.battering_ram_count.ToString();
            cityBallista.text = presentArmy.ballista_count.ToString();
        }
    }

    public void March()
    {
        long value;
        army = new UnitsModel();
        if (long.TryParse(dispatchWorker.text, out value))
        {
            army.workers_count = value;
        }
        if (long.TryParse(dispatchSpy.text, out value))
        {
            army.spy_count = value;
        }
        if (long.TryParse(dispatchSwordsman.text, out value))
        {
            army.swards_man_count = value;
        }
        if (long.TryParse(dispatchSpearman.text, out value))
        {
            army.spear_man_count = value;
        }
        if (long.TryParse(dispatchPikeman.text, out value))
        {
            army.pike_man_count = value;
        }
        if (long.TryParse(dispatchScoutRider.text, out value))
        {
            army.scout_rider_count = value;
        }
        if (long.TryParse(dispatchLightCavalry.text, out value))
        {
            army.light_cavalry_count = value;
        }
        if (long.TryParse(dispatchHeavyCavalry.text, out value))
        {
            army.heavy_cavalry_count = value;
        }
        if (long.TryParse(dispatchArcher.text, out value))
        {
            army.archer_count = value;
        }
        if (long.TryParse(dispatchArcherRider.text, out value))
        {
            army.archer_rider_count = value;
        }
        if (long.TryParse(dispatchHollyman.text, out value))
        {
            army.holly_man_count = value;
        }
        if (long.TryParse(dispatchWagon.text, out value))
        {
            army.wagon_count = value;
        }
        if (long.TryParse(dispatchTrebuchet.text, out value))
        {
            army.trebuchet_count = value;
        }
        if (long.TryParse(dispatchSiegeTower.text, out value))
        {
            army.siege_towers_count = value;
        }
        if (long.TryParse(dispatchBatteringRam.text, out value))
        {
            army.battering_ram_count = value;
        }
        if (long.TryParse(dispatchBallista.text, out value))
        {
            army.ballista_count = value;
        }
        army.city = GlobalGOScript.instance.gameManager.cityManager.currentCity.id;

        GlobalGOScript.instance.gameManager.battleManager.onMarchLaunched += MarchLaunched;

        StartCoroutine(GlobalGOScript.instance.gameManager.battleManager.RecruitBarbarians(army, long.Parse(xInput.text), long.Parse(yInput.text), targetField.id));
    }

    public void MarchLaunched(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.battleManager.onMarchLaunched -= MarchLaunched;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.closeRecruitBarbarians();
        }
    }
}
