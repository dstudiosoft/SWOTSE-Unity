﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class CityChangerContentManager : MonoBehaviour {

    public GameObject cityItemPrefab;

    void Start()
    {
        if (GlobalGOScript.instance.gameManager != null)
        {
            GlobalGOScript.instance.gameManager.cityManager.cityChangerUpdateImage += UpdateCityImage;
        }
    }

	void OnEnable () {

        if (GlobalGOScript.instance.gameManager != null)
        {
            CityManager manager = GlobalGOScript.instance.gameManager.cityManager;
            // instantiate capital game always starts from capital
            GameObject prefab = GameObject.Instantiate(cityItemPrefab);
            CityItemLine cityLine = prefab.GetComponent<CityItemLine>();
            cityLine.SetCityId(manager.currentCity.id);
            cityLine.SetCityImage(manager.currentCity.image_name);
            cityLine.SetCoords(manager.currentCity.posX, manager.currentCity.posY);
            cityLine.SetCurrent(true);
            
            prefab.transform.SetParent(gameObject.transform, false);

            foreach (MyCityModel city in manager.myCities.Values)
            {
                if (city.id != manager.currentCity.id)
                {
                    prefab = GameObject.Instantiate(cityItemPrefab);
                    cityLine = prefab.GetComponent<CityItemLine>();
                    cityLine.SetCityId(city.id);
                    cityLine.SetCityImage(city.image_name);
                    cityLine.SetCoords(city.posX, city.posY);
                    cityLine.SetCurrent(false);
                    prefab.transform.SetParent(gameObject.transform, false);
                }
            }
        }
	}

    void OnDisable()
    {
        if (transform.childCount > 0)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                GameObject.Destroy(transform.GetChild(i).gameObject);
            }
        }
    }

    public void UpdateCityImage(object sender, string imageName)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            CityItemLine line = transform.GetChild(i).GetComponent<CityItemLine>();

            if (line.cityId == GlobalGOScript.instance.gameManager.cityManager.currentCity.id)
            {
                line.SetCityImage(imageName);
            }
        }
    }

    public void UpdateCityCoords(long x, long y)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            CityItemLine line = transform.GetChild(i).GetComponent<CityItemLine>();

            if (line.cityId == GlobalGOScript.instance.gameManager.cityManager.currentCity.id)
            {
                line.SetCoords(x, y);
            }
        }
    }

    public IEnumerator ChangeCity (long cityId)
    {
        LoginManager loginManager = GlobalGOScript.instance.playerManager.loginManager;

        loginManager.gotCityResources = false;
        loginManager.gotCityBuildings = false;
        loginManager.gotCityFields = false;
        loginManager.gotCityValleys = false;
        loginManager.gotCityUnits = false;
        loginManager.gotCityGenerals = false;
        loginManager.gotCityTreasures = false;
        GlobalGOScript.instance.gameManager.cityManager.resourceManager.resourcesWorking = false;
        GlobalGOScript.instance.playerManager.reportManager.buildingTimers = null;
        GlobalGOScript.instance.playerManager.reportManager.unitTimers = null;
        GlobalGOScript.instance.playerManager.reportManager.armyTimers = null;
        GlobalGOScript.instance.playerManager.reportManager.itemTimers = null;

        for (int i = 0; i < transform.childCount; i++)
        {
            CityItemLine line = transform.GetChild(i).GetComponent<CityItemLine>();

            if (line.currentCity)
            {
                line.SetCurrent(false);
            }
        }

        GlobalGOScript.instance.gameManager.cityManager.currentCity = GlobalGOScript.instance.gameManager.cityManager.myCities[cityId];
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.resourceManager.GetInitialReourcesForCity());
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.GetInitialCityBuildings());
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.GetInitialCityFields());
		StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.GetInitialCityValleys(true));
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.GetInitialCityUnits());
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.GetInitialCityGenerals());
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.GetCityTreasures(true));
        StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetBuildingTimers(true));
        StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetUnitTimers(true));
        StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetArmyTimers(true));

        while (!loginManager.gotCityResources || !loginManager.gotCityBuildings || !loginManager.gotCityFields || !loginManager.gotCityValleys || !loginManager.gotCityUnits || !loginManager.gotCityGenerals || !loginManager.gotCityTreasures)
        {
            yield return new WaitForSeconds(1.0f);
        }

        while (GlobalGOScript.instance.gameManager.cityManager.resourceManager.resourceCoroutine != null)
        {
            yield return new WaitForSeconds(0.3f);
        }

        GlobalGOScript.instance.gameManager.cityManager.resourceManager.StartResources();

        for (int i = 0; i < transform.childCount; i++)
        {
            CityItemLine line = transform.GetChild(i).GetComponent<CityItemLine>();

            if (line.cityId == GlobalGOScript.instance.gameManager.cityManager.currentCity.id)
            {
                line.SetCurrent(true);
            }
        }

        if (SceneManager.GetActiveScene().buildIndex == 2 || SceneManager.GetActiveScene().buildIndex == 3)
        {
            GameObject world = GameObject.Find("GameWorld");
            world.SetActive(false);
            world.SetActive(true);
            gameObject.SetActive(false);
        }
        else if (SceneManager.GetActiveScene().buildIndex == 4)
        {
            GameObject world = GameObject.Find("GameWorld");
            TestWorldChunksManager worldManager = world.GetComponent<TestWorldChunksManager>();
            worldManager.GoToCoords(GlobalGOScript.instance.gameManager.cityManager.currentCity.posX, GlobalGOScript.instance.gameManager.cityManager.currentCity.posY);
            gameObject.SetActive(false);
        }
    }

    public void BuildNewCity(long x, long y, string name)
    {
        //GlobalGOScript.instance.gameManager.cityManager.onCityBuilt += NewCityHandler;

        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.BuildCity(x, y, name));
    }

    public void NewCityHandler(object sender, string value)
    {
        //GlobalGOScript.instance.gameManager.cityManager.onCityBuilt -= NewCityHandler;

        if (string.Equals(value, "success"))
        {
            if (gameObject.activeInHierarchy)
            {
                GlobalGOScript.instance.gameManager.cityManager.onGotNewCity += UpdateViews;
                StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.GetInitialCities());
            }
        }
    }

    public void UpdateViews(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.onGotNewCity -= UpdateViews;

        //update city changer
        IList<long> changerIDs = new List<long>();

        for (int i = 0; i < transform.childCount; i++)
        {
            CityItemLine item = transform.GetChild(i).GetComponent<CityItemLine>();
            changerIDs.Add(item.cityId);
        }

        CityManager manager = GlobalGOScript.instance.gameManager.cityManager;

        foreach (long id in manager.myCities.Keys)
        {
            if (!changerIDs.Contains(id))
            {
                GameObject prefab = GameObject.Instantiate(cityItemPrefab);
                CityItemLine cityLine = prefab.GetComponent<CityItemLine>();
                cityLine.SetCityId(id);
                cityLine.SetCityImage(manager.myCities[id].image_name);
                cityLine.SetCurrent(false);
                cityLine.SetCoords(manager.myCities[id].posX, manager.myCities[id].posY);
                prefab.transform.SetParent(gameObject.transform, false);
            }
        }

        //update map
        if (SceneManager.GetActiveScene().buildIndex == 4)
        {
            GameObject world = GameObject.Find("GameWorld");
            TestWorldChunksManager worldManager = world.GetComponent<TestWorldChunksManager>();
			worldManager.UpdateWorldMap();
        }
    }

	public void UpdateWorldMap(object sender, string value)
	{
		GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onGotValleys -= UpdateWorldMap;
		GlobalGOScript.instance.playerManager.onGetMyUserInfo -= UpdateWorldMap;

		if (string.Equals (value, "success")) 
		{
			if (SceneManager.GetActiveScene().buildIndex == 4)
			{
				GameObject world = GameObject.Find("GameWorld");
				TestWorldChunksManager worldManager = world.GetComponent<TestWorldChunksManager>();
				worldManager.UpdateWorldMap();
			}
		}
	}
}
