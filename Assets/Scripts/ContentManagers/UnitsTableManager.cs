﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UnitsTableManager : MonoBehaviour
{
    public Text cityWorker;
    public Text citySpy;
    public Text citySwordsman;
    public Text citySpearman;
    public Text cityPikeman;
    public Text cityScoutRider;
    public Text cityLightCavalry;
    public Text cityHeavyCavalry;
    public Text cityArcher;
    public Text cityArcherRider;
    public Text cityHollyman;
    public Text cityWagon;
    public Text cityTrebuchet;
    public Text citySiegeTower;
    public Text cityBatteringRam;
    public Text cityBallista;
    public Text cityBeaconTower;
    public Text cityArcherTower;
    public Text cityHotOilHole;
    public Text cityTrebuchetTower;
    public Text cityBallistaTower;

    void OnEnable()
    {
        if (GlobalGOScript.instance == null)
            return;
        if (cityWorker != null) cityWorker.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count);
        if (citySpy != null) citySpy.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spy_count);
        if (citySwordsman != null) citySwordsman.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.swards_man_count);
        if (citySpearman != null) citySpearman.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spear_man_count);
        if (cityPikeman != null) cityPikeman.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.pike_man_count);
        if (cityScoutRider != null) cityScoutRider.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.scout_rider_count);
        if (cityLightCavalry != null) cityLightCavalry.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.light_cavalry_count);
        if (cityHeavyCavalry != null) cityHeavyCavalry.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.heavy_cavalry_count);
        if (cityArcher != null) cityArcher.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_count);
        if (cityArcherRider != null) cityArcherRider.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_rider_count);
        if (cityHollyman != null) cityHollyman.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.holly_man_count);
        if (cityWagon != null) cityWagon.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.wagon_count);
        if (cityTrebuchet != null) cityTrebuchet.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.trebuchet_count);
        if (citySiegeTower != null) citySiegeTower.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.siege_towers_count);
        if (cityBatteringRam != null) cityBatteringRam.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.battering_ram_count);
        if (cityBallista != null) cityBallista.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.ballista_count);
    }

    void FixedUpdate()
    {
        if (cityWorker != null) cityWorker.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count);
        if (citySpy != null) citySpy.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spy_count);
        if (citySwordsman != null) citySwordsman.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.swards_man_count);
        if (citySpearman != null) citySpearman.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spear_man_count);
        if (cityPikeman != null) cityPikeman.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.pike_man_count);
        if (cityScoutRider != null) cityScoutRider.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.scout_rider_count);
        if (cityLightCavalry != null) cityLightCavalry.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.light_cavalry_count);
        if (cityHeavyCavalry != null) cityHeavyCavalry.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.heavy_cavalry_count);
        if (cityArcher != null) cityArcher.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_count);
        if (cityArcherRider != null) cityArcherRider.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_rider_count);
        if (cityHollyman != null) cityHollyman.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.holly_man_count);
        if (cityWagon != null) cityWagon.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.wagon_count);
        if (cityTrebuchet != null) cityTrebuchet.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.trebuchet_count);
        if (citySiegeTower != null) citySiegeTower.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.siege_towers_count);
        if (cityBatteringRam != null) cityBatteringRam.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.battering_ram_count);
        if (cityBallista != null) cityBallista.text = Format(GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.ballista_count);
    }

    private string Format(long value)
    {
        if (value > 999999)
            return (value / 1000000).ToString() + "m";
        if (value > 999)
            return (value / 1000).ToString() + "k";
        return value.ToString();
    }
}
