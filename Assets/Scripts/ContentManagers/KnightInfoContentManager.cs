﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class KnightInfoContentManager : MonoBehaviour {

    public Text knightName;
    public Text knightPoint;
    public Text knightMarches;
    public Text knightPolitics;
    public Text knightSpeed;
    public Text knightLevel;
    public Text knightLoyalty;
    public Text knightSalary;

    public ArmyGeneralModel general;

    private long increaseMarches = 0;
    private long increasePolitics = 0;
    private long increaseSpeed = 0;
    private long increaseSalary = 0;
    private long increaseLoyalty = 0;
    private long increaseLevel = 0;
    private long decreaseExperience = 0;

    void OnEnable()
    {
        knightName.text = general.name;
        knightPoint.text = general.experience.ToString();
        knightMarches.text = general.marches.ToString();
        knightPolitics.text = general.politics.ToString();
        knightSpeed.text = general.speed.ToString();
        knightLevel.text = general.level.ToString();
        knightLoyalty.text = general.loyalty.ToString();
        knightSalary.text = general.salary.ToString();
    }

    public void IncreaseMarches()
    {
        if (general.experience >= 5000)
        {
            increaseMarches += 1;
            knightMarches.text = (general.marches + increaseMarches).ToString();
            increaseLevel += 1;
            decreaseExperience += 5000;
            knightLevel.text = (general.level + increaseLevel).ToString();
            knightPoint.text = (general.experience - decreaseExperience).ToString();
        }
    }

    public void IncreasePolitics()
    {
        if (general.experience >= 5000)
        {
            increasePolitics += 1;
            knightPolitics.text = (general.politics + increasePolitics).ToString();
            increaseLevel += 1;
            decreaseExperience += 5000;
            knightLevel.text = (general.level + increaseLevel).ToString();
            knightPoint.text = (general.experience - decreaseExperience).ToString();
        }
    }

    public void IncreaseSpeed()
    {
        if (general.experience >= 5000)
        {
            increaseSpeed += 1;
            knightSpeed.text = (general.speed + increaseSpeed).ToString();
            increaseLevel += 1;
            decreaseExperience += 5000;
            knightLevel.text = (general.level + increaseLevel).ToString();
            knightPoint.text = (general.experience - decreaseExperience).ToString();
        }
    }

    public void IncreaseSalary()
    {
        if (general.experience >= 5000)
        {
            increaseSalary += 1;
            knightSalary.text = (general.salary + increaseSalary).ToString();
            increaseLevel += 1;
            decreaseExperience += 5000;
            knightLevel.text = (general.level + increaseLevel).ToString();
            knightPoint.text = (general.experience - decreaseExperience).ToString();
        }
    }

    public void IncreaseLoyalty()
    {
        if (general.experience >= 5000)
        {
            increaseLoyalty += 1;
            knightLoyalty.text = (general.loyalty + increaseLoyalty).ToString();
            increaseLevel += 1;
            decreaseExperience += 5000;
            knightLevel.text = (general.level + increaseLevel).ToString();
            knightPoint.text = (general.experience - decreaseExperience).ToString();
        }
    }

    public void Save()
    {
        GlobalGOScript.instance.gameManager.cityManager.unitsManager.onGeneralUpgraded += KnightUpgraded;
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.UpgradeGeneral(general.id, increaseMarches, increasePolitics, increaseSpeed, increaseSalary, increaseLoyalty));
    }

    public void KnightUpgraded(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.unitsManager.onGeneralUpgraded -= KnightUpgraded;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.openNotification("Upgrade successful");
            general.marches += increaseMarches;
            general.politics += increasePolitics;
            general.speed += increaseSpeed;
            general.loyalty += increaseLoyalty;
            general.salary += increaseSalary;
            general.level += increaseLevel;
            general.experience -= decreaseExperience;
            increaseMarches = 0;
            increasePolitics = 0;
            increaseSpeed = 0;
            increaseSalary = 0;
            increaseLoyalty = 0;
            increaseLevel = 0;
            decreaseExperience = 0;
        }
        else
        {
            GlobalGOScript.instance.windowInstanceManager.openNotification("Upgrade failed");
            increaseMarches = 0;
            increasePolitics = 0;
            increaseSpeed = 0;
            increaseSalary = 0;
            increaseLoyalty = 0;
            increaseLevel = 0;
            decreaseExperience = 0;
            knightPoint.text = general.experience.ToString();
            knightMarches.text = general.marches.ToString();
            knightPolitics.text = general.politics.ToString();
            knightSpeed.text = general.speed.ToString();
            knightLevel.text = general.level.ToString();
            knightLoyalty.text = general.loyalty.ToString();
            knightSalary.text = general.salary.ToString();
        }
    }
}
