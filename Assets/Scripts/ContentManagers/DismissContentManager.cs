﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DismissContentManager : MonoBehaviour {

    public Text cityWorker;
    public Text citySpy;
    public Text citySwordsman;
    public Text citySpearman;
    public Text cityPikeman;
    public Text cityScoutRider;
    public Text cityLightCavalry;
    public Text cityHeavyCavalry;
    public Text cityArcher;
    public Text cityArcherRider;
    public Text cityHollyman;
    public Text cityWagon;
    public Text cityTrebuchet;
    public Text citySiegeTower;
    public Text cityBatteringRam;
    public Text cityBallista;

    public InputField dismissWorker;
    public InputField dismissSpy;
    public InputField dismissSwordsman;
    public InputField dismissSpearman;
    public InputField dismissPikeman;
    public InputField dismissScoutRider;
    public InputField dismissLightCavalry;
    public InputField dismissHeavyCavalry;
    public InputField dismissArcher;
    public InputField dismissArcherRider;
    public InputField dismissHollyman;
    public InputField dismissWagon;
    public InputField dismissTrebuchet;
    public InputField dismissSiegeTower;
    public InputField dismissBatteringRam;
    public InputField dismissBallista;

    private UnitsModel units;

    void OnEnable()
    {
        UnitsModel cityUnits = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits;

        cityWorker.text = cityUnits.workers_count.ToString();
        citySpy.text = cityUnits.spy_count.ToString();
        citySwordsman.text = cityUnits.swards_man_count.ToString();
        citySpearman.text = cityUnits.spear_man_count.ToString();
        cityPikeman.text = cityUnits.pike_man_count.ToString();
        cityScoutRider.text = cityUnits.scout_rider_count.ToString();
        cityLightCavalry.text = cityUnits.light_cavalry_count.ToString();
        cityHeavyCavalry.text = cityUnits.heavy_cavalry_count.ToString();
        cityArcher.text = cityUnits.archer_count.ToString();
        cityArcherRider.text = cityUnits.archer_rider_count.ToString();
        cityHollyman.text = cityUnits.holly_man_count.ToString();
        cityWagon.text = cityUnits.wagon_count.ToString();
        cityTrebuchet.text = cityUnits.trebuchet_count.ToString();
        citySiegeTower.text = cityUnits.siege_towers_count.ToString();
        cityBatteringRam.text = cityUnits.battering_ram_count.ToString();
        cityBallista.text = cityUnits.ballista_count.ToString();

        dismissWorker.text = 0.ToString();
        dismissSpy.text = 0.ToString();
        dismissSwordsman.text = 0.ToString();
        dismissSpearman.text = 0.ToString();
        dismissPikeman.text = 0.ToString();
        dismissScoutRider.text = 0.ToString();
        dismissLightCavalry.text = 0.ToString();
        dismissHeavyCavalry.text = 0.ToString();
        dismissArcher.text = 0.ToString();
        dismissArcherRider.text = 0.ToString();
        dismissHollyman.text = 0.ToString();
        dismissWagon.text = 0.ToString();
        dismissTrebuchet.text = 0.ToString();
        dismissSiegeTower.text = 0.ToString();
        dismissBatteringRam.text = 0.ToString();
        dismissBallista.text = 0.ToString();
    }

    void FixedUpdate()
    {
        UnitsModel cityUnits = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits;

        cityWorker.text = cityUnits.workers_count.ToString();
        citySpy.text = cityUnits.spy_count.ToString();
        citySwordsman.text = cityUnits.swards_man_count.ToString();
        citySpearman.text = cityUnits.spear_man_count.ToString();
        cityPikeman.text = cityUnits.pike_man_count.ToString();
        cityScoutRider.text = cityUnits.scout_rider_count.ToString();
        cityLightCavalry.text = cityUnits.light_cavalry_count.ToString();
        cityHeavyCavalry.text = cityUnits.heavy_cavalry_count.ToString();
        cityArcher.text = cityUnits.archer_count.ToString();
        cityArcherRider.text = cityUnits.archer_rider_count.ToString();
        cityHollyman.text = cityUnits.holly_man_count.ToString();
        cityWagon.text = cityUnits.wagon_count.ToString();
        cityTrebuchet.text = cityUnits.trebuchet_count.ToString();
        citySiegeTower.text = cityUnits.siege_towers_count.ToString();
        cityBatteringRam.text = cityUnits.battering_ram_count.ToString();
        cityBallista.text = cityUnits.ballista_count.ToString();
    }

    public void Dismiss()
    {
        long value;
        units = new UnitsModel();
        if (long.TryParse(dismissWorker.text, out value))
        {
            units.workers_count = value;
        }
        if (long.TryParse(dismissSpy.text, out value))
        {
            units.spy_count = value;
        }
        if (long.TryParse(dismissSwordsman.text, out value))
        {
            units.swards_man_count = value;
        }
        if (long.TryParse(dismissSpearman.text, out value))
        {
            units.spear_man_count = value;
        }
        if (long.TryParse(dismissPikeman.text, out value))
        {
            units.pike_man_count = value;
        }
        if (long.TryParse(dismissScoutRider.text, out value))
        {
            units.scout_rider_count = value;
        }
        if (long.TryParse(dismissLightCavalry.text, out value))
        {
            units.light_cavalry_count = value;
        }
        if (long.TryParse(dismissHeavyCavalry.text, out value))
        {
            units.heavy_cavalry_count = value;
        }
        if (long.TryParse(dismissArcher.text, out value))
        {
            units.archer_count = value;
        }
        if (long.TryParse(dismissArcherRider.text, out value))
        {
            units.archer_rider_count = value;
        }
        if (long.TryParse(dismissHollyman.text, out value))
        {
            units.holly_man_count = value;
        }
        if (long.TryParse(dismissWagon.text, out value))
        {
            units.wagon_count = value;
        }
        if (long.TryParse(dismissTrebuchet.text, out value))
        {
            units.trebuchet_count = value;
        }
        if (long.TryParse(dismissSiegeTower.text, out value))
        {
            units.siege_towers_count = value;
        }
        if (long.TryParse(dismissBatteringRam.text, out value))
        {
            units.battering_ram_count = value;
        }
        if (long.TryParse(dismissBallista.text, out value))
        {
            units.ballista_count = value;
        }

        units.city = GlobalGOScript.instance.gameManager.cityManager.currentCity.id;

        GlobalGOScript.instance.gameManager.cityManager.unitsManager.onUnitsDismissed += UnitsDismissed;
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.DismissUnits(units));
    }

    public void UnitsDismissed(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.unitsManager.onUnitsDismissed -= UnitsDismissed;

        if (string.Equals(value, "success"))
        {
            UnitsModel cityunits = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits;
            cityunits.archer_count -= units.archer_count;
            cityunits.archer_rider_count -= units.archer_rider_count;
            cityunits.ballista_count -= units.ballista_count;
            cityunits.battering_ram_count -= units.battering_ram_count;
            cityunits.heavy_cavalry_count -= units.heavy_cavalry_count;
            cityunits.holly_man_count -= units.holly_man_count;
            cityunits.light_cavalry_count -= units.light_cavalry_count;
            cityunits.pike_man_count -= units.pike_man_count;
            cityunits.scout_rider_count -= units.scout_rider_count;
            cityunits.siege_towers_count -= units.siege_towers_count;
            cityunits.spear_man_count -= units.spear_man_count;
            cityunits.spy_count -= units.spy_count;
            cityunits.swards_man_count -= units.swards_man_count;
            cityunits.trebuchet_count -= units.trebuchet_count;
            cityunits.wagon_count -= units.wagon_count;
            cityunits.workers_count -= units.workers_count;

            GlobalGOScript.instance.windowInstanceManager.closeDismiss();
        }
    }
}
