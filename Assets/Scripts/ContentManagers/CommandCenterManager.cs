﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CommandCenterManager : MonoBehaviour {

    public Text cityWorker;
    public Text citySpy;
    public Text citySwordsman;
    public Text citySpearman;
    public Text cityPikeman;
    public Text cityScoutRider;
    public Text cityLightCavalry;
    public Text cityHeavyCavalry;
    public Text cityArcher;
    public Text cityArcherRider;
    public Text cityHollyman;
    public Text cityWagon;
    public Text cityTrebuchet;
    public Text citySiegeTower;
    public Text cityBatteringRam;
    public Text cityBallista;

    void OnEnable()
    {
        cityWorker.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
        citySpy.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spy_count.ToString();
        citySwordsman.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.swards_man_count.ToString();
        citySpearman.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spear_man_count.ToString();
        cityPikeman.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.pike_man_count.ToString();
        cityScoutRider.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.scout_rider_count.ToString();
        cityLightCavalry.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.light_cavalry_count.ToString();
        cityHeavyCavalry.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.heavy_cavalry_count.ToString();
        cityArcher.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_count.ToString();
        cityArcherRider.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_rider_count.ToString();
        cityHollyman.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.holly_man_count.ToString();
        cityWagon.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.wagon_count.ToString();
        cityTrebuchet.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.trebuchet_count.ToString();
        citySiegeTower.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.siege_towers_count.ToString();
        cityBatteringRam.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.battering_ram_count.ToString();
        cityBallista.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.ballista_count.ToString();
    }

    void FixedUpdate()
    {
        cityWorker.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
        citySpy.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spy_count.ToString();
        citySwordsman.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.swards_man_count.ToString();
        citySpearman.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spear_man_count.ToString();
        cityPikeman.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.pike_man_count.ToString();
        cityScoutRider.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.scout_rider_count.ToString();
        cityLightCavalry.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.light_cavalry_count.ToString();
        cityHeavyCavalry.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.heavy_cavalry_count.ToString();
        cityArcher.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_count.ToString();
        cityArcherRider.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_rider_count.ToString();
        cityHollyman.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.holly_man_count.ToString();
        cityWagon.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.wagon_count.ToString();
        cityTrebuchet.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.trebuchet_count.ToString();
        citySiegeTower.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.siege_towers_count.ToString();
        cityBatteringRam.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.battering_ram_count.ToString();
        cityBallista.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.ballista_count.ToString();
    }

    public void OpenCenterDispatch()
    {
//        GlobalGOScript.instance.windowInstanceManager.openDispatch(null, false);
        GlobalGOScript.instance.windowInstanceManager.openDispatchWindow(null, false);
    }
}
