﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;
using ArabicSupport;

public class AllianceContentChanger : MonoBehaviour {

    public GameObject informationSection;

    public GameObject contentSection;
    public Text contentLabel;

    public GameObject membersContent;
    public GameObject reportsContent;
    public GameObject eventsContent;
    public GameObject alliancesContent;
    public GameObject inviteContent;
    public GameObject applicationsContent;
    public GameObject permissionsContent;
    public GameObject guidelinesContent;
    public GameObject messageContent;

    public GameObject[] contents;

    private event ConfirmationEvent confirmed;

    /*public void OnEnable()
    {
        CloseAllContent();
        informationSection.SetActive(true);
        //update alliance info
    }*/

    public void OpenContent(string name)
    {
        CloseAllContent();

        switch (name)
        {
            case "Information":
                informationSection.SetActive(true);
                break;

            case "Members":
                contentSection.SetActive(true);
                contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                membersContent.SetActive(true);
                break;

            case "Reports":
                contentSection.SetActive(true);
                contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                reportsContent.SetActive(true);
                break;

            case "Events":
                contentSection.SetActive(true);
                contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                eventsContent.SetActive(true);
                break;

            case "Alliances":
                contentSection.SetActive(true);
                contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                alliancesContent.SetActive(true);
                break;

            case "Invite":
                contentSection.SetActive(true);
                contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                inviteContent.SetActive(true);
                break;

            case "Applications":
                contentSection.SetActive(true);
                contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                applicationsContent.SetActive(true);
                break;

            case "Permissions":
                contentSection.SetActive(true);
                contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                permissionsContent.SetActive(true);
                break;

            case "Guidelines":
                contentSection.SetActive(true);
                contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                guidelinesContent.SetActive(true);
                break;

            case "Message":
                contentSection.SetActive(true);
                contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                messageContent.SetActive(true);
                break;

            default:
                break;
        }
    }

    private void CloseAllContent()
    {
        informationSection.SetActive(false);
        contentSection.SetActive(false);
        membersContent.SetActive(false);
        reportsContent.SetActive(false);
        eventsContent.SetActive(false);
        alliancesContent.SetActive(false);
        inviteContent.SetActive(false);
        applicationsContent.SetActive(false);
        permissionsContent.SetActive(false);
        guidelinesContent.SetActive(false);
        messageContent.SetActive(false);
    }

    public void ResignRank()
    {
        if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance != null)
        {
            string localizedValue = LanguageManager.Instance.GetTextValue("SWOTSELocalization.ConfirmPostResign");

            if (LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
            {
                localizedValue = ArabicFixer.Fix(localizedValue);
            }

            confirmed += ResignConfirmed;
            GlobalGOScript.instance.windowInstanceManager.openConfirmationWindow(localizedValue, confirmed);
        }
    }

    void ResignConfirmed(bool value)
    {
        confirmed -= ResignConfirmed;

        if (value)
        {
            StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.ResignRank());
        }

        GlobalGOScript.instance.windowInstanceManager.closeConfirmationWindow();
    }

    public void QuitAlliance()
    {
        if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance != null)
        {
            string localizedValue = LanguageManager.Instance.GetTextValue("SWOTSELocalization.ConfirmAllianceQuit");

            if (LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
            {
                localizedValue = ArabicFixer.Fix(localizedValue);
            }

            confirmed += QuitConfirmed;
            GlobalGOScript.instance.windowInstanceManager.openConfirmationWindow(localizedValue, confirmed);
        }
    }

    void QuitConfirmed(bool value)
    {
        confirmed -= QuitConfirmed;

        if (value)
        {
            GlobalGOScript.instance.playerManager.allianceManager.onQuitAlliance += OnQuitAlliance;
            StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.QuitAlliance());
        }

        GlobalGOScript.instance.windowInstanceManager.closeConfirmationWindow();
    }

    public void OnQuitAlliance(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.allianceManager.onQuitAlliance -= OnQuitAlliance;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.closeAlliance();
        }
    }
}
