﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;

public class SettingsContentManager : MonoBehaviour {

    //current user
    public InputField currentUserNick;
    public InputField currentUserEmail;
    public InputField currentUserMobile;
    public InputField currentUserFlag;
    public Image currentUserImage;

    private LanguageChangerContentManager languageChanger;
    private event ConfirmationEvent confirmed;

    public UserModel user;

    void OnEnable()
    {
        user = GlobalGOScript.instance.playerManager.user;
        languageChanger = gameObject.GetComponent<LanguageChangerContentManager>();
        //init my user
        currentUserNick.text = user.username;
        currentUserEmail.text = user.email;
        //            currentUserMobile.text = user. mobile
        currentUserFlag.text = user.flag;   
    }

    public void ConfirmSave()
    {
        confirmed += saveConfirmed;
        GlobalGOScript.instance.windowInstanceManager.openConfirmationWindow("Are you sure you want to save these settings?", confirmed);
    }

    public void saveConfirmed(bool value)
    {
        confirmed -= saveConfirmed;
        if (value)
        {
            SaveUserData();
        }

        GlobalGOScript.instance.windowInstanceManager.closeConfirmationWindow();
    }

    public void SaveUserData()
    {
        //change user data
        if (!string.IsNullOrEmpty(currentUserNick.text) && !string.IsNullOrEmpty(currentUserEmail.text))
        {
            if (!string.Equals(currentUserNick.text, user.username) || !string.Equals(currentUserEmail.text, user.email) || !string.Equals(currentUserFlag.text, user.flag))
            {
                StartCoroutine(GlobalGOScript.instance.playerManager.ChangeUserInfo(currentUserNick.text, currentUserEmail.text, "0", currentUserFlag.text));
            }
        }
        languageChanger.Save();
    }
}