﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Globalization;
using UnityEngine.Networking;

public class MessageContentManager : MonoBehaviour {

    public GameObject readContent;
    public GameObject replyContent;

    //read content
    public Text username;
    public Image userIcon;
    public Text userRank;
    public Text userAlliance;
    public Text userPosition;
    public Text userExperiance;

    public Text messageSubject;
    public Text messageText;
    public Text messageDate;

    //reply content
    public Text initialSubject;
    public InputField initialMessage;
    public InputField replySubject;
    public InputField replyMessage;

    public long messageId;
    public bool reply;
    public UserMessageReportModel message;
    public MailInboxTableController ownerTable;

    private string dateFormat = "yyyy-MM-ddTHH:mm:ss.ffffffZ";
    private event SimpleEvent gotMessageDetails;

	// Use this for initialization
	void OnEnable () 
    {
        gotMessageDetails += OnGetMessageDetails;
        StartCoroutine(GetMesssageDetails());
	}

    public void Reply()
    {
        initialSubject.text = message.subject;
        initialMessage.text = message.message;
        readContent.SetActive(false);
        replyContent.SetActive(true);
    }

    public void SendUserMessage()
    {
        GlobalGOScript.instance.playerManager.reportManager.onUserMessageSent += OnMessageSent;
        if (!string.IsNullOrEmpty(replyMessage.text))
        {
            StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.SendUserMessage(message.fromUser.id, replySubject.text, replyMessage.text));
        }
    }

    public void OnMessageSent(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.reportManager.onUserMessageSent -= OnMessageSent;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.closeMessage();
        }
    }

    public void DeleteMessage()
    {
        ownerTable.DeleteMessage(messageId);
        GlobalGOScript.instance.windowInstanceManager.closeMessage();
    }

    public void AcceptCityDeed()
    {
    }

    public void RejectCityDeed()
    {
    }

    public IEnumerator GetMesssageDetails()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/reports/messageReportsDetails/" + messageId.ToString() + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject messageObject = JSONObject.Create(request.downloadHandler.text);

            message = JsonUtility.FromJson<UserMessageReportModel>(messageObject.Print());

            message.fromUser = JsonUtility.FromJson<UserModel>(messageObject["fromUser"].Print());
            message.fromUser.capital = new MyCityModel();

            message.fromUser.capital.city_name = messageObject["fromUser"]["capital"]["city_name"].str;
            message.fromUser.capital.posX = messageObject["fromUser"]["capital"]["posX"].i;
            message.fromUser.capital.posY = messageObject["fromUser"]["capital"]["posY"].i;

            if (!messageObject["fromUser"]["alliance"].IsNull)
            {
                message.fromUser.alliance = new AllianceModel();
                message.fromUser.alliance.alliance_name = messageObject["fromUser"]["alliance"]["alliance_name"].str;
                message.fromUser.alliance.id = messageObject["fromUser"]["alliance"]["id"].i;
            }

            if (!messageObject["fromUser"]["last_login"].IsNull)
            {
                message.fromUser.last_login = DateTime.ParseExact(messageObject["fromUser"]["last_login"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
            }

            if (!messageObject["creation_date"].IsNull)
            {
                message.creation_date = DateTime.ParseExact(messageObject["creation_date"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
            }

            if (gotMessageDetails != null)
            {
                gotMessageDetails(this, "success");
            }
        }
    }

    void OnGetMessageDetails(object sender, string value)
    {
        gotMessageDetails -= OnGetMessageDetails;

        if (string.Equals(value, "success"))
        {
            if (reply)
            {
                initialSubject.text = message.subject;
                initialMessage.text = message.message;
                replyContent.SetActive(true);
            }
            else
            {
                username.text = message.fromUser.username;
                Sprite userSprite = Resources.Load<Sprite>("UI/UserIcon/" + message.fromUser.image_name);
                userIcon.sprite = userSprite;
                userRank.text = TextLocalizer.LocalizeString(message.fromUser.rank);
                if (message.fromUser.alliance != null)
                {
                    userAlliance.text = message.fromUser.alliance.alliance_name;
                    userPosition.text = TextLocalizer.LocalizeString(message.fromUser.alliance_rank);
                }
                else
                {
                    userAlliance.text = TextLocalizer.LocalizeString("none");
                    userPosition.text = TextLocalizer.LocalizeString("none");
                }
                userExperiance.text = message.fromUser.experience.ToString();
                messageSubject.text = message.subject;
                messageText.text = message.message;
                messageDate.text = message.creation_date.ToString();
                readContent.SetActive(true);
            }

            StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.ReadReportMessage(message.id));
            GlobalGOScript.instance.playerManager.reportManager.inbox[message.id].isRead = true;
        }
    }
}
