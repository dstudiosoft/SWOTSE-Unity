﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;
using ArabicSupport;

public class AllianceInformationContentManager : MonoBehaviour {
    public Text allianceName;
    public Text allianceFounder;
    public Text allianceLeader;
    public Text allianceRank;
    public Text allianceExperiance;
    public Text allianceMembersCount;
    public Text allianceGuideline;

    private event ConfirmationEvent confirmed;

    void OnEnable()
    {
        StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.AllAlliances());
        if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance != null)
        {
            allianceName.text = GlobalGOScript.instance.playerManager.allianceManager.userAlliance.alliance_name;
            allianceFounder.text = GlobalGOScript.instance.playerManager.allianceManager.userAlliance.founderName;
            allianceLeader.text = GlobalGOScript.instance.playerManager.allianceManager.userAlliance.leaderName;
			allianceRank.text = GlobalGOScript.instance.playerManager.allianceManager.userAlliance.rank.ToString();
            allianceExperiance.text = GlobalGOScript.instance.playerManager.allianceManager.userAlliance.alliance_experience.ToString();
            allianceMembersCount.text = GlobalGOScript.instance.playerManager.allianceManager.userAlliance.members_count.ToString();
            allianceGuideline.text = GlobalGOScript.instance.playerManager.allianceManager.userAlliance.guideline;
        }
        else
        {
            allianceName.text = TextLocalizer.LocalizeString("none");
            allianceFounder.text = TextLocalizer.LocalizeString("none");
            allianceLeader.text = TextLocalizer.LocalizeString("none");
            allianceRank.text = TextLocalizer.LocalizeString("none");
            allianceExperiance.text = TextLocalizer.LocalizeString("none");
            allianceMembersCount.text = TextLocalizer.LocalizeString("none");
            allianceGuideline.text = TextLocalizer.LocalizeString("none");
        }
    }

    public void ResignRank()
    {
        if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance != null)
        {
            confirmed += ResignConfirmed;
            GlobalGOScript.instance.windowInstanceManager.openConfirmationWindow(TextLocalizer.LocalizeString("ConfirmPostResign"), confirmed);
        }
    }

    void ResignConfirmed(bool value)
    {
        confirmed -= ResignConfirmed;

        if (value)
        {
            StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.ResignRank());
        }

        GlobalGOScript.instance.windowInstanceManager.closeConfirmationWindow();
    }

    public void QuitAlliance()
    {
        if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance != null)
        {
            confirmed += QuitConfirmed;
            GlobalGOScript.instance.windowInstanceManager.openConfirmationWindow(TextLocalizer.LocalizeString("ConfirmAllianceQuit"), confirmed);
        }
    }

    void QuitConfirmed(bool value)
    {
        confirmed -= QuitConfirmed;

        if (value)
        {
            GlobalGOScript.instance.playerManager.allianceManager.onQuitAlliance += OnQuitAlliance;
            StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.QuitAlliance());
        }

        GlobalGOScript.instance.windowInstanceManager.closeConfirmationWindow();
    }

    public void OnQuitAlliance(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.allianceManager.onQuitAlliance -= OnQuitAlliance;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.closeAlliance();
        }
    }
}
