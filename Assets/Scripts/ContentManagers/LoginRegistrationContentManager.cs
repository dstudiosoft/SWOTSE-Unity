﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LoginRegistrationContentManager : MonoBehaviour {

    public GameObject loginContent;
    public GameObject registrationContent;
    public GameObject forgotPasswordContent;
    public GameObject resetPasswordContent;

    public InputField loginUsername;
    public InputField loginPassword;

    public InputField registerUsername;
    public InputField registerPassword;
    public InputField registerConfirmPassword;
    public InputField registerEmail;
    public Dropdown registerEmperors;
    public Dropdown registerLanguage;
    public InputField forgotPasswordEmail;
    public InputField resetSecretKey;
    public InputField resetNewPassword;

    public GameObject directLoginButton;

    private List<Dropdown.OptionData> emperorsList;
    
    public void Start()
    {
        loginContent.SetActive(true);

        #if !UNITY_EDITOR
        directLoginButton.SetActive(false);
        #endif
		//PlayerPrefs.DeleteKey ("token");
        if (PlayerPrefs.HasKey("token"))
        {
            GlobalGOScript.instance.token = PlayerPrefs.GetString("token");
            GlobalGOScript.instance.playerManager.onGetMyUserInfo += TokenValid;
            StartCoroutine(GlobalGOScript.instance.playerManager.getMyUserInfo(false));
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab) && loginUsername.isFocused)
        {
            EventSystem.current.SetSelectedGameObject(loginPassword.gameObject, null);
        }
    }

    public void TokenValid(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.onGetMyUserInfo -= TokenValid;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.playerManager.loginManager.LoginHandler(null, string.Empty);
        }
        else
        {
            GlobalGOScript.instance.token = string.Empty;
            PlayerPrefs.DeleteKey("token");
        }
    }

    public void LoginContent()
    {
        loginContent.SetActive(true);
        registrationContent.SetActive(false);
        forgotPasswordContent.SetActive(false);
        resetPasswordContent.SetActive(false);
    }

    public void RegisterContent()
    {
        loginContent.SetActive(false);
        registrationContent.SetActive(true);
        forgotPasswordContent.SetActive(false);
        resetPasswordContent.SetActive(false);
    }

    public void ForgotPasswordContent()
    {
        loginContent.SetActive(false);
        registrationContent.SetActive(false);
        forgotPasswordContent.SetActive(true);
        resetPasswordContent.SetActive(false);
    }

    private void GoToReset()
    {
        forgotPasswordContent.SetActive(false);
        resetPasswordContent.SetActive(true);
    }

    public void InitializeEmperorsDropdown(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.loginManager.onGetEmperors -= InitializeEmperorsDropdown;
        List<string> emperorNames = GlobalGOScript.instance.playerManager.loginManager.EmperorNames();
        emperorsList = new List<Dropdown.OptionData>();
        emperorNames.ForEach(AddEmperorsOption);
        registerEmperors.options = emperorsList;
    }

    public void RegistrationHandler(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.loginManager.onRegister -= RegistrationHandler;
        if (string.IsNullOrEmpty(value))
        {
            //registration successful
            GlobalGOScript.instance.playerManager.loginManager.onLogin += GlobalGOScript.instance.playerManager.loginManager.LoginHandler;
            StartCoroutine(GlobalGOScript.instance.playerManager.loginManager.LoginRequest(registerUsername.text, registerPassword.text));
        }
        else
        {
            GlobalGOScript.instance.windowInstanceManager.closeLoginProgressbar();
        }
    }

    public void Register()
    {
        //add validation
        if (string.Equals(registerPassword.text, registerConfirmPassword.text))
        {
            GlobalGOScript.instance.windowInstanceManager.openLoginProgressbar();
            GlobalGOScript.instance.playerManager.loginManager.onRegister += RegistrationHandler;
            StartCoroutine(GlobalGOScript.instance.playerManager.loginManager.RegisterRequest(registerUsername.text, registerPassword.text, registerEmail.text, registerEmperors.captionText.text, registerLanguage.captionText.text.ToLower()));
        }
    }

    public void Login()
    {
        if (string.IsNullOrEmpty(loginUsername.text))
        {
            //no username
        }
        else if (string.IsNullOrEmpty(loginPassword.text))
        {
            //no password
        }
        else
        {
            GlobalGOScript.instance.playerManager.loginManager.onLogin += GlobalGOScript.instance.playerManager.loginManager.LoginHandler;
            StartCoroutine(GlobalGOScript.instance.playerManager.loginManager.LoginRequest(loginUsername.text, loginPassword.text));
        }
    }

    public void SendForgotPassword()
    {
        if (!string.IsNullOrEmpty(forgotPasswordEmail.text))
        {
            //GlobalGOScript.instance.playerManager.onResetPasswordRequested += ForgotPasswordSent;
			StartCoroutine(GlobalGOScript.instance.playerManager.loginManager.ForgotPassword(forgotPasswordEmail.text));
        }
    }

    public void SendResetassword()
    {
        if (!string.IsNullOrEmpty(resetSecretKey.text) && !string.IsNullOrEmpty(resetNewPassword.text))
        {
            GlobalGOScript.instance.playerManager.onPasswordReset += PasswordReset;
            StartCoroutine(GlobalGOScript.instance.playerManager.ResetPassword(forgotPasswordEmail.text, resetSecretKey.text, resetNewPassword.text));
        }
    }

    public void ForgotPasswordSent(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.onResetPasswordRequested -= ForgotPasswordSent;

        if (string.Equals(value, "success"))
        {
            //GoToReset();
            LoginContent();
        }
    }

    public void PasswordReset(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.onPasswordReset -= PasswordReset;

        if (string.Equals(value, "success"))
        {
            LoginContent();
        }
    }

    public void EndEdit()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            Login();
        }
    }

    private void AddEmperorsOption(string name)
    {
        Dropdown.OptionData option = new Dropdown.OptionData(name);
        emperorsList.Add(option);
    }

    //temporary
    public void DirectLogin()
    {
        GlobalGOScript.instance.playerManager.loginManager.LoginHandler(this, null);
    }
}
