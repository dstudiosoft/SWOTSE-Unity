﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MayorResidenceOverviewContentManager : MonoBehaviour {

    public Text cityName;
    public Text cityRegion;
    public Text cityCarrage;
    public Text cityHappiness;
    public Text cityPopulation;
    public Text cityIdlePopulation;
    public Text cityTax;
    public Text playerWins;
    public Text barbarianWins;
    public Text playerLoses;
    public Text barbarianLoses;

    public Dropdown taxDropdown;
    private long taxIndex;

    public InputField cityNameInput;
    public Image cityIcon;
    private long iconIndex;

    private string userImageName;

    // Use this for initialization
    void OnEnable () 
    {
//        cityName.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.city_name;
        cityRegion.text = TextLocalizer.LocalizeString(GlobalGOScript.instance.gameManager.cityManager.currentCity.region);
        cityCarrage.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.courage.ToString();
        cityHappiness.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.happiness.ToString();
        cityPopulation.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.max_population.ToString();
        cityIdlePopulation.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.population.ToString();
        cityTax.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.gold_tax_rate.ToString();
        playerWins.text = GlobalGOScript.instance.playerManager.user.players_wins.ToString();
        barbarianWins.text = GlobalGOScript.instance.playerManager.user.barbarians_wins.ToString();
        playerLoses.text = GlobalGOScript.instance.playerManager.user.players_losses.ToString();
        barbarianLoses.text = GlobalGOScript.instance.playerManager.user.barbarians_losses.ToString();

        taxIndex = (GlobalGOScript.instance.gameManager.cityManager.currentCity.gold_tax_rate / 10) - 1;
        taxDropdown.value = (int)taxIndex;

        iconIndex = long.Parse(GlobalGOScript.instance.gameManager.cityManager.currentCity.image_name);
        Sprite citySprite = Resources.Load<Sprite>("UI/CityIcon/" + GlobalGOScript.instance.gameManager.cityManager.currentCity.image_name);
        cityIcon.sprite = citySprite;
        cityNameInput.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.city_name;
    }

    void FixedUpdate () 
    {
//        cityName.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.city_name;
        cityRegion.text = TextLocalizer.LocalizeString(GlobalGOScript.instance.gameManager.cityManager.currentCity.region);
        cityCarrage.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.courage.ToString();
        cityHappiness.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.happiness.ToString();
        cityPopulation.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.max_population.ToString();
        cityIdlePopulation.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.population.ToString();
        cityTax.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.gold_tax_rate.ToString();
        playerWins.text = GlobalGOScript.instance.playerManager.user.players_wins.ToString();
        barbarianWins.text = GlobalGOScript.instance.playerManager.user.barbarians_wins.ToString();
        playerLoses.text = GlobalGOScript.instance.playerManager.user.players_losses.ToString();
        barbarianLoses.text = GlobalGOScript.instance.playerManager.user.barbarians_losses.ToString();

    }

    #region - Tax

    public void UpdateTax()
    {
        if ((int)taxIndex != taxDropdown.value)
        {
            GlobalGOScript.instance.gameManager.cityManager.onTaxUpdated += TaxUpdated;
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.UpdateCityTax((taxDropdown.value + 1) * 10));
        }
    }

    public void TaxUpdated(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.onTaxUpdated -= TaxUpdated;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.gameManager.cityManager.currentCity.gold_tax_rate = (taxDropdown.value + 1) * 10;
            taxIndex = taxDropdown.value;
        }
        else
        {
            taxDropdown.value = (int)taxIndex;
        }
    } 

    #endregion

    public void SwitchIcon(bool forward)
    {
        Sprite citySprite;

        if (forward)
        {
            if (iconIndex == 19)
            {
                iconIndex = 1;
            }
            else
            {
                iconIndex++;
            }
        }
        else
        {
            if (iconIndex == 1)
            {
                iconIndex = 19;
            }
            else
            {
                iconIndex--;
            }
        }

        citySprite = Resources.Load<Sprite>("UI/CityIcon/" + iconIndex.ToString());

        cityIcon.sprite = citySprite;
    }

    public void ChangeNameAndIcon()
    {
        if (!string.IsNullOrEmpty(cityNameInput.text))
        {
            GlobalGOScript.instance.gameManager.cityManager.onCityNameAndIconChanged += NameAndIconChanged;
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.ChangeNameAndImage(cityNameInput.text, iconIndex.ToString()));
        }
    }

    public void NameAndIconChanged(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.onCityNameAndIconChanged -= NameAndIconChanged;
        if (string.Equals(value, "success"))
        {
            //cityName.text = cityNameInput.text.ToUpper();
        }
    }
}
