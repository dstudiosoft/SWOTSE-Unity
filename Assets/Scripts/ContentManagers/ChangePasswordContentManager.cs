﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChangePasswordContentManager : MonoBehaviour {

    public InputField oldPassword;
    public InputField newPassword;
    public InputField confirmPassword;

    public void ChangePassword()
    {
        if (string.Equals(newPassword.text, confirmPassword.text))
        {
            GlobalGOScript.instance.playerManager.onPasswordChanged += PasswordChanged;
            StartCoroutine(GlobalGOScript.instance.playerManager.ChangeUserPassword(oldPassword.text, newPassword.text));
        }
        else
        {
            GlobalGOScript.instance.windowInstanceManager.openNotification("Confirmation does not match the password");
        }
    }

    public void PasswordChanged(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.onPasswordChanged -= PasswordChanged;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.openNotification("Password changed successfully");
            GlobalGOScript.instance.windowInstanceManager.closeChangePassword();
        }
        else
        {
            GlobalGOScript.instance.windowInstanceManager.openNotification("Password change failed");
        }
    }
}
