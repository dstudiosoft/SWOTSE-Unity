﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;
using ArabicSupport;

public class MailContentChange : MonoBehaviour {

    public GameObject inboxPanel;
    public GameObject reportsPanel;
    public GameObject systemPanel;
    public GameObject invitePanel;

    public Text panelName;

    public Image[] tabs;
    public Sprite tabSelectedSprite;
    public Sprite tabUnselectedSprite;

	// Use this for initialization
	void OnEnable () 
    {
        //disablePanels();
        if (string.Equals(LanguageManager.Instance.CurrentlyLoadedCulture.languageCode, "ar"))
        {
            panelName.text = ArabicFixer.Fix(LanguageManager.Instance.GetTextValue("SWOTSELocalization.Inbox")).ToUpper();
        }
        else
        {
            panelName.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization.Inbox").ToUpper();
        }
        inboxPanel.SetActive(true);
	}

    public void openPanel(string name)
    {
        disablePanels(); //will work wrong for default

        foreach (Image tab in tabs)
            tab.sprite = tabUnselectedSprite;
        switch (name)
        {
            case "Inbox":
                panelName.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                inboxPanel.SetActive(true);
                tabs[0].sprite = tabSelectedSprite;
                break;

            case "Reports":
                panelName.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                reportsPanel.SetActive(true);
                tabs[1].sprite = tabSelectedSprite;
                break;

            case "System":
                panelName.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                systemPanel.SetActive(true);
                tabs[2].sprite = tabSelectedSprite;
                break;

            case "Invite":
                panelName.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                invitePanel.SetActive(true);
                tabs[3].sprite = tabSelectedSprite;
                break;

            default:
                break;
        }
    }

    void disablePanels()
    {
        inboxPanel.SetActive(false);
        reportsPanel.SetActive(false);
        systemPanel.SetActive(false);
        invitePanel.SetActive(false);
    }

    public void OpenNewMessage()
    {
        GlobalGOScript.instance.windowInstanceManager.openNewMail();
    }
}
