﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ConfirmationContentManager : MonoBehaviour {

    public ConfirmationEvent confirmed;
    public Text confirmationMessage;
	
    public void Ok()
    {
        confirmed.Invoke(true);
    }

    public void Cancel()
    {
        confirmed.Invoke(false);
    }
}
