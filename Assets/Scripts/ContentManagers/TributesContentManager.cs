﻿using UnityEngine;
using UnityEngine.UI;

public class TributesContentManager : MonoBehaviour {

    public ResourcesModel tributes;

    public Text tributesFood;
    public Text tributesWood;
    public Text tributesIron;
    public Text tributesStone;
    public Text tributesCopper;
    public Text tributesSilver;
    public Text tributesGold;

    void OnEnable () {
        tributesFood.text = tributes.food.ToString();
        tributesWood.text = tributes.wood.ToString();
        tributesIron.text = tributes.iron.ToString();
        tributesStone.text = tributes.stone.ToString();
        tributesCopper.text = tributes.cooper.ToString();
        tributesSilver.text = tributes.silver.ToString();
        tributesGold.text = tributes.gold.ToString();
    }
}
