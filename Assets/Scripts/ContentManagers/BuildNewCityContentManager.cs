﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuildNewCityContentManager : MonoBehaviour {

    public InputField cityName;
    public long posX;
    public long posY;

    //present resources
    public Text foodPresent;
    public Text woodPresent;
    public Text stonePresent;
    public Text ironPresent;
    public Text copperPresent;
    public Text silverPresent;
    public Text goldPresent;

    private ResourcesModel cityResources;
	
    void OnEnable()
    {
        cityResources = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources;
        foodPresent.text = cityResources.food.ToString();
        woodPresent.text = cityResources.wood.ToString();
        stonePresent.text = cityResources.stone.ToString();
        ironPresent.text = cityResources.iron.ToString();
        copperPresent.text = cityResources.cooper.ToString();
        silverPresent.text = cityResources.silver.ToString();
        goldPresent.text = cityResources.gold.ToString();
    }

    void FixedUpdate()
    {
        foodPresent.text = cityResources.food.ToString();
        woodPresent.text = cityResources.wood.ToString();
        stonePresent.text = cityResources.stone.ToString();
        ironPresent.text = cityResources.iron.ToString();
        copperPresent.text = cityResources.cooper.ToString();
        silverPresent.text = cityResources.silver.ToString();
        goldPresent.text = cityResources.gold.ToString();
    }

    public void BuildNewCity()
    {
        if (!string.IsNullOrEmpty(cityName.text))
        {
            GlobalGOScript.instance.gameManager.cityManager.onCityBuilt += NewCityHandler;
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.BuildCity(posX, posY, cityName.text));
        }
    }

    public void NewCityHandler(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.onCityBuilt -= NewCityHandler;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.closeBuildNewCity();
            GlobalGOScript.instance.windowInstanceManager.closeValleyInfo();
        }
    }
}
