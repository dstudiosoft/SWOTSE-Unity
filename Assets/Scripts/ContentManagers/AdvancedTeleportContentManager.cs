﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AdvancedTeleportContentManager : MonoBehaviour {

    public InputField xCoord;
    public InputField yCoord;

    public string item_id;
    public IReloadable owner;

    public void Close()
    {
        GlobalGOScript.instance.windowInstanceManager.closeTeleport();
    }

    public void Teleport()
    {
        if (!string.Equals(xCoord.text, string.Empty) && !string.Equals(yCoord.text, string.Empty))
        {
            long x;
            long y;

            if (!long.TryParse(xCoord.text, out x))
            {
                return;
            }

            if (!long.TryParse(yCoord.text, out y))
            {
                return;
            }

            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += owner.TeleportUsed;
            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += owner.ReloadTable;
            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed += CityTeleported;
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.UseTeleport(item_id, x, y));
        }
    }

    public void CityTeleported(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemUsed -= CityTeleported;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.closeTeleport();
        }
    }
}
