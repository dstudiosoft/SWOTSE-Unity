﻿using UnityEngine;
using UnityEngine.UI;
public class UpdateBuildingContentManager : MonoBehaviour {

    public bool upgrade;
    public string type;
    public long pitId;

    public Text contentLabel;
    public Text description;

    public GameObject resources;
    public GameObject downgradeDescription;

    //needed resources
    public Text foodNeeded;
    public Text woodNeeded;
    public Text stoneNeeded;
    public Text ironNeeded;
    public Text copperNeeded;
    public Text silverNeeded;
    public Text goldNeeded;

    //present resources
    public Text foodPresent;
    public Text woodPresent;
    public Text stonePresent;
    public Text ironPresent;
    public Text copperPresent;
    public Text silverPresent;
    public Text goldPresent;

    private BuildingModel building;
    private WorldFieldModel valley;
    private BuildingConstantModel constant;
    private ResourcesModel cityResources;

	void OnEnable () {

        if (upgrade)
        {
            cityResources = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources;
            foodPresent.text = cityResources.food.ToString();
            woodPresent.text = cityResources.wood.ToString();
            stonePresent.text = cityResources.stone.ToString();
            ironPresent.text = cityResources.iron.ToString();
            copperPresent.text = cityResources.cooper.ToString();
            silverPresent.text = cityResources.silver.ToString();
            goldPresent.text = cityResources.gold.ToString();
        }

        if (string.Equals(type, "Buildings"))
        {
            building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitId];
        }
        else if (string.Equals(type, "Fields"))
        {
            building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pitId];
        }
        else if (string.Equals(type, "World"))
        {
            valley = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityValleys[pitId];
        }

        if (string.Equals(type, "World"))
        {
            switch (valley.cityType)
            {
                case "Forest":
                    constant = GlobalGOScript.instance.gameManager.buildingConstants["Saw_Mill"];
                    break;
                case "Lake":
                    constant = GlobalGOScript.instance.gameManager.buildingConstants["Grain_Mill"];
                    break;
                case "Hill":
                    constant = GlobalGOScript.instance.gameManager.buildingConstants["Quarry"];
                    break;
                case "Mountin_Iron":
                    constant = GlobalGOScript.instance.gameManager.buildingConstants["Iron_Mine"];
                    break;
                case "Mountin_Cooper":
                    constant = GlobalGOScript.instance.gameManager.buildingConstants["Copper_Mine"];
                    break;
                default:
                    break;
            }

            description.text = description.text.Replace("#buildingName#", valley.cityType.Replace('_', ' '));
            description.text = description.text.Replace("#fromLevel#", valley.level.ToString());
        }
        else
        {
            constant = GlobalGOScript.instance.gameManager.buildingConstants[building.building];
            description.text = description.text.Replace("#buildingName#", constant.building_name);
            description.text = description.text.Replace("#fromLevel#", building.level.ToString());
        }


        if (upgrade)
        {
            contentLabel.text = "UPGRADE";
            resources.SetActive(true);
            downgradeDescription.SetActive(false);
            BuildingPrice upgradePrice;
            description.text = description.text.Replace("#action#", "Upgrading");
            if (string.Equals(type, "World"))
            {
                upgradePrice = (BuildingPrice)constant.building_prices[(int)valley.level];
                description.text = description.text.Replace("#toLevel#", (valley.level + 1).ToString());
            }
            else
            {
                upgradePrice = (BuildingPrice)constant.building_prices[(int)building.level];
                description.text = description.text.Replace("#toLevel#", (building.level + 1).ToString());
            }
            foodNeeded.text = upgradePrice.food_price.ToString();
            woodNeeded.text = upgradePrice.wood_price.ToString();
            stoneNeeded.text = upgradePrice.stone_price.ToString();
            ironNeeded.text = upgradePrice.iron_price.ToString();
            copperNeeded.text = upgradePrice.cooper_price.ToString();
            silverNeeded.text = upgradePrice.silver_price.ToString();
            goldNeeded.text = upgradePrice.gold_price.ToString();

            description.text = description.text.Replace("#hours#", (upgradePrice.time_seconds / 3600L).ToString());
            description.text = description.text.Replace("#minutes#", ((upgradePrice.time_seconds % 3600L) / 60L).ToString());
        }
        else
        {
            contentLabel.text = "DOWNGRADE";
            resources.SetActive(false);
            downgradeDescription.SetActive(true);
            description.text = description.text.Replace("#action#", "Downgrading");
            BuildingPrice downgradePrice;
            if (string.Equals(type, "World"))
            {
                description.text = description.text.Replace("#toLevel#", (valley.level - 1).ToString());
                if (valley.level == 1)
                {
                    downgradePrice = (BuildingPrice)constant.building_prices[0];
                }
                else
                {
                    downgradePrice = (BuildingPrice)constant.building_prices[(int)valley.level - 2];
                }
            }
            else
            {
                description.text = description.text.Replace("#toLevel#", (building.level - 1).ToString());
                if (building.level == 1)
                {
                    downgradePrice = (BuildingPrice)constant.building_prices[0];
                }
                else
                {
                    downgradePrice = (BuildingPrice)constant.building_prices[(int)building.level - 2];
                }
            }

            description.text = description.text.Replace("#hours#", (downgradePrice.time_seconds / 3600L).ToString());
            description.text = description.text.Replace("#minutes#", ((downgradePrice.time_seconds % 3600L) / 60L).ToString());
        }


	}
	
	void FixedUpdate () {
        if (upgrade)
        {
            foodPresent.text = cityResources.food.ToString();
            woodPresent.text = cityResources.wood.ToString();
            stonePresent.text = cityResources.stone.ToString();
            ironPresent.text = cityResources.iron.ToString();
            copperPresent.text = cityResources.cooper.ToString();
            silverPresent.text = cityResources.silver.ToString();
            goldPresent.text = cityResources.gold.ToString();
        }
    }

    public void PerformAction()
    {
        if (upgrade)
        {
            if (string.Equals(type, "World"))
            {
                if (valley.level < 10)
                {
                    GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate += ActionPeformed;
                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.UpgradeValley(valley.id));
                }
            }
            else
            {
                if (building.level < 10)
                {
                    GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate += ActionPeformed;
                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.UpdateBuilding(building.city, building.pit_id, "upgrade", type));
                }
            }
        }
        else
        {
            if (string.Equals(type, "World"))
            {
                if (valley.level > 1)
                {
                    GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate += ActionPeformed;
                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.DowngradeValley(valley.id));
                }
            }
            else
            {
                if (building.level > 1)
                {
                    GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate += ActionPeformed;
                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.UpdateBuilding(building.city, building.pit_id, "downgrade", type));
                }
                else
                {
                    if (!string.Equals(building.building, "Mayor_Residence") && !string.Equals(building.building, "Wall"))
                    {
                        GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate += ActionPeformed;
                        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.UpdateBuilding(building.city, building.pit_id, "downgrade", type));
                    }
                }
            }
        }
    }

    public void ActionPeformed(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate -= ActionPeformed;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.closeUpdateBuilding();
        }
    }
}
