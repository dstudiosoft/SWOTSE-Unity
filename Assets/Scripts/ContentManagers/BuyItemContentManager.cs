﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuyItemContentManager : MonoBehaviour {

    public Text itemName;
    public Image itemImage;
    public Text itemsOwned;
    public Text unitPrice;
    public InputField quantityInput;
    public Text totalShillings;

    private string itemId;
    private long unitCost;
    private long itemcount;
    private long toBuyCount = 0;
    private IReloadable owner;
    private bool buy;

    public void SetContent(Sprite image, string name, string count, string unitCost, string id, IReloadable controller, bool buy)
    {
        owner = controller;
        itemImage.sprite = image;
        itemName.text = name;
        itemcount = long.Parse(count);
        itemsOwned.text = count;
        this.unitCost = long.Parse(unitCost);
        unitPrice.text = unitCost;
        itemId = id;
        totalShillings.text = "0";
        this.buy = buy;
    }

    public void ChangeTotalCost()
    {
        if (string.IsNullOrEmpty(quantityInput.text))
        {
            totalShillings.text = "0";
            return;
        }
            
        long count;
        long.TryParse(quantityInput.text, out count);

        if (count <= 0)
        {
            totalShillings.text = "0";
            return;
        }

        long totalPrice = unitCost * count;
        totalShillings.text = totalPrice.ToString();
        toBuyCount = count;
    }

    public void BuyItems()
    {
        if (buy)
        {
            if (toBuyCount > 0)
            {
                GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemBought += ItemsBought;
                GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemBought += owner.ReloadTable;

                ShopTableController cntr = (ShopTableController)owner;

                if (cntr.cityId != -1)
                {
                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.BuyPlayerItem(itemId, toBuyCount));
                }
                else
                {
                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.BuyItem(itemId, toBuyCount));
                }
            }
        }
        else
        {
            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemGifted += ItemsGifted;
            GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemGifted += owner.ReloadTable;

            ShopTableController cntr = (ShopTableController)owner;

            if (cntr.cityId != -1)
            {
                StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.treasureManager.GiftItem(cntr.cityId, itemId, toBuyCount));
            }
        }
    }

    public void ItemsBought(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemBought -= ItemsBought;
        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.playerManager.onGetMyUserInfo += Close;
            StartCoroutine(GlobalGOScript.instance.playerManager.getMyUserInfo(false));
            itemcount += toBuyCount;
            itemsOwned.text = itemcount.ToString();
        }
    }

    public void ItemsGifted(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.treasureManager.onItemGifted -= ItemsGifted;
        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.playerManager.onGetMyUserInfo += Close;
            StartCoroutine(GlobalGOScript.instance.playerManager.getMyUserInfo(false));
            itemcount -= toBuyCount;
            itemsOwned.text = itemcount.ToString();
        }
    }

    public void Close(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.onGetMyUserInfo -= Close;
        GlobalGOScript.instance.windowInstanceManager.closeBuyItem();
    }

    public void Cancel()
    {
        GlobalGOScript.instance.windowInstanceManager.closeBuyItem();
    }
}
