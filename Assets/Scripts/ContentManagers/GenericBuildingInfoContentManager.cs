﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using ArabicSupport;

public class GenericBuildingInfoContentManager : MonoBehaviour {

    public Text buildingName;
    public Text buildingLevel;
    public Text buildingDescription;
    public GameObject demolishBtn;

    private static string type;
    private static long pitId;

    private event ConfirmationEvent confirmed;

    public void SetInitialInfo(string type, long pitId)
    {
        GenericBuildingInfoContentManager.type = type;
        GenericBuildingInfoContentManager.pitId = pitId;
        BuildingModel building = null;
        WorldFieldModel valley = null;

        if (string.Equals(type, "Buildings"))
        {
            building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitId];
        }
        else if (string.Equals(type, "Fields"))
        {
            building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pitId];
        }

        if (string.Equals(type, "World"))
        {
            valley = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityValleys[pitId];
            buildingLevel.text = valley.level.ToString();
            buildingDescription.text = string.Empty;

            if (SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
            {
                buildingName.text = ArabicFixer.Fix(valley.cityType.Replace('_', ' ').ToUpper());
            }
            else
            {
                buildingName.text = valley.cityType.Replace('_', ' ').ToUpper();
            }
        }
        else
        {
            buildingLevel.text = building.level.ToString();

            if (SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
            {
                buildingDescription.alignment = TextAnchor.UpperRight;
                buildingName.text = ArabicFixer.Fix(GlobalGOScript.instance.gameManager.buildingConstants[building.building].building_name.Replace("_", " ").ToUpper());
                StartCoroutine(TextLocalizer.WrapMultiline(buildingDescription, GlobalGOScript.instance.gameManager.buildingConstants[building.building].description));
            }
            else
            {
                buildingName.text = GlobalGOScript.instance.gameManager.buildingConstants[building.building].building_name.Replace("_", " ").ToUpper();
                buildingDescription.text = GlobalGOScript.instance.gameManager.buildingConstants[building.building].description.ToUpper();
            }
        }

        if (building != null)
        {
            if (!string.Equals(building.building, "Mayor_Residence") && !string.Equals(building.building, "Wall"))
            {
                demolishBtn.SetActive(true);
            }
        }
    }

    public void Upgrade()
    {
        BuildingModel building = null;
        WorldFieldModel valley = null;

        if (string.Equals(type, "Buildings"))
        {
            building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitId];
            if (building.level < 10)
            {
                GlobalGOScript.instance.windowInstanceManager.openUpdateBuilding(true, type, pitId);
            }
        }
        else if (string.Equals(type, "Fields"))
        {
            building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pitId];
            if (building.level < 10)
            {
                GlobalGOScript.instance.windowInstanceManager.openUpdateBuilding(true, type, pitId);
            }
        }
        else if (string.Equals(type, "World"))
        {
            valley = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityValleys[pitId];
        }

        if (string.Equals(type, "World"))
        {
            if (valley.level < 10)
            {
                GlobalGOScript.instance.windowInstanceManager.openUpdateBuilding(true, type, pitId);
            }
        }
    }

    public void Downgrade()
    {
        WorldFieldModel valley = null;

        if (string.Equals(type, "Buildings"))
        {
            GlobalGOScript.instance.windowInstanceManager.openUpdateBuilding(false, type, pitId);
        }
        else if (string.Equals(type, "Fields"))
        {
            GlobalGOScript.instance.windowInstanceManager.openUpdateBuilding(false, type, pitId);
        }
        else if (string.Equals(type, "World"))
        {
            valley = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityValleys[pitId];
        }
    }

    public void Demolish()
    {
        confirmed += DemolishConfirmed;
        GlobalGOScript.instance.windowInstanceManager.openConfirmationWindow("Are you sure you want to destroy this building?", confirmed);
    }

    void DemolishConfirmed(bool value)
    {
        confirmed -= DemolishConfirmed;

        if (value)
        {
            BuildingModel building = null;

            if (string.Equals(type, "Buildings"))
            {
                building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitId];
            }
            if (string.Equals(type, "Fields"))
            {
                building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pitId];
            }

            GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate += Demolished;
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.DestroyBuilding(building.id));
        }

        GlobalGOScript.instance.windowInstanceManager.closeConfirmationWindow();
    }

    void Demolished(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate -= Demolished;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.closeWindow();
        }
    }


    public void HandleUpdate(object sender, string value)
    {
        if (string.Equals(value, "success"))
        {
            BuildingModel buildingState = null;

            if (string.Equals(type, "Buildings"))
            {
                if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings.ContainsKey(pitId))
                {
                    buildingState = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitId];
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.closeBuildingInformation();
                    return;
                }
            }
            else if (string.Equals(type, "Fields"))
            {
                if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields.ContainsKey(pitId))
                {
                    buildingState = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pitId];
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.closeBuildingInformation();
                    return;
                }
            }            

            if (buildingState == null)
            {
                GlobalGOScript.instance.windowInstanceManager.closeBuildingInformation();
                return;
            }
            else
            {
                buildingLevel.text = buildingState.level.ToString();
            }
        }
    }
}
