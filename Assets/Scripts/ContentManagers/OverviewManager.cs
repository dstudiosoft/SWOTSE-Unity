﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OverviewManager : MonoBehaviour {

    public GameObject lordPanel;
    public GameObject overviewPanel;

    public Image playerImage;
    public Text playerName;
    public Text playerRank;
    public Text playerAlliance;
    public Text playerExperience;

    public Text cityName;
    public Text cityRegion;
    public Text cityCarrage;
    public Text cityHappiness;
    public Text cityPopulation;
    public Text cityIdlePopulation;
    public Text cityTax;

    private string userImageName;

	// Use this for initialization
	void OnEnable () 
    {
        userImageName = GlobalGOScript.instance.playerManager.user.image_name;
		lordPanel.SetActive (true);
        overviewPanel.SetActive (true);
        Sprite userIcon = Resources.Load<Sprite>("UI/UserIcon/" + GlobalGOScript.instance.playerManager.user.image_name);
        playerImage.sprite = userIcon;
        playerName.text = GlobalGOScript.instance.playerManager.user.username;
        playerRank.text = GlobalGOScript.instance.playerManager.user.rank;
        if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance != null)
        {
            playerAlliance.text = GlobalGOScript.instance.playerManager.allianceManager.userAlliance.alliance_name;
        }
        else
        {
            playerAlliance.text = "None";
        }
        playerExperience.text = GlobalGOScript.instance.playerManager.user.experience.ToString();
        cityRegion.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.region;
        cityName.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.city_name;
        cityRegion.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.region;
        cityCarrage.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.courage.ToString();
        cityHappiness.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.happiness.ToString();
        cityPopulation.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.max_population.ToString();
        cityIdlePopulation.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.population.ToString();
        cityTax.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.gold_tax_rate.ToString();
	}

	void FixedUpdate () 
    {
        if (!string.Equals(userImageName, GlobalGOScript.instance.playerManager.user.image_name))
        {
            Sprite userIcon = Resources.Load<Sprite>("UI/UserIcon/" + GlobalGOScript.instance.playerManager.user.image_name);
            playerImage.sprite = userIcon;
        }

        playerRank.text = GlobalGOScript.instance.playerManager.user.rank;
        if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance != null)
        {
            playerAlliance.text = GlobalGOScript.instance.playerManager.allianceManager.userAlliance.alliance_name;
        }
        else
        {
            playerAlliance.text = "None";
        }
        playerExperience.text = GlobalGOScript.instance.playerManager.user.experience.ToString();
        cityName.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.city_name;
        cityRegion.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.region;
        cityCarrage.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.courage.ToString();
        cityHappiness.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.happiness.ToString();
        cityPopulation.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.max_population.ToString();
        cityIdlePopulation.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.population.ToString();
        cityTax.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.gold_tax_rate.ToString();
	}

    public void closePanel (string name)
    {
        switch (name)
        {
            case "lord":
                lordPanel.SetActive(false);
                break;
            case "overview":
                overviewPanel.SetActive(false);
                break;
            default:
                break;
        }

        if (!lordPanel.activeInHierarchy && !overviewPanel.activeInHierarchy)
        {
            gameObject.SetActive(false);
        }
    }

    public void OpenUserInfo()
    {
        GlobalGOScript.instance.windowInstanceManager.openUserInfo(GlobalGOScript.instance.playerManager.user);
    }
}
