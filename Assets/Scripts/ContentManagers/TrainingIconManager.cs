﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;

public class TrainingIconManager : MonoBehaviour {

    public string internalUnitName;

    public Text foodPrice;
    public Text woodPrice;
    public Text stonePrice;
    public Text ironPrice;
    public Text copperPrice;
    public Text silverPrice;
    public Text goldPrice;

    public Text trainingTime;
    public Text unitCount;
    public Text unitName;

	// Use this for initialization
	void Start () 
    {
        foodPrice.text = GlobalGOScript.instance.gameManager.unitConstants[internalUnitName].food_price.ToString();
        woodPrice.text = GlobalGOScript.instance.gameManager.unitConstants[internalUnitName].wood_price.ToString();
        stonePrice.text = GlobalGOScript.instance.gameManager.unitConstants[internalUnitName].stone_price.ToString();
        ironPrice.text = GlobalGOScript.instance.gameManager.unitConstants[internalUnitName].iron_price.ToString();
        copperPrice.text = GlobalGOScript.instance.gameManager.unitConstants[internalUnitName].cooper_price.ToString();
        silverPrice.text = GlobalGOScript.instance.gameManager.unitConstants[internalUnitName].silver_price.ToString();
        goldPrice.text = GlobalGOScript.instance.gameManager.unitConstants[internalUnitName].gold_price.ToString();

        trainingTime.text = GlobalGOScript.instance.gameManager.unitConstants[internalUnitName].time_seconds.ToString() + " S";
        unitName.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + internalUnitName.Replace("_", ""));

        if (GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits != null)
        {
            switch (internalUnitName)
            {
                case "Worker":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
                    break;
                case "Spy":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spy_count.ToString();
                    break;
                case "Swards_Man":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.swards_man_count.ToString();
                    break;
                case "Spear_Man":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spear_man_count.ToString();
                    break;
                case "Pike_Man":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.pike_man_count.ToString();
                    break;
                case "Scout_Rider":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.scout_rider_count.ToString();
                    break;
                case "Light_Cavalry":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.light_cavalry_count.ToString();
                    break;
                case "Heavy_Cavalry":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.heavy_cavalry_count.ToString();
                    break;
                case "Archer":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_count.ToString();
                    break;
                case "Archer_Rider":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_rider_count.ToString();
                    break;
                case "Holly_man":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.holly_man_count.ToString();
                    break;
                case "Wagon":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.wagon_count.ToString();
                    break;
                case "Trebuchet":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.trebuchet_count.ToString();
                    break;
                case "Siege_Towers":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.siege_towers_count.ToString();
                    break;
                case "Battering_Ram":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.battering_ram_count.ToString();
                    break;
                case "Ballista":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.ballista_count.ToString();
                    break;
                default:
                    break;
            }
        }
	}

    public void FixedUpdate()
    {
        if (GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits != null)
        {
            switch (internalUnitName)
            {
                case "Worker":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
                    break;
                case "Spy":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spy_count.ToString();
                    break;
                case "Swards_Man":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.swards_man_count.ToString();
                    break;
                case "Spear_Man":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spear_man_count.ToString();
                    break;
                case "Pike_Man":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.pike_man_count.ToString();
                    break;
                case "Scout_Rider":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.scout_rider_count.ToString();
                    break;
                case "Light_Cavalry":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.light_cavalry_count.ToString();
                    break;
                case "Heavy_Cavalry":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.heavy_cavalry_count.ToString();
                    break;
                case "Archer":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_count.ToString();
                    break;
                case "Archer_Rider":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_rider_count.ToString();
                    break;
                case "Holly_Man":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.holly_man_count.ToString();
                    break;
                case "Wagon":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.wagon_count.ToString();
                    break;
                case "Trebuchet":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.trebuchet_count.ToString();
                    break;
                case "Siege_Towers":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.siege_towers_count.ToString();
                    break;
                case "Battering_Ram":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.battering_ram_count.ToString();
                    break;
                case "Ballista":
                    unitCount.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.ballista_count.ToString();
                    break;
                default:
                    break;
            }
        }
    }

    public void OpenTrainMenu()
    {
        GlobalGOScript.instance.windowInstanceManager.openUnitDetails(internalUnitName);
    }
}
