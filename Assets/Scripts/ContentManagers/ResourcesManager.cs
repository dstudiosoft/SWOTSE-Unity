﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class ResourcesManager : MonoBehaviour {
    public Text cityFood;
	public Text cityWood;
	public Text cityIron;
	public Text cityStone;
	public Text cityCopper;
	public Text citySilver;
	public Text cityGold;

	// Use this for initialization
	void OnEnable () 
	{
        if (GlobalGOScript.instance != null) cityFood.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.food);
        if (GlobalGOScript.instance != null) cityWood.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.wood);
        if (GlobalGOScript.instance != null) cityIron.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.iron);
        if (GlobalGOScript.instance != null) cityStone.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.stone);
        if (GlobalGOScript.instance != null) cityCopper.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.cooper);
        if (GlobalGOScript.instance != null) citySilver.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.silver);
        if (GlobalGOScript.instance != null) cityGold.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.gold);
	}

	void FixedUpdate () 
	{
        cityFood.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.food);
        cityWood.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.wood);
        cityIron.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.iron);
        cityStone.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.stone);
        cityCopper.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.cooper);
        citySilver.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.silver);
        cityGold.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.gold);
	}

    string Format(long value)
    {
        if (value > 999999)
            return (value / 1000000f).ToString("0.0") + "m";
        if (value > 999)
            return (value / 1000f).ToString("0.0") + "k";
        
        return value.ToString();
    }
}
