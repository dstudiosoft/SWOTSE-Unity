﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using SmartLocalization;

public class MarketManager : MonoBehaviour {

    public Text goldValue;
    public Text silverValue;

    public Text resourceLabel;

    public InputField buyCount;

    public InputField sellCount;
    public InputField sellUnitPrice;

    public Text buyUnitPrice;
    public Text buyComission;
    public Text buyTotal;

    public Text sellComission;
    public Text sellTotal;

    public MarketBuyTableController buyTable;
    public MarketSellTableController sellTable;

    private event SimpleEvent onBuyLotsUpdated;
    private event SimpleEvent onSellLotsUpdated;
    private event SimpleEvent onResourceBought;
    private event SimpleEvent onResourceSold;
    private event SimpleEvent onLotRetracted;

    private ArrayList lotsToBuy;
    private ArrayList lotsToSell;

    public string currentResource;

    //bad idea to store coroutines here but i don't have time to make a separate manager
	
    public void Start()
    {
        goldValue.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.gold);
        silverValue.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.silver);
//        resourceLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + currentResource).ToUpper() + " (" + GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.food.ToString() + ")";
        switch (currentResource)
        {
            case "Food":
                resourceLabel.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.food);
                break;
            case "Wood":
                resourceLabel.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.wood);
                break;
            case "Iron":
                resourceLabel.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.iron);
                break;
            case "Copper":
                resourceLabel.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.cooper);
                break;
            case "Stone":
                resourceLabel.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.stone);
                break;
            default:
                break;
        }
        onBuyLotsUpdated += LotsToBuyUpdated;
        onSellLotsUpdated += LotsToSellUpdated;
        StartCoroutine(GetLotsToBuy());
        StartCoroutine(GetLotsToSell());
    }
        
	void FixedUpdate () 
    {
        goldValue.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.gold);
        silverValue.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.silver);

        switch (currentResource)
        {
            case "Food":
//                resourceLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + currentResource).ToUpper() + " (" + GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.food.ToString() + ")";
                resourceLabel.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.food);
                break;
            case "Wood":
//                resourceLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + currentResource).ToUpper() + " (" + GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.wood.ToString() + ")";
                resourceLabel.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.wood);
                break;
            case "Iron":
//                resourceLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + currentResource).ToUpper() + " (" + GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.iron.ToString() + ")";
                resourceLabel.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.iron);
                break;
            case "Copper":
//                resourceLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + currentResource).ToUpper() + " (" + GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.cooper.ToString() + ")";
                resourceLabel.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.cooper);
                break;
            case "Stone":
//                resourceLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + currentResource).ToUpper() + " (" + GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.stone.ToString() + ")";
                resourceLabel.text = Format(GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.stone);
                break;
            default:
                break;
        }
	}

    public void OpenPanel(string panel)
    {        
        currentResource = panel;
        //change datasource
        onBuyLotsUpdated += LotsToBuyUpdated;
        onSellLotsUpdated += LotsToSellUpdated;
        StartCoroutine(GetLotsToBuy());
        StartCoroutine(GetLotsToSell());
    }

    public void UpdatedBuyingValues()
    {
        if (string.IsNullOrEmpty(buyCount.text))
        {
            return;
        }
        else if (string.Equals(buyCount.text, "0"))
        {
            return;
        }

        long resourceCount = long.Parse(buyCount.text);
        long totalCount = resourceCount;
        long totalPrice = 0;

        foreach (MarketLotModel lot in lotsToBuy)
        {
            if (resourceCount > lot.count)
            {
                totalPrice += lot.count * lot.price;
                resourceCount -= lot.count;
            }
            else if (resourceCount == lot.count)
            {
                totalPrice += lot.count * lot.price;
                resourceCount -= lot.count;
                break;
            }
            else
            {
                totalPrice += resourceCount * lot.price;
                resourceCount = 0;
                break;
            }
        }

        buyTotal.text = totalPrice.ToString();
        if (resourceCount == 0)
        {
            buyUnitPrice.text = ((float)totalPrice / totalCount).ToString();
        }
        else
        {
            buyUnitPrice.text = ((float)totalPrice / (totalCount - resourceCount)).ToString();
        }
        buyComission.text = (totalPrice * 0.02f).ToString();
    }

    public void UpdateSellingValues()
    {
        if (string.IsNullOrEmpty(sellCount.text))
        {
            return;
        }
        else if (string.Equals(sellCount.text, "0"))
        {
            return;
        }

        if (string.IsNullOrEmpty(sellUnitPrice.text))
        {
            return;
        }
        else if (string.Equals(sellUnitPrice.text, "0"))
        {
            return;
        }

        long resourceCount = long.Parse(sellCount.text);
        long unitPrice = long.Parse(sellUnitPrice.text);

        switch (currentResource)
        {
            case "Food":
                if (GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.food < resourceCount)
                {
                    return;
                }
                break;
            case "Wood":
                if (GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.wood < resourceCount)
                {
                    return;
                }
                break;
            case "Iron":
                if (GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.iron < resourceCount)
                {
                    return;
                }
                break;
            case "Copper":
                if (GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.cooper < resourceCount)
                {
                    return;
                }
                break;
            case "Stone":
                if (GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.stone < resourceCount)
                {
                    return;
                }
                break;
            default:
                break;
        }

        sellTotal.text = (unitPrice * resourceCount).ToString();
        sellComission.text = ((unitPrice * resourceCount) * 0.02f).ToString();
    }

    public void RetractLot(long lotId)
    {
        onLotRetracted += LotRetracted;
        StartCoroutine(RemoveLots(lotId));
    }

    public void LotRetracted(object sender, string value)
    {
        onLotRetracted -= LotRetracted;
        if (string.Equals(value, "success"))
        {
            onSellLotsUpdated += LotsToSellUpdated;
            StartCoroutine(GetLotsToSell());
        }
    }

    public void BuyLots()
    {
        if (string.IsNullOrEmpty(buyCount.text))
        {
            return;
        }
        else if (string.Equals(buyCount.text, "0"))
        {
            return;
        }

        long resourceCount = long.Parse(buyCount.text);
        string buyDict = "{";

        foreach (MarketLotModel lot in lotsToBuy)
        {
            if (resourceCount > lot.count)
            {
                buyDict += lot.id.ToString() + ":" + lot.count.ToString() + ",";
                resourceCount -= lot.count;
            }
            else if (resourceCount == lot.count)
            {
                buyDict += lot.id.ToString() + ":" + lot.count.ToString() + ",";
                resourceCount -= lot.count;
                break;
            }
            else
            {
                buyDict += lot.id.ToString() + ":" + resourceCount.ToString() + ",";
                resourceCount = 0;
                break;
            }
        }
        buyDict.TrimEnd(new char [1] {','});
        buyDict += "}";

        onResourceBought += ResourcesBought;
        StartCoroutine(BuyLots(buyDict));
    }

    public void ResourcesBought(object sender, string value)
    {
        onResourceBought -= ResourcesBought;
        if (string.Equals(value, "success"))
        {
            onBuyLotsUpdated += LotsToBuyUpdated;
            StartCoroutine(GetLotsToBuy());
        }
    }

    public void LotsToBuyUpdated(object sender, string value)
    {
        onBuyLotsUpdated -= LotsToBuyUpdated;
        if (string.Equals(value, "success"))
        {
            buyTable.ReloadTable(lotsToBuy);
        }
    }

    public void SellLot()
    {
        if (string.IsNullOrEmpty(sellCount.text))
        {
            return;
        }
        else if (string.Equals(sellCount.text, "0"))
        {
            return;
        }

        if (string.IsNullOrEmpty(sellUnitPrice.text))
        {
            return;
        }
        else if (string.Equals(sellUnitPrice.text, "0"))
        {
            return;
        }

        long resourceCount = long.Parse(sellCount.text);
        long unitPrice = long.Parse(sellUnitPrice.text);

        switch (currentResource)
        {
            case "Food":
                if (GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.food < resourceCount)
                {
                    return;
                }
                break;
            case "Wood":
                if (GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.wood < resourceCount)
                {
                    return;
                }
                break;
            case "Iron":
                if (GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.iron < resourceCount)
                {
                    return;
                }
                break;
            case "Copper":
                if (GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.cooper < resourceCount)
                {
                    return;
                }
                break;
            case "Stone":
                if (GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.stone < resourceCount)
                {
                    return;
                }
                break;
            default:
                break;
        }

        onResourceSold += ResourcesSold;
        StartCoroutine(AddLot(resourceCount, unitPrice));
    }

    public void ResourcesSold(object sender, string value)
    {
        onResourceSold -= ResourcesSold;
        if (string.Equals(value, "success"))
        {
            onSellLotsUpdated += LotsToSellUpdated;
            StartCoroutine(GetLotsToSell());
        }
    }

    public void LotsToSellUpdated(object sender, string value)
    {
        onSellLotsUpdated -= LotsToSellUpdated;
        if (string.Equals(value, "success"))
        {
            sellTable.ReloadTable(lotsToSell);
        }
    }


    public IEnumerator GetLotsToBuy()
    {
        lotsToBuy = null;
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/market/getLots/" + currentResource + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                //failed
                if (onBuyLotsUpdated != null)
                {
                    onBuyLotsUpdated(this, "fail");
                }
            }
            else
            {
                //success
                JSONObject lotsArray = JSONObject.Create(request.downloadHandler.text);

                if (lotsArray.list.Count > 0)
                {
                    lotsToBuy = new ArrayList(lotsArray.list.Count);

                    foreach (JSONObject lot in lotsArray.list)
                    {
                        MarketLotModel lotModel = JsonUtility.FromJson<MarketLotModel>(lot.Print());
                        lotsToBuy.Add(lotModel);
                    }
                }

                //sort that
                if (lotsToBuy != null)
                {
                    for (int j = 0; j < lotsToBuy.Count; j++)
                    {
                        for (int i = 0; i < lotsToBuy.Count - 1; i++)
                        {
                            if (((MarketLotModel)(lotsToBuy[i])).price > ((MarketLotModel)(lotsToBuy[i + 1])).price)
                            {
                                MarketLotModel temp = (MarketLotModel)lotsToBuy[i];
                                lotsToBuy[i] = lotsToBuy[i + 1];
                                lotsToBuy[i + 1] = lotsToBuy[i];
                            }
                        }
                    }
                }

                if (onBuyLotsUpdated != null)
                {
                    onBuyLotsUpdated(this, "success");
                }
            }    
        }
    }

    public IEnumerator GetLotsToSell()
    {
        lotsToSell = null;
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/market/getCityLots/" + GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString() + "/" + currentResource + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                //failed
                if (onSellLotsUpdated != null)
                {
                    onSellLotsUpdated(this, "fail");
                }
            }
            else
            {
                //success
                JSONObject lotsArray = JSONObject.Create(request.downloadHandler.text);

                if (lotsArray.list.Count > 0)
                {
                    lotsToSell = new ArrayList(lotsArray.list.Count);

                    foreach (JSONObject lot in lotsArray.list)
                    {
                        MarketLotModel lotModel = JsonUtility.FromJson<MarketLotModel>(lot.Print());
                        lotsToSell.Add(lotModel);
                    }
                }

                if (onSellLotsUpdated != null)
                {
                    onSellLotsUpdated(this, "success");
                }
            }    
        }
    }

    public IEnumerator AddLot(long resourceCount, long unitPrice)
    {
        WWWForm addForm = new WWWForm();
        addForm.AddField("city", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());
        addForm.AddField("resource_type", currentResource.ToLower());
        addForm.AddField("count", resourceCount.ToString());
        addForm.AddField("price", unitPrice.ToString());
        
        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/market/addLot/", addForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Lot successfully added");

                    if (onResourceSold != null)
                    {
                        onResourceSold(this, "success");
                    }
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Lot adding failed");

                    if (onResourceSold != null)
                    {
                        onResourceSold(this, "fail");
                    }
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Lot adding failed");

                if (onResourceSold != null)
                {
                    onResourceSold(this, "fail");
                }
            }    
        }
    }

    public IEnumerator RemoveLots(long lotId)
    {
        WWWForm removeForm = new WWWForm();
        removeForm.AddField("lotID", lotId.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/market/removeLots/", removeForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    if (onLotRetracted != null)
                    {
                        onLotRetracted(this, "success");
                    }
                }
                else
                {
                    if (onLotRetracted != null)
                    {
                        onLotRetracted(this, "fail");
                    }
                }
            }
            else
            {
                if (onLotRetracted != null)
                {
                    onLotRetracted(this, "fail");
                }
            }    
        }
    }

    public IEnumerator BuyLots(string lotsDict)
    {
        WWWForm buyForm = new WWWForm();
        buyForm.AddField("city", GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString());
        buyForm.AddField("lots", lotsDict);

        UnityWebRequest request = UnityWebRequest.Post("http://" + GlobalGOScript.instance.host + ":8889/market/buyLots/", buyForm);
        request.SetRequestHeader("authorization", "Token " + GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                if (string.Equals(obj.str, "success"))
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Resources bought successfully");

                    if (onResourceBought != null)
                    {
                        onResourceBought(this, "success");
                    }
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openNotification("Resources not bought");

                    if (onResourceBought != null)
                    {
                        onResourceBought(this, "fail");
                    }
                }
            }
            else
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Resources not bought");

                if (onResourceBought != null)
                {
                    onResourceBought(this, "fail");
                }
            }    
        }
    }

    string Format(long value)
    {
        if (value > 999999)
            return (value / 1000000f).ToString("0.0") + "m";
        if (value > 999)
            return (value / 1000f).ToString("0.0") + "k";

        return value.ToString();
    }
}
