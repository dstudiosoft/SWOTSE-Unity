﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NewMailContentManager : MonoBehaviour {

    public InputField subject;
    public InputField message;

    public void SendUserMessage(long userId)
    {
        if (!string.IsNullOrEmpty(message.text))
        {
            GlobalGOScript.instance.playerManager.reportManager.onUserMessageSent += UserMessageSent;
            StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.SendUserMessage(userId, subject.text, message.text));
        }
    }

    public void UserMessageSent(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.reportManager.onUserMessageSent -= UserMessageSent;

        if (string.Equals(value, "success"))
        {
            subject.text = string.Empty;
            message.text = string.Empty;

            GlobalGOScript.instance.windowInstanceManager.openNotification("Message successfully sent");
        }
    }
}
