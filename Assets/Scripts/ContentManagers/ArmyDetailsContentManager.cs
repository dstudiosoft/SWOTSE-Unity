﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ArmyDetailsContentManager : MonoBehaviour {

    public Text coords;
    public Text cityName;
    public Text targetPlayer;

    public long armyId;
    public CommandCenterArmiesTableController owner;

    public Text armyWorker;
    public Text armySpy;
    public Text armySwordsman;
    public Text armySpearman;
    public Text armyPikeman;
    public Text armyScoutRider;
    public Text armyLightCavalry;
    public Text armyHeavyCavalry;
    public Text armyArcher;
    public Text armyArcherRider;
    public Text armyHollyman;
    public Text armyWagon;
    public Text armyTrebuchet;
    public Text armySiegeTower;
    public Text armyBatteringRam;
    public Text armyBallista;

    private ArmyModel army;
    private SimpleEvent gotReturnArmy;
	
	void Start () {

        army = GlobalGOScript.instance.gameManager.battleManager.cityArmies[armyId];

        coords.text = "(" + army.destination_point.posX.ToString() + ", " + army.destination_point.posY.ToString() + ")";
        if (army.destination_point.city != null)
        {
            cityName.text = army.destination_point.city.city_name;
        }
        else
        {
            cityName.text = army.destination_point.cityType.Replace('_', ' ');
        }

        if (army.hero != null)
        {
            targetPlayer.text = army.hero.name;
        }
        else
        {
            targetPlayer.text = "None";
        }

        armyWorker.text = army.workers_count.ToString();
        armySpy.text = army.spy_count.ToString();
        armySwordsman.text = army.swards_man_count.ToString();
        armySpearman.text = army.spear_man_count.ToString();
        armyPikeman.text = army.pike_man_count.ToString();
        armyScoutRider.text = army.scout_rider_count.ToString();
        armyLightCavalry.text = army.light_cavalry_count.ToString();
        armyHeavyCavalry.text = army.heavy_cavalry_count.ToString();
        armyArcher.text = army.archer_count.ToString();
        armyArcherRider.text = army.archer_rider_count.ToString();
        armyHollyman.text = army.holly_man_count.ToString();
        armyWagon.text = army.wagon_count.ToString();
        armyTrebuchet.text = army.trebuchet_count.ToString();
        armySiegeTower.text = army.siege_towers_count.ToString();
        armyBatteringRam.text = army.battering_ram_count.ToString();
        armyBallista.text = army.ballista_count.ToString();
	}

    public void ReturnArmy()
    {
        GlobalGOScript.instance.gameManager.battleManager.onArmyReturnInitiated += owner.ReloadData;
        GlobalGOScript.instance.gameManager.battleManager.onArmyReturnInitiated += ArmyReturnSet;
        StartCoroutine(GlobalGOScript.instance.gameManager.battleManager.ReturnArmy(armyId));
    }

    public void ArmyReturnSet(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.battleManager.onArmyReturnInitiated -= ArmyReturnSet;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.closeArmyDetails();
        }
    }
}
