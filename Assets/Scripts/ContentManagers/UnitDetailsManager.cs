﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;

public class UnitDetailsManager : MonoBehaviour {

    public Text unitCountLabel;
    public Text unitDescriptionLabel;
    public Image unitIcon;

    public Text speed;
    public Text defence;
    public Text food;
    public Text range;
    public Text life;
    public Text population;
    public Text attack;
    public Text capacity;
    public Text time;

    //needed resources
    public Text foodNeeded;
    public Text woodNeeded;
    public Text stoneNeeded;
    public Text ironNeeded;
    public Text copperNeeded;
    public Text silverNeeded;
    public Text goldNeeded;

    //present resources
    public Text foodPresent;
    public Text woodPresent;
    public Text stonePresent;
    public Text ironPresent;
    public Text copperPresent;
    public Text silverPresent;
    public Text goldPresent;

    public InputField unitsToRecruit;

    public string unitName;

    private UnitConstantModel unit;
    private ResourcesModel resources;

    void OnEnable()
    {
        unit = GlobalGOScript.instance.gameManager.unitConstants[unitName];

        string localizedValue = null;
        localizedValue = SmartLocalization.LanguageManager.Instance.GetTextValue("SWOTSELocalization.EnterUnits").ToUpper();

        if (SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
        {
            localizedValue = ArabicSupport.ArabicFixer.Fix(localizedValue);
            StartCoroutine(TextLocalizer.WrapMultiline(unitDescriptionLabel, unit.description));
            unitDescriptionLabel.alignment = TextAnchor.UpperRight;
        }
            
        unitsToRecruit.textComponent.text = localizedValue;

        speed.text = unit.speed.ToString();
        defence.text = unit.defence.ToString();
        food.text = ((float)unit.food_price / 10).ToString();
        range.text = unit.range.ToString();
        life.text = unit.life.ToString();
        population.text = unit.population_price.ToString();
        attack.text = unit.attack.ToString();
        capacity.text = unit.capacity.ToString();
        time.text = unit.time_seconds.ToString() + " S";

        resources = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources;
        foodPresent.text = Format(resources.food);
        woodPresent.text = Format(resources.wood);
        stonePresent.text = Format(resources.stone);
        ironPresent.text = Format(resources.iron);
        copperPresent.text = Format(resources.cooper);
        silverPresent.text = Format(resources.silver);
        goldPresent.text = Format(resources.gold);

        foodNeeded.text = Format(unit.food_price);
        woodNeeded.text = Format(unit.wood_price);
        stoneNeeded.text = Format(unit.stone_price);
        ironNeeded.text = Format(unit.iron_price);
        copperNeeded.text = Format(unit.cooper_price);
        silverNeeded.text = Format(unit.silver_price);
        goldNeeded.text = Format(unit.gold_price);

        switch (unitName)
        {
            case "Worker":
                unitCountLabel.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
                break;
            case "Spy":
                unitCountLabel.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spy_count.ToString();
                break;
            case "Swards_Man":
                unitCountLabel.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.swards_man_count.ToString();
                break;
            case "Spear_Man":
                unitCountLabel.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spear_man_count.ToString();
                break;
            case "Pike_Man":
                unitCountLabel.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.pike_man_count.ToString();
                break;
            case "Scout_Rider":
                unitCountLabel.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.scout_rider_count.ToString();
                break;
            case "Light_Cavalry":
                unitCountLabel.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.light_cavalry_count.ToString();
                break;
            case "Heavy_Cavalry":
                unitCountLabel.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.heavy_cavalry_count.ToString();
                break;
            case "Archer":
                unitCountLabel.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_count.ToString();
                break;
            case "Archer_Rider":
                unitCountLabel.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_rider_count.ToString();
                break;
            case "Holly_Man":
                unitCountLabel.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.holly_man_count.ToString();
                break;
            case "Wagon":
                unitCountLabel.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.wagon_count.ToString();
                break;
            case "Trebuchet":
                unitCountLabel.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.trebuchet_count.ToString();
                break;
            case "Siege_Towers":
                unitCountLabel.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.siege_towers_count.ToString();
                break;
            case "Battering_Ram":
                unitCountLabel.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.battering_ram_count.ToString();
                break;
            case "Ballista":
                unitCountLabel.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.ballista_count.ToString();
                break;
            default:
                break;
            }

        Sprite unitSprite = Resources.Load<Sprite>("UI/UnitIcons/" + unit.name);
        unitIcon.sprite = unitSprite;
	}

    public void UpdatePrice(string count)
    {        
        long multiplier;
        long.TryParse(unitsToRecruit.text, out multiplier);
        foodNeeded.text = Format(unit.food_price * multiplier);
        woodNeeded.text = Format(unit.wood_price * multiplier);
        stoneNeeded.text = Format(unit.stone_price * multiplier);
        ironNeeded.text = Format(unit.iron_price * multiplier);
        copperNeeded.text = Format(unit.cooper_price * multiplier);
        silverNeeded.text = Format(unit.silver_price * multiplier);
        goldNeeded.text = Format(unit.gold_price * multiplier);
    }
	
	void FixedUpdate () 
    {
        resources = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources;
        foodPresent.text = Format(resources.food);
        woodPresent.text = Format(resources.wood);
        stonePresent.text = Format(resources.stone);
        ironPresent.text = Format(resources.iron);
        copperPresent.text = Format(resources.cooper);
        silverPresent.text = Format(resources.silver);
        goldPresent.text = Format(resources.gold);
	}

    public void HireUnits()
    {
        GlobalGOScript.instance.gameManager.cityManager.unitsManager.onUnitsHired += UnitsHired;
        long hireCount;
        long.TryParse(unitsToRecruit.text, out hireCount);
        if (hireCount != 0)
        {
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.HireUnits(hireCount, unitName));
        }
    }

    public void UnitsHired(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.unitsManager.onUnitsHired -= UnitsHired;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.openNotification("Units hired successfully");
            GlobalGOScript.instance.windowInstanceManager.closeUnitDetails();
        }
        else
        {
            GlobalGOScript.instance.windowInstanceManager.openNotification(value);
        }
    }

    string Format(long value)
    {
        if (value > 999999)
            return (value / 1000000f).ToString("0.0") + "m";
        if (value > 999)
            return (value / 1000f).ToString("0.0") + "k";

        return value.ToString();
    }
}
