﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AllianceGuidelineManager : MonoBehaviour {

    public InputField guidelineField;

	// Use this for initialization
	void Start () 
    {
        if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance != null)
        {
            if (!string.IsNullOrEmpty(GlobalGOScript.instance.playerManager.allianceManager.userAlliance.guideline))
            {
                guidelineField.text = GlobalGOScript.instance.playerManager.allianceManager.userAlliance.guideline;
            }
        }
	}

    public void UpdateGuideline()
    {
        if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance != null)
        {
            GlobalGOScript.instance.playerManager.allianceManager.onUpdateGuideline += GuideLineUpdated;

            StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.UpdateGuideline(guidelineField.text));
        }
        else
        {
            guidelineField.text = string.Empty;
        }
    }

    public void GuideLineUpdated(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.allianceManager.onUpdateGuideline -= GuideLineUpdated;

        if (string.Equals(value, "success"))
        {
            guidelineField.text = GlobalGOScript.instance.playerManager.allianceManager.userAlliance.guideline;
        }
    }
}
