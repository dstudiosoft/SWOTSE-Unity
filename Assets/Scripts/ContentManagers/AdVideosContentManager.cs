﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Globalization;

public class AdVideosContentManager : MonoBehaviour {

    public Button foodButton;
    public Button woodButton;
    public Button stoneButton;
    public Button ironButton;
    public Button silverButton;
    public Button goldButton;
    public Button copperButton;

    public Text foodTime;
    public Text woodTime;
    public Text stoneTime;
    public Text ironTime;
    public Text silverTime;
    public Text goldTime;
    public Text copperTime;

	private bool isDownloaded;

	// Use this for initialization
	void Start () 
    {
        GlobalGOScript.instance.gameManager.levelLoadManager.adManager.requestInterstitialAd();
		this.isDownloaded = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
    {
        DateTime now = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update;
        woodButton.interactable = GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusWoodDate < now;
        foodButton.interactable = GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusFoodDate < now;
        stoneButton.interactable = GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusStoneDate < now;
        ironButton.interactable = GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusIronDate < now;
        silverButton.interactable = GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusSilverDate < now;
        goldButton.interactable = GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusGoldDate < now;
        copperButton.interactable = GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusCopperDate < now;


        TimeSpan diff = GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusFoodDate - now;

		bool isLoad = GlobalGOScript.instance.gameManager.levelLoadManager.adManager.IsLoaded ();


		if (!this.isDownloaded) {
			if (isLoad) {
				this.isDownloaded = true;
			}
		}
		 
		if (this.isDownloaded) {
			if (!isLoad) {
				GlobalGOScript.instance.gameManager.levelLoadManager.adManager.requestInterstitialAd();
				this.isDownloaded = false;
			}
		}

        if (diff.Ticks < 0)
        {
            diff = TimeSpan.Zero;
        }

        foodTime.text = diff.Minutes.ToString() + ":" + diff.Seconds.ToString();

        diff = GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusWoodDate - now;

        if (diff.Ticks < 0)
        {
            diff = TimeSpan.Zero;
        }

        woodTime.text = diff.Minutes.ToString() + ":" + diff.Seconds.ToString();

        diff = GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusStoneDate - now;

        if (diff.Ticks < 0)
        {
            diff = TimeSpan.Zero;
        }

        stoneTime.text = diff.Minutes.ToString() + ":" + diff.Seconds.ToString();

        diff = GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusIronDate - now;

        if (diff.Ticks < 0)
        {
            diff = TimeSpan.Zero;
        }

        ironTime.text = diff.Minutes.ToString() + ":" + diff.Seconds.ToString();

        diff = GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusSilverDate - now;

        if (diff.Ticks < 0)
        {
            diff = TimeSpan.Zero;
        }

        silverTime.text = diff.Minutes.ToString() + ":" + diff.Seconds.ToString();

        diff = GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusGoldDate - now;

        if (diff.Ticks < 0)
        {
            diff = TimeSpan.Zero;
        }

        goldTime.text = diff.Minutes.ToString() + ":" + diff.Seconds.ToString();

        diff = GlobalGOScript.instance.gameManager.levelLoadManager.adManager.nextBonusCopperDate - now;

        if (diff.Ticks < 0)
        {
            diff = TimeSpan.Zero;
        }

        copperTime.text = diff.Minutes.ToString() + ":" + diff.Seconds.ToString();
	}

    public void WatchButtonPressed(string resource)
    {
        GlobalGOScript.instance.gameManager.levelLoadManager.adManager.ShowInterstitialForResource(resource);
    }
}
