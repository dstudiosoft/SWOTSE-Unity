﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class StatisticsManager : MonoBehaviour {

	public GameObject resourcesPanel;
	public GameObject knightsPanel;
	public GameObject unitsPanel;

	public Text cityFood;
	public Text cityWood;
	public Text cityIron;
	public Text cityStone;
	public Text cityCopper;
	public Text citySilver;
	public Text cityGold;

	//for knights handling

	public Text cityWorker;
	public Text citySpy;
	public Text citySwordsman;
	public Text citySpearman;
	public Text cityPikeman;
	public Text cityScoutRider;
	public Text cityLightCavalry;
	public Text cityHeavyCavalry;
	public Text cityArcher;
	public Text cityArcherRider;
	public Text cityHollyman;
	public Text cityWagon;
	public Text cityTrebuchet;
	public Text citySiegeTower;
	public Text cityBatteringRam;
	public Text cityBallista;
	public Text cityBeaconTower;
	public Text cityArcherTower;
	public Text cityHotOilHole;
	public Text cityTrebuchetTower;
	public Text cityBallistaTower;

	// Use this for initialization
	void OnEnable () 
	{
		resourcesPanel.SetActive (true);
		knightsPanel.SetActive (true);
		unitsPanel.SetActive (true);

		//resources
        cityFood.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.food.ToString();
        cityWood.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.wood.ToString();
        cityIron.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.iron.ToString();
        cityStone.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.stone.ToString();
        cityCopper.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.cooper.ToString();
        citySilver.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.silver.ToString();
        cityGold.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.gold.ToString();
//
//		//knights update
//
//		//units
        cityWorker.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
        citySpy.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spy_count.ToString();
        citySwordsman.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.swards_man_count.ToString();
        citySpearman.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spear_man_count.ToString();
        cityPikeman.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.pike_man_count.ToString();
        cityScoutRider.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.scout_rider_count.ToString();
        cityLightCavalry.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.light_cavalry_count.ToString();
        cityHeavyCavalry.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.heavy_cavalry_count.ToString();
        cityArcher.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_count.ToString();
        cityArcherRider.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_rider_count.ToString();
        cityHollyman.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.holly_man_count.ToString();
        cityWagon.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.wagon_count.ToString();
        cityTrebuchet.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.trebuchet_count.ToString();
        citySiegeTower.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.siege_towers_count.ToString();
        cityBatteringRam.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.battering_ram_count.ToString();
        cityBallista.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.ballista_count.ToString();
//        cityBeaconTower.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
//        cityArcherTower.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
//        cityHotOilHole.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
//        cityTrebuchetTower.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
//        cityBallistaTower.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
	}

	void FixedUpdate () 
	{
		//resources
      cityFood.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.food.ToString();
      cityWood.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.wood.ToString();
      cityIron.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.iron.ToString();
      cityStone.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.stone.ToString();
      cityCopper.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.cooper.ToString();
      citySilver.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.silver.ToString();
      cityGold.text = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.gold.ToString();

		//knights update

//		//units
        cityWorker.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
        citySpy.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spy_count.ToString();
        citySwordsman.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.swards_man_count.ToString();
        citySpearman.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.spear_man_count.ToString();
        cityPikeman.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.pike_man_count.ToString();
        cityScoutRider.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.scout_rider_count.ToString();
        cityLightCavalry.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.light_cavalry_count.ToString();
        cityHeavyCavalry.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.heavy_cavalry_count.ToString();
        cityArcher.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_count.ToString();
        cityArcherRider.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.archer_rider_count.ToString();
        cityHollyman.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.holly_man_count.ToString();
        cityWagon.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.wagon_count.ToString();
        cityTrebuchet.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.trebuchet_count.ToString();
        citySiegeTower.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.siege_towers_count.ToString();
        cityBatteringRam.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.battering_ram_count.ToString();
        cityBallista.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.ballista_count.ToString();
//        cityBeaconTower.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
//        cityArcherTower.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
//        cityHotOilHole.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
//        cityTrebuchetTower.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
//        cityBallistaTower.text = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits.workers_count.ToString();
	}

	public void closePanel(string name)
	{
		switch(name)
		{
		case "resources":
			resourcesPanel.SetActive (false);
			break;

		case "knights":
			knightsPanel.SetActive (false);
			break;

		case "units":
			unitsPanel.SetActive (false);
			break;

			default:
				break;
		}

		if (!resourcesPanel.activeInHierarchy && !knightsPanel.activeInHierarchy && !unitsPanel.activeInHierarchy) 
		{
			gameObject.SetActive (false);
		}
	}
}
