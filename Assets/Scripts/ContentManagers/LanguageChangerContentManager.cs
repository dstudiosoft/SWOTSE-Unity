﻿using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using System.Collections;

public class LanguageChangerContentManager : MonoBehaviour {

    public Dropdown language;
    private int previousValue;

	void Start () 
    {
        if (string.Equals(LanguageManager.Instance.CurrentlyLoadedCulture.languageCode, "en"))
        {
            language.value = 0;
            previousValue = 0;
        }
        else
        {
            language.value = 1;
            previousValue = 1;
        }
	}

    public void Close()
    {
        GlobalGOScript.instance.windowInstanceManager.closeChangeLanguage();
    }

    public void Save()
    {
        if (language.value != previousValue)
        {
            GlobalGOScript.instance.playerManager.onLanguageChanged += LanguageChanged;
            if (language.value == 0)
            {
                StartCoroutine(GlobalGOScript.instance.playerManager.ChangeLanguage("en"));
            }
            else
            {
                StartCoroutine(GlobalGOScript.instance.playerManager.ChangeLanguage("ar"));
            }   
        }
    }

    public void LanguageChanged(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.onLanguageChanged -= LanguageChanged;
        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.Relogin();
//            GlobalGOScript.instance.windowInstanceManager.closeChangeLanguage();
//            GlobalGOScript.instance.windowInstanceManager.closeUserInfo();
            GlobalGOScript.instance.windowInstanceManager.closeWindow();
        }
    }
}
