﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class BuildingInformationManager : MonoBehaviour {

    public ConstructionPitManager pitManager;

    public GameObject trainingPanel;
    public GameObject marketPanel;
    public GameObject residencePanel;
    public GameObject guestHousePanel;
    public GameObject feastingHallPanel;
    public GameObject diplomacyPanel;
    public GameObject commandCenterPanel;

    public GameObject archery;
    public GameObject godhouse;
    public GameObject stable;
    public GameObject academy;
    public GameObject engineering;

    public Text buildingName;
    public Text buildingDesctiption;
    public Text buildingLevel;
    public Image buildingImage;

    public Dropdown taxDropdown;
    private long taxIndex;

    public string type;
    public long world_id;

    public void OnEnable()
    {
        if (string.Equals(type, string.Empty))
        {
            type = pitManager.type;
        }

        BuildingModel building = null;
        WorldFieldModel valley = null;

        if (string.Equals(type, "Buildings"))
        {
            building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitManager.pitId];
        }
        else if (string.Equals(type, "Fields"))
        {
            building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pitManager.pitId];
        }

        if (string.Equals(type, "World"))
        {
            valley = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityValleys[world_id];
            Sprite buildingSprite = Resources.Load<Sprite>("WorldMapBuildings/" + valley.cityType + "/" + valley.level.ToString());
            buildingImage.sprite = buildingSprite;
            buildingName.text = valley.cityType.Replace('_', ' ');
            buildingLevel.text = valley.level.ToString();
            buildingDesctiption.text = string.Empty;
        }
        else
        {
            Sprite buildingSprite = Resources.Load<Sprite>("UI/"+type+"Icon/"+building.building);
            buildingImage.sprite = buildingSprite;
            buildingName.text = GlobalGOScript.instance.gameManager.buildingConstants[building.building].building_name.Replace("_", " ");
            buildingLevel.text = building.level.ToString();
            buildingDesctiption.text = GlobalGOScript.instance.gameManager.buildingConstants[building.building].description;

            switch (building.building)
            {
                case "Archery":
                    trainingPanel.SetActive(true);
                    archery.SetActive(true);
                    break;
                case "God_House":
                    trainingPanel.SetActive(true);
                    godhouse.SetActive(true);
                    break;
                case "Stable":
                    trainingPanel.SetActive(true);
                    stable.SetActive(true);
                    break;
                case "Military_School":
                    trainingPanel.SetActive(true);
                    academy.SetActive(true);
                    break;
                case "Engineering":
                    trainingPanel.SetActive(true);
                    engineering.SetActive(true);
                    break;
                case "Market":
                    marketPanel.SetActive(true);
                    break;
                case "Mayor_Residence":
                    taxIndex = (GlobalGOScript.instance.gameManager.cityManager.currentCity.gold_tax_rate / 10) - 1;
                    taxDropdown.value = (int)taxIndex;
                    residencePanel.SetActive(true);
                    break;
                case "Guest_House":
                    guestHousePanel.SetActive(true);
                    break;
                case "Feasting_Hall":
                    feastingHallPanel.SetActive(true);
                    break;
                case "Diplomacy":
                    diplomacyPanel.SetActive(true);
                    break;
                case "Command_Center":
                    commandCenterPanel.SetActive(true);
                    break;
                default:
                    break;
            }
        }
    }

    public void Upgrade()
    {
        BuildingModel building = null;
        WorldFieldModel valley = null;

        if (string.Equals(type, "Buildings"))
        {
            building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitManager.pitId];
            if (building.level < 10)
            {
                GlobalGOScript.instance.windowInstanceManager.openUpdateBuilding(true, type, pitManager.pitId);
            }
        }
        else if (string.Equals(type, "Fields"))
        {
            building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pitManager.pitId];
            if (building.level < 10)
            {
                GlobalGOScript.instance.windowInstanceManager.openUpdateBuilding(true, type, pitManager.pitId);
            }
        }
        else if (string.Equals(type, "World"))
        {
            valley = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityValleys[world_id];
        }

        if (string.Equals(type, "World"))
        {
            if (valley.level < 10)
            {
                StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.UpgradeValley(valley.id));
            }
        }
        //else
        //{
        //    if (building.level < 10)
        //    {
        //        //            GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate += pitManager.UpdatePitState;
        //        //            GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate += HandleUpdate;
        //        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.UpdateBuilding(building.city, building.pit_id, "upgrade", type));
        //    }
        //}
    }

    public void Downgrade()
    {
        WorldFieldModel valley = null;

        if (string.Equals(type, "Buildings"))
        {
            //building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitManager.pitId];
            GlobalGOScript.instance.windowInstanceManager.openUpdateBuilding(false, type, pitManager.pitId);
        }
        else if (string.Equals(type, "Fields"))
        {
            //building = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pitManager.pitId];
            GlobalGOScript.instance.windowInstanceManager.openUpdateBuilding(false, type, pitManager.pitId);
        }
        else if (string.Equals(type, "World"))
        {
            valley = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityValleys[world_id];
        }

//        if (building.level > 1)
//        {
////            GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate += pitManager.UpdatePitState;
////            GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate += HandleUpdate;

//            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.UpdateBuilding(building.city, building.pit_id, "downgrade", type));
//            //valley
//        }
//        else
//        {
//            if (!string.Equals(building.building, "Mayor_Residence") && !string.Equals(building.building, "Wall") && !string.Equals(type, "World"))
//            {
////                GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate += pitManager.UpdatePitState;
////                GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate += HandleUpdate;

////                StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.DestroyBuilding(type, building.pit_id));
//                StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.UpdateBuilding(building.city, building.pit_id, "downgrade", type));
//            }
//        }
    }
        
    public void HandleUpdate(object sender, string value)
    {
        if (string.Equals(value, "success"))
        {
            BuildingModel buildingState = null;

            if (string.Equals(type, "Buildings"))
            {
                if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings.ContainsKey(pitManager.pitId))
                {
                    buildingState = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitManager.pitId];
                }
                else
                {
//                    GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate -= HandleUpdate;
                    GlobalGOScript.instance.windowInstanceManager.closeBuildingInformation();
                    return;
                }
            }
            else if (string.Equals(type, "Fields"))
            {
                if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields.ContainsKey(pitManager.pitId))
                {
                    buildingState = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pitManager.pitId];
                }
                else
                {
//                    GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate -= HandleUpdate;
                    GlobalGOScript.instance.windowInstanceManager.closeBuildingInformation();
                    return;
                }
            }            

            if (buildingState == null)
            {
//                GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate -= HandleUpdate;
                GlobalGOScript.instance.windowInstanceManager.closeBuildingInformation();
                return;
            }
            else
            {
                buildingLevel.text = buildingState.level.ToString();
            }

//            GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate -= HandleUpdate;
        }
        else
        {
//            GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate -= HandleUpdate;
        }
    }

    #region - Tax

    public void UpdateTax()
    {
        if ((int)taxIndex != taxDropdown.value)
        {
            GlobalGOScript.instance.gameManager.cityManager.onTaxUpdated += TaxUpdated;
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.UpdateCityTax((taxDropdown.value + 1) * 10));
        }
    }

    public void TaxUpdated(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.onTaxUpdated -= TaxUpdated;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.gameManager.cityManager.currentCity.gold_tax_rate = (taxDropdown.value + 1) * 10;
            taxIndex = taxDropdown.value;
        }
        else
        {
            taxDropdown.value = (int)taxIndex;
        }
    } 

    #endregion
}
