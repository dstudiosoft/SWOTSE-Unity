﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class AttackColonizeDestroyReportManager : MonoBehaviour
{
    //attacker
    public Text attackerName;
    public Image attackerImage;
    public Text attackerRank;
    public Text attackerAllianceName;
    public Text attackerAlliancePosition;
    public Text attackerCityName;
    public Image attackerCityImage;
    public Text attackerExperiance;
    public Text attackerCityRegion;

    //defender
    public Text defenderName;
    public Image defenderImage;
    public Text defenderRank;
    public Text defenderAllianceName;
    public Text defenderAlliancePosition;
    public Text defenderCityName;
    public Image defenderCityImage;
    public Text defenderExperiance;
    public Text defenderCityRegion;
    public Text defenderCityHappiness;
    public Text defenderCityCourage;

    public Text attackerResult;
    public Text defenderResult;

    public BattleReportModel report;

    public BattleReportUnitTableController attackerUnitsController;
    public BattleReportUnitTableController defenderUnitsController;
    public BattleReportResourcesTableController attackerResourcesController;
    public BattleReportResourcesTableController defenderResourcesController;
    public BattleReportItemsTableController attackerItemsController;

    private List<string> attackerUnitNames;
    private List<long> attackerBeforeCount;
    private List<long> attackerAfterCount;

    private List<string> defenderUnitNames;
    private List<long> defenderBeforeCount;
    private List<long> defenderAfterCount;

    void OnEnable()
    {

        if (report.isSuccessAttack)
        {
            attackerResult.text = "Won";
            defenderResult.text = "Lost";
        }
        else
        {
            attackerResult.text = "Lost";
            defenderResult.text = "Won";
        }

        //attacker
        attackerName.text = report.attackUser.username;
        Sprite attackerSprite = Resources.Load<Sprite>("UI/UserIcon/" + report.attackUser.image_name);
        attackerImage.sprite = attackerSprite;
        attackerRank.text = TextLocalizer.LocalizeString(report.attackUser.rank);
        if (report.attackUser.alliance != null)
        {
            attackerAllianceName.text = report.attackUser.alliance.alliance_name;
            attackerAlliancePosition.text = TextLocalizer.LocalizeString(report.attackUser.alliance_rank);
        }
        else
        {
            attackerAllianceName.text = TextLocalizer.LocalizeString("none");
            attackerAlliancePosition.text = TextLocalizer.LocalizeString("none");
        }
        attackerCityName.text = report.attackersCity.city_name;
        Sprite attackerCitySprite = Resources.Load<Sprite>("UI/CityIcon/" + report.attackersCity.image_name);
        attackerCityImage.sprite = attackerCitySprite;
        attackerExperiance.text = report.attackUser.experience.ToString();
        if (!string.IsNullOrEmpty(report.attackersCity.region))
        {
            attackerCityRegion.text = TextLocalizer.LocalizeString(report.attackersCity.region);
        }
        else
        {
            attackerCityRegion.text = TextLocalizer.LocalizeString("none");
        }

        //defender
        defenderName.text = report.defenceUser.username;
        Sprite defenderSprite = Resources.Load<Sprite>("UI/UserIcon/" + report.defenceUser.image_name);
        defenderImage.sprite = defenderSprite;
        defenderRank.text = TextLocalizer.LocalizeString(report.defenceUser.rank);
        if (report.defenceUser.alliance != null)
        {
            defenderAllianceName.text = report.defenceUser.alliance.alliance_name;
            defenderAlliancePosition.text = TextLocalizer.LocalizeString(report.defenceUser.alliance_rank);
        }
        else
        {
            defenderAllianceName.text = TextLocalizer.LocalizeString("none");
            defenderAlliancePosition.text = TextLocalizer.LocalizeString("none");
        }
        defenderCityName.text = report.defendersCity.city_name;
        Sprite defenderCitySprite = Resources.Load<Sprite>(report.defendersCity.image_name);
        defenderCityImage.sprite = defenderCitySprite;
        defenderExperiance.text = report.defenceUser.experience.ToString();
        if (!string.IsNullOrEmpty(report.defendersCity.region))
        {
            defenderCityRegion.text = TextLocalizer.LocalizeString(report.defendersCity.region);
        }
        else
        {
            defenderCityRegion.text = TextLocalizer.LocalizeString("none");
        }
        defenderCityHappiness.text = report.defendersCity.happiness.ToString() + " %";
        defenderCityCourage.text = report.defendersCity.courage.ToString() + " %";

        //parse armies
        attackerUnitNames = new List<string>();
        attackerBeforeCount = new List<long>();
        attackerAfterCount = new List<long>();

        defenderUnitNames = new List<string>();
        defenderBeforeCount = new List<long>();
        defenderAfterCount = new List<long>();

        //attacker
        for (int i = 0; i < report.attackArmyBeforeBattle.list.Count; i++)
        {
            if (report.attackArmyBeforeBattle.list[i].i > 0)
            {
                if (!report.attackArmyBeforeBattle.keys[i].Contains("count"))
                {
                    continue;
                }

                string[] parts = report.attackArmyBeforeBattle.keys[i].Split('_');
                string result = string.Empty;

                for (int j = 0; j < parts.Length - 1; j++)
                {
                    result += parts[j] + ' ';
                }
                result = result.Trim();

                attackerUnitNames.Add(result);
                attackerBeforeCount.Add(report.attackArmyBeforeBattle[i].i);
                attackerAfterCount.Add(report.attackArmyAfterBattle[i].i);
            }
        }

        //defender
        if (!report.defenceArmyBeforeBattle.IsNull)
        {
            for (int i = 0; i < report.defenceArmyBeforeBattle.list.Count; i++)
            {
                if (report.defenceArmyBeforeBattle.list[i].i > 0)
                {
                    if (!report.attackArmyBeforeBattle.keys[i].Contains("count"))
                    {
                        continue;
                    }

                    string[] parts = report.defenceArmyBeforeBattle.keys[i].Split('_');
                    string result = string.Empty;

                    for (int j = 0; j < parts.Length - 1; j++)
                    {
                        result += parts[j] + ' ';
                    }
                    result = result.Trim();

                    defenderUnitNames.Add(result);
                    defenderBeforeCount.Add(report.defenceArmyBeforeBattle[i].i);
                    defenderAfterCount.Add(report.defenceArmyAfterBattle[i].i);
                }
            }
        }

        attackerUnitsController.unitNames = attackerUnitNames;
        attackerUnitsController.unitBeforeCount = attackerBeforeCount;
        attackerUnitsController.unitAfterCount = attackerAfterCount;

        defenderUnitsController.unitNames = defenderUnitNames;
        defenderUnitsController.unitBeforeCount = defenderBeforeCount;
        defenderUnitsController.unitAfterCount = defenderAfterCount;

        //resources and items
        if (report.isSuccessAttack)
        {
            attackerResourcesController.armyResources = new List<ResourcesModel>();
            attackerResourcesController.armyResources.Add(report.attackArmyResources);
            ResourcesModel defenderResources = report.defenceArmyResources;
            defenderResources.cooper = -defenderResources.cooper;
            defenderResources.food = -defenderResources.food;
            defenderResources.gold = -defenderResources.gold;
            defenderResources.iron = -defenderResources.iron;
            defenderResources.silver = -defenderResources.silver;
            defenderResources.stone = -defenderResources.stone;
            defenderResources.wood = -defenderResources.wood;
            defenderResourcesController.armyResources = new List<ResourcesModel>();
            defenderResourcesController.armyResources.Add(defenderResources);

            attackerItemsController.items = report.stolenCityTreasures;
        }
        else
        {
            attackerResourcesController.armyResources = new List<ResourcesModel>();
            attackerResourcesController.armyResources.Add(report.attackArmyResources);
            defenderResourcesController.armyResources = new List<ResourcesModel>();
            defenderResourcesController.armyResources.Add(report.defenceArmyResources);
        }

        //notify read
        StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.ReadBattleReport(report.id));
    }

    public void CloseReport()
    {
        GlobalGOScript.instance.windowInstanceManager.closeReport();
    }
}
