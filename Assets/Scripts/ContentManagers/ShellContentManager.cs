﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class ShellContentManager : MonoBehaviour {

    public Image icon;
    public TextLocalizer windowName;

    public GameObject content;
    public Sprite tabSelectedTop;
    public Sprite tabSelectedBottom;
    public Sprite tabUnselectedTop;
    public Sprite tabUnselectedBottom;

    public GameObject moreButton;
    public GameObject moreLeftArrow;
    public GameObject moreRightArrow;
    public GameObject[] tabs;
    public GameObject[] contents;
    public int selectedTabIndex;

    public List<string> keys;
    public List<Sprite> icons;

    public string[] tabsNames;

    //tabs
    public string[] actionTabsNames;
    public string[] shopTabsNames;
    public string[] treasureTabsNames;
    public string[] allianceTabsNames;
    public string[] mailTabsNames;
    public string[] chatTabsNames;
    public string[] genericBuildingTabsNames;
    public string[] commandCenterTabsNames;
    public string[] archeryTabsNames;
    public string[] enginneringTabsNames;
    public string[] godHouseTabsNames;
    public string[] militarySchoolTabsNames;
    public string[] stableTabsNames;
    public string[] dispatchTabsNames;
    public string[] residenceTabsNames;
    public string[] guestHouseTabsNames;
    public string[] feastingHallTabsNames;
    public string[] marketTabsNames;
    public string[] settingsTabsNames;

    //contents
    public GameObject[] actionContents;
    public GameObject[] shopContents;
    public GameObject[] treasureContents;
    public GameObject[] allianceContents;
    public GameObject[] mailContents;
    public GameObject[] chatContents;
    public GameObject[] genericBuildingsContents;
    public GameObject[] commandCenterContents;
    public GameObject[] archeryContents;
    public GameObject[] enginneringContents;
    public GameObject[] godHouseContents;
    public GameObject[] militarySchoolContents;
    public GameObject[] stableContents;
    public GameObject[] dispatchContents;
    public GameObject[] residenceContents;
    public GameObject[] guestHouseContents;
    public GameObject[] feastingHallContents;
    public GameObject[] marketContents;
    public GameObject[] settingsContents;

    public MonoBehaviour contentChanger;
	private string type;
	private long pitId;

    public void UpdateInfo(string name)
    {
        windowName.key = name;
        windowName.OnEnable();
        icon.sprite = icons[keys.IndexOf(name)];

        MonoBehaviour[] components = gameObject.GetComponents<MonoBehaviour>();
        foreach(MonoBehaviour component in components)
        {
            if (component.GetType().ToString().Contains("ContentChanger"))
                Destroy(component);
        }
         
        switch (name)
        {
            case "Action":
                SetTabs(actionTabsNames, actionContents);
                //gameObject.AddComponent<ActionManager>();
                break;
            case "Shop":
                SetTabs(shopTabsNames, shopContents);
                break;
            case "Treasure":
                SetTabs(treasureTabsNames, treasureContents);
                //gameObject.AddComponent<ActionManager>();
                break;
            case "Alliance":
                SetTabs(allianceTabsNames, allianceContents);
                //gameObject.AddComponent<AllianceContentChanger>();
                break;
            case "Mail":
                SetTabs(mailTabsNames, mailContents);
                //gameObject.AddComponent<ActionManager>();
                break;
            case "Chat":
                SetTabs(chatTabsNames, chatContents);
                //gameObject.AddComponent<ActionManager>();
                break;
            case "Settings":
                SetTabs(settingsTabsNames, settingsContents);
                //gameObject.AddComponent<ActionManager>();
                break;
        }
        if (moreButton != null)
        {
            moreButton.GetComponentInChildren<TextLocalizer>().key = "More";
            moreButton.GetComponentInChildren<TextLocalizer>().OnEnable();
            moreLeftArrow.SetActive(true);
            moreRightArrow.SetActive(false);
        }
        TabOnClick(0);
    }

    public void UpdateBuildingInfo(long pitId, string type)
    {
        string buildingConstantId = null;
		Debug.Log ("UPDATE BUILDING INFO, TYPE = " + type);
        switch (type)
        {
            case "Buildings":
                buildingConstantId = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitId].building;
                windowName.key = GlobalGOScript.instance.gameManager.buildingConstants[buildingConstantId].building_name.Replace("_", " ");
                break;
            case "Fields":
                buildingConstantId = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pitId].building;
                windowName.key = GlobalGOScript.instance.gameManager.buildingConstants[buildingConstantId].building_name.Replace("_", " ");
                break;
            case "World":
                buildingConstantId = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityValleys[pitId].cityType;
                windowName.key = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityValleys[pitId].cityType.Replace("_", " ");
                break;
            default:
                break;
        }
 
        windowName.OnEnable();
        icon.sprite = icons[keys.IndexOf(buildingConstantId)];

        MonoBehaviour[] components = gameObject.GetComponents<MonoBehaviour>();
        foreach(MonoBehaviour component in components)
        {
            if (component.GetType().ToString().Contains("ContentChanger"))
                Destroy(component);
        }

        switch (buildingConstantId)
        {
            case "BlackSmith":
            case "Furnace":
            case "House":
            case "Sport_Arena":
            case "Storage":
            case "Training_Ground":
            case "University":
            case "Hospital":
            case "Diplomacy":
            case "Grain_Mill":
            case "Saw_Mill":
            case "Iron_Mine":
            case "Copper_Mine":
            case "Quarry":
            case "Forest":
            case "Hill":
            case "Lake":
            case "Mountin_Iron":
            case "Mountin_Cooper":
            case "Wall":
                SetTabs(genericBuildingTabsNames, genericBuildingsContents);
                break;
            case "Command_Center":
                SetTabs(commandCenterTabsNames, commandCenterContents);
                break;
            case "Archery":
                SetTabs(archeryTabsNames, archeryContents);
                break;
            case "Engineering":
                SetTabs(enginneringTabsNames, enginneringContents);
                break;
            case "God_House":
                SetTabs(godHouseTabsNames, godHouseContents);
                break;
            case "Military_School":
                SetTabs(militarySchoolTabsNames, militarySchoolContents);
                break;
            case "Stable":
                SetTabs(stableTabsNames, stableContents);
                break;
			case "Mayor_Residence":
				Debug.Log ("MAYOR, tabs = " + residenceTabsNames.ToString() + ", contens = " + residenceContents.ToString());
                SetTabs(residenceTabsNames, residenceContents);
                break;
            case "Guest_House":
                SetTabs(guestHouseTabsNames, guestHouseContents);
                break;
            case "Feasting_Hall":
                SetTabs(feastingHallTabsNames, feastingHallContents);
                break;
            case "Market":
                SetTabs(marketTabsNames, marketContents);
                break;
            default:
                break;
        }
		TabOnClick(0);

        if (moreButton != null)
        {
            moreButton.GetComponentInChildren<TextLocalizer>().key = "More";
            moreButton.GetComponentInChildren<TextLocalizer>().OnEnable();
            moreLeftArrow.SetActive(true);
            moreRightArrow.SetActive(false);
        }
    }

    public void UpdateDispatchWindow(WorldFieldModel worldField, bool fromMap)
    {
        windowName.key = "Dispatch";
        windowName.OnEnable();
        icon.sprite = icons[keys.IndexOf("Dispatch")];
        SetTabs(dispatchTabsNames, dispatchContents);
        DispatchManager manager = dispatchContents[0].GetComponent<DispatchManager>();
        if (manager.fromMap = fromMap)
        {
            manager.targetField = worldField;
        }

        TabOnClick(0);
    }
    
	public void setType(string type){
		this.type = type;
	}

	public void setPitId(long pitId){
		this.pitId = pitId;
	}

	public void TabOnClick(int index)
    {
        Sprite tabSelected = index <= 3 ? tabSelectedTop : tabSelectedBottom;
        Sprite tabUnselected = selectedTabIndex <= 3 ? tabUnselectedTop : tabUnselectedBottom;

        tabs[selectedTabIndex].GetComponent<Image>().sprite = tabUnselected;
        tabs[index].GetComponent<Image>().sprite = tabSelected;
        
        Destroy(content);
        int indexCorrectedWithMore = index;
        if (moreButton.GetComponentInChildren<TextLocalizer>().key.Equals("Back"))
            indexCorrectedWithMore += 6;
        if (contents.Length > 0 && contents[indexCorrectedWithMore] != null)
        {
            content = Instantiate<GameObject>(contents[indexCorrectedWithMore]);
            content.transform.SetParent(transform);
            content.transform.localScale = Vector3.one;

            RectTransform contentRectTransform = content.GetComponent<RectTransform>();
            contentRectTransform.offsetMax = Vector3.zero;
            contentRectTransform.offsetMin = Vector3.zero;
            contentRectTransform.localPosition = new Vector3(contentRectTransform.localPosition.x, contentRectTransform.localPosition.y, 0f);
        }

        selectedTabIndex = index;
		if (index == 0) {
			//Debug.Log ("Type = " + type + ", ID = " + pitId);
			if (pitId != 0)
				content.GetComponent<GenericBuildingInfoContentManager>().SetInitialInfo(type, pitId);
		}
    }

    public void SetTabs(string[] tabsNames, GameObject[] contents)
    {
        this.tabsNames = tabsNames;
        this.contents = contents;
        foreach(GameObject tab in tabs)
            tab.SetActive(false);

        int visibleTabsCont = tabsNames.Length <= 6 ? tabsNames.Length : 6;
        for(int i = 0; i < visibleTabsCont; i++)
        {
            tabs[i].SetActive(true);
            TextLocalizer tabTextLocalizer = tabs[i].GetComponentInChildren<TextLocalizer>();
            tabTextLocalizer.key = tabsNames[i].Replace(" ", "");
            tabTextLocalizer.OnEnable();
        }
        moreButton.SetActive(tabsNames.Length > 6);
        if (tabsNames.Length == 1)
            tabs[0].SetActive(false);
    }

    public void MoreOnClick()
    {
        if(moreButton.GetComponentInChildren<TextLocalizer>().key.Equals("More"))
        {
            moreButton.GetComponentInChildren<TextLocalizer>().key = "Back";
            moreButton.GetComponentInChildren<TextLocalizer>().OnEnable();
            moreLeftArrow.SetActive(false);
            moreRightArrow.SetActive(true);
            foreach (GameObject tab in tabs)
                tab.SetActive(false);
            for(int i = 6; i < tabsNames.Length; i++)
            {
                tabs[i - 6].SetActive(true);
                TextLocalizer tabTextLocalizer = tabs[i - 6].GetComponentInChildren<TextLocalizer>();
                tabTextLocalizer.key = tabsNames[i];
                tabTextLocalizer.OnEnable();
            }
        }
        else
        {
            moreButton.GetComponentInChildren<TextLocalizer>().key = "More";
            moreButton.GetComponentInChildren<TextLocalizer>().OnEnable();
            moreLeftArrow.SetActive(true);
            moreRightArrow.SetActive(false);
            tabs[6].SetActive(false);
            for (int i = 0; i < 6; i++)
            {
                tabs[i].SetActive(true);
                TextLocalizer tabTextLocalizer = tabs[i].GetComponentInChildren<TextLocalizer>();
                tabTextLocalizer.key = tabsNames[i];
                tabTextLocalizer.OnEnable();
            }
        }
        TabOnClick(0);
    }
}
