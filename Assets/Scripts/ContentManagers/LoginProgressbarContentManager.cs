﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoginProgressbarContentManager : MonoBehaviour {

    public RectTransform progressTransform;
    private LoginManager loginManager;

    private long loadCount = 13;
    private float step;

	// Use this for initialization
	void Start () {
        step = 450.0f / loadCount;
        loginManager = GlobalGOScript.instance.playerManager.loginManager;
        progressTransform.offsetMax = new Vector2(-450.0f, -8.0f);
	}
	
	// Update is called once per frame
	void Update () {
        float offset = 0.0f;
        if (loginManager.gotBuildingConstants)
        {
            offset += step;
        }
        if (loginManager.gotCityBuildings)
        {
            offset += step;
        }
        if (loginManager.gotCityFields)
        {
            offset += step;
        }
        if (loginManager.gotCityValleys)
        {
            offset += step;
        }
        if (loginManager.gotCityGenerals)
        {
            offset += step;
        }
        if (loginManager.gotCityResources)
        {
            offset += step;
        }
        if (loginManager.gotCityUnits)
        {
            offset += step;
        }
        if (loginManager.gotPlayerCities)
        {
            offset += step;
        }
        if (loginManager.gotPlayerInfo)
        {
            offset += step;
        }
        if (loginManager.gotUnitConstants)
        {
            offset += step;
        }
        if (loginManager.gotCityTreasures)
        {
            offset += step;
        }
        progressTransform.offsetMax = new Vector2(-450.0f + offset, -8.0f);
	}
}
