﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DiplomacyContentManager : MonoBehaviour {

    public Toggle allowTroopsTGL;

    void OnEnable()
    {
        allowTroopsTGL.isOn = GlobalGOScript.instance.gameManager.cityManager.currentCity.reinforce_flag;
    }

    public void ToggleStateChnaged()
    {
        GlobalGOScript.instance.gameManager.cityManager.gotAllowTroopsState += AllowTroopsSet;
        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.SetAllowTroops(allowTroopsTGL.isOn));
    }

    public void AllowTroopsSet(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.gotAllowTroopsState -= AllowTroopsSet;

        if (string.Equals(value, "success"))
        {
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.UpdateCityAllowTroops());
        }
        else
        {
            allowTroopsTGL.isOn = !allowTroopsTGL.isOn;
        }
    }
}
