﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;

public class BattleReportContentManager : MonoBehaviour {

    //panels
    public GameObject attackColonizeDestroySection;
    public GameObject convertSection;
    public GameObject scoutSection;
    public GameObject reinforceTransportSection;

    public long reportId;
    public BattleReportModel report;

    private event SimpleEvent onGetBattleReport;

    private string dateFormat = "yyyy-MM-ddTHH:mm:ss.ffffffZ";

	void OnEnable () 
    {
        onGetBattleReport += GotBattleReport;
        StartCoroutine(GetBattleReport());
	}

    public void CloseReport()
    {
        GlobalGOScript.instance.windowInstanceManager.closeReport();
    }

    private IEnumerator GetBattleReport()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/reports/battleReportsDetails/" + reportId.ToString() + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
            onGetBattleReport -= GotBattleReport;
        }
        else {
            JSONObject temp = JSONObject.Create(request.downloadHandler.text);

            if (temp.IsString)
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification("Failed to fetch a report");
                onGetBattleReport -= GotBattleReport;
            }
            else
            {
                ParseBattleReport(temp);
            }
        }
    }

    private void ParseBattleReport(JSONObject battleReport)
    {
        report = JsonUtility.FromJson<BattleReportModel>(battleReport.Print());

        report.attackUser = JsonUtility.FromJson<UserModel>(battleReport["attackUser"].Print());

        if (!battleReport ["defenceUser"].IsNull) {
            if (battleReport ["defenceUser"].IsString) {
                report.defenceUser = new UserModel();
                report.defenceUser.username = "Barbarians";
            } else {
                report.defenceUser = JsonUtility.FromJson<UserModel>(battleReport ["defenceUser"].Print());
            }

            if (!battleReport ["defenceUser"] ["alliance"].IsNull) {
                report.defenceUser.alliance = JsonUtility.FromJson<AllianceModel>(battleReport ["defenceUser"]["alliance"].Print());
            }
        } 
        else 
        {
            report.defenceUser = new UserModel();
            report.defenceUser.experience = 0;
            report.defenceUser.flag = "none";
            report.defenceUser.rank = "none";
            //add other fields
        }

        if (!battleReport["attackUser"]["alliance"].IsNull)
        {
            report.attackUser.alliance = JsonUtility.FromJson<AllianceModel>(battleReport["attackUser"]["alliance"].Print());
        }

        report.attackersCity = JsonUtility.FromJson<MyCityModel>(battleReport["attackersCity"].Print());

        if (!battleReport["defendersCity"].IsNull)
        {
            report.defendersCity = JsonUtility.FromJson<MyCityModel>(battleReport["defendersCity"].Print());
            if (report.defendersCity.city_name.Contains("Barbarians"))
            {
                report.defendersCity.image_name = "WorldMapBuildings/Barbarians/1";
            }
            else
            {
                report.defendersCity.image_name = "UI/CityIcon/" + report.defendersCity.image_name;
            }
        }
        else
        {
            report.defendersCity = new MyCityModel();
            switch (battleReport["attackArmyBeforeBattle"]["destination_point"]["cityType"].str)
            {
                case "Hill":
                    report.defendersCity.city_name = "Hill";
                    report.defendersCity.image_name = "WorldMapBuildings/Hill/1";
                    break;
                case "Forest":
                    report.defendersCity.city_name = "Forest";
                    report.defendersCity.image_name = "WorldMapBuildings/Forest/1";
                    break;
                case "Lake":
                    report.defendersCity.city_name = "Lake";
                    report.defendersCity.image_name = "WorldMapBuildings/Lake/1";
                    break;
                case "Mountin_Iron":
                    report.defendersCity.city_name = "Mountain Iron";
                    report.defendersCity.image_name = "WorldMapBuildings/Mountin_Iron/1";
                    break;
                case "Mountin_Cooper":
                    report.defendersCity.city_name = "Mountain Copper";
                    report.defendersCity.image_name = "WorldMapBuildings/Mountin_Cooper/1";
                    break;
                case "Empty_Land":
                    report.defendersCity.city_name = "Empty Land";
                    report.defendersCity.image_name = "Buildings/Pit/1.png";
                    break;
                default:
                    break;
            }
        }

        report.attackArmyBeforeBattle = battleReport["attackArmyBeforeBattle"];

        if (battleReport.HasField("defenceArmyBeforeBattle"))
        {
            report.defenceArmyBeforeBattle = battleReport["defenceArmyBeforeBattle"];
        }

        report.attackArmyAfterBattle = battleReport["attackArmyAfterBattle"];

        if (battleReport["attackArmyAfterBattle"]["stolenCityItems"].Count != 0)
        {
            report.stolenCityTreasures = new List<CityTreasureModel>();
            JSONObject itemsArray = battleReport["attackArmyAfterBattle"]["stolenCityItems"];

            foreach (JSONObject item in itemsArray.list)
            {
                CityTreasureModel temp = JsonUtility.FromJson<CityTreasureModel>(item.Print());
                temp.item_constant = JsonUtility.FromJson<ShopItemModel>(item["item_constant"].Print());
                report.stolenCityTreasures.Add(temp);
            }
        }

        if (battleReport.HasField("defenceArmyAfterBattle"))
        {
            report.defenceArmyAfterBattle = battleReport["defenceArmyAfterBattle"];
        }

        if (!battleReport["attackArmyResources"].IsNull)
        {
            report.attackArmyResources = JsonUtility.FromJson<ResourcesModel>(battleReport["attackArmyResources"].Print());
        }

        if (!battleReport["defenceArmyResources"].IsNull)
        {
            report.defenceArmyResources = JsonUtility.FromJson<ResourcesModel>(battleReport["defenceArmyResources"].Print());
        }

        if (!battleReport["defendersResources"].IsNull)
        {
            report.defendersResources = JsonUtility.FromJson<ResourcesModel>(battleReport["defendersResources"].Print());
        }

        if (!battleReport["defendersCityItems"].IsNull)
        {
            report.defendersCityItems = new List<CityTreasureModel>();
            foreach (JSONObject item in battleReport["defendersCityItems"].list)
            {
                CityTreasureModel temp = JsonUtility.FromJson<CityTreasureModel>(item.Print());
                temp.item_constant = JsonUtility.FromJson<ShopItemModel>(item["item_constant"].Print());
            }
        }

        if (!battleReport["creation_date"].IsNull)
        {
            report.creation_date = DateTime.ParseExact(battleReport["creation_date"].str, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
        }

        if (onGetBattleReport != null)
        {
            onGetBattleReport(this, "success");
        }
    }

    public void GotBattleReport(object sender, string value)
    {
        onGetBattleReport -= GotBattleReport;

        if (string.Equals(value, "success"))
        {
            switch (report.attack_type)
            {
                case "attack":
                case "colonize":
                case "destroy":
                    attackColonizeDestroySection.GetComponent<AttackColonizeDestroyReportManager>().report = report;
                    attackColonizeDestroySection.SetActive(true);
                    break;
                case "convert":
                    convertSection.GetComponent<ConvertReportManager>().report = report;
                    convertSection.SetActive(true);
                    break;
                case "scout":
                    scoutSection.GetComponent<ScoutReportManager>().report = report;
                    scoutSection.SetActive(true);
                    break;
                case "reinforce":
                case "transport":
                    reinforceTransportSection.GetComponent<ReinforceTransportReportManager>().report = report;
                    reinforceTransportSection.SetActive(true);
                    break;
                default:
                    break;
            }
        }
    }

    public void OpenBattleLog()
    {
        GlobalGOScript.instance.windowInstanceManager.openBattleLog(report.log);
    }
}
