﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections;

public class ResidenceProductionContentManager : MonoBehaviour {
    //order 0-stone 1-copper 2-gold 3-food 4-wood 5-iron 6-silver

    public Text[] laborForces;
    public Text[] basicProduction;
    public Text[] coloniesTributes;
    public Text[] valleysProduction;
    public Text[] mayorPlus;
    public Text[] tributesPaid;
    public Text[] troopUpkeep;
    public Text[] netProduction;


    void OnEnable()
    {
        StartCoroutine(GetCityProduction());
    }

    public IEnumerator GetCityProduction()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/resources/getStatistics/"+ GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString() + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            if (obj.IsString)
            {
                //failed
                Debug.Log(request.downloadHandler.text);
            }
            else
            {
                //success
                if (obj.HasField("valleys"))
                {
                    for (int i = 0; i < 7; i++)
                    {
                        valleysProduction[i].text = ((int)(obj["valleys"].list[i].f)).ToString();
                    }
                }

                if (obj.HasField("sumProduction"))
                {
                    for (int i = 0; i < 7; i++)
                    {
                        netProduction[i].text = ((int)(obj["sumProduction"].list[i].f)).ToString();
                    }
                }

                if (obj.HasField("tributesPaid"))
                {
                    for (int i = 0; i < 7; i++)
                    {
                        tributesPaid[i].text = ((int)(obj["tributesPaid"].list[i].f)).ToString();
                    }
                }

                if (obj.HasField("troopsUpkeep"))
                {
                    for (int i = 0; i < 7; i++)
                    {
                        troopUpkeep[i].text = ((int)(obj["troopsUpkeep"].list[i].f)).ToString();
                    }
                }

                if (obj.HasField("mayerPlus"))
                {
                    for (int i = 0; i < 7; i++)
                    {
                        mayorPlus[i].text = ((int)(obj["mayerPlus"].list[i].f)).ToString();
                    }
                }

                if (obj.HasField("basic"))
                {
                    for (int i = 0; i < 7; i++)
                    {
                        basicProduction[i].text = ((int)(obj["basic"].list[i].f)).ToString();
                    }
                }

                if (obj.HasField("colonies"))
                {
                    for (int i = 0; i < 7; i++)
                    {
                        coloniesTributes[i].text = ((int)(obj["colonies"].list[i].f)).ToString();
                    }
                }
            }    
        }
    }
}
