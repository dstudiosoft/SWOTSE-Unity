﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NewAllianceContentManager : MonoBehaviour {

    public InputField allianceName;

    public void SendNewAllianceRequest()
    {
        if (!string.IsNullOrEmpty(allianceName.text))
        {
            GlobalGOScript.instance.playerManager.allianceManager.onCreateNewAlliance += RequestSent;
            StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.CreateNewAllliance(allianceName.text));
        }
    }

    public void RequestSent(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.allianceManager.onCreateNewAlliance -= RequestSent;
        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.closeNewAllianceNameWindow();
        }
        else
        {
            GlobalGOScript.instance.windowInstanceManager.openNotification("Failed to create new alliance");
        }
    }
}
