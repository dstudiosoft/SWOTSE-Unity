﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ActionManager : MonoBehaviour {

    public GameObject constructionPanel;
    public GameObject marchesPanel;
    public GameObject itemsPanel;
    public GameObject unitsPanel;

    /*public void OnEnable()
    {
        constructionPanel.SetActive(true);
        marchesPanel.SetActive(true);
        itemsPanel.SetActive(true);
        unitsPanel.SetActive(true);
    }*/

    public void closePanel (string name)
    {
        switch (name)
        {
            case "construction":
                constructionPanel.SetActive(false);
                break;
            case "marches":
                marchesPanel.SetActive(false);
                break;
            case "items":
                itemsPanel.SetActive(false);
                break;
            case "units":
                unitsPanel.SetActive(false);
                break;
            default:
                break;
        }

        if (!constructionPanel.activeInHierarchy && !marchesPanel.activeInHierarchy && !itemsPanel.activeInHierarchy && !unitsPanel.activeInHierarchy)
        {
            gameObject.SetActive(false);
        }
    }
}
