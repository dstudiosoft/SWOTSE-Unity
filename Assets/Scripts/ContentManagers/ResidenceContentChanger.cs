﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;

public class ResidenceContentChanger : MonoBehaviour {

    public Text contentLabel;
    public Text cityName;
    public InputField cityNameInput;
    public Image cityIcon;

    public GameObject buildingsContent;
    public GameObject productionContent;
    public GameObject citiesContent;
    public GameObject valleysContent;
    public GameObject coloniesContent;

    private long iconIndex;

	void OnEnable () 
    {
        closeAllContent();
        contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization.Buildings").ToUpper();
        buildingsContent.SetActive(true);
        iconIndex = long.Parse(GlobalGOScript.instance.gameManager.cityManager.currentCity.image_name);
        Sprite citySprite = Resources.Load<Sprite>("UI/CityIcon/" + GlobalGOScript.instance.gameManager.cityManager.currentCity.image_name);
        cityIcon.sprite = citySprite;
        cityName.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.city_name.ToUpper();
        cityNameInput.text = GlobalGOScript.instance.gameManager.cityManager.currentCity.city_name;
	}

    public void openContent(string name)
    {
        closeAllContent();

        switch (name)
        {
            case "Buildings":
                contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                buildingsContent.SetActive(true);
                break;

            case "Production":
                contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                productionContent.SetActive(true);
                break;

            case "Cities":
                contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                citiesContent.SetActive(true);
                break;

            case "Valleys":
                contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                valleysContent.SetActive(true);
                break;

            case "Colonies":
                contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + name).ToUpper();
                coloniesContent.SetActive(true);
                break;
            
            default:
                break;
        }
    }

    private void closeAllContent()
    {
        buildingsContent.SetActive(false);
        productionContent.SetActive(false);
        citiesContent.SetActive(false);
        valleysContent.SetActive(false);
        coloniesContent.SetActive(false);
    }

    public void SwitchIcon(bool forward)
    {
        Sprite citySprite;

        if (forward)
        {
            if (iconIndex == 19)
            {
                iconIndex = 1;
            }
            else
            {
                iconIndex++;
            }
        }
        else
        {
            if (iconIndex == 1)
            {
                iconIndex = 19;
            }
            else
            {
                iconIndex--;
            }
        }

        citySprite = Resources.Load<Sprite>("UI/CityIcon/" + iconIndex.ToString());

        cityIcon.sprite = citySprite;
    }

    public void ChangeNameAndIcon()
    {
        if (!string.IsNullOrEmpty(cityNameInput.text))
        {
            GlobalGOScript.instance.gameManager.cityManager.onCityNameAndIconChanged += NameAndIconChanged;
            StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.ChangeNameAndImage(cityNameInput.text, iconIndex.ToString()));
        }
    }

    public void NameAndIconChanged(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.onCityNameAndIconChanged -= NameAndIconChanged;
        if (string.Equals(value, "success"))
        {
            cityName.text = cityNameInput.text.ToUpper();
        }
    }
}
