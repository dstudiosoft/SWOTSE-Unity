﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;
using ArabicSupport;

public class HelpContentChanger : MonoBehaviour {

    public GameObject homeContent;
    public GameObject unitsContent;
    public GameObject hierarchyContent;
    public GameObject promotionContent;

    public GameObject engSteps;
    public GameObject arSteps;

    public Image homeButtonImage;
    public Image unitsButtonImage;
    public Image hierarchyButtonImage;
    public Image promotionButtonImage;

    public Sprite tabSelected;
    public Sprite tabUnselected;

    public void Start()
    {
        homeContent.SetActive(true);
        arSteps.SetActive(true);
        Text steps = arSteps.GetComponent<Text>();
        steps.text = ArabicFixer.Fix(steps.text, false, false);
        homeContent.SetActive(false);
        promotionContent.SetActive(true);
    }

    private void closeAllPanels()
    {
        homeContent.SetActive(false);
        unitsContent.SetActive(false);
        hierarchyContent.SetActive(false);
        promotionContent.SetActive(false);
    }

    private void resetAllSprites()
    {
        homeButtonImage.sprite = tabUnselected;
        unitsButtonImage.sprite = tabUnselected;
        hierarchyButtonImage.sprite = tabUnselected;
        promotionButtonImage.sprite = tabUnselected;
    }

    public void openPanel(string panel)
    {
        closeAllPanels();
        resetAllSprites();

        switch (panel)
        {
            case "home":
                homeContent.SetActive(true);
                if (LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
                {
                    arSteps.SetActive(true);
                    engSteps.SetActive(false);
                    homeContent.GetComponent<ScrollRect>().content = arSteps.GetComponent<RectTransform>();
                }
                else
                {
                    arSteps.SetActive(false);
                    engSteps.SetActive(true);
                    homeContent.GetComponent<ScrollRect>().content = arSteps.GetComponent<RectTransform>();
                }
                homeButtonImage.sprite = tabSelected;
                break;
            case "units":
                unitsContent.SetActive(true);
                unitsButtonImage.sprite = tabSelected;
                break;
            case "hierarchy":
                hierarchyContent.SetActive(true);
                hierarchyButtonImage.sprite = tabSelected;
                break;
            case "promotion":
                promotionContent.SetActive(true);
                promotionButtonImage.sprite = tabSelected;
                break;
            default:
                break;
        }
    }
}
