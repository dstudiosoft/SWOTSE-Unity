﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class CityDeedContentManager : MonoBehaviour {

    public Text totalBuildings;
    public Text buildingsHighestLevel;
    public Text totalFields;
    public Text fieldsHighestLevel;
    public Text totalUnits;
    public Dropdown citiesDropdown;
    public Text cityName;
    public Text cityCoords;
    public InputField cityPrice;
    public InputField buyerName;
    public InputField buyerEmail;
    public Image cityIcon;

    public IReloadable owner;
    public string item_id;

    private Dictionary<string, long> nameToId;
    private List<Dropdown.OptionData> citiesList;

    void OnEnable()
    {
        nameToId = new Dictionary<string, long>();

        foreach (MyCityModel city in GlobalGOScript.instance.gameManager.cityManager.myCities.Values)
        {
            nameToId.Add(city.city_name, city.id);
        }
            
        citiesList = new List<Dropdown.OptionData>();
        Dropdown.OptionData option;

        foreach (string name in nameToId.Keys)
        {
            option = new Dropdown.OptionData(name);
            citiesList.Add(option);
        }
            
        citiesDropdown.options = citiesList;

        StartCoroutine(GetExoticInfo());
        cityName.text = GlobalGOScript.instance.gameManager.cityManager.myCities[nameToId[citiesDropdown.captionText.text]].city_name;
        cityCoords.text = "(" + GlobalGOScript.instance.gameManager.cityManager.myCities[nameToId[citiesDropdown.captionText.text]].posX.ToString()
            + "," + GlobalGOScript.instance.gameManager.cityManager.myCities[nameToId[citiesDropdown.captionText.text]].posY.ToString() + ")";
        Sprite citySprite = Resources.Load<Sprite>("UI/CityIcon/" + GlobalGOScript.instance.gameManager.cityManager.myCities[nameToId[citiesDropdown.captionText.text]].image_name);
        cityIcon.sprite = citySprite;
    }

    public IEnumerator GetExoticInfo()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://"+GlobalGOScript.instance.host+":8889/cities/exoticCities/" + citiesDropdown.captionText.text + "/");
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {

            JSONObject cityInfo = JSONObject.Create(request.downloadHandler.text);

            totalBuildings.text = cityInfo["cityBuildingsCount"].i.ToString();
            totalFields.text = cityInfo["fieldsBuildingsCount"].i.ToString();
            buildingsHighestLevel.text = cityInfo["maxCityBuildingLevel"].i.ToString();
            fieldsHighestLevel.text = cityInfo["maxFieldsBuildingLevel"].i.ToString();
            totalUnits.text = cityInfo["unitsCount"].i.ToString();
        }
    }

    public void DropdownValueChanged()
    {
        StartCoroutine(GetExoticInfo());
        cityName.text = GlobalGOScript.instance.gameManager.cityManager.myCities[nameToId[citiesDropdown.captionText.text]].city_name;
        cityCoords.text = "(" + GlobalGOScript.instance.gameManager.cityManager.myCities[nameToId[citiesDropdown.captionText.text]].posX.ToString()
            + "," + GlobalGOScript.instance.gameManager.cityManager.myCities[nameToId[citiesDropdown.captionText.text]].posY.ToString() + ")";
        Sprite citySprite = Resources.Load<Sprite>("UI/CityIcon/" + GlobalGOScript.instance.gameManager.cityManager.myCities[nameToId[citiesDropdown.captionText.text]].image_name);
        cityIcon.sprite = citySprite;
    }

    public void Activate()
    {
        
    }
}
