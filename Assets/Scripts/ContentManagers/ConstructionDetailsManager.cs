﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class ConstructionDetailsManager : MonoBehaviour {

    public ConstructionPitManager pitManager;

    public GameObject cityPanel;
    public GameObject fieldsPanel;

    public GameObject feastingHall;
    public GameObject market;
    public GameObject guestHouse;
    public GameObject storage;
    public GameObject furnace;
    public GameObject blackSmith;
    public GameObject hospital;
    public GameObject godHouse;
    public GameObject commandCenter;
    public GameObject diplomacy;
    public GameObject trainingGround;
    public GameObject sportArena;
    public GameObject univercity;
    public GameObject militarySchool;
    public GameObject archery;
    public GameObject stable;
    public GameObject engineering;

    public void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.IsBuildingPresent("House"))
            {
                EnableBuilding(feastingHall);
                EnableBuilding(market);
            }

            if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.IsBuildingPresent("Market"))
            {
                EnableBuilding(storage);
                EnableBuilding(furnace);
                EnableBuilding(blackSmith);
            }

            if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.IsBuildingPresent("Feasting_Hall"))
            {
                EnableBuilding(guestHouse);
            }

            if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.IsBuildingPresent("Guest_House"))
            {
                EnableBuilding(hospital);
                EnableBuilding(godHouse);
                EnableBuilding(commandCenter);
            }

            if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.IsBuildingPresent("Command_Center"))
            {
                EnableBuilding(diplomacy);
                EnableBuilding(trainingGround);
                EnableBuilding(sportArena);
                EnableBuilding(univercity);
            }

            if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.IsBuildingPresent("Training_Ground"))
            {
                EnableBuilding(militarySchool);
                EnableBuilding(archery);
            }

            if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.IsBuildingPresent("Sport_Arena"))
            {
                EnableBuilding(stable);
            }

            if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.IsBuildingPresent("University"))
            {
                EnableBuilding(engineering);
            }

            cityPanel.SetActive(true);
        }
        else if (SceneManager.GetActiveScene().buildIndex == 3)
        {
            fieldsPanel.SetActive(true);
        }
    }

    public void Build(string buildingName)
    {
        GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate += HandleBuildingRequested;
        GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate += pitManager.UpdatePitState;

        long city_id = GlobalGOScript.instance.gameManager.cityManager.currentCity.id;

        string location = "city";

        if (cityPanel.activeInHierarchy)
        {
            location = "city";
        }
        else
        {
            location = "fields";
        }

        StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.AddBuilding(buildingName, city_id, pitManager.pitId, location));
    }

    public void HandleBuildingRequested(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate -= HandleBuildingRequested;

        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.closeBuildingConstruction();
        }
    }

    private void EnableBuilding(GameObject building)
    {
        building.GetComponent<Image>().color = Color.white;
        building.GetComponent<Button>().enabled = true;
    }
}
