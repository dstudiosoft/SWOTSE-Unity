﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;
using ArabicSupport;

public class DispatchManager : MonoBehaviour {

    public GameObject Page1;
    public GameObject Page2;

    public Text cityFood;
    public Text cityWood;
    public Text cityIron;
    public Text cityStone;
    public Text cityCopper;
    public Text citySilver;
    public Text cityGold;

    public Text cityWorker;
    public Text citySpy;
    public Text citySwordsman;
    public Text citySpearman;
    public Text cityPikeman;
    public Text cityScoutRider;
    public Text cityLightCavalry;
    public Text cityHeavyCavalry;
    public Text cityArcher;
    public Text cityArcherRider;
    public Text cityHollyman;
    public Text cityWagon;
    public Text cityTrebuchet;
    public Text citySiegeTower;
    public Text cityBatteringRam;
    public Text cityBallista;

    public Text cityWorkerName;
    public Text citySpyName;
    public Text citySwordsmanName;
    public Text citySpearmanName;
    public Text cityPikemanName;
    public Text cityScoutRiderName;
    public Text cityLightCavalryName;
    public Text cityHeavyCavalryName;
    public Text cityArcherName;
    public Text cityArcherRiderName;
    public Text cityHollymanName;
    public Text cityWagonName;
    public Text cityTrebuchetName;
    public Text citySiegeTowerName;
    public Text cityBatteringRamName;
    public Text cityBallistaName;

    public InputField dispatchFood;
    public InputField dispatchWood;
    public InputField dispatchIron;
    public InputField dispatchStone;
    public InputField dispatchCopper;
    public InputField dispatchSilver;
    public InputField dispatchGold;

    public InputField dispatchWorker;
    public InputField dispatchSpy;
    public InputField dispatchSwordsman;
    public InputField dispatchSpearman;
    public InputField dispatchPikeman;
    public InputField dispatchScoutRider;
    public InputField dispatchLightCavalry;
    public InputField dispatchHeavyCavalry;
    public InputField dispatchArcher;
    public InputField dispatchArcherRider;
    public InputField dispatchHollyman;
    public InputField dispatchWagon;
    public InputField dispatchTrebuchet;
    public InputField dispatchSiegeTower;
    public InputField dispatchBatteringRam;
    public InputField dispatchBallista;

    public Dropdown attackType;
    public Dropdown knights;

    public InputField dispatchHour;
    public InputField dispatchMinute;

    public Text xCoord;
    public Text yCoord;
    public InputField xInput;
    public InputField yInput;

    public InputField foodPrice;
    public InputField loadVacancy;
    public InputField tripTime;

    public bool fromMap;
    public WorldFieldModel targetField;

    private List<ArmyGeneralModel> generals;
    private UnitsModel army;
    private ResourcesModel resources;
    private bool calculated = false;
    private event SimpleEvent onGotParams;

    private List<string> attackTypes = new List<string>() {"attack", "colonize", "convert", "reinforce", "transport", "scout", "destroy"};

    void OnEnable()
    {
        if (fromMap)
        {
            xCoord.gameObject.SetActive(true);
            yCoord.gameObject.SetActive(true);
            xInput.gameObject.SetActive(false);
            yInput.gameObject.SetActive(false);

            xCoord.text = targetField.posX.ToString();
            yCoord.text = targetField.posY.ToString();
        }
        else
        {
            xCoord.gameObject.SetActive(false);
            yCoord.gameObject.SetActive(false);
            xInput.gameObject.SetActive(true);
            yInput.gameObject.SetActive(true);
        }

        ResourcesModel resources = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources;
        UnitsModel cityUnits = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits;

        cityWorkerName.text = GlobalGOScript.instance.gameManager.unitConstants["Worker"].unit_name.Replace('_', ' ');
        citySpyName.text = GlobalGOScript.instance.gameManager.unitConstants["Spy"].unit_name.Replace('_', ' ');
        citySwordsmanName.text = GlobalGOScript.instance.gameManager.unitConstants["Swards_Man"].unit_name.Replace('_', ' ');
        citySpearmanName.text = GlobalGOScript.instance.gameManager.unitConstants["Spear_Man"].unit_name.Replace('_', ' ');
        cityPikemanName.text = GlobalGOScript.instance.gameManager.unitConstants["Pike_Man"].unit_name.Replace('_', ' ');
        cityScoutRiderName.text = GlobalGOScript.instance.gameManager.unitConstants["Scout_Rider"].unit_name.Replace('_', ' ');
        cityLightCavalryName.text = GlobalGOScript.instance.gameManager.unitConstants["Light_Cavalry"].unit_name.Replace('_', ' ');
        cityHeavyCavalryName.text = GlobalGOScript.instance.gameManager.unitConstants["Heavy_Cavalry"].unit_name.Replace('_', ' ');
        cityArcherName.text = GlobalGOScript.instance.gameManager.unitConstants["Archer"].unit_name.Replace('_', ' ');
        cityArcherRiderName.text = GlobalGOScript.instance.gameManager.unitConstants["Archer_Rider"].unit_name.Replace('_', ' ');
        cityHollymanName.text = GlobalGOScript.instance.gameManager.unitConstants["Holly_Man"].unit_name.Replace('_', ' ');
        cityWagonName.text = GlobalGOScript.instance.gameManager.unitConstants["Wagon"].unit_name.Replace('_', ' ');
        cityTrebuchetName.text = GlobalGOScript.instance.gameManager.unitConstants["Trebuchet"].unit_name.Replace('_', ' ');
        citySiegeTowerName.text = GlobalGOScript.instance.gameManager.unitConstants["Siege_Towers"].unit_name.Replace('_', ' ');
        cityBatteringRamName.text = GlobalGOScript.instance.gameManager.unitConstants["Battering_Ram"].unit_name.Replace('_', ' ');
        cityBallistaName.text = GlobalGOScript.instance.gameManager.unitConstants["Ballista"].unit_name.Replace('_', ' ');

        cityFood.text = resources.food.ToString();
        cityWood.text = resources.wood.ToString();
        cityStone.text = resources.stone.ToString();
        cityIron.text = resources.iron.ToString();
        cityCopper.text = resources.cooper.ToString();
        citySilver.text = resources.silver.ToString();
        cityGold.text = resources.gold.ToString();

        cityWorker.text = cityUnits.workers_count.ToString();
        citySpy.text = cityUnits.spy_count.ToString();
        citySwordsman.text = cityUnits.swards_man_count.ToString();
        citySpearman.text = cityUnits.spear_man_count.ToString();
        cityPikeman.text = cityUnits.pike_man_count.ToString();
        cityScoutRider.text = cityUnits.scout_rider_count.ToString();
        cityLightCavalry.text = cityUnits.light_cavalry_count.ToString();
        cityHeavyCavalry.text = cityUnits.heavy_cavalry_count.ToString();
        cityArcher.text = cityUnits.archer_count.ToString();
        cityArcherRider.text = cityUnits.archer_rider_count.ToString();
        cityHollyman.text = cityUnits.holly_man_count.ToString();
        cityWagon.text = cityUnits.wagon_count.ToString();
        cityTrebuchet.text = cityUnits.trebuchet_count.ToString();
        citySiegeTower.text = cityUnits.siege_towers_count.ToString();
        cityBatteringRam.text = cityUnits.battering_ram_count.ToString();
        cityBallista.text = cityUnits.ballista_count.ToString();

        dispatchFood.text = 0.ToString();
        dispatchWood.text = 0.ToString();
        dispatchStone.text = 0.ToString();
        dispatchIron.text = 0.ToString();
        dispatchCopper.text = 0.ToString();
        dispatchSilver.text = 0.ToString();
        dispatchGold.text = 0.ToString();

        dispatchWorker.text = 0.ToString();
        dispatchSpy.text = 0.ToString();
        dispatchSwordsman.text = 0.ToString();
        dispatchSpearman.text = 0.ToString();
        dispatchPikeman.text = 0.ToString();
        dispatchScoutRider.text = 0.ToString();
        dispatchLightCavalry.text = 0.ToString();
        dispatchHeavyCavalry.text = 0.ToString();
        dispatchArcher.text = 0.ToString();
        dispatchArcherRider.text = 0.ToString();
        dispatchHollyman.text = 0.ToString();
        dispatchWagon.text = 0.ToString();
        dispatchTrebuchet.text = 0.ToString();
        dispatchSiegeTower.text = 0.ToString();
        dispatchBatteringRam.text = 0.ToString();
        dispatchBallista.text = 0.ToString();

        generals = new List<ArmyGeneralModel>();

        foreach (ArmyGeneralModel general in GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityGenerals.Values)
        {
            if (string.Equals(general.status, "passive"))
            {
                generals.Add(general);
            }
        }

        List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();

        foreach (ArmyGeneralModel general in generals)
        {
            Dropdown.OptionData option = new Dropdown.OptionData(general.name);
            options.Add(option);
        }

        knights.options = options;

        List<string> localizedAttackTypes = new List<string>();

        foreach (string type in attackTypes)
        {
            string localizedValue = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + type);

            if (LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
            {
                localizedValue = ArabicFixer.Fix(localizedValue);
            }

            localizedAttackTypes.Add(localizedValue);
        }

        attackType.AddOptions(localizedAttackTypes);
    }

    void FixedUpdate()
    {
        ResourcesModel resources = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources;
        UnitsModel cityUnits = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits;

        cityFood.text = resources.food.ToString();
        cityWood.text = resources.wood.ToString();
        cityStone.text = resources.stone.ToString();
        cityIron.text = resources.iron.ToString();
        cityCopper.text = resources.cooper.ToString();
        citySilver.text = resources.silver.ToString();
        cityGold.text = resources.gold.ToString();

        cityWorker.text = cityUnits.workers_count.ToString();
        citySpy.text = cityUnits.spy_count.ToString();
        citySwordsman.text = cityUnits.swards_man_count.ToString();
        citySpearman.text = cityUnits.spear_man_count.ToString();
        cityPikeman.text = cityUnits.pike_man_count.ToString();
        cityScoutRider.text = cityUnits.scout_rider_count.ToString();
        cityLightCavalry.text = cityUnits.light_cavalry_count.ToString();
        cityHeavyCavalry.text = cityUnits.heavy_cavalry_count.ToString();
        cityArcher.text = cityUnits.archer_count.ToString();
        cityArcherRider.text = cityUnits.archer_rider_count.ToString();
        cityHollyman.text = cityUnits.holly_man_count.ToString();
        cityWagon.text = cityUnits.wagon_count.ToString();
        cityTrebuchet.text = cityUnits.trebuchet_count.ToString();
        citySiegeTower.text = cityUnits.siege_towers_count.ToString();
        cityBatteringRam.text = cityUnits.battering_ram_count.ToString();
        cityBallista.text = cityUnits.ballista_count.ToString();
    }

    public void March()
    {
        if (!calculated)
        {
            return;
        }

        long value;
        army = new UnitsModel();
        if (long.TryParse(dispatchWorker.text, out value))
        {
            army.workers_count = value;
        }
        if (long.TryParse(dispatchSpy.text, out value))
        {
            army.spy_count = value;
        }
        if (long.TryParse(dispatchSwordsman.text, out value))
        {
            army.swards_man_count = value;
        }
        if (long.TryParse(dispatchSpearman.text, out value))
        {
            army.spear_man_count = value;
        }
        if (long.TryParse(dispatchPikeman.text, out value))
        {
            army.pike_man_count = value;
        }
        if (long.TryParse(dispatchScoutRider.text, out value))
        {
            army.scout_rider_count = value;
        }
        if (long.TryParse(dispatchLightCavalry.text, out value))
        {
            army.light_cavalry_count = value;
        }
        if (long.TryParse(dispatchHeavyCavalry.text, out value))
        {
            army.heavy_cavalry_count = value;
        }
        if (long.TryParse(dispatchArcher.text, out value))
        {
            army.archer_count = value;
        }
        if (long.TryParse(dispatchArcherRider.text, out value))
        {
            army.archer_rider_count = value;
        }
        if (long.TryParse(dispatchHollyman.text, out value))
        {
            army.holly_man_count = value;
        }
        if (long.TryParse(dispatchWagon.text, out value))
        {
            army.wagon_count = value;
        }
        if (long.TryParse(dispatchTrebuchet.text, out value))
        {
            army.trebuchet_count = value;
        }
        if (long.TryParse(dispatchSiegeTower.text, out value))
        {
            army.siege_towers_count = value;
        }
        if (long.TryParse(dispatchBatteringRam.text, out value))
        {
            army.battering_ram_count = value;
        }
        if (long.TryParse(dispatchBallista.text, out value))
        {
            army.ballista_count = value;
        }
        army.city = GlobalGOScript.instance.gameManager.cityManager.currentCity.id;

        resources = new ResourcesModel();
        if (long.TryParse(dispatchFood.text, out value))
        {
            resources.food = value;
        }
        if (long.TryParse(dispatchWood.text, out value))
        {
            resources.wood = value;
        }
        if (long.TryParse(dispatchStone.text, out value))
        {
            resources.stone = value;
        }
        if (long.TryParse(dispatchIron.text, out value))
        {
            resources.iron = value;
        }
        if (long.TryParse(dispatchCopper.text, out value))
        {
            resources.cooper = value;
        }
        if (long.TryParse(dispatchSilver.text, out value))
        {
            resources.silver = value;
        }
        if (long.TryParse(dispatchGold.text, out value))
        {
            resources.gold = value;
        }

        GlobalGOScript.instance.gameManager.battleManager.onMarchLaunched += MarchLaunched;

        if (knights.options.Count != 0)
        {
            if (fromMap)
            {
                StartCoroutine(GlobalGOScript.instance.gameManager.battleManager.March(army, resources, attackTypes[attackType.value].ToLower(), targetField.posX, targetField.posY, generals[knights.value].id));
            }
            else
            {
                StartCoroutine(GlobalGOScript.instance.gameManager.battleManager.March(army, resources, attackTypes[attackType.value].ToLower(), long.Parse(xInput.text), long.Parse(yInput.text), generals[knights.value].id));
            }
        }
    }

    public void MarchLaunched(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.battleManager.onMarchLaunched -= MarchLaunched;

        if (string.Equals(value, "success"))
        {
            UnitsModel cityArmy = GlobalGOScript.instance.gameManager.cityManager.unitsManager.cityUnits;
            cityArmy.archer_count -= army.archer_count;
            cityArmy.archer_rider_count -= army.archer_rider_count;
            cityArmy.ballista_count -= army.ballista_count;
            cityArmy.battering_ram_count -= army.battering_ram_count;
            cityArmy.heavy_cavalry_count -= army.heavy_cavalry_count;
            cityArmy.holly_man_count -= army.holly_man_count;
            cityArmy.light_cavalry_count -= army.light_cavalry_count;
            cityArmy.pike_man_count -= army.pike_man_count;
            cityArmy.scout_rider_count -= army.scout_rider_count;
            cityArmy.siege_towers_count -= army.siege_towers_count;
            cityArmy.spear_man_count -= army.spear_man_count;
            cityArmy.spy_count -= army.spy_count;
            cityArmy.swards_man_count -= army.swards_man_count;
            cityArmy.trebuchet_count -= army.trebuchet_count;
            cityArmy.wagon_count -= army.wagon_count;
            cityArmy.workers_count -= army.workers_count;

//            knights.options.RemoveAt(knights.value);
//            knights.RefreshShownValue();

            GlobalGOScript.instance.windowInstanceManager.closeDispatch();
        }
    }

    public void RefreshAttackParams()
    {
        long value;
        army = new UnitsModel();
        if (long.TryParse(dispatchWorker.text, out value))
        {
            army.workers_count = value;
        }
        if (long.TryParse(dispatchSpy.text, out value))
        {
            army.spy_count = value;
        }
        if (long.TryParse(dispatchSwordsman.text, out value))
        {
            army.swards_man_count = value;
        }
        if (long.TryParse(dispatchSpearman.text, out value))
        {
            army.spear_man_count = value;
        }
        if (long.TryParse(dispatchPikeman.text, out value))
        {
            army.pike_man_count = value;
        }
        if (long.TryParse(dispatchScoutRider.text, out value))
        {
            army.scout_rider_count = value;
        }
        if (long.TryParse(dispatchLightCavalry.text, out value))
        {
            army.light_cavalry_count = value;
        }
        if (long.TryParse(dispatchHeavyCavalry.text, out value))
        {
            army.heavy_cavalry_count = value;
        }
        if (long.TryParse(dispatchArcher.text, out value))
        {
            army.archer_count = value;
        }
        if (long.TryParse(dispatchArcherRider.text, out value))
        {
            army.archer_rider_count = value;
        }
        if (long.TryParse(dispatchHollyman.text, out value))
        {
            army.holly_man_count = value;
        }
        if (long.TryParse(dispatchWagon.text, out value))
        {
            army.wagon_count = value;
        }
        if (long.TryParse(dispatchTrebuchet.text, out value))
        {
            army.trebuchet_count = value;
        }
        if (long.TryParse(dispatchSiegeTower.text, out value))
        {
            army.siege_towers_count = value;
        }
        if (long.TryParse(dispatchBatteringRam.text, out value))
        {
            army.battering_ram_count = value;
        }
        if (long.TryParse(dispatchBallista.text, out value))
        {
            army.ballista_count = value;
        }
        army.city = GlobalGOScript.instance.gameManager.cityManager.currentCity.id;

        resources = new ResourcesModel();
        if (long.TryParse(dispatchFood.text, out value))
        {
            resources.food = value;
        }
        if (long.TryParse(dispatchWood.text, out value))
        {
            resources.wood = value;
        }
        if (long.TryParse(dispatchStone.text, out value))
        {
            resources.stone = value;
        }
        if (long.TryParse(dispatchIron.text, out value))
        {
            resources.iron = value;
        }
        if (long.TryParse(dispatchCopper.text, out value))
        {
            resources.cooper = value;
        }
        if (long.TryParse(dispatchSilver.text, out value))
        {
            resources.silver = value;
        }
        if (long.TryParse(dispatchGold.text, out value))
        {
            resources.gold = value;
        }

        GlobalGOScript.instance.gameManager.battleManager.onMarchLaunched += MarchLaunched;

        if (knights.options.Count != 0)
        {
            if (fromMap)
            {
                onGotParams += GotParams;
                StartCoroutine(GetAttackParams(army, resources, attackTypes[attackType.value].ToLower(), targetField.posX, targetField.posY, generals[knights.value].id));
            }
            else
            {
                onGotParams += GotParams;
                StartCoroutine(GetAttackParams(army, resources, attackTypes[attackType.value].ToLower(), long.Parse(xInput.text), long.Parse(yInput.text), generals[knights.value].id));
            }

            string localizedValue = string.Empty;

            switch (attackType.value)
            {
                case 1:
                    localizedValue = LanguageManager.Instance.GetTextValue("SWOTSELocalization.ColonizeDescription");
                    break;
                case 2:
                    localizedValue = LanguageManager.Instance.GetTextValue("SWOTSELocalization.ConvertDescription");
                    break;
                case 5:
                    localizedValue = LanguageManager.Instance.GetTextValue("SWOTSELocalization.ScoutDescription");
                    break;
                case 6:
                    localizedValue = LanguageManager.Instance.GetTextValue("SWOTSELocalization.DestroyDescription");
                    break;
                default:
                    break;
            }

            if (!string.IsNullOrEmpty(localizedValue))
            {
                if (LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
                {
                    localizedValue = ArabicFixer.Fix(localizedValue, false, false);
                }
                
                GlobalGOScript.instance.windowInstanceManager.openNotification(localizedValue);
            }
        }
    }

    public IEnumerator GetAttackParams(UnitsModel army, ResourcesModel resources, string attcakType, long destX, long destY, long knightId)
    {
        WWWForm form = new WWWForm();
        form.AddField("army_status", attcakType);
        //        form.AddField("destination_city", destinationCity.ToString());
        form.AddField("cityID", army.city.ToString());
        form.AddField("workers_count", army.workers_count.ToString());
        form.AddField("spy_count", army.spy_count.ToString());
        form.AddField("swards_man_count", army.swards_man_count.ToString());
        form.AddField("spear_man_count", army.spear_man_count.ToString());
        form.AddField("pike_man_count", army.pike_man_count.ToString());
        form.AddField("scout_rider_count", army.scout_rider_count.ToString());
        form.AddField("light_cavalry_count", army.light_cavalry_count.ToString());
        form.AddField("heavy_cavalry_count", army.heavy_cavalry_count.ToString());
        form.AddField("archer_count", army.archer_count.ToString());
        form.AddField("archer_rider_count", army.archer_rider_count.ToString());
        form.AddField("holly_man_count", army.holly_man_count.ToString());
        form.AddField("trebuchet_count", army.trebuchet_count.ToString());
        form.AddField("siege_towers_count", army.siege_towers_count.ToString());
        form.AddField("battering_ram_count", army.battering_ram_count.ToString());
        form.AddField("ballista_count", army.ballista_count.ToString());
		form.AddField("wagon_count", army.wagon_count.ToString());
        form.AddField("food", resources.food.ToString());
        form.AddField("wood", resources.wood.ToString());
        form.AddField("stone", resources.stone.ToString());
        form.AddField("iron", resources.iron.ToString());
        form.AddField("cooper", resources.cooper.ToString());
        form.AddField("silver", resources.silver.ToString());
        form.AddField("gold", resources.gold.ToString());
        form.AddField("posX", destX.ToString());
        form.AddField("posY", destY.ToString());
        form.AddField("hero", knightId.ToString());

        UnityWebRequest request = UnityWebRequest.Post("http://" + GlobalGOScript.instance.host + ":8889/army/getAttackData/", form);
        request.SetRequestHeader("authorization", "Token " + GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            JSONObject obj = JSONObject.Create(request.downloadHandler.text);
            Debug.Log(request.downloadHandler.text);
            if (obj.IsString)
            {
                GlobalGOScript.instance.windowInstanceManager.openNotification(obj.str);
            }
            else
            {
                foodPrice.text = obj["foodRequired"].i.ToString();
                tripTime.text = obj["armyMarchTime"].i.ToString();
                loadVacancy.text = obj["armyCapacity"].i.ToString();
                dispatchFood.text = obj["foodRequired"].i.ToString();
                calculated = true;

                if (onGotParams != null)
                {
                    onGotParams(this, "success");
                }
            }
        }
    }

    private void GotParams(object sender, string value)
    {
        onGotParams -= GotParams;

        if (string.Equals(value, "success"))
        {
            Page1.SetActive(false);
            Page2.SetActive(true);
        }
    }

    public void GoBack()
    {
        Page1.SetActive(true);
        Page2.SetActive(false);
    }
}
