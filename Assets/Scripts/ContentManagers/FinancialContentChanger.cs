﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using SmartLocalization;
using ArabicSupport;

public class FinancialContentChanger : MonoBehaviour
{

    public Text contentLabel;
    public Text shillingsCount;
    public Text totalPurchases;
    public Text totalLabel;
    public Text tableColumn;
    public Sprite tabSelectedSprite;
    public Sprite tabUnselectedSprite;
    public Image[] tabs;

    private ArrayList boughtItems;
    private ArrayList soldItems;
    private ArrayList offeredItems;

    private event SimpleEvent gotOffers;
    private event SimpleEvent offerCanceled;

    public FinanceTableController tableController;
    public string content;

    public void Start()
    {
        content = "Offers";
        gotOffers += OnGetOffers;
        StartCoroutine(GetFinanceOffers());
        StartCoroutine(GetFinanceReports());

        if (string.Equals(LanguageManager.Instance.CurrentlyLoadedCulture.languageCode, "ar"))
        {
            contentLabel.text = ArabicFixer.Fix(LanguageManager.Instance.GetTextValue("SWOTSELocalization.Offers")).ToUpper();
            totalLabel.text = ArabicFixer.Fix(LanguageManager.Instance.GetTextValue("SWOTSELocalization.TotalOffers")).ToUpper();
            tableColumn.text = ArabicFixer.Fix(LanguageManager.Instance.GetTextValue("SWOTSELocalization.From")).ToUpper();
        }
        else
        {
            contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization.Offers").ToUpper();
            totalLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization.TotalOffers").ToUpper();
            tableColumn.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization.From").ToUpper();
        
        }
    }

    void FixedUpdate()
    {
        shillingsCount.text = GlobalGOScript.instance.playerManager.user.shillings.ToString();
    }

    public void openPanel(string panel)
    {
        long total = 0;
        content = panel;

        if (string.Equals(LanguageManager.Instance.CurrentlyLoadedCulture.languageCode, "ar"))
        {
            contentLabel.text = ArabicFixer.Fix(LanguageManager.Instance.GetTextValue("SWOTSELocalization." + panel)).ToUpper();
        }
        else
        {
            contentLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + panel).ToUpper();
        }

        foreach (Image tab in tabs)
            tab.sprite = tabUnselectedSprite;
        
        switch (panel)
        {
            case "Offers":
                tabs[0].sprite = tabSelectedSprite;
                totalPurchases.text = "0";
                if (offeredItems != null)
                {
                    tableController.currentSourceArray = offeredItems;
                    total = 0;
                    foreach (FinanceReportModel report in offeredItems)
                    {
                        total += report.price;
                    }
                    totalPurchases.text = total.ToString();
                }
                if (string.Equals(LanguageManager.Instance.CurrentlyLoadedCulture.languageCode, "ar"))
                {
                    totalLabel.text = ArabicFixer.Fix(LanguageManager.Instance.GetTextValue("SWOTSELocalization.TotalOffers")).ToUpper();
                    tableColumn.text = ArabicFixer.Fix(LanguageManager.Instance.GetTextValue("SWOTSELocalization.From")).ToUpper();
                }
                else
                {
                    totalLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization.TotalOffers").ToUpper();
                    tableColumn.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization.From").ToUpper();
                }
                tableController.m_tableView.ReloadData();
                break;

            case "Sales":
                tabs[1].sprite = tabSelectedSprite;
                totalPurchases.text = "0";
                if (soldItems != null)
                {
                    tableController.currentSourceArray = soldItems;
                    total = 0;
                    foreach (FinanceReportModel report in soldItems)
                    {
                        total += report.price;
                    }
                    totalPurchases.text = total.ToString();
                }
                if (string.Equals(LanguageManager.Instance.CurrentlyLoadedCulture.languageCode, "ar"))
                {
                    totalLabel.text = ArabicFixer.Fix(LanguageManager.Instance.GetTextValue("SWOTSELocalization.TotalSales")).ToUpper();
                    tableColumn.text = ArabicFixer.Fix(LanguageManager.Instance.GetTextValue("SWOTSELocalization.To")).ToUpper();
                }
                else
                {
                    totalLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization.TotalSales").ToUpper();
                    tableColumn.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization.To").ToUpper();
                }
                tableController.m_tableView.ReloadData();
                break;

            case "Purchases":
                tabs[2].sprite = tabSelectedSprite;
                totalPurchases.text = "0";
                if (boughtItems != null)
                {
                    tableController.currentSourceArray = boughtItems;
                    total = 0;
                    foreach (FinanceReportModel report in boughtItems)
                    {
                        total += report.price;
                    }
                    totalPurchases.text = total.ToString();
                }
                if (string.Equals(LanguageManager.Instance.CurrentlyLoadedCulture.languageCode, "ar"))
                {
                    totalLabel.text = ArabicFixer.Fix(LanguageManager.Instance.GetTextValue("SWOTSELocalization.Totalpurchases")).ToUpper();
                    tableColumn.text = ArabicFixer.Fix(LanguageManager.Instance.GetTextValue("SWOTSELocalization.From")).ToUpper();
                }
                else
                {
                    totalLabel.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization.Totalpurchases").ToUpper();
                    tableColumn.text = LanguageManager.Instance.GetTextValue("SWOTSELocalization.From").ToUpper();
                }
                tableController.m_tableView.ReloadData();
                break;

            default:
                break;
        }
    }

    public IEnumerator GetFinanceReports()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://" + GlobalGOScript.instance.host + ":8889/reports/getCityItemsReports/" + GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString() + "/");
        request.SetRequestHeader("authorization", "Token " + GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            string decodedString = System.Text.Encoding.UTF8.GetString(request.downloadHandler.data);
            JSONObject itemArray = JSONObject.Create(decodedString);
            boughtItems = new ArrayList();
            soldItems = new ArrayList();

            foreach (JSONObject item in itemArray.list)
            {
                FinanceReportModel tempItem = JsonUtility.FromJson<FinanceReportModel>(item.Print());
                tempItem.item_constant = JsonUtility.FromJson<ShopItemModel>(item["item_constant"].Print());
                tempItem.date = System.DateTime.Parse(item["date"].str);

                if (string.Equals(LanguageManager.Instance.CurrentlyLoadedCulture.languageCode, "ar"))
                {
                    tempItem.item_constant.name = ArabicFixer.Fix(tempItem.item_constant.name);
                    tempItem.item_constant.description = ArabicFixer.Fix(tempItem.item_constant.description);
                }

                if (string.Equals(tempItem.type, "buy"))
                {
                    boughtItems.Add(tempItem);
                }
                else if (string.Equals(tempItem.type, "sale"))
                {
                    soldItems.Add(tempItem);
                }
            }
        }
    }

    public IEnumerator GetFinanceOffers()
    {
        UnityWebRequest request = UnityWebRequest.Get("http://" + GlobalGOScript.instance.host + ":8889/shop/getCityLots/" + GlobalGOScript.instance.gameManager.cityManager.currentCity.id.ToString() + "/");
        request.SetRequestHeader("authorization", "Token " + GlobalGOScript.instance.token);

        yield return request.Send();

        if (request.isError)
        {
            Debug.Log(request.error);
        }
        else
        {
            string decodedString = System.Text.Encoding.UTF8.GetString(request.downloadHandler.data);
            JSONObject itemArray = JSONObject.Create(decodedString);
            offeredItems = new ArrayList();

            foreach (JSONObject item in itemArray.list)
            {
                FinanceReportModel offeredItem = JsonUtility.FromJson<FinanceReportModel>(item.Print());
                offeredItem.item_constant = JsonUtility.FromJson<ShopItemModel>(item["item_constant"].Print());
                offeredItem.date = System.DateTime.Parse(item["date"].str);

                if (string.Equals(LanguageManager.Instance.CurrentlyLoadedCulture.languageCode, "ar"))
                {
                    offeredItem.item_constant.name = ArabicFixer.Fix(offeredItem.item_constant.name);
                    offeredItem.item_constant.description = ArabicFixer.Fix(offeredItem.item_constant.description);
                }

                offeredItems.Add(offeredItem);
            }
        }

        gotOffers.Invoke(this, "success");
    }

    void OnGetOffers(object sender, string value)
    {
        gotOffers -= OnGetOffers;
        if (string.Equals(value, "success"))
        {
            tableController.currentSourceArray = offeredItems;
            tableController.m_tableView.ReloadData();
        }
    }

    public IEnumerator CancelOfferRequest(string id)
    {
        WWWForm form = new WWWForm();
        form.AddField("city_item_lot_id", id);
        UnityWebRequest request = UnityWebRequest.Post("http://" + GlobalGOScript.instance.host + ":8889/shop/deleteCityItemLot/", form);
        request.SetRequestHeader("authorization", "Token " + GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject response = JSONObject.Create(request.downloadHandler.text);

            if (!response.IsString)
            {
                //fail
                offerCanceled.Invoke(this, "fail");
            }
            else
            {
                //success
                if (string.Equals(response.str, "success", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    offerCanceled.Invoke(this, "success");
                }
                else
                {
                    offerCanceled.Invoke(this, "fail");
                    GlobalGOScript.instance.windowInstanceManager.openNotification(response.str);
                }
            }
        }
    }

    void OnOfferCancelled(object sender, string value)
    {
        offerCanceled -= OnOfferCancelled;

        if (string.Equals(value, "success"))
        {
            gotOffers += OnGetOffers;
            StartCoroutine(GetFinanceOffers());
        }
    }

    public void CancelOffer(string id)
    {
        offerCanceled += OnOfferCancelled;
        StartCoroutine(CancelOfferRequest(id));
    }
}
