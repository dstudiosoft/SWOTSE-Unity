﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using SmartLocalization;


public class AllianceChat : MonoBehaviour {

    public Text chatWindow;
	public InputField input;
    public Scrollbar Scroll;


    private String MessageBuffer;
    public bool chatWorking;
    public bool readyToChat;

    void Start()
    {
        StartCoroutine(BeginChat());
    }

    // Use this for initialization
    public IEnumerator BeginChat () {
        WebSocket w = new WebSocket(new Uri("ws://"+GlobalGOScript.instance.host+":8891/alliance"));
        yield return StartCoroutine(w.Connect());
        JSONObject initial = JSONObject.Create(JSONObject.Type.OBJECT);
        initial.AddField("message", "allianceChatBuffer");
        initial.AddField("userToken", GlobalGOScript.instance.token.ToString());
        w.SendString(initial.Print());
        chatWindow.text = string.Empty;
        chatWorking = true;
        while (chatWorking)
        {
            string reply = w.RecvString();
            if (reply != null)
            {
                if (chatWindow.preferredHeight > 1000.0f)
                {
                    chatWindow.text = string.Empty;
                }
                string[] parts = reply.Split(new String[] { "]:" }, StringSplitOptions.None);
                if (TextLocalizer.HasArabicGlyphs(parts[1]))
                {
					string arabic = parts[1].ToString();
					Debug.Log ("ARABIC = " + arabic);
					Debug.Log ("ARABIC FIX = " + ArabicSupport.ArabicFixer.Fix (arabic));
                    //System.Array.Reverse(arabic);
					arabic = ArabicSupport.ArabicFixer.Fix (arabic);
					reply = parts[0] + "]: " + arabic;
                }
                Debug.Log ("Received: " + reply);
                chatWindow.text += (reply + "\n");
                Scroll.value = 0.0f;
            }
            if (!String.IsNullOrEmpty(MessageBuffer))
            {
                JSONObject obj = JSONObject.Create(JSONObject.Type.OBJECT);
                obj.AddField("userToken", GlobalGOScript.instance.token);
                obj.AddField("message", MessageBuffer);
                w.SendString(obj.Print());
                MessageBuffer = String.Empty;
            }
            if (w.error != null)
            {
                Debug.LogError ("Error: "+w.error);
                break;
            }
            yield return 0;
        }
        w.Close();
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (!pauseStatus && readyToChat)
        {
            if(gameObject.activeInHierarchy)
                StartCoroutine(BeginChat());
        }
        else
        {
            chatWorking = false;
            StopCoroutine(BeginChat());
        }
    }

    public void SendMessage()
    {
        if (!String.IsNullOrEmpty(input.text))
        {
            MessageBuffer = input.text;
            input.text = String.Empty;
        }
    }
}
