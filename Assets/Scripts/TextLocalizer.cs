﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;
using ArabicSupport;

public class TextLocalizer : MonoBehaviour {

    public bool uppercase;
    public string key;
    public string appendText;

    public static string LocalizeString(string value) {
		Debug.Log (value);
		string localizedString = LanguageManager.Instance.GetTextValue("SWOTSELocalization." + value.Replace(" ", "").Replace("_", ""));
		Debug.Log ("TRANSLATE = " + localizedString);
		//Debug.Log (localizedString);
		//if (LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar")
       // {
		//	localizedString = ArabicFixer.Fix(localizedString, false, false);
       // }
        return localizedString;
    }

	public static string localizeArFix(string value){
		if (SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar") {
			value = ArabicFixer.Fix (value);
		}
		return value;
	}

    public static bool HasArabicGlyphs(string text)
    {
        char[] glyphs = text.ToCharArray();

        foreach (char glyph in glyphs)
        {
            if (glyph >= 0x600 && glyph <= 0x6ff)
                return true;
            if (glyph >= 0x750 && glyph <= 0x7ff)
                return true;
            if (glyph >= 0xfb50 && glyph <= 0xfc3f)
                return true;
            if (glyph >= 0xfe70 && glyph <= 0xfefc)
                return true;
        }
        return false;
    }

    public static IEnumerator WrapMultiline(UnityEngine.UI.Text reciever, string textToWrap)
    {
        List<int> lineBreakIndices = new List<int>();

        //char[] text = textToWrap.ToCharArray();
        //System.Array.Reverse(text);
        //reciever.text = new string(text);
        //reciever.text = ArabicFixer.Fix(new string(text), false, false);


        reciever.text = textToWrap;

        Canvas.ForceUpdateCanvases();
        yield return new WaitForEndOfFrame();
        for (int i = 0; i < reciever.cachedTextGenerator.lines.Count - 1; i++)
        {
            int endIndex = reciever.cachedTextGenerator.lines[i + 1].startCharIdx - 1;
            lineBreakIndices.Add(endIndex);
        }

        char[] chars = textToWrap.ToCharArray();
        List<char> charsList = new List<char>(chars);
        foreach (int index in lineBreakIndices)
        {
            charsList.Insert(index, '\n');
        }
        string rebuilded = new string(charsList.ToArray());
       reciever.text = ArabicFixer.Fix(rebuilded, false, false);
    }

	public void OnEnable () {
        if (LanguageManager.HasInstance)
        {
            LanguageManager manager = LanguageManager.Instance;
            Text text = gameObject.GetComponent<Text>();
            string localizedValue = manager.GetTextValue("SWOTSELocalization." + key);

            if (!string.IsNullOrEmpty(localizedValue))
            {

                if (manager.CurrentlyLoadedCulture.languageCode == "ar")
                {
                    //char[] reversedString = new char[localizedValue.Length];

                    //for (int i = localizedValue.Length - 1; i >= 0; i--)
                    //{
                    //    reversedString[localizedValue.Length - i - 1] = localizedValue[i];
                    //}

                    //localizedValue = new string(reversedString);

                }

                localizedValue = ArabicFixer.Fix(localizedValue, false, false);

                if (uppercase)
                {
                    localizedValue = localizedValue.ToUpper();
                }

                if (!string.IsNullOrEmpty(appendText))
                {
                    localizedValue = string.Concat(localizedValue, appendText);
                }

                text.text = localizedValue;
            }
            else
            {
                localizedValue = key.Replace("_", " ");
                localizedValue = ArabicFixer.Fix(localizedValue, false, false);

                if (uppercase)
                {
                    localizedValue = localizedValue.ToUpper();
                }

                if (!string.IsNullOrEmpty(appendText))
                {
                    localizedValue = string.Concat(localizedValue, appendText);
                }

                text.text = localizedValue;
            }
        }
	}
}
