﻿using UnityEngine;
using System.Collections;

public class OpenLink : MonoBehaviour {

    public string link;

    public void openLink()
    {
        Application.OpenURL(link);
    }
}
