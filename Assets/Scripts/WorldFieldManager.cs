﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class WorldFieldManager : MonoBehaviour {

    public int internalPitId;
    public GameObject fieldObject;
    public GameObject flag;
    public GameObject occupationIcon;
    private WorldFieldModel fieldInfo;

    public void DisableField()
    {
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        flag.SetActive(false);
        occupationIcon.SetActive(false);
        MeshRenderer textRenderer = flag.transform.GetChild(0).GetComponent<MeshRenderer>();
        textRenderer.enabled = false;
        fieldObject.SetActive(false);
    }

    public void InitFieldState(WorldFieldModel field)
    {
        fieldInfo = field;
        // check field type;

        if (!string.Equals(field.cityType, "Empty_Land"))
        {
            Sprite fieldImage = Resources.Load<Sprite>("WorldMapBuildings/" + field.cityType + "/1");
            fieldObject.GetComponent<SpriteRenderer>().sprite = fieldImage;
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            fieldObject.SetActive(true);
        }
        else
        {
            fieldObject.GetComponent<SpriteRenderer>().sprite = null;
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            fieldObject.SetActive(true);
        }

        if (field.owner.id != 0)
        {
            if (field.owner.id == GlobalGOScript.instance.playerManager.user.id)
            {
                Sprite flagSprite = Resources.Load<Sprite>("WorldMapBuildings/Flags/My");
                flag.GetComponent<SpriteRenderer>().sprite = flagSprite;
                flag.SetActive(true);
            }
            else if (GlobalGOScript.instance.playerManager.user.alliance.id != 0 && field.owner.allianceID != 0)
            {
                if (GlobalGOScript.instance.playerManager.user.alliance.id == field.owner.allianceID)
                {
                    Sprite flagSprite = Resources.Load<Sprite>("WorldMapBuildings/Flags/Ally");
                    flag.GetComponent<SpriteRenderer>().sprite = flagSprite;
                    flag.SetActive(true);
                }
                else if (GlobalGOScript.instance.playerManager.user.alliance.allies.Contains(field.owner.allianceID))
                {
                    Sprite flagSprite = Resources.Load<Sprite>("WorldMapBuildings/Flags/Ally");
                    flag.GetComponent<SpriteRenderer>().sprite = flagSprite;
                    flag.SetActive(true);
                }
                else if (GlobalGOScript.instance.playerManager.user.alliance.enemies.Contains(field.owner.allianceID))
                {
                    Sprite flagSprite = Resources.Load<Sprite>("WorldMapBuildings/Flags/Enemy");
                    flag.GetComponent<SpriteRenderer>().sprite = flagSprite;
                    flag.SetActive(true);
                }
                else
                {
                    Sprite flagSprite = Resources.Load<Sprite>("WorldMapBuildings/Flags/Neutral");
                    flag.GetComponent<SpriteRenderer>().sprite = flagSprite;
                    flag.SetActive(true);
                }
            }
            else
            {
                Sprite flagSprite = Resources.Load<Sprite>("WorldMapBuildings/Flags/Neutral");
                flag.GetComponent<SpriteRenderer>().sprite = flagSprite;
                flag.SetActive(true);
            }

            if (!string.IsNullOrEmpty(fieldInfo.city.occupantCityOwnerName))
            {
                occupationIcon.SetActive(true);
            }

            MeshRenderer textRenderer = flag.transform.GetChild(0).GetComponent<MeshRenderer>();
            TextMesh textMesh = textRenderer.GetComponent<TextMesh>();
            textMesh.text = field.owner.flag;
            textRenderer.sortingLayerName = "GameObjects";
            textRenderer.sortingOrder = 1;
            textRenderer.enabled = true;
        }
        else
        {
            if (string.Equals(field.cityType, "Barbarians"))
            {
                Sprite flagSprite = Resources.Load<Sprite>("WorldMapBuildings/Flags/Barbarians");
                flag.GetComponent<SpriteRenderer>().sprite = flagSprite;
                flag.SetActive(true);
            }
            else
            {
                flag.SetActive(false);
            }
        }

    }

    public void OnMouseUpAsButton()
    {
//        #if UNITY_IOS || UNITY_ANDROID
//        if (!EventSystem.current.IsPointerOverGameObject(0))
//        #else
//        if (!EventSystem.current.IsPointerOverGameObject())
//        #endif
//        {
//            if (fieldObject.activeInHierarchy)
//            {
//                if (string.Equals(fieldInfo.cityType, "City"))
//                {
//                    GlobalGOScript.instance.windowInstanceManager.openCityInfo(fieldInfo);
//                }
//                else
//                {
//                    if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityValleys.ContainsKey(fieldInfo.id) && !string.Equals(fieldInfo.cityType, "Empty_Land"))
//                    {
//                        GlobalGOScript.instance.windowInstanceManager.openBuildingInformation(fieldInfo.id);
//                    }
//                    else
//                    {
//                        GlobalGOScript.instance.windowInstanceManager.openValleyInfo(fieldInfo);
//                    }
//                }
//            }
//        }
//    }

    #if UNITY_IOS || UNITY_ANDROID
    if (!EventSystem.current.IsPointerOverGameObject(0))
    #else
    if (!EventSystem.current.IsPointerOverGameObject())
    #endif
    {
        if (fieldObject.activeInHierarchy)
        {
            if (string.Equals(fieldInfo.cityType, "City"))
            {
                GlobalGOScript.instance.windowInstanceManager.openCityInfo(fieldInfo);
            }
            else
            {
                if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityValleys.ContainsKey(fieldInfo.id) && !string.Equals(fieldInfo.cityType, "Empty_Land"))
                {
						Debug.Log("WORLD CLICK");
                        GlobalGOScript.instance.windowInstanceManager.openBuildingWindwow(fieldInfo.id, "World");
                }
                else
                {
                    GlobalGOScript.instance.windowInstanceManager.openValleyInfo(fieldInfo);
                }
            }
        }
    }
}
}
