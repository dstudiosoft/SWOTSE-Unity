﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class ServerTimeUpdater : MonoBehaviour {
    public static ServerTimeUpdater instance;

    public Text currentTime;
    private DateTime currentDate;

    void Start () {
        instance = this;
    }

	void OnEnable () {
		if (GlobalGOScript.instance.gameManager != null)
        {
            currentDate = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update;
            currentTime.text = currentDate.Hour.ToString("D2") + ":" + currentDate.Minute.ToString("D2") + ":" + currentDate.Second.ToString("D2");
        }
	}
	
	void FixedUpdate () {
        currentDate = GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update;
        currentTime.text = currentDate.Hour.ToString("D2") + ":" + currentDate.Minute.ToString("D2") + ":" + currentDate.Second.ToString("D2");
	}
}
