﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;

public class ValleyInfoManager : MonoBehaviour {

    public Text valleyName;
    public Text product;
    public Text coords;

    public Image valleyImage;

    public GameObject marchBtn;
    public GameObject buildCityBtn;
    public GameObject recruitBtn;

    public Image[] tabs;
    public Sprite tabSelectedSprite;
    public Sprite tabUnselectedSprite;

    public WorldFieldModel field;

    void OnEnable()
    {
        coords.text = field.posX.ToString() + ", " + field.posY.ToString();
        string localizedValue = TextLocalizer.LocalizeString(field.cityType);

        if (string.IsNullOrEmpty(localizedValue))
        {
            valleyName.text = field.cityType.Replace('_', ' ');
        }
        else
        {
            valleyName.text = localizedValue;
        }

        if (string.Equals(field.cityType, "Barbarians"))
        {
            product.text = TextLocalizer.LocalizeString("none");
            Sprite fieldSprite = Resources.Load<Sprite>("WorldMapBuildings/" + field.cityType + "/1");
            valleyImage.sprite = fieldSprite;
            recruitBtn.SetActive(true);
        }
        else if (string.Equals(field.cityType, "Empty_Land"))
        {
            product.text = TextLocalizer.LocalizeString("none");
            Sprite fieldSprite = Resources.Load<Sprite>("Buildings/Pit/1.png");
            valleyImage.sprite = fieldSprite;
            valleyImage.transform.Translate(new Vector2(0.0f, -0.5f));
        }
        else
        {
            Sprite fieldSprite = Resources.Load<Sprite>("WorldMapBuildings/" + field.cityType + "/" + field.level.ToString());
            valleyImage.sprite = fieldSprite;

            switch (field.cityType)
            {
                case "Forest":
                    product.text = TextLocalizer.LocalizeString("Wood").ToUpper();
                    break;
                case "Lake":
                    product.text = TextLocalizer.LocalizeString("Food").ToUpper();
                    break;
                case "Hill":
                    product.text = TextLocalizer.LocalizeString("Stone").ToUpper();
                    break;
                case "Mountin_Iron":
                    product.text = TextLocalizer.LocalizeString("Iron").ToUpper();
                    break;
                case "Mountin_Cooper":
                    product.text = TextLocalizer.LocalizeString("Copper").ToUpper();
                    break;
                default:
                    break;
            }
        }

        if (field.owner == null)
        {
            marchBtn.SetActive(true);
        }
        else
        {
            if (GlobalGOScript.instance.playerManager.user.id == field.owner.id && string.Equals(field.cityType, "Empty_Land"))
            {
                // activate build button
                buildCityBtn.SetActive(true);
            }

            if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.IsBuildingPresent("Command_Center"))
            {
                if (GlobalGOScript.instance.playerManager.allianceManager.allAlliances != null)
                {
                    if (!GlobalGOScript.instance.playerManager.allianceManager.allAlliances.ContainsKey(field.owner.id))
                    {
                        marchBtn.SetActive(true);
                    }
                }
                else
                {
                    if (GlobalGOScript.instance.playerManager.user.id != field.owner.id)
                    {
                        marchBtn.SetActive(true);
                    }
                }
            }
        }
    }

    public void March()
    {
//        GlobalGOScript.instance.windowInstanceManager.openDispatch(field, true);
        GlobalGOScript.instance.windowInstanceManager.openDispatchWindow(field, true);
        GlobalGOScript.instance.windowInstanceManager.closeValleyInfo();
        foreach (Image tab in tabs)
            tab.sprite = tabUnselectedSprite;
        tabs[1].sprite = tabSelectedSprite;
    }

    public void BuildCity()
    {
//        CityChangerContentManager manager = GlobalGOScript.instance.cityChanger;
//
//        GlobalGOScript.instance.gameManager.cityManager.onCityBuilt += CityBuiltHandler;
//
//        manager.BuildNewCity(field.posX, field.posY, "cityName");

        GlobalGOScript.instance.windowInstanceManager.openBuildNewCity(field.posX, field.posY);

        foreach (Image tab in tabs)
            tab.sprite = tabUnselectedSprite;
        tabs[0].sprite = tabSelectedSprite;
    }

    public void Recruit()
    {
        GlobalGOScript.instance.windowInstanceManager.openRecruitBarbarians(field);
        foreach (Image tab in tabs)
            tab.sprite = tabUnselectedSprite;
        tabs[2].sprite = tabSelectedSprite;
    }

    public void CityBuiltHandler(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.cityManager.onCityBuilt -= CityBuiltHandler;
        
        if (string.Equals(value, "success"))
        {
            GlobalGOScript.instance.windowInstanceManager.closeValleyInfo();
        }
    }
}
