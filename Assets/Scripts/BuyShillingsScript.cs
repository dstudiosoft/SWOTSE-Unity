﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;
using System.Collections;
using System.Collections.Generic;

public class BuyShillingsScript : MonoBehaviour {

    public ToggleGroup choisesGroup;
	
    public void BuyShillings()
    {
        IEnumerable<Toggle> l = choisesGroup.ActiveToggles();
        foreach (Toggle t in l)
        {
            StartCoroutine(SendBuyRequest(t.gameObject.name));
        }
    }

    IEnumerator SendBuyRequest(string count)
    {
        WWWForm requestForm = new WWWForm();
        requestForm.AddField("count", count);

        #if UNITY_WEBGL
            requestForm.AddField("unity", "web");
        #else
            requestForm.AddField("unity", "mobile");
        #endif

        UnityWebRequest request = UnityWebRequest.Post("http://"+GlobalGOScript.instance.host+":8889/payment/buyShillings/", requestForm);
        request.SetRequestHeader("authorization", "Token "+GlobalGOScript.instance.token);

        yield return request.Send();

        if(request.isError) {
            Debug.Log(request.error);
        }
        else {
            JSONObject response = JSONObject.Create(request.downloadHandler.text, 2);
            if (string.Equals(response["result"].str, "success"))
            {
                Application.OpenURL(response["link"].str);
            }
            else
            {
                WindowInstanceManager.instance.openNotification(response["error"].str);
            }
        }
    }
}