﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Collections;

public class ConstructionPitManager : MonoBehaviour {

    public long pitId;
    public GameObject building;

    public string type = "Buildings";

    public void OnEnable()
    {
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            type = "Buildings";
        }
        else if (SceneManager.GetActiveScene().buildIndex == 3)
        {
            type = "Fields";
        }

        BuildingModel buildingState = null;

        if (string.Equals(type, "Buildings"))
        {
            if (pitId > 31 && pitId < 900 && !GlobalGOScript.instance.gameManager.cityManager.currentCity.isExpantion)
            {
                DisablePit();
            }
            else
            {
                if (gameObject.GetComponent<BoxCollider2D>() != null)
                {
                    if (!gameObject.GetComponent<BoxCollider2D>().enabled)
                    {
                        EnablePit();
                    }
                }
                    
                if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings.ContainsKey(pitId))
                {
                    buildingState = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitId];
                }            
            }
        }
        else if (string.Equals(type, "Fields"))
        {
            if (pitId < GlobalGOScript.instance.gameManager.residenceFields[GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[1000].level - 1])
            {
                if (gameObject.GetComponent<BoxCollider2D>() != null)
                {
                    if (!gameObject.GetComponent<BoxCollider2D>().enabled)
                    {
                        EnablePit();
                    }
                }

                if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields.ContainsKey(pitId))
                {
                    buildingState = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pitId];
                }              
            }
            else
            {
                DisablePit();
            }
        }            

        if (buildingState == null)
        {
            building.GetComponent<SpriteRenderer>().sprite = null;
            building.SetActive(false);
        }
        else
        {
            if (string.Equals(type, "Buildings"))
            {
                if (string.Equals(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitId].status, "creating"))
                {
                    Sprite buildingSprite = Resources.Load<Sprite>("Buildings/Pit/bldg_construction_mod");
                    building.GetComponent<SpriteRenderer>().sprite = buildingSprite;
                }
                else if (string.Equals(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitId].building, "Wall"))
                {
                    Sprite buildingSprite = Resources.Load<Sprite>(type + "/" + buildingState.building + "/castle");
                    building.GetComponent<SpriteRenderer>().sprite = buildingSprite;
                }
                else
                {
                    Sprite buildingSprite = Resources.Load<Sprite>(type + "/" + buildingState.building + "/" + buildingState.level);
                    building.GetComponent<SpriteRenderer>().sprite = buildingSprite;
                }
            }
            else if (string.Equals(type, "Fields"))
            {
                if (string.Equals(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pitId].status, "creating"))
                {
                    Sprite buildingSprite = Resources.Load<Sprite>("Buildings/Pit/bldg_construction_mod");
                    building.GetComponent<SpriteRenderer>().sprite = buildingSprite;
                }
                else
                {
                    Sprite buildingSprite = Resources.Load<Sprite>(type + "/" + buildingState.building + "/" + buildingState.level);
                    building.GetComponent<SpriteRenderer>().sprite = buildingSprite;
                }
            }
            building.SetActive(true);
        }
    }

    public void UpdatePitState(object sender, string value)
    {
        if (string.Equals(value, "success"))
        {
            BuildingModel buildingState = null;

            if (string.Equals(type, "Buildings"))
            {
                if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings.ContainsKey(pitId))
                {
                    buildingState = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitId];
                }            
            }
            else if (string.Equals(type, "Fields"))
            {
                if (GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields.ContainsKey(pitId))
                {
                    buildingState = GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pitId];
                }            
            }            

            if (buildingState == null)
            {
                building.GetComponent<SpriteRenderer>().sprite = null;
                building.SetActive(false);
            }
            else
            {
                if (string.Equals(type, "Buildings"))
                {
                    if (string.Equals(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitId].status, "creating"))
                    {
                        Sprite buildingSprite = Resources.Load<Sprite>("Buildings/Pit/bldg_construction_mod");
                        building.GetComponent<SpriteRenderer>().sprite = buildingSprite;
                    }
                    else if (string.Equals(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitId].building, "Wall"))
                    {
                        Sprite buildingSprite = Resources.Load<Sprite>(type + "/" + buildingState.building + "/1");
                        building.GetComponent<SpriteRenderer>().sprite = buildingSprite;
                    }
                    else
                    {
                        Sprite buildingSprite = Resources.Load<Sprite>(type + "/" + buildingState.building + "/" + buildingState.level);
                        building.GetComponent<SpriteRenderer>().sprite = buildingSprite;
                    }
                }
                else if (string.Equals(type, "Fields"))
                {
                    if (string.Equals(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pitId].status, "creating"))
                    {
                        Sprite buildingSprite = Resources.Load<Sprite>("Buildings/Pit/bldg_construction_mod");
                        building.GetComponent<SpriteRenderer>().sprite = buildingSprite;
                    }
                    else
                    {
                        Sprite buildingSprite = Resources.Load<Sprite>(type + "/" + buildingState.building + "/" + buildingState.level);
                        building.GetComponent<SpriteRenderer>().sprite = buildingSprite;
                    }
                }

                building.SetActive(true);
            }

            GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate -= UpdatePitState;
        }
        else
        {
            GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingUpdate -= UpdatePitState;
        }
    }
        

    public void OnMouseUpAsButton()
    {
//        #if UNITY_IOS || UNITY_ANDROID
//        if (!EventSystem.current.IsPointerOverGameObject(0))
//        #else
//        if (!EventSystem.current.IsPointerOverGameObject())
//        #endif
//        {
//            if (building.activeInHierarchy)
//            {
//                if (string.Equals(type, "Buildings"))
//                {
//                    if (!string.Equals(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitId].status, "creating"))
//                    {
//                        GlobalGOScript.instance.windowInstanceManager.openBuildingInformation(this);
//                    }
//                }
//                else if (string.Equals(type, "Fields"))
//                {
//                    if (!string.Equals(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pitId].status, "creating"))
//                    {
//                        GlobalGOScript.instance.windowInstanceManager.openBuildingInformation(this);
//                    }
//                }
//            }
//            else
//            {
//                GlobalGOScript.instance.windowInstanceManager.openBuildingConstruction(this);
//            }
//        }

        #if UNITY_IOS || UNITY_ANDROID
        if (!EventSystem.current.IsPointerOverGameObject(0))
        #else
        if (!EventSystem.current.IsPointerOverGameObject())
        #endif
        {
            if (building.activeInHierarchy)
            {
                if (string.Equals(type, "Buildings"))
                {
                    if (!string.Equals(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityBuildings[pitId].status, "creating"))
                    {
						Debug.Log ("OPEN DETAILS BUILDINGS");
                        GlobalGOScript.instance.windowInstanceManager.openBuildingWindwow(pitId, type);
                    }
                }
                else if (string.Equals(type, "Fields"))
                {
                    if (!string.Equals(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.cityFields[pitId].status, "creating"))
                    {
						Debug.Log ("OPEN DETAILS FIELDS");
                        GlobalGOScript.instance.windowInstanceManager.openBuildingWindwow(pitId, type);
                    }
                }
            }
            else
            {
				Debug.Log ("openBuildingConstruction");
                GlobalGOScript.instance.windowInstanceManager.openBuildingConstruction(this);
            }
        }
    }

    private void DisablePit()
    {
        gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
    }

    private void EnablePit()
    {
        gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
    }

}
