﻿using UnityEngine;
using System.Collections;

public class ZoomManager : MonoBehaviour {
	
    public Camera mainCamera;

    public void Start()
    {
        mainCamera = Camera.main;
    }

    public void ZoomIn()
    {
        if (mainCamera.orthographicSize > 1.0f)
        {
            mainCamera.orthographicSize -= 0.5f;
        }
    }

    public void ZoomOut()
    {
        if (mainCamera.orthographicSize < 5.0f)
        {
            mainCamera.orthographicSize += 0.5f;
        }
    }

    public void MaxZoom()
    {
        mainCamera.orthographicSize = 1.0f;
    }

    public void MinZoom()
    {
        mainCamera.orthographicSize = 5.0f;
    }

    public void MiddleZoom()
    {
        mainCamera.orthographicSize = 2.5f;
    }
}
