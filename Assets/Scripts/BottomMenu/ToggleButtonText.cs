﻿using UnityEngine;
using System.Collections;

public class ToggleButtonText : MonoBehaviour {

    public GameObject text;

	// Use this for initialization
	void Start () 
    {
        text.SetActive(false);
	}

    public void ToggleTextActive(bool active)
    {
        text.SetActive(active);
    }
}
