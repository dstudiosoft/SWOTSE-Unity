﻿using UnityEngine;
using UnityEngine.UI;
using Tacticsoft;

public class FinancialTableLine : TableViewCell {

    public Image productImage;
    public Text productName;
    public Text quantity;
    public Text price;
    public Text date;
    public Text fromUser;
    public GameObject cancelOfferBtn;

    private FinancialContentChanger owner;
    private string lotId;

    public void SetImage(string path)
    {
        Sprite imageSprite = Resources.Load<Sprite>(path);
        productImage.sprite = imageSprite;
    }

    public void SetName(string name)
    {
        productName.text = name;
    }

    public void SetQuantity(long value)
    {
        quantity.text = value.ToString();   
    }

    public void SetPrice(long value)
    {
        price.text = value.ToString();
    }

    public void SetDate(string dateValue)
    {
        date.text = dateValue;
    }

    public void SetFromUser(string value)
    {
        fromUser.text = value;
    }

    public void SetOwner(FinancialContentChanger value)
    {
        owner = value;
    }

    public void SetLotId(string id)
    {
        lotId = id;
    }

    public void SetCancelable(bool value)
    {
        cancelOfferBtn.SetActive(value);
    }

    public void CancelOffer()
    {
        owner.CancelOffer(lotId);
    }
}
