﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CityItemLine : MonoBehaviour {

    public Image cityImage;
    public Image backGround;
    public Text coords;

    public long cityId;
    public bool currentCity;

    public void SetCityId(long id)
    {
        cityId = id;
    }

    public void SetCurrent(bool current)
    {
        Sprite back;
        currentCity = current;

        if (current)
        {
            back = Resources.Load<Sprite>("UI/currentCity");
            RightPanel.instance.cityIcon.sprite = cityImage.sprite;
            RightPanel.instance.cityCoords.text = coords.text;
        }
        else
        {
            back = Resources.Load<Sprite>("UI/availableCity");
        }

        backGround.sprite = back;
    }

    public void SetCityImage(string imageName)
    {
        Sprite icon = Resources.Load<Sprite>("UI/CityIcon/" + imageName);
        cityImage.sprite = icon;
    }

    public void SetCoords(long x, long y)
    {
        coords.text = x.ToString() + "," + y.ToString();
    }

    public void ChangeCity()
    {
        if (cityId != GlobalGOScript.instance.gameManager.cityManager.currentCity.id)
        {
            StartCoroutine(transform.parent.GetComponent<CityChangerContentManager>().ChangeCity(cityId));
        }
    }
}
