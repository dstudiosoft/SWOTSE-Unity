﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class BattleReportItemsLine : TableViewCell {

    public Text itemName;
    public Text itemPrice;
    public Text itemCount;

    public void SetItemName(string name)
    {
        itemName.text = name;
    }

    public void SetItemPrice(long price)
    {
        itemPrice.text = price.ToString();
    }

    public void SetItemCount(long count)
    {
        itemCount.text = count.ToString();
    }
}
