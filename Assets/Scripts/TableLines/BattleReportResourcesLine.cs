﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class BattleReportResourcesLine : TableViewCell
{

    public Text gold;
    public Text silver;
    public Text food;
    public Text wood;
    public Text iron;
    public Text copper;
    public Text stone;

    public void SetGold(string value)
    {
        gold.text = value;
    }

    public void SetSilver(string value)
    {
        silver.text = value;
    }

    public void SetFood(string value)
    {
        food.text = value;
    }

    public void SetWood(string value)
    {
        wood.text = value;
    }

    public void SetIron(string value)
    {
        iron.text = value;
    }

    public void SetCopper(string value)
    {
        copper.text = value;
    }

    public void SetStone(string value)
    {
        stone.text = value;
    }
}
