﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class AllianceInviteLine : TableViewCell {

    public Text memberName;
    public Text allianceName;
    public Text rank;
    public Text experience;
    public Text cities;
    public Text capitalName;
    public Text lastOnline;

    private UserModel user;
    private AllianceInviteTableController owner;

    public void SetOwner(AllianceInviteTableController controller)
    {
        owner = controller;
    }

    public void SetUserId(UserModel userValue)
    {
        user = userValue;
    }

    public void SetMemberName(string name)
    {
        memberName.text = name;
    }

    public void SetAllianceName(string name)
    {
        allianceName.text = name;
    }

    public void SetRank(string value)
    {
        rank.text = value;
    }

    public void SetExperience(long value)
    {
        experience.text = value.ToString();
    }

    public void SetCities(long value)
    {
        cities.text = value.ToString();
    }

    public void SetCapitalName(string name)
    {
        capitalName.text = name;
    }

    public void SetCapitalCoords(long x, long y)
    {
        capitalName.text += "(" + x.ToString() + "," + y.ToString() + ")";
    }

    public void SetLastOnline(string date)
    {
        lastOnline.text = date;
    }

    public void SendInvite()
    {
        GlobalGOScript.instance.playerManager.allianceManager.onInviteUser += InviteSent;
        StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.InviteUser(user.id));
    }

    public void InviteSent(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.allianceManager.onInviteUser -= InviteSent;

        if (string.Equals(value, "success"))
        {
            owner.RemoveUser(user);
        }
    }
}
