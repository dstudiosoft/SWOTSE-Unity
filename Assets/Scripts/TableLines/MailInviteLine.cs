﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class MailInviteLine : TableViewCell {

    public Text senderName;
    public Text date;

    private MailInviteTableController owner;

    private long inviteId;

    public void SetInviteId(long id)
    {
        inviteId = id;
    }

    public void SetOwner(MailInviteTableController controller)
    {
        owner = controller;
    }

    public void SetName(string name)
    {
        senderName.text = name;
    }

    public void SetMailDate(string dateValue)
    {
        date.text = dateValue;
    }

    public void AcceptInvite()
    {
        owner.AcceptInvite(inviteId);
    }

    public void RejectInvite()
    {
        owner.RejectInvite(inviteId);
    }
}
