﻿using UnityEngine;
using UnityEngine.UI;
using Tacticsoft;

public class ResidenceValleysTableLine : TableViewCell {

    public Text valleyName;
    public Text resource;
    public Text region;
    public Text valleyCoords;
    public Text valleyLevel;
    public Text production;
	private WorldFieldModel model;

    private ResidenceValleysTableController owner;

    public void SetOwner(ResidenceValleysTableController controller)
    {
        owner = controller;
    }

    public void SetValleyName(string name)
    {
		valleyName.text = TextLocalizer.localizeArFix(name);;
    }

    public void SetLevel(long value)
    {
        valleyLevel.text = value.ToString();
    }

    public void SetCoords(string value)
    {
        valleyCoords.text = value;
    }

    public void SetResource(string value)
    {
		resource.text = TextLocalizer.localizeArFix(value);;
    }

    public void SetRegion(string value)
    {
		region.text = TextLocalizer.localizeArFix(value);;
    }

    public void SetProduction(long value)
    {
        production.text = value.ToString();
    }
	public void setWorldFieldModel(WorldFieldModel value){
		model = value;
	}

    public void OpenBuildingWindow()
    {
		GlobalGOScript.instance.windowInstanceManager.openBuildingWindwow(model.id, "World");
    }
}
