﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class AllianceEventsLine : TableViewCell {
    public Text date;
    public Text remark;

    public void SetDate(string dateValue)
    {
        date.text = dateValue;
    }

    public void SetRemark(string text)
    {
        remark.text = text;
    }
}
