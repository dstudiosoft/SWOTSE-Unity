﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class PanelKnightTableLine : TableViewCell {

    public Text knightName;
    public Text knightLevel;
    public Text status;

    private long knightId;

    public void SetKnightName(string name)
    {
        knightName.text = name;
    }
        
    public void SetLevel(long value)
    {
        knightLevel.text = value.ToString();
    }

    public void SetStatus(string value)
    {
        status.text = value;
    }
        
    public void SetKnightId(long value)
    {
        knightId = value;
    }

    public void OpenKnightStats()
    {
    }
}
