﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class ResidenceBuildingsTableLine : TableViewCell {

    public Image buildingIcon;
    public Text buildingName;
    public Text buildingLevel;
    public Text status;
	private string type;

    private ResidenceBuildingsTableController owner;

    private long pitId;

    public void SetOwner(ResidenceBuildingsTableController controller)
    {
        owner = controller;
    }

    public void SetBuildingName(string name)
    {
        buildingName.text = name.Replace('_', ' ');
		if (SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar") {
			buildingName.text = ArabicSupport.ArabicFixer.Fix (buildingName.text);
		}
    }

    public void SetLevel(long value)
    {
        buildingLevel.text = value.ToString();
    }

    public void SetStatus(string value)
    {
        status.text = value;
		if (SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar") {
			status.text = ArabicSupport.ArabicFixer.Fix (status.text);
		}
    }

    public void SetPitId(long value)
    {
        pitId = value;
    }

    public void SetIcon(string path)
    {
        Sprite buidlingSprite = Resources.Load<Sprite>(path);
        buildingIcon.sprite = buidlingSprite;
    }

	public void SetType(string value){
		type = value;
	}
		
    public void OpenBuildingWindow()
    {
		GlobalGOScript.instance.windowInstanceManager.openBuildingWindwow(pitId, type);
    }
		
}
