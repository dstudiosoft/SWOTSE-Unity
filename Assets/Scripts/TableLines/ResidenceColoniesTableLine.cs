﻿using UnityEngine;
using UnityEngine.UI;
using Tacticsoft;

public class ResidenceColoniesTableLine : TableViewCell {

    public Text playerName;
    public Text cityName;
    public Text region;
    public Text cityCoords;
    public Text mayerLevel;

    private ResidenceColoniesTableController owner;
    private ResourcesModel tributes;

    public void SetOwner(ResidenceColoniesTableController controller)
    {
        owner = controller;
    }

    public void SetCityName(string name)
    {
        cityName.text = name;
    }

    public void SetPlayerName(string name)
    {
        playerName.text = name;
    }

    public void SetMayerLevel(long value)
    {
        mayerLevel.text = value.ToString();
    }

    public void SetCoords(string value)
    {
        cityCoords.text = value;
    }

    public void SetRegion(string value)
    {
		region.text = TextLocalizer.localizeArFix(value);
    }

    public void SetResources(ResourcesModel model)
    {
        tributes = model;
    }

    public void OpenTributes()
    {
        GlobalGOScript.instance.windowInstanceManager.openTributes(tributes);
    }
}
