﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class UpgradeBuildingTableLine : TableViewCell {

    public Image buildingIcon;
    public Text buildingName;
    public Text buildingLevel;

    private UpgradeBuildingTableController owner;
    private long id;

    public void SetOwner(UpgradeBuildingTableController controller)
    {
        owner = controller;
    }

    public void SetBuildingName(string name)
    {
        buildingName.text = name.Replace('_', ' ');
    }

    public void SetLevel(long value)
    {
        buildingLevel.text = value.ToString();
    }

    public void SetId(long value)
    {
        id = value;
    }

    public void SetIcon(string path)
    {
        Sprite buidlingSprite = Resources.Load<Sprite>(path);
        buildingIcon.sprite = buidlingSprite;
    }

    public void UpgradeBuilding()
    {
        owner.UpgradeBuilding(id);
    }
}
