﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using Tacticsoft;

public class PanelTableLine : TableViewCell {

    public Image actionImage;
    public Image itemImage;

    public Text itemText;
    public Text itemTimer;

    public GameObject speedUpBTN;
    public GameObject cancelBTN;

    public RectTransform progressBar;

    private long eventId;
    private long itemId;
    private PanelTableController owner;
    private DateTime startTime;
    private DateTime finishTime;
    private bool cityBuilding;
    private TimeSpan timespan;
    private float valuePerSecond;

    public void SetEventId(long id)
    {
        eventId = id;
    }

    public void SetItemId(long id)
    {
        itemId = id;
    }

    public void SetOwner(PanelTableController controller)
    {
        owner = controller;
    }

    public void SetActionImage(string path)
    {
        Sprite actionSprite = Resources.Load<Sprite>(path);
        actionImage.sprite = actionSprite;
    }

    public void SetItemImage(string path)
    {
        Sprite itemSprite = Resources.Load<Sprite>(path);
        itemImage.sprite = itemSprite;
    }

    public void SetItemText(string text)
    {
        itemText.text = text;
    }

    public void SetInteractable(bool value)
    {
        speedUpBTN.SetActive(value);
    }

    public void SetCancelable(bool value)
    {
        cancelBTN.SetActive(value);
    }

    public void SetCityBuilding(bool value)
    {
        cityBuilding = value;
    }

    public void OpenSpeedUp()
    {
        GlobalGOScript.instance.windowInstanceManager.openSpeedUp(itemImage.sprite, eventId, startTime, finishTime, owner);
    }

    public void CancelAction()
    {
        switch (owner.content)
        {
            case "BUILDINGS":
                GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingCancelled += ActionCancelled;
                if (cityBuilding)
                {
                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.CancelBuilding(eventId));
                }
                else
                {
                    StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.buildingsManager.CancelValley(eventId));
                }
                break;
            case "UNITS":
                GlobalGOScript.instance.gameManager.cityManager.unitsManager.onTrainingCancelled += ActionCancelled;
                StartCoroutine(GlobalGOScript.instance.gameManager.cityManager.unitsManager.CancelTraining(eventId));
                break;
            case "ARMY":
                GlobalGOScript.instance.gameManager.battleManager.onMarchCanceled += ActionCancelled;
                StartCoroutine(GlobalGOScript.instance.gameManager.battleManager.CancelAttack(itemId));
                break;
            default:
                break;
        }
    }

    public void ActionCancelled(object sender, string value)
    {
        GlobalGOScript.instance.gameManager.battleManager.onMarchCanceled -= ActionCancelled;
        GlobalGOScript.instance.gameManager.cityManager.buildingsManager.onBuildingCancelled -= ActionCancelled;
        GlobalGOScript.instance.gameManager.cityManager.unitsManager.onTrainingCancelled -= ActionCancelled;

        if (string.Equals(value, "success"))
        {
            switch (owner.content)
            {
                case "BUILDINGS":
                    GlobalGOScript.instance.playerManager.reportManager.onGetBuildingTimers += owner.ReloadTable;
                    StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetBuildingTimers(true));
                    GlobalGOScript.instance.playerManager.reportManager.onGetWorldMapTimers += owner.ReloadTable;
                    StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetWorldMapTimers(true));
                    break;
                case "UNITS":
                    GlobalGOScript.instance.playerManager.reportManager.onGetUnitTimers += owner.ReloadTable;
                    StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetUnitTimers(true));
                    break;
                case "ARMY":
                    GlobalGOScript.instance.playerManager.reportManager.onGetArmyTimers += owner.ReloadTable;
                    StartCoroutine(GlobalGOScript.instance.playerManager.reportManager.GetArmyTimers(true));
                    break;
                default:
                    break;
            }
        }
    }

    public void SetTimer(DateTime start, DateTime finish)
    {
        startTime = start;
        finishTime = finish;
        timespan = finishTime - start;
        valuePerSecond = (progressBar.anchorMax.x - progressBar.anchorMin.x) / (float)(timespan.TotalSeconds);
        timespan -= GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update - start;
        itemTimer.text = ((int)(timespan.TotalHours)).ToString() + ":" + timespan.Minutes.ToString() + ":" + timespan.Seconds.ToString();
        progressBar.anchorMax = new Vector2(progressBar.anchorMax.x - valuePerSecond * (float)(timespan.TotalSeconds), progressBar.anchorMax.y);
    }

    void FixedUpdate()
    {
        if (finishTime <= GlobalGOScript.instance.gameManager.cityManager.resourceManager.cityResources.last_update)
        {
            owner.RemoveTimer(eventId);
        }
        else
        {
            timespan = timespan.Subtract(new TimeSpan(0, 0, 1));
            itemTimer.text = ((int)(timespan.TotalHours)).ToString() + ":" + timespan.Minutes.ToString() + ":" + timespan.Seconds.ToString();
            progressBar.anchorMax += new Vector2(valuePerSecond, 0.0f);
        }
    }
}
