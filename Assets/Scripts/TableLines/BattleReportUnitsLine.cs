﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class BattleReportUnitsLine : TableViewCell {

    public Text unitName;
    public Text beforeCount;
    public Text lostCount;
    public Text remainingCount;


    public void SetUnitName(string name)
    {
        unitName.text = name;
    }

    public void SetBeforeCount(long count)
    {
        beforeCount.text = count.ToString();
    }

    public void SetLostCount(long count)
    {
        lostCount.text = count.ToString();
    }

    public void SetRemainingCount(long count)
    {
        remainingCount.text = count.ToString();
    }
}
