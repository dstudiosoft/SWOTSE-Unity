﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DiplomacyTableLine : MonoBehaviour {

    public Text nickName;
    public Text knight;
    public Text city;
    public Text coord;
    public Text date;

    public void SetNickName(string name)
    {
        nickName.text = name;
    }

    public void SetKnight(string name)
    {
        knight.text = name;
    }

    public void SetCity(string name)
    {
        city.text = name;
    }

    public void SetCoord(string value)
    {
        coord.text = value;
    }

    public void SetDate(string value)
    {
        date.text = value;
    }

    public void DismissTroops()
    {
    }

    public void ShowTroops()
    {
    }
}
