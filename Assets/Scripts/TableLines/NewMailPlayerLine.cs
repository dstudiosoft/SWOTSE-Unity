﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class NewMailPlayerLine : TableViewCell {

    public Text playeName;
    private long playerId;

    private NewMailContentManager manager;

    public void SetManager(NewMailContentManager value)
    {
        manager = value;
    }

    public void SetPlayerId(long id)
    {
        playerId = id;
    }

    public void SetPlayerName(string name)
    {
        playeName.text = name;
    }

    public void SendUserMessage()
    {
        manager.SendUserMessage(playerId);
    }
}
