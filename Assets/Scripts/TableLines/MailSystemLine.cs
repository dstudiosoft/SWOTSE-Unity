﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class MailSystemLine : TableViewCell {

    public Text subject;
    public Text message;
    //public Text date;

    public void SetSubject(string value)
    {
        subject.text = value;
    }

    public void SetMessage(string value)
    {
        message.text = value;
    }

//    public void SetDate(string dateValue)
//    {
//        date.text = dateValue;
//    }
}
