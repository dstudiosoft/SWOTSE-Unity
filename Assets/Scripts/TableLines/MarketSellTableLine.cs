﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class MarketSellTableLine : TableViewCell {

    public Text quantity;
    public Text unitPrice;

    private long lotId;
    private MarketManager owner;

    public void SetId(long id)
    {
        lotId = id;
    }

    public void SetOwner(MarketManager manager)
    {
        owner = manager;
    }

    public void SetQuantity(long value)
    {
        quantity.text = value.ToString();
    }

    public void SetUnitPrice(long value)
    {
        unitPrice.text = value.ToString();
    }

    public void RetractLot()
    {
        owner.RetractLot(lotId);
    }
}
