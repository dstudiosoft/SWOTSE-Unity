﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class MailAllianceReportTableLine : TableViewCell {

    public Text userName;
    public Text type;
    public Text source;
    public Text target;
    public Text time;

    private long reportId;
    private MailAllianceReportTableController owner;

    public void SetOwner(MailAllianceReportTableController controller)
    {
        owner = controller;
    }

    public void SetId(long id)
    {
        reportId = id;
    }

    public void SetUserName(string name)
    {
        userName.text = name;
    }

    public void SetType(string typeValue)
    {
        type.text = typeValue;
    }

    public void SetSource(string sourceValue)
    {
        source.text = sourceValue;
    }

    public void SetTarget(string targetValue)
    {
        target.text = targetValue;
    }

    public void SetTime(string timeValue)
    {
        time.text = timeValue;
    }

    public void ShowReport()
    {
        GlobalGOScript.instance.windowInstanceManager.openReport(reportId);
    }

    public void DeleteReport()
    {
        owner.DeleteReport(reportId);
    }
}
