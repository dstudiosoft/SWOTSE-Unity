﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class AllianceAlliancesLine : TableViewCell {

    public Text allianceName;
    public Text founder;
    public Text leader;
    public Text experience;
    public Text rank;
    public Text members;
    public GameObject joinBtn;

    private long allianceId;
    private AllianceAlliancesTableController owner;

    public void SetOwner(AllianceAlliancesTableController controller)
    {
        owner = controller;
    }

    public void SetAllianceName(string name)
    {
        allianceName.text = name;
    }

    public void SetFounder(string name)
    {
        founder.text = name;
    }

    public void SetLeader(string name)
    {
        leader.text = name;
    }

    public void SetExperience(long value)
    {
        experience.text = value.ToString();
    }

    public void SetRank(long value)
    {
        rank.text = value.ToString();
    }

    public void SetMembers(long value)
    {
        members.text = value.ToString();
    }

    public void SetAllianceId(long value)
    {
        allianceId = value;
    }

    public void SetJoinable(bool value)
    {
        joinBtn.SetActive(value);
    }

    public void SendJoinRequest()
    {
        GlobalGOScript.instance.playerManager.allianceManager.onRequestJoin += JoinRequestSent;
        StartCoroutine(GlobalGOScript.instance.playerManager.allianceManager.RequestJoin(allianceId));
    }

    public void JoinRequestSent(object sender, string value)
    {
        GlobalGOScript.instance.playerManager.allianceManager.onRequestJoin -= JoinRequestSent;

        if (string.Equals(value, "success"))
        {
            //some action
        }
    }
}
