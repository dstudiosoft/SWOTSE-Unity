﻿using UnityEngine;
using UnityEngine.UI;
using Tacticsoft;

public class ResidenceCitiesTableLine : TableViewCell {

    public Text cityName;
    public Text region;
    public Text cityCoords;
    public Text mayerLevel;
    public Text fieldsCount;
    public Text valleysCount;

    private ResidenceCitiesTableController owner;

    public void SetOwner(ResidenceCitiesTableController controller)
    {
        owner = controller;
    }

    public void SetCityName(string name)
    {
        cityName.text = name;
    }

    public void SetMayerLevel(long value)
    {
        mayerLevel.text = value.ToString();
    }

    public void SetCoords(string value)
    {
        cityCoords.text = value;
    }

    public void SetRegion(string value)
    {
		region.text = TextLocalizer.localizeArFix(value);
    }

    public void SetFieldsCount(long value)
    {
        fieldsCount.text = value.ToString();
    }

    public void SetValleysCount(long value)
    {
        valleysCount.text = value.ToString();
    }
}
