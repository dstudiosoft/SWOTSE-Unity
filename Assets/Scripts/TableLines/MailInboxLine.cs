﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class MailInboxLine : TableViewCell {

    public Text playerName;
    public Text subject;
    public Text date;
//    public Image readMail;

    private long messageId;
    private MailInboxTableController owner;
//    private UserMessageReportModel message;

//    public void SetMessageModel(UserMessageReportModel model)
//    {
//        message = model;
//    }

    public void SetOwner(MailInboxTableController controller)
    {
        owner = controller;
    }

    public void SetMessageId(long id)
    {
        messageId = id;
    }

    public void SetPlayerName(string name)
    {
        playerName.text = name;
    }

    public void SetSubject(string name)
    {
        subject.text = name;
    }

    public void SetDate(string dateValue)
    {
        date.text = dateValue;
    }

    public void SetRead()
    {
    }

//    public void ShowUser()
//    {
//        GlobalGOScript.instance.windowInstanceManager.openUserInfo(message.fromUser);
//    }

    public void ShowMessage()
    {
        GlobalGOScript.instance.windowInstanceManager.openMessage(messageId, false, owner);
    }

    public void ReplyMessage()
    {
        GlobalGOScript.instance.windowInstanceManager.openMessage(messageId, true, owner);
    }

    public void DeleteMessage()
    {
        owner.DeleteMessage(messageId);
    }
}
