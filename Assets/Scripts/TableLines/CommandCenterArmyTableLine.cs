﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class CommandCenterArmyTableLine : TableViewCell {

    public Text knightName;
    public Text status;
    private long armyId;
    private CommandCenterArmiesTableController owner;

    public void SetKnightName(string name)
    {
        knightName.text = name;
    }
        
    public void SetStatus(string value)
    {
        status.text = value;
    }

    public void SetArmyId(long id)
    {
        armyId = id;
    }

    public void SetOwner(CommandCenterArmiesTableController controller)
    {
        owner = controller;
    }

    public void ShowDetails()
    {
        GlobalGOScript.instance.windowInstanceManager.openArmyDetails(armyId, owner);
    }
}
