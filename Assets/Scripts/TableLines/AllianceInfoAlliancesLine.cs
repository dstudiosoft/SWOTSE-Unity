﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class AllianceInfoAlliancesLine : TableViewCell {

    public Text allianceName;
    public Text rank;
    public Text members;
    public GameObject neutralBTN;
    public GameObject allyBTN;
    public GameObject enemyBTN;

    private AllianceInfoTableContoller owner;
    private long allianceId;

    public void SetOwner(AllianceInfoTableContoller controller)
    {
        owner = controller;
    }

    public void SetAllianceId(long id)
    {
        allianceId = id;
    }

    public void SetAllianceName(string name)
    {
        allianceName.text = name;
    }

    public void SetRank(long value)
    {
        rank.text = value.ToString();
    }

    public void SetMembers(long value)
    {
        members.text = value.ToString();
    }

    public void SetNeutral(bool neutral)
    {
        if (neutral)
        {
            allyBTN.SetActive(true);
            enemyBTN.SetActive(true);
            neutralBTN.SetActive(false);
        }
        else
        {
            neutralBTN.SetActive(true);
            allyBTN.SetActive(false);
            enemyBTN.SetActive(false);
        }
    }

    public void MakeNeutral()
    {
        owner.MakeNeutral(allianceId);
    }

    public void MakeEnemy()
    {
        owner.MakeEnemy(allianceId);
    }

    public void MakeAlly()
    {
        owner.MakeAlly(allianceId);
    }
}
