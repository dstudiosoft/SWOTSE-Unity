﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class GuestHouseTableLine : TableViewCell {

    public Text knightName;
    public Text politicsLevel;
    public Text marchesLevel;
    public Text speedLevel;
    public Text loyaltyLevel;
    public Text knightLevel;
    public Text salary;
    public Text status;
    public Text actionText;
    public GameObject nameBackground;

    private GuestFeastingTableController owner;
    private ArmyGeneralModel general;

    public void SetOwner(GuestFeastingTableController controller)
    {
        owner = controller;
    }

    public void SetGeneralModel(ArmyGeneralModel model)
    {
        general = model;
    }

    public void SetKnightName(string name)
    {
        knightName.text = name;
    }

    public void SetPolitics(long value)
    {
        politicsLevel.text = value.ToString();
    }

    public void SetMarches(long value)
    {
        marchesLevel.text = value.ToString();
    }

    public void SetSpeed(long value)
    {
        speedLevel.text = value.ToString();
    }

    public void SetLoyalty(long value)
    {
        loyaltyLevel.text = value.ToString();
    }

    public void SetLevel(long value)
    {
        knightLevel.text = value.ToString();
    }

    public void SetSalary(long value)
    {
        salary.text = value.ToString();
    }

    public void SetStatus(string value)
    {
        status.text = value;
    }

    public void SetActionText(string text)
    {
        actionText.text = text;
    }

    public void RecruitDismissKnight()
    {
        owner.PerformAction(general);
    }

    public void SetPressable(bool state)
    {
        if (nameBackground != null)
        {
            nameBackground.SetActive(state);
        }
    }

    public void OpenGeneralInfo()
    {
        if (!owner.guest)
        {
            GlobalGOScript.instance.windowInstanceManager.openKnightInfo(general);
        }
    }
}
