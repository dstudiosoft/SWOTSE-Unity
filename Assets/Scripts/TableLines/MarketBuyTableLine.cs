﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class MarketBuyTableLine : TableViewCell {

    public Text quantity;
    public Text unitPrice;

    public void SetQuantity(long value)
    {
        quantity.text = value.ToString();
    }

    public void SetUnitPrice(long value)
    {
        unitPrice.text = value.ToString();
    }
}
