﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;

public class AllianceMembersPermissionsLine : TableViewCell {

    public Text memberName;
    public Text position;
    public Text rank;
    public Text experience;
    public Text cities;
    public Text capitalName;
    public Text lastOnline;

    private UserModel user;

    public void SetUser(UserModel userValue)
    {
        user = userValue;
    }

    public void SetMemberName(string name)
    {
        memberName.text = name;
    }

    public void SetPosition(string name)
    {
        position.text = name;
		if (SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.languageCode == "ar") {
			position.text = ArabicSupport.ArabicFixer.Fix (position.text);
		}
    }

    public void SetRank(string value)
    {
        rank.text = value;
    }

    public void SetExperience(long value)
    {
        experience.text = value.ToString();
    }

    public void SetCitiesCount(long value)
    {
        cities.text = value.ToString();
    }

    public void SetCapitalName(string name)
    {
        capitalName.text = name;
    }

    public void SetLastOnline(string value)
    {
        lastOnline.text = value;
    }

    public void ShowMemberInfo()
    {
        GlobalGOScript.instance.windowInstanceManager.openUserInfo(user);
    }
}
