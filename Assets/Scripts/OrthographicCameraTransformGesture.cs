﻿/*
 * @author Valentin Simonov / http://va.lent.in/
 */

using TouchScript.Gestures.Base;
using TouchScript.Layers;
using TouchScript.Utils.Geom;
using UnityEngine;

namespace TouchScript.Gestures
{
	/// <summary>
	/// Recognizes a transform gesture in screen space, i.e. translation, rotation, scaling or a combination of these.
	/// </summary>
	[AddComponentMenu("TouchScript/Gestures/Screen Transform Gesture")]
	[HelpURL("http://touchscript.github.io/docs/html/T_TouchScript_Gestures_ScreenTransformGesture.htm")]
	public class OrthographicCameraTransformGesture : TransformGestureBase, ITransformGesture
	{

        public bool worldMap = false;
        public Material mapBackground;
        public GameObject mapGameWorld;
        public TestWorldChunksManager chuncksManager;

		#region Public TransformVariables
		public Vector2 leftBottom;
		public Vector2 rightTop;
		#endregion

		#region Private TransformVariables
		private Vector2 actualLeftBottom;
		private Vector2 actualRightTop;
		#endregion

		#region Public methods

		public void Start()
		{
            GlobalGOScript.instance.windowInstanceManager.windowOpened = false;
			float vHalfExt = Camera.main.orthographicSize; // vertical half extent
			float hHalfExt = vHalfExt * Screen.width / Screen.height; // horizontal half extent
			leftBottom = new Vector2 (-hHalfExt, -vHalfExt);
			rightTop = new Vector2 (hHalfExt, vHalfExt);
			actualLeftBottom = new Vector2 (leftBottom.x + hHalfExt, leftBottom.y + vHalfExt);
			actualRightTop = new Vector2 (rightTop.x - hHalfExt, rightTop.y - vHalfExt);
		}

		/// <inheritdoc />
		public void ApplyTransform(Transform target)
		{
            if (!GlobalGOScript.instance.windowInstanceManager.windowOpened)
            {
                if (worldMap)
                {
                    if (DeltaPosition != Vector3.zero)
                    {
//                    target.position -= DeltaPosition / 750.0f * Camera.main.orthographicSize;
                        //need separate speeds for x and y in texture offset
                        //                    mapBackground.mainTextureOffset -= (Vector2)DeltaPosition / 1000.0f;
                        Vector2 textureOffset = new Vector2();
                        textureOffset.x = mapBackground.mainTextureOffset.x - DeltaPosition.x / 1300.0f;
                        textureOffset.y = mapBackground.mainTextureOffset.y - DeltaPosition.y / 600.0f;
                        mapBackground.mainTextureOffset = textureOffset;
                        mapGameWorld.transform.position += DeltaPosition / 60.0f;
                        chuncksManager.CheckChunkGrid();
                    }
                }
                else
                {
                    if (!Mathf.Approximately(DeltaScale, 1f))
                    {
                        Camera cam = target.gameObject.GetComponent<Camera>();
                        float orthSize = cam.orthographicSize;
                        //              camera scale adjustment
                        orthSize /= DeltaScale;
                        orthSize = Mathf.Clamp(orthSize, 1.0f, 5.0f); // scale clamp
                        cam.orthographicSize = orthSize;
                    }

                    if (DeltaPosition != Vector3.zero)
                    {
                        target.position -= DeltaPosition / 750.0f * Camera.main.orthographicSize;
                    }

                    //          bouds adjustment
                    float vHalfExt = Camera.main.orthographicSize; // vertical half extent
                    float hHalfExt = vHalfExt * Screen.width / Screen.height; // horizontal half extent
                    actualLeftBottom = new Vector2(leftBottom.x + hHalfExt, leftBottom.y + vHalfExt);
                    actualRightTop = new Vector2(rightTop.x - hHalfExt, rightTop.y - vHalfExt);

                    Vector3 pos = target.position;
                    pos.x = Mathf.Clamp(pos.x, actualLeftBottom.x, actualRightTop.x);
                    pos.y = Mathf.Clamp(pos.y, actualLeftBottom.y, actualRightTop.y);
                    target.position = pos;
                }
            }
		}

		#endregion

		#region Protected methods

		/// <inheritdoc />
		protected override float doRotation(Vector2 oldScreenPos1, Vector2 oldScreenPos2, Vector2 newScreenPos1,
			Vector2 newScreenPos2, ProjectionParams projectionParams)
		{
			var oldScreenDelta = oldScreenPos2 - oldScreenPos1;
			var newScreenDelta = newScreenPos2 - newScreenPos1;
			return (Mathf.Atan2(newScreenDelta.y, newScreenDelta.x) -
				Mathf.Atan2(oldScreenDelta.y, oldScreenDelta.x)) * Mathf.Rad2Deg;
		}

		/// <inheritdoc />
		protected override float doScaling(Vector2 oldScreenPos1, Vector2 oldScreenPos2, Vector2 newScreenPos1,
			Vector2 newScreenPos2, ProjectionParams projectionParams)
		{
			return (newScreenPos2 - newScreenPos1).magnitude / (oldScreenPos2 - oldScreenPos1).magnitude;
		}

		/// <inheritdoc />
		protected override Vector3 doOnePointTranslation(Vector2 oldScreenPos, Vector2 newScreenPos,
			ProjectionParams projectionParams)
		{
			if (isTransforming)
			{
				return new Vector3(newScreenPos.x - oldScreenPos.x, newScreenPos.y - oldScreenPos.y, 0);
			}

			screenPixelTranslationBuffer += newScreenPos - oldScreenPos;
			if (screenPixelTranslationBuffer.sqrMagnitude > screenTransformPixelThresholdSquared)
			{
				isTransforming = true;
				return screenPixelTranslationBuffer;
			}

			return Vector3.zero;
		}

		/// <inheritdoc />
		protected override Vector3 doTwoPointTranslation(Vector2 oldScreenPos1, Vector2 oldScreenPos2,
			Vector2 newScreenPos1, Vector2 newScreenPos2, float dR, float dS, ProjectionParams projectionParams)
		{
			if (isTransforming)
			{
				var transformedPoint = scaleAndRotate(oldScreenPos1, (oldScreenPos1 + oldScreenPos2) * .5f, dR, dS);
				return new Vector3(newScreenPos1.x - transformedPoint.x, newScreenPos1.y - transformedPoint.y, 0);
			}

			screenPixelTranslationBuffer += newScreenPos1 - oldScreenPos1;
			if (screenPixelTranslationBuffer.sqrMagnitude > screenTransformPixelThresholdSquared)
			{
				isTransforming = true;
				oldScreenPos1 = newScreenPos1 - screenPixelTranslationBuffer;
				var transformedPoint = scaleAndRotate(oldScreenPos1, (oldScreenPos1 + oldScreenPos2) * .5f, dR, dS);
				return new Vector3(newScreenPos1.x - transformedPoint.x, newScreenPos1.y - transformedPoint.y, 0);
			}

			return Vector3.zero;
		}

		#endregion

		#region Private functions

		private Vector2 scaleAndRotate(Vector2 point, Vector2 center, float dR, float dS)
		{
			var delta = point - center;
//			if (dR != 0) delta = TwoD.Rotate(delta, dR);
			if (dS != 0) delta = delta * dS;
			return center + delta;
		}

		#endregion
	}
}