﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class WindowInstanceManager : MonoBehaviour {

    public static WindowInstanceManager instance;

    public bool windowOpened = false;
    public string shillingsHtml;

    //prefaps
    public GameObject BuildingConstructionWindow;
    public GameObject BuildingInformationWindow;
    public GameObject ReportContainerWindow;
    public GameObject ShopWindow; //treasure and shop make datasource and prefab change
    public GameObject MailWindow;
    public GameObject AlliancesWindow;
    public GameObject UserInfoWindow;
    public GameObject FinanceWindow;
    public GameObject HelpWindow;
    public GameObject NewMailWindow;
    public GameObject MessageWindow;
    public GameObject MessageAllianceMemberWindow;
    public GameObject EventsWindow;
    public GameObject AllianceInfoWindow;
    public GameObject UnitDetailsWindow;
    public GameObject CityInfoWindow;
    public GameObject ValleyInfoWindow;
    public GameObject DispatchWindow;
    public GameObject RecruitBarbariansWindow;
    public GameObject NotificationWindow;
    public GameObject LoginProgressBar;
    public GameObject KnightInfoWindow;
    public GameObject BuyItemWindow;
    public GameObject SellItemWindow;
    public GameObject SpeedUpWindow;
    public GameObject UpgradeBuildingWindow;
    public GameObject AdvencedTeleportWindow;
    public GameObject CityDeedWindow;
    public GameObject BuyShillingsWindow;
    public GameObject ChangeLanguageWindow;
    public GameObject DismissWindow;
    public GameObject ChangePasswordWindow;
    public GameObject TributesWindow;
    public GameObject UpdateBuildingWindow;
    public GameObject BattleLogWindow;
    public GameObject BuildNewCityWindow;
    public GameObject ArmyDetailsWIndow;
    public GameObject NewAllianceNameWindow;
    public GameObject ConfirmationWindow;
    public GameObject ShieldTimeWindow;
    public GameObject TutorialWindow;
    public GameObject VideoAdsWindow;

    //instances
    private GameObject BuildingConstructionWindowInstance;
    private GameObject BuildingInformationWindowInstance;
    private GameObject BuildingInformationWindowInstance2;
    private GameObject ReportContainerWindowInstance;
    private GameObject ShopWindowInstance;
    private GameObject MailWindowInstance;
    private GameObject AlliancesWindowInstance;
    private GameObject UserInfoWindowInstance;
    private GameObject FinanceWindowInstance;
    private GameObject HelpWindowInstance;
    private GameObject NewMailWindowInstance;
    private GameObject MessageWindowInstance;
    private GameObject MessageAllianceMemberWindowInstance;
    private GameObject EventsWindowInstance;
    private GameObject AllianceInfoWindowInstance;
    private GameObject UnitDetailsWindowInstance;
    private GameObject CityInfoWindowInstance;
    private GameObject DispatchWindowInstance;
    private GameObject RecruitBarbariansWindowInstance;
    private GameObject ValleyInfoWindowInstance;
    private GameObject NotificationWindowInstance;
    private GameObject LoginProgressBarInstance;
    private GameObject KnightInfoWindowInstance;
    private GameObject BuyItemWindowInstance;
    private GameObject SellItemWindowInstance;
    private GameObject SpeedUpWindowInstance;
    private GameObject UpgradeBuildingWindowInstance;
    private GameObject AdvencedTeleportWindowInstance;
    private GameObject CityDeedWindowInstance;
    private GameObject BuyShillingsWindowInstance;
    private GameObject ChangeLanguageWindowInstance;
    private GameObject DismissWindowInstance;
    private GameObject ChangePasswordWindowInstance;
    private GameObject TributesWindowInstance;
    private GameObject UpdateBuildingWindowInstance;
    private GameObject BattleLogWindowInstance;
    private GameObject BuildNewCityWindowInstance;
    private GameObject ArmyDetailsWindowInstance;
    private GameObject ShillingsWebViewWindowInstance;
    private GameObject NewAllianceNameWindowInstance;
    private GameObject ConfirmationWindowInstance;
    private GameObject ShieldTimeWindowInstance;
    private GameObject TutorialWindowInstance;
    private GameObject VideoAdsWindowInstance;

    //panels are not instanciated
    public GameObject overviewPanel;
    public GameObject statisticsPanel;
    public GameObject actionPanel;

    //contents arrays
    public ActionManager actionManager;
    public AllianceContentChanger allianceManager;
    public TreasureManager treasureManager;

    //panels
    public GameObject oldDesignPanel;
    public GameObject newDesignPanel;
    public GameObject viewPanel;
    public GameObject itemPanel;
    public GameObject citiesPanel;

    //buttons
    public Image viewButton;
    public Image itemButton;
    public Image voiceButton;

    //sprites
    public Sprite viewClosedSprite;
    public Sprite viewOpenedSprite;
    public Sprite itemClosedSprite;
    public Sprite itemOpenedSprite;
    public Sprite voiceOnSprite;
    public Sprite voiceOffSprite;

    public GameObject UI;
    public GameObject Shell;

    public void Start()
    {
        if (!instance)
        {
            instance = this;
            instance.windowOpened = false;
        }
    }

    private void closeAllPanels()
    {
        instance.overviewPanel.SetActive(false);
        instance.statisticsPanel.SetActive(false);
        instance.actionPanel.SetActive(false);
    }

    public void toggleAction(bool active) //for now can only be true
    {
        if (instance.actionPanel.activeInHierarchy)
        {
            instance.actionPanel.SetActive(!active);
        }
        else
        {
            instance.closeAllPanels();
            instance.actionPanel.SetActive(active);
        }
    }

    public void toggleStatistics(bool active) //for now can only be true
    {
        if (instance.statisticsPanel.activeInHierarchy)
        {
            instance.statisticsPanel.SetActive(!active);
        }
        else
        {
            instance.closeAllPanels();
            instance.statisticsPanel.SetActive(active);
        }
    }

    public void toggleOverview(bool active) //for now can only be true
    {
        if (instance.overviewPanel.activeInHierarchy)
        {
            instance.overviewPanel.SetActive(!active);
        }
        else
        {
            instance.closeAllPanels();
            instance.overviewPanel.SetActive(active);
        }
    }

    public void openLoginProgressbar()
    {
        if (instance.LoginProgressBarInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDLoginProgressbar");
        GameObject UI = GameObject.Find("/Canvas");
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.black;
        GameObject progress = GameObject.Instantiate(instance.LoginProgressBar);
        progress.transform.SetParent(transf, false);
        instance.LoginProgressBarInstance = clickIntercepter;
        progress.SetActive(true);
    }

    public void closeLoginProgressbar()
    {
        if (instance.LoginProgressBarInstance)
        {
            GameObject.Destroy(instance.LoginProgressBarInstance);
        }
    }

    public void openAlliance()
    {
        if (instance.AlliancesWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDAlliance");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject allianceWindow = GameObject.Instantiate(instance.AlliancesWindow);
        allianceWindow.transform.SetParent(transf, false);
        instance.AlliancesWindowInstance = clickIntercepter;
        allianceWindow.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeAlliance()
    {
        if (instance.AlliancesWindowInstance)
        {
            GameObject.Destroy(instance.AlliancesWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openBuildingConstruction(ConstructionPitManager pit)
    {
        if (instance.BuildingConstructionWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDBuildingConstruction");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject buildingConstruction = GameObject.Instantiate(instance.BuildingConstructionWindow);
        buildingConstruction.transform.SetParent(transf, false);
        instance.BuildingConstructionWindowInstance = clickIntercepter;
        buildingConstruction.GetComponent<ConstructionDetailsManager>().pitManager = pit;
        buildingConstruction.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeBuildingConstruction()
    {
        if (instance.BuildingConstructionWindowInstance)
        {
            GameObject.Destroy(instance.BuildingConstructionWindowInstance);
            instance.windowOpened = false;
        }
    }


    public void openBuildingInformation(ConstructionPitManager pit)
    {
        if (instance.BuildingInformationWindowInstance2)
        {
            return;
        }

        //determination by pit id all wrong
        GameObject clickIntercepter = new GameObject("WNDBuildingInformation");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;

        GameObject buildingInformation = GameObject.Instantiate(instance.BuildingInformationWindow);
        buildingInformation.transform.SetParent(transf, false); 

        if (!instance.BuildingInformationWindowInstance)
        {
            instance.BuildingInformationWindowInstance = clickIntercepter;
        }
        else if (!instance.BuildingInformationWindowInstance2)
        {
            instance.BuildingInformationWindowInstance2 = clickIntercepter;
        }
        else
        {
            GameObject.Destroy(clickIntercepter);
            return;
        }
            
        buildingInformation.GetComponent<BuildingInformationManager>().pitManager = pit;
        buildingInformation.SetActive(true);
        instance.windowOpened = true;
    }

    public void openBuildingInformation(long world_id)
    {
        if (instance.BuildingInformationWindowInstance2)
        {
            return;
        }

        //determination by pit id all wrong
        GameObject clickIntercepter = new GameObject("WNDBuildingInformation");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;

        GameObject buildingInformation = GameObject.Instantiate(instance.BuildingInformationWindow);
        buildingInformation.transform.SetParent(transf, false); 

        if (!instance.BuildingInformationWindowInstance)
        {
            instance.BuildingInformationWindowInstance = clickIntercepter;
        }
        else if (!instance.BuildingInformationWindowInstance2)
        {
            instance.BuildingInformationWindowInstance2 = clickIntercepter;
        }
        else
        {
            GameObject.Destroy(clickIntercepter);
            return;
        }

        BuildingInformationManager manager = buildingInformation.GetComponent<BuildingInformationManager>();
        manager.type = "World";
        manager.world_id = world_id;

        buildingInformation.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeBuildingInformation()
    {
        if (instance.BuildingInformationWindowInstance2)
        {
            GameObject.Destroy(instance.BuildingInformationWindowInstance2);
            instance.windowOpened = false;
        }
        else if (instance.BuildingInformationWindowInstance)
        {
            GameObject.Destroy(instance.BuildingInformationWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openMail(string panel)
    {
        if (instance.MailWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDMail");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject mail = GameObject.Instantiate(instance.MailWindow);
        mail.transform.SetParent(transf, false);
        instance.MailWindowInstance = clickIntercepter;
        GameObject contentObject = mail.transform.Find("contentSection").gameObject;
        contentObject.GetComponent<MailContentChange>().openPanel(panel);
        mail.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeMail()
    {
        if (instance.MailWindowInstance)
        {
            GameObject.Destroy(instance.MailWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openFinance()
    {
        if (instance.FinanceWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDFinance");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject finance = GameObject.Instantiate(instance.FinanceWindow);
        finance.transform.SetParent(transf, false);
        instance.FinanceWindowInstance = clickIntercepter;
        finance.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeFinance()
    {
        if (instance.FinanceWindowInstance)
        {
            GameObject.Destroy(instance.FinanceWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openUserInfo(UserModel user)
    {
        if (instance.UserInfoWindowInstance)
        {
            return;
        }

        if (user.id == GlobalGOScript.instance.playerManager.user.id)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDUserInfo");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject info = GameObject.Instantiate(instance.UserInfoWindow);
        info.transform.SetParent(transf, false);
        instance.UserInfoWindowInstance = clickIntercepter;
        info.GetComponent<UserInfoContentManager>().user = user;
        info.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeUserInfo()
    {
        if (instance.UserInfoWindowInstance)
        {
            GameObject.Destroy(instance.UserInfoWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openHelp()
    {
        if (instance.HelpWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDHelp");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject help = GameObject.Instantiate(instance.HelpWindow);
        help.transform.SetParent(transf, false);
        instance.HelpWindowInstance = clickIntercepter;
        help.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeHelp()
    {
        if (instance.HelpWindowInstance)
        {
            GameObject.Destroy(instance.HelpWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openShop(bool shop)
    {
        if (instance.ShopWindowInstance)
        {
            return;
        }

//        if (GlobalGOScript.instance.playerManager.allianceManager.userAlliance == null)
//        {
//            instance.openNotification("You have to be in an alliance to use the shop");
//            return;
//        }

        GameObject clickIntercepter = new GameObject("WNDShop");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject shopWindow = GameObject.Instantiate(instance.ShopWindow);
        shopWindow.transform.SetParent(transf, false);
        instance.ShopWindowInstance = clickIntercepter;

        shopWindow.GetComponentInChildren<ShopTableController>().shop = shop;
        shopWindow.SetActive(true);
        instance.windowOpened = true;
    }

    public void openPlayerShop(long cityId, bool shop)
    {
        if (instance.ShopWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDShop");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject shopWindow = GameObject.Instantiate(instance.ShopWindow);
        shopWindow.transform.SetParent(transf, false);
        instance.ShopWindowInstance = clickIntercepter;

        shopWindow.GetComponentInChildren<ShopTableController>().cityId = cityId;
        shopWindow.GetComponentInChildren<ShopTableController>().shop = shop;
        shopWindow.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeShop()
    {
        if (instance.ShopWindowInstance)
        {
            GameObject.Destroy(instance.ShopWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openReport(long reportId)
    {
        if (instance.ReportContainerWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDReport");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject report = GameObject.Instantiate(instance.ReportContainerWindow);
        report.transform.SetParent(transf, false);
        instance.ReportContainerWindowInstance = clickIntercepter;

        report.GetComponent<BattleReportContentManager>().reportId = reportId;
        report.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeReport()
    {
        if (instance.ReportContainerWindowInstance)
        {
            GameObject.Destroy(instance.ReportContainerWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openNewMail()
    {
        if (instance.NewMailWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDNewMail");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject newMail = GameObject.Instantiate(instance.NewMailWindow);
        newMail.transform.SetParent(transf, false);
        instance.NewMailWindowInstance = clickIntercepter;

        //tell manager to initialize
        newMail.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeNewMail()
    {
        if (instance.NewMailWindowInstance)
        {
            GameObject.Destroy(instance.NewMailWindowInstance);
            instance.windowOpened = false;
        }
    }

//    public void openMessage(UserMessageReportModel messageModel, bool reply)
//    {
//        if (instance.MessageWindowInstance)
//        {
//            return;
//        }
//
//        GameObject clickIntercepter = new GameObject("WNDMessage");
//        GameObject UI = this.UI;
//        clickIntercepter.transform.SetParent(UI.transform, false);
//        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
//        transf.anchorMin = Vector2.zero;
//        transf.anchorMax = Vector2.one;
//        Image image = clickIntercepter.AddComponent<Image>();
//        image.color = Color.clear;
//        GameObject message = GameObject.Instantiate(instance.MessageWindow);
//        message.transform.SetParent(transf, false);
//        instance.MessageWindowInstance = clickIntercepter;
//
//        MessageContentManager manager = message.GetComponent<MessageContentManager>();
//        manager.message = messageModel;
//        manager.reply = reply;
//        message.SetActive(true);
//        instance.windowOpened = true;
//    }

    public void openMessage(long messageId, bool reply, MailInboxTableController owner)
    {
        if (instance.MessageWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDMessage");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject message = GameObject.Instantiate(instance.MessageWindow);
        message.transform.SetParent(transf, false);
        instance.MessageWindowInstance = clickIntercepter;

        MessageContentManager manager = message.GetComponent<MessageContentManager>();
        manager.messageId = messageId;
        manager.reply = reply;
        manager.ownerTable = owner;
        message.SetActive(true);
        instance.windowOpened = true;
    }


    public void closeMessage()
    {
        if (instance.MessageWindowInstance)
        {
            GameObject.Destroy(instance.MessageWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openMessageAlliance(long id)
    {
        if (instance.MessageAllianceMemberWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDMessageAlliance");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject messageAlliance = GameObject.Instantiate(instance.MessageAllianceMemberWindow);
        messageAlliance.transform.SetParent(transf, false);
        instance.MessageAllianceMemberWindowInstance = clickIntercepter;

        messageAlliance.GetComponent<MessageAllianceManager>().SetUserId(id);
        messageAlliance.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeMessageAlliance()
    {
        if (instance.MessageAllianceMemberWindowInstance)
        {
            GameObject.Destroy(instance.MessageAllianceMemberWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openEvents()
    {
        if (instance.EventsWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDEvents");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject events = GameObject.Instantiate(instance.EventsWindow);
        events.transform.SetParent(transf, false);
        instance.EventsWindowInstance = clickIntercepter;

        //tell manager to initialize
        events.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeEvents()
    {
        if (instance.EventsWindowInstance)
        {
            GameObject.Destroy(instance.EventsWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openAllianceInfo()
    {
        if (instance.AllianceInfoWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDAllianceInfo");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject allianceInfo = GameObject.Instantiate(instance.AllianceInfoWindow);
        allianceInfo.transform.SetParent(transf, false);
        instance.AllianceInfoWindowInstance = clickIntercepter;

        //tell manager to initialize
        allianceInfo.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeAllianceInfo()
    {
        if (instance.AllianceInfoWindowInstance)
        {
            GameObject.Destroy(instance.AllianceInfoWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openUnitDetails(string unitName)
    {
        if (instance.UnitDetailsWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDUnitDetails");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject unitDetails = GameObject.Instantiate(instance.UnitDetailsWindow);
        unitDetails.transform.SetParent(transf, false);
        instance.UnitDetailsWindowInstance = clickIntercepter;
        unitDetails.GetComponent<UnitDetailsManager>().unitName = unitName;
        unitDetails.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeUnitDetails()
    {
        if (instance.UnitDetailsWindowInstance)
        {
            GameObject.Destroy(instance.UnitDetailsWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openCityInfo(WorldFieldModel field)
    {
        if (instance.CityInfoWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDCityInfo");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject cityInfo = GameObject.Instantiate(instance.CityInfoWindow);
        cityInfo.transform.SetParent(transf, false);
        instance.CityInfoWindowInstance = clickIntercepter;
        cityInfo.GetComponent<MapCityInfoManager>().field = field;
        cityInfo.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeCityInfo()
    {
        if (instance.CityInfoWindowInstance)
        {
            GameObject.Destroy(instance.CityInfoWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openValleyInfo(WorldFieldModel field)
    {
        if (instance.ValleyInfoWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDValleyInfo");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMax = Vector2.zero;
        transf.offsetMin = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject valleyInfo = GameObject.Instantiate(instance.ValleyInfoWindow);
        valleyInfo.transform.SetParent(transf, false);
        instance.ValleyInfoWindowInstance = clickIntercepter;
        valleyInfo.GetComponent<ValleyInfoManager>().field = field;
        valleyInfo.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeValleyInfo()
    {
        if (instance.ValleyInfoWindowInstance)
        {
            GameObject.Destroy(instance.ValleyInfoWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openDispatch(WorldFieldModel field, bool fromMap)
    {
        if (instance.DispatchWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDDispatch");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject dispatch = GameObject.Instantiate(instance.DispatchWindow);
        dispatch.transform.SetParent(transf, false);
        instance.DispatchWindowInstance = clickIntercepter;
        dispatch.GetComponent<DispatchManager>().targetField = field;
        dispatch.GetComponent<DispatchManager>().fromMap = fromMap;
        dispatch.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeDispatch()
    {
        if (instance.DispatchWindowInstance)
        {
            GameObject.Destroy(instance.DispatchWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openRecruitBarbarians(WorldFieldModel field)
    {
        if (instance.RecruitBarbariansWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDRecruitBarbarians");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject recruit = GameObject.Instantiate(instance.RecruitBarbariansWindow);
        recruit.transform.SetParent(transf, false);
        instance.RecruitBarbariansWindowInstance = clickIntercepter;
        recruit.GetComponent<BarbarianRecruitManager>().targetField = field;
        recruit.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeRecruitBarbarians()
    {
        if (instance.RecruitBarbariansWindowInstance)
        {
            GameObject.Destroy(instance.RecruitBarbariansWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openNotification(string message, bool aboveAll = false)
    {
        if (instance.NotificationWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDNotification");
        GameObject UI = this.UI;
        if (aboveAll)
        {
            UI = GameObject.Find("/Canvas");
        }
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject notification = GameObject.Instantiate(instance.NotificationWindow);
        notification.transform.SetParent(transf, false);
        instance.NotificationWindowInstance = clickIntercepter;
        notification.GetComponent<NotificationWindowManager>().SetNotificationMessage(message);
        notification.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeNotification()
    {
        if (instance.NotificationWindowInstance)
        {
            GameObject.Destroy(instance.NotificationWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openKnightInfo(ArmyGeneralModel general)
    {
        if (instance.KnightInfoWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDKnightInfo");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMax = Vector2.zero;
        transf.offsetMin = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject info = GameObject.Instantiate(instance.KnightInfoWindow);
        info.transform.SetParent(transf, false);
        instance.KnightInfoWindowInstance = clickIntercepter;
        info.GetComponent<KnightInfoContentManager>().general = general;
        info.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeKnightInfo()
    {
        if (instance.KnightInfoWindowInstance)
        {
            GameObject.Destroy(instance.KnightInfoWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openBuyItem(Sprite icon, string name, string count, string unitCost, string id, IReloadable controller, bool buy)
    {
        if (instance.BuyItemWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDBuyItem");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject item = GameObject.Instantiate(instance.BuyItemWindow);
        item.transform.SetParent(transf, false);
        instance.BuyItemWindowInstance = clickIntercepter;
        item.GetComponent<BuyItemContentManager>().SetContent(icon, name, count, unitCost, id, controller, buy);
        item.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeBuyItem()
    {
        if (instance.BuyItemWindowInstance)
        {
            GameObject.Destroy(instance.BuyItemWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openSellItem(Sprite icon, string name, string count, string id, IReloadable controller)
    {
        if (instance.SellItemWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDSellItem");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject item = GameObject.Instantiate(instance.SellItemWindow);
        item.transform.SetParent(transf, false);
        instance.SellItemWindowInstance = clickIntercepter;
        item.GetComponent<SellItemContentManager>().SetContent(icon, name, count, id, controller);
        item.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeSellItem()
    {
        if (instance.SellItemWindowInstance)
        {
            GameObject.Destroy(instance.SellItemWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openSpeedUp(Sprite icon, long event_id, System.DateTime start_time, System.DateTime finish_time, PanelTableController controller)
    {
        if (instance.SpeedUpWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDSpeedUp");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMax = Vector2.zero;
        transf.offsetMin = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject speedup = GameObject.Instantiate(instance.SpeedUpWindow);
        speedup.transform.SetParent(transf, false);
        instance.SpeedUpWindowInstance = clickIntercepter;
        speedup.GetComponentInChildren<SpeedUpTableController>().SetContent(icon, event_id, start_time, finish_time, controller);
        speedup.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeSpeedUp()
    {
        if (instance.SpeedUpWindowInstance)
        {
            GameObject.Destroy(instance.SpeedUpWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openUpgrade(string item_id, IReloadable owner)
    {
        if (instance.UpgradeBuildingWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDUpgradeBuilding");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject upgrade = GameObject.Instantiate(instance.UpgradeBuildingWindow);
        upgrade.transform.SetParent(transf, false);
        instance.UpgradeBuildingWindowInstance = clickIntercepter;
        UpgradeBuildingTableController[] controllers = upgrade.GetComponentsInChildren<UpgradeBuildingTableController>();

        foreach (UpgradeBuildingTableController controller in controllers)
        {
            controller.item_id = long.Parse(item_id);
            controller.owner = owner;
        }

        upgrade.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeUpgrade()
    {
        if (instance.UpgradeBuildingWindowInstance)
        {
            GameObject.Destroy(instance.UpgradeBuildingWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openTeleport(string item_id, IReloadable owner)
    {
        if (instance.UpgradeBuildingWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDAdvancedTeleport");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject teleport = GameObject.Instantiate(instance.AdvencedTeleportWindow);
        teleport.transform.SetParent(transf, false);
        instance.AdvencedTeleportWindowInstance = clickIntercepter;
        teleport.GetComponent<AdvancedTeleportContentManager>().item_id = item_id;
        teleport.GetComponent<AdvancedTeleportContentManager>().owner = owner;

        teleport.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeTeleport()
    {
        if (instance.AdvencedTeleportWindowInstance)
        {
            GameObject.Destroy(instance.AdvencedTeleportWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openCityDeed(string item_id, IReloadable owner)
    {
        if (instance.CityDeedWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDCityDeed");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject deed = GameObject.Instantiate(instance.CityDeedWindow);
        deed.transform.SetParent(transf, false);
        instance.CityDeedWindowInstance = clickIntercepter;
        deed.GetComponent<CityDeedContentManager>().item_id = item_id;
        deed.GetComponent<CityDeedContentManager>().owner = owner;

        deed.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeCityDeed()
    {
        if (instance.CityDeedWindowInstance)
        {
            GameObject.Destroy(instance.CityDeedWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openBuyShillings()
    {
        if (instance.BuyShillingsWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDBuyShilligs");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject buy = GameObject.Instantiate(instance.BuyShillingsWindow);
        buy.transform.SetParent(transf, false);
        instance.BuyShillingsWindowInstance = clickIntercepter;

        buy.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeBuyShillings()
    {
        if (instance.BuyShillingsWindowInstance)
        {
            GameObject.Destroy(instance.BuyShillingsWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openChangeLanguage()
    {
        if (instance.ChangeLanguageWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDChangeLanguage");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject change = GameObject.Instantiate(instance.ChangeLanguageWindow);
        change.transform.SetParent(transf, false);
        instance.ChangeLanguageWindowInstance = clickIntercepter;

        change.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeChangeLanguage()
    {
        if (instance.ChangeLanguageWindowInstance)
        {
            GameObject.Destroy(instance.ChangeLanguageWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openDismiss()
    {
        if (instance.DismissWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDDismiss");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject dismiss = GameObject.Instantiate(instance.DismissWindow);
        dismiss.transform.SetParent(transf, false);
        instance.DismissWindowInstance = clickIntercepter;

        dismiss.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeDismiss()
    {
        if (instance.DismissWindowInstance)
        {
            GameObject.Destroy(instance.DismissWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openChangePassword()
    {
        if (instance.ChangePasswordWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDChangePassword");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject change = GameObject.Instantiate(instance.ChangePasswordWindow);
        change.transform.SetParent(transf, false);
        instance.ChangePasswordWindowInstance = clickIntercepter;

        change.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeChangePassword()
    {
        if (instance.ChangePasswordWindowInstance)
        {
            GameObject.Destroy(instance.ChangePasswordWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openTributes(ResourcesModel resources)
    {
        if (instance.ChangePasswordWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDTributes");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject tributes = GameObject.Instantiate(instance.TributesWindow);
        tributes.transform.SetParent(transf, false);
        instance.TributesWindowInstance = clickIntercepter;

        tributes.GetComponent<TributesContentManager>().tributes = resources;

        tributes.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeTributes()
    {
        if (instance.TributesWindowInstance)
        {
            GameObject.Destroy(instance.TributesWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openUpdateBuilding(bool upgrade, string type, long pitId)
    {
        if (instance.UpdateBuildingWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDUpdateBuilding");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject update = GameObject.Instantiate(instance.UpdateBuildingWindow);
        update.transform.SetParent(transf, false);
        instance.UpdateBuildingWindowInstance = clickIntercepter;

        UpdateBuildingContentManager manager = update.GetComponent<UpdateBuildingContentManager>();
        manager.upgrade = upgrade;
        manager.type = type;
        manager.pitId = pitId;

        update.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeUpdateBuilding()
    {
        if (instance.UpdateBuildingWindowInstance)
        {
            GameObject.Destroy(instance.UpdateBuildingWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openBattleLog(string logText)
    {
        if (instance.BattleLogWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDBattleLog");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject log = GameObject.Instantiate(instance.BattleLogWindow);
        log.transform.SetParent(transf, false);
        instance.BattleLogWindowInstance = clickIntercepter;

        BattleLogContentManager manager = log.GetComponent<BattleLogContentManager>();
        manager.log = logText;

        log.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeBattleLog()
    {
        if (instance.BattleLogWindowInstance)
        {
            GameObject.Destroy(instance.BattleLogWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openBuildNewCity(long x, long y)
    {
        if (instance.BuildNewCityWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDBuildNewCity");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject build = GameObject.Instantiate(instance.BuildNewCityWindow);
        build.transform.SetParent(transf, false);
        instance.BuildNewCityWindowInstance = clickIntercepter;

        BuildNewCityContentManager manager = build.GetComponent<BuildNewCityContentManager>();
        manager.posX = x;
        manager.posY = y;

        build.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeBuildNewCity()
    {
        if (instance.BuildNewCityWindowInstance)
        {
            GameObject.Destroy(instance.BuildNewCityWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openArmyDetails(long armyId, CommandCenterArmiesTableController owner)
    {
        if (instance.ArmyDetailsWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDArmyDetails");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject details = GameObject.Instantiate(instance.ArmyDetailsWIndow);
        details.transform.SetParent(transf, false);
        instance.ArmyDetailsWindowInstance = clickIntercepter;

        ArmyDetailsContentManager manager = details.GetComponent<ArmyDetailsContentManager>();
        manager.armyId = armyId;
        manager.owner = owner;

        details.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeArmyDetails()
    {
        if (instance.ArmyDetailsWindowInstance)
        {
            GameObject.Destroy(instance.ArmyDetailsWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openNewAllianceNameWindow()
    {
        if (instance.NewAllianceNameWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDNewAllianceName");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject build = GameObject.Instantiate(instance.NewAllianceNameWindow);
        build.transform.SetParent(transf, false);
        instance.NewAllianceNameWindowInstance = clickIntercepter;

        instance.windowOpened = true;
    }

    public void closeNewAllianceNameWindow()
    {
        if (instance.NewAllianceNameWindowInstance)
        {
            GameObject.Destroy(instance.NewAllianceNameWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openConfirmationWindow(string message, ConfirmationEvent confirmed)
    {
        if (instance.ConfirmationWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDConfirmation");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject conf = GameObject.Instantiate(instance.ConfirmationWindow);
        conf.transform.SetParent(transf, false);
        instance.ConfirmationWindowInstance = clickIntercepter;

        ConfirmationContentManager manager = conf.GetComponent<ConfirmationContentManager>();
        manager.confirmationMessage.text = message;
        manager.confirmed = confirmed;

        conf.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeConfirmationWindow()
    {
        if (instance.ConfirmationWindowInstance)
        {
            GameObject.Destroy(instance.ConfirmationWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openShieldTimeWindow(string itemId, bool hour, IReloadable owner)
    {
        if (instance.ShieldTimeWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDShieldTimeSet");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject time = GameObject.Instantiate(instance.ShieldTimeWindow);
        time.transform.SetParent(transf, false);
        instance.ShieldTimeWindowInstance = clickIntercepter;

        ShieldTimeContentManager manager = time.GetComponent<ShieldTimeContentManager>();
        manager.hour = hour;
        manager.owner = owner;
        manager.itemId = itemId;

        time.SetActive(true);
        instance.windowOpened = true;
    }

    public void openTutorial()
    {
        if (instance.TutorialWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDTutorial");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject tut = GameObject.Instantiate(instance.TutorialWindow);
        tut.transform.SetParent(transf, false);
        instance.TutorialWindowInstance = clickIntercepter;

        tut.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeTutorial()
    {
        if (instance.TutorialWindowInstance)
        {
            GameObject.Destroy(instance.TutorialWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openVideoAds()
    {
        if (instance.TutorialWindowInstance)
        {
            return;
        }

        GameObject clickIntercepter = new GameObject("WNDAdBonus");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        GameObject ads = GameObject.Instantiate(instance.VideoAdsWindow);
        ads.transform.SetParent(transf, false);
        instance.VideoAdsWindowInstance = clickIntercepter;

        ads.SetActive(true);
        instance.windowOpened = true;
    }

    public void closeVideAds()
    {
        if (instance.VideoAdsWindowInstance)
        {
            GameObject.Destroy(instance.VideoAdsWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void closeShieldTimeWindow()
    {
        if (instance.ShieldTimeWindowInstance)
        {
            GameObject.Destroy(instance.ShieldTimeWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openShillingsWebView()
    {
        if (instance.ShillingsWebViewWindowInstance)
        {
            return;
        }

        #if !UNITY_WEBGL
        GameObject clickIntercepter = new GameObject("WNDShillingsWebView");
        GameObject UI = this.UI;
        clickIntercepter.transform.SetParent(UI.transform, false);
        RectTransform transf = clickIntercepter.AddComponent<RectTransform>();
        transf.anchorMin = Vector2.zero;
        transf.anchorMax = Vector2.one;
        transf.offsetMin = Vector2.zero;
        transf.offsetMax = Vector2.zero;
        Image image = clickIntercepter.AddComponent<Image>();
        image.color = Color.clear;
        UniWebView webView = clickIntercepter.AddComponent<UniWebView>();
        webView.LoadHTMLString(shillingsHtml, null);
        webView.Show();
        webView.toolBarShow = true;
        instance.ShillingsWebViewWindowInstance = clickIntercepter;
        instance.windowOpened = true;
        #endif
    }

    public void closeShillingsWebView()
    {
        if (instance.ShillingsWebViewWindowInstance)
        {
            GameObject.Destroy(instance.ShillingsWebViewWindowInstance);
            instance.windowOpened = false;
        }
    }

    public void openWindow(string name)
    {
        Shell.SetActive(true);
        Shell.GetComponent<ShellContentManager>().UpdateInfo(name);
    }

    public void openBuildingWindwow(long pitId, string type)
    {
        Shell.SetActive(true);
		Shell.GetComponent<ShellContentManager> ().setType (type);
		Shell.GetComponent<ShellContentManager> ().setPitId (pitId);
        Shell.GetComponent<ShellContentManager>().UpdateBuildingInfo(pitId, type);
    }

    public void openDispatchWindow(WorldFieldModel field, bool fromMap)
    {
        Shell.SetActive(true);
        Shell.GetComponent<ShellContentManager>().UpdateDispatchWindow(field, fromMap);
    }

    public void closeWindow()
    {
        ShellContentManager manager = Shell.GetComponent<ShellContentManager>();
        GameObject.Destroy(manager.content);
        Shell.SetActive(false);
    }

    public void itemButtonOnClick()
    {
        if (itemButton.sprite.Equals(itemClosedSprite))
        {
            itemButton.sprite = itemOpenedSprite;
            itemPanel.gameObject.SetActive(true);
            return;
        }
        else
        {
            itemButton.sprite = itemClosedSprite;
            itemPanel.gameObject.SetActive(false);
        }
    }

    public void viewButtonOnClick()
    {
        if(viewButton.sprite.Equals(viewClosedSprite))
        {
            viewButton.sprite = viewOpenedSprite;
            viewPanel.gameObject.SetActive(true);
            return;
        }
        else
        {
            viewButton.sprite = viewClosedSprite;
            viewPanel.gameObject.SetActive(false);
        }
    }

    public void oldDesignButtonOnClick()
    {
        oldDesignPanel.SetActive(!oldDesignPanel.activeInHierarchy);
    }

    public void newDesignButtonOnClick()
    {
        newDesignPanel.SetActive(!newDesignPanel.activeInHierarchy);
    }

    public void voiceButtonOnClick()
    {
        if(voiceButton.sprite == voiceOnSprite)
        {
            voiceButton.sprite = voiceOffSprite;
            SoundManager.instance.switchMusic(false);
        }
        else
        {
            voiceButton.sprite = voiceOnSprite;
            SoundManager.instance.switchMusic(true);
        }
    }

    public void cityIconOnClick()
    {
        citiesPanel.SetActive(!citiesPanel.activeInHierarchy);
    }
}
