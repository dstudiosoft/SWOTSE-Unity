﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SocialPlatforms;

namespace Tacticsoft
{
    /// <summary>
    /// A reusable table for for (vertical) tables. API inspired by Cocoa's UITableView
    /// Hierarchy structure should be :
    /// GameObject + TableView (this) + Mask + Scroll Rect (point to child)
    /// - Child GameObject + Vertical Layout Group
    /// This class should be after Unity's internal UI components in the Script Execution Order
    /// </summary>
    [RequireComponent(typeof(ScrollRect))]
    public class GridTableView : TableView
    {
        #region Public API
        /// <summary>
        /// Reload the table view. Manually call this if the data source changed in a way that alters the basic layout
        /// (number of rows changed, etc)
        /// </summary>
        public override void ReloadData() {
            m_rowHeights = new float[m_dataSource.GetNumberOfRowsForTableView(this)];
            this.isEmpty = m_rowHeights.Length == 0;
            if (this.isEmpty) {
                ClearAllRows();
                return;
            }
            m_cumulativeRowHeights = new float[m_rowHeights.Length];
            m_cleanCumulativeIndex = -1;

            for (int i = 0; i < m_rowHeights.Length; i++) {
                m_rowHeights[i] = m_dataSource.GetHeightForRowInTableView(this, i);
                if (i > 0) {
                    m_rowHeights[i] += m_gridLayoutGroup.spacing.y;
                }
            }

            m_scrollRect.content.sizeDelta = new Vector2(m_scrollRect.content.sizeDelta[0], 
                GetCumulativeRowHeight(m_rowHeights.Length - 1));

            RecalculateVisibleRowsFromScratch();
            m_requiresReload = false;
        }

        /// <summary>
        /// Notify the table view that one of its rows changed size
        /// </summary>
        public override void NotifyCellDimensionsChanged(int row) {
            float oldHeight = m_rowHeights[row];
            m_rowHeights[row] = m_dataSource.GetHeightForRowInTableView(this, row);
            m_cleanCumulativeIndex = Mathf.Min(m_cleanCumulativeIndex, row - 1);
            if (m_visibleRowRange.Contains(row)) {
                TableViewCell cell = GetCellAtRow(row);
                cell.GetComponent<LayoutElement>().preferredHeight = m_rowHeights[row];
                if (row > 0) {
                    cell.GetComponent<LayoutElement>().preferredHeight -= m_gridLayoutGroup.spacing.y;
                }
            }
            float heightDelta = m_rowHeights[row] - oldHeight;
            m_scrollRect.content.sizeDelta = new Vector2(m_scrollRect.content.sizeDelta.x,
                m_scrollRect.content.sizeDelta.y + heightDelta);
            m_requiresRefresh = true;
        }

        #endregion

        protected GridLayoutGroup m_gridLayoutGroup;

        void Awake()
        {
            isEmpty = true;
            m_scrollRect = GetComponent<ScrollRect>();
            m_gridLayoutGroup = GetComponentInChildren<GridLayoutGroup>();
            m_topPadding = CreateEmptyPaddingElement("TopPadding");
            m_topPadding.transform.SetParent(m_scrollRect.content, false);
            m_bottomPadding = CreateEmptyPaddingElement("Bottom");
            m_bottomPadding.transform.SetParent(m_scrollRect.content, false);
            m_visibleCells = new Dictionary<int, TableViewCell>();

            m_reusableCellContainer = new GameObject("ReusableCells", typeof(RectTransform)).GetComponent<RectTransform>();
            m_reusableCellContainer.SetParent(this.transform, false);
            m_reusableCellContainer.gameObject.SetActive(false);
            m_reusableCells = new Dictionary<string, LinkedList<TableViewCell>>();
        }

        void Update()
        {
            if (m_requiresReload) {
                ReloadData();
            }
        }

        void LateUpdate() {
            if (m_requiresRefresh) {
                RefreshVisibleRows();
            }
        }

        void OnEnable() {
            m_scrollRect.onValueChanged.AddListener(ScrollViewValueChanged);
        }

        void OnDisable() {
            m_scrollRect.onValueChanged.RemoveListener(ScrollViewValueChanged);
        }

        protected override void AddRow(int row, bool atEnd)
        {
            TableViewCell newCell = m_dataSource.GetCellForRowInTableView(this, row);
            newCell.transform.SetParent(m_scrollRect.content, false);

            LayoutElement layoutElement = newCell.GetComponent<LayoutElement>();
            if (layoutElement == null) {
                layoutElement = newCell.gameObject.AddComponent<LayoutElement>();
            }
            layoutElement.preferredHeight = m_rowHeights[row];
            if (row > 0) {
                layoutElement.preferredHeight -= m_gridLayoutGroup.spacing.y;
            }

            m_visibleCells[row] = newCell;
            if (atEnd) {
                newCell.transform.SetSiblingIndex(m_scrollRect.content.childCount - 2); //One before bottom padding
            } else {
                newCell.transform.SetSiblingIndex(1); //One after the top padding
            }
            this.onCellVisibilityChanged.Invoke(row, true);
        }

        protected override void UpdatePaddingElements() {
            float hiddenElementsHeightSum = 0;
            for (int i = 0; i < m_visibleRowRange.from; i++) {
                hiddenElementsHeightSum += m_rowHeights[i];
            }
            m_topPadding.preferredHeight = hiddenElementsHeightSum;
            m_topPadding.gameObject.SetActive(m_topPadding.preferredHeight > 0);
            for (int i = m_visibleRowRange.from; i <= m_visibleRowRange.Last(); i++) {
                hiddenElementsHeightSum += m_rowHeights[i];
            }
            float bottomPaddingHeight = m_scrollRect.content.rect.height - hiddenElementsHeightSum;
            m_bottomPadding.preferredHeight = bottomPaddingHeight - m_gridLayoutGroup.spacing.y;
            m_bottomPadding.gameObject.SetActive(m_bottomPadding.preferredHeight > 0);
        }

    }
}
