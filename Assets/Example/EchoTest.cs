﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class EchoTest : MonoBehaviour {

    public Text chatWindow;
    public InputField Input;

    private String MessageBuffer;

	// Use this for initialization
	IEnumerator Start () {
        WebSocket w = new WebSocket(new Uri("ws://192.168.20.163:8888/alliance"));
		yield return StartCoroutine(w.Connect());
		w.SendString("1");
		int i=0;
		while (true)
		{
			string reply = w.RecvString();
			if (reply != null)
			{
				Debug.Log ("Received: "+reply);
                chatWindow.text += (reply + "\n");
			}
            if (!String.IsNullOrEmpty(MessageBuffer))
            {
                w.SendString(MessageBuffer);
                chatWindow.text += MessageBuffer + "\n";
                MessageBuffer = String.Empty;
            }
			if (w.error != null)
			{
				Debug.LogError ("Error: "+w.error);
				break;
			}
			yield return 0;
		}
		w.Close();
	}

    public void SendMessage()
    {
        if (!String.IsNullOrEmpty(Input.text))
        {
            MessageBuffer = "ololosh: " + Input.text;
            Input.text = String.Empty;
        }
    }
}
