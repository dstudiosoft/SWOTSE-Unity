﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using ArabicSupport;

public class TextReverse : MonoBehaviour {

	// Use this for initialization
	void Start () 
    {
        StreamReader input = new StreamReader("Assets/Lang-sheet-ar.csv");
        StreamWriter output = new StreamWriter("Assets/Lang-sheet-ar-reversed.csv");

        while (!input.EndOfStream)
        {
            string inputLine = input.ReadLine();
            Debug.Log(inputLine);
            string[] parts = inputLine.Split(',');
            parts[1] = ArabicFixer.Fix(parts[1]);
            char[] arabic = parts[1].ToCharArray();
//            Array.Reverse(arabic);
            string reversedArabic = new string(arabic);
            parts[1] = reversedArabic;
            string outputLine = string.Join(",", parts);
            output.WriteLine(outputLine);
        }

        input.Close();
        output.Flush();
        output.Close();
	}
}
